<?php

return [
    'version' => '3.0.1',
    /*
    |--------------------------------------------------------------------------
    | Transaction Date Restrictionn
    |--------------------------------------------------------------------------
    |
    | As per management, start date for transaction entry will be restricted for previous days
    */
    'allowed_previous_days' => '3',
    'admin_allowed_previous_days' => '180',

    'js_date_format' => 'dd-M-yyyy',
    'php_date_format' => 'd-m-Y'
];
