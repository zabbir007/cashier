<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTransactionsOsReconciliationDetailsTable extends Migration
{
    private $table = 'transactions_os_reconciliation_details';
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {    Schema::create($this->table, function (Blueprint $table) {
        $table->increments('id')->unsigned();

        $table->string('trans_id')->comment('Date Wise Receipt head id');
        $table->string('trans_sl')->comment('Date Wise Receipt head Serial Number');

        $table->integer('branch_id')->nullable();
        $table->string('branch_name')->nullable();
        $table->string('trans_date')->nullable();

        $table->integer('brand_id')->nullable();
        $table->string('brand_name')->nullable();
        $table->integer('product_id')->nullable();
        $table->string('product_name')->nullable();

        $table->decimal('sales_tp',11,4)->nullable()->comment('Sales Trade Price');
        $table->decimal('sales_vat',11,4)->nullable()->comment('Sales Vat');
        $table->decimal('sales_discount',11,4)->nullable()->comment('Sales Discount');
        $table->decimal('sales_sp_disc',11,4)->nullable()->comment('Sales Special Discount');
        $table->decimal('sales_total',11,4)->nullable()->comment('Sales Total');

        $table->decimal('return_tp',11,4)->nullable()->comment('Return Trade Price');
        $table->decimal('return_vat',11,4)->nullable()->comment('Return Vat');
        $table->decimal('return_discount',11,4)->nullable()->comment('Return Discount');
        $table->decimal('return_sp_disc',11,4)->nullable()->comment('Return Special Discount');
        $table->decimal('return_total',11,4)->nullable()->comment('Return Total');

        $table->decimal('net_sales_tp',11,4)->nullable()->comment('Net Sales Trade Price');
        $table->decimal('net_sales_vat',11,4)->nullable()->comment('Net Sales Vat');
        $table->decimal('net_sales_discount',11,4)->nullable()->comment('Net Sales Discount');
        $table->decimal('net_sales_sp_disc',11,4)->nullable()->comment('Net Sales Special Discount');
        $table->decimal('net_sales_total',11,4)->nullable()->comment('Net Sales Total');

        $table->decimal('op_out',11,4)->nullable()->comment('Opening Outstanding');
        $table->decimal('close_out',11,4)->nullable()->comment('Closing Outstanding');
        $table->decimal('pdf',11,4)->nullable()->comment('As per PDF');
        $table->decimal('as_sales',11,4)->nullable()->comment('As per Sales');

        $table->string('remarks')->nullable();

        $table->tinyInteger('approve')->default(0);
        $table->integer('approve_by')->nullable();
        $table->string('status')->nullable();

        $table->integer('year');
        $table->string('month', 15);

        $table->integer('created_by')->unsigned()->index();
        $table->integer('updated_by')->unsigned()->nullable();
        $table->integer('deleted_by')->unsigned()->nullable();

        $table->timestamps();
        $table->softDeletes();
    });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop($this->table);
    }
}
