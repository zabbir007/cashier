<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOpeningDetailsTable extends Migration
{
    private $table = 'setup_opening_details';
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {    Schema::create($this->table, function (Blueprint $table) {
        $table->increments('id')->unsigned();

        $table->string('trans_id')->comment('Opening head id');
        $table->string('trans_sl')->comment('Opening head Serial Number');

        $table->integer('branch_id')->nullable();
        $table->string('branch_name')->nullable();
        $table->string('trans_date')->nullable();
        $table->integer('brand_id')->nullable();
        $table->string('brand_name')->nullable();
        $table->integer('product_id')->nullable();
        $table->string('product_name')->nullable();

        $table->decimal('collection',20,4)->nullable();
        $table->decimal('outstanding',20,4)->nullable();
        $table->decimal('pc_cash_amount',20,4)->nullable();
        $table->decimal('pc_bank_amount',20,4)->nullable();

        $table->tinyInteger('approve')->default(0);
        $table->integer('approve_by')->nullable();
        $table->string('status')->nullable();

        $table->integer('year');
        $table->string('month', 15);

        $table->integer('created_by')->unsigned()->index();
        $table->integer('updated_by')->unsigned()->nullable();
        $table->integer('deleted_by')->unsigned()->nullable();

        $table->timestamps();
        $table->softDeletes();
    });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop($this->table);
    }
}
