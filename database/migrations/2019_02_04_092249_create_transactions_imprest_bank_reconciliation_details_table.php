<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTransactionsImprestBankReconciliationDetailsTable extends Migration
{
    private $table = 'trns_impr_bnk_details';

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create($this->table, function (Blueprint $table) {
            $table->increments('id')->unsigned();

            $table->string('trans_id')->comment('Imprest bank reconciliation head id');
            $table->string('trans_sl')->comment('Imprest bank reconciliation head serial number');

            $table->integer('branch_id')->index();
            $table->string('branch_code', 5)->index();
            $table->integer('bank_id')->index();
            $table->string('account_number', 50);

            $table->date('entry_date')->comment('selected entry date');
            $table->decimal('amount')->comment('all types of amount');
            $table->tinyInteger('types')->comment('1=Deposit Amount,2=Withdrawn Amount,3=Cheque Issued Amount');
            $table->string('ref_no')->nullable()->comment('Reference No');

            $table->date('trans_date');

            $table->tinyInteger('approve')->default(0);
            $table->integer('approve_by')->nullable();
            $table->string('status')->nullable();

            $table->integer('year');
            $table->integer('month');
            $table->string('yearmonth', 15);

            $table->integer('created_by')->unsigned()->index();
            $table->integer('updated_by')->unsigned()->nullable();
            $table->integer('deleted_by')->unsigned()->nullable();

            $table->timestamp('created_at');
            $table->dateTime('updated_at')->nullable();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop($this->table);
    }
}
