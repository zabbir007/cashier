<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBrandsTable extends Migration
{
    private $table = 'brands';
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {    Schema::create($this->table, function (Blueprint $table) {
        $table->increments('id')->unsigned();

        $table->string('name')->nullable();
        $table->string('code')->nullable();
        $table->string('remarks')->nullable();

        $table->string('order')->nullable();
        $table->tinyInteger('status')->default(1);

        $table->integer('created_by')->unsigned()->index();
        $table->integer('updated_by')->unsigned()->nullable();
        $table->integer('deleted_by')->unsigned()->nullable();

        $table->timestamps();
        $table->softDeletes();
    });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop($this->table);
    }
}
