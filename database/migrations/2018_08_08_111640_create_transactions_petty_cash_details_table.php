<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTransactionsPettyCashDetailsTable extends Migration
{
    private $table = 'transactions_petty_cash_details';
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {    Schema::create($this->table, function (Blueprint $table) {
        $table->increments('id')->unsigned();

        $table->string('trans_id')->comment('Date Wise Receipt head id');
        $table->string('trans_sl')->comment('Date Wise Receipt head Serial Number');

        $table->integer('branch_id')->nullable();
        $table->string('branch_name')->nullable();
        $table->string('trans_date')->nullable();

        $table->decimal('total_receipt_hq',11,4)->comment('Total Receipt From HQ')->nullable();
        $table->decimal('transfer_imprest_cash',11,4)->comment('Transfer to Imprest Cash')->nullable();
        $table->decimal('m_receipt',11,4)->comment('Miss. Receipt')->nullable();
        $table->decimal('total_expense_cq',11,4)->comment('Total Expenses Through CQ')->nullable();
        $table->decimal('total_exp',11,4)->comment('Total Expenses')->nullable();

        $table->string('optional_field1')->nullable();
        $table->string('optional_field2')->nullable();
        $table->string('optional_field3')->nullable();


        $table->tinyInteger('approve')->default(0);
        $table->integer('approve_by')->nullable();
        $table->string('status')->nullable();

        $table->integer('year');
        $table->string('month', 15);

        $table->integer('created_by')->unsigned()->index();
        $table->integer('updated_by')->unsigned()->nullable();
        $table->integer('deleted_by')->unsigned()->nullable();

        $table->timestamps();
        $table->softDeletes();
    });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop($this->table);
    }
}
