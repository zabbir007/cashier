<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTransactionsImprestBankReconciliationSummaryTable extends Migration
{
    private $table = 'trns_impr_bnk_summary';

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create($this->table, function (Blueprint $table) {
            $table->increments('id')->unsigned();

            $table->string('sl')->comment('transaction no')->index();

            $table->integer('branch_id')->index();
            $table->string('branch_code', 5)->index();
            $table->integer('bank_id')->index();
            $table->string('account_number', 50);

            $table->decimal('opening_balance', 13, 6)->default(0)->comment('Opening balance as per bank book');
            $table->decimal('bank_interest', 13, 6)->default(0)->comment('Bank interest of the month (if any)');
            $table->decimal('bank_charge', 13, 6)->default(0)->comment('Bank Charges/AIT/Excise duty fo the month (if any)');
            $table->decimal('closing_balance', 13, 6)->default(0)->comment('Closing balance as per bank statement (B)');

            $table->decimal('closing_per_bank', 13, 6)->default(0)->comment('Closing balance as per bank (A)');
            $table->decimal('closing_per_bank_book', 13, 6)->default(0)->comment('Closing balance as per bank book');

            $table->date('preparation_date');
            $table->date('trans_date');

            $table->tinyInteger('approve')->default(0);
            $table->integer('approve_by')->nullable();
            $table->string('status')->default(1);

            $table->integer('year');
            $table->integer('month');
            $table->string('yearmonth', 15);

            $table->integer('created_by')->unsigned()->index();
            $table->integer('updated_by')->unsigned()->nullable();
            $table->integer('deleted_by')->unsigned()->nullable();

            $table->timestamp('created_at');
            $table->dateTime('updated_at')->nullable();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop($this->table);
    }
}
