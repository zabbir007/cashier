<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTransactionsDateWiseDetailsTable extends Migration
{
    private $table = 'transactions_date_wise_details';
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {    Schema::create($this->table, function (Blueprint $table) {
        $table->increments('id')->unsigned();

        $table->string('trans_id')->comment('Date Wise Receipt head id');
        $table->string('trans_sl')->comment('Date Wise Receipt head Serial Number');

        $table->integer('branch_id')->nullable();
        $table->string('branch_name')->nullable();
        $table->string('trans_date')->nullable();
        $table->integer('brand_id')->nullable();
        $table->string('brand_name')->nullable();
        $table->integer('product_id')->nullable();
        $table->string('product_name')->nullable();

        $table->decimal('collection',11,4)->nullable();
        $table->decimal('money_receipt',11,4)->nullable();
        $table->decimal('adv_receipt',11,4)->nullable();
        $table->decimal('short_receipt',11,4)->comment('Short/Express Receipt')->nullable();
        $table->decimal('adv_adj',11,4)->comment('Advance Adjustment')->nullable();
        $table->decimal('adj_plus',11,4)->comment('Adjustment(+)')->nullable();
        $table->decimal('adj_minus',11,4)->comment('Adjustment(-)')->nullable()->nullable();

        $table->tinyInteger('approve')->default(0);
        $table->integer('approve_by')->nullable();
        $table->string('status')->nullable();

        $table->integer('year');
        $table->string('month', 15);

        $table->integer('created_by')->unsigned()->index();
        $table->integer('updated_by')->unsigned()->nullable();
        $table->integer('deleted_by')->unsigned()->nullable();

        $table->timestamps();
        $table->softDeletes();
    });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop($this->table);
    }
}
