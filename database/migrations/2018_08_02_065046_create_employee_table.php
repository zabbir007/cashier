<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEmployeeTable extends Migration
{
    private $table = 'employees';
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {    Schema::create($this->table, function (Blueprint $table) {
        $table->increments('id')->unsigned();

        $table->string('employee_id')->nullable();
        $table->string('employee_name')->nullable();
        $table->string('employee_designation')->nullable();
        
        $table->string('grade')->nullable();
        $table->string('mobile_no')->nullable();
        $table->string('email_address')->nullable();

        $table->integer('branch_id')->nullable();
        $table->string('branch_code')->nullable();
        $table->string('branch_name')->nullable();

        $table->integer('status_id')->default(1);
        $table->string('status_name')->default('Entry');

        $table->string('order')->nullable();
        $table->tinyInteger('status')->default(1);

        $table->integer('created_by')->unsigned()->index();
        $table->integer('updated_by')->unsigned()->nullable();
        $table->integer('deleted_by')->unsigned()->nullable();

        $table->timestamps();
        $table->softDeletes();
    });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop($this->table);
    }
}
