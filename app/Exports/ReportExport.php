<?php

namespace App\Exports;

use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\FromView;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;

/**
 * Class ReportExport
 *
 * @package App\Exports
 */
class ReportExport implements FromView, ShouldAutoSize
{
    /**
     * @var
     */
    private $_view;
    /**
     * @var
     */
    private $_data;

    /**
     * ReportExport constructor.
     *
     * @param $_view
     * @param $_data
     */
    public function __construct($_view, $_data)
    {
        $this->_view = $_view;
        $this->_data = $_data;
    }

    /**
     * @return View
     */
    public function view(): View
    {
        return view($this->_view, $this->_data);
    }
}
