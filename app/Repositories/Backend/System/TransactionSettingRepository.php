<?php

namespace App\Repositories\Backend\System;

use App\Exceptions\GeneralException;
use App\Models\System\TransactionSetting;
use App\Repositories\BaseRepository;
use Auth;
use Carbon\Carbon;
use Carbon\CarbonPeriod;
use Config;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;
use Request;
use Throwable;

/**
 * Class TransactionSetting
 *
 * @package App\Repositories\Backend\System
 */
final class TransactionSettingRepository extends BaseRepository
{
    /**
     * @return string
     */
    public function model()
    {
        return TransactionSetting::class;
    }


    /**
     * @param  string  $order_by
     * @param  string  $sort
     *
     * @return mixed
     * @throws GeneralException
     */
    public function getAll($order_by = 'id', $sort = 'asc')
    {
        return $this->makeModel()
            ->whereHas('user.roles', function ($q) {
                $q->whereIn('role_id', [3, 5]);
            })
            ->with([
                'user' => function ($q) {
                    return $q->select(['id', 'first_name', 'last_name']);
                }
            ])
            ->with([
                'branch' => function ($q) {
                    return $q->select(['id', 'name', 'code']);
                }
            ])
            ->orderByDesc('expire_at')
            ->get();
    }

    /**
     * @return mixed
     * @throws GeneralException
     */
    public function getForDataTable()
    {
        $dataTableQuery = $this->makeModel()
            ->select(['id', 'user_id', 'branch_id', 'previous', 'interval', 'day', 'expire_at'])
            ->whereHas('user.roles', function ($q) {
                $q->whereIn('role_id', [3, 5]);
            })
            ->with([
                'user' => function ($q) {
                    return $q->select(['id', 'first_name', 'last_name']);
                }
            ])
            ->with([
                'branch' => function ($q) {
                    return $q->select(['id', 'name', 'code']);
                }
            ])
            ->orderByDesc('updated_at');

        return $dataTableQuery;
    }

    /**
     * @param $userId
     * @param $branchId
     * @return string
     * @throws GeneralException
     */
    public function getAllowedDays($userId, $branchId)
    {
        $result = $this->makeModel()
            ->select(['id', 'user_id', 'branch_id', 'previous', 'interval', 'day', 'expire_at'])
            ->where('user_id', $userId)
            ->where('branch_id', $branchId)
            ->where('expire_at', '>', Carbon::now()->toDateTimeString())
            ->first();

        if ($result) {
            $allowedDays = ($result->previous == 1 ? '-' : '+').''.$result->interval.''.$result->day;
        } else {
            $allowedDays = config('application.allowed_previous_days');
        }

        return $allowedDays;
    }

    /**
     * @param  bool  $user
     * @return string
     * @throws GeneralException
     */
    public function getPermittedDays($user = false)
    {
        $user = $user === false ? Auth::user() : $user;
        $user = $user->load('branches');

        $userId = $user->id;
        if ($user->branches) {
            if ($user->branches->count() > 15) {
                $startDate = Carbon::now()->subDays(Config::get('application.admin_allowed_previous_days'));
                $endDate = Carbon::now();
            } else {
                $branchId = $user->branches->first()->id;

                $dates = $this->getStartEndDate($userId, $branchId);
//                $allowedDays = $settingsRepo->getAllowedDays($userId, $branchId);

                if ($dates) {
                    $startDate = Carbon::parse($dates['from_date']);
                    $endDate = Carbon::parse($dates['to_date']);
                } else {
                    $startDate = Carbon::now()->subDays(Config::get('application.allowed_previous_days'));
                    $endDate = Carbon::now();
                }
            }
        } else {
//            $allowedDays = Config::get('application.allowed_previous_days');
            $startDate = Carbon::now()->subDays(Config::get('application.allowed_previous_days'));
            $endDate = Carbon::now();
        }

//        $view->with('start_date', $startDate->format(Config::get('application.php_date_format')));
//        $view->with('end_date', $endDate->format(Config::get('application.php_date_format')));
//        $view->with('start_date', $startDate);
//        $view->with('end_date', $endDate);

        $periods = CarbonPeriod::create($startDate, $endDate);
        //for auto add last two days
        $periods2 = CarbonPeriod::create(Carbon::now()->subDays(Config::get('application.allowed_previous_days')),
            Carbon::now());

        $enabledDates = '';

        foreach ($periods as $period) {
            $enabledDates .= $period->format('d-m-Y').',';
        }

        foreach ($periods2 as $period) {
            $enabledDates .= $period->format('d-m-Y').',';
        }

        return $enabledDates;
    }

    /**
     * @param $userId
     * @param $branchId
     * @return mixed
     * @throws GeneralException
     */
    public function getStartEndDate($userId, $branchId)
    {
        return $this->makeModel()
            ->select(['id', 'user_id', 'branch_id', 'from_date', 'to_date', 'expire_at'])
            ->where('user_id', $userId)
            ->where('branch_id', $branchId)
            ->where('expire_at', '>', Carbon::now()->toDateTimeString())
            ->first();
    }

    /**
     * @param $trans_date
     * @param $userId
     * @param $branchId
     * @return bool
     * @throws GeneralException
     */
    function checkIfAllowed($trans_date, $branchId)
    {
        $transDate = Carbon::parse($trans_date);
        $userId = Request::user()->id;

        $startDate = Carbon::now()->subDays(Config::get('application.allowed_previous_days') + 1);
        $endDate = Carbon::now();
        if ($transDate->isBetween($startDate, $endDate)) {
            return true;
        }

        $dates = $this->makeModel()
            //->select(['from_date', 'to_date'])
            ->where('user_id', $userId)
            ->where('branch_id', $branchId)
            ->where(function ($query) use ($transDate) {
                $query->where('from_date', '<=', $transDate->toDateString())
                    ->where('to_date', '>=', $transDate->toDateString());
            })
            ->where('expire_at', '>', Carbon::now()->toDateTimeString())
            ->count();

        return $dates === 1;
    }

    /**
     * @param $date
     * @return bool
     * @throws GeneralException
     */
    function checkIfAllowed1($date)
    {
        $user = Request::user();
        $user = $user->load('branches');

        $userId = $user->id;
        if ($user->branches) {
            if ($user->branches->count() > 15) {
                $startDate = Carbon::now()->subDays(Config::get('application.admin_allowed_previous_days'));
                $endDate = Carbon::now();
            } else {
                $branchId = $user->branches->first()->id;

                $dates = $this->getStartEndDate($userId, $branchId);


//                $allowedDays = $settingsRepo->getAllowedDays($userId, $branchId);

                if ($dates) {
                    $startDate = Carbon::parse($dates['from_date']);
                    $endDate = Carbon::parse($dates['to_date']);
                } else {
                    $startDate = Carbon::now()->subDays(Config::get('application.allowed_previous_days'));
                    $endDate = Carbon::now();
                }
            }
        } else {
//            $allowedDays = Config::get('application.allowed_previous_days');
            $startDate = Carbon::now()->subDays(Config::get('application.allowed_previous_days'));
            $endDate = Carbon::now();
        }

//        $view->with('start_date', $startDate->format(Config::get('application.php_date_format')));
//        $view->with('end_date', $endDate->format(Config::get('application.php_date_format')));
//        $view->with('start_date', $startDate);
//        $view->with('end_date', $endDate);

        $periods = CarbonPeriod::create($startDate, $endDate);
        //for auto add last two days
        $periods2 = CarbonPeriod::create(Carbon::now()->subDays(Config::get('application.allowed_previous_days')),
            Carbon::now());

        $enabledDates = new Collection();

        foreach ($periods as $period) {
            $enabledDates->push($period->format('Y-m-d'));
        }

        foreach ($periods2 as $period) {
            $enabledDates->push($period->format('Y-m-d'));
        }

        $date = Carbon::parse($date)->format('Y-m-d');

        $enabledDate = $enabledDates->filter(function ($item, $key) use ($date) {
            return $item === $date;
        });

        return $enabledDate->count() > 0;
    }

    /**
     * @param         $id
     * @param  array  $input
     *
     * @return mixed
     * @throws Throwable
     */
    public function update($id, array $input)
    {
        $data = $input['data'];

        $model = $this->find($id);

        $fromDate = Carbon::createFromFormat('d-M-Y', $data['from_date']);
        $toDate = Carbon::createFromFormat('d-M-Y', $data['to_date']);

        $model->from_date = $fromDate->toDateString();
        $model->to_date = $toDate->toDateString();

        $diff = $toDate->diff($fromDate);

        $model->previous = $diff->invert;
        $model->interval = $diff->days;
        $model->day = $diff->m > 0 ? 'm' : 'd';

        $model->expire_at = Carbon::createFromFormat('d-M-Y h:i A',
            $data['date'].' '.$data['time'])->toDateTimeString();

        try {
            return DB::transaction(function () use ($model) {
                if ($model->save()) {
                    return true;
                }

                throw new GeneralException(trans('There was a problem updating this record. Please try again.'));
            });
        } catch (Throwable $e) {
            throw $e;
        }
    }
}
