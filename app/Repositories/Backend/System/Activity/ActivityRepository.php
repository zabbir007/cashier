<?php

namespace App\Repositories\Backend\System\Activity;

use App\Repositories\BaseRepository;
use Spatie\Activitylog\Models\Activity;

/**
 * Class ActivityRepository
 *
 * @package App\Repositories\Backend\System\Activity
 */
final class ActivityRepository extends BaseRepository
{
    /**
     * @return string
     */
    public function model()
    {
        return Activity::class;
    }

    public function getPaginated($perPage = 100){
        return $this->model->with(['subject','causer'])->paginate($perPage);
    }

    /**
     * @param $id
     * @return mixed
     */
    public function show($id)
    {
        return $this->model->find($id);
    }

    /**
     * @param $id
     * @return mixed
     */
    public function getRevisionHistory($id)
    {
        return $this->model->inLog('activity')->where('subject_id', $id)->get();
    }

    /**
     * @param $id
     * @param $causerId
     * @return mixed
     */
    public function getRevisionHistoryByCauser($id, $causerId)
    {
        return $this->model->inLog('activity')->where('subject_id', $id)->where('causer_id', $causerId)->get();
    }
}
