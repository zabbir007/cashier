<?php

namespace App\Repositories\Backend\Access\Permission;

use App\Events\Backend\Access\Permission\PermissionCreated;
use App\Events\Backend\Access\Permission\PermissionUpdated;
use App\Exceptions\GeneralException;
use App\Models\Access\Permission\Permission;
use App\Repositories\BaseRepository;
use DB;
use Illuminate\Database\Eloquent\Model;
use Throwable;

/**
 * Class PermissionRepository.
 */
final class PermissionRepository extends BaseRepository
{
    /**
     * @return string
     */
    public function model()
    {
        return Permission::class;
    }

    /**
     * @param  string  $order_by
     * @param  string  $sort
     *
     * @return mixed
     */
    public function getAll($order_by = 'display_name', $sort = 'asc')
    {
        return $this->with('roles', 'permissions')
            ->orderBy($order_by, $sort)
            ->get();
    }

    /**
     * @return mixed
     */
    public function getForDataTable()
    {
        return $this->getModel()
            ->with('roles')
            ->select([
                config('access.permission_table').'.id',
                config('access.permission_table').'.name',
                config('access.permission_table').'.display_name'
            ]);
    }

    /**
     * @param  array  $input
     *
     * @return void
     * @throws GeneralException
     * @throws Throwable
     */
    public function create(array $input)
    {
        if ($this->where('display_name', $input['display_name'])->first()) {
            throw new GeneralException(trans('exceptions.backend.access.permissions.already_exists'));
        }

        DB::transaction(function () use ($input) {
            $model = $this->getModel();
            $model->display_name = $input['display_name'];
            $model->name = str_slug($input['display_name']);

            if ($model->save()) {
                event(new PermissionCreated($model));

                return true;
            }

            throw new GeneralException(trans('exceptions.backend.access.permissions.create_error'));
        });
    }

    /**
     * @param  Model  $permission
     * @param  array  $input
     *
     * @return void
     * @throws Throwable
     */
    public function update(Model $permission, array $input)
    {
        $permission->name = $input['name'];
        $permission->display_name = $input['display_name'];

        DB::transaction(function () use ($permission) {
            if ($permission->save()) {

                event(new PermissionUpdated($permission));

                return true;
            }

            throw new GeneralException(trans('exceptions.backend.access.permissions.update_error'));
        });
    }

    /**
     * @param  Model  $role
     *
     * @return bool
     * @throws GeneralException
     *
     */
    public function delete(Model $role)
    {
        //Would be stupid to delete the administrator role
        if ($role->id == 1) { //id is 1 because of the seeder
            throw new GeneralException(trans('exceptions.backend.access.roles.cant_delete_admin'));
        }

        //Don't delete the role is there are users associated
        if ($role->users()->count() > 0) {
            throw new GeneralException(trans('exceptions.backend.access.roles.has_users'));
        }

        DB::transaction(function () use ($role) {
            //Detach all associated roles
            $role->permissions()->sync([]);

            if ($role->delete()) {
                event(new RoleDeleted($role));

                return true;
            }

            throw new GeneralException(trans('exceptions.backend.access.roles.delete_error'));
        });
    }

    /**
     * @return mixed
     */
    public function getDefaultUserRole()
    {
        if (is_numeric(config('access.users.default_role'))) {
            return $this->where('id', (int) config('access.users.default_role'))->first();
        }

        return $this->where('name', config('access.users.default_role'))->first();
    }
}
