<?php

namespace App\Repositories\Backend\Access\User;

use App\Events\Backend\Access\User\UserConfirmed;
use App\Events\Backend\Access\User\UserCreated;
use App\Events\Backend\Access\User\UserDeactivated;
use App\Events\Backend\Access\User\UserDeleted;
use App\Events\Backend\Access\User\UserPasswordChanged;
use App\Events\Backend\Access\User\UserPermanentlyDeleted;
use App\Events\Backend\Access\User\UserReactivated;
use App\Events\Backend\Access\User\UserRestored;
use App\Events\Backend\Access\User\UserUnconfirmed;
use App\Events\Backend\Access\User\UserUpdated;
use App\Exceptions\GeneralException;
use App\Models\Access\User\User;
use App\Notifications\Backend\Access\UserAccountActive;
use App\Notifications\Frontend\Auth\UserNeedsConfirmation;
use App\Repositories\Backend\Access\Role\RoleRepository;
use App\Repositories\BaseRepository;
use Auth;
use Event;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Throwable;

/**
 * Class UserRepository.
 */
final class UserRepository extends BaseRepository
{
    /**
     * @var RoleRepository
     */
    protected $role;

    /**
     * @param  RoleRepository  $role
     * @throws GeneralException
     */
    public function __construct(RoleRepository $role)
    {
        parent::__construct();
        $this->role = $role;
    }

    /**
     * @return string
     */
    public function model()
    {
        return User::class;
    }

    /**
     * @param          $permissions
     * @param  string  $by
     *
     * @return mixed
     */
    public function getByPermission($permissions, $by = 'name')
    {
        if (!is_array($permissions)) {
            $permissions = [$permissions];
        }

        return $this->whereHas('roles.permissions', function ($query) use ($permissions, $by) {
            $query->whereIn('permissions.'.$by, $permissions);
        })->get();
    }

    /**
     * @param          $roles
     * @param  string  $by
     *
     * @return mixed
     */
    public function getByRole($roles, $by = 'name')
    {
        if (!is_array($roles)) {
            $roles = [$roles];
        }

        return $this->whereHas('roles', function ($query) use ($roles, $by) {
            $query->whereIn('roles.'.$by, $roles);
        })->get();
    }

    /**
     * @param  int   $status
     * @param  bool  $trashed
     *
     * @return mixed
     */
    public function getForDataTable($status = 1, $trashed = false)
    {
        /**
         * Note: You must return deleted_at or the User getActionButtonsAttribute won't
         * be able to differentiate what buttons to show for each row.
         */
        $dataTableQuery = $this->getModel()
            ->with('roles', 'branches')
            ->select([
                config('access.users_table').'.id',
                config('access.users_table').'.first_name',
                config('access.users_table').'.last_name',
                config('access.users_table').'.username',
                config('access.users_table').'.email',
                config('access.users_table').'.status',
                config('access.users_table').'.confirmed',
                config('access.users_table').'.created_at',
                config('access.users_table').'.updated_at',
                config('access.users_table').'.deleted_at',
                'is_logged',
                'last_login_at'
            ]);

        if ($trashed === 'true') {
            return $dataTableQuery->onlyTrashed();
        }

        // active() is a scope on the UserScope trait
        return $dataTableQuery->active($status);
    }

    /**
     * @param  int   $status
     * @param  bool  $trashed
     *
     * @return mixed
     */
    public function getLoggedUserForDataTable($status = 1, $trashed = false)
    {
        /**
         * Note: You must return deleted_at or the User getActionButtonsAttribute won't
         * be able to differentiate what buttons to show for each row.
         */
        $dataTableQuery = $this->getModel()
            ->with('roles', 'branches')
            ->select([
                config('access.users_table').'.id',
                config('access.users_table').'.first_name',
                config('access.users_table').'.last_name',
                config('access.users_table').'.username',
                config('access.users_table').'.email',
                config('access.users_table').'.status',
                config('access.users_table').'.created_at',
                config('access.users_table').'.updated_at',
                config('access.users_table').'.deleted_at',
                'is_logged',
                'last_login_at',
                'last_login_ip'
            ])
            ->where('is_logged', 1)
            ->whereNotIn('id', ['1', '3', '4', '6', '7']);

        if ($trashed === 'true') {
            return $dataTableQuery->onlyTrashed();
        }

        // active() is a scope on the UserScope trait
        return $dataTableQuery->active($status);
    }

    /**
     * @param  int   $status
     * @param  bool  $trashed
     *
     * @return mixed
     */
    public function getLoggedUsers()
    {
        /**
         * Note: You must return deleted_at or the User getActionButtonsAttribute won't
         * be able to differentiate what buttons to show for each row.
         */
        return $this->getModel()
            ->select([
                'id',
                'is_logged',
                'last_login_at',
                'last_login_ip'
            ])
            ->where('is_logged', 1)
            ->whereNotIn('id', ['1', '3', '4', '6', '7'])
            ->get();
    }

    /**
     * @return mixed
     */
    public function getUnconfirmedCount()
    {
        return $this->where('confirmed', 0)->count();
    }

    /**
     * @param  array  $input
     * @return mixed
     * @throws GeneralException
     * @throws Throwable
     */
    public function create(array $input)
    {
        $data = $input['data'];
        $roles = $input['roles'];
        $branches = $input['branches'];
        $user = $this->createUserStub($data);
        $this->checkUserByUsername($data, $user);

        return DB::transaction(function () use ($user, $data, $roles, $branches) {
            if ($user->save()) {
                //User Created, Validate Roles
                if (!count($roles['assignees_roles'])) {
                    throw new GeneralException(trans('exceptions.backend.access.users.role_needed_create'));
                }

                //Attach new roles
                $user->attachRoles($roles['assignees_roles']);
                //Attach new company list
                $user->branches()->attach($branches['branch_list']);
                //Send confirmation email if requested and account approval is off
                if (isset($data['confirmation_email']) && $user->confirmed == 0 && !config('access.users.requires_approval')) {
                    $user->notify(new UserNeedsConfirmation($user->confirmation_code));
                }

                Event::dispatch(new UserCreated($user));

                return true;
            }

            throw new GeneralException(trans('exceptions.backend.access.users.create_error'));
        });
    }

    /**
     * @param  $input
     *
     * @return mixed
     */
    protected function createUserStub($input)
    {
        $user = $this->getModel();
        $user->first_name = $input['first_name'];
        $user->last_name = $input['last_name'];
        $user->username = $input['username'];
        $user->email = $input['email'];
        $user->password = bcrypt($input['password']);
        $user->status = isset($input['status']) ? 1 : 0;
        $user->confirmation_code = md5(uniqid(mt_rand(), true));
        $user->confirmed = isset($input['confirmed']) ? 1 : 0;

        return $user;
    }

    /**
     * @param  $input
     * @param  $user
     *
     * @throws GeneralException
     */
    protected function checkUserByUsername($input, $user)
    {
        //Figure out if username is not the same
        if (($user->username != $input['username']) && $this->where('username', '=', $input['username'])->first()) {
            throw new GeneralException(trans('exceptions.backend.access.users.email_error'));
        }
    }

    /**
     * @param  Model  $user
     * @param  array  $input
     *
     * @return mixed
     * @throws GeneralException
     * @throws Throwable
     */
    public function update(Model $user, array $input)
    {
        $data = $input['data'];
        $roles = $input['roles'];
        $branches = $input['branches'];

        $this->checkUserByEmail($data, $user);
        $this->checkUserByUsername($data, $user);

        $user->first_name = $data['first_name'];
        $user->last_name = $data['last_name'];
        $user->username = $data['username'];
        $user->email = $data['email'];
        $user->status = isset($data['status']) ? 1 : 0;

        try {
            return DB::transaction(function () use ($user, $roles, $branches) {
                if ($user->save()) {
                    $this->checkUserRolesCount($roles);
                    $this->flushRoles($roles, $user);

                    $user->branches()->detach($branches['branch_list']);
                    $user->branches()->sync($branches['branch_list']);
                    Event::dispatch(new UserUpdated($user));

                    return true;
                }

                throw new GeneralException(trans('exceptions.backend.access.users.update_error'));
            });
        } catch (Throwable $e) {
            throw $e;
        }
    }

    /**
     * @param  $input
     * @param  $user
     *
     * @throws GeneralException
     */
    protected function checkUserByEmail($input, $user)
    {
        //Figure out if email is not the same
        if (($user->email != $input['email']) && $this->where('email', '=', $input['email'])->first()) {
            throw new GeneralException(trans('exceptions.backend.access.users.email_error'));
        }
    }

    /**
     * @param  $roles
     *
     * @throws GeneralException
     */
    protected function checkUserRolesCount($roles)
    {
        //User Updated, Update Roles
        //Validate that there's at least one role chosen
        if (count($roles['assignees_roles']) === 0) {
            throw new GeneralException(trans('exceptions.backend.access.users.role_needed'));
        }
    }

    /**
     * @param $roles
     * @param $user
     */
    protected function flushRoles($roles, $user)
    {
        //Flush roles out, then add array of new ones
        $user->detachRoles($user->roles);
        $user->attachRoles($roles['assignees_roles']);
    }

    /**
     * @param  Model  $user
     * @param         $input
     *
     * @return bool
     * @throws GeneralException
     *
     */
    public function updatePassword(Model $user, $input)
    {
        $user->password = bcrypt($input['password']);

        if ($user->save()) {
            Event::dispatch(new UserPasswordChanged($user));

            return true;
        }

        throw new GeneralException(trans('exceptions.backend.access.users.update_password_error'));
    }

    /**
     * @param  Model  $user
     *
     * @return bool
     * @throws GeneralException
     *
     */
    public function delete(Model $user)
    {
        if (access()->id() == $user->id) {
            throw new GeneralException(trans('exceptions.backend.access.users.cant_delete_self'));
        }

        if ($user->id == 1) {
            throw new GeneralException(trans('exceptions.backend.access.users.cant_delete_admin'));
        }

        if ($user->delete()) {
            Event::dispatch(new UserDeleted($user));

            return true;
        }

        throw new GeneralException(trans('exceptions.backend.access.users.delete_error'));
    }

    /**
     * @param  Model  $user
     *
     * @return mixed
     * @throws GeneralException
     * @throws Throwable
     */
    public function forceDelete(Model $user)
    {
        if ($user->deleted_at === null) {
            throw new GeneralException(trans('exceptions.backend.access.users.delete_first'));
        }

        return DB::transaction(function () use ($user) {
            if ($user->forceDelete()) {
                Event::dispatch(new UserPermanentlyDeleted($user));

                return true;
            }

            throw new GeneralException(trans('exceptions.backend.access.users.delete_error'));
        });
    }

    /**
     * @param  Model  $user
     *
     * @return bool
     * @throws GeneralException
     *
     */
    public function restore(Model $user)
    {
        if ($user->deleted_at === null) {
            throw new GeneralException(trans('exceptions.backend.access.users.cant_restore'));
        }

        if ($user->restore()) {
            Event::dispatch(new UserRestored($user));

            return true;
        }

        throw new GeneralException(trans('exceptions.backend.access.users.restore_error'));
    }

    /**
     * @param  Model  $user
     * @param         $status
     *
     * @return bool
     * @throws GeneralException
     *
     */
    public function mark(Model $user, $status)
    {
        if (Auth::id() === $user->id && $status === 0) {
            throw new GeneralException(trans('exceptions.backend.access.users.cant_deactivate_self'));
        }

        $user->status = $status;

        switch ($status) {
            case 0:
                Event::dispatch(new UserDeactivated($user));
                break;

            case 1:
                Event::dispatch(new UserReactivated($user));
                break;
        }

        if ($user->save()) {
            return true;
        }

        throw new GeneralException(trans('exceptions.backend.access.users.mark_error'));
    }

    /**
     * @param  Model  $user
     *
     * @return bool
     * @throws GeneralException
     */
    public function confirm(Model $user)
    {
        if ($user->confirmed == 1) {
            throw new GeneralException(trans('exceptions.backend.access.users.already_confirmed'));
        }

        $user->confirmed = 1;
        $confirmed = $user->save();

        if ($confirmed) {
            Event::dispatch(new UserConfirmed($user));

            // Let user know their account was approved
            if (config('access.users.requires_approval')) {
                $user->notify(new UserAccountActive());
            }

            return true;
        }

        throw new GeneralException(trans('exceptions.backend.access.users.cant_confirm'));
    }

    /**
     * @param  Model  $user
     *
     * @return bool
     * @throws GeneralException
     */
    public function unconfirm(Model $user)
    {
        if ($user->confirmed == 0) {
            throw new GeneralException(trans('exceptions.backend.access.users.not_confirmed'));
        }

        if ($user->id == 1) {
            // Cant un-confirm admin
            throw new GeneralException(trans('exceptions.backend.access.users.cant_unconfirm_admin'));
        }

        if ($user->id == access()->id()) {
            // Cant un-confirm self
            throw new GeneralException(trans('exceptions.backend.access.users.cant_unconfirm_self'));
        }

        $user->confirmed = 0;
        $unconfirmed = $user->save();

        if ($unconfirmed) {
            Event::dispatch(new UserUnconfirmed($user));

            return true;
        }

        throw new GeneralException(trans('exceptions.backend.access.users.cant_unconfirm')); // TODO
    }
}
