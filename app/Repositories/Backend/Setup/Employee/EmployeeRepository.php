<?php

namespace App\Repositories\Backend\Setup\Employee;

use App\Exceptions\GeneralException;
use App\Models\Employee\Employee;
use App\Repositories\Backend\Setup\Branch\BranchRepository;
use App\Repositories\BaseRepository;
use Illuminate\Support\Facades\DB;
use Throwable;


/**
 * Class EmployeeRepository.
 */
final class EmployeeRepository extends BaseRepository
{
    /**
     * @return string
     */
    public function model()
    {
        return Employee::class;
    }

    /**
     * @param  string  $order_by
     * @param  string  $sort
     *
     * @return mixed
     */
    public function getAll($order_by = 'id', $sort = 'asc')
    {
        return $this
            ->orderBy($order_by, $sort)
            ->get();
    }

    /**
     * @param  array  $input
     * @return mixed
     * @throws Throwable
     */
    public function create(array $input)
    {
        $data = $input['data'];

        $branchRepository = new BranchRepository();

        $branch = $branchRepository->find($data['branch_id']);

        $model = $this->getModel();
        $model->employee_id = $data['employee_id'];
        $model->employee_name = $data['employee_name'];
        $model->employee_designation = $data['employee_designation'];

        $model->grade = $data['grade'];
        $model->mobile_no = $data['mobile_no'];
        $model->email_address = $data['email_address'];
        $model->branch_id = $branch->id;
        $model->branch_code = $branch->code;
        $model->branch_name = $branch->name;
        $model->status = $data['status'] ?? 1;

        try {
            return DB::transaction(function () use ($model) {
                if ($model->save()) {

                    return true;
                }

                throw new GeneralException('There was a problem creating this record. Please try again.');
            });
        } catch (Throwable $e) {
            throw $e;
        }
    }

    /**
     * @param         $id
     * @param  array  $input
     * @return bool
     * @throws Throwable
     */
    public function update($id, array $input)
    {
        $data = $input['data'];

        $model = $this->find($id);

        $model->employee_id = $data['employee_id'];
        $model->employee_name = $data['employee_name'];
        $model->employee_designation = $data['employee_designation'];

        $model->grade = $data['grade'];
        $model->mobile_no = $data['mobile_no'];
        $model->email_address = $data['email_address'];
        $model->branch_id = $data['branch_id'];
        $model->branch_code = $data['branch_code'];
        $model->branch_name = $data['branch_name'];
        $model->status = $data['status'];

        try {
            return DB::transaction(function () use ($model) {
                if ($model->save()) {

                    return true;
                }

                throw new GeneralException(trans('There was a problem updating this record. Please try again.'));
            });
        } catch (Throwable $e) {
            throw $e;
        }
    }


    /**
     * @param $id
     * @return bool
     * @throws Throwable
     */
    public function delete($id)
    {
        $model = $this->find($id);

        try {
            return DB::transaction(function () use ($model) {
                if ($model->delete()) {
                    return true;
                }

                throw new GeneralException(trans('There was a problem deleting this record. Please try again.'));
            });
        } catch (Throwable $e) {
            throw $e;
        }
    }

    /**
     * @param $id
     * @return mixed
     * @throws GeneralException
     * @throws Throwable
     */
    public function forceDelete($id)
    {
        $model = $this->withTrashed()->find($id);
        if ($model->deleted_at === null) {
            throw new GeneralException(trans('This record must be deleted first before it can be destroyed permanently.!'));
        }

        try {
            return DB::transaction(function () use ($model) {
                if ($model->forceDelete()) {

                    return true;
                }

                throw new GeneralException(trans('There was a problem deleting this record. Please try again.'));
            });
        } catch (Throwable $e) {
            throw $e;
        }
    }

    /**
     * @param $id
     * @return bool
     * @throws GeneralException
     * @throws Throwable
     */
    public function restore($id)
    {
        $model = $this->model->withTrashed()->find($id);

        if ($model->deleted_at === null) {
            throw new GeneralException(trans('This record is not deleted so it can not be restored.'));
        }

        try {
            return DB::transaction(function () use ($model) {
                if ($model->restore()) {
                    return true;
                }

                throw new GeneralException(trans('There was a problem restoring this record. Please try again.'));
            });
        } catch (Throwable $e) {
            throw $e;
        }
    }

    /**
     * @param $id
     * @param $status
     *
     * @return bool
     * @throws GeneralException
     */
    public function mark($id, $status)
    {
        $model = $this->find($id);

        $model->status = $status;

        if ($model->save()) {
            return true;
        }

        throw new GeneralException(trans('There was a problem updating this record. Please try again.'));
    }

    /**
     * @param  int   $status
     * @param  bool  $trashed
     *
     * @return mixed
     */
    public function getDeactivated($status = 2, $trashed = false)
    {
        if ($trashed == 'true') {
            return $this->onlyTrashed();
        }

        return $this->active($status)->get();
    }


    /**
     * @param  bool  $status
     * @param  bool  $trashed
     *
     * @return mixed
     */
    public function getDeleted($status = false, $trashed = true)
    {
        if ($trashed == 'true') {
            return $this->onlyTrashed()->get();
        }

        return $this->active($status)->get();
    }
}
