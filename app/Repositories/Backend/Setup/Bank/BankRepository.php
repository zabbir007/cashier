<?php

namespace App\Repositories\Backend\Setup\Bank;

use App\Exceptions\GeneralException;
use App\Models\Bank\Bank;
use App\Repositories\BaseRepository;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;
use Throwable;


/**
 * Class BankRepository
 *
 * @package App\Repositories\Backend\Setup\Bank
 */
final class BankRepository extends BaseRepository
{
    /**
     * @return string
     */
    public function model()
    {
        return Bank::class;
    }


    /**
     * @param  string  $order_by
     * @param  string  $sort
     *
     * @return mixed
     */
    public function getAll($order_by = 'id', $sort = 'asc')
    {
        return $this->select(['id', 'name', 'short_name'])
            ->orderBy($order_by, $sort)
            ->get();
    }

    /**
     * @return Collection
     */
    public function toSelect()
    {
        return $this->select(['id', 'name'])->orderBy('name')->get()->pluck('name', 'id');
    }

    /**
     * @param $branchId
     * @return Collection
     */
    public function toSelectBanks($branchId)
    {
        return $this->select(['id', 'name'])->orderBy('name')
            ->whereHas('branch', function ($q) use ($branchId) {
                return $q->where('branch_id', $branchId);
            })
            ->get()
            ->pluck('name', 'id');
    }

    /**
     * @param $branchId
     * @return Collection
     */
    public function toJsonBanks($branchId)
    {
        return $this->select(['id', 'name'])->orderBy('name')
            ->whereHas('branch', function ($q) use ($branchId) {
                return $q->where('branch_id', $branchId);
            })
            ->get()
            ->toJson();
    }

    /**
     * @param  array  $input
     * @return mixed
     * @throws Throwable
     */
    public function create(array $input)
    {
        $data = $input['data'];
        $model = $this->getModel();

        $model->name = $data['name'];
        $model->short_name = $data['short_name'];
        $model->address = $data['address'];
        $model->remarks = $data['remarks'];

        try {
            return DB::transaction(function () use ($model) {
                if ($model->save()) {

                    return true;
                }

                throw new GeneralException('There was a problem creating this record. Please try again.');
            });
        } catch (Throwable $e) {
            throw $e;
        }
    }

    /**
     * @param         $id
     * @param  array  $input
     *
     * @return mixed
     * @throws Throwable
     */
    public function update($id, array $input)
    {
        $data = $input['data'];

        $model = $this->find($id);

        $model->name = $data['name'];
        $model->short_name = $data['short_name'];
        $model->address = $data['address'];
        $model->remarks = $data['remarks'];

        try {
            return DB::transaction(function () use ($model) {
                if ($model->save()) {
                    return true;
                }

                throw new GeneralException(trans('There was a problem updating this record. Please try again.'));
            });
        } catch (Throwable $e) {
            throw $e;
        }
    }


    /**
     * @param $id
     * @return bool
     * @throws Throwable
     */
    public function delete($id)
    {
        $model = $this->find($id);

        try {
            return DB::transaction(function () use ($model) {
                if ($model->delete()) {
                    return true;
                }

                throw new GeneralException(trans('There was a problem deleting this record. Please try again.'));
            });
        } catch (Throwable $e) {
            throw $e;
        }
    }

    /**
     * @param $id
     * @return mixed
     * @throws GeneralException
     * @throws Throwable
     */
    public function forceDelete($id)
    {
        $model = $this->withTrashed()->find($id);
        if ($model->deleted_at === null) {
            throw new GeneralException(trans('This record must be deleted first before it can be destroyed permanently.!'));
        }

        try {
            return DB::transaction(function () use ($model) {
                if ($model->forceDelete()) {

                    return true;
                }

                throw new GeneralException(trans('There was a problem deleting this record. Please try again.'));
            });
        } catch (Throwable $e) {
            throw $e;
        }
    }

    /**
     * @param $id
     * @return bool
     * @throws GeneralException
     * @throws Throwable
     */
    public function restore($id)
    {
        $model = $this->model->withTrashed()->find($id);

        if ($model->deleted_at === null) {
            throw new GeneralException(trans('This record is not deleted so it can not be restored.'));
        }

        try {
            return DB::transaction(function () use ($model) {
                if ($model->restore()) {
                    return true;
                }

                throw new GeneralException(trans('There was a problem restoring this record. Please try again.'));
            });
        } catch (Throwable $e) {
            throw $e;
        }
    }

    /**
     * @param $id
     * @param $status
     *
     * @return bool
     * @throws GeneralException
     */
    public function mark($id, $status)
    {
        $model = $this->find($id);

        $model->status = $status;

        if ($model->save()) {
            return true;
        }

        throw new GeneralException(trans('There was a problem updating this record. Please try again.'));
    }

    /**
     * @param  int   $status
     * @param  bool  $trashed
     *
     * @return mixed
     */
    public function getDeactivated($status = 2, $trashed = false)
    {
        /**
         * Note: You must return deleted_at or the User getActionButtonsAttribute won't
         * be able to differentiate what buttons to show for each row.
         */

        if ($trashed == 'true') {
            return $this->onlyTrashed();
        }

        // active() is a scope on the UserScope trait
        return $this->active($status)->get();
    }


    /**
     * @param  bool  $status
     * @param  bool  $trashed
     *
     * @return mixed
     */
    public function getDeleted($status = false, $trashed = true)
    {
        /**
         * Note: You must return deleted_at or the User getActionButtonsAttribute won't
         * be able to differentiate what buttons to show for each row.
         */

        if ($trashed == 'true') {
            return $this->onlyTrashed()->get();
        }

        // active() is a scope on the UserScope trait
        return $this->active($status)->get();
    }

}
