<?php

namespace App\Repositories\Backend\Setup\Corporate;

use App\Exceptions\GeneralException;
use App\Models\Corporate\CorporateDetails;
use App\Repositories\BaseRepository;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Throwable;


/**
 * Class CorporateDetailsRepository.
 */
final class CorporateDetailsRepository extends BaseRepository
{
    /**
     * @return string
     */
    public function model()
    {
        return CorporateDetails::class;
    }


    /**
     * @param  string  $order_by
     * @param  string  $sort
     *
     * @return mixed
     */
    public function getAll($order_by = 'sort', $sort = 'asc')
    {
        return $this->orderBy($order_by, $sort)
            ->with('item')
//            ->with('base_group')
//            ->with('unit')
//            ->with('brand')
//            ->with('item_class')
//            ->with('pricing')
            ->with('statuses')
            ->get();
    }

    /**
     * @param $branch
     * @return mixed
     */
    public function getCorporateByBranch($branch)
    {
        return $this->model
            ->whereIn('branch_id', $branch)
            ->sum('collection');
    }

    /**
     * @param $branch
     * @return mixed
     */
    public function getInitialCorporateByBranch($branch)
    {
        return $this->model::selectRaw('sum(collection) as collection')
            //->select(['branch_id', 'collection'])
            ->whereIn('branch_id', $branch)
//                ->groupBy('yearmonth')
            ->get()
            ->first()
            ->collection;
    }

    /**
     * @param  array  $input
     * @return bool
     * @throws GeneralException
     */
    public function create(array $input)
    {
        $data = $input['data'];
        //first get data
        $model = $this->getModel();

        $model->trans_id = $data['trans_id'];
        $model->trans_sl = $data['trans_sl'];

        $model->item_id = $data['item_id'];
//        $model->item_name = $data['item_name'];
        $model->item_brand = $data['item_brand'];
        $model->item_code = $data['item_code'];
        $model->item_unit = $data['item_unit'];
        $model->br_id = $data['br_id'];
        $model->distributor_id = $data['distributor_id'];
        $model->distributor_code = $data['distributor_code'];
        $model->market_return = $data['market_return'];
        $model->distributor_point = $data['distributor_point'];
        $model->submit_date = $data['submit_date'];
        $model->year = $data['year'];
        $model->month = $data['month'];
        $model->week = $data['week'];

        //DB::transaction(function () use ($data, $input, $model) {
        if ($model->save()) {

            return true;
        }

        throw new GeneralException('There was a problem creating this record. Please try again.');
        // });

    }

    /**
     * @param  Model  $model
     * @param  array  $input
     *
     * @return bool
     * @throws GeneralException
     */
    public function update(Model $model, array $input)
    {
        $data = $input['data'];

        $model->trans_id = $data['trans_id'];
        $model->trans_sl = $data['trans_sl'];

        $model->item_id = $data['item_id'];
        // $model->item_name = $data['item_name'];
        $model->item_brand = $data['item_brand'];
        $model->item_unit = $data['item_unit'];
        $model->item_code = $data['item_code'];
        $model->br_id = $data['br_id'];
        $model->distributor_id = $data['distributor_id'];
        $model->distributor_code = $data['distributor_code'];
        $model->market_return = $data['market_return'];
        $model->distributor_point = $data['distributor_point'];
        $model->submit_date = $data['submit_date'];
        $model->year = $data['year'];
        $model->month = $data['month'];
        $model->week = $data['week'];


        // DB::transaction(function () use ($model, $data) {

        if ($model->save()) {

            return true;
        }

        throw new GeneralException(trans('There was a problem updating this record. Please try again.'));
        //});
    }


    /**
     * @param  array  $input
     *
     * @return void
     * @throws Throwable
     */
    public function updateOrCreate(array $input)
    {
        $data = $input['data'];
        $model = $this->getModel();

        $model->trans_id = $data['trans_id'];
        $model->trans_sl = $data['trans_sl'];

        $model->item_id = $data['item_id'];
        $model->item_name = $data['item_name'];
        $model->item_brand = $data['item_brand'];
        $model->item_unit = $data['item_unit'];
        $model->br_id = $data['br_id'];
        $model->distributor_id = $data['distributor_id'];
        $model->market_return = $data['market_return'];
        $model->distributor_point = $data['distributor_point'];
        $model->submit_date = $data['submit_date'];
        $model->year = $data['year'];
        $model->month = $data['month'];
        $model->week = $data['week'];

        DB::transaction(function () use ($model, $data) {

            if ($model->updateOrCreate($data)) {

                return true;
            }

            throw new GeneralException(trans('There was a problem updating this record. Please try again.'));
        });
    }

    /**
     * @param  Model  $model
     * @return bool
     * @throws GeneralException
     */
    public function delete(Model $model)
    {
        if ($model->delete()) {
            //event(new UserDeleted($user));

            return true;
        }

        throw new GeneralException(trans('There was a problem deleting this record. Please try again.'));
    }

    /**
     * @param  Model  $model
     * @throws GeneralException
     * @throws Throwable
     */
    public function forceDelete(Model $model)
    {
        if ($model->deleted_at === null) {
            throw new GeneralException(trans('This record must be deleted first before it can be destroyed permanently.!'));
        }

        DB::transaction(function () use ($model) {
            if ($model->forceDelete()) {
                //event(new UserPermanentlyDeleted($model));

                return true;
            }

            throw new GeneralException(trans('There was a problem deleting this record. Please try again.'));
        });
    }

    /**
     * @param  Model  $user
     *
     * @return bool
     * @throws GeneralException
     *
     */
    public function restore(Model $model)
    {
        if ($model->deleted_at === null) {
            throw new GeneralException(trans('This record is not deleted so it can not be restored.'));
        }

        if ($model->restore()) {
            //event(new UserRestored($user));

            return true;
        }

        throw new GeneralException(trans('There was a problem restoring this record. Please try again.'));
    }

    /**
     * @param  Model  $user
     * @param         $status
     *
     * @return bool
     * @throws GeneralException
     *
     */
    public function mark(Model $model, $status)
    {
        $model->status = $status;

        switch ($status) {
            case 2:
                // event(new UserDeactivated($user));
                break;

            case 1:
                //event(new UserReactivated($user));
                break;
        }

        if ($model->save()) {
            return true;
        }

        throw new GeneralException(trans('There was a problem updating this record. Please try again.'));
    }

    /**
     * @param  int   $status
     * @param  bool  $trashed
     *
     * @return mixed
     */
    public function getDeactivated($status = 2, $trashed = false)
    {
        /**
         * Note: You must return deleted_at or the User getActionButtonsAttribute won't
         * be able to differentiate what buttons to show for each row.
         */
        $dataTableQuery = $this;

        if ($trashed == 'true') {
            return $dataTableQuery->onlyTrashed();
        }

        // active() is a scope on the UserScope trait
        return $dataTableQuery->active($status)->get();
    }


    /**
     * @param  int   $status
     * @param  bool  $trashed
     *
     * @return mixed
     */
    public function getDeleted($status = false, $trashed = true)
    {
        /**
         * Note: You must return deleted_at or the User getActionButtonsAttribute won't
         * be able to differentiate what buttons to show for each row.
         */
        $dataTableQuery = $this;

        if ($trashed == 'true') {
            return $dataTableQuery->onlyTrashed()->get();
        }

        // active() is a scope on the UserScope trait
        return $dataTableQuery->active($status)->get();
    }

}
