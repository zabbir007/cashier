<?php

namespace App\Repositories\Backend\Setup\Closing;

use App\Exceptions\GeneralException;
use App\Models\Closing\ClosingSummary;
use App\Repositories\Backend\Setup\Branch\BranchRepository;
use App\Repositories\Backend\Setup\Product\ProductRepository;
use App\Repositories\BaseRepository;
use Auth;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\DB;
use Log;
use Throwable;

/**
 * Class ClosingSummaryRepository.
 */
final class ClosingSummaryRepository extends BaseRepository
{
    /**
     * @return string
     */
    public function model()
    {
        return ClosingSummary::class;
    }

    /**
     * @param  string  $order_by
     * @param  string  $sort
     *
     * @return mixed
     */
    public function getAll($order_by = 'sl', $sort = 'asc')
    {
        return $this
            ->orderBy($order_by, $sort)
            ->get();
    }

    /**
     * @param          $branches
     * @param  string  $order_by
     * @param  string  $sort
     * @return ClosingSummaryRepository[]|Collection
     */
    public function getAllByBranch($branches, $order_by = 'year', $sort = 'asc')
    {
        return $this
            ->whereIn('branch_id', $branches)
            ->where('year', Carbon::now()->year)
            ->with(['details'])
//            ->with('branch')
            ->orderBy($order_by, $sort)
            ->get();
    }

    /**
     * @param  array  $input
     * @return bool
     * @throws GeneralException
     * @throws Throwable
     */
    public function create(array $input)
    {

        $data = $input['data'];

        $model = $this->getModel();

        $carbon = new Carbon();
        $trans_date = $carbon->copy()->createFromFormat('d-M-Y', $data['trans_date']);


        $branch = new BranchRepository();
        $branch = $branch->find($data['branch_id']);

        $lastSl = $this->getCount() + 1;

        $productRepository = new ProductRepository();

        $sl = strtoupper(substr($branch->name, 0, 3));
        $sl .= '-';
        $sl .= $trans_date->year;
        $sl .= $trans_date->format('m');
        $sl .= $trans_date->day;
        $sl .= '-';
        $sl .= $lastSl;

        $model->sl = $sl;

        $model->branch_id = $branch->id;
        $model->branch_name = $branch->name;

        $model->pc_cash_amount = $data['pc_cash_amount'];
        $model->pc_bank_amount = $data['pc_cash_amount'];


        $model->trans_date = $trans_date->toDateString();
        $model->year = $trans_date->year;
        $model->month = $trans_date->copy()->month;

        $model->created_at = $carbon->toDateTimeString();
        $model->created_by = auth()->user()->id;
        $model->status = 1;


        try {
            return DB::transaction(function () use ($model, $data, $productRepository, $trans_date, $carbon) {
                $alreadyExists = $model
                    ->where('branch_id', $model->branch_id)
                    ->where('year', $model->year)
                    ->count();

                if ($alreadyExists == 0) {
                    if ($model->save()) {
                        foreach ($data['product'] as $productId => $value) {
                            if ($value !== null) {
                                $product = $productRepository->find($productId);

                                if ($product) {

                                    $attributes = [];

                                    $attributes['trans_id'] = $model->id;
                                    $attributes['trans_sl'] = $model->sl;
                                    $attributes['trans_date'] = $trans_date->toDateString();
                                    $attributes['branch_id'] = $model->branch_id;
                                    $attributes['branch_name'] = $model->branch_name;

                                    $attributes['product_id'] = $product->id;
                                    $attributes['product_name'] = $product->name;
                                    $attributes['brand_id'] = $product->brand_id;
                                    $attributes['brand_name'] = $product->brand_name;

                                    $attributes['pc_cash_amount'] = $model->pc_cash_amount;
                                    $attributes['pc_bank_amount'] = $model->pc_bank_amount;

                                    $attributes['collection'] = Arr::get($value, 'collection', 0);
                                    $attributes['outstanding'] = Arr::get($value, 'outstanding', 0);

                                    $attributes['status'] = $model->status;
                                    $attributes['year'] = $model->year;
                                    $attributes['month'] = $model->month;

                                    $attributes['created_by'] = auth()->user()->id;
                                    $attributes['created_at'] = $carbon->toDateTimeString();

                                    $model->details()->insert($attributes);
                                }

                            }
                        }
                        Log::alert('Store : Date Wise Summary Receipt, SL : '.$model->sl.' By User : '.Auth::user()->name);
                        return true;
                    }

                    throw new GeneralException('There was a problem creating this record. Please try again.');
                }

                throw new GeneralException('Records already exists!');
            });
        } catch (Throwable $e) {
            throw $e;
        }
    }

    /**
     * @param         $id
     * @param  array  $input
     *
     * @return bool
     * @throws Throwable
     */
    public function update($id, array $input)
    {
        $data = $input['data'];

        $model = $this->find($id);
        try {
            return DB::transaction(function () use ($model, $data) {
                foreach ($data['product'] as $productId => $value) {
                    if ($value['amount'] !== null) {

                        $attributes = $values = [];

                        $productRep = new ProductRepository();
                        $productRep = $productRep->find($productId);

                        $collection = $value['collection'];
                        $outstanding = $value['outstanding'];

                        $cash = $value['pc_cash_amount'];
                        $bank = $value['pc_bank_amount'];

                        $attributes['trans_id'] = $model->id;
                        $attributes['trans_sl'] = $model->sl;

                        $attributes['trans_date'] = $model->trans_date;
                        $attributes['year'] = $model->year;
                        $attributes['month'] = $model->month;

                        $attributes['product_id'] = $productRep->id;
                        $attributes['product_name'] = $productRep->name;

                        $attributes['brand_id'] = $productRep->brand_id;
                        $attributes['brand_name'] = $productRep->brand_name;
                        $attributes['branch_id'] = $model->branch_id;
                        $attributes['branch_name'] = $model->branch_name;

                        $values['pc_cash_amount'] = $cash;
                        $values['pc_bank_amount'] = $bank;

                        $values['collection'] = $collection;
                        $values['outstanding'] = $outstanding;

                        $values['updated_by'] = access()->user()->id;
                        $values['updated_at'] = Carbon::now()->toDateString();

                        $model->details()->updateOrCreate($attributes, $values);

                    }
                }

                Log::alert('Update : Statement of Physical Stock Count (Expired Products), SL : '.$model->sl.' By User : '.Auth::user()->name);
                return true;
            });
        } catch (Throwable $e) {
            throw $e;
        }
    }

    /**
     * @param $id
     * @return
     */
    public function show($id)
    {
        $data = $this->whereSl($id)->first();
        $detailRep = new DateWiseDetailsRepository();
        $data->details = $detailRep->where('trans_id', $data->id)->where('trans_sl', $data->sl)->get();

        return $data;
    }

    /**
     * @param $id
     * @return bool
     * @throws Throwable
     */
    public function approve($id)
    {
        $model = $this->find($id);

        $model->approve = 1;
        $model->approved_by = Auth::user()->id;
        try {
            return DB::transaction(function () use ($model) {
                if ($model->delete()) {
                    return true;
                }

                throw new GeneralException(trans('There was a problem deleting this record. Please try again.'));
            });
        } catch (Throwable $e) {
            throw $e;
        }
    }

    /**
     * @param $id
     * @return bool
     * @throws Throwable
     */
    public function delete($id)
    {
        $model = $this->find($id);

        try {
            return DB::transaction(function () use ($model) {
                if ($model->delete()) {
                    return true;
                }

                throw new GeneralException(trans('There was a problem deleting this record. Please try again.'));
            });
        } catch (Throwable $e) {
            throw $e;
        }
    }

    /**
     * @param $id
     * @return mixed
     * @throws GeneralException
     * @throws Throwable
     */
    public function forceDelete($id)
    {
        $model = $this->withTrashed()->find($id);
        if ($model->deleted_at === null) {
            throw new GeneralException(trans('This record must be deleted first before it can be destroyed permanently.!'));
        }

        try {
            return DB::transaction(function () use ($model) {
                if ($model->forceDelete()) {

                    return true;
                }

                throw new GeneralException(trans('There was a problem deleting this record. Please try again.'));
            });
        } catch (Throwable $e) {
            throw $e;
        }
    }

    /**
     * @param $id
     * @return bool
     * @throws GeneralException
     * @throws Throwable
     */
    public function restore($id)
    {
        $model = $this->model->withTrashed()->find($id);

        if ($model->deleted_at === null) {
            throw new GeneralException(trans('This record is not deleted so it can not be restored.'));
        }

        try {
            return DB::transaction(function () use ($model) {
                if ($model->restore()) {
                    return true;
                }

                throw new GeneralException(trans('There was a problem restoring this record. Please try again.'));
            });
        } catch (Throwable $e) {
            throw $e;
        }
    }

    /**
     * @param $id
     * @param $status
     *
     * @return bool
     * @throws GeneralException
     */
    public function mark($id, $status)
    {
        $model = $this->find($id);

        $model->status = $status;

        if ($model->save()) {
            return true;
        }

        throw new GeneralException(trans('There was a problem updating this record. Please try again.'));
    }


    /**
     * @param  int   $status
     * @param  bool  $trashed
     *
     * @return mixed
     */
    public function getDeactivated($status = 2, $trashed = false)
    {
        if ($trashed == 'true') {
            return $this->onlyTrashed();
        }

        return $this->active($status)->get();
    }


    /**
     * @param  bool  $status
     * @param  bool  $trashed
     *
     * @return mixed
     */
    public function getDeleted($status = false, $trashed = true)
    {
        if ($trashed == 'true') {
            return $this->onlyTrashed()->get();
        }

        return $this->active($status)->get();
    }

}
