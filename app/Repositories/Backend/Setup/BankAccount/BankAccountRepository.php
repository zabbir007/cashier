<?php

namespace App\Repositories\Backend\Setup\BankAccount;

use App\Exceptions\GeneralException;
use App\Models\BankAccount\BankAccount;
use App\Repositories\BaseRepository;
use App\Repositories\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\DB;
use Throwable;


/**
 * Class BankRepository
 *
 * @package App\Repositories\Backend\Setup\Bank
 */
final class BankAccountRepository extends BaseRepository
{
    /**
     * @return string
     */
    public function model()
    {
        return BankAccount::class;
    }


    /**
     * @param  string  $order_by
     * @param  string  $sort
     *
     * @return mixed
     */
    public function getAll($order_by = 'id', $sort = 'asc')
    {
        return $this->getModel()->orderBy($order_by, $sort)
            ->with([
                'branch' => function ($q) {
                    return $q->select(['id', 'code', 'short_name', 'name']);
                }
            ])
            ->with([
                'bank' => function ($q) {
                    return $q->select(['id', 'short_name', 'name']);
                }
            ])
            ->get();
    }

    /**
     * @param $branchId
     * @param $bankId
     * @return BankAccountRepository[]|Collection
     */
    public function getAccountNumber($branchId, $bankId)
    {
        return $this->getModel()->select(['account_number'])
            ->where('branch_id', $branchId)
            ->where('bank_id', $bankId)
            ->pluck('account_number');
    }

    /**
     * @param  array  $input
     * @return mixed
     * @throws Throwable
     */
    public function create(array $input)
    {
        $data = Arr::get($input, 'data');
        $model = $this->getModel();

        $model->branch_id = $data['branch_id'];
        $model->bank_id = $data['bank_id'];
        $model->account_number = $data['account_number'];
        $model->account_type = 1;
        $model->status = 1;

        try {
            return DB::transaction(function () use ($model) {
                if ($model->save()) {

                    return true;
                }

                throw new GeneralException('There was a problem creating this record. Please try again.');
            });
        } catch (Throwable $e) {
            throw $e;
        }
    }

    /**
     * @param  Model  $model
     * @param  array  $input
     *
     * @return mixed
     * @throws Throwable
     */
    public function update(Model $model, array $input)
    {
        $data = Arr::get($input, 'data');

        $model->account_number = $data['account_number'];

        try {
            return DB::transaction(function () use ($model) {
                if ($model->save()) {

                    return true;
                }

                throw new GeneralException(trans('There was a problem updating this record. Please try again.'));
            });
        } catch (Throwable $e) {
            throw $e;
        }
    }


    /**
     * @param $id
     * @return bool
     * @throws Throwable
     */
    public function delete($id)
    {
        $model = $this->find($id);

        try {
            return DB::transaction(function () use ($model) {
                if ($model->delete()) {
                    return true;
                }

                throw new GeneralException(trans('There was a problem deleting this record. Please try again.'));
            });
        } catch (Throwable $e) {
            throw $e;
        }
    }

    /**
     * @param $id
     * @return mixed
     * @throws GeneralException
     * @throws Throwable
     */
    public function forceDelete($id)
    {
        $model = $this->withTrashed()->find($id);
        if ($model->deleted_at === null) {
            throw new GeneralException(trans('This record must be deleted first before it can be destroyed permanently.!'));
        }

        try {
            return DB::transaction(function () use ($model) {
                if ($model->forceDelete()) {

                    return true;
                }

                throw new GeneralException(trans('There was a problem deleting this record. Please try again.'));
            });
        } catch (Throwable $e) {
            throw $e;
        }
    }

    /**
     * @param $id
     * @return bool
     * @throws GeneralException
     * @throws Throwable
     */
    public function restore($id)
    {
        $model = $this->model->withTrashed()->find($id);

        if ($model->deleted_at === null) {
            throw new GeneralException(trans('This record is not deleted so it can not be restored.'));
        }

        try {
            return DB::transaction(function () use ($model) {
                if ($model->restore()) {
                    return true;
                }

                throw new GeneralException(trans('There was a problem restoring this record. Please try again.'));
            });
        } catch (Throwable $e) {
            throw $e;
        }
    }

    /**
     * @param $id
     * @param $status
     * @return bool
     * @throws GeneralException
     */
    public function mark($id, $status)
    {
        $model = $this->find($id);

        $model->status = $status;

        if ($model->save()) {
            return true;
        }

        throw new GeneralException(trans('There was a problem updating this record. Please try again.'));
    }

    /**
     * @param  int   $status
     * @param  bool  $trashed
     *
     * @return mixed
     */
    public function getDeactivated($status = 2, $trashed = false)
    {
        /**
         * Note: You must return deleted_at or the User getActionButtonsAttribute won't
         * be able to differentiate what buttons to show for each row.
         */

        if ($trashed == 'true') {
            return $this->onlyTrashed();
        }

        // active() is a scope on the UserScope trait
        return $this->active($status)->get();
    }


    /**
     * @param  bool  $status
     * @param  bool  $trashed
     *
     * @return mixed
     */
    public function getDeleted($status = false, $trashed = true)
    {
        /**
         * Note: You must return deleted_at or the User getActionButtonsAttribute won't
         * be able to differentiate what buttons to show for each row.
         */

        if ($trashed == 'true') {
            return $this->onlyTrashed()->get();
        }

        // active() is a scope on the UserScope trait
        return $this->active($status)->get();
    }
}
