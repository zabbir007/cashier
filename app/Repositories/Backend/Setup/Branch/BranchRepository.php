<?php

namespace App\Repositories\Backend\Setup\Branch;

use App\Exceptions\GeneralException;
use App\Models\Branch\Branch;
use App\Repositories\BaseRepository;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;
use Throwable;


/**
 * Class BranchRepository.
 */
final class BranchRepository extends BaseRepository
{
    /**
     * @return string
     */
    public function model()
    {
        return Branch::class;
    }

    /**
     * @param  string  $order_by
     * @param  string  $sort
     *
     * @return mixed
     */
    public function getAll($order_by = 'id', $sort = 'asc')
    {
//        return $this->getModel()->with(['banks' => function ($q) {
//            $q->select(['id', 'name', 'short_name']);
//        }])
        return $this->getModel()->with(['banks'])
            ->orderBy($order_by, $sort)
            ->get();
    }

    /**
     * @return Collection
     */
    public function toSelect()
    {
        return $this->getModel()->select(['id', 'name'])->orderBy('name')->get()->pluck('name', 'id');
    }

    /**
     * @param  array  $input
     * @return mixed
     * @throws Throwable
     */
    public function create(array $input)
    {
        $data = $input['data'];

        $banks = $input['banks'];

        $model = $this->getModel();
        $model->code = $data['code'];
        $model->name = $data['name'];
        $model->location = $data['location'];
        $model->email = $data['email'];
        $model->address1 = $data['address1'];
        $model->address2 = $data['address2'];
        $model->address3 = $data['address3'];
        $model->address4 = $data['address4'];

        try {
            return DB::transaction(function () use ($model, $banks) {
                if ($model->save()) {
                    $model->banks()->attach($banks['bank_list']);
                    return true;
                }

                throw new GeneralException('There was a problem creating this record. Please try again.');
            });
        } catch (Throwable $e) {
            throw $e;
        }
    }

    /**
     * @param         $id
     * @param  array  $input
     *
     * @return bool
     * @throws Throwable
     */
    public function update($id, array $input)
    {
        $data = $input['data'];

        $banks = $input['banks'];

        $model = $this->find($id);

        $model->code = $data['code'];
        $model->name = $data['name'];
        $model->location = $data['location'];
        $model->email = $data['email'];
        $model->address1 = $data['address1'];
        $model->address2 = $data['address2'];
        $model->address3 = $data['address3'];
        $model->address4 = $data['address4'];

        try {
            return DB::transaction(function () use ($model, $banks) {
                if ($model->save()) {
                    //                $model->bank()->detach($banks['branch_list']);
                    $model->banks()->sync($banks['bank_list']);
                    return true;
                }

                throw new GeneralException(trans('There was a problem updating this record. Please try again.'));
            });
        } catch (Throwable $e) {
            throw $e;
        }
    }


    /**
     * @param $id
     * @return bool
     * @throws Throwable
     */
    public function delete($id)
    {
        $model = $this->find($id);

        try {
            return DB::transaction(function () use ($model) {
                if ($model->delete()) {
                    return true;
                }

                throw new GeneralException(trans('There was a problem deleting this record. Please try again.'));
            });
        } catch (Throwable $e) {
            throw $e;
        }
    }

    /**
     * @param $id
     * @return mixed
     * @throws GeneralException
     * @throws Throwable
     */
    public function forceDelete($id)
    {
        $model = $this->withTrashed()->find($id);
        if ($model->deleted_at === null) {
            throw new GeneralException(trans('This record must be deleted first before it can be destroyed permanently.!'));
        }

        try {
            return DB::transaction(function () use ($model) {
                if ($model->forceDelete()) {

                    return true;
                }

                throw new GeneralException(trans('There was a problem deleting this record. Please try again.'));
            });
        } catch (Throwable $e) {
            throw $e;
        }
    }

    /**
     * @param $id
     * @return bool
     * @throws GeneralException
     * @throws Throwable
     */
    public function restore($id)
    {
        $model = $this->model->withTrashed()->find($id);

        if ($model->deleted_at === null) {
            throw new GeneralException(trans('This record is not deleted so it can not be restored.'));
        }

        try {
            return DB::transaction(function () use ($model) {
                if ($model->restore()) {
                    return true;
                }

                throw new GeneralException(trans('There was a problem restoring this record. Please try again.'));
            });
        } catch (Throwable $e) {
            throw $e;
        }
    }

    /**
     * @param $id
     * @param $status
     *
     * @return bool
     * @throws GeneralException
     */
    public function mark($id, $status)
    {
        $model = $this->find($id);

        $model->status = $status;

        if ($model->save()) {
            return true;
        }

        throw new GeneralException(trans('There was a problem updating this record. Please try again.'));
    }

    /**
     * @param  int   $status
     * @param  bool  $trashed
     *
     * @return mixed
     */
    public function getDeactivated($status = 2, $trashed = false)
    {
        if ($trashed == 'true') {
            return $this->onlyTrashed();
        }

        // active() is a scope on the UserScope trait
        return $this->active($status)->get();
    }


    /**
     * @param  bool  $status
     * @param  bool  $trashed
     *
     * @return mixed
     */
    public function getDeleted($status = false, $trashed = true)
    {
        if ($trashed == 'true') {
            return $this->onlyTrashed()->get();
        }

        // active() is a scope on the UserScope trait
        return $this->active($status)->get();
    }
}
