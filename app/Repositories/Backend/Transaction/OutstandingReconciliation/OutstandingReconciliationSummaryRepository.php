<?php

namespace App\Repositories\Backend\Transaction\OutstandingReconciliation;

use App\Exceptions\GeneralException;
use App\Models\OutstandingReconciliation\OutstandingReconciliationSummary;
use App\Repositories\Backend\Setup\Branch\BranchRepository;
use App\Repositories\BaseRepository;
use Auth;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\DB;
use Log;
use Throwable;


/**
 * Class OutstandingReconciliationSummaryRepository
 *
 * @package App\Repositories\Backend\Transaction\OutstandingReconciliation
 */
final class OutstandingReconciliationSummaryRepository extends BaseRepository
{
    /**
     * @return string
     */
    public function model()
    {
        return OutstandingReconciliationSummary::class;
    }

    /**
     * @param  string  $order_by
     * @param  string  $sort
     *
     * @return mixed
     */
    public function getAll($order_by = 'sl', $sort = 'asc')
    {
        return $this
            ->orderBy($order_by, $sort)
            ->get();
    }

    /**
     * @param  int   $status
     * @param  bool  $trashed
     *
     * @return mixed
     */
    public function getForDataTable($status = 1, $trashed = false)
    {
        $dataTableQuery = $this->getModel()
            ->branch()
            ->select([
                'id',
                'sl',
                'branch_name',
                'branch_id',
                'trans_date'
            ])
//            ->orderByDesc('id')
            ->with([
                'details' => function ($q) {
                    return $q->select([
                        'trans_id', 'trans_sl', 'branch_id', 'trans_date', 'brand_id', 'sales_tp', 'sales_vat',
                        'sales_discount', 'sales_sp_disc', 'return_tp', 'return_vat', 'return_discount',
                        'return_sp_disc'
                    ]);
                }
            ]);

        return $dataTableQuery;
    }

    /**
     * @param  array  $input
     * @return bool
     * @throws Throwable
     */
    public function create(array $input)
    {
        $data = Arr::get($input, 'data');

        $model = $this->getModel();

        $carbon = new Carbon();
        $transDate = $carbon->copy()->createFromFormat('d-M-Y', Arr::get($data, 'trans_date'));

        $model->branch_id = Arr::get($data, 'branch_id');
        $model->branch_name = Arr::get($data, 'branch_name');

        $branch = new BranchRepository();
        $branch = $branch->find(Arr::get($data, 'branch_id'));

//        $sl = strtoupper(substr($model->branch_name, 0, 3));
        $sl = $branch->short_name;

        $sl .= '-';
        $sl .= $transDate->year;
        $sl .= $transDate->format('m');
        $sl .= $transDate->day;
        $sl .= '-';
        $sl .= 4361 + $this->query()->where('branch_id', $model->branch_id)->count() + 1;
//        $sl .= $this->getCount() + 1;

        $model->sl = $sl;

        $model->trans_date = $transDate->toDateString();
        $model->year = $transDate->year;
        $model->month = $transDate->copy()->month;

        $model->created_at = $carbon->toDateTimeString();
        $model->created_by = Auth::user()->id;
        $model->status = 1;

        try {
            return DB::transaction(function () use ($model, $data, $transDate, $carbon) {
                $alreadyExists = $model
                    ->where('branch_id', $model->branch_id)
                    ->where('trans_date', $model->trans_date)
                    ->where('month', $model->month)
                    ->where('year', $model->year)
                    ->exists();

                if ($alreadyExists === false) {
                    if ($model->save()) {
                        $grandSalesTotal = $grandReturnTotal = $grandNetTotal = 0;

                        $transactions = [];

                        foreach (Arr::get($data, 'data') as $values) {
                            $transaction = [
                                'trans_id' => $model->id,
                                'trans_sl' => $model->sl,
                                'trans_date' => $transDate->toDateString(),
                                'branch_id' => $model->branch_id,
                                'branch_name' => $model->branch_name,

                                'product_id' => Arr::get($values, 'product_id'),
                                'product_name' => Arr::get($values, 'product_name'),
                                'brand_id' => Arr::get($values, 'brand_id'),
                                'brand_name' => Arr::get($values, 'brand_name'),

                                'sales_tp' => Arr::get($values, 'sales_tp', 0),
                                'sales_vat' => Arr::get($values, 'sales_vat', 0),
                                'sales_discount' => Arr::get($values, 'sales_discount', 0),
                                'sales_sp_disc' => Arr::get($values, 'sales_sp_discount', 0),
                                'sales_total' => (Arr::get($values, 'sales_tp', 0) + Arr::get($values, 'sales_vat',
                                            0)) - (Arr::get($values, 'sales_discount', 0) + Arr::get($values,
                                            'sales_sp_discount', 0)),

                                'return_tp' => Arr::get($values, 'return_tp', 0),
                                'return_vat' => Arr::get($values, 'return_vat', 0),
                                'return_discount' => Arr::get($values, 'return_discount', 0),
                                'return_sp_disc' => Arr::get($values, 'return_sp_discount', 0),
                                'return_total' => (Arr::get($values, 'return_tp', 0) + Arr::get($values, 'return_vat',
                                            0)) - (Arr::get($values, 'return_discount', 0) + Arr::get($values,
                                            'return_sp_discount', 0)),

                                'net_sales_tp' => Arr::get($values, 'net_sales_tp', 0),
                                'net_sales_vat' => Arr::get($values, 'net_sales_vat', 0),
                                'net_sales_discount' => Arr::get($values, 'net_sales_discount', 0),
                                'net_sales_sp_disc' => Arr::get($values, 'net_sales_sp_discount', 0),
                                'net_sales_total' => (Arr::get($values, 'net_sales_tp', 0) + Arr::get($values,
                                            'net_sales_vat', 0)) - (Arr::get($values, 'net_sales_discount',
                                            0) + Arr::get($values, 'net_sales_sp_disc', 0)),

                                'status' => $model->status,
                                'year' => $model->year,
                                'month' => $model->month,
                                'created_by' => Auth::user()->id,
                                'created_at' => $carbon->toDateTimeString(),
                            ];

                            $grandSalesTotal += Arr::get($transaction, 'sales_total');
                            $grandReturnTotal += Arr::get($transaction, 'return_total');
                            $grandNetTotal += Arr::get($transaction, 'net_sales_total');

                            $transactions = Arr::prepend($transactions, $transaction);
                        }

                        if ($model->details()->insert($transactions)) {
                            $model->update([
                                'total_sales' => $grandSalesTotal,
                                'total_return' => $grandReturnTotal,
                                'total_net_sales' => $grandNetTotal
                            ]);
                            Log::alert('Store : Outstanding Reconciliation, SL : '.$model->sl.' By User : '.Auth::user()->name);
                            return true;
                        }

                        throw new GeneralException('There was a problem creating this record. Please try again.');
                    }
                    throw new GeneralException('There was a problem creating this record. Please try again.');
                }

                throw new GeneralException('Records already exists!');
            });
        } catch (Throwable $e) {
            throw  $e;
        }
    }

    /**
     * @param  array  $sl
     * @return
     * @throws GeneralException
     */
    public function show($sl)
    {
        $data = $this->whereSl($sl)->first();
        $detailRep = new OutstandingReconciliationDetailsRepository();
        $data->details = $detailRep->where('trans_id', $data->id)->where('trans_sl', $data->sl)->get();

        return $data;
    }

    /**
     * @param  Model  $model
     * @param  array  $input
     *
     * @return bool
     * @throws Throwable
     */
    public function update(Model $model, array $input)
    {
        $data = Arr::get($input, 'data');

        try {
            return DB::transaction(function () use ($model, $data) {

                $grandSalesTotal = $grandReturnTotal = $grandNetTotal = 0;

                $transactions = [];

                //old values details
                $oldTransactionDetails = $model->load('details');

                //for loop
                foreach (Arr::get($data, 'data') as $values) {
                    $oldTransaction = $oldTransactionDetails->details
                        ->where('product_id', Arr::get($values, 'product_id'));

                    $transaction = [
                        'trans_id' => $model->id,
                        'trans_sl' => $model->sl,
                        'trans_date' => $model->trans_date,

                        'branch_id' => $model->branch_id,
                        'branch_name' => $model->branch_name,

                        'product_id' => Arr::get($values, 'product_id'),
                        'product_name' => Arr::get($values, 'product_name'),
                        'brand_id' => Arr::get($values, 'brand_id'),
                        'brand_name' => Arr::get($values, 'brand_name'),

                        'sales_tp' => Arr::get($values, 'sales_tp') - $oldTransaction->sum('sales_tp'),
                        'sales_vat' => Arr::get($values, 'sales_vat', 0) - $oldTransaction->sum('sales_vat'),
                        'sales_discount' => Arr::get($values, 'sales_discount',
                                0) - $oldTransaction->sum('sales_discount'),
                        'sales_sp_disc' => Arr::get($values, 'sales_sp_discount',
                                0) - $oldTransaction->sum('sales_sp_disc'),
                        'sales_total' => ((Arr::get($values, 'sales_tp', 0) + Arr::get($values, 'sales_vat',
                                        0)) - (Arr::get($values, 'sales_discount', 0) + Arr::get($values,
                                        'sales_sp_discount', 0))) - $oldTransaction->sum('sales_total'),

                        'return_tp' => Arr::get($values, 'return_tp', 0) - $oldTransaction->sum('return_tp'),
                        'return_vat' => Arr::get($values, 'return_vat', 0) - $oldTransaction->sum('return_vat'),
                        'return_discount' => Arr::get($values, 'return_discount',
                                0) - $oldTransaction->sum('return_discount'),
                        'return_sp_disc' => Arr::get($values, 'return_sp_discount',
                                0) - $oldTransaction->sum('return_sp_disc'),
                        'return_total' => ((Arr::get($values, 'return_tp', 0) + Arr::get($values, 'return_vat',
                                        0)) - (Arr::get($values, 'return_discount', 0) + Arr::get($values,
                                        'return_sp_discount', 0))) - $oldTransaction->sum('return_total'),

                        'net_sales_tp' => Arr::get($values, 'net_sales_tp', 0) - $oldTransaction->sum('net_sales_tp'),
                        'net_sales_vat' => Arr::get($values, 'net_sales_vat',
                                0) - $oldTransaction->sum('net_sales_vat'),
                        'net_sales_discount' => Arr::get($values, 'net_sales_discount',
                                0) - $oldTransaction->sum('net_sales_discount'),
                        'net_sales_sp_disc' => Arr::get($values, 'net_sales_sp_discount',
                                0) - $oldTransaction->sum('net_sales_sp_disc'),
                        'net_sales_total' => ((Arr::get($values, 'net_sales_tp', 0) + Arr::get($values, 'net_sales_vat',
                                        0)) - (Arr::get($values, 'net_sales_discount', 0) + Arr::get($values,
                                        'net_sales_sp_disc', 0))) - $oldTransaction->sum('net_sales_total'),

                        'status' => $model->status,
                        'year' => $model->year,
                        'month' => $model->month,

                        'created_by' => $model->created_by,
                        'created_at' => $model->created_at,
                        'updated_by' => Auth::user()->id,
                        'updated_at' => Carbon::now()->toDateTimeString()
                    ];

                    $grandSalesTotal += Arr::get($transaction, 'sales_total');
                    $grandReturnTotal += Arr::get($transaction, 'return_total');
                    $grandNetTotal += Arr::get($transaction, 'net_sales_total');

                    $transactions = Arr::prepend($transactions, $transaction);
                }

                if ($model->details()->insert($transactions)) {
                    $model->update([
                        'total_sales' => $grandSalesTotal,
                        'total_return' => $grandReturnTotal,
                        'total_net_sales' => $grandNetTotal
                    ]);
                    Log::alert('Update : Transaction :: Outstanding Reconciliation, SL : '.$model->sl.' By User : '.Auth::user()->name);
                    return true;
                }

                throw new GeneralException(trans('There was a problem updating this record. Please try again.'));
            });
        } catch (Throwable $e) {
            throw $e;
        }

//        foreach ($data['product'] as $productId => $value) {
//
//            if ((!is_null($value['sales_tp'])) || (!is_null($value['sales_vat'])) || (!is_null($value['sales_discount'])) || (!is_null($value['sales_sp_discount'])) || (!is_null($value['return_tp'])) || (!is_null($value['return_vat']) || (!is_null($value['return_discount'])) || (!is_null($value['return_sp_discount'])) || (!is_null($value['net_sales_tp'])) || (!is_null($value['net_sales_vat'])) || (!is_null($value['net_sales_discount'])) || (!is_null($value['net_sales_sp_discount'])))) {
//
//                $attributes = $values = [];
//
//                $productRep = new ProductRepository();
//                $productRep = $productRep->find($productId);
//
//                $attributes['trans_id'] = $model->id;
//                $attributes['trans_sl'] = $model->sl;
//
//                $attributes['product_id'] = $productRep->id;
//                $attributes['product_name'] = $productRep->name;
//
//                $attributes['brand_id'] = $productRep->brand_id;
//                $attributes['brand_name'] = $productRep->brand_name;
//                $attributes['branch_id'] = $model->branch_id;
//                $attributes['branch_name'] = $model->branch_name;
//
//                $values['sales_tp'] = $value['sales_tp'];
//                $values['sales_vat'] = $value['sales_vat'];
//                $values['sales_discount'] = $value['sales_discount'];
//                $values['sales_sp_disc'] = $value['sales_sp_discount'];
//
//                $values['sales_total'] = ($values['sales_tp'] + $values['sales_vat']) - ($values['sales_discount'] + $values['sales_sp_disc']);
//
//                $grandSalesTotal += $values['sales_total'];
//
//                $values['return_tp'] = $value['return_tp'];
//                $values['return_vat'] = $value['return_vat'];
//                $values['return_discount'] = $value['return_discount'];
//                $values['return_sp_disc'] = $value['return_sp_discount'];
//
//                $values['return_total'] = ($values['return_tp'] + $values['return_vat']) - ($values['return_discount'] + $values['return_sp_disc']);
//
//                $grandReturnTotal += $values['return_total'];
//
//                $values['net_sales_tp'] = $value['net_sales_tp'];
//                $values['net_sales_vat'] = $value['net_sales_vat'];
//                $values['net_sales_discount'] = $value['net_sales_discount'];
//                $values['net_sales_sp_disc'] = $value['net_sales_sp_discount'];
//
//                $values['net_sales_total'] = ($values['net_sales_tp'] + $values['net_sales_vat']) - ($values['net_sales_discount'] + $values['net_sales_sp_disc']);
//
//                $grandNetTotal += $values['net_sales_total'];
//
//                $attributes['trans_date'] = $model->trans_date;
//                $attributes['year'] = $model->year;
//                $attributes['month'] = $model->month;
//
//                $values['updated_by'] = access()->user()->id;
//                $values['updated_at'] = Carbon::now()->toDateString();
//
//                $model->details()->updateOrCreate($attributes, $values);
//
//            }
//
//        }
//
//        $model->update([
//            'total_sales' => $grandSalesTotal,
//            'total_return' => $grandReturnTotal,
//            'total_net_sales' => $grandNetTotal
//        ]);

    }


    /**
     * @param  Model  $model
     * @return bool
     * @throws GeneralException
     */
    public function delete(Model $model)
    {
        if ($model->delete()) {
            //event(new UserDeleted($user));

            return true;
        }

        throw new GeneralException(trans('There was a problem deleting this record. Please try again.'));
    }

    /**
     * @param  Model  $model
     * @return mixed
     * @throws GeneralException
     * @throws Throwable
     */
    public function forceDelete(Model $model)
    {
        if ($model->deleted_at === null) {
            throw new GeneralException(trans('This record must be deleted first before it can be destroyed permanently.!'));
        }

        return DB::transaction(function () use ($model) {
            if ($model->forceDelete()) {
                return true;
            }

            throw new GeneralException(trans('There was a problem deleting this record. Please try again.'));
        });
    }

    /**
     * @param  Model  $model
     * @return bool
     * @throws GeneralException
     */
    public function restore(Model $model)
    {
        if ($model->deleted_at === null) {
            throw new GeneralException(trans('This record is not deleted so it can not be restored.'));
        }

        if ($model->restore()) {
            //event(new UserRestored($user));

            return true;
        }

        throw new GeneralException(trans('There was a problem restoring this record. Please try again.'));
    }

    /**
     * @param  Model  $model
     * @param         $status
     *
     * @return bool
     * @throws GeneralException
     */
    public function mark(Model $model, $status)
    {
        $model->status = $status;

        switch ($status) {
            case 2:
                // event(new UserDeactivated($user));
                break;

            case 1:
                //event(new UserReactivated($user));
                break;
        }

        if ($model->save()) {
            return true;
        }

        throw new GeneralException(trans('There was a problem updating this record. Please try again.'));
    }

    /**
     * @param  int   $status
     * @param  bool  $trashed
     *
     * @return mixed
     */
    public function getDeactivated($status = 2, $trashed = false)
    {
        /**
         * Note: You must return deleted_at or the User getActionButtonsAttribute won't
         * be able to differentiate what buttons to show for each row.
         */
        $dataTableQuery = $this;

        if ($trashed == 'true') {
            return $dataTableQuery->onlyTrashed();
        }

        // active() is a scope on the UserScope trait
        return $dataTableQuery->active($status)->get();
    }


    /**
     * @param  bool  $status
     * @param  bool  $trashed
     *
     * @return mixed
     */
    public function getDeleted($status = false, $trashed = true)
    {
        /**
         * Note: You must return deleted_at or the User getActionButtonsAttribute won't
         * be able to differentiate what buttons to show for each row.
         */
        $dataTableQuery = $this;

        if ($trashed == 'true') {
            return $dataTableQuery->onlyTrashed()->get();
        }

        // active() is a scope on the UserScope trait
        return $dataTableQuery->active($status)->get();
    }

}
