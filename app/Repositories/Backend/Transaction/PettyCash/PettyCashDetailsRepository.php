<?php

namespace App\Repositories\Backend\Transaction\PettyCash;

use App\Exceptions\GeneralException;
use App\Models\PettyCash\PettyCashDetails;
use App\Repositories\BaseRepository;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Arr;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\DB;
use Throwable;


/**
 * Class PettyCashDetailsRepository.
 */
final class PettyCashDetailsRepository extends BaseRepository
{
    /**
     * @return string
     */
    public function model()
    {
        return PettyCashDetails::class;
    }

    /**
     * @param  string  $order_by
     * @param  string  $sort
     *
     * @return mixed
     */
    public function getAll($order_by = 'sort', $sort = 'asc')
    {
        return $this
            ->orderBy($order_by, $sort)
            ->with('item')
//            ->with('base_group')
//            ->with('unit')
//            ->with('brand')
//            ->with('item_class')
//            ->with('pricing')
            ->with('statuses')
            ->get();
    }

    /**
     * @param $branch
     * @return mixed
     */
    public function getCurrentTotalExpenseCqByBranch($branch)
    {
        $startDate = Carbon::now()->firstOfMonth();
        $endDate = Carbon::now()->lastOfMonth();

        return $this->model::whereIn('branch_id', $branch)
            ->whereBetween('trans_date', [$startDate->toDateString(), $endDate->toDateString()])
//            ->where('month', Carbon::now()->month)
//            ->where('year', Carbon::now()->year)
            ->sum('total_expense_cq');
    }

    /**
     * @param $branch
     * @return mixed
     */
    public function getCurrentTotalExpenseByBranch($branch)
    {
        $startDate = Carbon::now()->firstOfMonth();
        $endDate = Carbon::now()->lastOfMonth();

        return $this->model::whereIn('branch_id', $branch)
            ->whereBetween('trans_date', [$startDate->toDateString(), $endDate->toDateString()])
//            ->where('month', Carbon::now()->month)
//            ->where('year', Carbon::now()->year)
            ->sum('total_exp');
    }

    /**
     * @param $branch
     * @param $startDate
     * @param $endDate
     * @return mixed
     */
    public function getCurrentTotalExpenseByBranchYearmonth($branch, $startDate, $endDate)
    {
        $allExpCq = $this->model::selectRaw('concat(year,month) as yearmonth, (sum(total_expense_cq) + sum(total_exp)) as total_expense_cq')
            //  ->addSelect(['month', 'year', 'trans_date','net_sales_total'])
            ->whereIn('branch_id', $branch)
            ->whereBetween('trans_date', [$startDate->toDateString(), $endDate->toDateString()])
            //  ->whereBetween('month', [$startDate->format('n'), $endDate->format('n')])
            //  ->whereBetween('year', [$startDate->format('Y'), $endDate->format('Y')])
            // ->where('collection', '<>', 0)
            ->groupBy('yearmonth')
            ->get()
            ->toArray();

//                ->select(['month', 'year', 'branch_id', 'total_expense_cq', 'total_exp'])
//                ->whereIn('branch_id', $branch)
//                ->whereBetween('trans_date', [$startDate->toDateString(), $endDate->toDateString()])
////                ->whereBetween('month', [$startDate->format('n'), $endDate->format('n')])
////                ->whereBetween('year', [$startDate->format('Y'), $endDate->format('Y')])
//                ->where('total_expense_cq', '!=', 0)
//                ->orWhere('total_exp', '!=', 0)
//                ->get();

        return Arr::pluck($allExpCq, 'total_expense_cq', 'yearmonth');
    }

    /**
     * @param  array  $input
     * @return mixed
     * @throws Throwable
     */
    public function create(array $input)
    {
        $data = Arr::get($input, 'data');

        //first get data
        $model = $this->getModel();

        $model->trans_id = $data['trans_id'];
        $model->trans_sl = $data['trans_sl'];

        $model->item_id = $data['item_id'];
//        $model->item_name = $data['item_name'];
        $model->item_brand = $data['item_brand'];
        $model->item_code = $data['item_code'];
        $model->item_unit = $data['item_unit'];
        $model->br_id = $data['br_id'];
        $model->distributor_id = $data['distributor_id'];
        $model->distributor_code = $data['distributor_code'];
        $model->market_return = $data['market_return'];
        $model->distributor_point = $data['distributor_point'];
        $model->submit_date = $data['submit_date'];
        $model->year = $data['year'];
        $model->month = $data['month'];
        $model->week = $data['week'];

        try {
            return DB::transaction(function () use ($model) {
                if ($model->save()) {

                    return true;
                }

                throw new GeneralException('There was a problem creating this record. Please try again.');
            });
        } catch (Throwable $e) {
            throw $e;
        }

    }

    /**
     * @param  Model  $model
     * @param  array  $input
     *
     * @return mixed
     * @throws Throwable
     */
    public function update(Model $model, array $input)
    {
        $data = $input['data'];

        $model->trans_id = $data['trans_id'];
        $model->trans_sl = $data['trans_sl'];

        $model->item_id = $data['item_id'];
        // $model->item_name = $data['item_name'];
        $model->item_brand = $data['item_brand'];
        $model->item_unit = $data['item_unit'];
        $model->item_code = $data['item_code'];
        $model->br_id = $data['br_id'];
        $model->distributor_id = $data['distributor_id'];
        $model->distributor_code = $data['distributor_code'];
        $model->market_return = $data['market_return'];
        $model->distributor_point = $data['distributor_point'];
        $model->submit_date = $data['submit_date'];
        $model->year = $data['year'];
        $model->month = $data['month'];
        $model->week = $data['week'];


        try {
            return DB::transaction(function () use ($model) {
                if ($model->save()) {
                    return true;
                }

                throw new GeneralException(trans('There was a problem updating this record. Please try again.'));
            });
        } catch (Throwable $e) {
            throw $e;
        }
    }


    /**
     * @param  array  $input
     *
     * @return mixed
     * @throws Throwable
     */
    public function updateOrCreate(array $input)
    {
        $data = Arr::get($input, 'data');
        $model = $this->getModel();

        $model->trans_id = $data['trans_id'];
        $model->trans_sl = $data['trans_sl'];

        $model->item_id = $data['item_id'];
        $model->item_name = $data['item_name'];
        $model->item_brand = $data['item_brand'];
        $model->item_unit = $data['item_unit'];
        $model->br_id = $data['br_id'];
        $model->distributor_id = $data['distributor_id'];
        $model->market_return = $data['market_return'];
        $model->distributor_point = $data['distributor_point'];
        $model->submit_date = $data['submit_date'];
        $model->year = $data['year'];
        $model->month = $data['month'];
        $model->week = $data['week'];

        try {
            return DB::transaction(function () use ($model, $data) {
                if ($model->updateOrCreate($data)) {
                    return true;
                }

                throw new GeneralException(trans('There was a problem updating this record. Please try again.'));
            });
        } catch (Throwable $e) {
            throw $e;
        }
    }

    /**
     * @param  Model  $model
     * @return bool
     * @throws GeneralException
     */
    public function delete(Model $model)
    {
        if ($model->delete()) {
            return true;
        }

        throw new GeneralException(trans('There was a problem deleting this record. Please try again.'));
    }

    /**
     * @param  Model  $model
     * @return mixed
     * @throws GeneralException
     * @throws Throwable
     */
    public function forceDelete(Model $model)
    {
        if ($model->deleted_at === null) {
            throw new GeneralException(trans('This record must be deleted first before it can be destroyed permanently.!'));
        }

        try {
            return DB::transaction(function () use ($model) {
                if ($model->forceDelete()) {
                    return true;
                }

                throw new GeneralException(trans('There was a problem deleting this record. Please try again.'));
            });
        } catch (Throwable $e) {
            throw $e;
        }
    }

    /**
     * @param  Model  $model
     * @return bool
     * @throws GeneralException
     */
    public function restore(Model $model)
    {
        if ($model->deleted_at === null) {
            throw new GeneralException(trans('This record is not deleted so it can not be restored.'));
        }

        if ($model->restore()) {
            return true;
        }

        throw new GeneralException(trans('There was a problem restoring this record. Please try again.'));
    }

    /**
     * @param  Model  $model
     * @param         $status
     *
     * @return bool
     * @throws GeneralException
     */
    public function mark(Model $model, $status)
    {
        $model->status = $status;

        switch ($status) {
            case 2:
                // event(new UserDeactivated($user));
                break;

            case 1:
                //event(new UserReactivated($user));
                break;
        }

        if ($model->save()) {
            return true;
        }

        throw new GeneralException(trans('There was a problem updating this record. Please try again.'));
    }

    /**
     * @param  int   $status
     * @param  bool  $trashed
     *
     * @return mixed
     */
    public function getDeactivated($status = 2, $trashed = false)
    {
        /**
         * Note: You must return deleted_at or the User getActionButtonsAttribute won't
         * be able to differentiate what buttons to show for each row.
         */
        $dataTableQuery = $this;

        if ($trashed == 'true') {
            return $dataTableQuery->onlyTrashed();
        }

        // active() is a scope on the UserScope trait
        return $dataTableQuery->active($status)->get();
    }


    /**
     * @param  int   $status
     * @param  bool  $trashed
     *
     * @return mixed
     */
    public function getDeleted($status = false, $trashed = true)
    {
        /**
         * Note: You must return deleted_at or the User getActionButtonsAttribute won't
         * be able to differentiate what buttons to show for each row.
         */
        $dataTableQuery = $this;

        if ($trashed == 'true') {
            return $dataTableQuery->onlyTrashed()->get();
        }

        // active() is a scope on the UserScope trait
        return $dataTableQuery->active($status)->get();
    }

}
