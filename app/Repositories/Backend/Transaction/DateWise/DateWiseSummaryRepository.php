<?php

namespace App\Repositories\Backend\Transaction\DateWise;

use App\Exceptions\GeneralException;
use App\Models\DateWise\DateWiseSummary;
use App\Repositories\Backend\Setup\Branch\BranchRepository;
use App\Repositories\BaseRepository;
use Auth;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\DB;
use Log;
use Throwable;

/**
 * Class DateWiseSummaryRepository.
 */
final class DateWiseSummaryRepository extends BaseRepository
{
    /**
     * @return string
     */
    public function model()
    {
        return DateWiseSummary::class;
    }

    /**
     * @param  int   $status
     * @param  bool  $trashed
     *
     * @return mixed
     */
    public function getForDataTable($status = 1, $trashed = false)
    {
        $dataTableQuery = $this->getModel()
            ->branch()
            ->select([
                'id',
                'sl',
                'branch_id',
                'branch_name',
                'trans_date'
            ])
//            ->orderByDesc('id')
            ->with([
                'details' => function ($q) {
                    return $q->select([
                        'trans_id', 'trans_sl', 'collection', 'money_receipt', 'adv_receipt', 'short_receipt',
                        'adv_adj', 'adj_plus', 'adj_minus','mr_reverse'
                    ]);
                }
            ]);

        return $dataTableQuery;
    }


    /**
     * @param  string  $order_by
     * @param  string  $sort
     * @return mixed
     */
    public function getAll($order_by = 'sl', $sort = 'asc')
    {
        return $this->orderBy($order_by, $sort)
            ->get();
    }


    /**
     * @param $input
     * @return bool
     * @throws GeneralException
     * @throws Throwable
     */
    public function create(array $input)
    {
        $data = Arr::get($input, 'data');

        $model = $this->getModel();

        $carbon = new Carbon();
        $transDate = $carbon->copy()->createFromFormat('d-M-Y', Arr::get($data, 'trans_date'));
        $branchRepository = new BranchRepository();
        $branch = $branchRepository->find(Arr::get($data, 'branch_id'));

        $lastSl = 5891 + $this->query()->where('branch_id', $branch->id)->count() + 1;
//        $lastSl = $this->getCount() + 1;

//        $productRepository = new ProductRepository();

        $sl = $branch->short_name;
        $sl .= '-';
        $sl .= $transDate->year;
        $sl .= $transDate->format('m');
        $sl .= $transDate->day;
        $sl .= '-';
        $sl .= $lastSl;

        $model->sl = $sl;

        $model->branch_id = $branch->id;
        $model->branch_name = $branch->name;

        $model->trans_date = $transDate->toDateString();
        $model->year = $transDate->year;
        $model->month = $transDate->copy()->month;

        $model->created_at = $carbon->toDateTimeString();
        $model->created_by = Auth::user()->id;
        $model->status = 1;

        $alreadyExists = $model
            ->where('branch_id', $model->branch_id)
            ->where('trans_date', $model->trans_date)
            ->where('month', $model->month)
            ->where('year', $model->year)
            ->exists();

        if ($alreadyExists === false) {
            return DB::transaction(function () use ($model, $data) {

                if ($model->save()) {

                    $transactions = [];

                    foreach (Arr::get($data, 'product') as $values) {
                        $transactions = Arr::prepend($transactions, [
                            'trans_id' => $model->id,
                            'trans_sl' => $model->sl,
                            'trans_date' => $model->trans_date,

                            'product_id' => Arr::get($values, 'product_id'),
                            'brand_id' => Arr::get($values, 'brand_id'),
                            'branch_id' => Arr::get($data, 'branch_id'),

                            'collection' => Arr::get($values, 'collection', 0),
                            'money_receipt' => Arr::get($values, 'money_receipt', 0),
                            'adv_receipt' => Arr::get($values, 'adv_receipt', 0),
                            'short_receipt' => Arr::get($values, 'short_receipt', 0),
                            'adv_adj' => Arr::get($values, 'adv_adj', 0),
                            'adj_plus' => Arr::get($values, 'adj_plus', 0),
                            'adj_minus' => Arr::get($values, 'adj_minus', 0),
                            'mr_reverse' => Arr::get($values, 'mr_reverse', 0),
                            'brand_name' => Arr::get($values, 'brand_name'),
                            'branch_name' => Arr::get($data, 'branch_name'),
                            'product_name' => Arr::get($values, 'product_name'),
                            'created_by' => Auth::user()->id,
                            'created_at' => Carbon::now()->toDateTimeString(),
                            'year' => Carbon::parse($model->trans_date)->year,
                            'month' => Carbon::parse($model->trans_date)->month,
                        ]);

//                    $product = $productRepository->find($productId);
//                    if ($product) {
//                            $attributes = [];
//
//                            $attributes['trans_id'] = $model->id;
//                            $attributes['trans_sl'] = $model->sl;
//                            $attributes['trans_date'] = $transDate->toDateString();
//                            $attributes['branch_id'] = $model->branch_id;
//                            $attributes['branch_name'] = $model->branch_name;
//
//                            $attributes['product_id'] = $product->id;
//                            $attributes['product_name'] = $product->name;
//                            $attributes['brand_id'] = $product->brand_id;
//                            $attributes['brand_name'] = $product->brand_name;
//
//                            $attributes['collection'] = Arr::get($value, 'collection', 0);
//                            $attributes['money_receipt'] = Arr::get($value, 'money_receipt', 0);
//                            $attributes['adv_receipt'] = Arr::get($value, 'adv_receipt', 0);
//                            $attributes['short_receipt'] = Arr::get($value, 'short_receipt', 0);
//                            $attributes['adv_adj'] = Arr::get($value, 'adv_adj', 0);
//                            $attributes['adj_plus'] = Arr::get($value, 'adj_plus', 0);
//                            $attributes['adj_minus'] = Arr::get($value, 'adj_minus', 0);
//
//                            $attributes['status'] = $model->status;
//                            $attributes['year'] = $model->year;
//                            $attributes['month'] = $model->month;
//
//                            $attributes['created_by'] = auth()->user()->id;
//                            $attributes['created_at'] = $carbon->toDateTimeString();
//
//                            $model->details()->insert($attributes);
//                        }
                    }

                    if ($model->details()->insert($transactions)) {
                        Log::alert('Store : Date Wise Summary Receipt, SL : '.$model->sl.' By User : '.Auth::user()->name);
                        return true;
                    }
                    throw new GeneralException('There was a problem creating this record. Please try again.');
                }
                throw new GeneralException('There was a problem creating this record. Please try again.');
            });
        }

        throw new GeneralException('Records already exists!');

    }


    /**
     * @param $sl
     * @return mixed
     * @throws GeneralException
     */
    public function show($sl)
    {
        $data = $this->whereSl($sl)->first();
        $detailRep = new DateWiseDetailsRepository();
        $data->details = $detailRep->where('trans_id', $data->id)->where('trans_sl', $data->sl)->get();

        return $data;
    }

    /**
     * @param  Model  $model
     * @param  array  $input
     *
     * @return mixed
     * @throws Throwable
     */
    public function update(Model $model, array $input)
    {
        $data = Arr::get($input, 'data');

        try {
            return DB::transaction(function () use ($model, $data) {

                $transactions = [];

                //old values details
                $oldTransactionDetails = $model->load('details');

                foreach (Arr::get($data, 'product') as $values) {
                    $oldTransaction = $oldTransactionDetails->details->where('product_id',
                        Arr::get($values, 'product_id'))->values();

                    $transactions = Arr::prepend($transactions, [
                        'trans_id' => $model->id,
                        'trans_sl' => $model->sl,
                        'trans_date' => $model->trans_date,
                        'branch_id' => $model->branch_id,
                        'branch_name' => $model->branch_name,

                        'product_id' => Arr::get($values, 'product_id'),
                        'product_name' => Arr::get($values, 'product_name'),

                        'brand_id' => Arr::get($values, 'brand_id'),
                        'brand_name' => Arr::get($values, 'brand_name'),

                        'collection' => Arr::get($values, 'collection', 0) - $oldTransaction->sum('collection'),
                        'money_receipt' => Arr::get($values, 'money_receipt',
                                0) - $oldTransaction->sum('money_receipt'),
                        'adv_receipt' => Arr::get($values, 'adv_receipt', 0) - $oldTransaction->sum('adv_receipt'),
                        'short_receipt' => Arr::get($values, 'short_receipt',
                                0) - $oldTransaction->sum('short_receipt'),
                        'adv_adj' => Arr::get($values, 'adv_adj', 0) - $oldTransaction->sum('adv_adj'),
                        'adj_plus' => Arr::get($values, 'adj_plus', 0) - $oldTransaction->sum('adj_plus'),
                        'adj_minus' => Arr::get($values, 'adj_minus', 0) - $oldTransaction->sum('adj_minus'),
                        'mr_reverse' => Arr::get($values, 'mr_reverse', 0) - $oldTransaction->sum('mr_reverse'),

                        'created_by' => $model->created_by,
                        'created_at' => $model->created_at,

                        'updated_by' => Auth::user()->id,
                        'updated_at' => Carbon::now()->toDateTimeString(),

                        'year' => Carbon::parse($model->trans_date)->year,
                        'month' => Carbon::parse($model->trans_date)->month,
                    ]);
                }

                if ($model->details()->insert($transactions)) {
                    Log::alert('Update : Statement of Date Wise Summery  (Expired Products), SL : '.$model->sl.' By User : '.Auth::user()->name);

                    return true;
                }

                throw new GeneralException(trans('There was a problem updating this record. Please try again.'));
            });
        } catch (Throwable $e) {
            throw $e;
        }
    }


    /**
     * @param  Model  $model
     * @return bool
     * @throws GeneralException
     */
    public function delete(Model $model)
    {
        if ($model->delete()) {
            return true;
        }

        throw new GeneralException(trans('There was a problem deleting this record. Please try again.'));
    }


    /**
     * @param  Model  $model
     * @return mixed
     * @throws GeneralException
     * @throws Throwable
     */
    public function forceDelete(Model $model)
    {
        if ($model->deleted_at === null) {
            throw new GeneralException(trans('This record must be deleted first before it can be destroyed permanently.!'));
        }

        try {
            return DB::transaction(function () use ($model) {
                if ($model->forceDelete()) {
                    return true;
                }

                throw new GeneralException(trans('There was a problem deleting this record. Please try again.'));
            });
        } catch (Throwable $e) {
            throw $e;
        }
    }


    /**
     * @param  Model  $model
     * @return bool
     * @throws GeneralException
     */
    public function restore(Model $model)
    {
        if ($model->deleted_at === null) {
            throw new GeneralException(trans('This record is not deleted so it can not be restored.'));
        }

        if ($model->restore()) {
            return true;
        }

        throw new GeneralException(trans('There was a problem restoring this record. Please try again.'));
    }


    /**
     * @param  Model  $model
     * @param         $status
     * @return bool
     * @throws GeneralException
     */
    public function mark(Model $model, $status)
    {
        $model->status = $status;

        switch ($status) {
            case 2:
                // event(new UserDeactivated($user));
                break;
            case 1:
                //event(new UserReactivated($user));
                break;
        }

        if ($model->save()) {
            return true;
        }

        throw new GeneralException(trans('There was a problem updating this record. Please try again.'));
    }


    /**
     * @param  int   $status
     * @param  bool  $trashed
     * @return mixed
     */
    public function getDeactivated($status = 2, $trashed = false)
    {
        /**
         * Note: You must return deleted_at or the User getActionButtonsAttribute won't
         * be able to differentiate what buttons to show for each row.
         */

        if ($trashed == 'true') {
            return $this->onlyTrashed();
        }

        // active() is a scope on the UserScope trait
        return $this->active($status)->get();
    }


    /**
     * @param  bool  $status
     * @param  bool  $trashed
     * @return mixed
     */
    public function getDeleted($status = false, $trashed = true)
    {
        /**
         * Note: You must return deleted_at or the User getActionButtonsAttribute won't
         * be able to differentiate what buttons to show for each row.
         */

        if ($trashed == 'true') {
            return $this->onlyTrashed()->get();
        }

        // active() is a scope on the UserScope trait
        return $this->active($status)->get();
    }

}
