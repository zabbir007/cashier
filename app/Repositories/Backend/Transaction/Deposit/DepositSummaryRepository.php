<?php

namespace App\Repositories\Backend\Transaction\Deposit;

use App\Exceptions\GeneralException;
use App\Models\Deposit\DepositSummary;
use App\Repositories\Backend\Setup\Branch\BranchRepository;
use App\Repositories\Backend\Transaction\DateWise\DateWiseDetailsRepository;
use App\Repositories\BaseRepository;
use Auth;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\DB;
use Log;
use Throwable;


/**
 * Class DepositSummaryRepository
 *
 * @package App\Repositories\Backend\Transaction\Deposit
 */
final class DepositSummaryRepository extends BaseRepository
{
    /**
     * @return string
     */
    public function model()
    {
        return DepositSummary::class;
    }

    /**
     * @param  string  $order_by
     * @param  string  $sort
     *
     * @return mixed
     */
    public function getAll($order_by = 'sl', $sort = 'asc')
    {
        return $this->orderBy($order_by, $sort)
            ->get();
    }

    /**
     * @param  int   $status
     * @param  bool  $trashed
     *
     * @return mixed
     */
    public function getForDataTable($status = 1, $trashed = false)
    {
        $dataTableQuery = $this->getModel()
            ->branch()
            ->select([
                'id',
                'sl',
                'branch_id',
                'branch_name',
                'trans_date'
            ])
//            ->orderByDesc('id')
            ->with([
                'details' => function ($q) {
                    return $q->select(['trans_id', 'trans_sl', 'amount']);
                }
            ]);

        return $dataTableQuery;
    }

    /**
     * @param  array  $input
     * @return bool
     * @throws Throwable
     */
    public function create(array $input)
    {
        $data = Arr::get($input, 'data');

        $model = $this->getModel();

        $carbon = new Carbon();
        $transDate = $carbon->copy()->createFromFormat('d-M-Y', Arr::get($data, 'trans_date'));

        $model->branch_id = Arr::get($data, 'branch_id');
        $model->branch_name = Arr::get($data, 'branch_name');

        $branch = new BranchRepository();
        $branch = $branch->find($data['branch_id']);

//        $productRepository = new ProductRepository();
//        $bankRepository = new BankRepository();

        $sl = $branch->short_name;

        $sl .= '-';
        $sl .= $transDate->year;
        $sl .= $transDate->format('m');
        $sl .= $transDate->day;
        $sl .= '-';
        $sl .= 4275 + $this->query()->where('branch_id', $model->branch_id)->count() + 1;
//        $sl .= $this->getCount() + 1;

        $model->sl = $sl;

        $model->trans_date = $transDate->toDateString();
        $model->year = $transDate->year;
        $model->month = $transDate->copy()->month;

        $model->created_at = $carbon->toDateTimeString();
        $model->created_by = Auth::user()->id;
        $model->status = 1;

        try {
            return DB::transaction(function () use ($model, $data) {
                $alreadyExists = $model
                    ->where('branch_id', $model->branch_id)
                    ->where('trans_date', $model->trans_date)
                    ->where('month', $model->month)
                    ->where('year', $model->year)
                    ->exists();

                if ($alreadyExists === false) {
                    if ($model->save()) {
                        $transactions = [];

                        foreach (Arr::get($data, 'data') as $array) {
                            if ($array !== null) {
                                foreach ($array as $values) {
                                    $transactions = Arr::prepend($transactions, [
                                        'trans_id' => $model->id,
                                        'trans_sl' => $model->sl,
                                        'trans_date' => $model->trans_date,

                                        'branch_id' => $model->branch_id,
                                        'branch_name' => $model->branch_name,

                                        'product_id' => Arr::get($values, 'product_id'),
                                        'product_name' => Arr::get($values, 'product_name'),
                                        'brand_id' => Arr::get($values, 'brand_id'),
                                        'brand_name' => Arr::get($values, 'brand_name'),

                                        'bank_id' => Arr::get($values, 'bank_id'),
                                        'bank_name' => Arr::get($values, 'bank_name'),

                                        'amount' => Arr::get($values, 'amount', 0),

                                        'status' => $model->status,
                                        'year' => $model->year,
                                        'month' => $model->month,

                                        'created_by' => Auth::user()->id,
                                        'created_at' => Carbon::now()->toDateTimeString(),
                                    ]);
                                }
                            }
                        }

                        $model->details()->insert($transactions);

                        Log::alert('Store : Date Wise Summary Receipt, SL : '.$model->sl.' By User : '.Auth::user()->name);

//                        activity()
//                            ->performedOn($model)
//                            ->causedBy(\Auth::user()->id)
//                            ->withProperties([
//                                'serial' => $model->sl,
//                                'user' => Auth::user()->name
//                            ])
//                            ->log('Store : Date Wise Summary Receipt, SL : ' .  . ' By User : ' . );
                        return true;
                    }
                    throw new GeneralException('There was a problem creating this record. Please try again.');
                }
                throw new GeneralException('Records already exists!');
            });
        } catch (Throwable $e) {
            throw $e;
        }

    }

    /**
     * @param  array  $sl
     * @return
     * @throws GeneralException
     */
    public
    function show(
        $sl
    ) {
        $data = $this->whereSl($sl)->first();
        $detailRep = new DateWiseDetailsRepository();
        $data->details = $detailRep->where('trans_id', $data->id)->where('trans_sl', $data->sl)->get();

        return $data;
    }

    /**
     * @param  Model  $model
     * @param  array  $input
     *
     * @return bool
     * @throws Throwable
     */
    public
    function update(
        Model $model,
        array $input
    ) {
        $data = Arr::get($input, 'data');

        try {
            return DB::transaction(function () use ($model, $data) {
                $transactions = [];

                $oldTransactionDetails = $model->load('details');

                foreach (Arr::get($data, 'data') as $array) {
                    if ($array !== null) {
                        foreach ($array as $values) {
                            //old values
                            $oldAmount = $oldTransactionDetails->details
                                ->where('product_id', Arr::get($values, 'product_id'))
                                ->where('brand_id', Arr::get($values, 'brand_id'))
                                ->where('bank_id', Arr::get($values, 'bank_id'))
                                ->sum('amount');

                            $transactions = Arr::prepend($transactions, [
                                'trans_id' => $model->id,
                                'trans_sl' => $model->sl,
                                'trans_date' => $model->trans_date,

                                'branch_id' => $model->branch_id,
                                'branch_name' => $model->branch_name,

                                'product_id' => Arr::get($values, 'product_id'),
                                'product_name' => Arr::get($values, 'product_name'),
                                'brand_id' => Arr::get($values, 'brand_id'),
                                'brand_name' => Arr::get($values, 'brand_name'),

                                'bank_id' => Arr::get($values, 'bank_id'),
                                'bank_name' => Arr::get($values, 'bank_name'),

                                'amount' => Arr::get($values, 'amount', 0) - $oldAmount,

                                'status' => $model->status,
                                'year' => $model->year,
                                'month' => $model->month,

                                'created_by' => $model->created_by,
                                'created_at' => $model->created_at,

                                'updated_by' => Auth::user()->id,
                                'updated_at' => Carbon::now()->toDateTimeString(),
                            ]);
                        }
                    }
                }

                if ($model->details()->insert($transactions)) {
                    Log::alert('Update : Bank Deposit, SL : '.$model->sl.' By User : '.Auth::user()->name);
                    return true;
                }

                throw new GeneralException(trans('There was a problem updating this record. Please try again.'));
            });
        } catch (Throwable $e) {
            throw $e;
        }
    }


    /**
     * @param  Model  $model
     * @return bool
     * @throws GeneralException
     */
    public
    function delete(
        Model $model
    ) {
        if ($model->delete()) {
            return true;
        }

        throw new GeneralException(trans('There was a problem deleting this record. Please try again.'));
    }

    /**
     * @param  Model  $model
     * @return mixed
     * @throws GeneralException
     * @throws Throwable
     */
    public
    function forceDelete(
        Model $model
    ) {
        if ($model->deleted_at === null) {
            throw new GeneralException(trans('This record must be deleted first before it can be destroyed permanently.!'));
        }

        try {
            return DB::transaction(function () use ($model) {
                if ($model->forceDelete()) {
                    return true;
                }

                throw new GeneralException(trans('There was a problem deleting this record. Please try again.'));
            });
        } catch (Throwable $e) {
            throw $e;
        }
    }

    /**
     * @param  Model  $model
     * @return bool
     * @throws GeneralException
     */
    public
    function restore(
        Model $model
    ) {
        if ($model->deleted_at === null) {
            throw new GeneralException(trans('This record is not deleted so it can not be restored.'));
        }

        if ($model->restore()) {
            return true;
        }

        throw new GeneralException(trans('There was a problem restoring this record. Please try again.'));
    }

    /**
     * @param  Model  $model
     * @param         $status
     *
     * @return bool
     * @throws GeneralException
     */
    public
    function mark(
        Model $model,
        $status
    ) {
        $model->status = $status;

        switch ($status) {
            case 2:
                // event(new UserDeactivated($user));
                break;

            case 1:
                //event(new UserReactivated($user));
                break;
        }

        if ($model->save()) {
            return true;
        }

        throw new GeneralException(trans('There was a problem updating this record. Please try again.'));
    }

    /**
     * @param  int   $status
     * @param  bool  $trashed
     *
     * @return mixed
     */
    public
    function getDeactivated(
        $status = 2,
        $trashed = false
    ) {
        /**
         * Note: You must return deleted_at or the User getActionButtonsAttribute won't
         * be able to differentiate what buttons to show for each row.
         */

        if ($trashed == 'true') {
            return $this->onlyTrashed();
        }

        // active() is a scope on the UserScope trait
        return $this->active($status)->get();
    }


    /**
     * @param  bool  $status
     * @param  bool  $trashed
     *
     * @return mixed
     */
    public
    function getDeleted(
        $status = false,
        $trashed = true
    ) {
        /**
         * Note: You must return deleted_at or the User getActionButtonsAttribute won't
         * be able to differentiate what buttons to show for each row.
         */

        if ($trashed == 'true') {
            return $this->onlyTrashed()->get();
        }

        // active() is a scope on the UserScope trait
        return $this->active($status)->get();
    }

}
