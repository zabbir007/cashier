<?php

namespace App\Repositories\Backend\Transaction\ImprestBankReconciliation;

use App\Exceptions\GeneralException;
use App\Models\ImprestBankReconciliation\ImprestBankReconciliationSummary;
use App\Repositories\Backend\Transaction\OutstandingReconciliation\OutstandingReconciliationDetailsRepository;
use App\Repositories\BaseRepository;
use Auth;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\DB;
use Log;
use Throwable;


/**
 * Class ImprestBankReconciliationSummaryRepository
 *
 * @package App\Repositories\Backend\Transaction\ImprestBankReconciliation
 */
final class ImprestBankReconciliationSummaryRepository extends BaseRepository
{
    /**
     * @return string
     */
    public function model()
    {
        return ImprestBankReconciliationSummary::class;
    }

    /**
     * @param  string  $order_by
     * @param  string  $sort
     *
     * @return mixed
     */
    public function getAll($order_by = 'sl', $sort = 'asc')
    {
        return $this
            ->orderBy($order_by, $sort)
            ->get();
    }

    /**
     * @param  int   $status
     * @param  bool  $trashed
     *
     * @return mixed
     */
    public function getForDataTable($status = 1, $trashed = false)
    {
        return $this->getModel()
            ->select([
                'id',
                'sl',
                'branch_id',
                'bank_id',
                'account_number',
                'opening_balance',
                'closing_per_bank',
                'closing_per_bank_book',
                'trans_date',
                'month',
                'year',
                'approve'
            ])
            ->branch()
            ->with([
                'branch' => function ($q) {
                    return $q->select(['id', 'name']);
                },
                'bank' => function ($q) {
                    return $q->select(['id', 'name']);
                },
                'details' => function ($q) {
                    return $q->select(['trans_id', 'trans_sl']);
                }
            ]);
    }

    public function checkPreviousEntry($data)
    {
        return $this->getModel()
            ->where('branch_id', $data['branch'])
            ->where('bank_id', $data['bank'])
            ->where('year', $data['year'])
            ->where('month', $data['month'])
            ->count();

    }

    /**
     * @param  array  $input
     * @return bool
     * @throws Throwable
     */
    public function create(array $input)
    {
        $data = Arr::get($input, 'data');
        // dd($data);
        $model = $this->getModel();

        //  $transDate = $carbon->copy()->createFromFormat('d-M-Y', Arr::get($data, 'trans_date'));
        $transDate = Carbon::now();

        $model->branch_id = Arr::get($data, 'branch_id');
        $model->branch_code = Arr::get($data, 'branch_code');
        $model->bank_id = Arr::get($data, 'bank_id');
        $model->account_number = Arr::get($data, 'account_number');

        //opening balance
        $model->opening_balance = Arr::get($data, 'opening_balance', 0);
        //bank interest
        $model->bank_interest = Arr::get($data, 'bank_interest', 0);
        $model->bank_interest = $model->bank_interest ?? 0;

        //bank charge
        $model->bank_charge = Arr::get($data, 'bank_charge', 0);
        $model->bank_charge = $model->bank_charge ?? 0;

        //Closing balance as per bank statement (B)
        $model->closing_balance = Arr::get($data, 'bank_statement_closing_balance', 0);
        $model->closing_balance = $model->closing_balance === null ? 0 : $model->closing_balance;

        //Closing balance as per bank
        $model->closing_per_bank = Arr::get($data, 'bank_statement_closing_balance', 0);
        $model->closing_per_bank = $model->closing_per_bank === null ? 0 : $model->closing_per_bank;

        //Closing balance as per bank book
        $model->closing_per_bank_book = Arr::get($data, 'closing_balance_book', 0);

        $model->preparation_date = Carbon::now()->toDateString();

        $sl = Arr::get($data, 'branch_short_name');

        $sl .= '-';
        $sl .= $transDate->year;
        $sl .= $transDate->format('m');
        $sl .= $transDate->day;
        $sl .= '-';
        $sl .= $this->query()->where('branch_id', $model->branch_id)->count() + 1;

        $model->sl = $sl;

        $model->trans_date = $transDate->toDateString();
        $model->year = Arr::get($data, 'year');
        $model->month = Arr::get($data, 'month');
        $model->yearmonth = $model->year.$model->month;

        $model->created_at = $transDate->toDateTimeString();
        $model->created_by = Auth::user()->id;
        $model->status = 1;

        try {
            return DB::transaction(function () use ($model, $data) {
                $alreadyExists = $model
                    ->where('branch_id', $model->branch_id)
                    ->where('trans_date', $model->trans_date)
                    ->where('month', $model->month)
                    ->where('year', $model->year)
                    ->exists();

                if ($alreadyExists == false) {

                    if ($model->save()) {

                        //first extract bank_book_deposited array
                        $bank_book_deposited = Arr::get($data, 'bank_book_deposited');

                        //second extract bank_book_withdrawn
                        $bank_book_withdrawn = Arr::get($data, 'bank_book_withdrawn');

                        //third extract cheque_issued
                        $cheque_issued = Arr::get($data, 'cheque_issued');

                        //fourth extract deposited_received_from_corporate
                        $deposited_received_from_corporates = Arr::get($data, 'deposited_received_from_corporate');

                        //fifth extract less_check_issued
                        $less_issued_checks = Arr::get($data, 'less_check_issued');

                        //sixth extract less_check_issued
                        $less_issued_cq_checks = Arr::get($data, 'less_check_issuedCQ');

                        $transactions = [];

                        for ($i = 1; $i <= 5; $i++) {
                            $items = $bank_book_deposited[$i];
                            foreach ($items as $item) {
                                $transaction = [
                                    'trans_id' => $model->id,
                                    'trans_sl' => $model->sl,
                                    'trans_date' => $model->trans_date,
                                    'branch_id' => $model->branch_id,
                                    'branch_code' => $model->branch_code,
                                    'bank_id' => $model->bank_id,
                                    'account_number' => $model->account_number,

                                    'entry_date' => Arr::get($item,
                                        'date') === null ? null : Carbon::createFromFormat('d-M-Y',
                                        Arr::get($item, 'date'))->toDateString(),
                                    'amount' => Arr::get($item, 'amount') ?? 0,
                                    'types' => 1,
                                    'ref_no' => Arr::get($item, 'ref_no'),

                                    'status' => $model->status,
                                    'year' => $model->year,
                                    'month' => $model->month,
                                    'yearmonth' => $model->yearmonth,
                                    'created_by' => $model->created_by,
                                    'created_at' => $model->created_at,
                                ];
                                $transactions = Arr::prepend($transactions, $transaction);
                            }

                            $items = $bank_book_withdrawn[$i];
                            foreach ($items as $item) {
                                $transaction = [
                                    'trans_id' => $model->id,
                                    'trans_sl' => $model->sl,
                                    'trans_date' => $model->trans_date,
                                    'branch_id' => $model->branch_id,
                                    'branch_code' => $model->branch_code,
                                    'bank_id' => $model->bank_id,
                                    'account_number' => $model->account_number,

                                    'entry_date' => Arr::get($item,
                                        'date') === null ? null : Carbon::createFromFormat('d-M-Y',
                                        Arr::get($item, 'date'))->toDateString(),
                                    'amount' => Arr::get($item, 'amount') ?? 0,
                                    'types' => 2,
                                    'ref_no' => Arr::get($item, 'ref_no'),

                                    'status' => $model->status,
                                    'year' => $model->year,
                                    'month' => $model->month,
                                    'yearmonth' => $model->yearmonth,
                                    'created_by' => $model->created_by,
                                    'created_at' => $model->created_at,
                                ];
                                $transactions = Arr::prepend($transactions, $transaction);
                            }

                            $items = $cheque_issued[$i];
                            foreach ($items as $item) {
                                $transaction = [
                                    'trans_id' => $model->id,
                                    'trans_sl' => $model->sl,
                                    'trans_date' => $model->trans_date,
                                    'branch_id' => $model->branch_id,
                                    'branch_code' => $model->branch_code,
                                    'bank_id' => $model->bank_id,
                                    'account_number' => $model->account_number,

                                    'entry_date' => Arr::get($item,
                                        'date') === null ? null : Carbon::createFromFormat('d-M-Y',
                                        Arr::get($item, 'date'))->toDateString(),
                                    'amount' => Arr::get($item, 'amount') ?? 0,
                                    'types' => 3,
                                    'ref_no' => Arr::get($item, 'ref_no'),

                                    'status' => $model->status,
                                    'year' => $model->year,
                                    'month' => $model->month,
                                    'yearmonth' => $model->yearmonth,
                                    'created_by' => $model->created_by,
                                    'created_at' => $model->created_at,
                                ];
                                $transactions = Arr::prepend($transactions, $transaction);
                            }
                        }

                        foreach ($deposited_received_from_corporates as $items) {
                            foreach ($items as $item) {
                                $transaction = [
                                    'trans_id' => $model->id,
                                    'trans_sl' => $model->sl,
                                    'trans_date' => $model->trans_date,
                                    'branch_id' => $model->branch_id,
                                    'branch_code' => $model->branch_code,
                                    'bank_id' => $model->bank_id,
                                    'account_number' => $model->account_number,

                                    'entry_date' => Arr::get($item,
                                        'date') === null ? null : Carbon::createFromFormat('d-M-Y',
                                        Arr::get($item, 'date'))->toDateString(),
                                    'amount' => Arr::get($item, 'amount') ?? 0,
                                    'types' => 4,
                                    'ref_no' => Arr::get($item, 'ref_no'),

                                    'status' => $model->status,
                                    'year' => $model->year,
                                    'month' => $model->month,
                                    'yearmonth' => $model->yearmonth,
                                    'created_by' => $model->created_by,
                                    'created_at' => $model->created_at,
                                ];
                                $transactions = Arr::prepend($transactions, $transaction);
                            }
                        }

                        foreach ($less_issued_checks as $items) {
                            foreach ($items as $item) {
                                $transaction = [
                                    'trans_id' => $model->id,
                                    'trans_sl' => $model->sl,
                                    'trans_date' => $model->trans_date,
                                    'branch_id' => $model->branch_id,
                                    'branch_code' => $model->branch_code,
                                    'bank_id' => $model->bank_id,
                                    'account_number' => $model->account_number,

                                    'entry_date' => Arr::get($item,
                                        'date') === null ? null : Carbon::createFromFormat('d-M-Y',
                                        Arr::get($item, 'date'))->toDateString(),
                                    'amount' => Arr::get($item, 'amount') ?? 0,
                                    'types' => 5,
                                    'ref_no' => Arr::get($item, 'ref_no'),

                                    'status' => $model->status,
                                    'year' => $model->year,
                                    'month' => $model->month,
                                    'yearmonth' => $model->yearmonth,
                                    'created_by' => $model->created_by,
                                    'created_at' => $model->created_at,
                                ];
                                $transactions = Arr::prepend($transactions, $transaction);
                            }
                        }

                        foreach ($less_issued_cq_checks as $items) {
                            foreach ($items as $item) {
                                $transaction = [
                                    'trans_id' => $model->id,
                                    'trans_sl' => $model->sl,
                                    'trans_date' => $model->trans_date,
                                    'branch_id' => $model->branch_id,
                                    'branch_code' => $model->branch_code,
                                    'bank_id' => $model->bank_id,
                                    'account_number' => $model->account_number,

                                    'entry_date' => Arr::get($item,
                                        'date') === null ? null : Carbon::createFromFormat('d-M-Y',
                                        Arr::get($item, 'date'))->toDateString(),
                                    'amount' => Arr::get($item, 'amount') ?? 0,
                                    'types' => 6,
                                    'ref_no' => Arr::get($item, 'ref_no'),

                                    'status' => $model->status,
                                    'year' => $model->year,
                                    'month' => $model->month,
                                    'yearmonth' => $model->yearmonth,
                                    'created_by' => $model->created_by,
                                    'created_at' => $model->created_at,
                                ];
                                $transactions = Arr::prepend($transactions, $transaction);
                            }
                        }

                        if ($model->details()->insert($transactions)) {
                            Log::alert('Store : Outstanding Reconciliation, SL : '.$model->sl.' By User : '.Auth::user()->name);
                        }

                        return true;
                    }
                    throw new GeneralException('There was a problem creating this record. Please try again.');
                }

                throw new GeneralException('Records already exists!');
            });
        } catch (Throwable $e) {
            throw  $e;
        }
    }

    /**
     * @param  array  $sl
     * @return
     */
    public function show($sl)
    {
        $data = $this->whereSl($sl)->first();
        $data->details = app(OutstandingReconciliationDetailsRepository::class)->where('trans_id',
            $data->id)->where('trans_sl', $data->sl)->get();

        return $data;
    }

    /**
     * @param         $sl
     * @param  array  $input
     *
     * @return bool
     * @throws Throwable
     */
    public function update($sl, array $input)
    {
        $data = Arr::get($input, 'data');

        $model = $this->getById($sl);

        try {
            return DB::transaction(function () use ($model, $data) {

                $model->branch_id = Arr::get($data, 'branch_id');
                $model->branch_code = Arr::get($data, 'branch_code');
                $model->bank_id = Arr::get($data, 'bank_id');
                $model->account_number = Arr::get($data, 'account_number');

                //opening balance
                $model->opening_balance = Arr::get($data, 'opening_balance', 0);
                //bank interest
                $model->bank_interest = Arr::get($data, 'bank_interest', 0);
                $model->bank_interest = $model->bank_interest ?? 0;

                //bank charge
                $model->bank_charge = Arr::get($data, 'bank_charge', 0);
                $model->bank_charge = $model->bank_charge ?? 0;

                //Closing balance as per bank statement (B)
                $model->closing_balance = Arr::get($data, 'bank_statement_closing_balance', 0);
                //Closing balance as per bank
                $model->closing_per_bank = Arr::get($data, 'bank_statement_closing_balance', 0);
                //Closing balance as per bank book
                $model->closing_per_bank_book = Arr::get($data, 'closing_balance_book', 0);

                if ($model->save()) {

                    //first extract bank_book_deposited array
                    $bank_book_deposited = Arr::get($data, 'bank_book_deposited');

                    //second extract bank_book_withdrawn
                    $bank_book_withdrawn = Arr::get($data, 'bank_book_withdrawn');

                    //third extract cheque_issued
                    $cheque_issued = Arr::get($data, 'cheque_issued');

                    //fourth extract less_check_issued
                    $deposited_received_from_corporates = Arr::get($data, 'deposited_received_from_corporate');

                    //fifth extract less_check_issued
                    $less_issued_checks = Arr::get($data, 'less_check_issued');

                    //sixth extract less_check_issued
                    $less_issued_cq_checks = Arr::get($data, 'less_check_issuedCQ');

                    $transactions = [];

                    foreach ($bank_book_deposited as $items) {
                        foreach ($items as $item) {
                            $entry_date = $item['date'] === null ? null : Carbon::parse($item['date'])->toDateString();
                            try {
                                $oldTrans = app(ImprestBankReconciliationDetailsRepository::class)
                                    ->where('id', $item['line_number'])
                                    ->where('trans_id', $model->id)
                                    ->where('types', 1)
                                    ->first();

                                $oldTrans = $oldTrans->fill([
                                    'entry_date' => $entry_date,
                                    'amount' => $item['amount'],
                                    'ref_no' => $item['ref_no'],
                                ]);
                                $oldTrans->save();
                            } catch (ModelNotFoundException $exception) {
                                $transaction = [
                                    'trans_id' => $model->id,
                                    'trans_sl' => $model->sl,
                                    'trans_date' => $model->trans_date,
                                    'branch_id' => $model->branch_id,
                                    'branch_code' => $model->branch_code,
                                    'bank_id' => $model->bank_id,
                                    'account_number' => $model->account_number,

                                    'entry_date' => $entry_date,
                                    'amount' => $item['amount'] ?? 0,
                                    'types' => 1,
                                    'ref_no' => $item['ref_no'],

                                    'status' => $model->status,
                                    'year' => $model->year,
                                    'month' => $model->month,
                                    'yearmonth' => $model->yearmonth,
                                    'created_by' => $model->created_by,
                                    'created_at' => $model->created_at,
                                ];
                                $transactions = Arr::prepend($transactions, $transaction);
                            }
                        }
                    }

                    foreach ($bank_book_withdrawn as $items) {
                        foreach ($items as $item) {
                            $entry_date = $item['date'] === null ? null : Carbon::parse($item['date'])->toDateString();
                            try {
                                $oldTrans = app(ImprestBankReconciliationDetailsRepository::class)
                                    ->where('id', $item['line_number'])
                                    ->where('trans_id', $model->id)
                                    ->where('types', 2)
                                    ->first();

                                $oldTrans = $oldTrans->fill([
                                    'entry_date' => $entry_date,
                                    'amount' => $item['amount'],
                                    'ref_no' => $item['ref_no'],
                                ]);
                                $oldTrans->save();
                            } catch (ModelNotFoundException $exception) {
                                $transaction = [
                                    'trans_id' => $model->id,
                                    'trans_sl' => $model->sl,
                                    'trans_date' => $model->trans_date,
                                    'branch_id' => $model->branch_id,
                                    'branch_code' => $model->branch_code,
                                    'bank_id' => $model->bank_id,
                                    'account_number' => $model->account_number,

                                    'entry_date' => $item['date'],
                                    'amount' => $item['amount'] ?? 0,
                                    'types' => 2,
                                    'ref_no' => $item['ref_no'],

                                    'status' => $model->status,
                                    'year' => $model->year,
                                    'month' => $model->month,
                                    'yearmonth' => $model->yearmonth,
                                    'created_by' => $model->created_by,
                                    'created_at' => $model->created_at,
                                ];
                                $transactions = Arr::prepend($transactions, $transaction);
                            }
                        }
                    }

                    foreach ($cheque_issued as $items) {
                        foreach ($items as $item) {
                            $entry_date = $item['date'] === null ? null : Carbon::parse($item['date'])->toDateString();
                            try {
                                $oldTrans = app(ImprestBankReconciliationDetailsRepository::class)
                                    ->where('id', $item['line_number'])
                                    ->where('trans_id', $model->id)
                                    ->where('types', 3)
                                    ->first();

                                $oldTrans = $oldTrans->fill([
                                    'entry_date' => $entry_date,
                                    'amount' => $item['amount'],
                                    'ref_no' => $item['ref_no'],
                                ]);
                                $oldTrans->save();
                            } catch (ModelNotFoundException $exception) {
                                $transaction = [
                                    'trans_id' => $model->id,
                                    'trans_sl' => $model->sl,
                                    'trans_date' => $model->trans_date,
                                    'branch_id' => $model->branch_id,
                                    'branch_code' => $model->branch_code,
                                    'bank_id' => $model->bank_id,
                                    'account_number' => $model->account_number,

                                    'entry_date' => $entry_date,
                                    'amount' => $item['amount'] ?? 0,
                                    'types' => 3,
                                    'ref_no' => $item['ref_no'],

                                    'status' => $model->status,
                                    'year' => $model->year,
                                    'month' => $model->month,
                                    'yearmonth' => $model->yearmonth,
                                    'created_by' => $model->created_by,
                                    'created_at' => $model->created_at,
                                ];
                                $transactions = Arr::prepend($transactions, $transaction);
                            }
                        }
                    }

                    foreach ($deposited_received_from_corporates as $items) {
                        foreach ($items as $item) {
                            $entry_date = $item['date'] === null ? null : Carbon::parse($item['date'])->toDateString();
                            try {
                                $oldTrans = app(ImprestBankReconciliationDetailsRepository::class)
                                    ->where('id', $item['line_number'])
                                    ->where('trans_id', $model->id)
                                    ->where('types', 4)
                                    // ->where('entry_date', $entry_date)
                                    ->first();

                                $oldTrans = $oldTrans->fill([
                                    'entry_date' => $entry_date,
                                    'amount' => $item['amount'],
                                    'ref_no' => $item['ref_no'],
                                ]);
                                $oldTrans->save();
                            } catch (ModelNotFoundException $exception) {
                                $transaction = [
                                    'trans_id' => $model->id,
                                    'trans_sl' => $model->sl,
                                    'trans_date' => $model->trans_date,
                                    'branch_id' => $model->branch_id,
                                    'branch_code' => $model->branch_code,
                                    'bank_id' => $model->bank_id,
                                    'account_number' => $model->account_number,

                                    'entry_date' => $entry_date,
                                    'amount' => $item['amount'] ?? 0,
                                    'types' => 4,
                                    'ref_no' => $item['ref_no'],

                                    'status' => $model->status,
                                    'year' => $model->year,
                                    'month' => $model->month,
                                    'yearmonth' => $model->yearmonth,
                                    'created_by' => $model->created_by,
                                    'created_at' => $model->created_at,
                                ];
                                $transactions = Arr::prepend($transactions, $transaction);
                            }
                        }
                    }

                    foreach ($less_issued_checks as $items) {
                        foreach ($items as $item) {
                            $entry_date = $item['date'] === null ? null : Carbon::parse($item['date'])->toDateString();

                            try {
                                $oldTrans = app(ImprestBankReconciliationDetailsRepository::class)
                                    ->where('id', $item['line_number'])
                                    ->where('trans_id', $model->id)
                                    ->where('types', 5)
//                                    ->where('entry_date', $entry_date)
                                    ->first();

                                $oldTrans = $oldTrans->fill([
                                    'entry_date' => $entry_date,
                                    'amount' => $item['amount'],
                                    'ref_no' => $item['ref_no'],
                                ]);
                                $oldTrans->save();
                            } catch (ModelNotFoundException $exception) {
                                $transaction = [
                                    'trans_id' => $model->id,
                                    'trans_sl' => $model->sl,
                                    'trans_date' => $model->trans_date,
                                    'branch_id' => $model->branch_id,
                                    'branch_code' => $model->branch_code,
                                    'bank_id' => $model->bank_id,
                                    'account_number' => $model->account_number,

                                    'entry_date' => $entry_date,
                                    'amount' => $item['amount'] ?? 0,
                                    'types' => 5,
                                    'ref_no' => Arr::get($item, 'ref_no'),

                                    'status' => $model->status,
                                    'year' => $model->year,
                                    'month' => $model->month,
                                    'yearmonth' => $model->yearmonth,
                                    'created_by' => $model->created_by,
                                    'created_at' => $model->created_at,
                                ];
                                $transactions = Arr::prepend($transactions, $transaction);
                            }
                        }
                    }

                    foreach ($less_issued_cq_checks as $items) {
                        foreach ($items as $item) {
                            $entry_date = $item['date'] === null ? null : Carbon::parse($item['date'])->toDateString();

                            try {
                                $oldTrans = app(ImprestBankReconciliationDetailsRepository::class)
                                    ->where('id', $item['line_number'])
                                    ->where('trans_id', $model->id)
                                    ->where('types', 6)
                                    //   ->where('entry_date', $entry_date)
                                    ->first();

                                $oldTrans = $oldTrans->fill([
                                    'entry_date' => $entry_date,
                                    'amount' => $item['amount'],
                                    'ref_no' => $item['ref_no'],
                                ]);
                                $oldTrans->save();
                            } catch (ModelNotFoundException $exception) {
                                $transaction = [
                                    'trans_id' => $model->id,
                                    'trans_sl' => $model->sl,
                                    'trans_date' => $model->trans_date,
                                    'branch_id' => $model->branch_id,
                                    'branch_code' => $model->branch_code,
                                    'bank_id' => $model->bank_id,
                                    'account_number' => $model->account_number,

                                    'entry_date' => $entry_date,
                                    'amount' => $item['amount'] ?? 0,
                                    'types' => 6,
                                    'ref_no' => $item['ref_no'],

                                    'status' => $model->status,
                                    'year' => $model->year,
                                    'month' => $model->month,
                                    'yearmonth' => $model->yearmonth,
                                    'created_by' => $model->created_by,
                                    'created_at' => $model->created_at,
                                ];
                                $transactions = Arr::prepend($transactions, $transaction);
                            }
                        }
                    }

                    if (count($transactions) > 0) {
                        if ($model->details()->insert($transactions)) {
                            Log::alert('Store : Outstanding Reconciliation, SL : '.$model->sl.' By User : '.Auth::user()->name);
                        }
                    }

//                    foreach ($transactions as $transaction){
//                        //get old transaction
//                        $oldTrns = $detailsRepository->where('trans_id', Arr::get($transaction,'trans_id'))
//                            ->where('trans_sl', Arr::get($transaction,'trans_sl'))
//                            ->where('trans_date', Arr::get($transaction,'trans_date'))
//                            ->where('branch_id', Arr::get($transaction,'branch_id'))
//                            ->where('branch_code', Arr::get($transaction,'branch_code'))
//                            ->where('bank_id', Arr::get($transaction,'bank_id'))
//                            ->where('account_number', Arr::get($transaction,'account_number'))
//                            ->where('entry_date', Arr::get($transaction,'entry_date'))
//                            ->where('types', Arr::get($transaction,'types'))
//                            ->get();
//
//                        dd($oldTrns);
//                    }
                    return true;
                }

                throw new GeneralException(trans('There was a problem updating this record. Please try again.'));
            });
        } catch (Throwable $e) {
            throw $e;
        }

    }


    /**
     * @param  Model  $model
     * @return bool
     * @throws GeneralException
     */
    public function delete(Model $model)
    {
        if ($model->delete()) {
            //event(new UserDeleted($user));

            return true;
        }

        throw new GeneralException(trans('There was a problem deleting this record. Please try again.'));
    }

    /**
     * @param  Model  $model
     * @return mixed
     * @throws GeneralException
     * @throws Throwable
     */
    public function forceDelete(Model $model)
    {
        if ($model->deleted_at === null) {
            throw new GeneralException(trans('This record must be deleted first before it can be destroyed permanently.!'));
        }

        return DB::transaction(function () use ($model) {
            if ($model->forceDelete()) {
                return true;
            }

            throw new GeneralException(trans('There was a problem deleting this record. Please try again.'));
        });
    }

    /**
     * @param  Model  $model
     * @return bool
     * @throws GeneralException
     */
    public function restore(Model $model)
    {
        if ($model->deleted_at === null) {
            throw new GeneralException(trans('This record is not deleted so it can not be restored.'));
        }

        if ($model->restore()) {
            //event(new UserRestored($user));

            return true;
        }

        throw new GeneralException(trans('There was a problem restoring this record. Please try again.'));
    }

    /**
     * @param  Model  $model
     * @param         $status
     *
     * @return bool
     * @throws GeneralException
     */
    public function mark(Model $model, $status)
    {
        $model->status = $status;

        switch ($status) {
            case 2:
                // event(new UserDeactivated($user));
                break;

            case 1:
                //event(new UserReactivated($user));
                break;
        }

        if ($model->save()) {
            return true;
        }

        throw new GeneralException(trans('There was a problem updating this record. Please try again.'));
    }

    /**
     * @param  int   $status
     * @param  bool  $trashed
     *
     * @return mixed
     */
    public function getDeactivated($status = 2, $trashed = false)
    {
        /**
         * Note: You must return deleted_at or the User getActionButtonsAttribute won't
         * be able to differentiate what buttons to show for each row.
         */
        $dataTableQuery = $this;

        if ($trashed == 'true') {
            return $dataTableQuery->onlyTrashed();
        }

        // active() is a scope on the UserScope trait
        return $dataTableQuery->active($status)->get();
    }


    /**
     * @param  bool  $status
     * @param  bool  $trashed
     *
     * @return mixed
     */
    public function getDeleted($status = false, $trashed = true)
    {
        /**
         * Note: You must return deleted_at or the User getActionButtonsAttribute won't
         * be able to differentiate what buttons to show for each row.
         */
        $dataTableQuery = $this;

        if ($trashed == 'true') {
            return $dataTableQuery->onlyTrashed()->get();
        }

        // active() is a scope on the UserScope trait
        return $dataTableQuery->active($status)->get();
    }

}
