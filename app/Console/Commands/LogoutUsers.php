<?php

namespace App\Console\Commands;

use App\Repositories\Backend\Access\User\UserRepository;
use Carbon\Carbon;
use Illuminate\Console\Command;

/**
 * Class LogoutUsers
 *
 * @package App\Console\Commands
 */
class LogoutUsers extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'users:logout';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Set all session out users to logout';


    /**
     * Execute the console command.
     *
     * @param  UserRepository  $userRepository
     * @return mixed
     */
    public function handle(UserRepository $userRepository)
    {
        $loggedUsers = $userRepository->getLoggedUsers();
        $logoutTime = Carbon::now()->addMinutes(\Config::get('session.lifetime'));

        foreach ($loggedUsers as $user) {
            if ($user->last_login_at->isBetween(Carbon::now(), $logoutTime) === false) {
                $user->fill([
                    'is_logged' => false
                ])->save();
            }
        }
    }
}
