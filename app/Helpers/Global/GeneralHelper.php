<?php

use App\Helpers\General\Timezone;

if (! function_exists('app_name')) {
    /**
     * Helper to grab the application name.
     *
     * @return mixed
     */
    function app_name()
    {
        return config('app.name');
    }
}

if (!function_exists('history')) {
    /**
     * Access the history facade anywhere.
     */
    function history()
    {
        return app('history');
    }
}

if (!function_exists('timezone')) {
    /**
     * Access the timezone helper.
     */
    function timezone()
    {
        return resolve(Timezone::class);
    }
}


if (!function_exists('encryptString')) {
    /**
     * @param $plaintext
     * @return string
     */
    function encryptString($plaintext)
    {
        # --- ENCRYPTION ---

        if (is_array($plaintext) || is_object($plaintext)) {
            $plaintext = json_encode($plaintext);
        }

        $key = pack('H*', "bcb04b7e103a0cd8b54763051cef08bc55abe029fdebae5e1d417e2ffb2a00a3");//change this

        # show key size use either 16, 24 or 32 byte keys for AES-128, 192
        # and 256 respectively
        $key_size = strlen($key);
        //echo "Key size: " . $key_size . "\n";

//
//        # create a random IV to use with CBC encoding
//        $iv_size = mcrypt_get_iv_size(MCRYPT_RIJNDAEL_128, MCRYPT_MODE_CBC);
//        $iv = mcrypt_create_iv($iv_size, MCRYPT_DEV_URANDOM);
//
//        # creates a cipher text compatible with AES (Rijndael block size = 128)
//        # to keep the text confidential
//        # only suitable for encoded input that never ends with value 00h
//        # (because of default zero padding)
//        $ciphertext = mcrypt_encrypt(MCRYPT_RIJNDAEL_128, $key,
//            $plaintext, MCRYPT_MODE_CBC, $iv);
//
//        # prepend the IV for it to be available for decryption
//        $ciphertext = $iv . $ciphertext;

        # encode the resulting cipher text so it can be represented by a string
        $ciphertext_base64 = base64_encode($plaintext);

        return rawurlencode($ciphertext_base64);//important rawurlencode for + symbol in url

    }
}

if (!function_exists('decryptString')) {
    /**
     * @param $ciphertext_base64
     * @return string
     */
    function decryptString($ciphertext_base64)
    {
        # --- DECRYPTION ---

        $key = pack('H*', "bcb04b7e103a0cd8b54763051cef08bc55abe029fdebae5e1d417e2ffb2a00a3");//change this

# show key size use either 16, 24 or 32 byte keys for AES-128, 192
# and 256 respectively
//        $key_size =  strlen($key);
////echo "Key size: " . $key_size . "\n";
//
//# create a random IV to use with CBC encoding
//        $iv_size = mcrypt_get_iv_size(MCRYPT_RIJNDAEL_128, MCRYPT_MODE_CBC);
//        $iv = mcrypt_create_iv($iv_size, MCRYPT_DEV_URANDOM);

        $plaintext_dec = base64_decode($ciphertext_base64);
//
//# retrieves the IV, iv_size should be created using mcrypt_get_iv_size()
//        $iv_dec = substr($ciphertext_dec, 0, $iv_size);
//
//# retrieves the cipher text (everything except the $iv_size in the front)
//        $ciphertext_dec = substr($ciphertext_dec, $iv_size);
//
//# may remove 00h valued characters from end of plain text
//        $plaintext_dec = mcrypt_decrypt(MCRYPT_RIJNDAEL_128, $key,
//            $ciphertext_dec, MCRYPT_MODE_CBC, $iv_dec);

        return rawurldecode($plaintext_dec);
    }
}

if (!function_exists('camelcase_to_word')) {

    /**
     * @param $str
     *
     * @return string
     */
    function camelcase_to_word($str)
    {
        return implode(' ', preg_split('/
          (?<=[a-z])
          (?=[A-Z])
        | (?<=[A-Z])
          (?=[A-Z][a-z])
        /x', $str));
    }
}

if (!function_exists('page_title')) {
    /**
     * @param  string  $parent
     * @param  string  $module
     * @param  string  $action
     * @param  bool    $plural
     * @return string
     */
    function page_title($parent, $module, $action, $plural = true)
    {
        return title_case(str_replace('_', ' ',
            $parent.' | '.Str::plural($module, $plural === true ? 1 : 2).' | '.$action));
    }
}

if (!function_exists('page_header')) {
    /**
     * @param  string       $noun
     * @param  string|null  $module
     * @return string
     */
    function page_header($noun, $module = null)
    {
        $module = $module !== null ? Str::contains('All', $noun) ? Str::plural($module) : Str::singular($module) : null;
        return title_case($noun.' '.str_replace('_', ' ', $module));
    }
}

if (!function_exists('is_countable')) {
    /**
     * @param $var
     * @return bool
     */
    function is_countable($var)
    {
        return (is_array($var) || $var instanceof Countable);
    }
}

if (!function_exists('carbon')) {
    /**
     * @param $time
     * @return Carbon
     */
    function carbon($time = null)
    {
        return Carbon::parse($time);
    }
}

if (!function_exists('now')) {
    /**
     * @return Carbon
     */
    function now()
    {
        return Carbon::now();
    }
}

if (!function_exists('_money_format')) {

    /**
     * @param          $number
     * @param  bool    $million
     * @param  int     $decimal
     * @param  string  $currency
     * @return string
     */
    function _money_format($number, $million = false, $decimal = 2, $currency = '৳')
    {
        return $currency.' '.number_format($million === true ? $number / 1000000 : $number, $decimal);
    }
}

if (!function_exists('_money_format_in_crore')) {

    /**
     * @param          $number
     * @param  int     $decimal
     * @param  string  $currency
     * @return string
     */
    function _money_format_in_crore($number, $decimal = 2, $currency = '৳')
    {
        return $currency.' '.number_format($number / 10000000, $decimal) . ' Crore';
    }
}


