<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Request;

/**
 * Class LogMiddleware
 *
 * @package App\Http\Middleware
 */
final class LogMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  Request  $request
     * @param  Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $response = $next($request);

//        \Log::debug('Dump request', [
//            'request' => serialize($request->all()),
//            'response' => serialize($response->toArray()),
//        ]);

        return $response;
    }


//    public function handle($request, Closure $next)
//    {
//        return $next($request);
//    }
}
