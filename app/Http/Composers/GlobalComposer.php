<?php

namespace App\Http\Composers;

use Illuminate\View\View;
use function request;

/**
 * Class GlobalComposer.
 */
final class GlobalComposer
{
    /**
     * Bind data to the view.
     *
     * @param  View  $view
     *
     * @return void
     */
    public function compose(View $view)
    {
        $view->with('logged_in_user', request()->user());
    }
}
