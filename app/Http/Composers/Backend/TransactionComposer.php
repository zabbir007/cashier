<?php

namespace App\Http\Composers\Backend;

use App\Exceptions\GeneralException;
use App\Repositories\Backend\System\TransactionSettingRepository;
use Illuminate\View\View;

/**
 * Class TransactionComposer
 *
 * @package App\Http\Composers\Backend
 */
final class TransactionComposer
{
    /**
     * @var TransactionSettingRepository
     */
    protected $settingRepository;

    /**
     * TransactionComposer constructor.
     *
     * @param  TransactionSettingRepository  $repository
     */
    public function __construct(TransactionSettingRepository $repository)
    {
        $this->settingRepository = $repository;
    }

    /**
     * @param  View  $view
     *
     * @return bool|mixed
     * @throws GeneralException
     */
    public function compose(View $view)
    {
        $enabledDates = $this->settingRepository->getPermittedDays();
        // now()->format('d-m-Y');

        $view->with('enable_dates', $enabledDates);
    }
}
