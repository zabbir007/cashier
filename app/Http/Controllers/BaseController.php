<?php

namespace App\Http\Controllers;

use App\Exceptions\GeneralException;
use App\Repositories\Backend\Setup\Branch\BranchRepository;
use Auth;
use DB;
use Illuminate\Contracts\Auth\Authenticatable;
use Illuminate\Support\Arr;
use Log;

/**
 * Class BaseController.
 */
class BaseController extends Controller
{
    /**
     * Module Repository
     *
     * @var
     */
    protected $repository;
    /**
     * Module Parent Name
     *
     * @var
     */
    protected $module_parent;
    /**
     * Module Name
     *
     * @var
     */
    protected $module_name;
    /**
     * Module Icon
     *
     * @var
     */
    protected $module_icon;
    /**
     * Module Action
     *
     * @var
     */
    protected $module_action;

    /**
     * Module Route
     *
     * @var
     */
    protected $module_route;
    /**
     * Module View
     *
     * @var
     */
    protected $module_view;
    /**
     * Module Page Heading
     *
     * @var
     */
    protected $page_heading;

    /**
     * Get Logged User Tagged Branch Codes
     *
     * @return mixed
     * @throws GeneralException
     */
    public function getBranchIds()
    {
        return $this->getBranches()->pluck('id');
    }

    /**
     * Get Logged User Tagged Branches
     *
     * @return mixed
     * @throws GeneralException
     */
    public function getBranches()
    {
        $branches = $this->getLoggedUser()->load('branches')->branches;
        if ($branches === null) {
            $branchRepository = new BranchRepository();
            $branches = $branchRepository->getAll();
        }

        return $branches;
    }

    /**
     * Get Logged User
     *
     * @return Authenticatable|null
     */
    public function getLoggedUser(): ?Authenticatable
    {
        return request()->user();
    }

    /**
     * Get Logged User Tagged Branch Codes
     *
     * @return mixed
     * @throws GeneralException
     */
    public function getBranchCodes()
    {
        return $this->getBranches()->pluck('code');
    }

    /**
     * Get Logged User Tagged First Branch Code
     *
     * @return mixed
     * @throws GeneralException
     */
    public function getBranchCode()
    {
        return Arr::first($this->getBranches()->pluck('code'));
    }

    /**
     * Get Logged User Tagged First Branch Code
     *
     * @return mixed
     * @throws GeneralException
     */
    public function getBranchId()
    {
        return Arr::first($this->getBranches()->pluck('id'));
    }

    /**
     * Get Logged User Tagged Branches
     *
     * @return mixed
     * @throws GeneralException
     */
    public function getBranch()
    {
        return $this->getBranches()->first();
    }

    /**
     *
     */
    public function onCreate()
    {
        Log::alert('Browse : '.ucfirst($this->module_parent).'  '.ucfirst($this->module_action).' By User : '.Auth::user()->name);
    }

    /**
     *
     */
    public function enableQueryLog(): void
    {
        DB::enableQueryLog();
    }

    /**
     *
     */
    public function disableQueryLog(): void
    {
        DB::disableQueryLog();
    }

    /**
     * @return array
     */
    public function getQueryLog(): array
    {
        return DB::getQueryLog();
    }
}
