<?php

namespace App\Http\Controllers\Backend\Transaction\DateWise;

use App\Http\Controllers\BaseController;
use App\Repositories\Backend\Transaction\DateWise\DateWiseSummaryRepository;
use Carbon\Carbon;
use Exception;
use Illuminate\Http\Request;
use Yajra\DataTables\Facades\DataTables;

/**
 * Class DateWiseTableController
 *
 * @package App\Http\Controllers\Backend\Transaction\DateWise
 */
final class DateWiseTableController extends BaseController
{
    /**
     * @param  DateWiseSummaryRepository  $repository
     */
    public function __construct(DateWiseSummaryRepository $repository)
    {
        $this->repository = $repository;
    }

    /**
     * @param  Request  $request
     *
     * @return mixed
     * @throws Exception
     */
    public function __invoke(Request $request)
    {
        return DataTables::of($this->repository->getForDataTable($request->get('status'), $request->get('trashed')))
            ->removeColumn('details')
            ->escapeColumns(['branch_name'])
            ->editColumn('sl', '<span style="font-weight: bold; color: #0d89ed;">{{ $sl }}</span>')
            ->editColumn('trans_date', function ($q) {
                return '<span style="font-weight: bold;">'.Carbon::parse($q->trans_date)->format('d-M-Y').'</span>';
            })
            ->addColumn('collection', function ($q) {
                return number_format($q->details->sum('collection'), 2);
            })
            ->addColumn('money_receipt', function ($q) {
                return number_format($q->details->sum('money_receipt'), 2);
            })
            ->addColumn('adv_receipt', function ($q) {
                return number_format($q->details->sum('adv_receipt'), 2);
            })
            ->addColumn('short_receipt', function ($q) {
                return number_format($q->details->sum('short_receipt'), 2);
            })
            ->addColumn('adv_adj', function ($q) {
                return number_format($q->details->sum('adv_adj'), 2);
            })
            ->addColumn('adj_plus', function ($q) {
                return number_format($q->details->sum('adj_plus'), 2);
            })
            ->addColumn('adj_minus', function ($q) {
                return number_format($q->details->sum('adj_minus'), 2);
            })
            ->addColumn('mr_reverse', function ($q) {
                return number_format($q->details->sum('mr_reverse'), 2);
            })
            ->addColumn('actions', function ($q) {
                return $q->action_buttons;
            })
            ->setRowClass(function ($q) {
                return $q->isApprove() ? 'bg-success-light' : '';
            })
            ->order(function ($query) {
                $query->orderBy('trans_date', 'desc');
                $query->orderBy('sl', 'asc');
            })
            //   ->rawColumns(['sl','trans_date'])
            ->make(true);
    }
}
