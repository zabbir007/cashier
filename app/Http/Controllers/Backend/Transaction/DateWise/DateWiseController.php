<?php

namespace App\Http\Controllers\Backend\Transaction\DateWise;


use App\Exceptions\GeneralException;
use App\Http\Controllers\BaseController;
use App\Models\DateWise\DateWiseSummary;
use App\Repositories\Backend\Setup\Branch\BranchRepository;
use App\Repositories\Backend\Setup\Brand\BrandRepository;
use App\Repositories\Backend\Setup\Product\ProductRepository;
use App\Repositories\Backend\Transaction\DateWise\DateWiseSummaryRepository;
use Auth;
use Carbon\Carbon;
use Illuminate\Contracts\View\Factory;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\View\View;
use Log;
use Throwable;


/**
 * Class DateWiseController
 *
 * @package App\Http\Controllers\Backend\Transaction\DateWise
 */
final class DateWiseController extends BaseController
{
    /**
     * @var ProductRepository
     */
    /**
     * @var BrandRepository|ProductRepository
     */
    /**
     * @var BrandRepository|ProductRepository
     */
    /**
     * @var BranchRepository|BrandRepository|ProductRepository
     */
    private $productRepository, $brandRepository;

    /**
     * DateWiseController constructor.
     *
     * @param  DateWiseSummaryRepository  $repository
     * @param  ProductRepository          $productRepository
     * @param  BrandRepository            $brandRepository
     */
    public function __construct(
        DateWiseSummaryRepository $repository,
        ProductRepository $productRepository,
        BrandRepository $brandRepository
    ) {
        $this->repository = $repository;
        $this->productRepository = $productRepository;
        $this->brandRepository = $brandRepository;

        $this->module_parent = 'Transaction:: Date Wise Receipts';
        $this->module_name = 'datewises';
        $this->module_icon = 'mdi-hospital-building';
        $this->module_route = 'admin.transaction.'.$this->module_name;
        $this->module_view = 'backend.transaction.'.$this->module_name;

    }

    /**
     * @return Factory|View
     */
    public function index()
    {
        $module_parent = $this->module_parent;
        $module_name = $this->module_name;
        $module_name_singular = str_singular($this->module_name);
        $module_icon = $this->module_icon;
        $module_action = 'Index';
        $module_route = $this->module_route;
        $module_view = $this->module_view;

        $page_heading = $this->module_parent.' - View All';

        Log::alert('Browse : '.ucfirst($module_parent).'  '.ucfirst($module_action).' By User : '.Auth::user()->name);
        return view($module_view.'.index',
            compact('module_name', $module_name, 'module_name_singular', 'module_parent', 'module_icon', 'page_heading',
                'module_action', 'module_view', 'module_route'));
    }

    /**
     * @return $this
     * @throws GeneralException
     */
    public function create()
    {
        $module_parent = $this->module_parent;
        $module_name = $this->module_name;
        $module_name_singular = str_singular($this->module_name);
        $module_icon = $this->module_icon;
        $module_action = 'Create';
        $module_route = $this->module_route;
        $module_view = $this->module_view;

        $page_heading = $this->module_parent.' - Add New';

        $brands = $this->brandRepository->getAll();

        $carbon = Carbon::now();
        $branch = $this->getBranch();

        $branchProduct = $branch->products->pluck('id');

        $product_all = [];
        foreach ($brands as $brand) {
            $products = $this->productRepository->getAll()->where('brand_id', $brand->id)->whereIn('id',
                $branchProduct);

            $table = [
                'brand_id' => $brand->id,
                'brand_name' => $brand->name,
                'all' => $products,
            ];
            $product_all[] = $table;
        }

        Log::alert('Create : '.ucfirst($module_parent).'  '.ucfirst($module_action).' By User : '.Auth::user()->name);

        return view($module_view.'.create',
            compact('module_name', 'module_name_singular', 'module_parent', 'module_icon', 'brand',
                'page_heading', 'module_action', 'module_view', 'module_route'))
            ->with('carbon', $carbon)
            ->with('products', $product_all)
            ->with('branches', $branch);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  Request  $request
     * @return Response
     * @throws GeneralException
     * @throws Throwable
     */
    public function store(Request $request)
    {
        $module_route = $this->module_route;

        $this->repository->create([
            'data' => $request->except(['_token']),
        ]);

        return redirect()->route($module_route.'.index')->withFlashSuccess('Record successfully saved');
    }

    /**
     * Display the specified resource.
     *
     * @param $datewise
     * @return Response
     * @throws GeneralException
     * @internal param int $id
     */
    public function show($datewise)
    {
        $module_parent = $this->module_parent;
        $module_name = $this->module_name;
        $module_icon = $this->module_icon;
        $module_action = 'Details';
        $module_route = $this->module_route;
        $module_view = $this->module_view;

        $page_heading = $this->module_parent.' - View';

        $brands = $this->brandRepository->getAll();
        $carbon = Carbon::now();
        $branch = $this->getBranch();

        $branchProduct = $branch->products->pluck('id');

        $product_all = [];
        foreach ($brands as $brand) {
            $products = $this->productRepository->getAll()->where('brand_id', $brand->id)->whereIn('id',
                $branchProduct);

            $table = [
                'brand_id' => $brand->id,
                'brand_name' => $brand->name,
                'all' => $products,
            ];
            $product_all[] = $table;
        }

        $module_name_singular = $this->repository->getModel()->where('id', $datewise)->with('details')->first();


        return view($module_view.'.show',
            compact('module_name', 'module_name_singular', 'module_parent', 'module_icon', 'page_heading',
                'module_action', 'module_view', 'module_route'))
            ->with('partial', 'overview')
            ->with('carbon', $carbon)
            ->with('products', $product_all)
            ->with('branches', $branch);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param $datewise
     * @return Response
     * @throws GeneralException
     */
    public function edit($datewise)
    {
        $module_parent = $this->module_parent;
        $module_name = $this->module_name;
        $module_icon = $this->module_icon;
        $module_action = 'Save';
        $module_route = $this->module_route;
        $module_view = $this->module_view;

        $page_heading = $this->module_parent.' - Edit';

        $brands = $this->brandRepository->getAll();
        $branch = $this->getBranch();
        $branchProduct = $branch->products->pluck('id');

        $product_all = [];
        foreach ($brands as $brand) {
            $products = $this->productRepository->getAll()->where('brand_id', $brand->id)->whereIn('id',
                $branchProduct);

            $table = [
                'brand_id' => $brand->id,
                'brand_name' => $brand->name,
                'all' => $products,
            ];
            $product_all[] = $table;
        }

        $module_name_singular = $this->repository->getModel()->where('id', $datewise)->with('details')->first();

        Log::alert('Edit : '.ucfirst($module_parent).'  '.ucfirst($module_action).' By User : '.Auth::user()->name);
        return view($module_view.'.edit',
            compact('module_name', 'module_name_singular', 'module_parent', 'module_icon', 'brand',
                'page_heading', 'module_action', 'module_view', 'module_route'))
            ->with('carbon', Carbon::now())
            ->with('now', Carbon::now()->format('d-M-Y'))
            ->with('products', $product_all)
            ->with('branches', $branch);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  DateWiseSummary  $datewise
     * @param  Request          $request
     * @return Response
     * @throws Throwable
     */
    public function update(DateWiseSummary $datewise, Request $request)
    {
        $module_route = $this->module_route;

        $this->repository->update($datewise,
            [
                'data' => $request->except(
                    '_token',
                    '_method'
                ),
            ]);

        return redirect()->route($module_route.'.index')->withFlashSuccess(trans('Record successfully updated'));
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  Request                   $request
     * @param                            $datewise
     * @return Response
     */
    public function approve(Request $request, $datewise)
    {
        $module_name_singular = str_singular($this->module_name);

        $$module_name_singular = DateWiseSummary::findOrFail($datewise);

        $$module_name_singular->update(['approve' => '1', 'approve_by' => access()->user()->id]);

//        Log::info(ucfirst($module_action) . " '$module_name': '" . $$module_name_singular->name . ", ID:" . $$module_name_singular->id . " ' by User:" . access()->user()->name);
        return redirect()->back()->with('flash_success',
            '<i class="fa fa-check"></i> '.ucfirst($module_name_singular).' Approved successfully!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  DateWiseSummaryRepository  $datewise
     * @return Response
     * @throws GeneralException
     */
    public function destroy(DateWiseSummaryRepository $datewise)
    {
        $module_name_singular = str_singular($this->module_name);
        $module_route = $this->module_route;

        $$module_name_singular = $this->repository->delete($datewise);

        return redirect()->route($module_route.'.deleted')->withFlashSuccess(trans('Record successfully deleted'));
    }

    /**
     * @param $datewise
     * @return mixed
     * @throws GeneralException
     */
    public function restore($datewise)
    {
        $module_route = $this->module_route;

        $module_name = DateWiseSummaryRepository::withTrashed()->find($datewise);
        $this->repository->restore($module_name);

        return redirect()->route($module_route.'.index')->withFlashSuccess(trans('Record successfully restored'));
    }

    /**
     * @param $datewise
     * @return mixed
     * @throws GeneralException
     * @throws Throwable
     */
    public function delete($datewise)
    {
        $module_route = $this->module_route;

        $module_name = DateWiseSummaryRepository::withTrashed()->find($datewise);
        $this->repository->forceDelete($module_name);

        return redirect()->route($module_route.'.deleted')->withFlashSuccess(trans('Records deleted permanently'));
    }

//    public function fix(){
//        $nullValues = $this->repository
//        ->whereHas('details', function($q){
//            return $q->whereNull('trans_date');
//        })->get();
//
//        foreach ($nullValues as $nullValue){
//            $nullValue->details()->update([
//                'trans_date' => $nullValue->trans_date
//            ]);
//        }
//    }
}
