<?php

namespace App\Http\Controllers\Backend\Transaction\PettyCash;

use App\Http\Controllers\BaseController;
use App\Repositories\Backend\Transaction\PettyCash\PettyCashSummaryRepository;
use Carbon\Carbon;
use Exception;
use Illuminate\Http\Request;
use Yajra\DataTables\Facades\DataTables;


/**
 * Class PettyCashTableController
 *
 * @package App\Http\Controllers\Backend\Transaction\PettyCash
 */
final class PettyCashTableController extends BaseController
{
    /**
     * PettyCashTableController constructor.
     *
     * @param  PettyCashSummaryRepository  $repository
     */
    public function __construct(PettyCashSummaryRepository $repository)
    {
        $this->repository = $repository;
    }

    /**
     * @param  Request  $request
     *
     * @return mixed
     * @throws Exception
     */
    public function __invoke(Request $request)
    {
        return DataTables::of($this->repository->getForDataTable($request->get('status'), $request->get('trashed')))
            ->removeColumn('details')
            ->escapeColumns(['branch_name'])
            ->editColumn('sl', '<span style="font-weight: bold; color: #0d89ed;">{{ $sl }}</span>')
            ->editColumn('trans_date', function ($q) {
                return '<span style="font-weight: bold;">'.Carbon::parse($q->trans_date)->format('d-M-Y').'</span>';
            })
            ->addColumn('total_receipt_hq', function ($q) {
                return number_format($q->details->sum('total_receipt_hq'), 2);
            })
            ->addColumn('transfer_imprest_cash', function ($q) {
                return number_format($q->details->sum('transfer_imprest_cash'), 2);
            })
            ->addColumn('m_receipt', function ($q) {
                return number_format($q->details->sum('m_receipt'), 2);
            })
            ->addColumn('total_expense_cq', function ($q) {
                return number_format($q->details->sum('total_expense_cq'), 2);
            })
            ->addColumn('total_exp', function ($q) {
                return number_format($q->details->sum('total_exp'), 2);
            })
            ->addColumn('actions', function ($q) {
                return $q->action_buttons;
            })
            ->setRowClass(function ($q) {
                return $q->isApprove() ? 'bg-success-light' : '';
            })
            ->order(function ($query) {
                $query->orderBy('trans_date', 'desc');
                $query->orderBy('sl', 'asc');
            })
            //   ->rawColumns(['sl','trans_date'])
            ->make(true);
    }
}
