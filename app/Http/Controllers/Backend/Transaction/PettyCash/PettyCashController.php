<?php

namespace App\Http\Controllers\Backend\Transaction\PettyCash;


use App\Exceptions\GeneralException;
use App\Http\Controllers\BaseController;
use App\Models\PettyCash\PettyCashSummary;
use App\Repositories\Backend\Setup\Bank\BankRepository;
use App\Repositories\Backend\Setup\Branch\BranchRepository;
use App\Repositories\Backend\Transaction\PettyCash\PettyCashSummaryRepository;
use Auth;
use Carbon\Carbon;
use Illuminate\Contracts\View\Factory;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\View\View;
use Log;
use Throwable;


/**
 * Class PettyCashController
 *
 * @package App\Http\Controllers\Backend\Transaction\PettyCash
 */
final class PettyCashController extends BaseController
{
    /**
     * @var BankRepository
     */
    /**
     * @var BankRepository
     */
    /**
     * @var BankRepository
     */
    /**
     * @var BankRepository|BranchRepository
     */
    private $bankRepository;

    /**
     * PettyCashController constructor.
     *
     * @param  PettyCashSummaryRepository  $repository
     * @param  BankRepository              $bankRepository
     */
    public function __construct(PettyCashSummaryRepository $repository, BankRepository $bankRepository)
    {
        $this->repository = $repository;
        $this->bankRepository = $bankRepository;

        $this->module_parent = 'Transaction :: Petty Cash';
        $this->module_name = 'pettycashes';
        $this->module_icon = 'mdi-hospital-building';
        $this->module_route = 'admin.transaction.'.$this->module_name;
        $this->module_view = 'backend.transaction.'.$this->module_name;

    }

    /**
     * @return Factory|View
     */
    public function index()
    {
        $module_parent = $this->module_parent;
        $module_name = $this->module_name;
        $module_name_singular = str_singular($this->module_name);
        $module_icon = $this->module_icon;
        $module_action = 'Index';
        $module_route = $this->module_route;
        $module_view = $this->module_view;

        $page_heading = $this->module_parent.' - View All';

        Log::alert('Browse : '.ucfirst($module_parent).'  '.ucfirst($module_action).' By User : '.Auth::user()->name);
        return view($module_view.'.index',
            compact('module_name', $module_name, 'module_name_singular', 'module_parent', 'module_icon', 'page_heading',
                'module_action', 'module_view', 'module_route'))
            ->with('carbon', Carbon::now());
    }

    /**
     * @return $this
     * @throws GeneralException
     */
    public function create()
    {
        $module_parent = $this->module_parent;
        $module_name = $this->module_name;
        $module_name_singular = str_singular($this->module_name);
        $module_icon = $this->module_icon;
        $module_action = 'Create';
        $module_route = $this->module_route;
        $module_view = $this->module_view;

        $page_heading = $this->module_parent.' - Add New';

        $branch = $this->getBranch();

        $branchBank = $branch->banks->pluck('id');

        $banks = $this->bankRepository->getAll()->whereIn('id', $branchBank);

        Log::alert('Create : '.ucfirst($module_parent).'  '.ucfirst($module_action).' By User : '.Auth::user()->name);

        return view($module_view.'.create',
            compact('module_name', 'module_name_singular', 'module_parent', 'module_icon',
                'page_heading', 'module_action', 'module_view', 'module_route'))
            ->with('carbon', Carbon::now())
            ->with('banks', $banks)
            ->with('branches', $branch);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  Request  $request
     * @return Response
     * @throws GeneralException
     * @throws Throwable
     */
    public function store(Request $request)
    {
        $module_route = $this->module_route;

        $this->repository->create([
            'data' => $request->except(['_token']),
        ]);

//        \Log::info(ucfirst($module_action) . " '$module_name': '" . $request->name . ", ID:" . $request->id . " ' by User:" . $this->user->name);
        return redirect()->route($module_route.'.index')->withFlashSuccess('Record successfully saved');
    }

    /**
     * Display the specified resource.
     *
     * @param $pettycash
     * @return Response
     * @throws GeneralException
     * @internal param int $id
     */
    public function show($pettycash)
    {
        $module_parent = $this->module_parent;
        $module_name = $this->module_name;
        $module_name_singular = str_singular($this->module_name);
        $module_icon = $this->module_icon;
        $module_action = 'View';
        $module_route = $this->module_route;
        $module_view = $this->module_view;

        $page_heading = $this->module_parent.' - View';

        $branch = $this->getBranch();
        $branchBank = $branch->banks->pluck('id');
        $banks = $this->bankRepository->getAll()->whereIn('id', $branchBank);

        $module_name_singular = $this->repository->find($pettycash);

        return view($module_view.'.show',
            compact('module_name', 'module_name_singular', 'module_parent', 'module_icon',
                'page_heading', 'module_action', 'module_view', 'module_route'))
            ->with('carbon', Carbon::now())
            ->with('banks', $banks)
            ->with('branches', $branch);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param $pettycash
     * @return Response
     * @throws GeneralException
     */
    public function edit($pettycash)
    {
        $module_parent = $this->module_parent;
        $module_name = $this->module_name;
        $module_name_singular = str_singular($this->module_name);
        $module_icon = $this->module_icon;
        $module_action = 'Save';
        $module_route = $this->module_route;
        $module_view = $this->module_view;

        $page_heading = $this->module_parent.' - Edit';

        $module_name_singular = $this->repository->find($pettycash);
        if (!$module_name_singular) {
            throw new GeneralException('No Transaction Found!');
        }

        $module_name_singular = $module_name_singular->load('details');

        $carbon = Carbon::now();
        $branch = $this->getBranch();

        $branchBank = $branch->banks->pluck('id');

        $banks = $this->bankRepository->getAll()->whereIn('id', $branchBank);

        Log::alert('Edit : '.ucfirst($module_parent).'  '.ucfirst($module_action).' By User : '.Auth::user()->name);
        return view($module_view.'.edit',
            compact('module_name', 'module_name_singular', 'module_parent', 'module_icon',
                'page_heading', 'module_action', 'module_view', 'module_route'))
            ->with('carbon', $carbon)
            ->with('banks', $banks)
            ->with('branches', $branch);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  PettyCashSummary  $pettycash
     * @param  Request           $request
     * @return Response
     * @throws Throwable
     */
    public function update(PettyCashSummary $pettycash, Request $request)
    {
        $module_route = $this->module_route;

        $this->repository->update($pettycash,
            [
                'data' => $request->except(
                    '_token',
                    '_method'
                ),
            ]);

        return redirect()->route($module_route.'.index')->withFlashSuccess(trans('Record successfully updated'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  Request                   $request
     * @param                            $pettycash
     * @return Response
     */
    public function approve(Request $request, $pettycash)
    {
        $module_name_singular = str_singular($this->module_name);

        $$module_name_singular = PettyCashSummary::findOrFail($pettycash);

        $$module_name_singular->update(['approve' => '1', 'approve_by' => access()->user()->id]);

//        Log::info(ucfirst($module_action) . " '$module_name': '" . $$module_name_singular->name . ", ID:" . $$module_name_singular->id . " ' by User:" . access()->user()->name);
        return redirect()->back()->with('flash_success',
            '<i class="fa fa-check"></i> '.ucfirst($module_name_singular).' Approved successfully!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  PettyCashSummary  $pettycash
     * @return Response
     * @throws GeneralException
     */
    public function destroy(PettyCashSummary $pettycash)
    {
        $module_route = $this->module_route;

        $this->repository->delete($pettycash);

        return redirect()->route($module_route.'.deleted')->withFlashSuccess(trans('Record successfully deleted'));
    }

    /**
     * @param  PettyCashSummary
     * @return mixed
     * @throws GeneralException
     */
    public function restore($pettycash)
    {
        $module_route = $this->module_route;

        $module_name = PettyCashSummary::withTrashed()->find($pettycash);
        $this->repository->restore($module_name);

        return redirect()->route($module_route.'.index')->withFlashSuccess(trans('Record successfully restored'));
    }

    /**
     * @param $pettycash
     * @return mixed
     * @throws GeneralException
     */
    public function delete($pettycash)
    {
        $module_route = $this->module_route;

        $module_name = PettyCashSummary::withTrashed()->find($pettycash);
        $this->repository->forceDelete($module_name);

        return redirect()->route($module_route.'.deleted')->withFlashSuccess(trans('Records deleted permanently'));
    }
}
