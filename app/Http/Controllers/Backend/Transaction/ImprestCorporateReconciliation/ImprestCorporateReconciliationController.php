<?php

namespace App\Http\Controllers\Backend\Transaction\ImprestCorporateReconciliation;

use App\Exceptions\GeneralException;
use App\Http\Controllers\BaseController;
use App\Imports\ExcelImport;
use App\Models\ImprestCorporateReconciliation\ImprestCorporateReconciliationSummary;
use App\Models\OutstandingReconciliation\OutstandingReconciliationSummary;
use App\Repositories\Backend\Setup\Bank\BankRepository;
use App\Repositories\Backend\Setup\BankAccount\BankAccountRepository;
use App\Repositories\Backend\Setup\Branch\BranchRepository;
use App\Repositories\Backend\Setup\Opening\OpeningSummaryRepository;
use App\Repositories\Backend\Setup\Corporate\CorporateSummaryRepository;
use App\Repositories\Backend\Transaction\ImprestCorporateReconciliation\ImprestCorporateReconciliationDetailsRepository;
use App\Repositories\Backend\Transaction\ImprestCorporateReconciliation\ImprestCorporateReconciliationSummaryRepository;
use App\Repositories\Backend\Transaction\OutstandingReconciliation\OutstandingReconciliationSummaryRepository;
use App\Repositories\Backend\Transaction\PettyCash\PettyCashDetailsRepository;
use App\Repositories\Backend\Transaction\PettyCash\PettyCashSummaryRepository;
use Auth;
use Carbon\Carbon;
use Carbon\CarbonPeriod;
use Illuminate\Contracts\View\Factory;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Arr;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;
use Illuminate\View\View;
use Log;
use Maatwebsite\Excel\Facades\Excel;
use Throwable;

/**
 * Class ImprestCorporateReconciliationController
 *
 * @package App\Http\Controllers\Backend\Transaction\ImprestCorporateReconciliation
 */
final class ImprestCorporateReconciliationController extends BaseController
{
    /**
     * @var ImprestCorporateReconciliationDetailsRepository
     */
    private $detailsRepository;
    /**
     * @var BankRepository
     */
    private $bankRepository;
    /**
     * @var BankAccountRepository
     */
    private $bankAccountRepository;

    /**
     * @var BranchRepository
     */
    private $branchRepository;

    /**
     * @var PettyCashSummaryRepository
     */
    private $pettyCashSummaryRepository;

    /**
     * @var PettyCashDetailsRepository
     */
    private $pettyCashDetailsRepository;

    /**
     * @var CorporateSummaryRepository
     */
    private $corporateSummaryRepository;

    /**
     * OutstandingReconciliationController constructor.
     *
     * @param ImprestCorporateReconciliationSummaryRepository $repository
     * @param BankRepository $bankRepository
     * @param BankAccountRepository $bankAccountRepository
     * @param BranchRepository $branchRepository
     * @param PettyCashSummaryRepository $pettyCashSummaryRepository
     * @param PettyCashDetailsRepository $pettyCashDetailsRepository
     * @param CorporateSummaryRepository $corporateSummaryRepository
     */
    public function __construct(
        ImprestCorporateReconciliationSummaryRepository $repository,
        BankRepository $bankRepository,
        BankAccountRepository $bankAccountRepository,
        BranchRepository $branchRepository,
        PettyCashSummaryRepository $pettyCashSummaryRepository,
        PettyCashDetailsRepository $pettyCashDetailsRepository,
        CorporateSummaryRepository $corporateSummaryRepository
    )
    {
        $this->repository = $repository;
        $this->bankRepository = $bankRepository;
        $this->bankAccountRepository = $bankAccountRepository;
        $this->branchRepository = $branchRepository;
        $this->pettyCashSummaryRepository = $pettyCashSummaryRepository;
        $this->pettyCashDetailsRepository = $pettyCashDetailsRepository;
        $this->corporateSummaryRepository = $corporateSummaryRepository;

        $this->module_parent = 'Transaction :: Imprest Corporate Reconciliation';
        $this->module_name = 'imprest_corporate_reconciliations';
        $this->module_icon = 'mdi-hospital-building';
        $this->module_route = 'admin.transaction.' . $this->module_name;
        $this->module_view = 'backend.transaction.' . $this->module_name;
    }

    /**
     * @return Factory|View
     */
    public function index()
    {
        $module_parent = $this->module_parent;
        $module_name = $this->module_name;
        $module_name_singular = str_singular($this->module_name);
        $module_icon = $this->module_icon;
        $module_action = 'Index';
        $module_route = $this->module_route;
        $module_view = $this->module_view;

        $page_heading = $this->module_parent . ' - View All';

        Log::alert('Browse : ' . ucfirst($module_parent) . '  ' . ucfirst($module_action) . ' By User : ' . Auth::user()->name);
        return view($module_view . '.index',
            compact('module_name', $module_name, 'module_name_singular', 'module_parent', 'module_icon', 'page_heading',
                'module_action', 'module_view', 'module_route'));
    }

    /**
     * @param Request $request
     * @return Collection | View
     * @throws GeneralException
     */
    public function create(Request $request)
    {
        $module_parent = $this->module_parent;
        $module_name = $this->module_name;
        $module_name_singular = str_singular($this->module_name);
        $module_icon = $this->module_icon;
        $module_action = 'Create';
        $module_route = $this->module_route;
        $module_view = $this->module_view;
        $getCorporateBranch = DB::table('branches')->where('name', 'corporate')->first();
        $page_heading = $this->module_parent . ' - Add New';

        if ($request->expectsJson()) {
            //get branch wise banks
            return $this->bankRepository->toJsonBanks($request->get('branch'));
        }

        Log::alert('Create : ' . ucfirst($module_parent) . '  ' . ucfirst($module_action) . ' By User : ' . Auth::user()->name);

        return view($module_view . '.create',
            compact('module_name', 'module_name_singular', 'module_parent', 'module_icon', 'module_view',
                'page_heading', 'module_action', 'module_route', 'getCorporateBranch'))
            ->with('carbon', Carbon::now())
            ->with('show_check_form', true)
            ->with('show_input_form', false);
    }

    /**
     * @param Request $request
     * @return mixed
     * @throws GeneralException
     */
    public function generate(Request $request)
    {
        $transDate = Carbon::now();
        $module_parent = $this->module_parent;
        $module_name = $this->module_name;
        $module_name_singular = str_singular($this->module_name);
        $module_icon = $this->module_icon;
        $module_action = 'Create';
        $module_route = $this->module_route;
        $module_view = $this->module_view;

        $page_heading = page_header('Create New', $module_name);

        if ($request->isMethod('get')) {
            return redirect()->route($module_route . '.create');
        }

        $showCheckForm = false;
        $showInputForm = true;

        $selectedBankId = $request->get('bank_id');
        $view_status = $request->get('view_status');
        $selectedBranchId = $request->get('branch_id');
        $selectedBranchCode = $this->branchRepository->getModel()->select(['code'])->where('id',
            $selectedBranchId)->first()->code;
        $selectedAccount = $request->get('account_number');
        $selectedMonth = $request->get('month');
        $selectedYear = $request->get('year');
        $selectedYearMonth = $selectedYear . $selectedMonth;
        $created_by = Auth::user()->id;
        $created_at = $transDate->toDateTimeString();
        $trans_date = $transDate->toDateTimeString();
        $selectedBank = $this->bankRepository->find($selectedBankId);
        $selectedBankName = $selectedBank->name;
        $selectedBranch = $this->branchRepository->find($selectedBranchId);
        $selectedBranchName = $selectedBranch->name;
        $selectedBranchShortName = $selectedBranch->short_name;
        $selectedBranchCode = $selectedBranch->code;
        $selectedBranchNameCode = $selectedBranchCode . '' . $selectedBranch->name;

        $selectedMonthName = strftime('%B', mktime(0, 0, 0, $selectedMonth, 1));

        $selectedMonthDates = CarbonPeriod::between(Carbon::parse('first day of ' . $selectedMonthName . ' ' . $selectedYear),
            Carbon::parse('last day of ' . $selectedMonthName . ' ' . $selectedYear));

        $selectedAccountNumber = $selectedAccount;


        $carbonMonth = Carbon::parse('previous month last day of ' . $selectedMonthName . ' ' . $selectedYear);

        $openingBalance = DB::table('setup_corporate_summary')
            ->where('branch_id', $selectedBranchId)
            ->where('bank_id', $selectedBankId)
            ->where('account_number', $selectedAccount)
            ->select('pc_bank_amount')
            ->first();
        //first initial opening
        $initialOpening = $this->corporateSummaryRepository->getModel()->select([
            'sl', 'branch_id', 'bank_id', 'trans_date', 'pc_bank_amount', 'pc_cash_amount', 'month', 'year'
        ])
            ->where('branch_id', $selectedBranchId)
            ->where('bank_id', $selectedBankId)
            ->orderByDesc('id')
            ->get();
        $count = count($initialOpening);
        if ($count == '0') {
            return redirect()->back()->withFlashSuccess(trans('Opening amount can not inserted'));
        }else{
            $lastMonthCalculation=DB::table('trns_impr_cor_summary')
                ->where('branch_id', $selectedBranchId)
                ->where('branch_code', $selectedBranchCode)
                ->where('bank_id', $selectedBankId)
                ->where('account_number', $selectedAccount)
                ->orderBy('id','DESC')
                ->first();
            if ($lastMonthCalculation){
                $final_opening=$lastMonthCalculation->total_i;
            }else{
                $final_opening=$openingBalance->pc_bank_amount;
            }

        }
        if ($view_status == 1) {
            $accpacData_entry_check = DB::table('trns_impr_cor_check_details')
                ->where('branch_id', $selectedBranchId)
                ->where('branch_code', $selectedBranchCode)
                ->where('bank_id', $selectedBankId)
                ->where('account_number', $selectedAccount)
                ->where('year', $selectedYear)
                ->where('month', $selectedMonth)
                ->get();
            $count_entry_check = count($accpacData_entry_check);
            if ($count_entry_check == 0) {
                return redirect()->back()->withFlashSuccess(trans('Data not inserted'));
            }
        } else {
            $check_data=DB::table('trns_impr_cor_details')
                ->where('branch_id', $selectedBranchId)
                ->where('branch_code', $selectedBranchCode)
                ->where('bank_id', $selectedBankId)
                ->where('account_number', $selectedAccount)
                ->where('year', $selectedYear)
                ->where('month', $selectedMonth)
                ->get();
            $count_check_data = count($check_data);
            if ($count_check_data) {
                return redirect()->back()->withFlashSuccess(trans('This data all-ready approved'));
            }
            $accpacData_entry_check = DB::table('trns_impr_cor_check_details')
                ->where('branch_id', $selectedBranchId)
                ->where('branch_code', $selectedBranchCode)
                ->where('bank_id', $selectedBankId)
                ->where('account_number', $selectedAccount)
                ->where('year', $selectedYear)
                ->where('month', $selectedMonth)
                ->get();
            $count_entry_check = count($accpacData_entry_check);
            if ($count_entry_check) {
                $accpacData_delete = DB::table('trns_impr_cor_check_details')
                    ->where('branch_id', $selectedBranchId)
                    ->where('branch_code', $selectedBranchCode)
                    ->where('bank_id', $selectedBankId)
                    ->where('account_number', $selectedAccount)
                    ->where('year', $selectedYear)
                    ->where('month', $selectedMonth)
                    ->delete();
            }

            if ($request->hasFile('select_file')) {
                $file = $request->select_file;
                Excel::import(new ExcelImport($selectedBranchCode, $selectedBranchId, $selectedBankId, $selectedAccount, $selectedYear, $selectedMonth, $selectedYearMonth, $created_by, $created_at, $trans_date), $file);
            }
        }

        $approved_data_found = DB::table('trns_impr_cor_summary')
            ->where('branch_id', $selectedBranchId)
            ->where('branch_code', $selectedBranchCode)
            ->where('bank_id', $selectedBankId)
            ->where('account_number', $selectedAccount)
            ->where('year', $selectedYear)
            ->where('month', $selectedMonth)
            ->first();
        if ($approved_data_found) {
            $approved_status = 1;
        } else {
            $approved_status = 0;
        }

//        $initialOpeningMonth = $initialOpening->first()->month;
//        $initialOpeningYear = $initialOpening->first()->year;
//        $initialOpeningDay = Carbon::createFromDate($initialOpeningYear, $initialOpeningMonth, '01')->lastOfMonth();

        //get opening of previous of month of selected month
//        $pettyCashClosing = $this->pettyCashSummaryRepository->getModel()->select([
//            'sl', 'branch_id', 'trans_date', 'month', 'year'
//        ])
//            ->where('branch_id', $selectedBranchId)
//            ->whereBetween('trans_date', [$initialOpeningDay->toDateString(), $carbonMonth->toDateString()])
//            ->get();

        //get all deposited/received fromm corporate office during selected month
//        $allDepositsFromHo = $this->pettyCashDetailsRepository->getModel()->select([
//            'trans_id', 'trans_sl', 'branch_id', 'trans_date', 'total_receipt_hq', 'transfer_imprest_cash', 'm_receipt',
//            'total_expense_cq', 'total_exp', 'month'
//        ])
//            ->where('month', $selectedMonth)
//            ->where('branch_id', $selectedBranchId)
//            ->get();

        ////calculations


//        $totalReceivedFromHo = $allDepositsFromHo->sum('total_receipt_hq');
//
//        $totalChequeIssued = $allDepositsFromHo->sum('total_expense_cq') + $allDepositsFromHo->sum('transfer_imprest_cash');


        $accpacData_b_data = DB::table('trns_impr_cor_check_details')
            ->where('branch_id', $selectedBranchId)
            ->where('branch_code', $selectedBranchCode)
            ->where('bank_id', $selectedBankId)
            ->where('account_number', $selectedAccount)
            ->where('year', $selectedYear)
            ->where('month', $selectedMonth)
            ->where('types', 'b')
            ->get();
        $accpacData_c_data = DB::table('trns_impr_cor_check_details')
            ->where('branch_id', $selectedBranchId)
            ->where('branch_code', $selectedBranchCode)
            ->where('bank_id', $selectedBankId)
            ->where('account_number', $selectedAccount)
            ->where('year', $selectedYear)
            ->where('month', $selectedMonth)
            ->where('types', 'c')
            ->get();
        $accpacData_f_data = DB::table('trns_impr_cor_check_details')
            ->where('branch_id', $selectedBranchId)
            ->where('branch_code', $selectedBranchCode)
            ->where('bank_id', $selectedBankId)
            ->where('account_number', $selectedAccount)
            ->where('year', $selectedYear)
            ->where('month', $selectedMonth)
            ->where('types', 'f')
            ->get();
        $accpacData_g_data = DB::table('trns_impr_cor_check_details')
            ->where('branch_id', $selectedBranchId)
            ->where('branch_code', $selectedBranchCode)
            ->where('bank_id', $selectedBankId)
            ->where('account_number', $selectedAccount)
            ->where('year', $selectedYear)
            ->where('month', $selectedMonth)
            ->where('types', 'g')
            ->get();
        $accpacData_h_data = DB::table('trns_impr_cor_check_details')
            ->where('branch_id', $selectedBranchId)
            ->where('branch_code', $selectedBranchCode)
            ->where('bank_id', $selectedBankId)
            ->where('account_number', $selectedAccount)
            ->where('year', $selectedYear)
            ->where('month', $selectedMonth)
            ->where('types', 'h')
            ->get();
        return view($module_view . '.create',
            compact('module_name', 'module_name_singular', 'module_parent', 'module_icon', 'module_view',
                'page_heading', 'module_action', 'module_route', 'accpacData_f_data',
                'accpacData_g_data', 'accpacData_b_data', 'accpacData_c_data', 'accpacData_h_data', 'final_opening', 'approved_status'))
            ->with('carbon', Carbon::now())
            ->with('selected_month_dates', $selectedMonthDates)
            ->with('branches', $this->getBranches()->pluck('name', 'id'))
            ->with('show_check_form', $showCheckForm)
            ->with('show_input_form', $showInputForm)
            ->with('selected_branch_name', strtoupper($selectedBranchName))
            ->with('selected_branch_id', $selectedBranchId)
            ->with('selected_branch_code', $selectedBranchCode)
            ->with('selected_branch_name', $selectedBranchName)
            ->with('selected_branch_short_name', $selectedBranchShortName)
            ->with('selected_branch_name_code', strtoupper($selectedBranchNameCode))
            ->with('selected_bank_name', strtoupper($selectedBankName))
            ->with('selected_bank_id', $selectedBankId)
            ->with('selected_month_id', $selectedMonth)
            ->with('selected_year', $selectedYear)
            ->with('selected_month_name', strtoupper($selectedMonthName))
            ->with('selected_account', $selectedAccount)
            ->with('selected_account_number', strtoupper($selectedAccountNumber));
//            ->with('all_received_from_ho', $allDepositsFromHo)
//            ->with('total_all_received_from_ho', $totalReceivedFromHo)
//            ->with('total_cheque_issued', $totalChequeIssued);

    }

    /**
     * @param Request $request
     * @return BankAccountRepository[]|bool
     */
    public function getAccountNumber(Request $request)
    {
        return DB::table('bank_accounts')
            ->where('branch_id', $request->get('branch'))
            ->where('bank_id', $request->get('bank'))
            ->get();
//        return $this->bankAccountRepository->getAccountNumber($request->get('branch'), $request->get('bank'));
    }

    /**
     * @param Request $request
     * @return
     */
    public function checkPreviousEntry(Request $request)
    {
//        dd($this->repository->checkPreviousEntry($request->all()));
        return $this->repository->checkPreviousEntry($request->all());
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     * @return Response
     * @throws Throwable
     */
    public function store(Request $request)
    {
        $module_route = $this->module_route;

        $this->repository->create([
            'data' => $request->except(['_token']),
        ]);

//        \Log::info(ucfirst($module_action) . " '$module_name': '" . $request->name . ", ID:" . $request->id . " ' by User:" . $this->user->name);
        return redirect()->route($module_route . '.index')->withFlashSuccess('Record successfully saved');
    }

    /**
     * Display the specified resource.
     *
     * @param $outstandingreconciliation
     * @return Factory|View
     * @throws GeneralException
     * @internal param int $id
     */
    public function show($outstandingreconciliation)
    {

        $module_parent = $this->module_parent;
        $module_name = $this->module_name;
        $module_name_singular = str_singular($this->module_name);
        $module_icon = $this->module_icon;
        $module_action = 'Details';
        $module_route = $this->module_route;
        $module_view = $this->module_view;

        $page_heading = $this->module_parent . ' - View';

        $$module_name_singular = $this->repository->getById($outstandingreconciliation)->load('details');
        //details table data here
        $details = Arr::get($$module_name_singular, 'details');


        $showCheckForm = false;
        $showInputForm = true;

        $selectedBankId = $$module_name_singular->bank_id;
        $selectedBranchId = $$module_name_singular->branch_id;

        $selectedBranchCode = $this->branchRepository->getModel()->select(['code'])
            ->where('id', $selectedBranchId)
            ->first()->code;

        $selectedAccount = $$module_name_singular->account_number;
        $selectedMonth = $$module_name_singular->month;
        $selectedYear = $$module_name_singular->year;

        $selectedBankName = $this->bankRepository->getById($selectedBankId)->name;
        $selectedBranch = $this->branchRepository->getById($selectedBranchId);
        $selectedBranchName = $selectedBranch->name;
        $selectedBranchCode = $selectedBranch->code;
        $selectedBranchNameCode = $selectedBranch->name . '' . $selectedBranchCode;
        $selectedMonthName = strftime('%B', mktime(0, 0, 0, $selectedMonth, 1));
        $selectedAccountNumber = $selectedAccount;

        $carbonMonth = Carbon::parse('previous month last day of ' . $selectedMonthName . ' ' . $selectedYear);

        $selectedMonthDates = CarbonPeriod::between(Carbon::parse('first day of ' . $selectedMonthName . ' ' . $selectedYear),
            Carbon::parse('last day of ' . $selectedMonthName . ' ' . $selectedYear));

        //first initial opening
        $initialOpening = $this->corporateSummaryRepository->getModel()->select([
            'sl', 'branch_id', 'bank_id', 'trans_date', 'pc_bank_amount', 'pc_cash_amount', 'month', 'year'
        ])
            ->where('branch_id', $selectedBranchId)
            ->where('bank_id', $selectedBankId)
            ->orderByDesc('id')
            ->get();

        $initialOpeningMonth = $initialOpening->first()->month;
        $initialOpeningYear = $initialOpening->first()->year;
        $initialOpeningDay = Carbon::createFromDate($initialOpeningYear, $initialOpeningMonth, '01')->lastOfMonth();

        //get opening of previous of month of selected moth
        $pettyCashClosing = $this->pettyCashSummaryRepository->getModel()->select([
            'sl', 'branch_id', 'trans_date', 'month', 'year'
        ])
            ->where('branch_id', $selectedBranchId)
            ->whereBetween('trans_date', [$initialOpeningDay->toDateString(), $carbonMonth->toDateString()])
            ->get();


        //get all deposited/received fromm corporate office during selected month
        $allDepositsFromHo = $this->pettyCashDetailsRepository->getModel()->select([
            'trans_id', 'trans_sl', 'branch_id', 'trans_date', 'total_receipt_hq', 'transfer_imprest_cash', 'm_receipt',
            'total_expense_cq', 'total_exp', 'month'
        ])
            ->where('month', $selectedMonth)
            ->where('branch_id', $selectedBranchId)
            ->get();


        ////calculations
        $openingBalance = DB::table('trns_impr_cor_summary')
            ->where('branch_id', $selectedBranchId)
            ->where('bank_id', $selectedBankId)
            ->where('account_number', $selectedAccount)
            ->where('year', $selectedYear)
            ->where('month', $selectedMonth)
            ->select('opening_balance','total_i')
            ->first();
        

        $totalReceivedFromHo = $allDepositsFromHo->sum('total_receipt_hq');

        $totalChequeIssued = $allDepositsFromHo->sum('total_expense_cq') + $allDepositsFromHo->sum('transfer_imprest_cash');

//        $allDeposits = $$module_name_singular->whereHas('details', function ($q){
//           return $q->where('types',1);
//        })->get();

        $allDeposits = $details->where('types', 1);
        $allWithdrawn = $details->where('types', 2);
        $allChequeIssues = $details->where('types', 3);
        $deposited_received_from_corporates = $details->where('types', 4);
        $less_issued_checks = $details->where('types', 5);
        $less_issued_cq_checks = $details->where('types', 6);

        $accpacData_b_data = DB::table('trns_impr_cor_check_details')
            ->where('branch_id', $selectedBranchId)
            ->where('branch_code', $selectedBranchCode)
            ->where('bank_id', $selectedBankId)
            ->where('account_number', $selectedAccount)
            ->where('year', $selectedYear)
            ->where('month', $selectedMonth)
            ->where('types', 'b')
            ->get();
        $accpacData_c_data = DB::table('trns_impr_cor_check_details')
            ->where('branch_id', $selectedBranchId)
            ->where('branch_code', $selectedBranchCode)
            ->where('bank_id', $selectedBankId)
            ->where('account_number', $selectedAccount)
            ->where('year', $selectedYear)
            ->where('month', $selectedMonth)
            ->where('types', 'c')
            ->get();
        $accpacData_f_data = DB::table('trns_impr_cor_check_details')
            ->where('branch_id', $selectedBranchId)
            ->where('branch_code', $selectedBranchCode)
            ->where('bank_id', $selectedBankId)
            ->where('account_number', $selectedAccount)
            ->where('year', $selectedYear)
            ->where('month', $selectedMonth)
            ->where('types', 'f')
            ->get();
        $accpacData_g_data = DB::table('trns_impr_cor_check_details')
            ->where('branch_id', $selectedBranchId)
            ->where('branch_code', $selectedBranchCode)
            ->where('bank_id', $selectedBankId)
            ->where('account_number', $selectedAccount)
            ->where('year', $selectedYear)
            ->where('month', $selectedMonth)
            ->where('types', 'g')
            ->get();
        $accpacData_h_data = DB::table('trns_impr_cor_check_details')
            ->where('branch_id', $selectedBranchId)
            ->where('branch_code', $selectedBranchCode)
            ->where('bank_id', $selectedBankId)
            ->where('account_number', $selectedAccount)
            ->where('year', $selectedYear)
            ->where('month', $selectedMonth)
            ->where('types', 'h')
            ->get();

        return view($module_view . '.show',
            compact('module_name', 'module_name_singular', $module_name_singular, 'module_parent', 'module_icon',
                'page_heading', 'module_action', 'module_view', 'module_route', 'accpacData_f_data',
                'accpacData_g_data', 'accpacData_b_data', 'accpacData_c_data', 'accpacData_h_data','openingBalance'))
            ->with('carbon', Carbon::now())
            ->with('selected_month_dates', $selectedMonthDates)
            ->with('branches', $this->getBranches()->pluck('name', 'id'))
            ->with('show_check_form', true)
            ->with('show_input_form', false)
            ->with('branches', $this->getBranches()->pluck('name', 'id'))
            ->with('show_check_form', $showCheckForm)
            ->with('show_input_form', $showInputForm)
            ->with('selected_branch_name', strtoupper($selectedBranchName))
            ->with('selected_branch_id', $selectedBranchId)
            ->with('selected_branch_name', $selectedBranchName)
            ->with('selected_branch_code', $selectedBranchCode)
            ->with('selected_branch_name_code', $selectedBranchNameCode)
            ->with('selected_bank_name', strtoupper($selectedBankName))
            ->with('selected_bank_id', $selectedBankId)
            ->with('selected_month_id', $selectedMonth)
            ->with('selected_month_name', strtoupper($selectedMonthName))
            ->with('selected_account', $selectedAccount)
            ->with('selected_account_number', strtoupper($selectedAccountNumber))
            ->with('all_received_from_ho', $allDepositsFromHo)
            ->with('total_all_received_from_ho', $totalReceivedFromHo)
            ->with('total_cheque_issued', $totalChequeIssued)
            ->with('all_deposits', $allDeposits)
            ->with('all_withdrawn', $allWithdrawn)
            ->with('all_cheque_issues', $allChequeIssues)
            ->with('deposited_received_from_corporates', $deposited_received_from_corporates)
            ->with('less_issued_checks', $less_issued_checks)
            ->with('less_issued_cq_checks', $less_issued_cq_checks)
            ->with('total_all_received_from_ho', $totalReceivedFromHo)
            ->with('total_cheque_issued', $totalChequeIssued);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param $outstandingreconciliation
     * @return Factory|View
     * @throws GeneralException
     */
    public function edit($outstandingreconciliation)
    {
        $module_parent = $this->module_parent;
        $module_name = $this->module_name;
        $module_name_singular = str_singular($this->module_name);
        $module_icon = $this->module_icon;
        $module_action = 'Save';
        $module_route = $this->module_route;
        $module_view = $this->module_view;

        $page_heading = $this->module_parent . ' - Edit';

        $$module_name_singular = $this->repository->getById($outstandingreconciliation)->load('details');

        $details = Arr::get($$module_name_singular, 'details');

        $showCheckForm = false;
        $showInputForm = true;

        $selectedBankId = $$module_name_singular->bank_id;
        $selectedBranchId = $$module_name_singular->branch_id;
        $selectedBranchCode = $this->branchRepository->getModel()->select(['code'])
            ->where('id', $selectedBranchId)
            ->first()->code;

        $selectedAccount = $$module_name_singular->account_number;
        $selectedMonth = $$module_name_singular->month;
        $selectedYear = $$module_name_singular->year;

        $selectedBankName = $this->bankRepository->getById($selectedBankId)->name;
        $selectedBranch = $this->branchRepository->getById($selectedBranchId);
        $selectedBranchName = $selectedBranch->name;
        $selectedBranchCode = $selectedBranch->code;
        $selectedBranchNameCode = $selectedBranch->name . '' . $selectedBranchCode;
        $selectedMonthName = strftime('%B', mktime(0, 0, 0, $selectedMonth, 1));
        $selectedAccountNumber = $selectedAccount;

        $selectedMonthDates = CarbonPeriod::between(Carbon::parse('first day of ' . $selectedMonthName . ' ' . $selectedYear),
            Carbon::parse('last day of ' . $selectedMonthName . ' ' . $selectedYear));

        $carbonMonth = Carbon::parse('previous month last day of ' . $selectedMonthName . ' ' . $selectedYear);

        //first initial opening
        $initialOpening = $this->corporateSummaryRepository->getModel()->select([
            'sl', 'branch_id', 'bank_id', 'trans_date', 'pc_bank_amount', 'pc_cash_amount', 'month', 'year'
        ])
            ->where('branch_id', $selectedBranchId)
            ->where('bank_id', $selectedBankId)
            ->orderByDesc('id')
            ->get();

        $initialOpeningMonth = $initialOpening->first()->month;
        $initialOpeningYear = $initialOpening->first()->year;
        $initialOpeningDay = Carbon::createFromDate($initialOpeningYear, $initialOpeningMonth, '01')->lastOfMonth();

        //get opening of previous of month of selected moth
        $pettyCashClosing = $this->pettyCashSummaryRepository->getModel()->select([
            'sl', 'branch_id', 'trans_date', 'month', 'year'
        ])
            ->where('branch_id', $selectedBranchId)
            ->whereBetween('trans_date', [$initialOpeningDay->toDateString(), $carbonMonth->toDateString()])
            ->get();


        //get all deposited/received fromm corporate office during selected month
        $allDepositsFromHo = $this->pettyCashDetailsRepository->getModel()->select([
            'trans_id', 'trans_sl', 'branch_id', 'trans_date', 'total_receipt_hq', 'transfer_imprest_cash', 'm_receipt',
            'total_expense_cq', 'total_exp', 'month'
        ])
            ->where('month', $selectedMonth)
            ->where('branch_id', $selectedBranchId)
            ->get();


        ////calculations
        $openingBalance = $initialOpening->sum('pc_bank_amount') + $pettyCashClosing->sum('receipt_hq') - $pettyCashClosing->sum('total_imprest_cash') - $pettyCashClosing->sum('expense_cq');

        $totalReceivedFromHo = $allDepositsFromHo->sum('total_receipt_hq');

        $totalChequeIssued = $allDepositsFromHo->sum('total_expense_cq') + $allDepositsFromHo->sum('transfer_imprest_cash');

//        $allDeposits = $$module_name_singular->whereHas('details', function ($q){
//           return $q->where('types',1);
//        })->get();

        $allDeposits = $details->where('types', 1);
        $allWithdrawn = $details->where('types', 2);
        $allChequeIssues = $details->where('types', 3);
        $deposited_received_from_corporates = $details->where('types', 4);
        $less_issued_checks = $details->where('types', 5);
        $less_issued_cq_checks = $details->where('types', 6);

        Log::alert('Edit : ' . ucfirst($module_parent) . '  ' . ucfirst($module_action) . ' By User : ' . Auth::user()->name);
        return view($module_view . '.edit',
            compact('module_name', 'module_name_singular', $module_name_singular, 'module_parent', 'module_icon',
                'page_heading', 'module_action', 'module_view', 'module_route'))
            ->with('carbon', Carbon::now())
            ->with('selected_month_dates', $selectedMonthDates)
            ->with('branches', $this->getBranches()->pluck('name', 'id'))
            ->with('show_check_form', true)
            ->with('show_input_form', false)
            ->with('branches', $this->getBranches()->pluck('name', 'id'))
            ->with('opening_balance', $openingBalance)
            ->with('show_check_form', $showCheckForm)
            ->with('show_input_form', $showInputForm)
            ->with('selected_branch_name', strtoupper($selectedBranchName))
            ->with('selected_branch_id', $selectedBranchId)
            ->with('selected_branch_name', $selectedBranchName)
            ->with('selected_branch_code', $selectedBranchCode)
            ->with('selected_branch_name_code', $selectedBranchNameCode)
            ->with('selected_bank_name', strtoupper($selectedBankName))
            ->with('selected_bank_id', $selectedBankId)
            ->with('selected_month_id', $selectedMonth)
            ->with('selected_month_name', strtoupper($selectedMonthName))
            ->with('selected_account', $selectedAccount)
            ->with('selected_account_number', strtoupper($selectedAccountNumber))
            ->with('all_received_from_ho', $allDepositsFromHo)
            ->with('total_all_received_from_ho', $totalReceivedFromHo)
            ->with('total_cheque_issued', $totalChequeIssued)
            ->with('all_deposits', $allDeposits)
            ->with('all_withdrawn', $allWithdrawn)
            ->with('all_cheque_issues', $allChequeIssues)
            ->with('deposited_received_from_corporates', $deposited_received_from_corporates)
            ->with('less_issued_checks', $less_issued_checks)
            ->with('less_issued_cq_checks', $less_issued_cq_checks)
            ->with('total_all_received_from_ho', $totalReceivedFromHo)
            ->with('total_cheque_issued', $totalChequeIssued);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param           $sl
     * @param Request $request
     * @return Response
     * @throws Throwable
     */
    public function update($sl, Request $request)
    {
        $module_route = $this->module_route;
        $this->repository->update($sl, [
            'data' => $request->except(
                '_token',
                '_method'
            ),
        ]);

        return redirect()->route($module_route . '.index')->withFlashSuccess('Record successfully updated');
    }


    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @param           $OutstandingReconciliation
     * @return Response
     */
    public function approve(Request $request, $OutstandingReconciliation)
    {
        $module_name = $this->module_name;
        $module_name_singular = str_singular($this->module_name);
        $module_action = "update";

        $$module_name_singular = OutstandingReconciliationSummary::findOrFail($OutstandingReconciliation);
        $$module_name_singular->update(['approve' => '1', 'approve_by' => access()->user()->id]);

//        Log::info(ucfirst($module_action) . " '$module_name': '" . $$module_name_singular->name . ", ID:" . $$module_name_singular->id . " ' by User:" . access()->user()->name);
        return redirect()->back()->with('flash_success',
            '<i class="fa fa-check"></i> ' . ucfirst($module_name_singular) . " Approved successfully!");
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return Response
     * @throws GeneralException
     */
    public function destroy(OutstandingReconciliationSummaryRepository $OutstandingReconciliation)
    {
        $module_name_singular = str_singular($this->module_name);
        $module_route = $this->module_route;
        $$module_name_singular = $this->repository->delete($OutstandingReconciliation);
        return redirect()->route($module_route . ".deleted")->withFlashSuccess(trans('Record successfully deleted'));
    }

    /**
     * @param $OutstandingReconciliation
     * @return mixed
     * @throws GeneralException
     */
    public function restore($OutstandingReconciliation)
    {
        $module_route = $this->module_route;
        $module_name = OutstandingReconciliationSummaryRepository::withTrashed()->find($OutstandingReconciliation);
        $this->repository->restore($module_name);
        return redirect()->route($module_route . '.index')->withFlashSuccess(trans('Record successfully restored'));
    }

    /**
     * @param $OutstandingReconciliation
     * @return mixed
     * @throws GeneralException
     * @throws Throwable
     */

    public function delete($OutstandingReconciliation)
    {
        $module_route = $this->module_route;
        $module_name = OutstandingReconciliationSummaryRepository::withTrashed()->find($OutstandingReconciliation);
        $this->repository->forceDelete($module_name);
        return redirect()->route($module_route . '.deleted')->withFlashSuccess(trans('Records deleted permanently'));
    }
}
