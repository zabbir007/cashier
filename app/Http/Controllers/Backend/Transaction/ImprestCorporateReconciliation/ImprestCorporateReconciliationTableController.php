<?php

namespace App\Http\Controllers\Backend\Transaction\ImprestCorporateReconciliation;

use App\Http\Controllers\BaseController;
use App\Repositories\Backend\Transaction\ImprestCorporateReconciliation\ImprestCorporateReconciliationSummaryRepository;
use Carbon\Carbon;
use Exception;
use Illuminate\Http\Request;
use Yajra\DataTables\Facades\DataTables;


/**
 * Class ImprestCorporateReconciliationTableController
 *
 * @package App\Http\Controllers\Backend\Transaction\ImprestCorporateReconciliation
 */
final class ImprestCorporateReconciliationTableController extends BaseController
{
    /**
     * ImprestCorporateReconciliationTableController constructor.
     *
     * @param  ImprestCorporateReconciliationSummaryRepository  $repository
     */
    public function __construct(ImprestCorporateReconciliationSummaryRepository $repository)
    {
        $this->repository = $repository;
    }

    /**
     * @param  Request  $request
     *
     * @return mixed
     * @throws Exception
     */
    public function __invoke(Request $request)
    {
        return DataTables::of($this->repository->getForDataTable($request->get('status'), $request->get('trashed')))
            ->removeColumn('details')
            ->removeColumn('branch')
            ->removeColumn('bank')
            ->escapeColumns(['branch_name', 'account_number', 'closing_per_bank', 'closing_per_bank_book'])
            ->editColumn('sl', '<span style="font-weight: bold; color: #0d89ed;">{{ $sl }}</span>')
            ->editColumn('trans_date', function ($q) {
                return '<span style="font-weight: bold;">'.Carbon::parse($q->trans_date)->format('d-M-Y').'</span>';
            })
            ->addColumn('branch_name', function ($q) {
                return $q->branch->code.$q->branch->name;
            })
            ->addColumn('bank_name', function ($q) {
                return $q->bank->name;
            })
            ->editColumn('opening_balance', function ($q) {
                return number_format($q->opening_balance, 2);
            })
            ->editColumn('closing_per_bank', function ($q) {
                return number_format($q->closing_per_bank, 2);
            })
            ->editColumn('closing_per_bank_book', function ($q) {
                return number_format($q->closing_per_bank_book, 2);
            })
//            ->addColumn('amount', function ($q) {
//                return number_format($q->details->sum('amount'), 2);
//            })
            ->addColumn('month_name', function ($q) {
                return date('F', mktime(0, 0, 0, $q->month, 1, $q->year));
            })
            ->addColumn('actions', function ($q) {
                return $q->action_buttons;
            })
            ->setRowClass(function ($q) {
                return $q->isApprove() ? 'bg-success-light' : '';
            })
            ->order(function ($query) {
                $query->orderBy('trans_date', 'desc');
                $query->orderBy('sl', 'asc');
            })
            ->make(true);

        //   ->rawColumns(['sl','trans_date'])
    }
}
