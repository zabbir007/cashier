<?php

namespace App\Http\Controllers\Backend\Transaction\BankCharge;


use App\Exceptions\GeneralException;
use App\Http\Controllers\BaseController;
use App\Models\BankCharge\BankChargeSummary;
use App\Repositories\Backend\Setup\Bank\BankRepository;
use App\Repositories\Backend\Transaction\BankCharge\BankChargeSummaryRepository;
use Auth;
use Carbon\Carbon;
use Illuminate\Contracts\View\Factory;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\View\View;
use Log;
use Throwable;


/**
 * Class BankChargeController
 *
 * @package App\Http\Controllers\Backend\Transaction\BankCharge
 */
final class BankChargeController extends BaseController
{
    private $bankRepository;

    /**
     * BankChargeController constructor.
     *
     * @param  BankChargeSummaryRepository  $repository
     * @param  BankRepository               $bankRepository
     */
    public function __construct(BankChargeSummaryRepository $repository, BankRepository $bankRepository)
    {
        $this->repository = $repository;
        $this->bankRepository = $bankRepository;

        $this->module_parent = 'Transaction:: Bank Charge';
        $this->module_name = 'bankcharges';
        $this->module_icon = 'mdi-hospital-building';
        $this->module_route = 'admin.transaction.'.$this->module_name;
        $this->module_view = 'backend.transaction.'.$this->module_name;

    }

    /**
     * @return Factory|View
     */
    public function index()
    {
        $module_parent = $this->module_parent;
        $module_name = $this->module_name;
        $module_name_singular = str_singular($this->module_name);
        $module_icon = $this->module_icon;
        $module_action = 'Index';
        $module_route = $this->module_route;
        $module_view = $this->module_view;

        $page_heading = $this->module_parent.' - View All';

        Log::alert('Browse : '.ucfirst($module_parent).'  '.ucfirst($module_action).' By User : '.Auth::user()->name);

        return view($module_view.'.index',
            compact('module_name', $module_name, 'module_name_singular', 'module_parent', 'module_icon', 'page_heading',
                'module_action', 'module_view', 'module_route'));
    }

    /**
     * @return $this
     * @throws GeneralException
     */
    public function create()
    {
        $module_parent = $this->module_parent;
        $module_name = $this->module_name;
        $module_name_singular = str_singular($this->module_name);
        $module_icon = $this->module_icon;
        $module_action = 'Create';
        $module_route = $this->module_route;
        $module_view = $this->module_view;

        $page_heading = $this->module_parent.' - Add New';

        $carbon = Carbon::now();
        $branch = $this->getBranch();
        $branchBank = $branch->banks->pluck('id');
        $banks = $this->bankRepository->getAll()->whereIn('id', $branchBank);

        Log::alert('Create : '.ucfirst($module_parent).'  '.ucfirst($module_action).' By User : '.Auth::user()->name);

        return view($module_view.'.create',
            compact('module_name', 'module_name_singular', 'module_parent', 'module_icon',
                'page_heading', 'module_action', 'module_view', 'module_route'))
            ->with('carbon', $carbon)
            ->with('banks', $banks)
            ->with('branches', $branch);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  Request  $request
     * @return Response
     * @throws GeneralException
     * @throws Throwable
     */
    public function store(Request $request)
    {
        $module_route = $this->module_route;

        $this->repository->create([
            'data' => $request->except(['_token']),
        ]);

//        \Log::info(ucfirst($module_action) . " '$module_name': '" . $request->name . ", ID:" . $request->id . " ' by User:" . $this->user->name);
        return redirect()->route($module_route.'.index')->withFlashSuccess('Record successfully saved');
    }

    /**
     * Display the specified resource.
     *
     * @param $bankcharge
     * @return Response
     * @throws GeneralException
     * @internal param int $id
     */
    public function show($bankcharge)
    {
        $module_parent = $this->module_parent;
        $module_name = $this->module_name;
        $module_name_singular = str_singular($this->module_name);
        $module_icon = $this->module_icon;
        $module_action = 'View';
        $module_route = $this->module_route;
        $module_view = $this->module_view;

        $page_heading = $this->module_parent.' - View';

        $branch = $this->getBranch();
        $branchBank = $branch->banks->pluck('id');

        $banks = $this->bankRepository->getAll()->whereIn('id', $branchBank);
        $module_name_singular = $this->repository->find($bankcharge);

        return view($module_view.'.show',
            compact('module_name', 'module_name_singular', 'module_parent', 'module_icon', 'page_heading',
                'module_action', 'module_view', 'module_route'))
            ->with('partial', 'overview')
            ->with('carbon', Carbon::now())
            ->with('banks', $banks)
            ->with('branches', $branch);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param $bankcharge
     * @return Response
     * @throws GeneralException
     */
    public function edit($bankcharge)
    {
        $module_parent = $this->module_parent;
        $module_name = $this->module_name;
        $module_name_singular = str_singular($this->module_name);
        $module_icon = $this->module_icon;
        $module_action = 'Save';
        $module_route = $this->module_route;
        $module_view = $this->module_view;

        $page_heading = $this->module_parent.' - Edit';
        $branch = $this->getBranches()->first();

        $branchBank = $branch->banks->pluck('id');
        $banks = $this->bankRepository->getAll()->whereIn('id', $branchBank);

        $module_name_singular = $this->repository->find($bankcharge);

        Log::alert('Edit : '.ucfirst($module_parent).'  '.ucfirst($module_action).' By User : '.Auth::user()->name);
        return view($module_view.'.edit',
            compact('module_name', 'module_name_singular', 'module_parent', 'module_icon', 'page_heading',
                'module_action', 'module_view', 'module_route'))
            ->with('partial', 'overview')
            ->with('carbon', Carbon::now())
            ->with('banks', $banks)
            ->with('branches', $branch);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  BankChargeSummary  $bankcharge
     * @param  Request            $request
     * @return Response
     * @throws Throwable
     */
    public function update(BankChargeSummary $bankcharge, Request $request)
    {
        $module_route = $this->module_route;

        $this->repository->update($bankcharge,
            [
                'data' => $request->except(
                    '_token',
                    '_method'
                ),
            ]);

        return redirect()->route($module_route.'.index')->withFlashSuccess(trans('Record successfully updated'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  Request                   $request
     * @param                            $bankcharge
     * @return Response
     */
    public function approve($id, Request $request)
    {
        $module_name = $this->module_name;
        $module_name_singular = str_singular($this->module_name);
        $module_action = 'update';

        $$module_name_singular = BankChargeSummary::findOrFail($id);

        $$module_name_singular->update(['approve' => '1', 'approve_by' => access()->user()->id]);

//        Log::info(ucfirst($module_action) . " '$module_name': '" . $$module_name_singular->name . ", ID:" . $$module_name_singular->id . " ' by User:" . access()->user()->name);
        return redirect()->back()->with('flash_success',
            '<i class="fa fa-check"></i> '.ucfirst($module_name_singular).' Approved successfully!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  BankChargeSummary  $bankcharge
     * @return Response
     * @throws GeneralException
     */
    public function destroy(BankChargeSummary $bankcharge)
    {
        $module_name_singular = str_singular($this->module_name);
        $module_route = $this->module_route;

        $$module_name_singular = $this->repository->delete($bankcharge);

        return redirect()->route($module_route.'.deleted')->withFlashSuccess(trans('Record successfully deleted'));
    }

    /**
     * @param $bankcharge
     * @return mixed
     * @throws GeneralException
     */
    public function restore($bankcharge)
    {
        $module_route = $this->module_route;

        $module_name = BankChargeSummary::withTrashed()->find($bankcharge);
        $this->repository->restore($module_name);

        return redirect()->route($module_route.'.index')->withFlashSuccess(trans('Record successfully restored'));
    }

    /**
     * @param $bankcharge
     * @return mixed
     * @throws GeneralException
     * @throws Throwable
     */
    public function delete($bankcharge)
    {
        $module_route = $this->module_route;

        $module_name = BankChargeSummary::withTrashed()->find($bankcharge);
        $this->repository->forceDelete($module_name);

        return redirect()->route($module_route.'.deleted')->withFlashSuccess(trans('Records deleted permanently'));
    }

    /**
     * @param  Branch   $branch
     * @param           $status
     * @param  Request  $request
     * @return mixed
     */
    public function mark(Belt $belt, $status, Request $request)
    {
        $module_name = $this->module_name;
        $module_name_singular = str_singular($this->module_name);
        $module_action = __FUNCTION__;
        $module_route = $this->module_route;
        $module_view = $this->module_view;

        $this->repository->mark($belt, $status);

        return redirect()->route($status == 1 ? $module_route.".index" : $module_route.".deactivated")->withFlashSuccess(trans('Records updated successfully'));
    }

    /**
     * @param  Request  $request
     * @return Factory|View
     */
    public function getDeactivated(Request $request)
    {
        $module_name = $this->module_name;
        $module_name_singular = str_singular($this->module_name);
        $module_action = 'Deactivated';
        $module_route = $this->module_route;
        $module_view = $this->module_view;

        $page_heading = $this->module_parent." | Deactivated Belts";

        $$module_name = $this->repository->getDeactivated();

        return view($module_view.".deactivated",
            compact('module_name', "$module_name", 'module_name_singular', 'module_icon', 'page_heading',
                'module_action', 'module_route', 'module_view'));
    }

    /**
     * @return Factory|View
     */
    public function trashed()
    {
        $module_name = $this->module_name;
        $module_name_singular = str_singular($this->module_name);
        $module_action = 'Deleted';
        $module_route = $this->module_route;
        $module_view = $this->module_view;

        $page_heading = $this->module_parent." | Deleted Belts";

        $$module_name = $this->repository->getDeleted();

        return view($module_view.".deleted",
            compact('module_name', "$module_name", "module_name_singular", 'module_icon', 'page_heading',
                'module_action', 'module_route', 'module_view'));

    }
}
