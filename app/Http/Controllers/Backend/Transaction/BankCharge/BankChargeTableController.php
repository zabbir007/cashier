<?php

namespace App\Http\Controllers\Backend\Transaction\BankCharge;

use App\Http\Controllers\BaseController;
use App\Repositories\Backend\Transaction\BankCharge\BankChargeSummaryRepository;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Yajra\DataTables\Facades\DataTables;

/**
 * Class BankChargeTableController
 *
 * @package App\Http\Controllers\Backend\Transaction\BankCharge
 */
final class BankChargeTableController extends BaseController
{
    /**
     * BankChargeController constructor.
     *
     * @param  BankChargeSummaryRepository  $repository
     */
    public function __construct(BankChargeSummaryRepository $repository)
    {
        $this->repository = $repository;
    }

    /**
     * @param  Request  $request
     *
     * @return mixed
     * @throws Exception
     */
    public function __invoke(Request $request)
    {
        return DataTables::eloquent($this->repository->getForDataTable())
            ->removeColumn('details')
            ->escapeColumns(['branch_name'])
            ->editColumn('sl', '<span style="font-weight: bold; color: #0d89ed;">{{ $sl }}</span>')
            ->editColumn('trans_date', function ($q) {
                return '<span style="font-weight: bold;">'.Carbon::parse($q->trans_date)->format('d-M-Y').'</span>';
            })
            ->addColumn('amount', function ($q) {
                return number_format($q->details->sum('amount'), 2);
            })
            ->addColumn('actions', function ($transaction) {
                return $transaction->action_buttons;
            })
            ->setRowClass(function ($q) {
                return $q->isApprove() ? 'bg-success-light' : '';
            })
            ->order(function ($query) {
                $query->orderBy('trans_date', 'desc');
                $query->orderBy('sl', 'asc');
            })
            //   ->rawColumns(['sl','trans_date'])
            ->make(true);
    }
}
