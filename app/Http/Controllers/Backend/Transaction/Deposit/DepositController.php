<?php

namespace App\Http\Controllers\Backend\Transaction\Deposit;


use App\Exceptions\GeneralException;
use App\Http\Controllers\BaseController;
use App\Models\Deposit\DepositSummary;
use App\Repositories\Backend\Setup\Brand\BrandRepository;
use App\Repositories\Backend\Setup\Product\ProductRepository;
use App\Repositories\Backend\Transaction\Deposit\DepositSummaryRepository;
use Auth;
use Carbon\Carbon;
use Illuminate\Contracts\View\Factory;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\View\View;
use Log;
use Throwable;


/**
 * Class DepositController
 *
 * @package App\Http\Controllers\Backend\Transaction\Deposit
 */
final class DepositController extends BaseController
{
    /**
     * @var ProductRepository
     */
    /**
     * @var BrandRepository|ProductRepository
     */
    private $productRepository, $brandRepository;

    /**
     * DepositController constructor.
     *
     * @param  DepositSummaryRepository  $repository
     * @param  ProductRepository         $productRepository
     * @param  BrandRepository           $brandRepository
     */
    public function __construct(
        DepositSummaryRepository $repository,
        ProductRepository $productRepository,
        BrandRepository $brandRepository
    ) {
        $this->repository = $repository;
        $this->productRepository = $productRepository;
        $this->brandRepository = $brandRepository;

        $this->module_parent = 'Transaction Date Wise';
        $this->module_name = 'deposits';
        $this->module_icon = 'mdi-hospital-building';
        $this->module_route = 'admin.transaction.'.$this->module_name;
        $this->module_view = 'backend.transaction.'.$this->module_name;
    }

    /**
     * @return Factory|View
     * @throws GeneralException
     */
    public function index()
    {
        $module_parent = $this->module_parent;
        $module_name = $this->module_name;
        $module_name_singular = str_singular($this->module_name);
        $module_icon = $this->module_icon;
        $module_action = 'Index';
        $module_route = $this->module_route;
        $module_view = $this->module_view;

        $page_heading = $this->module_parent.' - View All';

        Log::alert('Browse : '.ucfirst($module_parent).'  '.ucfirst($module_action).' By User : '.Auth::user()->name);
        return view($module_view.'.index',
            compact('module_name', $module_name, 'module_name_singular', 'module_parent', 'module_icon', 'page_heading',
                'module_action', 'module_view', 'module_route'))
            ->with('carbon', Carbon::now());
    }

    /**
     * @return $this
     * @throws GeneralException
     */
    public function create()
    {
        $module_parent = $this->module_parent;
        $module_name = $this->module_name;
        $module_name_singular = str_singular($this->module_name);
        $module_icon = $this->module_icon;
        $module_action = 'Create';
        $module_route = $this->module_route;
        $module_view = $this->module_view;

        $page_heading = 'Create New Deposit ';

        $brands = $this->brandRepository->getAll();
        $branch = $this->getBranch();

        $branchProduct = $branch->products->pluck('id');
        $banks = $branch->banks;

        foreach ($brands as $brand) {
            $products = $this->productRepository->getAll()->where('brand_id', $brand->id)->whereIn('id',
                $branchProduct);

            $table = [
                'brand_id' => $brand->id,
                'brand_name' => $brand->name,
                'all' => $products,
            ];
            $product_all[] = $table;
        }

        Log::alert('Create : '.ucfirst($module_parent).'  '.ucfirst($module_action).' By User : '.Auth::user()->name);

        return view($module_view.'.create',
            compact('module_name', 'module_name_singular', 'module_parent', 'module_icon',
                'page_heading', 'module_action', 'module_view', 'module_route'))
            ->with('carbon', Carbon::now())
            ->with('products', $product_all)
            ->with('banks', $banks)
            ->with('branches', $branch);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  Request  $request
     * @return Response
     * @throws Throwable
     */
    public function store(Request $request)
    {
        $module_route = $this->module_route;

        $this->repository->create([
            'data' => $request->except(['_token']),
        ]);

//        \Log::info(ucfirst($module_action) . " '$module_name': '" . $request->name . ", ID:" . $request->id . " ' by User:" . $this->user->name);
        return redirect()->route($module_route.'.index')->withFlashSuccess('Record successfully saved');
    }

    /**
     * Display the specified resource.
     *
     * @param $deposits
     * @return Response
     * @throws GeneralException
     * @internal param int $id
     */
    public function show($deposits)
    {
        $module_parent = $this->module_parent;
        $module_name = $this->module_name;
        $module_name_singular = str_singular($this->module_name);
        $module_icon = $this->module_icon;
        $module_action = 'Show';
        $module_route = $this->module_route;
        $module_view = $this->module_view;

        $page_heading = $this->module_parent.' - View';

        $brands = $this->brandRepository->getAll();

        $branch = $this->getBranch();

        $branchProduct = $branch->products->pluck('id');
        $banks = $branch->banks;

        foreach ($brands as $brand) {
            $products = $this->productRepository->getAll()->where('brand_id', $brand->id)->whereIn('id',
                $branchProduct);

            $table = [
                'brand_id' => $brand->id,
                'brand_name' => $brand->name,
                'all' => $products,
            ];
            $product_all[] = $table;
        }

        $module_name_singular = $this->repository->find($deposits);

        return view($module_view.'.show',
            compact('module_name', 'module_name_singular', 'module_parent', 'module_icon', 'page_heading',
                'module_action', 'module_view', 'module_route'))
            ->with('partial', 'overview')
            ->with('carbon', Carbon::now())
            ->with('products', $product_all)
            ->with('banks', $banks)
            ->with('branches', $branch);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param $deposit
     * @return Response
     * @throws GeneralException
     */
    public function edit($deposit)
    {
        $module_parent = $this->module_parent;
        $module_name = $this->module_name;
        $module_name_singular = str_singular($this->module_name);
        $module_icon = $this->module_icon;
        $module_action = 'Save';
        $module_route = $this->module_route;
        $module_view = $this->module_view;

        $page_heading = $this->module_parent.' - Edit';

        $brands = $this->brandRepository->getAll();
        $branch = $this->getBranch();

        $branchProduct = $branch->products->pluck('id');
        $banks = $branch->banks;

        $product_all = [];
        foreach ($brands as $brand) {
            $products = $this->productRepository->getAll()->where('brand_id', $brand->id)->whereIn('id',
                $branchProduct);

            $table = [
                'brand_id' => $brand->id,
                'brand_name' => $brand->name,
                'all' => $products,
            ];
            $product_all[] = $table;
        }

        $module_name_singular = $this->repository->find($deposit);
        Log::alert('Edit : '.ucfirst($module_parent).'  '.ucfirst($module_action).' By User : '.Auth::user()->name);
        return view($module_view.'.edit',
            compact('module_name', 'module_name_singular', 'module_parent', 'module_icon',
                'page_heading', 'module_action', 'module_view', 'module_route'))
            ->with('carbon', Carbon::now())
            ->with('products', $product_all)
            ->with('banks', $banks)
            ->with('branches', $branch);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  DepositSummary  $deposit
     * @param  Request         $request
     * @return Response
     * @throws Throwable
     */
    public function update(DepositSummary $deposit, Request $request)
    {
        $module_route = $this->module_route;

        $this->repository->update($deposit,
            [
                'data' => $request->except(
                    '_token',
                    '_method'
                ),
            ]);

        return redirect()->route($module_route.'.index')->withFlashSuccess(trans('Record successfully updated'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  Request                   $request
     * @param                            $deposit
     * @return Response
     */
    public function approve(Request $request, $deposit)
    {
        $module_name = $this->module_name;
        $module_name_singular = str_singular($this->module_name);
        $module_action = 'update';

        $$module_name_singular = DepositSummary::findOrFail($deposit);

        $$module_name_singular->update(['approve' => '1', 'approve_by' => access()->user()->id]);

//        Log::info(ucfirst($module_action) . " '$module_name': '" . $$module_name_singular->name . ", ID:" . $$module_name_singular->id . " ' by User:" . access()->user()->name);
        return redirect()->back()->with('flash_success',
            '<i class="fa fa-check"></i> '.ucfirst($module_name_singular).' Approved successfully!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  DepositSummary  $deposit
     * @return Response
     * @throws GeneralException
     */
    public function destroy(DepositSummary $deposit)
    {
        $module_name_singular = str_singular($this->module_name);
        $module_route = $this->module_route;

        $$module_name_singular = $this->repository->delete($deposit);

        return redirect()->route($module_route.'.deleted')->withFlashSuccess(trans('Record successfully deleted'));
    }

    /**
     * @param $deposit
     * @return mixed
     * @throws GeneralException
     */
    public function restore($deposit)
    {
        $module_route = $this->module_route;

        $module_name = DepositSummary::withTrashed()->find($deposit);
        $this->repository->restore($module_name);

        return redirect()->route($module_route.'.index')->withFlashSuccess(trans('Record successfully restored'));
    }

    /**
     * @param $deposit
     * @return mixed
     * @throws GeneralException
     * @throws Throwable
     */
    public function delete($deposit)
    {
        $module_route = $this->module_route;

        $module_name = DepositSummary::withTrashed()->find($deposit);
        $this->repository->forceDelete($module_name);

        return redirect()->route($module_route.'.deleted')->withFlashSuccess(trans('Records deleted permanently'));
    }

    public function fix()
    {
        $nullValues = $this->repository->getModel()->whereHas('details', function ($q) {
            return $q->whereNull('trans_date');
        })->get();

        foreach ($nullValues as $nullValue) {
            $nullValue->details()->update([
                'trans_date' => $nullValue->trans_date
            ]);
        }
    }
}
