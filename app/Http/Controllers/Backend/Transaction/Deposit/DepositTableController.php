<?php

namespace App\Http\Controllers\Backend\Transaction\Deposit;

use App\Http\Controllers\BaseController;
use App\Repositories\Backend\Transaction\Deposit\DepositSummaryRepository;
use Carbon\Carbon;
use Exception;
use Illuminate\Http\Request;
use Yajra\DataTables\Facades\DataTables;


/**
 * Class DepositTableController
 *
 * @package App\Http\Controllers\Backend\Transaction\Deposit
 */
final class DepositTableController extends BaseController
{
    /**
     * @param  DepositSummaryRepository  $repository
     */
    public function __construct(DepositSummaryRepository $repository)
    {
        $this->repository = $repository;
    }

    /**
     * @param  Request  $request
     *
     * @return mixed
     * @throws Exception
     */
    public function __invoke(Request $request)
    {
        return DataTables::of($this->repository->getForDataTable($request->get('status'), $request->get('trashed')))
            ->removeColumn('details')
            ->escapeColumns(['branch_name'])
            ->editColumn('sl', '<span style="font-weight: bold; color: #0d89ed;">{{ $sl }}</span>')
            ->editColumn('trans_date', function ($q) {
                return '<span style="font-weight: bold;">'.Carbon::parse($q->trans_date)->format('d-M-Y').'</span>';
            })
            ->addColumn('amount', function ($q) {
                return number_format($q->details->sum('amount'), 2);
            })
            ->addColumn('actions', function ($q) {
                return $q->action_buttons;
            })
            ->setRowClass(function ($q) {
                return $q->isApprove() ? 'bg-success-light' : '';
            })
            ->order(function ($query) {
                $query->orderBy('trans_date', 'desc');
                $query->orderBy('sl', 'asc');
            })
            //   ->rawColumns(['sl','trans_date'])
            ->make(true);
    }
}
