<?php

namespace App\Http\Controllers\Backend\Transaction\OutstandingReconciliation;


use App\Exceptions\GeneralException;
use App\Http\Controllers\BaseController;
use App\Models\OutstandingReconciliation\OutstandingReconciliationSummary;
use App\Repositories\Backend\Setup\Brand\BrandRepository;
use App\Repositories\Backend\Setup\Product\ProductRepository;
use App\Repositories\Backend\Transaction\OutstandingReconciliation\OutstandingReconciliationSummaryRepository;
use Auth;
use Carbon\Carbon;
use Illuminate\Contracts\View\Factory;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\View\View;
use Log;
use Throwable;


/**
 * Class OutstandingReconciliationController
 *
 * @package App\Http\Controllers\Backend\Transaction\OutstandingReconciliation
 */
final class OutstandingReconciliationController extends BaseController
{

    /**
     * @var ProductRepository
     */
    /**
     * @var BrandRepository|ProductRepository
     */
    private $productRepository, $brandRepository;

    /**
     * OutstandingReconciliationController constructor.
     *
     * @param  OutstandingReconciliationSummaryRepository  $repository
     * @param  ProductRepository                           $productRepository
     * @param  BrandRepository                             $brandRepository
     */
    public function __construct(
        OutstandingReconciliationSummaryRepository $repository,
        ProductRepository $productRepository,
        BrandRepository $brandRepository
    ) {
        $this->repository = $repository;
        $this->productRepository = $productRepository;
        $this->brandRepository = $brandRepository;

        $this->module_parent = 'Transaction :: Outstanding Reconciliation';
        $this->module_name = 'outstandingreconciliations';
        $this->module_icon = 'mdi-hospital-building';
        $this->module_route = 'admin.transaction.'.$this->module_name;
        $this->module_view = 'backend.transaction.'.$this->module_name;
    }

    /**
     * @return Factory|View
     */
    public function index()
    {
        $module_parent = $this->module_parent;
        $module_name = $this->module_name;
        $module_name_singular = str_singular($this->module_name);
        $module_icon = $this->module_icon;
        $module_action = 'Index';
        $module_route = $this->module_route;
        $module_view = $this->module_view;

        $page_heading = $this->module_parent.' - View All';

        Log::alert('Browse : '.ucfirst($module_parent).'  '.ucfirst($module_action).' By User : '.Auth::user()->name);
        return view($module_view.'.index',
            compact('module_name', $module_name, 'module_name_singular', 'module_parent', 'module_icon', 'page_heading',
                'module_action', 'module_view', 'module_route'))
            ->with('carbon', Carbon::now());
    }

    /**
     * @return $this
     * @throws GeneralException
     */
    public function create()
    {
        $module_parent = $this->module_parent;
        $module_name = $this->module_name;
        $module_name_singular = str_singular($this->module_name);
        $module_icon = $this->module_icon;
        $module_action = 'Create';
        $module_route = $this->module_route;
        $module_view = $this->module_view;

        $page_heading = $this->module_parent.' - Add New';

        $brands = $this->brandRepository->getAll();

        $branch = $this->getBranches()->first();

        $branchProduct = $branch->products->pluck('id');

        $product_all = [];
        foreach ($brands as $brand) {
            $products = $this->productRepository->getAll()->where('brand_id', $brand->id)->whereIn('id',
                $branchProduct);

            $table = [
                'brand_id' => $brand->id,
                'brand_name' => $brand->name,
                'all' => $products,
            ];
            $product_all[] = $table;
        }


        Log::alert('Create : '.ucfirst($module_parent).'  '.ucfirst($module_action).' By User : '.Auth::user()->name);

        return view($module_view.'.create',
            compact('module_name', 'module_name_singular', 'module_parent', 'module_icon',
                'page_heading', 'module_action', 'module_view', 'module_route'))
            ->with('carbon', Carbon::now())
            ->with('products', $product_all)
            ->with('branches', $branch);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  Request  $request
     * @return Response
     * @throws Throwable
     */
    public function store(Request $request)
    {
        $module_route = $this->module_route;

        $this->repository->create([
            'data' => $request->except(['_token']),
        ]);

        return redirect()->route($module_route.'.index')->withFlashSuccess('Record successfully saved');
    }

    /**
     * Display the specified resource.
     *
     * @param $outstandingreconciliation
     * @return Response
     * @throws GeneralException
     * @internal param int $id
     */
    public function show($outstandingreconciliation)
    {
        $module_parent = $this->module_parent;
        $module_name = $this->module_name;
        $module_name_singular = str_singular($this->module_name);
        $module_icon = $this->module_icon;
        $module_action = 'View';
        $module_route = $this->module_route;
        $module_view = $this->module_view;

        $page_heading = $this->module_parent.' - View';

        $brands = $this->brandRepository->getAll();
        $branch = $this->getBranches()->first();
        $branchProduct = $branch->products->pluck('id');

        $product_all = [];
        foreach ($brands as $brand) {
            $products = $this->productRepository->getAll()->where('brand_id', $brand->id)->whereIn('id',
                $branchProduct);

            $table = [
                'brand_id' => $brand->id,
                'brand_name' => $brand->name,
                'all' => $products,
            ];
            $product_all[] = $table;
        }

        $module_name_singular = $this->repository->find($outstandingreconciliation);

        return view($module_view.'.show',
            compact('module_name', 'module_name_singular', 'module_parent', 'module_icon', 'page_heading',
                'module_action', 'module_view', 'module_route'))
            ->with('partial', 'overview')
            ->with('carbon', Carbon::now())
            ->with('products', $product_all)
            ->with('branches', $branch);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param $outstandingreconciliation
     * @return Response
     * @throws GeneralException
     */
    public function edit($outstandingreconciliation)
    {
        $module_parent = $this->module_parent;
        $module_name = $this->module_name;
        $module_name_singular = str_singular($this->module_name);
        $module_icon = $this->module_icon;
        $module_action = "Save";
        $module_route = $this->module_route;
        $module_view = $this->module_view;

        $page_heading = $this->module_parent.' - Edit';

        $carbon = Carbon::now();
        $current_month = $carbon->month;
        $now = $carbon->format('d-M-Y');

        $brands = $this->brandRepository->getAll();

        $branch = $this->getBranches()->first();

        $branchProduct = $this->getBranch()->products->pluck('id');

        foreach ($brands as $brand) {
            $products = $this->productRepository->getAll()->where('brand_id', $brand->id)->whereIn('id',
                $branchProduct);

            $table = [
                'brand_id' => $brand->id,
                'brand_name' => $brand->name,
                'all' => $products,
            ];
            $product_all[] = $table;
        }

        $module_name_singular = $this->repository->find($outstandingreconciliation);

        Log::alert('Edit : '.ucfirst($module_parent).'  '.ucfirst($module_action).' By User : '.Auth::user()->name);
        return view($module_view.'.edit',
            compact('module_name', 'module_name_singular', 'module_parent', 'module_icon', 'brand',
                'page_heading', 'module_action', 'module_view', 'module_route', 'now'))
            ->with('carbon', $carbon)
            ->with('products', $product_all)
            ->with('branches', $branch);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  OutstandingReconciliationSummary  $outstandingreconciliation
     * @param  Request                           $request
     * @return Response
     * @throws Throwable
     */
    public function update(OutstandingReconciliationSummary $outstandingreconciliation, Request $request)
    {
        $module_route = $this->module_route;


        $this->repository->update($outstandingreconciliation,
            [
                'data' => $request->except(
                    '_token',
                    '_method'
                ),
            ]);

        return redirect()->route($module_route.'.index')->withFlashSuccess(trans('Record successfully updated'));
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  Request  $request
     * @param           $OutstandingReconciliation
     * @return Response
     */
    public function approve(Request $request, $OutstandingReconciliation)
    {
        $module_name = $this->module_name;
        $module_name_singular = str_singular($this->module_name);
        $module_action = "update";

        $$module_name_singular = OutstandingReconciliationSummary::findOrFail($OutstandingReconciliation);

        $$module_name_singular->update(['approve' => '1', 'approve_by' => access()->user()->id]);

//        Log::info(ucfirst($module_action) . " '$module_name': '" . $$module_name_singular->name . ", ID:" . $$module_name_singular->id . " ' by User:" . access()->user()->name);
        return redirect()->back()->with('flash_success',
            '<i class="fa fa-check"></i> '.ucfirst($module_name_singular).' Approved successfully!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy(OutstandingReconciliationSummaryRepository $OutstandingReconciliation)
    {
        $module_name_singular = str_singular($this->module_name);
        $module_route = $this->module_route;

        $$module_name_singular = $this->repository->delete($OutstandingReconciliation);

        return redirect()->route($module_route.".deleted")->withFlashSuccess(trans('Record successfully deleted'));
    }

    /**
     * @param           $officer
     * @param  Request  $request
     * @return mixed
     * @throws GeneralException
     */
    public function restore($OutstandingReconciliation)
    {
        $module_route = $this->module_route;

        $module_name = OutstandingReconciliationSummaryRepository::withTrashed()->find($OutstandingReconciliation);
        $this->repository->restore($module_name);

        return redirect()->route($module_route.'.index')->withFlashSuccess(trans('Record successfully restored'));
    }

    /**
     * @param $OutstandingReconciliation
     * @return mixed
     * @throws GeneralException
     * @throws Throwable
     */
    public function delete($OutstandingReconciliation)
    {
        $module_route = $this->module_route;

        $module_name = OutstandingReconciliationSummaryRepository::withTrashed()->find($OutstandingReconciliation);
        $this->repository->forceDelete($module_name);

        return redirect()->route($module_route.'.deleted')->withFlashSuccess(trans('Records deleted permanently'));
    }
}
