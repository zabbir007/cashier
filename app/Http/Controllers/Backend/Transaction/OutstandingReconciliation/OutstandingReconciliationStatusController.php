<?php

namespace App\Http\Controllers\Backend\Transaction\OutstandingReconciliation;

use App\Http\Controllers\Controller;
use App\Repositories\Backend\Transaction\OutstandingReconciliation\OutstandingReconciliationSummaryRepository;
use Illuminate\Contracts\Auth\Authenticatable;
use Illuminate\Contracts\View\Factory;
use Illuminate\Http\Request;
use Illuminate\View\View;


/**
 * Class OutstandingReconciliationStatusController
 *
 * @package App\Http\Controllers\Backend\Transaction\OutstandingReconciliation
 */
final class OutstandingReconciliationStatusController extends Controller
{
    /**
     * @var OutstandingReconciliationSummaryRepository
     */
    /**
     * @var OutstandingReconciliationSummaryRepository|string
     */
    /**
     * @var OutstandingReconciliationSummaryRepository|string
     */
    /**
     * @var OutstandingReconciliationSummaryRepository|string
     */
    /**
     * @var OutstandingReconciliationSummaryRepository|string
     */
    /**
     * @var OutstandingReconciliationSummaryRepository|Authenticatable|string|null
     */
    /**
     * @var OutstandingReconciliationSummaryRepository|Authenticatable|string|null
     */
    /**
     * @var OutstandingReconciliationSummaryRepository|Authenticatable|string|null
     */
    private $repository, $module_parent, $module_name, $module_icon, $module_action, $user, $module_route, $module_view;

    /**
     * BranchStatusController constructor.
     *
     * @param  BranchRepository  $repository
     */
    public function __construct()
    {
//        $this->repository = $repository;
        $this->repository = new OutstandingReconciliationSummaryRepository();
        $this->user = access()->user();
        $this->module_action = "";
        $this->module_parent = "Transaction Date Wise";
        $this->module_name = 'outstandingreconciliations';
        $this->module_icon = 'mdi-hospital-building';
        $this->module_route = "admin.transaction.".$this->module_name;
        $this->module_view = "backend.transaction.".$this->module_name;
    }

    /**
     * @param  Branch   $branch
     * @param           $status
     * @param  Request  $request
     * @return mixed
     */
    public function mark(Belt $belt, $status, Request $request)
    {
        $module_name = $this->module_name;
        $module_name_singular = str_singular($this->module_name);
        $module_action = __FUNCTION__;
        $module_route = $this->module_route;
        $module_view = $this->module_view;

        $this->repository->mark($belt, $status);

        return redirect()->route($status == 1 ? $module_route.".index" : $module_route.".deactivated")->withFlashSuccess(trans('Records updated successfully'));
    }

    /**
     * @param  Request  $request
     * @return Factory|View
     */
    public function getDeactivated(Request $request)
    {
        $module_name = $this->module_name;
        $module_name_singular = str_singular($this->module_name);
        $module_action = 'Deactivated';
        $module_route = $this->module_route;
        $module_view = $this->module_view;

        $page_heading = $this->module_parent." | Deactivated Belts";

        $$module_name = $this->repository->getDeactivated();

        return view($module_view.".deactivated",
            compact('module_name', "$module_name", 'module_name_singular', 'module_icon', 'page_heading',
                'module_action', 'module_route', 'module_view'));
    }

    /**
     * @return Factory|View
     */
    public function trashed()
    {
        $module_name = $this->module_name;
        $module_name_singular = str_singular($this->module_name);
        $module_action = 'Deleted';
        $module_route = $this->module_route;
        $module_view = $this->module_view;

        $page_heading = $this->module_parent." | Deleted Belts";

        $$module_name = $this->repository->getDeleted();

        return view($module_view.".deleted",
            compact('module_name', "$module_name", "module_name_singular", 'module_icon', 'page_heading',
                'module_action', 'module_route', 'module_view'));

    }
}
