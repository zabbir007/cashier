<?php

namespace App\Http\Controllers\Backend\Transaction\OutstandingReconciliation;

use App\Http\Controllers\BaseController;
use App\Repositories\Backend\Transaction\OutstandingReconciliation\OutstandingReconciliationSummaryRepository;
use Carbon\Carbon;
use Exception;
use Illuminate\Http\Request;
use Yajra\DataTables\Facades\DataTables;


/**
 * Class OutstandingReconciliationTableController
 *
 * @package App\Http\Controllers\Backend\Transaction\OutstandingReconciliation
 */
final class OutstandingReconciliationTableController extends BaseController
{
    /**
     * OutstandingReconciliationTableController constructor.
     *
     * @param  OutstandingReconciliationSummaryRepository  $repository
     */
    public function __construct(OutstandingReconciliationSummaryRepository $repository)
    {
        $this->repository = $repository;
    }

    /**
     * @param  Request  $request
     *
     * @return mixed
     * @throws Exception
     */
    public function __invoke(Request $request)
    {
        return DataTables::of($this->repository->getForDataTable($request->get('status'), $request->get('trashed')))
            ->removeColumn('details')
            ->escapeColumns(['branch_name'])
            ->editColumn('sl', '<span style="font-weight: bold; color: #0d89ed;">{{ $sl }}</span>')
            ->editColumn('trans_date', function ($q) {
                return '<span style="font-weight: bold;">'.Carbon::parse($q->trans_date)->format('d-M-Y').'</span>';
            })
            ->addColumn('sales', function ($q) {
                $total = $q->details->sum('sales_tp') + $q->details->sum('sales_vat') - $q->details->sum('sales_discount') - $q->details->sum('sales_sp_disc');

                return number_format($total, 2);
            })
            ->addColumn('returns', function ($q) {
                $total = $q->details->sum('return_tp') + $q->details->sum('return_vat') - $q->details->sum('return_discount') - $q->details->sum('return_sp_disc');
                return number_format($total, 2);
            })
            ->addColumn('net_sales', function ($q) {
                $salesTotal = $q->details->sum('sales_tp') + $q->details->sum('sales_vat') - $q->details->sum('sales_discount') - $q->details->sum('sales_sp_disc');
                $returnTotal = $q->details->sum('return_tp') + $q->details->sum('return_vat') - $q->details->sum('return_discount') - $q->details->sum('return_sp_disc');

                return number_format($salesTotal - $returnTotal, 2);
            })
            ->addColumn('actions', function ($q) {
                return $q->action_buttons;
            })
            ->setRowClass(function ($q) {
                return $q->isApprove() ? 'bg-success-light' : '';
            })
            ->order(function ($query) {
                $query->orderBy('trans_date', 'desc');
                $query->orderBy('sl', 'asc');
            })
            //   ->rawColumns(['sl','trans_date'])
            ->make(true);
    }
}
