<?php

namespace App\Http\Controllers\Backend\Excle;

use App\Http\Controllers\Controller;
use App\Imports\ExcelImport;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;

class ImportExcleController extends Controller
{
    function index()
    {

    }

    function import(Request $request)
    {

        $this->validate($request, [
            'select_file'  => 'required|mimes:xls,xlsx'
        ]);
        $selectedBranchCode=$request->selectedBranchCode;
        $selectedBranchId=$request->selectedBranchId;
        $selectedBankId=$request->selectedBankId;
        $selectedAccount=$request->selectedAccount;
        $selectedYear=$request->selectedYear;
        $selectedMonth=$request->selectedMonth;
        $selectedYearMonth=$request->selectedYearMonth;
        $created_by=$request->created_by;
        $created_at=$request->created_at;
        $trans_date=$request->trans_date;

        if($request->hasFile('select_file')) {
            $file=$request->select_file;
            Excel::import(new ExcelImport($selectedBranchCode,$selectedBranchId,$selectedBankId,$selectedAccount,$selectedYear,$selectedMonth,$selectedYearMonth,$created_by,$created_at,$trans_date),$file);
//            echo "insert successfully";


        }

    }
}
