<?php

namespace App\Http\Controllers\Backend\System;

use App\Http\Controllers\BaseController;
use Artisan;

/**
 * Class SystemController
 *
 * @package App\Http\Controllers\Backend\System\System
 */
final class SystemCommandController extends BaseController
{

    /**
     * @return mixed
     */
    public function clearAll()
    {
        Artisan::call('cache:clear');
        Artisan::call('route:clear');
        Artisan::call('view:clear');
        Artisan::call('config:clear');
        Artisan::call('debugbar:clear');

        return redirect()->intended()->withFlashSuccess(Artisan::output());
    }

    /**
     * @return mixed
     */
    public function cacheAll()
    {
        Artisan::call('config:cache');
        Artisan::call('route:cache');

        return redirect()->intended()->withFlashSuccess(Artisan::output());
    }

    /**
     * @return mixed
     */
    public function clearCache()
    {
        $output = Artisan::call('cache:clear');
        if ($output === 0) {
            return redirect()->intended()->withFlashSuccess(Artisan::output());
        }

        return redirect()->intended()->withFlashWarning('Failed to run command!');
    }

    /**
     * @return mixed
     */
    public function clearRoutes()
    {
        $output = Artisan::call('route:clear');
        if ($output === 0) {
            return redirect()->intended()->withFlashSuccess(Artisan::output());
        }

        return redirect()->intended()->withFlashWarning('Failed to run command!');
    }

    /**
     * @return mixed
     */
    public function clearViews()
    {
        $output = Artisan::call('view:clear');
        if ($output === 0) {
            return redirect()->intended()->withFlashSuccess(Artisan::output());
        }

        return redirect()->intended()->withFlashWarning('Failed to run command!');
    }

    /**
     * @return mixed
     */
    public function clearConfigs()
    {
        $output = Artisan::call('config:clear');
        if ($output === 0) {
            return redirect()->intended()->withFlashSuccess(Artisan::output());
        }

        return redirect()->intended()->withFlashWarning('Failed to run command!');
    }

    /**
     * @return mixed
     */
    public function clearDebugbar()
    {
        $output = Artisan::call('debugbar:clear');
        if ($output === 0) {
            return redirect()->intended()->withFlashSuccess(Artisan::output());
        }

        return redirect()->intended()->withFlashWarning('Failed to run command!');
    }

    /**
     * @return mixed
     */
    public function cacheConfigs()
    {
        $output = Artisan::call('config:cache');
        if ($output === 0) {
            return redirect()->intended()->withFlashSuccess(Artisan::output());
        }

        return redirect()->intended()->withFlashWarning('Failed to run command!');
    }

    /**
     * @return mixed
     */
    public function cacheRoutes()
    {
        $output = Artisan::call('route:cache');
        if ($output === 0) {
            return redirect()->intended()->withFlashSuccess(Artisan::output());
        }

        return redirect()->intended()->withFlashWarning('Failed to run command!');
    }

}
