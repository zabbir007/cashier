<?php

namespace App\Http\Controllers\Backend\System\LoggedUser;

use App\Http\Controllers\BaseController;
use App\Repositories\Backend\Access\User\UserRepository;
use Exception;
use Illuminate\Http\Request;
use Yajra\DataTables\Facades\DataTables;

/**
 * Class TransactionSettingTableController
 *
 * @package App\Http\Controllers\Backend\System\TransactionSetting
 */
final class LoggedUserTableController extends BaseController
{

    /**
     * LoggedUserTableController constructor.
     *
     * @param  UserRepository  $repository
     */
    public function __construct(UserRepository $repository)
    {
        $this->repository = $repository;
    }

    /**
     * @param  Request  $request
     *
     * @return mixed
     * @throws Exception
     */
    public function __invoke(Request $request)
    {
        return DataTables::of($this->repository->getLoggedUserForDataTable($request->get('status'),
            $request->get('trashed')))
            ->escapeColumns(['first_name', 'last_name', 'email', 'username'])
            ->editColumn('full_name', function ($q) {
                return $q->full_name;
            })
            ->addColumn('roles', function ($q) {
                return $q->roles->count() ?
                    implode('<br/>', $q->roles->pluck('name')->toArray()) :
                    trans('labels.general.none');
            })
            ->addColumn('branches', function ($q) {
                $total_branch = $q->branches->count();

                if ($total_branch > 5) {
                    return 'All';
                }
                if ($total_branch == 0) {
                    return 'N/A';
                }

                return implode('<br/>', $q->branches->pluck('name')->toArray());
            })
            ->editColumn('is_logged', function ($q) {
                return $q->isLogged() ? 'Y' : 'N';
            })
            ->editColumn('last_login_at', function ($q) {
                return $q->last_login_at != null ? $q->last_login_at->diffForHumans() : 'N/A';
            })
//            ->editColumn('updated_at', function ($q) {
//                return $q->updated_at->diffForHumans();
//            })
            ->addColumn('actions', function ($q) {
                return $q->action_buttons;
            })
            ->setRowClass(function ($q) {
                return !$q->isLogged() ? '' : '';
            })
            ->make(true);
    }
}
