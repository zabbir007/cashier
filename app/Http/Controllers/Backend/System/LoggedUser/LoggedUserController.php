<?php

namespace App\Http\Controllers\Backend\System\LoggedUser;

use App\Exceptions\GeneralException;
use App\Http\Controllers\BaseController;
use App\Repositories\Backend\Access\User\UserRepository;
use Illuminate\Contracts\View\Factory;
use Illuminate\Http\Response;
use Illuminate\Support\Str;
use Illuminate\View\View;

/**
 * Class TransactionSettingController
 *
 * @package App\Http\Controllers\Backend\TransactionSetting
 */
final class LoggedUserController extends BaseController
{

    /**
     * LoggedUserController constructor.
     *
     * @param  UserRepository  $repository
     */
    public function __construct(UserRepository $repository)
    {
        $this->repository = $repository;

        $this->module_action = '';
        $this->module_parent = 'System';
        $this->module_name = 'logged_users';
        $this->module_icon = 'mdi-hospital-building';
        $this->module_route = 'admin.system.'.$this->module_name;
        $this->module_view = 'backend.system.'.$this->module_name;
    }

    /**
     * @return Factory|View
     * @throws GeneralException
     */
    public function index()
    {
        $module_parent = $this->module_parent;
        $module_name = $this->module_name;
        $module_name_singular = str_singular($this->module_name);
        $module_icon = $this->module_icon;
        $module_action = 'index';
        $module_route = $this->module_route;
        $module_view = $this->module_view;

        $page_heading = $this->module_parent.' Transaction Settings - View All';

//        $module_name_singular = DataTables::eloquent($this->repository->getForDataTable())
//            ->addIndexColumn()
//            ->addColumn('branch_name', function ($q) {
//                return $q->branch->name;
//            })
//            ->addColumn('user_name', function ($q) {
//                return $q->user->first_name.' '.$q->user->last_name;
//            })
//            ->addColumn('allowed_days', function ($q) {
//                return ($q->isPrevious() ? 'Previous' : 'Next').' '.$q->interval.' '.($q->day == 'd' ? 'Days' : 'Months');
//            })
//            ->editColumn('expire_at', function ($q) {
//                return $q->expire_at->format('d M Y h:i a');
//            })
//            ->addColumn('expire_label', function ($q) {
//                return $q->expire_label;
//            })
//            ->addColumn('actions', function ($q) {
//                return $q->action_buttons;
//            })
//            ->setRowClass(function ($q) {
//                return $q->isExpired() ? 'bg-danger-light' : 'bg-success-light';
//            })
//            ->rawColumns(['actions', 'expire_label'])
//            ->make();

        $module_name_singular = $this->repository->getAll();

        return view($module_view.'.'.$module_action,
            compact('module_name', $module_name, 'module_name_singular', 'module_parent', 'module_icon', 'page_heading',
                'module_action', 'module_view', 'module_route'));
    }

    /**
     * Display the specified resource.
     *
     * @param $id
     * @return Response
     * @internal param int $id
     */
    public function show($id)
    {
        $module_parent = $this->module_parent;
        $module_name = $this->module_name;
        $module_name_singular = Str::singular($this->module_name);
        $module_icon = $this->module_icon;
        $module_action = 'show';
        $module_route = $this->module_route;
        $module_view = $this->module_view;

        $page_heading = $this->module_parent.' - View';

        $$module_name_singular = $this->repository->find($id);

        return view($module_view.'.show',
            compact('module_name', $module_name_singular, 'module_name_singular', 'module_parent', 'module_icon',
                'page_heading', 'module_action', 'module_view', 'module_route'));
    }
}
