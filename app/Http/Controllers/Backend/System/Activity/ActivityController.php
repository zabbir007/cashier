<?php

namespace App\Http\Controllers\Backend\System\Activity;

use App\Http\Controllers\BaseController;
use App\Repositories\Backend\System\Activity\ActivityRepository;
use Illuminate\Contracts\View\Factory;
use Illuminate\Http\Request;
use Illuminate\View\View;

/**
 * Class ActivityController
 *
 * @package App\Http\Controllers\Backend\System\Activity
 */
final class ActivityController extends BaseController
{
    /**
     * ActivityController constructor.
     *
     * @param  ActivityRepository  $repository
     */
    public function __construct(ActivityRepository $repository)
    {
        $this->repository = $repository;

        $this->module_action = '';
        $this->module_parent = 'System Management';
        $this->module_name = 'activity_log';
        $this->module_icon = 'mdi-hospital-building';
        $this->module_route = 'admin.system.'.$this->module_name;
        $this->module_view = 'backend.system.'.$this->module_name;

    }

    /**
     * @return Factory|View
     */
    public function index()
    {
        $module_parent = $this->module_parent;
        $module_name = $this->module_name;
        $module_name_singular = str_singular($this->module_name);
        $module_icon = $this->module_icon;
        $module_action = 'index';
        $module_route = $this->module_route;
        $module_view = $this->module_view;

        $page_heading = 'Activity Logs';

        $logs = $this->repository->getPaginated();

        return view($module_view.'.'.$module_action,
            compact('module_name','module_name_singular', 'module_parent', 'module_icon', 'page_heading',
                'module_action', 'module_view', 'module_route'))
            ->with('logs', $logs);
    }


    /**
     * @param           $id
     * @param  Request  $request
     * @return Factory|View
     */
    public function show($id, Request $request)
    {
        $module_parent = $this->module_parent;
        $module_name = $this->module_name;
        $module_name_singular = str_singular($this->module_name);
        $module_icon = $this->module_icon;
        $module_action = 'show';
        $module_route = $this->module_route;
        $module_view = $this->module_view;

        $page_heading = 'Activity Log Details';

        $$module_name_singular = $this->repository->show($id);

        return view($module_view.'.'.$module_action,
            compact('module_name', 'module_name_singular', $module_name, $module_name_singular, 'module_parent',
                'module_icon', 'page_heading', 'module_action', 'module_view', 'module_route'))
            ->with('revision_history', $this->repository->getRevisionHistory($id))
            ->with('revision_history_by_causer',
                $this->repository->getRevisionHistoryByCauser($id, $$module_name_singular->causer->id));
    }
}
