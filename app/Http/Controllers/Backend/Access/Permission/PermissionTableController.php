<?php

namespace App\Http\Controllers\Backend\Access\Permission;

use App\Http\Controllers\Controller;
use App\Http\Requests\Backend\Access\Permission\ManagePermissionRequest;
use App\Repositories\Backend\Access\Permission\PermissionRepository;
use Exception;
use Yajra\DataTables\Facades\DataTables;

/**
 * Class PermissionTableController
 *
 * @package App\Http\Controllers\Backend\Access\Permission
 */
final class PermissionTableController extends Controller
{
    /**
     * @var PermissionRepository
     */
    protected $permissions;

    /**
     * @param  PermissionRepository  $permissions
     */
    public function __construct(PermissionRepository $permissions)
    {
        $this->permissions = $permissions;
    }

    /**
     * @param  ManagePermissionRequest  $request
     *
     * @return mixed
     * @throws Exception
     */
    public function __invoke(ManagePermissionRequest $request)
    {
        return DataTables::of($this->permissions->getForDataTable())
            ->escapeColumns(['name', 'display_name'])
//            ->addColumn('permissions', function ($role) {
//                if ($role->all) {
//                    return '<span class="label label-success">'.trans('labels.general.all').'</span>';
//                }
//
//                return $role->permissions->count() ?
//                    implode('<br/>', $role->permissions->pluck('display_name')->toArray()) :
//                    '<span class="label label-danger">'.trans('labels.general.none').'</span>';
//            })
//            ->addColumn('users', function ($role) {
//                return $role->users->count();
//            })
            ->addColumn('actions', function ($permission) {
                return $permission->action_buttons;
            })
            ->make(true);
    }
}
