<?php

namespace App\Http\Controllers\Backend\Access\Permission;

use App\Exceptions\GeneralException;
use App\Http\Controllers\BaseController;
use App\Http\Requests\Backend\Access\Permission\ManagePermissionRequest;
use App\Http\Requests\Backend\Access\Permission\StorePermissionRequest;
use App\Http\Requests\Backend\Access\Permission\UpdatePermissionRequest;
use App\Models\Access\Permission\Permission;
use App\Repositories\Backend\Access\Permission\PermissionRepository;
use Throwable;

/**
 * Class PermissionController
 *
 * @package App\Http\Controllers\Backend\Access\Permission
 */
final class PermissionController extends BaseController
{
    /**
     * @var PermissionRepository
     */
    protected $permissions;

    /**
     * @param  PermissionRepository  $permissions
     */
    public function __construct(PermissionRepository $permissions)
    {
        $this->permissions = $permissions;
    }

    /**
     * @param  ManagePermissionRequest  $request
     *
     * @return mixed
     */
    public function index(ManagePermissionRequest $request)
    {
        return view('backend.access.permissions.index');
    }

    /**
     * @param  ManagePermissionRequest  $request
     *
     * @return mixed
     */
    public function create(ManagePermissionRequest $request)
    {
        return view('backend.access.permissions.create');
    }

    /**
     * @param  StorePermissionRequest  $request
     *
     * @return mixed
     * @throws GeneralException
     * @throws Throwable
     */
    public function store(StorePermissionRequest $request)
    {
        $this->permissions->create($request->only('display_name'));

        return redirect()->route('admin.access.permission.index')->withFlashSuccess(trans('alerts.backend.permissions.created'));
    }

    /**
     * @param  Permission               $permission
     * @param  ManagePermissionRequest  $request
     *
     * @return mixed
     */
    public function edit(Permission $permission, ManagePermissionRequest $request)
    {
        return view('backend.access.permissions.edit')
            ->with('permission', $permission);
    }

    /**
     * @param  Permission               $permission
     * @param  UpdatePermissionRequest  $request
     *
     * @return mixed
     * @throws Throwable
     */
    public function update(Permission $permission, UpdatePermissionRequest $request)
    {
        $this->permissions->update($permission, $request->only('name', 'display_name'));

        return redirect()->route('admin.access.permission.index')->withFlashSuccess(trans('alerts.backend.permissions.updated'));
    }

    /**
     * @param  Permission               $permission
     * @param  ManagePermissionRequest  $request
     *
     * @return mixed
     * @throws GeneralException
     */
    public function destroy(Permission $permission, ManagePermissionRequest $request)
    {
        $this->permissions->delete($permission);

        return redirect()->route('admin.access.role.index')->withFlashSuccess(trans('alerts.backend.roles.deleted'));
    }
}
