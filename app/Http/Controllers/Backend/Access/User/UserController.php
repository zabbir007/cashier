<?php

namespace App\Http\Controllers\Backend\Access\User;

use App\Exceptions\GeneralException;
use App\Http\Controllers\BaseController;
use App\Http\Requests\Backend\Access\User\ManageUserRequest;
use App\Http\Requests\Backend\Access\User\StoreUserRequest;
use App\Http\Requests\Backend\Access\User\UpdateUserRequest;
use App\Models\Access\User\User;
use App\Models\Branch\Branch;
use App\Repositories\Backend\Access\Role\RoleRepository;
use App\Repositories\Backend\Access\User\UserRepository;
use Illuminate\Contracts\View\Factory;
use Illuminate\View\View;
use Throwable;

/**
 * Class UserController.
 */
final class UserController extends BaseController
{
    /**
     * @var UserRepository
     */
    protected $users;

    /**
     * @var RoleRepository
     */
    protected $roles;

    /**
     * @param  UserRepository  $users
     * @param  RoleRepository  $roles
     */
    public function __construct(UserRepository $users, RoleRepository $roles)
    {
        $this->module_parent = 'User';
        $this->module_name = str_plural($this->module_parent);
        $this->users = $users;
        $this->roles = $roles;
    }

    /**
     * @param  ManageUserRequest  $request
     *
     * @return Factory|View
     */
    public function index(ManageUserRequest $request)
    {
        $module_name = $this->module_name;
        $module_name_singular = str_singular($this->module_name);
        return view('backend.access.index', compact($module_name, $module_name_singular, 'module_name'));
    }

    /**
     * @param  ManageUserRequest  $request
     *
     * @return mixed
     */
    public function create(ManageUserRequest $request)
    {
        $module_name = $this->module_name;
        $module_name_singular = str_singular($this->module_name);
        $branches = Branch::pluck('name', 'id');
        return view('backend.access.create', compact($module_name, $module_name_singular, 'module_name', 'branches'))
            ->withRoles($this->roles->getAll());
    }

    /**
     * @param  StoreUserRequest  $request
     *
     * @return mixed
     * @throws GeneralException
     * @throws Throwable
     */
    public function store(StoreUserRequest $request)
    {
        $this->users->create(
            [
                'data' => $request->only(
                    'first_name',
                    'last_name',
                    'email',
                    'username',
                    'password',
                    'status',
                    'confirmed',
                    'confirmation_email'
                ),
                'roles' => $request->only('assignees_roles'),
                'branches' => $request->only('branch_list'),
            ]);

        return redirect()->route('admin.access.user.index')->withFlashSuccess(trans('alerts.backend.users.created'));
    }

    /**
     * @param  User               $user
     * @param  ManageUserRequest  $request
     *
     * @return mixed
     */
    public function show(User $user, ManageUserRequest $request)
    {
        return view('backend.access.show')
            ->withUser($user);
    }

    /**
     * @param  User               $user
     * @param  ManageUserRequest  $request
     *
     * @return mixed
     */
    public function edit(User $user, ManageUserRequest $request)
    {
//        dd($user->load('branches'));
        $branches = Branch::pluck('name', 'id');

        return view('backend.access.edit', compact('branches'))
            ->withUser($user)
            ->withUserRoles($user->roles->pluck('id')->all())
            ->withRoles($this->roles->getAll());
    }

    /**
     * @param  User               $user
     * @param  UpdateUserRequest  $request
     *
     * @return mixed
     * @throws GeneralException
     * @throws Throwable
     */
    public function update(User $user, UpdateUserRequest $request)
    {
        $this->users->update($user,
            [
                'data' => $request->only(
                    'first_name',
                    'last_name',
                    'username',
                    'email',
                    'status',
                    'confirmed'
                ),
                'roles' => $request->only('assignees_roles'),
                'branches' => $request->only('branch_list'),
            ]);

        return redirect()->route('admin.access.user.index')->withFlashSuccess(trans('alerts.backend.users.updated'));
    }

    /**
     * @param  User               $user
     * @param  ManageUserRequest  $request
     *
     * @return mixed
     * @throws GeneralException
     */
    public function destroy(User $user, ManageUserRequest $request)
    {
        $this->users->delete($user);

        return redirect()->route('admin.access.user.deleted')->withFlashSuccess(trans('alerts.backend.users.deleted'));
    }
}
