<?php

namespace App\Http\Controllers\Backend\Access\User;

use App\Http\Controllers\Controller;
use App\Http\Requests\Backend\Access\User\ManageUserRequest;
use App\Repositories\Backend\Access\User\UserRepository;
use Exception;
use Yajra\DataTables\Facades\DataTables;

/**
 * Class UserTableController.
 */
final class UserTableController extends Controller
{
    /**
     * @var UserRepository
     */
    protected $qs;

    /**
     * @param  UserRepository  $qs
     */
    public function __construct(UserRepository $qs)
    {
        $this->users = $qs;
    }

    /**
     * @param  ManageUserRequest  $request
     *
     * @return mixed
     * @throws Exception
     */
    public function __invoke(ManageUserRequest $request)
    {
        return DataTables::of($this->users->getForDataTable($request->get('status'), $request->get('trashed')))
            ->escapeColumns(['first_name', 'last_name', 'email', 'username'])
            ->editColumn('full_name', function ($q) {
                return $q->full_name;
            })
            ->editColumn('confirmed', function ($q) {
                return $q->confirmed_label;
            })
            ->addColumn('roles', function ($q) {
                return $q->roles->count() ?
                    implode('<br/>', $q->roles->pluck('name')->toArray()) :
                    trans('labels.general.none');
            })
//            ->addColumn('social', function ($q) {
//                $accounts = [];
//
//                // Note: If you have a provider that does not have a corresponding
//                // font-awesome icon, you can override it here
//                foreach ($q->providers as $social) {
//                    $accounts[] = '<a href="' . route('admin.access.user.social.unlink',
//                            [$q, $social]) . '" data-toggle="tooltip" data-placement="top" title="' . trans('buttons.backend.access.users.unlink') . '" data-method="delete"><i class="fa fa-' . $social->provider . '"></i></a>';
//                }
//
//                return count($accounts) ? implode(' ', $accounts) : 'None';
//            })
            ->addColumn('branches', function ($q) {
                $total_branch = $q->branches->count();

                if ($total_branch > 5) {
                    return 'All';
                }
                if ($total_branch == 0) {
                    return 'N/A';
                }

                return implode('<br/>', $q->branches->pluck('name')->toArray());
            })
            ->editColumn('is_logged', function ($q) {
                return $q->isLogged() ? 'Y' : 'N';
            })
            ->editColumn('last_login_at', function ($q) {
                return $q->last_login_at != null ? $q->last_login_at->diffForHumans() : 'N/A';
            })
//            ->editColumn('updated_at', function ($q) {
//                return $q->updated_at->diffForHumans();
//            })
            ->addColumn('actions', function ($q) {
                return $q->action_buttons;
            })
            ->setRowClass(function ($q) {
                return !$q->isLogged() ? '' : 'bg-success';
            })
            //->removeColumn(['roles','branches'])
            ->make(true);
    }
}
