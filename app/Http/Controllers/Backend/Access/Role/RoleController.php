<?php

namespace App\Http\Controllers\Backend\Access\Role;

use App\Exceptions\GeneralException;
use App\Http\Controllers\BaseController;
use App\Http\Requests\Backend\Access\Role\ManageRoleRequest;
use App\Http\Requests\Backend\Access\Role\StoreRoleRequest;
use App\Http\Requests\Backend\Access\Role\UpdateRoleRequest;
use App\Models\Access\Role\Role;
use App\Repositories\Backend\Access\Permission\PermissionRepository;
use App\Repositories\Backend\Access\Role\RoleRepository;
use Throwable;

/**
 * Class RoleController.
 */
final class RoleController extends BaseController
{
    /**
     * @var RoleRepository
     */
    protected $roles;

    /**
     * @var PermissionRepository
     */
    protected $permissions;

    /**
     * @param  RoleRepository        $roles
     * @param  PermissionRepository  $permissions
     */
    public function __construct(RoleRepository $roles, PermissionRepository $permissions)
    {
        $this->roles = $roles;
        $this->permissions = $permissions;
    }

    /**
     * @param  ManageRoleRequest  $request
     *
     * @return mixed
     */
    public function index(ManageRoleRequest $request)
    {
        return view('backend.access.roles.index');
    }

    /**
     * @param  ManageRoleRequest  $request
     *
     * @return mixed
     */
    public function create(ManageRoleRequest $request)
    {
        return view('backend.access.roles.create')
            ->withPermissions($this->permissions->getAll())
            ->withRoles($this->roles->getCount());

    }

    /**
     * @param  StoreRoleRequest  $request
     *
     * @return mixed
     * @throws GeneralException
     * @throws Throwable
     */
    public function store(StoreRoleRequest $request)
    {
        $this->roles->create($request->only('name', 'associated-permissions', 'permissions', 'sort'));

        return redirect()->route('admin.access.role.index')->withFlashSuccess(trans('alerts.backend.roles.created'));
    }

    /**
     * @param  Role               $role
     * @param  ManageRoleRequest  $request
     *
     * @return mixed
     */
    public function edit(Role $role, ManageRoleRequest $request)
    {
//        dd($role->permissions->pluck('id')->all());
        return view('backend.access.roles.edit')
            ->withRole($role)
            ->withRolepermissions($role->permissions->pluck('id')->all())
            ->withPermissions($this->permissions->getAll());
    }

    /**
     * @param  Role               $role
     * @param  UpdateRoleRequest  $request
     *
     * @return mixed
     * @throws GeneralException
     */
    public function update(Role $role, UpdateRoleRequest $request)
    {
        $this->roles->update($role, $request->only('name', 'associated-permissions', 'permissions', 'sort'));

        return redirect()->route('admin.access.role.index')->withFlashSuccess(trans('alerts.backend.roles.updated'));
    }

    /**
     * @param  Role               $role
     * @param  ManageRoleRequest  $request
     *
     * @return mixed
     * @throws GeneralException
     */
    public function destroy(Role $role, ManageRoleRequest $request)
    {
        $this->roles->delete($role);

        return redirect()->route('admin.access.role.index')->withFlashSuccess(trans('alerts.backend.roles.deleted'));
    }
}
