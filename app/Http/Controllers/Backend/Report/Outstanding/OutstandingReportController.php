<?php

namespace App\Http\Controllers\Backend\Report\OutStanding;

use App\Exceptions\GeneralException;
use App\Exports\ReportExport;
use App\Http\Controllers\BaseController;
use App\Repositories\Backend\Setup\Branch\BranchRepository;
use App\Repositories\Backend\Setup\Brand\BrandRepository;
use App\Repositories\Backend\Setup\Opening\OpeningDetailsRepository;
use App\Repositories\Backend\Setup\Opening\OpeningSummaryRepository;
use App\Repositories\Backend\Setup\Product\ProductRepository;
use App\Repositories\Backend\Transaction\DateWise\DateWiseDetailsRepository;
use App\Repositories\Backend\Transaction\DateWise\DateWiseSummaryRepository;
use App\Repositories\Backend\Transaction\OutstandingReconciliation\OutstandingReconciliationDetailsRepository;
use App\Repositories\Backend\Transaction\OutstandingReconciliation\OutstandingReconciliationSummaryRepository;
use Carbon\Carbon;
use Carbon\CarbonInterval;
use Carbon\CarbonPeriod;
use DB;
use Excel;
use Illuminate\Http\Response;
use Illuminate\Support\Collection;

/**
 * Class OutstandingReportController
 *
 * @package App\Http\Controllers\Backend\Report\OutStanding
 */
final class OutstandingReportController extends BaseController
{
    /**
     * @var BrandRepository
     */
    private $brandRepository;
    /**
     * @var ProductRepository
     */
    private $productRepository;
    /**
     * @var BranchRepository
     */
    private $branchRepository;

    /**
     * @var DateWiseSummaryRepository
     */
    private $dateWiseReceiptSummaryRepository;

    /**
     * @var DateWiseDetailsRepository
     */
    private $dateWiseReceiptDetailRepository;

    /**
     * @var OutstandingReconciliationSummaryRepository
     */
    private $outstandingSummaryRepository;

    /**
     * @var OutstandingReconciliationDetailsRepository
     */
    private $outstandingDetailRepository;

    /**
     * @var OpeningSummaryRepository
     */
    private $openingSummaryRepository;

    /**
     * @var OpeningDetailsRepository
     */
    private $openingDetailsRepository;

    /**
     * OutstandingReportController constructor.
     *
     * @param  DateWiseDetailsRepository                   $dateWiseDetailsRepository
     * @param  DateWiseSummaryRepository                   $dateWiseSummaryRepository
     * @param  OutstandingReconciliationSummaryRepository  $outstandingSummaryRepository
     * @param  OutstandingReconciliationDetailsRepository  $outstandingDetailRepository
     * @param  BrandRepository                             $brandRepository
     * @param  ProductRepository                           $productRepository
     * @param  BranchRepository                            $branchRepository
     * @param  OpeningSummaryRepository                    $openingSummaryRepository
     * @param  OpeningDetailsRepository                    $openingDetailsRepository
     */
    public function __construct(
        DateWiseDetailsRepository $dateWiseDetailsRepository,
        DateWiseSummaryRepository $dateWiseSummaryRepository,
        OutstandingReconciliationSummaryRepository $outstandingSummaryRepository,
        OutstandingReconciliationDetailsRepository $outstandingDetailRepository,
        BrandRepository $brandRepository,
        ProductRepository $productRepository,
        BranchRepository $branchRepository,
        OpeningSummaryRepository $openingSummaryRepository,
        OpeningDetailsRepository $openingDetailsRepository
    ) {
        $this->dateWiseReceiptDetailRepository = $dateWiseDetailsRepository;
        $this->dateWiseReceiptSummaryRepository = $dateWiseSummaryRepository;
        $this->outstandingSummaryRepository = $outstandingSummaryRepository;
        $this->outstandingDetailRepository = $outstandingDetailRepository;
        $this->brandRepository = $brandRepository;
        $this->productRepository = $productRepository;
        $this->branchRepository = $branchRepository;
        $this->openingSummaryRepository = $openingSummaryRepository;
        $this->openingDetailsRepository = $openingDetailsRepository;

        $this->module_parent = 'Outstanding Reconciliation Report';
        $this->module_name = 'outstanding';
        $this->module_icon = 'mdi-hospital-building';
        $this->module_route = 'admin.reports.'.$this->module_name;
        $this->module_view = 'backend.reports.'.$this->module_name;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param $encodedReq
     * @return Response
     * @throws GeneralException
     */
    public function branchWise($encodedReq)
    {
        ini_set('memory_limit', '1024M');

        $module_parent = '3.1 Branch wise Outstanding Report';
        $module_name = $this->module_name;
        $module_name_singular = str_singular($this->module_name);
        $module_icon = $this->module_icon;
        $module_action = 'Save';
        $module_route = $this->module_route;
        $module_view = $this->module_view;

        $report_name = 'branch_wise_outstanding';

        $page_heading = $this->module_parent;

        //convert to collection from request
        $request = new Collection(json_decode(decryptString($encodedReq), true));


        //branches
        $requestBranches = $this->branchRepository->getModel()
            ->select(['id', 'name', 'code', 'short_name'])
            ->whereIn('id', $request->get('branch_list'))
            ->get();

        //brands
        $requestProducts = $this->productRepository->getModel()
            ->select(['id', 'name', 'code', 'short_name', 'brand_id'])
            ->whereIn('id', $request->get('brand_list'))
            // ->whereIn('brand_id', $requestBrands->pluck('id'))
            ->get();

        //segments
        $requestBrands = $this->brandRepository->getModel()
            ->select(['id', 'name', 'code'])
            ->whereIn('id', $requestProducts->pluck('brand_id'))
            ->get();
        

//        $requestBrands = $this->brandRepository
//            ->select(['id', 'name'])
//            ->whereIn('id', $request->get('segment_list'))
//            ->cursor();
//
//        $requestProducts = $this->productRepository
//            ->select(['id', 'name', 'brand_id'])
//            ->whereIn('id', $request->get('brand_list'))
//            ->whereIn('brand_id', $requestBrands->pluck('id'))
//            ->cursor();

        $outstandingRevisedOpeningDate = Carbon::parse('2019-07-01');

        $fromDate = Carbon::parse($request->get('from_date'));
        $toDate = Carbon::parse($request->get('to_date'));

        //for outstanding revised opening date
        if ($outstandingRevisedOpeningDate->diff($fromDate)->invert === 1) {
            $fromDate = $outstandingRevisedOpeningDate;
        }

//        dd($outstandingRevisedOpeningDate->diff($fromDate), $fromDate->toDate(), $outstandingRevisedOpeningDate->toDate());


        $interval = CarbonInterval::createFromDateString('1 day');
        $dates = new CarbonPeriod($fromDate, $interval, $toDate);

        $branchName = ($requestBranches->count() > 15) ? 'All' : implode(',',
            $requestBranches->pluck('name')->toArray());

        //get initial openings month and year
        $firstInitialOpening = $this->openingSummaryRepository->getModel()
            ->select(['month', 'year'])
            ->whereIn('branch_id', $requestBranches->pluck('id'))
            ->orderByDesc('id')
            ->first();
        $initialOpeningMonth = $firstInitialOpening->month;
        $initialOpeningYear = $firstInitialOpening->year;
        $initialOpeningDay = Carbon::createFromDate($initialOpeningYear, $initialOpeningMonth, '01')->lastOfMonth();

        $initialOpening = $this->openingSummaryRepository->getModel()
            ->whereIn('branch_id', $requestBranches->pluck('id'))
            ->orderBy('id')
            ->with([
                'details' => function ($query) use ($requestProducts, $requestBrands) {
                    return $query->whereIn('product_id', $requestProducts->pluck('id'))
                        ->whereIn('brand_id', $requestBrands->pluck('id'));
                }
            ])
            ->get();

        $outstanding = $this->outstandingSummaryRepository->getModel()
            ->select([
                'id', 'sl', 'branch_id', 'trans_date', 'total_sales', 'total_return', 'total_net_sales', 'approve',
                'approve_by', 'created_by'
            ])
            ->whereIn('branch_id', $request->get('branch_list'))
            ->with([
                'details' => function ($query) use ($requestProducts, $requestBrands) {
                    return $query->select([
                        'trans_id', 'trans_sl', 'trans_date', 'branch_id', 'brand_id', 'product_id', 'sales_tp',
                        'sales_vat', 'sales_discount', 'sales_sp_disc', 'sales_total', 'return_tp', 'return_vat',
                        'return_discount', 'return_sp_disc', 'return_total', 'net_sales_tp', 'net_sales_vat',
                        'net_sales_discount', 'net_sales_sp_disc', 'net_sales_total', 'op_out', 'close_out'
                    ])
                        ->whereIn('product_id', $requestProducts->pluck('id'))
                        ->whereIn('brand_id', $requestBrands->pluck('id'));
                }
            ])
            ->whereBetween('trans_date', [$fromDate->toDateString(), $toDate->toDateString()])
//            ->whereBetween('trans_date', ['2019-07-01', $toDate->toDateString()])
            ->get();

        $dateWiseReceipts = $this->dateWiseReceiptDetailRepository->getModel()
            ->whereIn('product_id', $requestProducts->pluck('id'))
            ->whereIn('brand_id', $requestBrands->pluck('id'))
            ->whereIn('branch_id', $requestBranches->pluck('id'))
            ->whereBetween('trans_date', [$fromDate->toDateString(), $toDate->toDateString()])
//            ->whereBetween('trans_date', ['2019-07-01', $toDate->toDateString()])
            ->get();

        //for closing only
        $receiptsClosing = $this->dateWiseReceiptSummaryRepository->getModel()
            ->select(['id', 'sl', 'trans_date', 'branch_id', 'approve', 'approve_by', 'created_by'])
            ->with([
                'details' => function ($query) use ($requestProducts, $requestBrands) {
                    return $query->select([
                        'trans_id', 'trans_sl', 'trans_date', 'branch_id', 'brand_id', 'product_id', 'collection',
                        'money_receipt', 'adv_receipt', 'short_receipt', 'adv_adj', 'adj_plus', 'adj_minus','mr_reverse'
                    ])
                        ->whereIn('product_id', $requestProducts->pluck('id'))
                        ->whereIn('brand_id', $requestBrands->pluck('id'));
                }
            ])
            ->whereIn('branch_id', $requestBranches->pluck('id'))
//            ->whereBetween('trans_date', [$initialOpeningDay->toDateString(), $fromDate->copy()->subDay()->toDateString()])
            ->whereBetween('trans_date',
                [$outstandingRevisedOpeningDate->toDateString(), $fromDate->copy()->subDay()->toDateString()])
            ->get();

        //for closing only
        $outstandingClosing = $this->outstandingSummaryRepository->getModel()
            ->select([
                'id', 'sl', 'branch_id', 'trans_date', 'total_sales', 'total_return', 'total_net_sales', 'approve',
                'approve_by', 'created_by'
            ])
            ->whereIn('branch_id', $request->get('branch_list'))
            ->with([
                'details' => function ($query) use ($requestProducts, $requestBrands) {
                    return $query->select([
                        'trans_id', 'trans_sl', 'trans_date', 'branch_id', 'brand_id', 'product_id', 'sales_tp',
                        'sales_vat', 'sales_discount', 'sales_sp_disc', 'sales_total', 'return_tp', 'return_vat',
                        'return_discount', 'return_sp_disc', 'return_total', 'net_sales_tp', 'net_sales_vat',
                        'net_sales_discount', 'net_sales_sp_disc', 'net_sales_total', 'op_out', 'close_out'
                    ])
                        ->whereIn('product_id', $requestProducts->pluck('id'))
                        ->whereIn('brand_id', $requestBrands->pluck('id'));
                }
            ])
//            ->whereBetween('trans_date', [$initialOpeningDay->toDateString(), $fromDate->copy()->subDay()->toDateString()])
            ->whereBetween('trans_date',
                [$outstandingRevisedOpeningDate->toDateString(), $fromDate->copy()->subDay()->toDateString()])
            ->get();

        if ($module_name_singular === null) {
            throw new GeneralException('No transaction found!');
        }
        $reportView = view($module_view.'.report',
            compact('module_name', $module_name_singular, 'module_name_singular', 'module_parent',
                'module_icon', 'page_heading', 'module_action', 'module_view', 'module_route'))
            ->with('report_name', $report_name)
            ->with('branches', $requestBranches)
            ->with('branch_name', $branchName)
            ->with('dates', $dates)
            ->with('initial_opening', $initialOpening->groupBy('branch_id'))
            ->with('outstanding', $outstanding->groupBy('branch_id'))
            ->with('receipts', $dateWiseReceipts->groupBy('branch_id'))
            ->with('receipt_closing', $receiptsClosing->groupBy('branch_id'))
            ->with('outstanding_closing', $outstandingClosing->groupBy('branch_id'))
            ->with('brands', $requestBrands)
            ->with('products', $requestProducts)
            ->with('fromDate', $fromDate)
            ->with('toDate', $toDate);

//        dd($outstanding->groupBy('branch_id'), $dateWiseReceipts->groupBy('branch_id') );

        if (request()->has('export')) {
            $exportType = request()->get('export');
            $exportAction = request()->get('action');

            $exportBlade = $module_view.'.'.$report_name;
            $exportData = $reportView->getData();

            switch ($exportType) {
//                case 'excel':
//                    $exportExtension = 'xlsx';
//                    break;
//
//                case 'xls':
//                    $exportExtension = 'xls';
//                    break;

                case 'pdf':
                    $exportExtension = 'pdf';
                    break;

                case 'csv':
                case 'excel':
                case 'xls':
                    $exportExtension = 'csv';
                    break;

                default:
                    $exportExtension = 'xls';

            }

            return Excel::download(new ReportExport($exportBlade, $exportData), $page_heading.'.'.$exportExtension);
        }

        return $reportView;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param $encodedReq
     * @return Response
     * @throws GeneralException
     */
    public function brandWise($encodedReq)
    {
        $module_parent = '3.2 Brandwise Outstanding Reconciliation Report';
        $module_name = $this->module_name;
        $module_name_singular = str_singular($this->module_name);
        $module_icon = $this->module_icon;
        $module_action = 'Save';
        $module_route = $this->module_route;
        $module_view = $this->module_view;

        $report_name = 'brand_wise_outstanding';

        $page_heading = 'Outstanding Reconciliation Report (Brand Wise)';

        //convert to collection from request
        $request = new Collection(json_decode(decryptString($encodedReq), true));

        //branches
        $requestBranches = $this->branchRepository->getModel()
            ->select(['id', 'name', 'code', 'short_name'])
            ->whereIn('id', $request->get('branch_list'))
            ->cursor();

        //brands
        $requestProducts = $this->productRepository->getModel()
            ->select(['id', 'name', 'code', 'short_name', 'brand_id'])
            ->whereIn('id', $request->get('brand_list'))
            // ->whereIn('brand_id', $requestBrands->pluck('id'))
            ->cursor();

        //segments
        $requestBrands = $this->brandRepository->getModel()
            ->select(['id', 'name', 'code'])
            ->whereIn('id', $requestProducts->pluck('brand_id'))
            ->cursor();

        $outstandingRevisedOpeningDate = Carbon::parse('2019-07-01');

        $fromDate = Carbon::parse($request->get('from_date'));
        $toDate = Carbon::parse($request->get('to_date'));

        //for outstanding revised opening date
        if ($outstandingRevisedOpeningDate->diff($fromDate)->invert === 1) {
            $fromDate = $outstandingRevisedOpeningDate;
        }
        $closingDate = $fromDate->copy()->subDay();


        $branchName = ($requestBranches->count() > 15) ? 'All' : implode(',',
            $requestBranches->pluck('name')->toArray());

        //get initial openings month and year
        $firstInitialOpening = $this->openingSummaryRepository->getModel()
            ->select(['month', 'year'])
            ->whereIn('branch_id', $requestBranches->pluck('id'))
            ->orderByDesc('id')
            ->first();
        $initialOpeningMonth = $firstInitialOpening->month;
        $initialOpeningYear = $firstInitialOpening->year;
        $initialOpeningDay = Carbon::createFromDate($initialOpeningYear, $initialOpeningMonth, '01')->lastOfMonth();

        $initialOpening = $this->openingSummaryRepository->makeModel()
            ->whereIn('branch_id', $requestBranches->pluck('id'))
            ->orderBy('id')
            ->with([
                'details' => function ($query) use ($requestProducts, $requestBrands) {
                    return $query->whereIn('product_id', $requestProducts->pluck('id'))
                        ->whereIn('brand_id', $requestBrands->pluck('id'));
                }
            ])
            ->cursor();

        $outstanding = $this->outstandingDetailRepository->makeModel()
            ->select([
                'trans_id', 'trans_sl', 'trans_date', 'branch_id', 'brand_id', 'product_id', 'sales_tp',
                'sales_vat', 'sales_discount', 'sales_sp_disc', 'sales_total', 'return_tp', 'return_vat',
                'return_discount', 'return_sp_disc', 'return_total', 'net_sales_tp', 'net_sales_vat',
                'net_sales_discount', 'net_sales_sp_disc', 'net_sales_total', 'op_out', 'close_out'
            ])
            ->whereIn('branch_id', $request->get('branch_list'))
            ->whereIn('product_id', $requestProducts->pluck('id'))
            //   ->whereIn('brand_id', $requestBrands->pluck('id'))
            ->whereBetween('trans_date', [$fromDate->toDateString(), $toDate->toDateString()])
//            ->whereBetween('trans_date', ['2019-07-01', $toDate->toDateString()])
            ->cursor();

//        $outstanding = $this->outstandingSummaryRepository->makeModel()
//            ->select([
//                'id', 'sl', 'branch_id', 'trans_date', 'total_sales', 'total_return', 'total_net_sales', 'approve',
//                'approve_by', 'created_by'
//            ])
//            ->whereIn('branch_id', $request->get('branch_list'))
//            ->with([
//                'details' => function ($query) use ($requestProducts, $requestBrands) {
//                    return $query->select([
//                        'trans_id', 'trans_sl', 'trans_date', 'branch_id', 'brand_id', 'product_id', 'sales_tp',
//                        'sales_vat', 'sales_discount', 'sales_sp_disc', 'sales_total', 'return_tp', 'return_vat',
//                        'return_discount', 'return_sp_disc', 'return_total', 'net_sales_tp', 'net_sales_vat',
//                        'net_sales_discount', 'net_sales_sp_disc', 'net_sales_total', 'op_out', 'close_out'
//                    ])
//                        ->whereIn('product_id', $requestProducts->pluck('id'))
//                        ->whereIn('brand_id', $requestBrands->pluck('id'));
//                }
//            ])
//            ->whereBetween('trans_date', [$fromDate->toDateString(), $toDate->toDateString()])
////            ->whereBetween('trans_date', ['2019-07-01', $toDate->toDateString()])
//            ->cursor();

        $dateWiseReceipts = $this->dateWiseReceiptDetailRepository->makeModel()
            ->whereIn('product_id', $requestProducts->pluck('id'))
            ->whereIn('brand_id', $requestBrands->pluck('id'))
            ->whereIn('branch_id', $requestBranches->pluck('id'))
            ->whereBetween('trans_date', [$fromDate->toDateString(), $toDate->toDateString()])
//            ->whereBetween('trans_date', ['2019-07-01', $toDate->toDateString()])
            ->cursor();

        //for closing only
        $receiptsClosing = $this->dateWiseReceiptDetailRepository->makeModel()
            ->select([
                'trans_id', 'trans_sl', 'trans_date', 'branch_id', 'brand_id', 'product_id', 'collection',
                'money_receipt', 'adv_receipt', 'short_receipt', 'adv_adj', 'adj_plus', 'adj_minus','mr_reverse'
            ])
            ->whereIn('product_id', $requestProducts->pluck('id'))
            ->whereIn('brand_id', $requestBrands->pluck('id'))
            ->whereIn('branch_id', $requestBranches->pluck('id'))
//            ->whereBetween('trans_date', [$initialOpeningDay->toDateString(), $fromDate->copy()->subDay()->toDateString()])
            ->whereBetween('trans_date', [$outstandingRevisedOpeningDate->toDateString(), $closingDate->toDateString()])
            ->cursor();

        //for closing only
        $outstandingClosingDetails = $this->outstandingDetailRepository->makeModel()
            ->select([
                'trans_id', 'trans_sl', 'trans_date', 'branch_id', 'brand_id', 'product_id', 'sales_tp',
                'sales_vat', 'sales_discount', 'sales_sp_disc', 'sales_total', 'return_tp', 'return_vat',
                'return_discount', 'return_sp_disc', 'return_total', 'net_sales_tp', 'net_sales_vat',
                'net_sales_discount', 'net_sales_sp_disc', 'net_sales_total', 'op_out', 'close_out'
            ])
            ->whereIn('branch_id', $request->get('branch_list'))
            ->whereIn('product_id', $requestProducts->pluck('id'))
//            ->whereBetween('trans_date', [$initialOpeningDay->toDateString(), $fromDate->copy()->subDay()->toDateString()])
            ->whereBetween('trans_date', [$outstandingRevisedOpeningDate->toDateString(), $closingDate->toDateString()])
            ->cursor();

        //report generate
        $rows = new Collection();

        foreach ($requestProducts as $product) {
            $row = new Collection();

            $firstOpening = $netSales = $adjPositive = $adjNegative = $netCollection = $closingBalance = $currentOpening = 0;

            //for initial opening
            $productWiseOpeningSummary = $initialOpening->first();
            $productWiseOpeningDetail = $productWiseOpeningSummary->details->filter(function ($item) use ($product) {
                return $item->product_id === $product->id;
            });

            //closing
            $productWiseClosingOutstanding = $this->_getFilter($outstandingClosingDetails, $product->id);
            $productWiseClosingReceipt = $this->_getFilter($receiptsClosing, $product->id);

            $firstOpening = $productWiseOpeningDetail->sum('outstanding');

            $totalSales = $this->_getSum($productWiseClosingOutstanding,
                    'sales_tp') + $this->_getSum($productWiseClosingOutstanding,
                    'sales_vat') - $this->_getSum($productWiseClosingOutstanding,
                    'sales_discount') - $this->_getSum($productWiseClosingOutstanding, 'sales_sp_disc');

            $totalReturn = $this->_getSum($productWiseClosingOutstanding,
                    'return_tp') + $this->_getSum($productWiseClosingOutstanding,
                    'return_vat') - $this->_getSum($productWiseClosingOutstanding,
                    'return_discount') - $this->_getSum($productWiseClosingOutstanding, 'return_sp_disc');

            $netSales = $totalSales - $totalReturn;

            $adjPositive = $this->_getSum($productWiseClosingReceipt, 'adj_plus');
            $adjNegative = $this->_getSum($productWiseClosingReceipt, 'adj_minus');
            $netCollection = $this->_getSum($productWiseClosingReceipt, 'money_receipt');
            $mrReverse = $this->_getSum($productWiseClosingReceipt, 'mr_reverse');
            

            $currentOpening = $firstOpening + $netSales + $adjPositive - $adjNegative - $netCollection + $mrReverse;

            //  dd($totalSales, $totalReturn);

            $row->put('opening', $currentOpening);


            //for current
            $productWiseReceipts = $this->_getFilter($dateWiseReceipts, $product->id);
            $productWiseOutstanding = $this->_getFilter($outstanding, $product->id);

            $row->put('sales_tp', $this->_getSum($productWiseOutstanding, 'sales_tp'));
            $row->put('sales_vat', $this->_getSum($productWiseOutstanding, 'sales_vat'));
            $row->put('sales_discount', $this->_getSum($productWiseOutstanding, 'sales_discount'));
            $row->put('sales_sp_disc', $this->_getSum($productWiseOutstanding, 'sales_sp_disc'));
            $totalSales = $row->get('sales_tp') + $row->get('sales_vat') - $row->get('sales_discount') - $row->get('sales_sp_disc');
            $row->put('total_sales', $totalSales);

            $row->put('return_tp', $this->_getSum($productWiseOutstanding, 'return_tp'));
            $row->put('return_vat', $this->_getSum($productWiseOutstanding, 'return_vat'));
            $row->put('return_discount', $this->_getSum($productWiseOutstanding, 'return_discount'));
            $row->put('return_sp_disc', $this->_getSum($productWiseOutstanding, 'return_sp_disc'));
            $totalReturn = $row->get('return_tp') + $row->get('return_vat') - $row->get('return_discount') - $row->get('return_sp_disc');
            $row->put('total_return', $totalReturn);

            $row->put('net_sales_tp', $row->get('sales_tp') - $row->get('return_tp'));
            $row->put('net_sales_vat', $row->get('sales_vat') - $row->get('return_vat'));
            $row->put('net_sales_discount', $row->get('sales_discount') - $row->get('return_discount'));
            $row->put('net_sales_sp_disc', $row->get('sales_sp_disc') - $row->get('return_sp_disc'));
            $row->put('net_total', $row->get('total_sales') - $row->get('total_return'));

            $row->put('adj_positive', $this->_getSum($productWiseReceipts, 'adj_plus'));
            $row->put('adj_negative', $this->_getSum($productWiseReceipts, 'adj_minus'));
            $row->put('net_collection', $this->_getSum($productWiseReceipts, 'money_receipt'));
            $row->put('mr_reverse', $this->_getSum($productWiseReceipts, 'mr_reverse'));

            $netSales = $totalSales - $totalReturn;
            $row->put('total_net_sales', $netSales);

            $closingBalance = $currentOpening + $netSales + $row->get('adj_positive') - $row->get('adj_negative') - $row->get('net_collection') + $row->get('mr_reverse');

            $row->put('closing', $closingBalance);

            $rows->put($product->name, $row);
        }

        if ($module_name_singular === null) {
            throw new GeneralException('No transaction found!');
        }
        $reportView = view($module_view.'.report',
            compact('module_name', $module_name_singular, 'module_name_singular', 'module_parent',
                'module_icon', 'page_heading', 'module_action', 'module_view', 'module_route'))
            ->with('report_name', $report_name)
            ->with('branches', $requestBranches)
            ->with('branch_name', $branchName)
            ->with('rows', $rows)
            ->with('from_date', $fromDate)
            ->with('to_date', $toDate);


        if (request()->has('export')) {
            $exportType = request()->get('export');
            $exportAction = request()->get('action');

            $exportBlade = $module_view.'.'.$report_name;
            $exportData = $reportView->getData();

            switch ($exportType) {
//                case 'excel':
//                    $exportExtension = 'xlsx';
//                    break;
//
//                case 'xls':
//                    $exportExtension = 'xls';
//                    break;

                case 'pdf':
                    $exportExtension = 'pdf';
                    break;

                case 'csv':
                case 'excel':
                case 'xls':
                    $exportExtension = 'csv';
                    break;

                default:
                    $exportExtension = 'xls';

            }

            return Excel::download(new ReportExport($exportBlade, $exportData), $page_heading.'.'.$exportExtension);
        }

        return $reportView;
    }

    /**
     * @param $collection
     * @param $productId
     * @return mixed
     */
    public function _getFilter($collection, $productId)
    {
        return $collection->filter(function ($item) use ($productId) {
            return $item->product_id == $productId;
        });
    }

    /**
     * @param $collection
     * @param $column
     * @return mixed
     */
    private function _getSum($collection, $column)
    {
        return $collection->sum($column);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param $encodedReq
     * @return Response
     * @throws GeneralException
     */
    public function branchBrandWise($encodedReq)
    {
        ini_set('memory_limit', '2048M');

        $module_parent = '3.3 Branch & Brand wise Outstanding Summary Report';
        $module_name = $this->module_name;
        $module_name_singular = str_singular($this->module_name);
        $module_icon = $this->module_icon;
        $module_action = 'Save';
        $module_route = $this->module_route;
        $module_view = $this->module_view;

        $report_name = 'brand_branch_wise_outstanding';

        $page_heading = 'Outstanding Summary (Brand & Branch Wise)';

        //convert to collection from request
        $request = new Collection(json_decode(decryptString($encodedReq), true));

        //branches
        $requestBranches = $this->branchRepository->getModel()
            ->select(['id', 'name', 'code', 'short_name'])
            ->whereIn('id', $request->get('branch_list'))
            ->cursor();

        //brands
        $requestProducts = $this->productRepository->getModel()
            ->select(['id', 'name', 'code', 'short_name', 'brand_id'])
            ->whereIn('id', $request->get('brand_list'))
            // ->whereIn('brand_id', $requestBrands->pluck('id'))
            ->cursor();

        //segments
        $requestBrands = $this->brandRepository->getModel()
            ->select(['id', 'name', 'code'])
            ->whereIn('id', $requestProducts->pluck('brand_id'))
            ->cursor();

        $outstandingRevisedOpeningDate = Carbon::parse('2019-07-01');

        $fromDate = Carbon::parse($request->get('from_date'));
        $toDate = Carbon::parse($request->get('to_date'));

        //for outstanding revised opening date
        if ($outstandingRevisedOpeningDate->diff($fromDate)->invert === 1) {
            $fromDate = $outstandingRevisedOpeningDate;
        }
        $closingDate = $toDate->copy()->subDay();


        $branchName = ($requestBranches->count() > 15) ? 'All' : implode(',',
            $requestBranches->pluck('name')->toArray());

        //get initial openings month and year
        $firstInitialOpening = $this->openingSummaryRepository->getModel()
            ->select(['month', 'year'])
            ->whereIn('branch_id', $requestBranches->pluck('id'))
            ->orderByDesc('id')
            ->first();
        $initialOpeningMonth = $firstInitialOpening->month;
        $initialOpeningYear = $firstInitialOpening->year;
        $initialOpeningDay = Carbon::createFromDate($initialOpeningYear, $initialOpeningMonth, '01')->lastOfMonth();

//        $initialOpening = $this->openingDetailsRepository->makeModel()
//            ->whereIn('branch_id', $requestBranches->pluck('id'))
//            ->orderBy('id')
//            ->whereIn('product_id', $requestProducts->pluck('id'))
//            ->whereIn('brand_id', $requestBrands->pluck('id'))
//            ->cursor();
//
//        $outstanding = $this->outstandingDetailRepository->makeModel()
//            ->select([
//                'trans_id', 'trans_sl', 'trans_date', 'branch_id', 'brand_id', 'product_id', 'sales_tp',
//                'sales_vat', 'sales_discount', 'sales_sp_disc', 'sales_total', 'return_tp', 'return_vat',
//                'return_discount', 'return_sp_disc', 'return_total', 'net_sales_tp', 'net_sales_vat',
//                'net_sales_discount', 'net_sales_sp_disc', 'net_sales_total', 'op_out', 'close_out'
//            ])
//            ->whereIn('branch_id', $request->get('branch_list'))
//            ->whereIn('product_id', $requestProducts->pluck('id'))
//            //   ->whereIn('brand_id', $requestBrands->pluck('id'))
//            ->whereBetween('trans_date', [$fromDate->toDateString(), $toDate->toDateString()])
////            ->whereBetween('trans_date', ['2019-07-01', $toDate->toDateString()])
//            ->cursor();
//
////        $outstanding = $this->outstandingSummaryRepository->makeModel()
////            ->select([
////                'id', 'sl', 'branch_id', 'trans_date', 'total_sales', 'total_return', 'total_net_sales', 'approve',
////                'approve_by', 'created_by'
////            ])
////            ->whereIn('branch_id', $request->get('branch_list'))
////            ->with([
////                'details' => function ($query) use ($requestProducts, $requestBrands) {
////                    return $query->select([
////                        'trans_id', 'trans_sl', 'trans_date', 'branch_id', 'brand_id', 'product_id', 'sales_tp',
////                        'sales_vat', 'sales_discount', 'sales_sp_disc', 'sales_total', 'return_tp', 'return_vat',
////                        'return_discount', 'return_sp_disc', 'return_total', 'net_sales_tp', 'net_sales_vat',
////                        'net_sales_discount', 'net_sales_sp_disc', 'net_sales_total', 'op_out', 'close_out'
////                    ])
////                        ->whereIn('product_id', $requestProducts->pluck('id'))
////                        ->whereIn('brand_id', $requestBrands->pluck('id'));
////                }
////            ])
////            ->whereBetween('trans_date', [$fromDate->toDateString(), $toDate->toDateString()])
//////            ->whereBetween('trans_date', ['2019-07-01', $toDate->toDateString()])
////            ->cursor();
//
//        $dateWiseReceipts = $this->dateWiseReceiptDetailRepository->makeModel()
//            ->whereIn('product_id', $requestProducts->pluck('id'))
//            ->whereIn('brand_id', $requestBrands->pluck('id'))
//            ->whereIn('branch_id', $requestBranches->pluck('id'))
//            ->whereBetween('trans_date', [$fromDate->toDateString(), $toDate->toDateString()])
////            ->whereBetween('trans_date', ['2019-07-01', $toDate->toDateString()])
//            ->cursor();
//
//        //for closing only
//        $receiptsClosing = $this->dateWiseReceiptDetailRepository->makeModel()
//            ->select([
//                'trans_id', 'trans_sl', 'trans_date', 'branch_id', 'brand_id', 'product_id', 'collection',
//                'money_receipt', 'adv_receipt', 'short_receipt', 'adv_adj', 'adj_plus', 'adj_minus'
//            ])
//            ->whereIn('product_id', $requestProducts->pluck('id'))
//            ->whereIn('brand_id', $requestBrands->pluck('id'))
//            ->whereIn('branch_id', $requestBranches->pluck('id'))
////            ->whereBetween('trans_date', [$initialOpeningDay->toDateString(), $fromDate->copy()->subDay()->toDateString()])
//            ->whereBetween('trans_date', [$outstandingRevisedOpeningDate->toDateString(), $closingDate->toDateString()])
//            ->cursor();
//
//        //for closing only
//        $outstandingClosingDetails = $this->outstandingDetailRepository->makeModel()
//            ->select([
//                'trans_id', 'trans_sl', 'trans_date', 'branch_id', 'brand_id', 'product_id', 'sales_tp',
//                'sales_vat', 'sales_discount', 'sales_sp_disc', 'sales_total', 'return_tp', 'return_vat',
//                'return_discount', 'return_sp_disc', 'return_total', 'net_sales_tp', 'net_sales_vat',
//                'net_sales_discount', 'net_sales_sp_disc', 'net_sales_total', 'op_out', 'close_out'
//            ])
//            ->whereIn('branch_id', $request->get('branch_list'))
//            ->whereIn('product_id', $requestProducts->pluck('id'))
////            ->whereBetween('trans_date', [$initialOpeningDay->toDateString(), $fromDate->copy()->subDay()->toDateString()])
//            ->whereBetween('trans_date', [$outstandingRevisedOpeningDate->toDateString(), $closingDate->toDateString()])
//            ->cursor();

        //report generate
        $rows = new Collection();
        $branchGrandTotal = new Collection();
        $productGrandTotal = new Collection();

        foreach ($requestBranches as $branch) {
            $row = new Collection();

            $branchTotal = $firstOpening = $netSales = $adjPositive = $adjNegative = $netCollection = $mrReverse = $closingBalance = $currentOpening = 0;

            $branchWiseOpening = $this->openingDetailsRepository->makeModel()
                ->where('branch_id', $branch->id)
                ->whereIn('product_id', $requestProducts->pluck('id'))
                ->whereIn('brand_id', $requestBrands->pluck('id'))
                ->cursor();

            $branchWiseOutstanding = $this->outstandingDetailRepository->makeModel()
                ->select([
                    'trans_id', 'trans_sl', 'trans_date', 'branch_id', 'brand_id', 'product_id', 'sales_tp',
                    'sales_vat', 'sales_discount', 'sales_sp_disc', 'sales_total', 'return_tp', 'return_vat',
                    'return_discount', 'return_sp_disc', 'return_total', 'net_sales_tp', 'net_sales_vat',
                    'net_sales_discount', 'net_sales_sp_disc', 'net_sales_total', 'op_out', 'close_out'
                ])
                ->where('branch_id', $branch->id)
                ->whereIn('product_id', $requestProducts->pluck('id'))
                ->whereBetween('trans_date', [$toDate->toDateString(), $toDate->toDateString()])
                ->cursor();

            $branchWiseReceipts = $this->dateWiseReceiptDetailRepository->makeModel()
                ->where('branch_id', $branch->id)
                ->whereIn('product_id', $requestProducts->pluck('id'))
                //   ->whereIn('brand_id', $requestBrands->pluck('id'))
                ->whereBetween('trans_date', [$toDate->toDateString(), $toDate->toDateString()])
                ->cursor();

            //for closing only
            $branchWiseReceiptClosing = $this->dateWiseReceiptDetailRepository->makeModel()
                ->select([
                    'trans_id', 'trans_sl', 'trans_date', 'branch_id', 'brand_id', 'product_id', 'collection',
                    'money_receipt', 'adv_receipt', 'short_receipt', 'adv_adj', 'adj_plus', 'adj_minus','mr_reverse'
                ])
                ->whereIn('product_id', $requestProducts->pluck('id'))
                //->whereIn('brand_id', $requestBrands->pluck('id'))
                ->where('branch_id', $branch->id)
                ->whereBetween('trans_date',
                    [$outstandingRevisedOpeningDate->toDateString(), $closingDate->toDateString()])
                ->cursor();

            $branchWiseOutstandingClosing = $this->outstandingDetailRepository->makeModel()
                ->select([
                    'trans_id', 'trans_sl', 'trans_date', 'branch_id', 'brand_id', 'product_id', 'sales_tp',
                    'sales_vat', 'sales_discount', 'sales_sp_disc', 'sales_total', 'return_tp', 'return_vat',
                    'return_discount', 'return_sp_disc', 'return_total', 'net_sales_tp', 'net_sales_vat',
                    'net_sales_discount', 'net_sales_sp_disc', 'net_sales_total', 'op_out', 'close_out'
                ])
                ->where('branch_id', $branch->id)
                ->whereIn('product_id', $requestProducts->pluck('id'))
                ->whereBetween('trans_date',
                    [$outstandingRevisedOpeningDate->toDateString(), $closingDate->toDateString()])
                ->cursor();

            foreach ($requestProducts as $product) {
                $row1 = new Collection();

                $productTotal = $firstOpening = $netSales = $adjPositive = $adjNegative = $netCollection = $mrReverse = $closingBalance = $currentOpening = 0;

                //for initial opening
                $productWiseOpeningDetail = $branchWiseOpening->where('product_id', $product->id);

                //closing
                $productWiseClosingOutstanding = $this->_getFilter($branchWiseOutstandingClosing, $product->id);
                $productWiseClosingReceipt = $this->_getFilter($branchWiseReceiptClosing, $product->id);


                $firstOpening = $productWiseOpeningDetail->sum('outstanding');

                $totalSales = $this->_getSum($productWiseClosingOutstanding,
                        'sales_tp') + $this->_getSum($productWiseClosingOutstanding,
                        'sales_vat') - $this->_getSum($productWiseClosingOutstanding,
                        'sales_discount') - $this->_getSum($productWiseClosingOutstanding, 'sales_sp_disc');

                $totalReturn = $this->_getSum($productWiseClosingOutstanding,
                        'return_tp') + $this->_getSum($productWiseClosingOutstanding,
                        'return_vat') - $this->_getSum($productWiseClosingOutstanding,
                        'return_discount') - $this->_getSum($productWiseClosingOutstanding, 'return_sp_disc');

                $netSales = $totalSales - $totalReturn;

                $adjPositive = $this->_getSum($productWiseClosingReceipt, 'adj_plus');
                $adjNegative = $this->_getSum($productWiseClosingReceipt, 'adj_minus');
                $netCollection = $this->_getSum($productWiseClosingReceipt, 'money_receipt');
                $mrReverse = $this->_getSum($productWiseClosingReceipt, 'mr_reverse');
                $currentOpening = $firstOpening + $netSales + $adjPositive - $adjNegative - $netCollection + $mrReverse;


                //for current
                $productWiseReceipts = $this->_getFilter($branchWiseReceipts, $product->id);
                $productWiseOutstanding = $this->_getFilter($branchWiseOutstanding, $product->id);

                $totalSales = $this->_getSum($productWiseOutstanding,
                        'sales_tp') + $this->_getSum($productWiseOutstanding,
                        'sales_vat') - $this->_getSum($productWiseOutstanding,
                        'sales_discount') - $this->_getSum($productWiseOutstanding, 'sales_sp_disc');

                $totalReturn = $this->_getSum($productWiseOutstanding,
                        'return_tp') + $this->_getSum($productWiseOutstanding,
                        'return_vat') - $this->_getSum($productWiseOutstanding,
                        'return_discount') - $this->_getSum($productWiseOutstanding, 'return_sp_disc');


                $adjPositive = $this->_getSum($productWiseReceipts, 'adj_plus');
                $adjNegative = $this->_getSum($productWiseReceipts, 'adj_minus');
                $netCollection = $this->_getSum($productWiseReceipts, 'money_receipt');
                $mrReverse = $this->_getSum($productWiseReceipts, 'mr_reverse');

                $netSales = $totalSales - $totalReturn;

                $closingBalance = $currentOpening + $netSales + $adjPositive - $adjNegative - $netCollection + $mrReverse;

                $branchTotal += $closingBalance;

                $row1->put('closing', $closingBalance);
                $row->put($product->name, $row1);
            }

            $branchGrandTotal->put($branch->short_name, $branchTotal);
            $rows->put($branch->short_name, $row);
        }


        if ($module_name_singular === null) {
            throw new GeneralException('No transaction found!');
        }
        $reportView = view($module_view.'.report',
            compact('module_name', $module_name_singular, 'module_name_singular', 'module_parent',
                'module_icon', 'page_heading', 'module_action', 'module_view', 'module_route'))
            ->with('report_name', $report_name)
            ->with('branches', $requestBranches)
            ->with('products', $requestProducts)
            ->with('branch_name', $branchName)
            ->with('rows', $rows)
            ->with('product_total', $productGrandTotal)
            ->with('branch_total', $branchGrandTotal)
            ->with('from_date', $fromDate)
            ->with('to_date', $toDate);


        if (request()->has('export')) {
            $exportType = request()->get('export');
            $exportAction = request()->get('action');

            $exportBlade = $module_view.'.'.$report_name;
            $exportData = $reportView->getData();

            switch ($exportType) {
//                case 'excel':
//                    $exportExtension = 'xlsx';
//                    break;
//
//                case 'xls':
//                    $exportExtension = 'xls';
//                    break;

                case 'pdf':
                    $exportExtension = 'pdf';
                    break;

                case 'csv':
                case 'excel':
                case 'xls':
                    $exportExtension = 'csv';
                    break;

                default:
                    $exportExtension = 'xls';

            }

            return Excel::download(new ReportExport($exportBlade, $exportData), $page_heading.'.'.$exportExtension);
        }

        return $reportView;
    }

    public function branchBrandWise1($encodedReq)
    {
        ini_set('memory_limit', '2048M');

        $module_parent = $this->module_parent;
        $module_name = $this->module_name;
        $module_name_singular = str_singular($this->module_name);
        $module_icon = $this->module_icon;
        $module_action = 'Save';
        $module_route = $this->module_route;
        $module_view = $this->module_view;

        $report_name = 'brand_branch_wise_outstanding';

        $page_heading = 'Outstanding Summary (Brand & Branch Wise)';

        //convert to collection from request
        $request = new Collection(json_decode(decryptString($encodedReq), true));

        //branches
        $requestBranches = $this->branchRepository->getModel()
            ->select(['id', 'name', 'code', 'short_name'])
            ->whereIn('id', $request->get('branch_list'))
            ->cursor();

        //brands
        $requestProducts = $this->productRepository->getModel()
            ->select(['id', 'name', 'code', 'short_name', 'brand_id'])
            ->whereIn('id', $request->get('brand_list'))
            // ->whereIn('brand_id', $requestBrands->pluck('id'))
            ->cursor();

        //segments
        $requestBrands = $this->brandRepository->getModel()
            ->select(['id', 'name', 'code'])
            ->whereIn('id', $requestProducts->pluck('brand_id'))
            ->cursor();

        $outstandingRevisedOpeningDate = Carbon::parse('2019-07-01');

        $fromDate = Carbon::parse($request->get('from_date'));
        $toDate = Carbon::parse($request->get('to_date'));

//        //for outstanding revised opening date
//        if ($outstandingRevisedOpeningDate->diff($fromDate)->invert === 1) {
//            $fromDate = $outstandingRevisedOpeningDate;
//        }
        $closingDate = $toDate->copy()->subDay();


        $branchName = ($requestBranches->count() > 15) ? 'All' : implode(',',
            $requestBranches->pluck('name')->toArray());

        //get initial openings month and year
        $firstInitialOpening = $this->openingSummaryRepository->getModel()
            ->select(['month', 'year'])
            ->whereIn('branch_id', $requestBranches->pluck('id'))
            ->orderByDesc('id')
            ->first();
        $initialOpeningMonth = $firstInitialOpening->month;
        $initialOpeningYear = $firstInitialOpening->year;
        $initialOpeningDay = Carbon::createFromDate($initialOpeningYear, $initialOpeningMonth, '01')->lastOfMonth();

        //report generate
        $rows = new Collection();

        foreach ($requestBranches as $branch) {
            $row2 = new Collection();
//            $firstOpening = $netSales = $adjPositive = $adjNegative = $netCollection = $closingBalance = $currentOpening = 0;
//
//            //for initial opening
//            $branchWiseOpeningSummary = $initialOpening->where('branch_id', $branch->id)->first();
//            $branchWiseOpeningDetailBranch = $branchWiseOpeningSummary->details->filter(function ($item) use ($branch) {
//                return $item->branch_id === $branch->id;
//            });
//
//            //closing
//            $branchWiseOutstandingClosing = $this->_getFilterBranch($outstandingClosingDetails, $branch->id);
//            $branchWiseReceiptClosing = $this->_getFilterBranch($receiptsClosing, $branch->id);
//
//
//            //current
//            $branchWiseReceipt = $this->_getFilterBranch($dateWiseReceipts, $branch->id);
//            $branchWiseOutstanding = $this->_getFilterBranch($outstanding, $branch->id);
            foreach ($requestProducts as $product) {
                $netSales = $adjPositive = $adjNegative = $netCollection = $closingBalance = $currentOpening = 0;

                $firstOpening = (float) $this->openingDetailsRepository->makeModel()
                    ->where('branch_id', $branch->id)
                    ->where('product_id', $product->id)
                    ->sum('outstanding');

                //closing
                $netSales = (float) $this->outstandingDetailRepository->makeModel()
                    ->select(DB::raw('(sum(sales_tp) + sum(sales_vat) - sum(sales_discount) - sum(sales_sp_disc)) - (sum(return_tp) + sum(return_vat) - sum(return_discount) - sum(return_sp_disc)) as net_sales'))
                    ->where('branch_id', $branch->id)
                    ->where('product_id', $product->id)
//                    ->whereIn('brand_id', $requestBrands->pluck('id'))
                    ->whereBetween('trans_date',
                        [$outstandingRevisedOpeningDate->toDateString(), $closingDate->toDateString()])
                    ->first()
                    ->net_sales;

                $netCollection = (float) $this->dateWiseReceiptDetailRepository->makeModel()
                    ->select(DB::raw('(sum(adj_plus) - sum(adj_minus) - sum(money_receipt)) as net_collection'))
                    ->where('product_id', $product->id)
                    ->where('branch_id', $branch->id)
                    ->whereBetween('trans_date',
                        [$outstandingRevisedOpeningDate->toDateString(), $closingDate->toDateString()])
                    ->first()
                    ->net_collection;

//                $totalReturn = $this->_getSum($productWiseClosingOutstanding,
//                        'return_tp') + $this->_getSum($productWiseClosingOutstanding,
//                        'return_vat') - $this->_getSum($productWiseClosingOutstanding,
//                        'return_discount') - $this->_getSum($productWiseClosingOutstanding, 'return_sp_disc');
//                $totalSales = $this->_getSum($productWiseClosingOutstanding,
//                        'sales_tp') + $this->_getSum($productWiseClosingOutstanding,
//                        'sales_vat') - $this->_getSum($productWiseClosingOutstanding,
//                        'sales_discount') - $this->_getSum($productWiseClosingOutstanding, 'sales_sp_disc');
//
//                $totalReturn = $this->_getSum($productWiseClosingOutstanding,
//                        'return_tp') + $this->_getSum($productWiseClosingOutstanding,
//                        'return_vat') - $this->_getSum($productWiseClosingOutstanding,
//                        'return_discount') - $this->_getSum($productWiseClosingOutstanding, 'return_sp_disc');
//                $netSales = $totalSales - $totalReturn;
//
//                $adjPositive = $this->_getSum($productWiseClosingReceipt, 'adj_plus');
//                $adjNegative = $this->_getSum($productWiseClosingReceipt, 'adj_minus');
//                $netCollection = $this->_getSum($productWiseClosingReceipt, 'money_receipt');

                $currentOpening = $firstOpening + $netSales + $netCollection;


                //for current
                $netSales = (float) $this->outstandingDetailRepository->makeModel()
                    ->select(DB::raw('(sum(sales_tp) + sum(sales_vat) - sum(sales_discount) - sum(sales_sp_disc)) - (sum(return_tp) + sum(return_vat) - sum(return_discount) - sum(return_sp_disc)) as net_sales'))
//                    ->select([
//                        'trans_id', 'trans_sl', 'trans_date', 'branch_id', 'brand_id', 'product_id', 'sales_tp',
//                        'sales_vat', 'sales_discount', 'sales_sp_disc', 'sales_total', 'return_tp', 'return_vat',
//                        'return_discount', 'return_sp_disc', 'return_total', 'net_sales_tp', 'net_sales_vat',
//                        'net_sales_discount', 'net_sales_sp_disc', 'net_sales_total', 'op_out', 'close_out'
//                    ])
                    ->where('branch_id', $branch->id)
                    ->where('product_id', $product->id)
                    ->whereBetween('trans_date', [$toDate->toDateString(), $toDate->toDateString()])
                    ->first()
                    ->net_sales;

                $netCollection = (float) $this->dateWiseReceiptDetailRepository->makeModel()
                    ->select(DB::raw('(sum(adj_plus) - sum(adj_minus) - sum(money_receipt)) as net_collection'))
                    ->where('product_id', $product->id)
                    ->where('branch_id', $branch->id)
                    ->whereBetween('trans_date', [$toDate->toDateString(), $toDate->toDateString()])
                    ->first()
                    ->net_collection;

                //for current
//                $productWiseReceipts = $this->_getFilter($branchWiseReceipt, $product->id);
//                $productWiseOutstanding = $this->_getFilter($branchWiseOutstanding, $product->id);
//                $totalSales = $this->_getSum($productWiseOutstanding,
//                        'sales_tp') + $this->_getSum($productWiseOutstanding,
//                        'sales_vat') - $this->_getSum($productWiseOutstanding,
//                        'sales_discount') - $this->_getSum($productWiseOutstanding, 'sales_sp_disc');
//
//                $totalReturn = $this->_getSum($productWiseOutstanding,
//                        'return_tp') + $this->_getSum($productWiseOutstanding,
//                        'return_vat') - $this->_getSum($productWiseOutstanding,
//                        'return_discount') - $this->_getSum($productWiseOutstanding, 'return_sp_disc');
//
//                $netSales = $totalSales - $totalReturn;
//
//                $adjPositive = $this->_getSum($productWiseReceipts, 'adj_plus');
//                $adjNegative = $this->_getSum($productWiseReceipts, 'adj_minus');
//                $netCollection = $this->_getSum($productWiseReceipts, 'money_receipt');

                $closingBalance = $currentOpening + $netSales + $netCollection;

                $row2->put($product->name, $closingBalance);
            }

            $rows->put($branch->short_name, $row2);
        }

//        $initialOpening = $this->openingSummaryRepository->getModel()
//            ->whereIn('branch_id', $requestBranches->pluck('id'))
//            ->orderBy('id')
//            ->with([
//                'details' => function ($query) use ($requestProducts, $requestBrands) {
//                    return $query->whereIn('product_id', $requestProducts->pluck('id'))
//                        ->whereIn('brand_id', $requestBrands->pluck('id'));
//                }
//            ])
//            ->cursor();
//
//        $outstanding = $this->outstandingSummaryRepository->getModel()
//            ->select([
//                'id', 'sl', 'branch_id', 'trans_date', 'total_sales', 'total_return', 'total_net_sales', 'approve',
//                'approve_by', 'created_by'
//            ])
//            ->whereIn('branch_id', $request->get('branch_list'))
//            ->with([
//                'details' => function ($query) use ($requestProducts, $requestBrands) {
//                    return $query->select([
//                        'trans_id', 'trans_sl', 'trans_date', 'branch_id', 'brand_id', 'product_id', 'sales_tp',
//                        'sales_vat', 'sales_discount', 'sales_sp_disc', 'sales_total', 'return_tp', 'return_vat',
//                        'return_discount', 'return_sp_disc', 'return_total', 'net_sales_tp', 'net_sales_vat',
//                        'net_sales_discount', 'net_sales_sp_disc', 'net_sales_total', 'op_out', 'close_out'
//                    ])
//                        ->whereIn('product_id', $requestProducts->pluck('id'))
//                        ->whereIn('brand_id', $requestBrands->pluck('id'));
//                }
//            ])
//            ->whereBetween('trans_date', [$fromDate->toDateString(), $toDate->toDateString()])
////            ->whereBetween('trans_date', ['2019-07-01', $toDate->toDateString()])
//            ->cursor();
//
//        $dateWiseReceipts = $this->dateWiseReceiptSummaryRepository->getModel()
//            ->select(['id', 'sl', 'trans_date', 'branch_id', 'approve', 'approve_by', 'created_by'])
//            ->with([
//                'details' => function ($query) use ($requestProducts, $requestBrands) {
//                    return $query->select([
//                        'trans_id', 'trans_sl', 'trans_date', 'branch_id', 'brand_id', 'product_id', 'collection',
//                        'money_receipt', 'adv_receipt', 'short_receipt', 'adv_adj', 'adj_plus', 'adj_minus'
//                    ])
//                        ->whereIn('product_id', $requestProducts->pluck('id'))
//                        ->whereIn('brand_id', $requestBrands->pluck('id'));
//                }
//            ])
////            ->whereIn('product_id', $requestProducts->pluck('id'))
////            ->whereIn('brand_id', $requestBrands->pluck('id'))
//            ->whereIn('branch_id', $requestBranches->pluck('id'))
//            ->whereBetween('trans_date', [$fromDate->toDateString(), $toDate->toDateString()])
////            ->whereBetween('trans_date', ['2019-07-01', $toDate->toDateString()])
//            ->cursor();
//
//        //for closing only
//        $receiptsClosing = $this->dateWiseReceiptSummaryRepository->getModel()
//            ->select(['id', 'sl', 'trans_date', 'branch_id', 'approve', 'approve_by', 'created_by'])
//            ->with([
//                'details' => function ($query) use ($requestProducts, $requestBrands) {
//                    return $query->select([
//                        'trans_id', 'trans_sl', 'trans_date', 'branch_id', 'brand_id', 'product_id', 'collection',
//                        'money_receipt', 'adv_receipt', 'short_receipt', 'adv_adj', 'adj_plus', 'adj_minus'
//                    ])
//                        ->whereIn('product_id', $requestProducts->pluck('id'))
//                        ->whereIn('brand_id', $requestBrands->pluck('id'));
//                }
//            ])
//            ->whereIn('branch_id', $requestBranches->pluck('id'))
////            ->whereBetween('trans_date', [$initialOpeningDay->toDateString(), $fromDate->copy()->subDay()->toDateString()])
//            ->whereBetween('trans_date', [$fromDate->toDateString(), $fromDate->copy()->subDay()->toDateString()])
//            ->cursor();
//
//        //for closing only
//        $outstandingClosing = $this->outstandingSummaryRepository->getModel()
//            ->select([
//                'id', 'sl', 'branch_id', 'trans_date', 'total_sales', 'total_return', 'total_net_sales', 'approve',
//                'approve_by', 'created_by'
//            ])
//            ->whereIn('branch_id', $request->get('branch_list'))
//            ->with([
//                'details' => function ($query) use ($requestProducts, $requestBrands) {
//                    return $query->select([
//                        'trans_id', 'trans_sl', 'trans_date', 'branch_id', 'brand_id', 'product_id', 'sales_tp',
//                        'sales_vat', 'sales_discount', 'sales_sp_disc', 'sales_total', 'return_tp', 'return_vat',
//                        'return_discount', 'return_sp_disc', 'return_total', 'net_sales_tp', 'net_sales_vat',
//                        'net_sales_discount', 'net_sales_sp_disc', 'net_sales_total', 'op_out', 'close_out'
//                    ])
//                        ->whereIn('product_id', $requestProducts->pluck('id'))
//                        ->whereIn('brand_id', $requestBrands->pluck('id'));
//                }
//            ])
//            ->whereBetween('trans_date', [$fromDate->toDateString(), $closingDate->toDateString()])
//            ->cursor();
//
//
//
//        //generate report
//        $rows = new Collection();
//        foreach ($requestBranches as $branch) {
//            $row1 = new Collection();
//            //for initial opening
//            $initialOpeningSummaryBranch = $initialOpening->where('branch_id', $branch->id);
//            $initialOpeningDetailsBranch = $initialOpeningSummaryBranch->pluck('details')->first();
//
//            //for outstanding closing
//            $outstandingClosingSummaryBranch = $outstandingClosing->where('branch_id', $branch->id);
//            $outstandingClosingDetailsBranch = $outstandingClosingSummaryBranch->pluck('details');
//
//            //for receipt closing
//            $receiptsClosingSummaryBranch = $receiptsClosing->where('branch_id', $branch->id);
//            $receiptClosingDetailsBranch = $receiptsClosingSummaryBranch->pluck('details');
//
//            //recent
//            $outstandingSummaryBranch = $outstanding->where('branch_id', $branch->id);
//            $outstandingDetailsBranch = $outstandingSummaryBranch->pluck('details');
//
//            $receiptSummaryBranch = $dateWiseReceipts->where('branch_id', $branch->id);
//            $receiptDetailsBranch = $receiptSummaryBranch->pluck('details');
//
//            foreach ($requestProducts as $product) {
//                $row2 = new Collection();
//                $row2->put('brand', $product->name);
//
//                //for initial opening
//                $initialOpeningDetailsProducts = $initialOpeningDetailsBranch->where('product_id', $product->id);
//
//                //for outstanding closing
//                $outstandingClosingDetailsProducts = $this->_getMap($outstandingClosingDetailsBranch, $product->id);
//
//                //for receipt closing
//                $receiptClosingDetailsProducts = $this->_getMap($receiptClosingDetailsBranch, $product->id);
//
//                //for regular
//                $outstandingDetailsProducts = $this->_getMap($outstandingDetailsBranch, $product->id);
//                $receiptDetailsProducts = $this->_getMap($receiptDetailsBranch, $product->id);
//
//                //for opening
//                $salesTp = $this->_getSum($outstandingClosingDetailsProducts, 'sales_tp');
//                $salesVat = $this->_getSum($outstandingClosingDetailsProducts, 'sales_vat');
//                $salesDiscount = $this->_getSum($outstandingClosingDetailsProducts, 'sales_discount');
//                $salesSpDiscount = $this->_getSum($outstandingClosingDetailsProducts, 'sales_sp_disc');
//                $returnTp = $this->_getSum($outstandingClosingDetailsProducts, 'return_tp');
//                $returnVat = $this->_getSum($outstandingClosingDetailsProducts, 'return_vat');
//                $returnDiscount = $this->_getSum($outstandingClosingDetailsProducts, 'return_discount');
//                $returnSpDiscount = $this->_getSum($outstandingClosingDetailsProducts, 'return_sp_disc');
//
//                $salesValue = $salesTp + $salesVat - $salesDiscount - $salesSpDiscount;
//                $returnValue = $returnTp + $returnVat - $returnDiscount - $returnSpDiscount;
//
//                $netSales = $salesValue - $returnValue;
//
//                $advReceipt = $this->_getSum($receiptClosingDetailsProducts, 'adv_receipt');
//                $adjPositive = $this->_getSum($receiptClosingDetailsProducts, 'adj_plus');
//                $adjNegative = $this->_getSum($receiptClosingDetailsProducts, 'adj_minus');
//
//                $netCollection = $this->_getSum($receiptClosingDetailsProducts, 'money_receipt');
//
//                $firstOpening = $this->_getSum($initialOpeningDetailsProducts, 'outstanding');
//
//                $openingBalance = $firstOpening + $netSales + $adjPositive - $adjNegative - $netCollection;
//
//
//
//                //daily receipt
//                $salesTp = $this->_getSum($outstandingDetailsProducts, 'sales_tp');
//                $salesVat = $this->_getSum($outstandingDetailsProducts, 'sales_vat');
//                $salesDiscount = $this->_getSum($outstandingDetailsProducts, 'sales_discount');
//                $salesSpDiscount = $this->_getSum($outstandingDetailsProducts, 'sales_sp_disc');
//                $returnTp = $this->_getSum($outstandingDetailsProducts, 'return_tp');
//                $returnVat = $this->_getSum($outstandingDetailsProducts, 'return_vat');
//                $returnDiscount = $this->_getSum($outstandingDetailsProducts, 'return_discount');
//                $returnSpDiscount = $this->_getSum($outstandingDetailsProducts, 'return_sp_disc');
//
//                $salesValue = $salesTp + $salesVat - $salesDiscount - $salesSpDiscount;
//                $returnValue = $returnTp + $returnVat - $returnDiscount - $returnSpDiscount;
//
//                $netSales = $salesValue - $returnValue;
//                $advReceipt = $this->_getSum($receiptDetailsProducts, 'adv_receipt');
//                $adjPositive = $this->_getSum($receiptDetailsProducts, 'adj_plus');
//                $adjNegative = $this->_getSum($receiptDetailsProducts, 'adj_minus');
//
//                $netCollection = $this->_getSum($receiptDetailsProducts, 'money_receipt');
//
//                $closingBalance = $openingBalance + $netSales + $adjPositive - $adjNegative - $netCollection;
//
//
//                 $row2->put('opening', $openingBalance);
//                 $row2->put('sales_tp', $salesTp);
//                 $row2->put('sales_vat', $salesVat);
//                 $row2->put('sales_discount', $salesDiscount);
//                 $row2->put('sales_sp_disc', $salesSpDiscount);
//                 $row2->put('return_tp', $returnTp);
//                 $row2->put('return_vat', $returnVat);
//                 $row2->put('return_discount', $returnDiscount);
//                 $row2->put('return_sp_disc', $returnSpDiscount);
//
//                 $row2->put('closing', $closingBalance);
//
//                 $row1->put($product->name, $row2);
//            }
//
//            $rows->put($branch->name, $row1);
//
//        }


        if ($module_name_singular === null) {
            throw new GeneralException('No transaction found!');
        }
        $reportView = view($module_view.'.report',
            compact('module_name', $module_name_singular, 'module_name_singular', 'module_parent',
                'module_icon', 'page_heading', 'module_action', 'module_view', 'module_route'))
            ->with('report_name', $report_name)
            ->with('branches', $requestBranches)
            ->with('products', $requestProducts)
            ->with('rows', $rows)
            ->with('from_date', $fromDate)
            ->with('to_date', $toDate);

        if (request()->has('export')) {
            $exportType = request()->get('export');
            $exportAction = request()->get('action');

            $exportBlade = $module_view.'.'.$report_name;
            $exportData = $reportView->getData();

            switch ($exportType) {
//                case 'excel':
//                    $exportExtension = 'xlsx';
//                    break;
//
//                case 'xls':
//                    $exportExtension = 'xls';
//                    break;

                case 'pdf':
                    $exportExtension = 'pdf';
                    break;

                case 'csv':
                case 'excel':
                case 'xls':
                    $exportExtension = 'csv';
                    break;

                default:
                    $exportExtension = 'xls';

            }

            return Excel::download(new ReportExport($exportBlade, $exportData), $page_heading.'.'.$exportExtension);
        }

        return $reportView;
    }

    /**
     * @param $collection
     * @param $productId
     * @return mixed
     */
    public function _getFilterBranch($collection, $branchId)
    {
        return $collection->filter(function ($item) use ($branchId) {
            return $item->branch_id == $branchId;
        });
    }

    /**
     * @param $collection
     * @param $productId
     * @return mixed
     */
    private function _getMap($collection, $productId)
    {
        return $collection->map(function ($item) use ($productId) {
            return $item->where('product_id', $productId);
        });
    }

    /**
     * @param $collection
     * @param $productId
     * @return Collection
     */
    private function _getDetails($collection, $productId)
    {
        //first get details from each summary collection
        $details = new Collection();
        $collection->each(function ($items) use ($details, $productId) {
            //               $details = $collection->details->filter(function ($item) use ($product){
//                    return $item->product_id = $product->id;
//                });
            $details->push($items->details->flatten()->where('product_id', $productId));
        });
        return $details->flatten();
    }
}
