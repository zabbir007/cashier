<?php

namespace App\Http\Controllers\Backend\Report\BackdateTransactionReport;

use App\Exceptions\GeneralException;
use App\Exports\ReportExport;
use App\Http\Controllers\BaseController;
use App\Repositories\Backend\Setup\Branch\BranchRepository;
use App\Repositories\Backend\Setup\Opening\OpeningSummaryRepository;
use App\Repositories\Backend\Transaction\DateWise\DateWiseDetailsRepository;
use App\Repositories\Backend\Transaction\DateWise\DateWiseSummaryRepository;
use App\Repositories\Backend\Transaction\OutstandingReconciliation\OutstandingReconciliationSummaryRepository;
use Arr;
use Carbon\Carbon;
use Carbon\CarbonInterval;
use Carbon\CarbonPeriod;
use Excel;
use Illuminate\Http\Response;
use Illuminate\Support\Collection;


/**
 * Class TransactionRestrictionReportController
 *
 * @package App\Http\Controllers\Backend\Report\ConsolidateBankReconciliationStatement
 */
final class BackdateTransactionReportController extends BaseController
{

    public function __construct()
    {
        $this->module_parent = 'Backdated Transaction Report';
        $this->module_name = 'backdated_transaction_report';
        $this->module_icon = 'mdi-hospital-building';
        $this->module_route = 'admin.reports.'.$this->module_name;
        $this->module_view = 'backend.reports.'.$this->module_name;

//        \Config::set('app.env', 'local');
//        \Config::set('app.debug', true);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param $encodedReq
     * @return Response|\Maatwebsite\Excel\BinaryFileResponse
     * @throws GeneralException
     */
    public function show($encodedReq)
    {
        $module_parent = $this->module_parent;
        $module_name = $this->module_name;
        $module_name_singular = str_singular($this->module_name);
        $module_icon = $this->module_icon;
        $module_action = 'Save';
        $module_route = $this->module_route;
        $module_view = $this->module_view;

        $page_heading = $this->module_parent;

        //convert to collection from request
        $request = new Collection(json_decode(decryptString($encodedReq), true));

        $transactionTypes = [
            0 => 'Date Wise Receipt',
            1 => 'Bank Charges',
           // 2 => 'Petty Cash',
            3 => 'Deposits',
            4 => 'Outstanding Reconciliation',
        ];

        //branches
        $requestBranches = app(BranchRepository::class)->getModel()
            ->select(['id', 'name', 'code', 'short_name'])
            ->when($request->get('branch_list', false), function ($q) use ($request) {
                $q->whereIn('id', $request->get('branch_list'));
            })
            ->get();

        $fromDate = Carbon::parse($request->get('from_date'));
        $toDate = Carbon::parse($request->get('to_date'));

//        $_activityLogs = Activity::query()
//            ->where('subject_type', 'like', '%TransactionSetting%')
//            //  ->latest()
//            //->whereBetween('created_at', [Carbon::now()->subDays(7)->toDateString(), Carbon::now()->toDateString()])
//            ->whereBetween('created_at', [$fromDate->toDateString(), $toDate->toDateString()])
//            //   ->where('from_date','>', '2019-09-21')
//            ->get();
        // dd($_activityLogs);
//
//        $activityLogs = new Collection();
//        $transactions = new Collection();
////
////        foreach ($_activityLogs as $id => $_activityLog) {
////            //$log = $_activityLog->first();
////            $subject = $_activityLog->subject;
////            $changes = $_activityLog->changes['attributes'];
////
////            $activityFrom = \Arr::has($changes, 'from_date') ? Carbon::parse(\Arr::get($changes, 'from_date')) : null;
////            $activityTo = \Arr::has($changes, 'to_date') ? Carbon::parse(\Arr::get($changes, 'to_date')) : null;
////
////            $settings = TransactionSetting::query()
////                ->where('id', $_activityLog->subject_id)
////                ->first();
////
////            if ($activityFrom) {
////                \DB::enableQueryLog();
////
////                $ids = DB::table('transactions_date_wise_details')
////                    ->selectRaw('DISTINCT(`trans_id`),`trans_sl`,DATEDIFF(date(`updated_at`),date(`created_at`)) as diff')
////                    ->whereRaw("date(`created_at`) <> date(`updated_at`) and `deleted_at` is null and DATE(`updated_at`) BETWEEN '".$activityFrom->toDateString()."' and '".$activityTo->toDateString()."'")
////                    ->having('diff', '>', 3)
////                    ->pluck('trans_id');
////
////                foreach ($ids as $id) {
////                    $createdData = \DB::table('transactions_date_wise_details')
////                        ->where('trans_id', $id)
////                        ->orderBy('created_at')
////                        ->select('trans_sl','branch_id','brand_id','product_id', DB::raw('date(created_at) as created_at, date(updated_at) as updated_at'), DB::raw('sum(collection) as collection, sum(money_receipt) as money_receipt, sum(adv_receipt) as adv_receipt, sum(short_receipt) as short_receipt, sum(adv_adj) as adv_adj, sum(adj_plus) as adj_plus, sum(adj_minus) as adj_minus'))
////                        ->groupBy('trans_sl','branch_id','brand_id','product_id','created_at','updated_at')
////                        //    ->where(\DB::raw('date(created_at)'), '=', \DB::raw('date(updated_at)'))
////                    //    ->whereNull('updated_at')
////                        //->with('summary')
////                        ->get();
////                   //     ->groupBy('trans_sl');
////
//////                    $updatedData = app(DateWiseDetailsRepository::class)->where('trans_id', $id)
//////                        ->whereBetween('updated_at', [$fromDate->toDateString(), $toDate->toDateString()])
//////                        ->orderBy('updated_at')
//////                        //   ->with('summary')
//////                        ->get()
//////                        ->groupBy('trans_sl');
////
////                    $transactions->push($createdData);
////                  //  $transactions->put('update_data', $updatedData);
////                }
////
//////                $transactions = Activity::query()
//////                    ->whereBetween(\DB::raw('date(updated_at)'),
//////                        [$activityFrom->toDateString(), $activityTo->toDateString()])
//////                    //->where('description', '<>',  'updated')
//////                    ->where('description', 'like', '%updated')
//////                    ->where('subject_type', 'not like', '%ImprestBankReconciliation%')
//////                    ->where('causer_id', $settings->user_id)
//////                    ->whereNotIn('id', $_activityLogs->pluck('id'))
//////                    ->latest('updated_at')
//////                    ->get();
////
////                //  dd($transactions->toArray(), \DB::getQueryLog());
////
////                $activityLogs->push($transactions);
////
////                //   dd($activityFrom, $activityTo);
////
//////            $user = app(UserRepository::class)->getById($subject->user_id, ['id', 'first_name', 'last_name']);
//////            $branch = app(BranchRepository::class)->getById($subject->branch_id, ['id', 'name', 'short_name']);
////////            $a = Activity::query()
////////                ->where('causer_id', $subject->user_id)
////////                ->latest()
////////                ->whereBetween('created_at', ['2019-12-25', '2020-01-05'])
////////                ->get();
//////              //  ->groupBy('subject_id');
//////            //dd($a->toArray(), $a->first()->subject);
//////
//////            $activityLog = new Collection();
//////            $activityLog->put('user', $user->fullname);
//////            $activityLog->put('branch', $branch->name);
//////            $activityLog->put('total', $_activityLog->count());
//////
//////           // $lastFrom =  $log->changes->get('attributes')['from_date'] ? Carbon::parse($log->changes->get('attributes')['from_date'])->format('d M Y') : null;
//////       //     $lastTo =  $log->changes->get('attributes')['to_date'] ? Carbon::parse($log->changes->get('attributes')['to_date'])->format('d M Y') : null;
//////
//////        //   $activityLog->put('last_from',$lastFrom);
//////       //    $activityLog->put('last_to', $lastTo);
//////
//////           $activityLogs->push($activityLog);
////
////                //   return $this->branchWiseOutstanding($requestBranches->pluck('id'), $activityFrom, $activityTo);
////            }
//////
////        }
//
//        dd($transactions->toArray());
        //$transactions = collect();


//        if ($module_name_singular === null) {
//            throw new GeneralException('No transaction found!');
//        }

        $oldTransactions = collect();
        $modifiedTransactions = collect();

        $i = $request->get('transaction');
        if ($i == '0') {
            $reportName = 'datewise_receipt';
            $page_heading = 'Backdate Daily Receipt Report';
            //for old
            $ids = \DB::table('transactions_date_wise_details')
                ->selectRaw('DISTINCT(`trans_id`),DATEDIFF(date(`updated_at`),date(`created_at`)) as diff')
                ->whereRaw("date(`created_at`) <> date(`updated_at`) and `deleted_at` is null and DATE(`created_at`) <= '".$toDate->toDateString()."'")
                ->where('year', $toDate->year)
                ->having('diff', '>', (int) config('application.allowed_previous_days'))
                ->pluck('trans_id');

            $oldTransactions = \DB::table('transactions_date_wise_details')
                ->selectRaw('CONCAT(`branch_name`, `brand_name`, `product_name`) AS UNIQ')
                ->selectRaw('`branch_name`, `brand_name`, `product_name`,`trans_sl`')
                ->selectRaw('sum(`collection`) as collection, sum(`money_receipt`) as money_receipt,sum(`adv_receipt`) as adv_receipt,sum(`short_receipt`) as short_receipt, sum(`adv_adj`) as adv_adj, sum(`adj_plus`) as adj_plus, sum(`adj_minus`) as adj_minus')
                ->whereIn('trans_id', $ids)
                ->groupBy('UNIQ', 'branch_name', 'brand_name', 'product_name','trans_sl')
                ->get()
                ->keyBy('UNIQ');

            //for modified data
            $ids = \DB::table('transactions_date_wise_details')
                ->selectRaw('DISTINCT(`trans_id`),DATEDIFF(date(`updated_at`),date(`created_at`)) as diff')
                ->whereRaw("date(`created_at`) <> date(`updated_at`) and `deleted_at` is null and DATE(`updated_at`) >= '".$fromDate->toDateString()."'")
                ->where('year', $toDate->year)
                ->having('diff', '>', (int) config('application.allowed_previous_days'))
                ->pluck('trans_id');

            $modifiedTransactions = \DB::table('transactions_date_wise_details')
                ->selectRaw('CONCAT(`branch_name`, `brand_name`, `product_name`) AS UNIQ')
                ->selectRaw('`branch_name`, `brand_name`, `product_name`,`trans_sl`')
                ->selectRaw('sum(`collection`) as collection, sum(`money_receipt`) as money_receipt,sum(`adv_receipt`) as adv_receipt,sum(`short_receipt`) as short_receipt, sum(`adv_adj`) as adv_adj, sum(`adj_plus`) as adj_plus, sum(`adj_minus`) as adj_minus')
                ->whereRaw(" DATE(`updated_at`) >= '".$fromDate->toDateString()."'")
                ->whereIn('trans_id', $ids)
                ->groupBy('UNIQ', 'branch_name', 'brand_name', 'product_name','trans_sl')
                ->orderBy('branch_name','asc')
                ->orderBy('product_name','asc')
                ->get()
                ->keyBy('UNIQ');

        } elseif ($i == '1') {
            $reportName = 'bank_charges';
            $page_heading = 'Backdate Bank Charges Report';
            //for old
            $ids = \DB::table('transactions_bank_charge_details')
                ->selectRaw('DISTINCT(`trans_id`),DATEDIFF(date(`updated_at`),date(`created_at`)) as diff')
                ->whereRaw("date(`created_at`) <> date(`updated_at`) and `deleted_at` is null and DATE(`created_at`) <= '".$toDate->toDateString()."'")
                ->where('year', $toDate->year)
                ->having('diff', '>', (int) config('application.allowed_previous_days'))
                ->pluck('trans_id');

            $oldTransactions = \DB::table('transactions_bank_charge_details')
                ->selectRaw('CONCAT(`branch_name`, `bank_name`) AS UNIQ')
                ->selectRaw('`branch_name`, `bank_name`,`trans_sl`')
                ->selectRaw('sum(`amount`) as amount')
                ->whereIn('trans_id', $ids)
                ->groupBy('UNIQ', 'branch_name', 'bank_name','trans_sl')
                ->get()
                ->keyBy('UNIQ');

            //for modified data
            $ids = \DB::table('transactions_bank_charge_details')
                ->selectRaw('DISTINCT(`trans_id`),DATEDIFF(date(`updated_at`),date(`created_at`)) as diff')
                ->whereRaw("date(`created_at`) <> date(`updated_at`) and `deleted_at` is null and DATE(`updated_at`) >= '".$fromDate->toDateString()."'")
                ->where('year', $toDate->year)
                ->having('diff', '>', (int) config('application.allowed_previous_days'))
                ->pluck('trans_id');

            $modifiedTransactions = \DB::table('transactions_bank_charge_details')
                ->selectRaw('CONCAT(`branch_name`, `bank_name`) AS UNIQ')
                ->selectRaw('`branch_name`, `bank_name`,`trans_sl`')
                ->selectRaw('sum(`amount`) as amount')
                ->whereRaw(" DATE(`updated_at`) >= '".$fromDate->toDateString()."'")
                ->whereIn('trans_id', $ids)
                ->groupBy('UNIQ', 'branch_name', 'bank_name','trans_sl')
                ->orderBy('branch_name','asc')
                ->orderBy('bank_name','asc')
                ->get()
                ->keyBy('UNIQ');

        } elseif ($i == '2') {
            $reportName = 'petty_cash';
            $page_heading = 'Backdate Petty Cash Report';


        } elseif ($i == '3') {
            $reportName = 'deposits';
            $page_heading = 'Backdate Deposits Report';

            //for old
            $ids = \DB::table('transactions_deposit_details')
                ->selectRaw('DISTINCT(`trans_id`),DATEDIFF(date(`updated_at`),date(`created_at`)) as diff')
                ->whereRaw("date(`created_at`) <> date(`updated_at`) and `deleted_at` is null and DATE(`created_at`) <= '".$toDate->toDateString()."'")
                ->where('year', $toDate->year)
                ->having('diff', '>', (int) config('application.allowed_previous_days'))
                ->pluck('trans_id');

            $oldTransactions = \DB::table('transactions_deposit_details')
                ->selectRaw('CONCAT(`branch_name`, `brand_name`, `product_name`) AS UNIQ')
                ->selectRaw('`branch_name`, `brand_name`, `product_name`,`trans_sl`')
                ->selectRaw('sum(`amount`) as amount')
                ->whereIn('trans_id', $ids)
                ->groupBy('UNIQ', 'branch_name', 'brand_name', 'product_name','trans_sl')
                ->get()
                ->keyBy('UNIQ');

            //for modified data
            $ids = \DB::table('transactions_deposit_details')
                ->selectRaw('DISTINCT(`trans_id`),DATEDIFF(date(`updated_at`),date(`created_at`)) as diff')
                ->whereRaw("date(`created_at`) <> date(`updated_at`) and `deleted_at` is null and DATE(`updated_at`) >= '".$fromDate->toDateString()."'")
                ->where('year', $toDate->year)
                ->having('diff', '>', (int) config('application.allowed_previous_days'))
                ->pluck('trans_id');

            $modifiedTransactions = \DB::table('transactions_deposit_details')
                ->selectRaw('CONCAT(`branch_name`, `brand_name`, `product_name`) AS UNIQ')
                ->selectRaw('`branch_name`, `brand_name`, `product_name`,`trans_sl`')
                ->selectRaw('sum(`amount`) as amount')
                ->whereRaw(" DATE(`updated_at`) >= '".$fromDate->toDateString()."'")
                ->whereIn('trans_id', $ids)
                ->groupBy('UNIQ', 'branch_name', 'brand_name', 'product_name','trans_sl')
                ->orderBy('branch_name','asc')
                ->orderBy('product_name','asc')
                ->get()
                ->keyBy('UNIQ');

        } elseif ($i == '4') {
            $reportName = 'outstanding_reconciliation';
            $page_heading = 'Backdated Outstanding Reconciliation Report';

            //for old
            $ids = \DB::table('transactions_os_reconciliation_details')
                ->selectRaw('DISTINCT(`trans_id`),DATEDIFF(date(`updated_at`),date(`created_at`)) as diff')
                ->whereRaw("date(`created_at`) <> date(`updated_at`) and `deleted_at` is null and DATE(`created_at`) <= '".$toDate->toDateString()."'")
                ->where('year', $toDate->year)
                ->having('diff', '>', (int) config('application.allowed_previous_days'))
                ->pluck('trans_id');

            $oldTransactions = \DB::table('transactions_os_reconciliation_details')
                ->selectRaw('CONCAT(`branch_name`, `brand_name`, `product_name`) AS UNIQ')
                ->selectRaw('`branch_name`, `brand_name`, `product_name`,`trans_sl`')
                ->selectRaw('sum(sales_tp) as sales_tp, sum(sales_vat) as sales_vat, sum(sales_discount) as sales_discount, sum(sales_sp_disc) as sales_sp_disc,sum(sales_total) as sales_total, sum(return_tp) as return_tp, sum(return_vat) as return_vat, sum(return_total) as return_total, sum(return_sp_disc) as return_sp_disc, sum(return_discount) as return_discount, sum(return_total) as return_total, sum(net_sales_tp) as net_sales_tp, sum(net_sales_vat) as net_sales_vat, sum(net_sales_discount) as net_sales_discount, sum(net_sales_sp_disc) as net_sales_sp_disc, sum(net_sales_total) as net_sales_total')
                ->whereIn('trans_id', $ids)
                ->groupBy('UNIQ', 'branch_name', 'brand_name', 'product_name','trans_sl')
                ->get()
                ->keyBy('UNIQ');

            //for modified data
            $ids = \DB::table('transactions_os_reconciliation_details')
                ->selectRaw('DISTINCT(`trans_id`),DATEDIFF(date(`updated_at`),date(`created_at`)) as diff')
                ->whereRaw("date(`created_at`) <> date(`updated_at`) and `deleted_at` is null and DATE(`updated_at`) >= '".$fromDate->toDateString()."'")
                ->where('year', $toDate->year)
                ->having('diff', '>', (int) config('application.allowed_previous_days'))
                ->pluck('trans_id');

            $modifiedTransactions = \DB::table('transactions_os_reconciliation_details')
                ->selectRaw('CONCAT(`branch_name`, `brand_name`, `product_name`) AS UNIQ')
                ->selectRaw('`branch_name`, `brand_name`, `product_name`,`trans_sl`')
                ->selectRaw('sum(sales_tp) as sales_tp, sum(sales_vat) as sales_vat, sum(sales_discount) as sales_discount, sum(sales_sp_disc) as sales_sp_disc,sum(sales_total) as sales_total, sum(return_tp) as return_tp, sum(return_vat) as return_vat, sum(return_total) as return_total, sum(return_sp_disc) as return_sp_disc, sum(return_discount) as return_discount, sum(return_total) as return_total, sum(net_sales_tp) as net_sales_tp, sum(net_sales_vat) as net_sales_vat, sum(net_sales_discount) as net_sales_discount, sum(net_sales_sp_disc) as net_sales_sp_disc, sum(net_sales_total) as net_sales_total')
                ->whereRaw(" DATE(`updated_at`) >= '".$fromDate->toDateString()."'")
                ->whereIn('trans_id', $ids)
                ->groupBy('UNIQ', 'branch_name', 'brand_name', 'product_name','trans_sl')
                ->orderBy('branch_name','asc')
                ->orderBy('product_name','asc')
                ->get()
                ->keyBy('UNIQ');
        } else {
            $reportName = '';
        }

        if ($reportName == ''){
            throw new GeneralException('Please select transaction type!');
        }

        $reportView = view($module_view.'.report',
            compact('module_name', 'module_name_singular', 'module_parent',
                'module_icon', 'page_heading', 'module_action', 'module_view', 'module_route'))
            ->with('report_name', $reportName)
            ->with('from_date', $fromDate)
            ->with('to_date', $toDate)
            ->with('old_transactions', $oldTransactions)
            ->with('modified_transactions', $modifiedTransactions);

        if (request()->has('export')) {
            $exportType = request()->get('export');
            $exportAction = request()->get('action');

            $exportBlade = $module_view.'.report_partial';
            $exportData = $reportView->getData();

            switch ($exportType) {
//                case 'excel':
//                    $exportExtension = 'xlsx';
//                    break;
//
//                case 'xls':
//                    $exportExtension = 'xls';
//                    break;

                case 'pdf':
                    $exportExtension = 'pdf';
                    break;

                case 'csv':
                case 'excel':
                case 'xls':
                    $exportExtension = 'csv';
                    break;

                default:
                    $exportExtension = 'xls';

            }

              return Excel::download(new ReportExport($exportBlade, $exportData), $page_heading.'.'.$exportExtension);
//            return Excel::create($page_heading, function ($excel) use ($exportBlade, $exportData) {
//                $excel->sheet('sheet1', function ($sheet) use ($exportBlade, $exportData) {
//                    $sheet->loadView($exportBlade, $exportData);
//                    $sheet->setAutosize(true);
//                });
//            })->export();
        }

        return $reportView;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param $encodedReq
     * @return Response
     * @throws GeneralException
     */
    public function branchWiseOutstanding($branchIds, $fromDate, $toDate)
    {
        $module_parent = 'Outstanding Reconciliation Report';
        $module_name = 'outstanding';
        $module_icon = 'mdi-hospital-building';
        $module_route = 'admin.reports.'.$module_name;
        $module_view = 'backend.reports.'.$module_name;

        $report_name = 'branch_wise_outstanding';

        $page_heading = $this->module_parent;

        //branches
        $requestBranches = app(BranchRepository::class)->getModel()
            ->select(['id', 'name', 'code', 'short_name'])
            ->whereIn('id', $branchIds)
            ->get();

//        //brands
//        $requestProducts = $this->productRepository->getModel()
//            ->select(['id', 'name', 'code', 'short_name', 'brand_id'])
//            ->whereIn('id', $request->get('brand_list'))
//            // ->whereIn('brand_id', $requestBrands->pluck('id'))
//            ->get();
//
//        //segments
//        $requestBrands = $this->brandRepository->getModel()
//            ->select(['id', 'name', 'code'])
//            ->whereIn('id', $requestProducts->pluck('brand_id'))
//            ->get();


        $outstandingRevisedOpeningDate = Carbon::parse('2019-07-01');

        //for outstanding revised opening date
        if ($outstandingRevisedOpeningDate->diff($fromDate)->invert === 1) {
            $fromDate = $outstandingRevisedOpeningDate;
        }

        $interval = CarbonInterval::createFromDateString('1 day');
        $dates = new CarbonPeriod($fromDate, $interval, $toDate);

        $branchName = ($requestBranches->count() > 15) ? 'All' : implode(',',
            $requestBranches->pluck('name')->toArray());

        //get initial openings month and year
        $firstInitialOpening = app(OpeningSummaryRepository::class)->getModel()
            ->select(['month', 'year'])
            ->whereIn('branch_id', $requestBranches->pluck('id'))
            ->orderByDesc('id')
            ->first();

        $initialOpeningMonth = $firstInitialOpening->month;
        $initialOpeningYear = $firstInitialOpening->year;
        $initialOpeningDay = Carbon::createFromDate($initialOpeningYear, $initialOpeningMonth, '01')->lastOfMonth();

        $initialOpening =
            app(OpeningSummaryRepository::class)
                ->whereIn('branch_id', $requestBranches->pluck('id'))
                ->orderBy('id')
//            ->with([
//                'details' => function ($query) use ($requestProducts, $requestBrands) {
//                    return $query->whereIn('product_id', $requestProducts->pluck('id'))
//                        ->whereIn('brand_id', $requestBrands->pluck('id'));
//                }
//            ])
                ->get()
                ->toArray();

        $outstanding = app(OutstandingReconciliationSummaryRepository::class)
            ->select([
                'id', 'sl', 'branch_id', 'trans_date', 'total_sales', 'total_return', 'total_net_sales', 'approve',
                'approve_by', 'created_by'
            ])
            ->whereIn('branch_id', $branchIds)
//            ->with([
//                'details' => function ($query) use ($requestProducts, $requestBrands) {
//                    return $query->select([
//                        'trans_id', 'trans_sl', 'trans_date', 'branch_id', 'brand_id', 'product_id', 'sales_tp',
//                        'sales_vat', 'sales_discount', 'sales_sp_disc', 'sales_total', 'return_tp', 'return_vat',
//                        'return_discount', 'return_sp_disc', 'return_total', 'net_sales_tp', 'net_sales_vat',
//                        'net_sales_discount', 'net_sales_sp_disc', 'net_sales_total', 'op_out', 'close_out'
//                    ])
//                        ->whereIn('product_id', $requestProducts->pluck('id'))
//                        ->whereIn('brand_id', $requestBrands->pluck('id'));
//                }
//            ])
            ->whereBetween('trans_date', [$fromDate->toDateString(), $toDate->toDateString()])
//            ->whereBetween('trans_date', ['2019-07-01', $toDate->toDateString()])
            ->get()
            ->toArray();

        $dateWiseReceipts = app(DateWiseDetailsRepository::class)
            // ->whereIn('product_id', $requestProducts->pluck('id'))
            //    ->whereIn('brand_id', $requestBrands->pluck('id'))
            ->whereIn('branch_id', $branchIds)
            ->whereBetween('trans_date', [$fromDate->toDateString(), $toDate->toDateString()])
//            ->whereBetween('trans_date', ['2019-07-01', $toDate->toDateString()])
            ->get()
            ->toArray();

        //for closing only
        $receiptsClosing = app(DateWiseSummaryRepository::class)
            ->select(['id', 'sl', 'trans_date', 'branch_id', 'approve', 'approve_by', 'created_by'])
//            ->with([
//                'details' => function ($query) use ($requestProducts, $requestBrands) {
//                    return $query->select([
//                        'trans_id', 'trans_sl', 'trans_date', 'branch_id', 'brand_id', 'product_id', 'collection',
//                        'money_receipt', 'adv_receipt', 'short_receipt', 'adv_adj', 'adj_plus', 'adj_minus'
//                    ])
//                        ->whereIn('product_id', $requestProducts->pluck('id'))
//                        ->whereIn('brand_id', $requestBrands->pluck('id'));
//                }
//            ])
            ->whereIn('branch_id', $requestBranches->pluck('id'))
//            ->whereBetween('trans_date', [$initialOpeningDay->toDateString(), $fromDate->copy()->subDay()->toDateString()])
            ->whereBetween('trans_date',
                [$outstandingRevisedOpeningDate->toDateString(), $fromDate->copy()->subDay()->toDateString()])
            ->get()
            ->flatten();


        //for closing only
        $outstandingClosing = app(OutstandingReconciliationSummaryRepository::class)
            ->select([
                'id', 'sl', 'branch_id', 'trans_date', 'total_sales', 'total_return', 'total_net_sales', 'approve',
                'approve_by', 'created_by'
            ])
            ->whereIn('branch_id', $branchIds)
//            ->with([
//                'details' => function ($query) use ($requestProducts, $requestBrands) {
//                    return $query->select([
//                        'trans_id', 'trans_sl', 'trans_date', 'branch_id', 'brand_id', 'product_id', 'sales_tp',
//                        'sales_vat', 'sales_discount', 'sales_sp_disc', 'sales_total', 'return_tp', 'return_vat',
//                        'return_discount', 'return_sp_disc', 'return_total', 'net_sales_tp', 'net_sales_vat',
//                        'net_sales_discount', 'net_sales_sp_disc', 'net_sales_total', 'op_out', 'close_out'
//                    ])
//                        ->whereIn('product_id', $requestProducts->pluck('id'))
//                        ->whereIn('brand_id', $requestBrands->pluck('id'));
//                }
//            ])
//            ->whereBetween('trans_date', [$initialOpeningDay->toDateString(), $fromDate->copy()->subDay()->toDateString()])
            ->whereBetween('trans_date',
                [$outstandingRevisedOpeningDate->toDateString(), $fromDate->copy()->subDay()->toDateString()])
            ->get()
            ->toArray();


//        $reportView = view($module_view.'.report',
//            compact('module_name', 'module_name_singular', 'module_parent',
//                'module_icon', 'page_heading', 'module_action', 'module_view', 'module_route'))
//            ->with('report_name', $report_name)
//            ->with('branches', $requestBranches)
//            ->with('branch_name', $branchName)
//            ->with('dates', $dates)
//            ->with('initial_opening', $initialOpening->groupBy('branch_id'))
//            ->with('outstanding', $outstanding->groupBy('branch_id'))
//            ->with('receipts', $dateWiseReceipts->groupBy('branch_id'))
//            ->with('receipt_closing', $receiptsClosing->groupBy('branch_id'))
//            ->with('outstanding_closing', $outstandingClosing->groupBy('branch_id'))
//            //   ->with('brands', $requestBrands)
//            //   ->with('products', $requestProducts)
//            ->with('fromDate', $fromDate)
//            ->with('toDate', $toDate);


        $reports = collect();

        foreach ($requestBranches as $branch) {
            $report = collect();

            $_first_opening = $_net_sales = $_adj_positive = $_adj_negative = $_net_collection = 0;
            $closing_balance = $current_opening = 0;

            $br_wise_opening = Arr::where($initialOpening, function ($value, $key) use ($branch) {
                return $key === $branch->id;
            });
            $br_wise_outstanding_closing = Arr::where($outstandingClosing, function ($value, $key) use ($branch
            ) {
                return $key === $branch->id;
            });

            $br_wise_receipt_closing = Arr::where($receiptsClosing, function ($value, $key) use ($branch) {
                return $key === $branch->id;
            });

            $_first_opening = $br_wise_opening ? $br_wise_opening->sum('total_outstanding') : 0;

            $_net_sales = ($br_wise_outstanding_closing ? $br_wise_outstanding_closing->sum('sales_total_net') : 0) - ($br_wise_outstanding_closing ? $br_wise_outstanding_closing->sum('return_total_net') : 0);
            $_adj_positive = $br_wise_receipt_closing ? $br_wise_receipt_closing->sum('total_adj_positive') : 0;
            $_adj_negative = $br_wise_receipt_closing ? $br_wise_receipt_closing->sum('total_adj_negative') : 0;
            $_net_collection = $br_wise_receipt_closing ? $br_wise_receipt_closing->sum('total_money_receipt') : 0;

            $current_opening = $_first_opening + $_net_sales + $_adj_positive - $_adj_negative - $_net_collection;
            // dd($current_opening, $_first_opening, $_net_sales, $_net_collection);

            $br_wise_outstanding = $outstanding->filter(function ($value, $key) use ($branch) {
                return $key === $branch->id;
            })->first();
            $br_wise_receipts = $dateWiseReceipts->filter(function ($value, $key) use ($branch) {
                return $key === $branch->id;
            })->first();


            foreach ($dates as $date) {
                $closing_balance = $current_opening = 0;

                $_date = $date->toDateString();

                $sales = $br_wise_outstanding ? $br_wise_outstanding->groupBy('trans_date')->filter(function (
                    $value,
                    $key
                ) use ($_date) {
                    return $key === $_date;
                })->first() : collect();

                //$current_opening = $current_opening + $sales->sum('net_sales_total') - $sales->sum('collection') + $sales->sum('adj_plus') - $sales->sum('adj_minus');

                //dump($sales->sum('net_sales_total'),$sales->sum('collection'),$sales->sum('adj_plus'),$sales->sum('adj_plus'),$sales->sum('adj_minus'));
                $sales_outstanding = $br_wise_outstanding ? $br_wise_outstanding->groupBy('trans_date')->filter(function (
                    $value,
                    $key
                ) use ($_date) {
                    return $key === $_date;
                })->first() : collect();

                $sales_tp = $sales_outstanding ? $sales_outstanding->sum('sales_total_tp') : 0;
                $sales_vat = $sales_outstanding ? $sales_outstanding->sum('sales_total_vat') : 0;
                $sales_disc = $sales_outstanding ? $sales_outstanding->sum('sales_total_discount') : 0;
                $sales_sp_disc = $sales_outstanding ? $sales_outstanding->sum('sales_total_sp_discount') : 0;
                $sales = $sales_tp + $sales_vat - $sales_disc - $sales_sp_disc;

                $returns_tp = $sales_outstanding ? $sales_outstanding->sum('return_total_tp') : 0;
                $returns_vat = $sales_outstanding ? $sales_outstanding->sum('return_total_vat') : 0;
                $returns_disc = $sales_outstanding ? $sales_outstanding->sum('return_total_discount') : 0;
                $returns_sp_disc = $sales_outstanding ? $sales_outstanding->sum('return_total_sp_discount') : 0;
                // $returns = $sales_outstanding->sum('return_total_net');
                $returns = $returns_tp + $returns_vat - $returns_disc - $returns_sp_disc;

                $net_sales_tp = $sales_tp - $returns_tp;
                $net_sales_vat = $sales_vat - $returns_vat;
                $net_sales_disc = $sales_disc - $returns_disc;
                $net_sales_sp_disc = $sales_sp_disc - $returns_sp_disc;
                $net_sales = $sales - $returns;

                $receipt = $br_wise_receipts ? $br_wise_receipts->groupBy('trans_date')->filter(function (
                    $value,
                    $key
                ) use ($_date) {
                    return $key === $_date;
                })->first() : collect();

                $adv_receipt = $receipt ? $receipt->sum('adv_receipt') : 0;
                $adj_positive = $receipt ? $receipt->sum('adj_plus') : 0;
                $adj_negative = $receipt ? $receipt->sum('adj_minus') : 0;

                $net_collection = $receipt ? $receipt->sum('money_receipt') : 0;

                $report->put('date', $date->format('d-m-Y'));
                $report->put('branch', $branch->name);
                $report->put('current_opening', $current_opening);
                $report->put('current_opening', $current_opening);
                $report->put('sales_tp', $sales_tp);
                $report->put('sales_vat', $sales_vat);
                $report->put('sales_disc', $sales_disc);
                $report->put('sales_sp_disc', $sales_sp_disc);
                $report->put('sales', $sales);
                $report->put('returns_tp', $returns_tp);
                $report->put('returns_vat', $returns_vat);
                $report->put('returns_disc', $returns_disc);
                $report->put('returns_sp_disc', $returns_sp_disc);
                $report->put('returns', $returns);
                $report->put('net_sales_tp', $net_sales_tp);
                $report->put('net_sales_vat', $net_sales_vat);
                $report->put('net_sales_disc', $net_sales_disc);
                $report->put('net_sales_sp_disc', $net_sales_sp_disc);
                $report->put('net_sales', $net_sales);
                $report->put('net_collection', $net_collection);
                $report->put('adj_positive', $adj_positive);
                $report->put('adj_negative', $adj_negative);
                $report->put('adj_negative', $adj_negative);


                $closing_balance = $current_opening + $net_sales + $adj_positive - $adj_negative - $net_collection;
                $current_opening = $closing_balance;

                $report->put('closing_balance', $closing_balance);
            }

            $reports->put($branch->name, $report);

        }

        dd($reports);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param $encodedReq
     * @return Response
     * @throws GeneralException
     */
    public function branchWiseOutstanding1($branchIds, $fromDate, $toDate)
    {
        $module_parent = 'Outstanding Reconciliation Report';
        $module_name = 'outstanding';
        $module_icon = 'mdi-hospital-building';
        $module_route = 'admin.reports.'.$module_name;
        $module_view = 'backend.reports.'.$module_name;

        $report_name = 'branch_wise_outstanding';

        $page_heading = $this->module_parent;

        //branches
        $requestBranches = app(BranchRepository::class)->getModel()
            ->select(['id', 'name', 'code', 'short_name'])
            ->whereIn('id', $branchIds)
            ->get();

//        //brands
//        $requestProducts = $this->productRepository->getModel()
//            ->select(['id', 'name', 'code', 'short_name', 'brand_id'])
//            ->whereIn('id', $request->get('brand_list'))
//            // ->whereIn('brand_id', $requestBrands->pluck('id'))
//            ->get();
//
//        //segments
//        $requestBrands = $this->brandRepository->getModel()
//            ->select(['id', 'name', 'code'])
//            ->whereIn('id', $requestProducts->pluck('brand_id'))
//            ->get();


        $outstandingRevisedOpeningDate = Carbon::parse('2019-07-01');

        //for outstanding revised opening date
        if ($outstandingRevisedOpeningDate->diff($fromDate)->invert === 1) {
            $fromDate = $outstandingRevisedOpeningDate;
        }

        $interval = CarbonInterval::createFromDateString('1 day');
        $dates = new CarbonPeriod($fromDate, $interval, $toDate);

        $branchName = ($requestBranches->count() > 15) ? 'All' : implode(',',
            $requestBranches->pluck('name')->toArray());

        //get initial openings month and year
        $firstInitialOpening = app(OpeningSummaryRepository::class)->getModel()
            ->select(['month', 'year'])
            ->whereIn('branch_id', $requestBranches->pluck('id'))
            ->orderByDesc('id')
            ->first();

        $initialOpeningMonth = $firstInitialOpening->month;
        $initialOpeningYear = $firstInitialOpening->year;
        $initialOpeningDay = Carbon::createFromDate($initialOpeningYear, $initialOpeningMonth, '01')->lastOfMonth();

        $initialOpening = app(OpeningSummaryRepository::class)->getModel()
            ->whereIn('branch_id', $requestBranches->pluck('id'))
            ->orderBy('id')
//            ->with([
//                'details' => function ($query) use ($requestProducts, $requestBrands) {
//                    return $query->whereIn('product_id', $requestProducts->pluck('id'))
//                        ->whereIn('brand_id', $requestBrands->pluck('id'));
//                }
//            ])
            ->get();

        $outstanding = app(OutstandingReconciliationSummaryRepository::class)->getModel()
            ->select([
                'id', 'sl', 'branch_id', 'trans_date', 'total_sales', 'total_return', 'total_net_sales', 'approve',
                'approve_by', 'created_by'
            ])
            ->whereIn('branch_id', $branchIds)
//            ->with([
//                'details' => function ($query) use ($requestProducts, $requestBrands) {
//                    return $query->select([
//                        'trans_id', 'trans_sl', 'trans_date', 'branch_id', 'brand_id', 'product_id', 'sales_tp',
//                        'sales_vat', 'sales_discount', 'sales_sp_disc', 'sales_total', 'return_tp', 'return_vat',
//                        'return_discount', 'return_sp_disc', 'return_total', 'net_sales_tp', 'net_sales_vat',
//                        'net_sales_discount', 'net_sales_sp_disc', 'net_sales_total', 'op_out', 'close_out'
//                    ])
//                        ->whereIn('product_id', $requestProducts->pluck('id'))
//                        ->whereIn('brand_id', $requestBrands->pluck('id'));
//                }
//            ])
            ->whereBetween('trans_date', [$fromDate->toDateString(), $toDate->toDateString()])
//            ->whereBetween('trans_date', ['2019-07-01', $toDate->toDateString()])
            ->get();

        $dateWiseReceipts = app(DateWiseDetailsRepository::class)->getModel()
            // ->whereIn('product_id', $requestProducts->pluck('id'))
            //    ->whereIn('brand_id', $requestBrands->pluck('id'))
            ->whereIn('branch_id', $branchIds)
            ->whereBetween('trans_date', [$fromDate->toDateString(), $toDate->toDateString()])
//            ->whereBetween('trans_date', ['2019-07-01', $toDate->toDateString()])
            ->get();

        //for closing only
        $receiptsClosing = app(DateWiseSummaryRepository::class)->getModel()
            ->select(['id', 'sl', 'trans_date', 'branch_id', 'approve', 'approve_by', 'created_by'])
//            ->with([
//                'details' => function ($query) use ($requestProducts, $requestBrands) {
//                    return $query->select([
//                        'trans_id', 'trans_sl', 'trans_date', 'branch_id', 'brand_id', 'product_id', 'collection',
//                        'money_receipt', 'adv_receipt', 'short_receipt', 'adv_adj', 'adj_plus', 'adj_minus'
//                    ])
//                        ->whereIn('product_id', $requestProducts->pluck('id'))
//                        ->whereIn('brand_id', $requestBrands->pluck('id'));
//                }
//            ])
            ->whereIn('branch_id', $requestBranches->pluck('id'))
//            ->whereBetween('trans_date', [$initialOpeningDay->toDateString(), $fromDate->copy()->subDay()->toDateString()])
            ->whereBetween('trans_date',
                [$outstandingRevisedOpeningDate->toDateString(), $fromDate->copy()->subDay()->toDateString()])
            ->get();

        //for closing only
        $outstandingClosing = app(OutstandingReconciliationSummaryRepository::class)->getModel()
            ->select([
                'id', 'sl', 'branch_id', 'trans_date', 'total_sales', 'total_return', 'total_net_sales', 'approve',
                'approve_by', 'created_by'
            ])
            ->whereIn('branch_id', $branchIds)
//            ->with([
//                'details' => function ($query) use ($requestProducts, $requestBrands) {
//                    return $query->select([
//                        'trans_id', 'trans_sl', 'trans_date', 'branch_id', 'brand_id', 'product_id', 'sales_tp',
//                        'sales_vat', 'sales_discount', 'sales_sp_disc', 'sales_total', 'return_tp', 'return_vat',
//                        'return_discount', 'return_sp_disc', 'return_total', 'net_sales_tp', 'net_sales_vat',
//                        'net_sales_discount', 'net_sales_sp_disc', 'net_sales_total', 'op_out', 'close_out'
//                    ])
//                        ->whereIn('product_id', $requestProducts->pluck('id'))
//                        ->whereIn('brand_id', $requestBrands->pluck('id'));
//                }
//            ])
//            ->whereBetween('trans_date', [$initialOpeningDay->toDateString(), $fromDate->copy()->subDay()->toDateString()])
            ->whereBetween('trans_date',
                [$outstandingRevisedOpeningDate->toDateString(), $fromDate->copy()->subDay()->toDateString()])
            ->get();


        $reportView = view($module_view.'.report',
            compact('module_name', 'module_name_singular', 'module_parent',
                'module_icon', 'page_heading', 'module_action', 'module_view', 'module_route'))
            ->with('report_name', $report_name)
            ->with('branches', $requestBranches)
            ->with('branch_name', $branchName)
            ->with('dates', $dates)
            ->with('initial_opening', $initialOpening->groupBy('branch_id'))
            ->with('outstanding', $outstanding->groupBy('branch_id'))
            ->with('receipts', $dateWiseReceipts->groupBy('branch_id'))
            ->with('receipt_closing', $receiptsClosing->groupBy('branch_id'))
            ->with('outstanding_closing', $outstandingClosing->groupBy('branch_id'))
            //   ->with('brands', $requestBrands)
            //   ->with('products', $requestProducts)
            ->with('fromDate', $fromDate)
            ->with('toDate', $toDate);

//        dd($outstanding->groupBy('branch_id'), $dateWiseReceipts->groupBy('branch_id') );

        if (request()->has('export')) {
            $exportType = request()->get('export');
            $exportAction = request()->get('action');

            $exportBlade = $module_view.'.'.$report_name;
            $exportData = $reportView->getData();

            switch ($exportType) {
//                case 'excel':
//                    $exportExtension = 'xlsx';
//                    break;
//
//                case 'xls':
//                    $exportExtension = 'xls';
//                    break;

                case 'pdf':
                    $exportExtension = 'pdf';
                    break;

                case 'csv':
                case 'excel':
                case 'xls':
                    $exportExtension = 'csv';
                    break;

                default:
                    $exportExtension = 'xls';

            }

            return Excel::download(new ReportExport($exportBlade, $exportData), $page_heading.'.'.$exportExtension);
        }

        return $reportView;
    }
}
