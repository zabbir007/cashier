<?php

namespace App\Http\Controllers\Backend\Report\TransactionRestrictionReport;

use App\Exceptions\GeneralException;
use App\Exports\ReportExport;
use App\Http\Controllers\BaseController;
use App\Repositories\Backend\Access\User\UserRepository;
use App\Repositories\Backend\Setup\Branch\BranchRepository;
use Carbon\Carbon;
use Excel;
use Illuminate\Http\Response;
use Illuminate\Support\Collection;
use Spatie\Activitylog\Models\Activity;
use View;


/**
 * Class TransactionRestrictionReportController
 *
 * @package App\Http\Controllers\Backend\Report\ConsolidateBankReconciliationStatement
 */
final class TransactionRestrictionReportController extends BaseController
{

    public function __construct()
    {
        $this->module_parent = 'Transaction Restriction Report';
        $this->module_name = 'transaction_restriction_report';
        $this->module_icon = 'mdi-hospital-building';
        $this->module_route = 'admin.reports.'.$this->module_name;
        $this->module_view = 'backend.reports.'.$this->module_name;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param $encodedReq
     * @return Response
     * @throws GeneralException
     */
    public function show($encodedReq)
    {
        $module_parent = $this->module_parent;
        $module_name = $this->module_name;
        $module_name_singular = str_singular($this->module_name);
        $module_icon = $this->module_icon;
        $module_action = 'Save';
        $module_route = $this->module_route;
        $module_view = $this->module_view;

        $page_heading = $this->module_parent;

        //convert to collection from request
        $request = new Collection(json_decode(decryptString($encodedReq), true));

        $fromDate = Carbon::parse($request->get('from_date'));
        $toDate = Carbon::parse($request->get('to_date'));

        $_activityLogs = Activity::query()
            ->where('subject_type', 'like', '%TransactionSetting%')
            ->latest()
            //->whereBetween('created_at', [Carbon::now()->subDays(7)->toDateString(), Carbon::now()->toDateString()])
            ->whereBetween('created_at', [$fromDate->toDateString(), $toDate->toDateString()])
         //   ->where('from_date','>', '2019-09-21')
            ->get()
            ->groupBy('subject_id');

        $activityLogs = new Collection();

        foreach ($_activityLogs as $id => $_activityLog) {
            $log = $_activityLog->first();
            $subject = $log->subject;

            $user = app(UserRepository::class)->getById($subject->user_id, ['id', 'first_name', 'last_name']);
            $branch = app(BranchRepository::class)->getById($subject->branch_id, ['id', 'name', 'short_name']);
//            $a = Activity::query()
//                ->where('causer_id', $subject->user_id)
//                ->latest()
//                ->whereBetween('created_at', ['2019-12-25', '2020-01-05'])
//                ->get();
              //  ->groupBy('subject_id');
            //dd($a->toArray(), $a->first()->subject);

            $activityLog = new Collection();
            $activityLog->put('user', $user->fullname);
            $activityLog->put('branch', $branch->name);
            $activityLog->put('total', $_activityLog->count());

           // $lastFrom =  $log->changes->get('attributes')['from_date'] ? Carbon::parse($log->changes->get('attributes')['from_date'])->format('d M Y') : null;
       //     $lastTo =  $log->changes->get('attributes')['to_date'] ? Carbon::parse($log->changes->get('attributes')['to_date'])->format('d M Y') : null;

        //   $activityLog->put('last_from',$lastFrom);
       //    $activityLog->put('last_to', $lastTo);

           $activityLogs->push($activityLog);
        }

        if ($module_name_singular === null) {
            throw new GeneralException('No transaction found!');
        }
        $reportView = view($module_view.'.report',
            compact('module_name', $module_name_singular, 'module_name_singular', 'module_parent',
                'module_icon', 'page_heading', 'module_action', 'module_view', 'module_route'))
            ->with('from_date', $fromDate)
            ->with('to_date', $toDate)
            ->with('activity_logs', $activityLogs);

        if (request()->has('export')) {
            $exportType = request()->get('export');
            $exportAction = request()->get('action');

            $exportBlade = $module_view.'.report_partial';
            $exportData = $reportView->getData();

            switch ($exportType) {
//                case 'excel':
//                    $exportExtension = 'xlsx';
//                    break;
//
//                case 'xls':
//                    $exportExtension = 'xls';
//                    break;

                case 'pdf':
                    $exportExtension = 'pdf';
                    break;

                case 'csv':
                case 'excel':
                case 'xls':
                    $exportExtension = 'csv';
                    break;

                default:
                    $exportExtension = 'xls';

            }

            return Excel::download(new ReportExport($exportBlade, $exportData), $page_heading.'.'.$exportExtension);
//            return Excel::create($page_heading, function ($excel) use ($exportBlade, $exportData) {
//                $excel->sheet('sheet1', function ($sheet) use ($exportBlade, $exportData) {
//                    $sheet->loadView($exportBlade, $exportData);
//                    $sheet->setAutosize(true);
//                });
//            })->export();
        }

        return $reportView;
    }
}
