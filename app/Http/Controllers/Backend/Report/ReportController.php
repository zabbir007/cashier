<?php

namespace App\Http\Controllers\Backend\Report;


use App\Exceptions\GeneralException;
use App\Http\Controllers\BaseController;
use App\Repositories\Backend\Setup\Branch\BranchRepository;
use App\Repositories\Backend\Setup\Brand\BrandRepository;
use App\Repositories\Backend\Setup\Opening\OpeningDetailsRepository;
use App\Repositories\Backend\Setup\Product\ProductRepository;
use Carbon\Carbon;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;

/**
 * Class ReportController
 *
 * @package App\Http\Controllers\Backend\Report
 */
final class ReportController extends BaseController
{
    /**
     * @var
     */
    private $branchRepository;

    /**
     * @var OpeningDetailsRepository
     */
    private $openingDetailsRepository;

    /**
     * @var ProductRepository
     */
    private $productRepository;

    /**
     * @var BrandRepository
     */
    private $brandRepository;

    /**
     * ReportController constructor.
     *
     * @param  BranchRepository  $branchRepository
     * @param  OpeningDetailsRepository  $openingDetailsRepository
     * @param  ProductRepository  $productRepository
     * @param  BrandRepository  $brandRepository
     */
    public function __construct(
        BranchRepository $branchRepository,
        OpeningDetailsRepository $openingDetailsRepository,
        ProductRepository $productRepository,
        BrandRepository $brandRepository
    ) {
        $this->branchRepository = $branchRepository;
        $this->openingDetailsRepository = $openingDetailsRepository;
        $this->productRepository = $productRepository;
        $this->brandRepository = $brandRepository;

        $this->module_parent = 'Reports';
        $this->module_name = 'reports';
        $this->module_icon = 'mdi-hospital-building';
        $this->module_route = 'admin.'.$this->module_name;
        $this->module_view = 'backend.'.$this->module_name;
    }

    /**
     * @return mixed
     * @throws GeneralException
     */
    public function index()
    {
        $module_parent = $this->module_parent;
        $module_name = $this->module_name;
        $module_name_singular = str_singular($this->module_name);
        $module_icon = $this->module_icon;
        $module_action = 'Index';
        $module_route = $this->module_route;
        $module_view = $this->module_view;

        $page_heading = 'All '.ucfirst($this->module_name);

        $months = [
            1 => 'January',
            2 => 'February',
            3 => 'March',
            4 => 'April',
            5 => 'May',
            6 => 'June',
            7 => 'July',
            8 => 'August',
            9 => 'September',
            10 => 'October',
            11 => 'November',
            12 => 'December',
        ];

        $transactionTypes = [
            0 => 'Date Wise Receipt',
            1 => 'Bank Charges',
          //  2 => 'Petty Cash',
            3 => 'Deposits',
            4 => 'Outstanding Reconciliation',
          //  5 => 'Imprest Bank Reconciliation',
        ];

        $branches = $this->getBranches();

        return view($module_view.'.index',
            compact('module_name', 'module_name_singular', 'module_parent', 'module_icon',
                'page_heading', 'module_action', 'module_view', 'module_route'))
            ->with('branches', $this->branchRepository->makeModel()->select(['id', 'name'])->whereIn('id',
                $branches->pluck('id'))->where('id', '!=', 1)->pluck('name', 'id'))
//            ->with('branches', $this->branches->pluck('name','id'))
            ->with('years',
                $this->openingDetailsRepository->getModel()->select(['year'])->distinct()->pluck('year', 'year'))
            ->with('months', $months)
            ->with('now', Carbon::now())
            ->with('brands', $this->productRepository->getModel()->select([
                'id', 'name', 'brand_id'
            ])->orderBy('name')->where('status', 1)->get()->toJson())
            ->with('segments',
                $this->brandRepository->getModel()->select(['id', 'name'])->where('status', 1)->get()->toJson())
            ->with('transaction_types', $transactionTypes);
    }

    /**
     * @param  Request  $request
     * @return RedirectResponse
     */
    public function show(Request $request)
    {
        $request = $request->except(['_token']);
        $module_route = $this->module_route;

        return redirect()->route($module_route.'.'.$request['report_name'].'.show',
            ['encodedReq' => encryptString($request)]);

//        $$module_name = $this->repository->getAll();

//        return view($module_view . ".index",
//            compact('module_name', "$module_name", "module_name_singular", 'module_parent','module_icon', 'page_heading', 'module_action', 'module_view', 'module_route'));
    }
}
