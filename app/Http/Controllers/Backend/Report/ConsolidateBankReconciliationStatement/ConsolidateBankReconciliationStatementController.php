<?php

namespace App\Http\Controllers\Backend\Report\ConsolidateBankReconciliationStatement;

use App\Exceptions\GeneralException;
use App\Exports\ReportExport;
use App\Http\Controllers\BaseController;
use App\Repositories\Backend\Setup\Branch\BranchRepository;
use App\Repositories\Backend\Setup\Opening\OpeningSummaryRepository;
use App\Repositories\Backend\Transaction\ImprestBankReconciliation\ImprestBankReconciliationDetailsRepository;
use App\Repositories\Backend\Transaction\ImprestBankReconciliation\ImprestBankReconciliationSummaryRepository;
use App\Repositories\Backend\Transaction\OutstandingReconciliation\OutstandingReconciliationSummaryRepository;
use Carbon\Carbon;
use Excel;
use Illuminate\Http\Response;
use Illuminate\Support\Collection;
use View;

/**
 * Class OutstandingReportController
 *
 * @package App\Http\Controllers\Backend\Report\OutStanding
 */
final class ConsolidateBankReconciliationStatementController extends BaseController
{
    /**
     * @var BranchRepository
     */
    private $branchRepository;

    /**
     * @var ImprestBankReconciliationDetailsRepository
     */
    private $imprestBankReconciliationDetailsRepository;

    /**
     * @var ImprestBankReconciliationSummaryRepository
     */
    private $imprestBankReconciliationSummaryRepository;

    /**
     * @var OutstandingReconciliationSummaryRepository
     */
    private $outstandingSummaryRepository;

    /**
     * @var OpeningSummaryRepository
     */
    private $openingSummaryRepository;

    /**
     * OutstandingReportController constructor.
     *
     * @param  ImprestBankReconciliationDetailsRepository  $imprestBankReconciliationDetailsRepository
     * @param  ImprestBankReconciliationSummaryRepository  $imprestBankReconciliationSummaryRepository
     * @param  BranchRepository                            $branchRepository
     */
    public function __construct(
        ImprestBankReconciliationDetailsRepository $imprestBankReconciliationDetailsRepository,
        ImprestBankReconciliationSummaryRepository $imprestBankReconciliationSummaryRepository,
        BranchRepository $branchRepository
    ) {
        $this->branchRepository = $branchRepository;
        $this->imprestBankReconciliationSummaryRepository = $imprestBankReconciliationSummaryRepository;

        $this->imprestBankReconciliationDetailsRepository = $imprestBankReconciliationDetailsRepository;

        $this->module_parent = 'Consolidate Bank Reconciliation Statement';
        $this->module_name = 'consolidate_bank_reconciliation_statement';
        $this->module_icon = 'mdi-hospital-building';
        $this->module_route = 'admin.reports.'.$this->module_name;
        $this->module_view = 'backend.reports.'.$this->module_name;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param $encodedReq
     * @return Response
     * @throws GeneralException
     */
    public function show($encodedReq)
    {
        ini_set('memory_limit', '2048M');

        $module_parent = $this->module_parent;
        $module_name = $this->module_name;
        $module_name_singular = str_singular($this->module_name);
        $module_icon = $this->module_icon;
        $module_action = 'Save';
        $module_route = $this->module_route;
        $module_view = $this->module_view;

        $page_heading = $this->module_parent;

        //convert to collection from request
        $request = new Collection(json_decode(decryptString($encodedReq), true));

        //branches
        $requestBranches = $this->branchRepository->getModel()
            ->select(['id', 'name', 'code', 'short_name'])
            ->whereIn('id', $request->get('branch_list'))
            ->orderBy('code')
            ->cursor();


        $branchName = ($requestBranches->count() > 15) ? 'All' : implode(',',
            $requestBranches->pluck('name')->toArray());

        $year = $request->get('year');
        $month = $request->get('month');

        $month_name = Carbon::createFromFormat('Y-m', $year.'-'.$month)->format('F');


        $consolidate_bank_reconciliation_statements = $this->imprestBankReconciliationSummaryRepository->getModel()
            ->select([
                'id', 'sl', 'branch_id', 'branch_code', 'opening_balance', 'bank_interest', 'bank_charge',
                'closing_balance', 'closing_per_bank', 'closing_per_bank_book', 'trans_date', 'month', 'year'
            ])
            ->whereIn('branch_id', $request->get('branch_list'))
            ->with([
                'details' => function ($query) use ($year, $month) {
                    return $query->select([
                        'id', 'trans_id', 'trans_sl', 'entry_date', 'branch_id', 'branch_code', 'amount', 'types',
                        'trans_date', 'year', 'month'
                    ])
                        ->where('year', $year)
                        ->where('month', $month);
                }
            ])
            ->with([
                'branch' => function ($q) {
                    return $q->select(['id', 'code', 'name']);
                }
            ])
            ->where('year', $year)
            ->where('month', $month)
            ->orderBy('branch_code')
            ->cursor();


        if ($module_name_singular === null) {
            throw new GeneralException('No transaction found!');
        }
        $reportView = View::make($module_view.'.report',
            compact('module_name', $module_name_singular, 'module_name_singular', 'module_parent',
                'module_icon', 'page_heading', 'module_action', 'module_view', 'module_route'))
            ->with('branches', $requestBranches)
            ->with('branch_name', $branchName)
            ->with('year', $year)
            ->with('month', $month)
            ->with('month_name', $month_name)
            ->with('consolidate_bank_reconciliation_statements', $consolidate_bank_reconciliation_statements);

        if (request()->has('export')) {
            $exportType = request()->get('export');
            $exportAction = request()->get('action');

            $exportBlade = $module_view.'.report_partial';
            $exportData = $reportView->getData();

            switch ($exportType) {
//                case 'excel':
//                    $exportExtension = 'xlsx';
//                    break;
//
//                case 'xls':
//                    $exportExtension = 'xls';
//                    break;

                case 'pdf':
                    $exportExtension = 'pdf';
                    break;

                case 'csv':
                case 'excel':
                case 'xls':
                    $exportExtension = 'csv';
                    break;

                default:
                    $exportExtension = 'xls';

            }

            return Excel::download(new ReportExport($exportBlade, $exportData), $page_heading.'.'.$exportExtension);
        }

        return $reportView;
    }
}
