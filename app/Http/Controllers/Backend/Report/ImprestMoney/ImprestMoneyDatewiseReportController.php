<?php

namespace App\Http\Controllers\Backend\Report\ImprestMoney;

use App\Exceptions\GeneralException;
use App\Exports\ReportExport;
use App\Http\Controllers\BaseController;
use App\Repositories\Backend\Setup\Branch\BranchRepository;
use App\Repositories\Backend\Setup\Brand\BrandRepository;
use App\Repositories\Backend\Setup\Opening\OpeningSummaryRepository;
use App\Repositories\Backend\Setup\Product\ProductRepository;
use App\Repositories\Backend\Transaction\PettyCash\PettyCashSummaryRepository;
use Carbon\Carbon;
use Carbon\CarbonInterval;
use Carbon\CarbonPeriod;
use Excel;
use Illuminate\Http\Response;
use Illuminate\Support\Collection;


/**
 * Class ImprestMoneyReportController
 *
 * @package App\Http\Controllers\Backend\Report\ImprestMoney
 */
final class ImprestMoneyDatewiseReportController extends BaseController
{
    /**
     * @var BrandRepository
     */
    private $brandRepository;
    /**
     * @var ProductRepository
     */
    private $productRepository;
    /**
     * @var BranchRepository
     */
    private $branchRepository;
    /**
     * @var OpeningSummaryRepository
     */
    private $openingSummaryRepository;

    /**
     * @var PettyCashSummaryRepository
     */
    private $pettyCashSummaryRepository;


    /**
     * ImprestMoneyReportController constructor.
     *
     * @param  BrandRepository             $brandRepository
     * @param  ProductRepository           $productRepository
     * @param  BranchRepository            $branchRepository
     * @param  OpeningSummaryRepository    $openingSummaryRepository
     * @param  PettyCashSummaryRepository  $pettyCashSummaryRepository
     */
    public function __construct(
        BrandRepository $brandRepository,
        ProductRepository $productRepository,
        BranchRepository $branchRepository,
        OpeningSummaryRepository $openingSummaryRepository,
        PettyCashSummaryRepository $pettyCashSummaryRepository
    ) {
        $this->brandRepository = $brandRepository;
        $this->productRepository = $productRepository;
        $this->branchRepository = $branchRepository;
        $this->openingSummaryRepository = $openingSummaryRepository;
        $this->pettyCashSummaryRepository = $pettyCashSummaryRepository;

        $this->module_action = '';
        $this->module_parent = 'Imprest Money Reconciliation Report';
        $this->module_name = 'imprest_money_datewise';
        $this->module_icon = 'mdi-hospital-building';
        $this->module_route = 'admin.reports.'.$this->module_name;
        $this->module_view = 'backend.reports.'.$this->module_name;

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param $encodedReq
     * @return Response
     * @throws GeneralException
     */
    public function show($encodedReq)
    {
        ini_set('memory_limit', '2048M');

        $module_parent = $this->module_parent;
        $module_name = $this->module_name;
        $module_name_singular = str_singular($this->module_name);
        $module_icon = $this->module_icon;
        $module_action = 'Save';
        $module_route = $this->module_route;
        $module_view = $this->module_view;

        //convert to collection from request
        $request = new Collection(json_decode(decryptString($encodedReq)));
        $report = $request->get('report_param');

        $page_heading = 'Date wise Imprest Money ('.ucfirst($report).') Reconciliation Report';

        $requestBranches = $this->branchRepository->getModel()
            ->select(['id', 'name', 'code', 'short_name'])
            ->whereIn('id', $request->get('branch_list'))
            ->cursor();

        $requestBrands = $this->brandRepository->getModel()
            ->select(['id', 'name', 'code'])
            ->whereIn('id', $request->get('segment_list'))
            ->cursor();

        $requestProducts = $this->productRepository->getModel()
            ->select(['id', 'name', 'short_name', 'code', 'brand_id'])
            ->whereIn('id', $request->get('brand_list'))
            // ->whereIn('brand_id', $requestBrands->pluck('id'))
            ->cursor();

        $fromDate = Carbon::parse($request->get('from_date'));
        $toDate = Carbon::parse($request->get('to_date'));
        $lastMonth = $toDate->month - 1;
        $lastYear = $toDate->year;
        //when month =1, then closing month = 0, so turn it to month=12 and set year=year-1
        if ($lastMonth == 0) {
            $lastMonth = 12;
            $lastYear = $lastYear - 1;
        }

//        $requestMonth = $toDate->month;
//        $requestYear = $toDate->year;
        $interval = CarbonInterval::createFromDateString('1 day');
        $dates = new CarbonPeriod($fromDate, $interval, $toDate);

        $branchName = ($requestBranches->count() > 15) ? 'All' : implode(',',
            $requestBranches->pluck('name')->toArray());

        //get initial openings month and year
        $firstInitialOpening = $this->openingSummaryRepository->getModel()
            ->select(['month', 'year'])
            ->whereIn('branch_id', $requestBranches->pluck('id'))
            ->orderByDesc('id')
            ->first();
        $initialOpeningMonth = $firstInitialOpening->month;
        $initialOpeningYear = $firstInitialOpening->year;
        $initialOpeningDay = Carbon::createFromDate($initialOpeningYear, $initialOpeningMonth, '01')->lastOfMonth();

        $initialOpening = $this->openingSummaryRepository->getModel()
            ->select(['id', 'sl', 'trans_date', 'branch_id', 'pc_bank_amount', 'pc_cash_amount'])
            ->whereIn('branch_id', $requestBranches->pluck('id'))
            ->orderByDesc('id')
            ->with([
                'details' => function ($query) use ($requestProducts) {
                    return $query->select([
                        'trans_id', 'trans_sl', 'trans_date', 'branch_id', 'brand_id', 'product_id', 'collection',
                        'outstanding', 'pc_bank_amount', 'pc_cash_amount'
                    ])
                        ->whereIn('product_id', $requestProducts->pluck('id'));
                }
            ])
            ->cursor();

        $pettyCash = $this->pettyCashSummaryRepository->getModel()
            ->select(['id', 'sl', 'trans_date', 'branch_id', 'approve', 'approve_by', 'created_by'])
            ->whereIn('branch_id', $request->get('branch_list'))
            ->whereBetween('trans_date', [$fromDate->toDateString(), $toDate->toDateString()])
            ->with([
                'details' => function ($query) use ($request) {
                    return $query->select([
                        'trans_id', 'trans_sl', 'trans_date', 'branch_id', 'total_receipt_hq', 'transfer_imprest_cash',
                        'm_receipt', 'total_expense_cq', 'total_exp'
                    ])
                        ->whereIn('branch_id', $request->get('branch_list'));
                }
            ])
            ->with([
                'createBy' => function ($q) {
                    return $q->select(['id', 'first_name', 'last_name', 'email']);
                }
            ])
            ->cursor();


        //for closing
        $pettyCashClosing = $this->pettyCashSummaryRepository->getModel()
            ->select(['id', 'sl', 'trans_date', 'branch_id', 'approve', 'approve_by', 'created_by'])
            ->whereIn('branch_id', $requestBranches->pluck('id'))
            ->whereBetween('trans_date',
                [$initialOpeningDay->toDateString(), $fromDate->copy()->subDay()->toDateString()])
            ->with([
                'details' => function ($query) use ($request) {
                    return $query->select([
                        'trans_id', 'trans_sl', 'trans_date', 'branch_id', 'total_receipt_hq', 'transfer_imprest_cash',
                        'm_receipt', 'total_expense_cq', 'total_exp'
                    ])
                        ->whereIn('branch_id', $request->get('branch_list'));
                }
            ])
            ->cursor();

//        $dateWiseReceipts = $this->dateWiseReceiptSummaryRepository
////            ->whereHas('details', function ($query) use ($requestProducts, $requestBrands) {
////                $query->whereIn('product_id', $requestProducts->pluck('id'));
////                // ->whereIn('brand_id', $requestBrands->pluck('id'));
////            })
//            ->with(['details' => function ($query) use ($requestProducts, $requestBrands) {
//                return $query->whereIn('product_id', $requestProducts->pluck('id'));
//                //->whereIn('brand_id', $requestBrands->pluck('id'));
//            }])
//            ->whereIn('branch_id', $requestBranches->pluck('id'))
//            ->whereBetween('trans_date', [$fromDate->toDateString(), $toDate->toDateString()])
////            ->where('year', $requestYear)
////            ->where('month', $requestMonth)
//            ->with(['details'])
//            ->cursor();
//
//        $deposits = $this->depositSummaryRepository
////            ->whereHas('details', function ($query) use ($requestProducts, $requestBrands) {
////                $query->whereIn('product_id', $requestProducts->pluck('id'));
////                //  ->whereIn('brand_id', $requestBrands->pluck('id'));
////            })
//            ->with(['details' => function ($query) use ($requestProducts, $requestBrands) {
//                return $query->whereIn('product_id', $requestProducts->pluck('id'));
//                //  ->whereIn('brand_id', $requestBrands->pluck('id'));
//            }])
//            ->whereIn('branch_id', $requestBranches->pluck('id'))
//            ->whereBetween('trans_date', [$fromDate->toDateString(), $toDate->toDateString()])
////            ->where('year', $requestYear)
////            ->where('month', $requestMonth)
//            ->with(['details','createBy'])
//            ->cursor();
//
//
//        //for closing only
//        $receiptsClosing = $this->dateWiseReceiptSummaryRepository
//            ->whereHas('details', function ($query) use ($requestProducts, $requestBrands) {
//                return $query->whereIn('product_id', $requestProducts->pluck('id'));
//                //  ->whereIn('brand_id', $requestBrands->pluck('id'));
//            })
//            ->whereIn('branch_id', $requestBranches->pluck('id'))
//            // ->whereBetween('month', [$initialOpeningMonth + 1, $toDate->month - 1])
//            // ->whereBetween('year', [$initialOpeningYear, $toDate->year])
//            ->whereBetween('trans_date', [$initialOpeningDay->toDateString(), $fromDate->copy()->subDay()->toDateString()])
//            ->cursor();
//
//        //for closing only
//        $depositClosing = $this->depositSummaryRepository
//            ->whereHas('details', function ($query) use ($requestProducts, $requestBrands) {
//                return $query->whereIn('product_id', $requestProducts->pluck('id'));
//                //  ->whereIn('brand_id', $requestBrands->pluck('id'));
//            })
//            ->whereIn('branch_id', $requestBranches->pluck('id'))
//            // ->whereBetween('month', [$initialOpeningMonth + 1, $lastMonth])
//            // ->whereBetween('year', [$initialOpeningYear, $lastYear])
//            ->whereBetween('trans_date', [$initialOpeningDay->toDateString(), $fromDate->copy()->subDay()->toDateString()])
//            ->cursor();

        if ($module_name_singular === null) {
            throw new GeneralException('No transaction found!');
        }

        $reportView = view($module_view.'.report',
            compact('module_name', $module_name_singular, 'module_name_singular', 'module_parent',
                'module_icon', 'page_heading', 'module_action', 'module_view', 'module_route'))
            ->with('report_type', $request->get('report_param'))
            ->with('branches', $requestBranches)
            ->with('branch_name', $branchName)
            ->with('dates', $dates)
            ->with('initial_opening', $initialOpening)
            ->with('petty_cash', $pettyCash)
            ->with('brands', $requestBrands)
            ->with('products', $requestProducts)
            ->with('pettyCashClosing', $pettyCashClosing)
            ->with('to_date', $toDate)
            ->with('report', $report);

        if (request()->has('export')) {
            $exportType = request()->get('export');
            $exportAction = request()->get('action');

            $exportBlade = $module_view.'.'.$report;
            $exportData = $reportView->getData();

            switch ($exportType) {
//                case 'excel':
//                    $exportExtension = 'xlsx';
//                    break;
//
//                case 'xls':
//                    $exportExtension = 'xls';
//                    break;

                case 'pdf':
                    $exportExtension = 'pdf';
                    break;

                case 'csv':
                case 'excel':
                case 'xls':
                    $exportExtension = 'csv';
                    break;

                default:
                    $exportExtension = 'xls';

            }

            return Excel::download(new ReportExport($exportBlade, $exportData), $page_heading.'.'.$exportExtension);
        }

        return $reportView;
    }
}
