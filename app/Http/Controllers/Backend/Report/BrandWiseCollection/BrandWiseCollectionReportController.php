<?php

namespace App\Http\Controllers\Backend\Report\BrandWiseCollection;

use App\Exceptions\GeneralException;
use App\Exports\ReportExport;
use App\Http\Controllers\BaseController;
use App\Repositories\Backend\Setup\Bank\BankRepository;
use App\Repositories\Backend\Setup\Branch\BranchRepository;
use App\Repositories\Backend\Setup\Brand\BrandRepository;
use App\Repositories\Backend\Setup\Opening\OpeningDetailsRepository;
use App\Repositories\Backend\Setup\Opening\OpeningSummaryRepository;
use App\Repositories\Backend\Setup\Product\ProductRepository;
use App\Repositories\Backend\Transaction\BankCharge\BankChargeDetailsRepository;
use App\Repositories\Backend\Transaction\BankCharge\BankChargeSummaryRepository;
use App\Repositories\Backend\Transaction\DateWise\DateWiseDetailsRepository;
use App\Repositories\Backend\Transaction\DateWise\DateWiseSummaryRepository;
use App\Repositories\Backend\Transaction\Deposit\DepositDetailsRepository;
use App\Repositories\Backend\Transaction\Deposit\DepositSummaryRepository;
use Carbon\Carbon;
use Carbon\CarbonInterval;
use Carbon\CarbonPeriod;
use Excel;
use Illuminate\Http\Response;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Cache;


/**
 * Class BrandWiseCollectionReportController
 *
 * @package App\Http\Controllers\Backend\Report\BrandWiseCollection
 */
final class BrandWiseCollectionReportController extends BaseController
{
    /**
     * @var BrandRepository
     */
    private $brandRepository;
    /**
     * @var ProductRepository
     */
    private $productRepository;
    /**
     * @var BranchRepository
     */
    private $branchRepository;
    /**
     * @var DateWiseSummaryRepository
     */
    private $dateWiseReceiptSummaryRepository;

    /**
     * @var
     */
    private $dateWiseReceiptDetailRepository;

    /**
     * @var DepositSummaryRepository
     */
    private $depositSummaryRepository;


    /**
     * @var DepositDetailsRepository
     */
    private $depositDetailRepository;

    /**
     * @var OpeningSummaryRepository
     */
    private $openingSummaryRepository;

    /**
     * @var OpeningDetailsRepository
     */
    private $openingDetailRepository;
    /**
     * @var BankRepository
     */
    private $bankRepository;

    /**
     * @var BankChargeSummaryRepository
     */
    private $bankChargeSummaryRepository;

    /**
     * @var BankChargeDetailsRepository
     */
    private $bankChargeDetailsRepository;

    /**
     * BrandWiseCollectionReportController constructor.
     *
     * @param  DateWiseSummaryRepository    $dateWiseReceiptSummaryRepository
     * @param  DateWiseDetailsRepository    $dateWiseDetailsRepository
     * @param  DepositSummaryRepository     $depositSummaryRepository
     * @param  DepositDetailsRepository     $depositDetailsRepository
     * @param  BankChargeSummaryRepository  $bankChargeSummaryRepository
     * @param  BrandRepository              $brandRepository
     * @param  ProductRepository            $productRepository
     * @param  BranchRepository             $branchRepository
     * @param  BankRepository               $bankRepository
     * @param  OpeningSummaryRepository     $openingSummaryRepository
     * @param  OpeningDetailsRepository     $openingDetailsRepository
     */
    public function __construct(
        DateWiseSummaryRepository $dateWiseReceiptSummaryRepository,
        DateWiseDetailsRepository $dateWiseDetailsRepository,
        DepositSummaryRepository $depositSummaryRepository,
        DepositDetailsRepository $depositDetailsRepository,
        BankChargeSummaryRepository $bankChargeSummaryRepository,
        BankChargeDetailsRepository $bankChargeDetailsRepository,
        BrandRepository $brandRepository,
        ProductRepository $productRepository,
        BranchRepository $branchRepository,
        BankRepository $bankRepository,
        OpeningSummaryRepository $openingSummaryRepository,
        OpeningDetailsRepository $openingDetailsRepository
    ) {
        $this->dateWiseReceiptSummaryRepository = $dateWiseReceiptSummaryRepository;
        $this->dateWiseReceiptDetailRepository = $dateWiseDetailsRepository;
        $this->depositSummaryRepository = $depositSummaryRepository;
        $this->depositDetailRepository = $depositDetailsRepository;

        $this->brandRepository = $brandRepository;
        $this->branchRepository = $branchRepository;
        $this->productRepository = $productRepository;
        $this->bankRepository = $bankRepository;

        $this->bankChargeSummaryRepository = $bankChargeSummaryRepository;
        $this->bankChargeDetailsRepository = $bankChargeDetailsRepository;
        $this->openingSummaryRepository = $openingSummaryRepository;
        $this->openingDetailRepository = $openingDetailsRepository;

        $this->module_parent = 'Brand wise Collection Reconciliation';
        $this->module_name = 'brand_wise_collection';
        $this->module_icon = 'mdi-hospital-building';
        $this->module_route = 'admin.reports.'.$this->module_name;
        $this->module_view = 'backend.reports.'.$this->module_name;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param $encodedReq
     * @return Response
     * @throws GeneralException
     */
    public function show($encodedReq)
    {
//        ini_set('max_execution_time', 400);
        ini_set('memory_limit', '4096M');

        $store_in_cache_for = 120;  //seconds

        $module_parent = $this->module_parent;
        $module_name = $this->module_name;
        $module_name_singular = str_singular($this->module_name);
        $module_icon = $this->module_icon;
        $module_action = 'Save';
        $module_route = $this->module_route;
        $module_view = $this->module_view;

        $page_heading = $this->module_parent;

        //convert to collection from request
        $request = new Collection(json_decode(decryptString($encodedReq)));

        //branches
        $requestBranches = $this->branchRepository->getBuilder()
            ->orderBy('name')
            ->select(['id', 'name', 'code', 'short_name'])
            ->whereIn('id', $request->get('branch_list'))
            ->get();

        //brands
        $requestProducts = $this->productRepository->getBuilder()
            ->select(['id', 'name', 'code', 'short_name', 'brand_id'])
            ->whereIn('id', $request->get('brand_list'))
            // ->whereIn('brand_id', $requestBrands->pluck('id'))
            ->get();


        //segments
        $requestBrands = $this->brandRepository->getBuilder()
            ->select(['id', 'name', 'code'])
            ->whereIn('id', $requestProducts->pluck('brand_id'))
            ->get();

        $fromDate = Carbon::parse($request->get('from_date'));
        $toDate = Carbon::parse($request->get('to_date'));
        $lastMonth = $toDate->month - 1;
        $lastYear = $toDate->year;
        //when month =1, then closing month = 0, so turn it to month=12 and set year=year-1
        if ($lastMonth == 0) {
            $lastMonth = 12;
            $lastYear = $lastYear - 1;
        }

//        $requestMonth = $toDate->month;
//        $requestYear = $toDate->year;
        $interval = CarbonInterval::createFromDateString('1 day');
        $dates = new CarbonPeriod($fromDate, $interval, $toDate);

        $branchName = ($requestBranches->count() > 15) ? 'All' : implode(',',
            $requestBranches->pluck('name')->toArray());

        $banks = Cache::remember('brand_wise_collection_banks', $store_in_cache_for, function () {
            return $this->bankRepository->getAll('name');
        });

//        $fromDate = Carbon::parse($request->get('from_date'));
//        $toDate = Carbon::parse($request->get('to_date'));
//        $requestMonth = $toDate->month;
//        $requestYear = $toDate->year;
//        $interval = \DateInterval::createFromDateString('1 day');
//        $dates = new \DatePeriod($fromDate, $interval, $toDate->copy()->modify('+1 day'));

        //get initial openings month and year
        $firstInitialOpening = $this->openingSummaryRepository->getBuilder()
            ->select(['month', 'year'])
            ->whereIn('branch_id', $requestBranches->pluck('id'))
            ->orderByDesc('id')
            ->first();


        $initialOpeningMonth = $firstInitialOpening->month;
        $initialOpeningYear = $firstInitialOpening->year;
        $initialOpeningDay = Carbon::createFromDate($initialOpeningYear, $initialOpeningMonth, '01')->lastOfMonth();


        $initialOpeningSummary = $this->openingSummaryRepository->getBuilder()
            ->select('id', 'sl', 'trans_date', 'branch_id', 'pc_bank_amount', 'pc_cash_amount')
            ->whereIn('branch_id', $requestBranches->pluck('id'))
            ->orderByDesc('id')
       //    ->groupBy('id', 'sl', 'trans_date', 'branch_id', 'pc_bank_amount', 'pc_cash_amount')
        ->get();

        $initialOpeningDetails = $this->openingDetailRepository->getBuilder()
            ->select(
                'trans_id', 'trans_sl', 'trans_date', 'branch_id', 'brand_id', 'product_id', 'collection',
                'outstanding', 'pc_bank_amount', 'pc_cash_amount'
            )
            ->whereIn('trans_id', $initialOpeningSummary->pluck('id'))
            //   ->whereIn('trans_sl', $initialOpeningSummary->pluck('sl'))
            ->whereIn('branch_id', $requestBranches->pluck('id'))
            ->whereIn('product_id', $requestProducts->pluck('id'))
            ->orderByDesc('id')
//           ->groupBy('trans_id', 'trans_sl', 'trans_date', 'branch_id', 'brand_id', 'product_id', 'collection',
//               'outstanding', 'pc_bank_amount', 'pc_cash_amount')
            ->get();

        //date wise receipt
        $dateWiseReceiptsDetails = $this->dateWiseReceiptDetailRepository->getBuilder()
            ->select(
                'transactions_date_wise_details.trans_id', 'transactions_date_wise_details.trans_sl',
                'transactions_date_wise_details.trans_date', 'transactions_date_wise_details.branch_id',
                'transactions_date_wise_details.brand_id', 'transactions_date_wise_details.product_id',
                'transactions_date_wise_details.collection', 'transactions_date_wise_details.money_receipt',
                'transactions_date_wise_details.adv_receipt', 'transactions_date_wise_details.short_receipt',
                'transactions_date_wise_details.adv_adj', 'transactions_date_wise_details.adj_plus',
                'transactions_date_wise_details.adj_minus', 'transactions_date_wise_summary.approve_by',
                'transactions_date_wise_summary.created_by'
            )
            ->join('transactions_date_wise_summary', function ($q) {
                $q->on('transactions_date_wise_summary.id', '=', 'transactions_date_wise_details.trans_id')
                    ->on('transactions_date_wise_summary.sl', '=', 'transactions_date_wise_details.trans_sl');
            })
//            ->join('products', 'products.id', '=', 'transactions_date_wise_details.product_id')
//            ->join('brands', 'brands.id', '=', 'transactions_date_wise_details.brand_id')
//            ->join('branches', 'branches.id', '=', 'transactions_date_wise_details.branch_id')
            ->whereIn('transactions_date_wise_details.product_id', $requestProducts->pluck('id'))
            ->whereIn('transactions_date_wise_details.brand_id', $requestBrands->pluck('id'))
            ->whereIn('transactions_date_wise_details.branch_id', $requestBranches->pluck('id'))
            ->whereBetween('transactions_date_wise_summary.trans_date',
                [$fromDate->toDateString(), $toDate->toDateString()])
            //deleted at not working in join
            //savar branch opening problem
            ->whereNull('transactions_date_wise_summary.deleted_at')
            ->whereNull('transactions_date_wise_details.deleted_at')
//            ->when($request->has('showBackTrans'), function ($q){
//                $q->whereRaw('(DATE(DATE_ADD(`transactions_date_wise_details`.`trans_date`, INTERVAL -'.config('application.allowed_previous_days').' DAY)) < date(`transactions_date_wise_details`.`updated_at`) and date(`transactions_date_wise_details`.`trans_date`) <> date(`transactions_date_wise_details`.`created_at`))');
//            })

       // dd($dateWiseReceiptsDetails);
//            ->groupBy('transactions_date_wise_details.trans_id', 'transactions_date_wise_details.trans_sl',
//                'transactions_date_wise_details.trans_date', 'transactions_date_wise_details.branch_id',
//                'transactions_date_wise_details.brand_id', 'transactions_date_wise_details.product_id',
//                'transactions_date_wise_details.collection', 'transactions_date_wise_details.money_receipt',
//                'transactions_date_wise_details.adv_receipt', 'transactions_date_wise_details.short_receipt',
//                'transactions_date_wise_details.adv_adj', 'transactions_date_wise_details.adj_plus',
//                'transactions_date_wise_details.adj_minus', 'transactions_date_wise_summary.approve_by',
//                'transactions_date_wise_summary.created_by')
            ->get();

        //deposits
        $depositDetails = $this->depositDetailRepository->getBuilder()
            ->select(
                'transactions_deposit_details.trans_id', 'transactions_deposit_details.trans_sl',
                'transactions_deposit_details.trans_date', 'transactions_deposit_details.branch_id',
                'transactions_deposit_details.brand_id', 'transactions_deposit_details.product_id',
                'transactions_deposit_details.bank_id', 'transactions_deposit_details.amount',
                'transactions_deposit_summary.approve_by', 'transactions_deposit_summary.created_by'
            )
            ->join('transactions_deposit_summary', function ($q) {
                $q->on('transactions_deposit_summary.id', '=', 'transactions_deposit_details.trans_id')
                    ->on('transactions_deposit_summary.sl', '=', 'transactions_deposit_details.trans_sl');
            })
            ->join('products', 'products.id', '=', 'transactions_deposit_details.product_id')
            ->join('brands', 'brands.id', '=', 'transactions_deposit_details.brand_id')
            ->join('branches', 'branches.id', '=', 'transactions_deposit_details.branch_id')
            ->whereIn('transactions_deposit_details.product_id', $requestProducts->pluck('id'))
            ->whereIn('transactions_deposit_details.brand_id', $requestBrands->pluck('id'))
            ->whereIn('transactions_deposit_details.branch_id', $requestBranches->pluck('id'))
            //deleted at not working in join
            //savar branch opening problem
            ->whereNull('transactions_deposit_details.deleted_at')
            ->whereBetween('transactions_deposit_details.trans_date',
                [$fromDate->toDateString(), $toDate->toDateString()])
//            ->when($request->has('showBackTrans'), function ($q){
//                $q->where(\DB::raw('DATE(DATE_ADD(transactions_deposit_details.trans_date, INTERVAL -'.config('application.allowed_previous_days').' DAY)) < date(transactions_deposit_details.updated_at) and date(transactions_deposit_details.trans_date) <> date(transactions_deposit_details.created_at)'));
//            })
//            ->groupBy(
//                'transactions_deposit_details.trans_id', 'transactions_deposit_details.trans_sl',
//                'transactions_deposit_details.trans_date', 'transactions_deposit_details.branch_id',
//                'transactions_deposit_details.brand_id', 'transactions_deposit_details.product_id',
//                'transactions_deposit_details.bank_id', 'transactions_deposit_details.amount',
//                'transactions_deposit_summary.approve_by', 'transactions_deposit_summary.created_by'
//            )
            ->get();

        //bank charges
//        $bankChargeSummary = $this->bankChargeSummaryRepository->getBuilder()
//            ->select(['id', 'sl', 'trans_date', 'branch_id', 'approve', 'approve_by', 'created_by'])
//            ->whereIn('branch_id', $requestBranches->pluck('id'))
//            ->whereBetween('trans_date', [$fromDate->toDateString(), $toDate->toDateString()])
//            ->cursor();

        $bankChargeDetails = $this->bankChargeDetailsRepository->getBuilder()
            ->select('trans_id', 'trans_sl', 'branch_id', 'bank_id', 'amount', 'trans_date')
            //     ->whereIn('trans_id', $bankChargeSummary->pluck('id'))
            ->whereIn('branch_id', $requestBranches->pluck('id'))
            ->whereBetween('trans_date', [$fromDate->toDateString(), $toDate->toDateString()])
           // ->groupBy('trans_id', 'trans_sl', 'branch_id', 'bank_id', 'amount', 'trans_date')
            ->get();


        //closing
        //for closing only
        $receiptsClosingDetails = $this->dateWiseReceiptDetailRepository->getBuilder()
            ->select(
                'transactions_date_wise_details.trans_id', 'transactions_date_wise_details.trans_sl',
                'transactions_date_wise_details.trans_date', 'transactions_date_wise_details.branch_id',
                'transactions_date_wise_details.brand_id', 'transactions_date_wise_details.product_id',
                'transactions_date_wise_details.collection', 'transactions_date_wise_details.money_receipt',
                'transactions_date_wise_details.adv_receipt', 'transactions_date_wise_details.short_receipt',
                'transactions_date_wise_details.adv_adj', 'transactions_date_wise_details.adj_plus',
                'transactions_date_wise_details.adj_minus', 'transactions_date_wise_summary.approve_by',
                'transactions_date_wise_summary.created_by'
            )
            ->join('transactions_date_wise_summary', function ($q) {
                $q->on('transactions_date_wise_summary.id', '=', 'transactions_date_wise_details.trans_id')
                    ->on('transactions_date_wise_summary.sl', '=', 'transactions_date_wise_details.trans_sl');
            })
//            ->join('products', 'products.id', '=', 'transactions_date_wise_details.product_id')
//            ->join('brands', 'brands.id', '=', 'transactions_date_wise_details.brand_id')
//            ->join('branches', 'branches.id', '=', 'transactions_date_wise_details.branch_id')
            ->whereIn('transactions_date_wise_details.product_id', $requestProducts->pluck('id'))
            ->whereIn('transactions_date_wise_details.brand_id', $requestBrands->pluck('id'))
            ->whereIn('transactions_date_wise_details.branch_id', $requestBranches->pluck('id'))
            ->whereBetween('transactions_date_wise_details.trans_date',
                [$initialOpeningDay->toDateString(), $fromDate->copy()->subDay()->toDateString()])
            //deleted at not working in join
            //savar branch opening problem
            ->whereNull('transactions_date_wise_details.deleted_at')
//            ->groupBy('transactions_date_wise_details.trans_id', 'transactions_date_wise_details.trans_sl',
//                'transactions_date_wise_details.trans_date', 'transactions_date_wise_details.branch_id',
//                'transactions_date_wise_details.brand_id', 'transactions_date_wise_details.product_id',
//                'transactions_date_wise_details.collection', 'transactions_date_wise_details.money_receipt',
//                'transactions_date_wise_details.adv_receipt', 'transactions_date_wise_details.short_receipt',
//                'transactions_date_wise_details.adv_adj', 'transactions_date_wise_details.adj_plus',
//                'transactions_date_wise_details.adj_minus', 'transactions_date_wise_summary.approve_by',
//                'transactions_date_wise_summary.created_by')
            ->get();

        //for closing only
        $depositClosingDetails = $this->depositDetailRepository->getBuilder()
            ->select(
                'transactions_deposit_details.trans_id', 'transactions_deposit_details.trans_sl',
                'transactions_deposit_details.trans_date', 'transactions_deposit_details.branch_id',
                'transactions_deposit_details.brand_id', 'transactions_deposit_details.product_id',
                'transactions_deposit_details.bank_id', 'transactions_deposit_details.amount',
                'transactions_deposit_summary.approve_by', 'transactions_deposit_summary.created_by'
            )
            ->join('transactions_deposit_summary', function ($q) {
                $q->on('transactions_deposit_summary.id', '=', 'transactions_deposit_details.trans_id')
                    ->on('transactions_deposit_summary.sl', '=', 'transactions_deposit_details.trans_sl');
            })
            ->join('products', 'products.id', '=', 'transactions_deposit_details.product_id')
            ->join('brands', 'brands.id', '=', 'transactions_deposit_details.brand_id')
            ->join('branches', 'branches.id', '=', 'transactions_deposit_details.branch_id')
            ->whereIn('transactions_deposit_details.product_id', $requestProducts->pluck('id'))
            ->whereIn('transactions_deposit_details.brand_id', $requestBrands->pluck('id'))
            ->whereIn('transactions_deposit_details.branch_id', $requestBranches->pluck('id'))
            ->whereBetween('transactions_deposit_details.trans_date',
                [$initialOpeningDay->toDateString(), $fromDate->copy()->subDay()->toDateString()])
            //deleted at not working in join
            //savar branch opening problem
            ->whereNull('transactions_deposit_details.deleted_at')
//            ->groupBy('transactions_deposit_details.trans_id', 'transactions_deposit_details.trans_sl',
//                'transactions_deposit_details.trans_date', 'transactions_deposit_details.branch_id',
//                'transactions_deposit_details.brand_id', 'transactions_deposit_details.product_id',
//                'transactions_deposit_details.bank_id', 'transactions_deposit_details.amount',
//                'transactions_deposit_summary.approve_by', 'transactions_deposit_summary.created_by')
            ->get();

        //        $dateWiseReceiptsSummary = $this->dateWiseReceiptSummaryRepository->getModel()
//            ->select(['id', 'sl', 'trans_date', 'branch_id', 'approve', 'approve_by', 'created_by', 'month', 'year'])
//            ->whereIn('branch_id', $requestBranches->pluck('id'))
//            ->whereBetween('trans_date', [$fromDate->toDateString(), $toDate->toDateString()])
//            ->cursor();
//
//        $dateWiseReceiptsDetails = $this->dateWiseReceiptDetailRepository->getModel()
//            ->select(['trans_id', 'trans_sl', 'trans_date', 'branch_id', 'brand_id', 'product_id', 'collection', 'money_receipt', 'adv_receipt', 'short_receipt', 'adv_adj', 'adj_plus', 'adj_minus'])
//            ->whereIn('trans_id', $dateWiseReceiptsSummary->pluck('id'))
//            ->whereIn('trans_sl', $dateWiseReceiptsSummary->pluck('sl'))
//            ->whereIn('product_id', $requestProducts->pluck('id'))
//            ->whereIn('brand_id', $requestBrands->pluck('id'))
//            ->whereIn('branch_id', $requestBranches->pluck('id'))
//            ->whereBetween('trans_date', [$fromDate->toDateString(), $toDate->toDateString()])
//            ->cursor();
//
        //date wise receipts
//        $dateWiseReceiptsSummary = $this->dateWiseReceiptSummaryRepository->getBuilder()
//            ->select(['id', 'sl', 'trans_date', 'branch_id', 'approve', 'approve_by', 'created_by'])
//            ->whereIn('branch_id', $requestBranches->pluck('id'))
//            ->whereBetween('trans_date', [$fromDate->toDateString(), $toDate->toDateString()])
//            ->cursor();
//
//        $dateWiseReceiptsDetails = $this->dateWiseReceiptDetailRepository->getBuilder()
//            ->select(['trans_id', 'trans_sl', 'trans_date', 'branch_id', 'brand_id', 'product_id', 'collection', 'money_receipt', 'adv_receipt', 'short_receipt', 'adv_adj', 'adj_plus', 'adj_minus'])
//            ->whereIn('trans_id', $dateWiseReceiptsSummary->pluck('id'))
//        //    ->whereIn('trans_sl', $dateWiseReceiptsSummary->pluck('sl'))
//            ->whereIn('product_id', $requestProducts->pluck('id'))
//            ->whereIn('brand_id', $requestBrands->pluck('id'))
//            ->whereIn('branch_id', $requestBranches->pluck('id'))
//            ->whereBetween('trans_date', [$fromDate->toDateString(), $toDate->toDateString()])
//            ->cursor();
//
//        $depositSummary = $this->depositSummaryRepository->getBuilder()
//            ->select(['id', 'sl', 'trans_date', 'branch_id', 'approve', 'approve_by', 'created_by'])
//            ->whereIn('branch_id', $requestBranches->pluck('id'))
//            ->whereBetween('trans_date', [$fromDate->toDateString(), $toDate->toDateString()])
////            ->with(['createBy' => function ($q) {
////                return $q->select(['id', 'first_name', 'last_name', 'email']);
////            }])
//            ->cursor();
//
//        $depositDetails = $this->depositDetailRepository->getBuilder()
//            ->select(['trans_id', 'trans_sl', 'trans_date', 'branch_id', 'brand_id', 'product_id', 'bank_id', 'amount'])
//            ->whereIn('trans_id', $depositSummary->pluck('id'))
//            //    ->whereIn('trans_sl', $depositSummary->pluck('sl'))
//            ->whereIn('product_id', $requestProducts->pluck('id'))
//            ->whereIn('brand_id', $requestBrands->pluck('id'))
//            ->whereIn('branch_id', $requestBranches->pluck('id'))
//            ->whereBetween('trans_date', [$fromDate->toDateString(), $toDate->toDateString()])
//            ->cursor();
//
//        $receiptsClosingSummary = $this->dateWiseReceiptSummaryRepository->getBuilder()
//            ->select(['id', 'sl', 'trans_date', 'branch_id'])
//            ->whereIn('branch_id', $requestBranches->pluck('id'))
//            ->whereBetween('trans_date', [$initialOpeningDay->toDateString(), $fromDate->copy()->subDay()->toDateString()])
//            ->cursor();
//
//        $receiptsClosingDetails = $this->dateWiseReceiptDetailRepository->getBuilder()
//            ->select(['trans_id', 'trans_sl', 'trans_date', 'branch_id', 'brand_id', 'product_id', 'collection', 'money_receipt', 'adv_receipt', 'short_receipt', 'adv_adj', 'adj_plus', 'adj_minus'])
//            ->whereIn('trans_id', $receiptsClosingSummary->pluck('id'))
//            //    ->whereIn('trans_sl', $receiptsClosingSummary->pluck('sl'))
//            ->whereIn('product_id', $requestProducts->pluck('id'))
//            ->whereIn('brand_id', $requestBrands->pluck('id'))
//            ->whereIn('branch_id', $requestBranches->pluck('id'))
//            ->whereBetween('trans_date', [$initialOpeningDay->toDateString(), $fromDate->copy()->subDay()->toDateString()])
//            ->cursor();
//
//        $depositClosingSummary = $this->depositSummaryRepository->getBuilder()
//            ->select(['id', 'sl', 'trans_date', 'branch_id'])
//            ->whereIn('branch_id', $requestBranches->pluck('id'))
//            ->whereBetween('trans_date', [$initialOpeningDay->toDateString(), $fromDate->copy()->subDay()->toDateString()])
//            ->cursor();
//
//        $depositClosingDetails = $this->depositDetailRepository->getBuilder()
//            ->select(['trans_id', 'trans_sl', 'trans_date', 'branch_id', 'brand_id', 'product_id', 'bank_id', 'amount'])
//            ->whereIn('trans_id', $depositClosingSummary->pluck('id'))
//            //     ->whereIn('trans_sl', $depositClosingSummary->pluck('sl'))
//            ->whereIn('product_id', $requestProducts->pluck('id'))
//            ->whereIn('brand_id', $requestBrands->pluck('id'))
//            ->whereIn('branch_id', $requestBranches->pluck('id'))
//            ->whereBetween('trans_date', [$initialOpeningDay->toDateString(), $fromDate->copy()->subDay()->toDateString()])
//            ->cursor();
//
//        $results = collect();
//        foreach ($requestBranches as $branch){
//            $result = collect();
//            $result->put('branch')
//        }
//
//
//       dd($dateWiseReceiptsDetails, $depositDetails);
//
//        $results = collect();
//        $closing = $_first_opening = $_total_collection = $total_collection = $_total_adv_receipt = $_total_adv_adj = $_total_deposit_amount = $total_money_receipt = $total_deposit = 0;
//
//        foreach ($requestBranches as $branch) {
//            $arr = collect();
//
//            $_total_grand_collection = 0;
//
//            $_first_opening = $initialOpeningDetails->where('branch_id', $branch->id)->sum('collection');
//            $_total_collection = $receiptsClosingDetails->where('branch_id', $branch->id)->sum('money_receipt');
//            $_total_adv_receipt = $receiptsClosingDetails->where('branch_id', $branch->id)->sum('adv_receipt');
//            $_total_adv_adj = $receiptsClosingDetails->where('branch_id', $branch->id)->sum('adv_adj');
//            $_total_deposit_amount = $depositClosingDetails->where('branch_id', $branch->id)->sum('amount');
//
//            $current_opening = $_first_opening + $_total_collection + $_total_adv_receipt - $_total_adv_adj - $_total_deposit_amount;
//
//            foreach ($dates as $date) {
//                $dt = collect();
//
//                $dt->put('opening_balance', $current_opening);
//
//                $collection = $dateWiseReceiptsDetails->where('branch_id', $branch->id)
//                    ->where('trans_date', $date->toDateString())
//                    ->sum('collection');
//                $total_collection += $collection;
//
//                $money_receipt = $dateWiseReceiptsDetails->where('branch_id', $branch->id)
//                    ->where('trans_date', $date->toDateString())
//                    ->sum('money_receipt');
//                $total_money_receipt += $money_receipt;
//
//                $deposit_amount = $dateWiseReceiptsDetails->where('branch_id', $branch->id)
//                    ->where('trans_date', $date->toDateString())
//                    ->sum('total_amount');
//                $total_deposit += $deposit_amount;
//
//                $closing_balance = $current_opening + $collection + $money_receipt - $deposit_amount;
//
//                $current_opening = $closing_balance;
//
//                $dt->put('collection', $collection);
//                $dt->put('total_collection', $total_collection);
//
//                $dt->put('money_receipt', $money_receipt);
//                $dt->put('total_money_receipt', $total_money_receipt);
//
//                $dt->put('deposit_amount', $deposit_amount);
//                $dt->put('total_deposit', $total_deposit);
//
//                $dt->put('closing_balance', $closing_balance);
//
//                foreach ($requestBrands as $brand){
//                    $br = collect();
//                    $brand_wise_mr = $brand_wise_total_mr = 0;
//
//                    $pro = collect();
//                    foreach ($requestProducts->where('brand_id', $brand->id) as $product){
//                        $_pro = collect();
//
//                        $brand_wise_mr =  $dateWiseReceiptsDetails->where('branch_id', $branch->id)
//                            ->where('trans_date', $date->toDateString())
//                            ->where('product_id', $product->id)
//                            ->sum('money_receipt');
//
//                        $brand_wise_total_mr += $brand_wise_mr;
//
//                        $_br = collect();
//                        $_br->put('brand_wise_mr', $brand_wise_mr);
//                        $_br->put('brand_wise_total_mr', $brand_wise_total_mr);
//
//                        $_pro->put($product->name, $_br);
//                    }
//                    $br->put($brand->name, $pro);
//
//                    $dt->put('brands', $br);
//                }
//
//                $arr->put($date->format('Y-m-d'), $dt);
//            }
//
//            $results->put($branch->name, $arr);
//        }
//
//        dd($results);

        if ($module_name_singular === null) {
            throw new GeneralException('No transaction found!');
        }

        // dd(collect($dateWiseReceiptsDetails->groupBy(['branch_id', 'trans_date'])));

        $reportView = view($module_view.'.report',
            compact('module_name', $module_name_singular, 'module_name_singular', 'module_parent',
                'module_icon', 'page_heading', 'module_action', 'module_view', 'module_route'))
            ->with('branches', $requestBranches)
            ->with('branch_name', $branchName)
            ->with('dates', $dates)
            // ->with('initial_opening_summary', $initialOpeningSummary)
            ->with('initial_opening', $initialOpeningDetails->groupBy('branch_id'))
            //->with('receiptSummaries', $dateWiseReceiptsSummary)
            ->with('allReceipts', collect($dateWiseReceiptsDetails->groupBy('branch_id')))
            // ->with('depositSummaries', $depositSummary)
            ->with('allDeposits', collect($depositDetails->groupBy('branch_id')))
            ->with('brands', $requestBrands)
            ->with('products', $requestProducts)
            ->with('receiptClosing', $receiptsClosingDetails->groupBy('branch_id'))
            ->with('depositClosing', $depositClosingDetails->groupBy('branch_id'))
            ->with('banks', $banks)
            ->with('bankCharges', $bankChargeDetails->groupBy('branch_id'))
            ->with('to_date', $toDate);

        if (request()->has('export')) {
            //            $exportType = request()->get('export');
//            $exportAction = request()->get('action');
//
//            $exportBlade = $module_view . '.report_export';
//            $exportBladePartial = $module_view . '.report_partial';
//            $reportExport = \View::make($exportBlade, $reportView->getData());
//
//
//            if ($exportType == "pdf") {
//                $pdf = \PDF::loadHTML($reportExport->render());
//                $pdf = $pdf->setPaper('a4');
//
//                if ($exportAction == "download") return $pdf->download($page_heading . ".pdf");
//                else return $pdf->inline($page_heading . ".pdf");
//            }
//
//            if ($exportType == "excel") {
//                return \Excel::create($page_heading, function ($excel) use ($exportBladePartial, $reportView, $reportExport) {
//                    $excel->sheet("sheet1", function ($sheet) use ($exportBladePartial, $reportView, $reportExport) {
//                        $sheet->loadView($exportBladePartial, $reportView->getData());
//                        $sheet->setAutoSize(true);
//                    });
//                })->export('xlsx');
//            }
//
//            if ($exportType == "csv") {
//                return \Excel::create($page_heading, function ($excel) use ($exportBladePartial, $reportView, $reportExport) {
//                    $excel->sheet("sheet1", function ($sheet) use ($exportBladePartial, $reportView, $reportExport) {
//                        $sheet->loadView($exportBladePartial, $reportView->getData());
//                        $sheet->setAutoSize(true);
//                    });
//                })->export('csv');
//            }

            $exportType = request()->get('export');
            $exportAction = request()->get('action');

            $exportBlade = $module_view.'.report_partial';
            $exportData = $reportView->getData();

            //$reportExport = \View::make($module_view . '.report_partial', $reportView->getData());

            switch ($exportType) {
//                case 'excel':
//                    $exportExtension = 'xlsx';
//                    break;
//
//                case 'xls':
//                    $exportExtension = 'xls';
//                    break;

                case 'pdf':
                    $exportExtension = 'pdf';
                    break;

                case 'csv':
                case 'excel':
                case 'xls':
                    $exportExtension = 'csv';
                    break;

                default:
                    $exportExtension = 'xls';

            }

            return Excel::download(new ReportExport($exportBlade, $exportData), $page_heading.'.'.$exportExtension);

//            if ($exportType === 'pdf') {
//                $reportExport = \View::make($exportBlade, $reportView->getData());
//                $pdf = \PDF::loadHTML($reportExport->render());
//                $pdf = $pdf->setPaper('a4');
//
//                if ($exportAction === 'download') {
//                    return $pdf->download($page_heading . '.pdf');
//                }
//
//                return $pdf->inline($page_heading . '.pdf');
//            }
//
//            if ($exportType === 'excel') {
//                ;
//            }
//
//            if ($exportType === 'csv') {
//                return \Excel::create($page_heading, function ($excel) use ($exportBlade, $exportData) {
//                    $excel->sheet('sheet1', function ($sheet) use ($exportBlade, $exportData) {
//                        $sheet->loadView($exportBlade, $exportData);
//                        $sheet->setAutoSize(true);
//                    });
//                })->export('csv');
//            }

        }

        return $reportView;
    }

}
