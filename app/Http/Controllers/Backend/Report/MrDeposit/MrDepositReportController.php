<?php

namespace App\Http\Controllers\Backend\Report\MrDeposit;

use App\Exceptions\GeneralException;
use App\Exports\ReportExport;
use App\Http\Controllers\BaseController;
use App\Repositories\Backend\Setup\Branch\BranchRepository;
use App\Repositories\Backend\Setup\Brand\BrandRepository;
use App\Repositories\Backend\Setup\Opening\OpeningSummaryRepository;
use App\Repositories\Backend\Setup\Product\ProductRepository;
use App\Repositories\Backend\Transaction\DateWise\DateWiseSummaryRepository;
use App\Repositories\Backend\Transaction\Deposit\DepositSummaryRepository;
use Carbon\Carbon;
use Carbon\CarbonInterval;
use Carbon\CarbonPeriod;
use Excel;
use Illuminate\Http\Response;
use Illuminate\Support\Collection;
use View;


/**
 * Class MrDepositReportController
 *
 * @package App\Http\Controllers\Backend\Report\MrDeposit
 */
final class MrDepositReportController extends BaseController
{
    /**
     * @var BrandRepository
     */
    private $brandRepository;
    /**
     * @var ProductRepository
     */
    private $productRepository;
    /**
     * @var BranchRepository
     */
    private $branchRepository;
    /**
     * @var DateWiseSummaryRepository
     */
    private $dateWiseReceiptSummaryRepository;
    /**
     * @var DepositSummaryRepository
     */
    private $depositSummaryRepository;

    /**
     * @var OpeningSummaryRepository
     */
    private $openingSummaryRepository;

    /**
     * MrDepositReportController constructor.
     *
     * @param  DateWiseSummaryRepository  $dateWiseReceiptSummaryRepository
     * @param  DepositSummaryRepository   $depositSummaryRepository
     * @param  BrandRepository            $brandRepository
     * @param  ProductRepository          $productRepository
     * @param  BranchRepository           $branchRepository
     * @param  OpeningSummaryRepository   $openingSummaryRepository
     */
    public function __construct(
        DateWiseSummaryRepository $dateWiseReceiptSummaryRepository,
        DepositSummaryRepository $depositSummaryRepository,
        BrandRepository $brandRepository,
        ProductRepository $productRepository,
        BranchRepository $branchRepository,
        OpeningSummaryRepository $openingSummaryRepository
    ) {
        $this->dateWiseReceiptSummaryRepository = $dateWiseReceiptSummaryRepository;
        $this->depositSummaryRepository = $depositSummaryRepository;
        $this->brandRepository = $brandRepository;
        $this->productRepository = $productRepository;
        $this->branchRepository = $branchRepository;
        $this->openingSummaryRepository = $openingSummaryRepository;


        $this->module_parent = '1.1 MR & Deposit Reconciliation Report';
        $this->module_name = 'mr_deposit';
        $this->module_icon = 'mdi-hospital-building';
        $this->module_route = 'admin.reports.'.$this->module_name;
        $this->module_view = 'backend.reports.'.$this->module_name;

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param $encodedReq
     * @return Response
     * @throws GeneralException
     */
    public function show($encodedReq)
    {
        ini_set('memory_limit', '8192M');

        $module_parent = $this->module_parent;
        $module_name = $this->module_name;
        $module_name_singular = str_singular($this->module_name);
        $module_icon = $this->module_icon;
        $module_action = 'Save';
        $module_route = $this->module_route;
        $module_view = $this->module_view;

        $page_heading = $this->module_parent;

        //convert to collection from request
        $request = new Collection(json_decode(decryptString($encodedReq)));

        //branches
        $requestBranches = $this->branchRepository->getModel()
            ->select(['id', 'name', 'code', 'short_name'])
            ->whereIn('id', $request->get('branch_list'))
            ->get();

        //brands
        $requestProducts = $this->productRepository->getModel()
            ->select(['id', 'name', 'code', 'short_name', 'brand_id'])
            ->whereIn('id', $request->get('brand_list'))
            // ->whereIn('brand_id', $requestBrands->pluck('id'))
            ->get();

        //segments
        $requestBrands = $this->brandRepository->getModel()
            ->select(['id', 'name', 'code'])
            ->whereIn('id', $requestProducts->pluck('brand_id'))
            ->get();

//        //branches
//        $requestBranches = $this->branchRepository->getModel()->select(['id', 'name'])
//            ->whereIn('id', $request->get('branch_list'))
//            ->cursor();
//
//        //brands
//        $requestProducts = $this->productRepository->getModel()->select(['id', 'name', 'brand_id'])
//            ->whereIn('id', $request->get('brand_list'))
//            // ->whereIn('brand_id', $requestBrands->pluck('id'))
//            ->cursor();
//
//        //segments
//        $requestBrands = $this->brandRepository->getModel()->select(['id', 'name'])
//            ->whereIn('id', $requestProducts->pluck('brand_id'))
//            ->cursor();
//
//        $requestBrands = $this->brandRepository
//            ->whereIn('id', $request->get('segment_list'))
//            ->cursor();
//
//        $requestProducts = $this->productRepository
//            ->whereIn('id', $request->get('brand_list'))
//            //->whereIn('brand_id', $requestBrands->pluck('id'))
//            ->cursor();

        $fromDate = Carbon::parse($request->get('from_date'));
        $toDate = Carbon::parse($request->get('to_date'));

        $lastMonth = $toDate->month - 1;
        $lastYear = $toDate->year;
        //when month =1, then closing month = 0, so turn it to month=12 and set year=year-1
        if ($lastMonth == 0) {
            $lastMonth = 12;
            $lastYear = $lastYear - 1;
        }

        $interval = CarbonInterval::createFromDateString('1 day');
        $dates = new CarbonPeriod($fromDate, $interval, $toDate);

        $branchName = ($requestBranches->count() > 15) ? 'All' : implode(',',
            $requestBranches->pluck('name')->toArray());

        //get initial openings month and year
        $firstInitialOpening = $this->openingSummaryRepository->getModel()
            ->select(['month', 'year'])
            ->whereIn('branch_id', $requestBranches->pluck('id'))
            ->orderByDesc('id')
            ->first();
        $initialOpeningMonth = $firstInitialOpening->month;
        $initialOpeningYear = $firstInitialOpening->year;
        $initialOpeningDay = Carbon::createFromDate($initialOpeningYear, $initialOpeningMonth, '01')->lastOfMonth();


        $initialOpening = $this->openingSummaryRepository->getModel()
            ->select(['id', 'sl', 'trans_date', 'branch_id', 'pc_bank_amount', 'pc_cash_amount'])
            ->whereIn('branch_id', $requestBranches->pluck('id'))
            ->orderByDesc('id')
            ->with([
                'details' => function ($query) use ($requestProducts) {
                    return $query->select([
                        'trans_id', 'trans_sl', 'trans_date', 'branch_id', 'brand_id', 'product_id', 'collection',
                        'outstanding', 'pc_bank_amount', 'pc_cash_amount'
                    ])
                        ->whereIn('product_id', $requestProducts->pluck('id'));
                }
            ])
            ->get();

        $dateWiseReceipts = $this->dateWiseReceiptSummaryRepository->getModel()
            ->select(['id', 'sl', 'trans_date', 'branch_id', 'approve', 'approve_by', 'created_by'])
            ->whereIn('branch_id', $requestBranches->pluck('id'))
            ->whereBetween('trans_date', [$fromDate->toDateString(), $toDate->toDateString()])
            ->with([
                'details' => function ($query) use ($requestProducts) {
                    return $query->select([
                        'trans_id', 'trans_sl', 'trans_date', 'branch_id', 'brand_id', 'product_id', 'collection',
                        'money_receipt', 'adv_receipt', 'short_receipt', 'adv_adj', 'adj_plus', 'adj_minus','mr_reverse'
                    ])
                        ->whereIn('product_id', $requestProducts->pluck('id'));
                }
            ])
            ->get();

        $deposits = $this->depositSummaryRepository->getModel()
            ->select(['id', 'sl', 'trans_date', 'branch_id', 'approve', 'approve_by', 'created_by'])
            ->whereIn('branch_id', $requestBranches->pluck('id'))
            ->whereBetween('trans_date', [$fromDate->toDateString(), $toDate->toDateString()])
            ->with([
                'details' => function ($query) use ($requestProducts) {
                    return $query->select([
                        'trans_id', 'trans_sl', 'trans_date', 'branch_id', 'brand_id', 'product_id', 'bank_id', 'amount'
                    ])
                        ->whereIn('product_id', $requestProducts->pluck('id'));
                }
            ])
            ->with([
                'createBy' => function ($q) {
                    return $q->select(['id', 'first_name', 'last_name', 'email']);
                }
            ])
            ->get();


        //for closing only
        $receiptsClosing = $this->dateWiseReceiptSummaryRepository->getModel()
            ->select(['id', 'sl', 'trans_date', 'branch_id'])
            ->with([
                'details' => function ($query) use ($requestProducts) {
                    return $query->select([
                        'trans_id', 'trans_sl', 'trans_date', 'branch_id', 'brand_id', 'product_id', 'collection',
                        'money_receipt', 'adv_receipt', 'short_receipt', 'adv_adj', 'adj_plus', 'adj_minus','mr_reverse'
                    ])
                        ->whereIn('product_id', $requestProducts->pluck('id'));
                }
            ])
            ->whereIn('branch_id', $requestBranches->pluck('id'))
            ->whereBetween('trans_date',
                [$initialOpeningDay->toDateString(), $fromDate->copy()->subDay()->toDateString()])
            ->get();


        //for closing only
        $depositClosing = $this->depositSummaryRepository->getModel()
            ->select(['id', 'sl', 'trans_date', 'branch_id'])
            ->with([
                'details' => function ($query) use ($requestProducts) {
                    return $query->select([
                        'trans_id', 'trans_sl', 'trans_date', 'branch_id', 'brand_id', 'product_id', 'bank_id', 'amount'
                    ])
                        ->whereIn('product_id', $requestProducts->pluck('id'));
                }
            ])
            ->whereIn('branch_id', $requestBranches->pluck('id'))
            ->whereBetween('trans_date',
                [$initialOpeningDay->toDateString(), $fromDate->copy()->subDay()->toDateString()])
            ->get();

        if ($module_name_singular === null) {
            throw new GeneralException('No transaction found!');
        }

        $reportView = view($module_view.'.report',
            compact('module_name', $module_name_singular, 'module_name_singular', 'module_parent',
                'module_icon', 'page_heading', 'module_action', 'module_view', 'module_route'))
            ->with('branches', $requestBranches)
            ->with('branch_name', $branchName)
            ->with('dates', $dates)
            ->with('initial_opening', $initialOpening)
            ->with('receipts', $dateWiseReceipts)
            ->with('deposits', $deposits)
            ->with('brands', $requestBrands)
            ->with('products', $requestProducts)
            ->with('receiptClosing', $receiptsClosing)
            ->with('depositClosing', $depositClosing)
            ->with('to_date', $toDate);

        if (request()->has('export')) {
            $exportType = request()->get('export');
            $exportAction = request()->get('action');

            $exportBlade = $module_view.'.report_partial';
            $exportData = $reportView->getData();

            switch ($exportType) {
//                case 'excel':
//                    $exportExtension = 'xlsx';
//                    break;
//
//                case 'xls':
//                    $exportExtension = 'xls';
//                    break;

                case 'pdf':
                    $exportExtension = 'pdf';
                    break;

                case 'csv':
                case 'excel':
                case 'xls':
                    $exportExtension = 'csv';
                    break;

                default:
                    $exportExtension = 'xls';

            }

            return Excel::download(new ReportExport($exportBlade, $exportData), $page_heading.'.'.$exportExtension);

        }

        return $reportView;
    }
}
