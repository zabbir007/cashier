<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\BaseController;
use App\Repositories\Backend\Setup\Brand\BrandRepository;
use App\Repositories\Backend\Setup\Opening\OpeningDetailsRepository;
use App\Repositories\Backend\Transaction\DateWise\DateWiseDetailsRepository;
use App\Repositories\Backend\Transaction\Deposit\DepositDetailsRepository;
use App\Repositories\Backend\Transaction\OutstandingReconciliation\OutstandingReconciliationDetailsRepository;
use App\Repositories\Backend\Transaction\PettyCash\PettyCashDetailsRepository;
use Auth;
use Cache;
use Carbon\Carbon;
use Carbon\CarbonInterval;
use Carbon\CarbonPeriod;
use Illuminate\Support\Collection;
use Illuminate\View\View;
use Throwable;

/**
 * Class DashboardController.
 */
final class DashboardController extends BaseController
{
    /**
     * @var BrandRepository
     */
    private $segmentRepository;
    /**
     * @var OpeningDetailsRepository
     */
    private $openingRepository;
    /**
     * @var DateWiseDetailsRepository
     */
    private $dateWiseRepository;
    /**
     * @var PettyCashDetailsRepository
     */
    private $pettyCashRepository;
    /**
     * @var DepositDetailsRepository
     */
    private $depositRepository;
    /**
     * @var OutstandingReconciliationDetailsRepository
     */
    private $outstandingRepository;

    /**
     * @var int
     */
    private $cacheMinute;

    /**
     * DashboardController constructor.
     *
     * @param  BrandRepository                             $brandRepository
     * @param  OpeningDetailsRepository                    $openingRepository
     * @param  DateWiseDetailsRepository                   $dateWiseDetailsRepository
     * @param  PettyCashDetailsRepository                  $pettyCashDetailsRepository
     * @param  DepositDetailsRepository                    $depositRepository
     * @param  OutstandingReconciliationDetailsRepository  $outstandingRepository
     */
    public function __construct(
        BrandRepository $brandRepository,
        OpeningDetailsRepository $openingRepository,
        DateWiseDetailsRepository $dateWiseDetailsRepository,
        PettyCashDetailsRepository $pettyCashDetailsRepository,
        DepositDetailsRepository $depositRepository,
        OutstandingReconciliationDetailsRepository $outstandingRepository
    ) {
        $this->segmentRepository = $brandRepository;
        $this->openingRepository = $openingRepository;
        $this->dateWiseRepository = $dateWiseDetailsRepository;
        $this->pettyCashRepository = $pettyCashDetailsRepository;
        $this->depositRepository = $depositRepository;
        $this->outstandingRepository = $outstandingRepository;

        $this->cacheMinute = 20;

        $this->module_action = '';
        $this->module_parent = 'Dashboard';
        $this->module_name = 'dashboard';
        $this->module_icon = 'mdi-hospital-building';
        $this->module_route = 'admin.dashboard';
    }

    /**
     * @return View
     */
    public function index()
    {
        $module_parent = $this->module_parent;
        $module_name = $this->module_name;
        $module_name_singular = str_singular($this->module_name);
        $module_icon = $this->module_icon;
        $module_action = 'index';
        $module_route = $this->module_route;
        $module_view = $this->module_view;

        return view('backend.dashboard.dashboard',
            compact('module_name', $module_name, 'module_name_singular', 'module_parent', 'module_icon',
                'module_action', 'module_view', 'module_route'))
            ->with('cache_minute', $this->cacheMinute);
    }

    /**
     * @return string
     * @throws Throwable
     */
    public function loadTotalSummary()
    {
        $userId = Auth::user()->id;
        $branch = Cache::remember($userId.'_branches', $this->cacheMinute, function () {
            return $this->getBranchIds();
        });

        $reportView = Cache::remember($userId.'_total_summary', $this->cacheMinute,
            function () use ($userId, $branch) {
                $opening = Cache::remember($userId.'_opening', $this->cacheMinute, function () use ($branch) {
                    return $this->openingRepository->getOpeningByBranch($branch);
                });

                //total summary
                $totalSales = Cache::remember($userId.'_sales', $this->cacheMinute, function () use ($branch) {
                    return $this->outstandingRepository->getCurrentNetSalesByBranch($branch);
                });

                $totalCollection = Cache::remember($userId.'_collections', $this->cacheMinute,
                    function () use ($branch) {
                        return $this->dateWiseRepository->getCurrentTotalCollectionByBranch($branch);
                    });

                $totalDeposit = Cache::remember($userId.'_deposits', $this->cacheMinute, function () use ($branch) {
                    return $this->depositRepository->getCurrentTotalDepositByBranch($branch);
                });

                $totalExpCq = Cache::remember($userId.'_total_expense_cq', $this->cacheMinute,
                    function () use ($branch) {
                        return $this->pettyCashRepository->getCurrentTotalExpenseCqByBranch($branch);
                    });

                $totalExp = Cache::remember($userId.'_total_exp', $this->cacheMinute, function () use ($branch) {
                    return $this->pettyCashRepository->getCurrentTotalExpenseByBranch($branch);
                });

//
//        $settingsRepo = new TransactionSettingRepository();
//
//        dd($settingsRepo->checkIfAllowed('2019-10-03', 28));

                $totalExpense = $totalExpCq + $totalExp;
                $outstanding = $opening + $totalSales - $totalCollection;

                return preg_replace('/\s+/', ' ', view('backend.dashboard.partial.total_summary')
                    ->with('branch', $branch)
                    ->with('collections', $totalCollection)
                    ->with('deposits', $totalDeposit)
                    ->with('sales', $totalSales)
                    ->with('expenses', $totalExpense)
                    ->with('outstanding', $outstanding)
                    ->with('opening', $opening)
                    ->with('expenses', $totalExpense)
                    ->with('outstanding', $outstanding)
                    ->render());
            });

        return response()->json([
            'report' => $reportView,
            'status' => 200
        ]);
    }

    /**
     * @return string
     * @throws Throwable
     */
    public function loadSegmentSummary()
    {
        $userId = Auth::user()->id;
        $branch = Cache::remember($userId.'_branches', $this->cacheMinute, function () {
            return $this->getBranchIds();
        });

        $reportView = Cache::remember($userId.'_segment_summary', $this->cacheMinute,
            function () use ($userId, $branch) {
                //segment stats
                $segments = Cache::remember($userId.'segments', $this->cacheMinute, function () {
                    return $this->segmentRepository->makeModel()
                        ->where('status', 1)
                        ->orderBy('id')
                        ->get();
                });

                $salesRow = new Collection();
                $collectionsRow = new Collection();
                $depositsRow = collect();

                $totalSales = Cache::remember($userId.'_sales', $this->cacheMinute, function () use ($branch) {
                    return $this->outstandingRepository->getCurrentNetSalesByBranch($branch);
                });

                $totalCollection = Cache::remember($userId.'_collections', $this->cacheMinute,
                    function () use ($branch) {
                        return $this->dateWiseRepository->getCurrentTotalCollectionByBranch($branch);
                    });

                $totalDeposit = Cache::remember($userId.'_deposits', $this->cacheMinute, function () use ($branch) {
                    return $this->depositRepository->getCurrentTotalDepositByBranch($branch);
                });

                foreach ($segments as $segment) {
                    $salesPie = Cache::remember($userId.'_'.$segment->id.'_salesPie', $this->cacheMinute,
                        function () use ($branch, $segment) {
                            return $this->outstandingRepository->getCurrentNetSalesByBranchSegment($branch,
                                $segment->id);
                        });
                    $salesRow->push([
                        'brand_name' => $segment->name,
                        'sales' => $salesPie,
                        'total' => $totalSales,
                    ]);

                    $collectionsPie = Cache::remember($userId.'_'.$segment->id.'_collectionsPie', $this->cacheMinute,
                        function () use ($branch, $segment) {
                            return $this->dateWiseRepository->getCurrentTotalCollectionByBranchSegment($branch,
                                $segment->id);
                        });
                    $collectionsRow->prepend([
                        'brand_name' => $segment->name,
                        'collections' => $collectionsPie,
                        'total' => $totalCollection
                    ]);

                    $depositsPie = Cache::remember($userId.'_'.$segment->id.'_depositsPie', $this->cacheMinute,
                        function () use ($branch, $segment) {
                            return $this->depositRepository->getCurrentTotalDepositByBranchSegment($branch,
                                $segment->id);
                        });
                    $depositsRow->prepend([
                        'brand_name' => $segment->name,
                        'deposits' => $depositsPie,
                        'total' => $totalDeposit
                    ]);
                }

                return preg_replace('/\s+/', ' ', view('backend.dashboard.partial.segment_summary')
                    ->with('branch', $branch)
                    ->with('salesPieRow', $salesRow)
                    ->with('collectionsPieRow', $collectionsRow)
                    ->with('depositsPieRow', $depositsRow)
                    ->render());
            });

        return response()->json([
            'report' => $reportView,
            'status' => 200
        ]);
    }

    /**
     * @return string
     * @throws Throwable
     */
    public function loadMonthWiseSummary()
    {
        $userId = Auth::user()->id;
        $branch = Cache::remember($userId.'_branches', $this->cacheMinute, function () {
            return $this->getBranchIds();
        });

        $reportView = Cache::remember($userId.'month_summary', $this->cacheMinute,
            function () use ($userId, $branch) {
                $opening = Cache::remember($userId.'_opening', $this->cacheMinute, function () use ($branch) {
                    return $this->openingRepository->getOpeningByBranch($branch);
                });

                //monthwise stats
                $endDate = Carbon::parse('last day of last month');
                $startDate = Carbon::now()->subMonths(7)->firstOfMonth();

                $interval = CarbonInterval::createFromDateString('1 month');
                $period = array_reverse(CarbonPeriod::create($startDate->format('Y-m-d'), $interval,
                    $endDate->format('Y-m-d'))->toArray());

                $allSales = Cache::remember($userId.'_allSales', $this->cacheMinute,
                    function () use ($branch, $startDate, $endDate) {
                        return $this->outstandingRepository->getCurrentNetSalesByBranchYearmonth($branch, $startDate,
                            $endDate);
                    });

                $allCollections = Cache::remember($userId.'_all_collections', $this->cacheMinute,
                    function () use ($branch, $startDate, $endDate) {
                        return $this->dateWiseRepository->getCurrentTotalCollectionByBranchYearmonth($branch,
                            $startDate, $endDate);
                    });

                $allDeposits = Cache::remember($userId.'_all_deposits', $this->cacheMinute,
                    function () use ($branch, $startDate, $endDate) {
                        return $this->depositRepository->getCurrentTotalDepositByBranchYearmonth($branch, $startDate,
                            $endDate);
                    });

                $allExpCq = Cache::remember($userId.'_all_expense_cq', $this->cacheMinute,
                    function () use ($branch, $startDate, $endDate) {
                        return $this->pettyCashRepository->getCurrentTotalExpenseByBranchYearmonth($branch, $startDate,
                            $endDate);
                    });

                $allOpening = Cache::remember($userId.'_all_opening', $this->cacheMinute, function () use ($branch) {
                    return (float) $this->openingRepository->getInitialOpeningByBranch($branch);
                });

                return preg_replace('/\s+/', ' ', view('backend.dashboard.partial.month_summary')
                    ->with('carbon', Carbon::now())
                    ->with('period', $period)
                    ->with('branch', $branch)
                    ->with('opening', $opening)
                    ->with('all_sales', $allSales)
                    ->with('all_deposits', $allDeposits)
                    ->with('all_expense_cq', $allExpCq)
                    ->with('initial_opening', $allOpening)
                    ->with('all_collections', $allCollections)
                    ->render());
            });

        return response()->json([
            'report' => $reportView,
            'status' => 200
        ]);
    }


//    /**
//     * @return View
//     * @throws GeneralException
//     */
//    public function index()
//    {
//        //        ini_set('memory_limit', '600M');
//
////        $startDate = Carbon::now()->firstOfMonth();
////        $endDate = Carbon::now()->lastOfMonth();
//        $userId = Auth::user()->id;
//
//        $module_parent = $this->module_parent;
//        $module_name = $this->module_name;
//        $module_name_singular = str_singular($this->module_name);
//        $module_icon = $this->module_icon;
//        $module_action = 'index';
//        $module_route = $this->module_route;
//        $module_view = $this->module_view;
//
//        $branch = Cache::remember($userId.'_branches', $this->cacheMinute, function () {
//            return $this->getBranchIds();
//        });
//
//        $opening = Cache::remember($userId.'_opening', $this->cacheMinute, function () use ($branch) {
//            return $this->openingRepository->getOpeningByBranch($branch);
//        });
//
//        //total summary
//        $totalSales = Cache::remember($userId.'_sales', $this->cacheMinute, function () use ($branch) {
//            return $this->outstandingRepository->getCurrentNetSalesByBranch($branch);
//        });
//
//        $totalCollection = Cache::remember($userId.'_collections', $this->cacheMinute, function () use ($branch) {
//            return $this->dateWiseRepository->getCurrentTotalCollectionByBranch($branch);
//        });
//
//        $totalDeposit = Cache::remember($userId.'_deposits', $this->cacheMinute, function () use ($branch) {
//            return $this->depositRepository->getCurrentTotalDepositByBranch($branch);
//        });
//
//        $totalExpCq = Cache::remember($userId.'_total_expense_cq', $this->cacheMinute,
//            function () use ($branch) {
//                return $this->pettyCashRepository->getCurrentTotalExpenseCqByBranch($branch);
//            });
//
//        $totalExp = Cache::remember($userId.'_total_exp', $this->cacheMinute, function () use ($branch) {
//            return $this->pettyCashRepository->getCurrentTotalExpenseByBranch($branch);
//        });
//
////
////        $settingsRepo = new TransactionSettingRepository();
////
////        dd($settingsRepo->checkIfAllowed('2019-10-03', 28));
//
//        $totalExpense = $totalExpCq + $totalExp;
//        $outstanding = $opening + $totalSales - $totalCollection;
//
//
//        //segment stats
//        $segments = Cache::remember($userId.'segments', $this->cacheMinute, function () {
//            return $this->segmentRepository->makeModel()
//                ->where('status', 1)
//                ->orderByDesc('id')
//                ->get();
//        });
//
//        $salesRow = new Collection();
//        $collectionsRow = new Collection();
//        $depositsRow = collect();
//
//        foreach ($segments as $segment) {
//            $salesPie = Cache::remember($userId.'_'.$segment->id.'_salesPie', $this->cacheMinute,
//                function () use ($branch, $segment) {
//                    return $this->outstandingRepository->getCurrentNetSalesByBranchSegment($branch, $segment->id);
//                });
//            $salesRow->push([
//                'brand_name' => $segment->name,
//                'sales' => $salesPie,
//                'total' => $totalSales,
//            ]);
//
////            $totalSales = Cache::remember($userId.'_totalSales', $this->cacheMinute,
////                function () use ($branch, $startDate, $endDate) {
////                    return $this->outstandingRepository->getModel()
////                        ->whereIn('branch_id', $branch)
////                        ->whereBetween('trans_date', [$startDate->toDateString(), $endDate->toDateString()])
//////                        ->where('month', $month)->where('year', $year)
////                        ->sum('net_sales_total');
////                });
//
//            $collectionsPie = Cache::remember($userId.'_'.$segment->id.'_collectionsPie', $this->cacheMinute, function () use ($branch, $segment) {
//                return $this->dateWiseRepository->getCurrentTotalCollectionByBranchSegment($branch, $segment->id);
//            });
//            $collectionsRow->prepend([
//                'brand_name' => $segment->name,
//                'collections' => $collectionsPie,
//                'total' => $totalCollection
//            ]);
////            $totalCollections = Cache::remember($userId.'_totalCollections', $this->cacheMinute, function () use ($branch, $startDate, $endDate) {
////                return $this->dateWiseRepository->getModel()
////                    ->whereIn('branch_id', $branch)
////                    ->whereBetween('trans_date', [$startDate->toDateString(), $endDate->toDateString()])
//////                    ->where('month', $month)->where('year', $year)
////                    ->sum('collection');
////            });
//
//            $depositsPie = Cache::remember($userId.'_'.$segment->id.'_depositsPie', $this->cacheMinute, function () use ($branch, $segment) {
//                return  $this->depositRepository->getCurrentTotalDepositByBranchSegment($branch, $segment->id);
//            });
//            $depositsRow->prepend([
//                'brand_name' => $segment->name,
//                'deposits' => $depositsPie,
//                'total' => $totalDeposit
//            ]);
//
////            $totalDeposits = Cache::remember($userId.'_'.$segment->id.'_totalDeposits', $this->cacheMinute, function () use ($branch, $startDate, $endDate) {
////                return $this->depositRepository->getModel()
////                    ->whereIn('branch_id', $branch)
////                    ->whereBetween('trans_date', [$startDate->toDateString(), $endDate->toDateString()])
//////                    ->where('month', $month)->where('year', $year)
////                    ->sum('amount');
////            });
//        }
//
//
//        //monthwise stats
//        $endDate = Carbon::parse('first day of last month');
//        $startDate = Carbon::now()->subMonth(7)->firstOfMonth();
//
//        $interval = CarbonInterval::createFromDateString('1 month');
//        $period = array_reverse(CarbonPeriod::create($startDate->format('Y-m-d'), $interval,
//            $endDate->format('Y-m-d'))->toArray());
//
//        $allSales = Cache::remember($userId.'_allSales', $this->cacheMinute, function () use ($branch, $startDate, $endDate) {
//            return $this->outstandingRepository->getCurrentNetSalesByBranchYearmonth($branch, $startDate, $endDate);
//        });
//
//        $allCollections = Cache::remember($userId.'_all_collections', $this->cacheMinute, function () use ($branch, $startDate, $endDate) {
//            return $this->dateWiseRepository->getCurrentTotalCollectionByBranchYearmonth($branch, $startDate, $endDate);
//        });
//
//        $allDeposits = Cache::remember($userId.'_all_deposits', $this->cacheMinute, function () use ($branch, $startDate, $endDate) {
//            return $this->depositRepository->getCurrentTotalDepositByBranchYearmonth($branch, $startDate, $endDate);
//        });
//
//
//        $allExpCq = Cache::remember($userId.'_all_expense_cq', $this->cacheMinute, function () use ($branch, $startDate, $endDate) {
//            return $this->pettyCashRepository->getCurrentTotalExpenseByBranchYearmonth($branch, $startDate, $endDate);
//        });
//
//
//        $allOpening = Cache::remember($userId.'_all_opening', $this->cacheMinute, function () use ($branch) {
//            return (float) $this->openingRepository->getInitialOpeningByBranch($branch);
//        });
//
//        return view('backend.dashboard.dashboard',
//            compact('module_name', $module_name, 'module_name_singular', 'module_parent', 'module_icon', 'module_action', 'module_view', 'module_route'))
//            ->with('carbon', Carbon::now())
//            ->with('branch', $branch)
//            ->with('opening', $opening)
//            ->with('period', $period)
//            ->with('collections', $totalCollection)
//            ->with('deposits', $totalDeposit)
//            ->with('sales', $totalSales)
//            ->with('expenses', $totalExpense)
//            ->with('outstanding', $outstanding)
//            ->with('salesPieRow', $salesRow)
//            ->with('collectionsPieRow', $collectionsRow)
//            ->with('depositsPieRow', $depositsRow)
//            ->with('opening', $opening)
//            ->with('all_sales', $allSales)
//            ->with('all_deposits', $allDeposits)
//            ->with('all_expense_cq', $allExpCq)
//            ->with('initial_opening', $allOpening)
//            ->with('all_collections', $allCollections)
//
////            ->with('period', $periods)
////            ->with('branch', $branch)
////            ->with('opening', $opening)
////            ->with('collections', $totalCollection)
////            ->with('deposits', $totalDeposit)
////            ->with('sales', $totalSales)
////            ->with('expenses', $totalExpense)
////            ->with('outstanding', $outstanding)
//////            ->with('salesPieRow', $salesPieRow)
//////            ->with('collectionsPieRow', $collectionsPieRow)
//////            ->with('depositsPieRow', $depositsPieRow)
////            ->with('all_sales', $allSales)
////            ->with('all_deposits', $allDeposits)
////            ->with('all_expense_cq', $allExpenseCq)
//////            ->with('all_opening', $all_opening)
////            ->with('all_collections', $allCollections)
//            ;
//    }

//    /**
//     * @return string
//     * @throws \Throwable
//     */
//    public function loadTotalSummary()
//    {
//        $month = Carbon::now()->month;
//        $year = Carbon::now()->year;
//        $userId = Auth::user()->id;
//
//        $branch = Cache::remember($userId.'_branches', $this->cacheMinute, function () {
//            return $this->getBranchIds();
//        });
//
//        $opening = Cache::remember($userId.'_opening', $this->cacheMinute, function () use ($branch) {
//            return $this->openingRepository->getModel()->whereIn('branch_id', $branch)->sum('collection');
//        });
//
//        $totalSales = Cache::remember($userId.'_sales', $this->cacheMinute, function () use ($branch) {
//            return $this->outstandingRepository->getCurrentNetSalesByBranch($branch);
//        });
//
//        $totalCollection = Cache::remember($userId.'_collections', $this->cacheMinute, function () use ($branch) {
//            return $this->dateWiseRepository->getCurrentTotalCollectionByBranch($branch);
//        });
//
//        $totalDeposit = Cache::remember($userId.'_deposits', $this->cacheMinute, function () use ($branch) {
//            return $this->depositRepository->getCurrentTotalDepositByBranch($branch);
//        });
//
//        $totalExpCq = Cache::remember($userId.'_total_expense_cq', $this->cacheMinute,
//            function () use ($branch, $month, $year) {
//                return $this->pettyCashRepository->getModel()
//                    ->whereIn('branch_id', $branch)
//                    ->where('month', $month)
//                    ->where('year', $year)
//                    ->sum('total_expense_cq');
//            });
//
//        $totalExp = Cache::remember($userId.'_total_exp', $this->cacheMinute, function () use ($branch, $month, $year) {
//            return $this->pettyCashRepository->getModel()
//                ->whereIn('branch_id', $branch)
//                ->where('month', $month)
//                ->where('year', $year)
//                ->sum('total_exp');
//        });
//
////
////        $settingsRepo = new TransactionSettingRepository();
////
////        dd($settingsRepo->checkIfAllowed('2019-10-03', 28));
//
//        $totalExpense = $totalExpCq + $totalExp;
//        $outstanding = $opening + $totalSales - $totalCollection;
//
//        return Cache::remember($userId.'_total_summary', $this->cacheMinute,
//            function () use (
//                $branch,
//                $opening,
//                $totalCollection,
//                $totalDeposit,
//                $totalSales,
//                $totalExpense,
//                $outstanding
//            ) {
//                return preg_replace('/\s+/', ' ',  view('backend.dashboard.partial.total_summary')
//                    ->with('branch', $branch)
//                    ->with('opening', $opening)
//                    ->with('collections', $totalCollection)
//                    ->with('deposits', $totalDeposit)
//                    ->with('sales', $totalSales)
//                    ->with('expenses', $totalExpense)
//                    ->with('outstanding', $outstanding)
//                    ->render());
//            });
//    }
//
//    /**
//     * @return string
//     * @throws \Throwable
//     */
//    public function loadSegmentSummary()
//    {
//        $startDate = Carbon::now()->firstOfMonth();
//        $endDate = Carbon::now()->lastOfMonth();
//        $userId = Auth::user()->id;
//
//        $branch = Cache::remember($userId.'_branches', $this->cacheMinute, function () {
//            return $this->getBranchIds();
//        });
//
//        $segments = Cache::remember($userId.'segments', $this->cacheMinute, function () {
//            return $this->segmentRepository->makeModel()
//                ->where('status', 1)
//                ->orderByDesc('id')
//                ->get();
//        });
//
//        $sales_row = collect();
//        foreach ($segments as $segment) {
//            $salesPie = Cache::remember($userId.'_salesPie', $this->cacheMinute,
//                function () use ($branch, $startDate, $endDate, $segment) {
//                    return $this->outstandingRepository->getModel()
//                        ->whereIn('branch_id', $branch)
//                        ->where('brand_id', $segment->id)
//                        ->whereBetween('trans_date', [$startDate->toDateString(), $endDate->toDateString()])
////                        ->where('year', $year)
//                        ->sum('net_sales_total');
//                });
//
//            $totalSales = Cache::remember($userId.'_totalSales', $this->cacheMinute,
//                function () use ($branch, $startDate, $endDate) {
//                    return $this->outstandingRepository->getModel()
//                        ->whereIn('branch_id', $branch)
//                        ->whereBetween('trans_date', [$startDate->toDateString(), $endDate->toDateString()])
////                        ->where('month', $month)->where('year', $year)
//                        ->sum('net_sales_total');
//                });
//
//            $sales_row->prepend([
//                'brand_name' => $segment->name,
//                'sales' => $salesPie,
//                'total' => $totalSales,
//            ]);
//        }
//
//        $salesPieRow = $sales_row;
//
//        $collections_row = collect();
//        foreach ($segments as $segment) {
//            $collectionsPie = Cache::remember($userId.'_collectionsPie', $this->cacheMinute, function () use ($branch, $startDate, $endDate, $segment) {
//                return $this->dateWiseRepository->getModel()
//                    ->whereIn('branch_id', $branch)
//                    ->where('brand_id', $segment->id)
//                    ->whereBetween('trans_date', [$startDate->toDateString(), $endDate->toDateString()])
////                    ->where('month', $month)->where('year', $year)
//                    ->sum('collection');
//            });
//
//            $totalCollections = Cache::remember($userId.'_totalCollections', $this->cacheMinute, function () use ($branch, $startDate, $endDate) {
//                return $this->dateWiseRepository->getModel()
//                    ->whereIn('branch_id', $branch)
//                    ->whereBetween('trans_date', [$startDate->toDateString(), $endDate->toDateString()])
////                    ->where('month', $month)->where('year', $year)
//                    ->sum('collection');
//            });
//
//            $collections_row->prepend([
//                'brand_name' => $segment->name,
//                'collections' => $collectionsPie,
//                'total' => $totalCollections
//            ]);
//        }
//        $collectionsPieRow = $collections_row;
//
//        $deposits_row = collect();
//        foreach ($segments as $segment) {
//            $depositsPie = Cache::remember($userId.$segment->id.'_depositsPie', $this->cacheMinute, function () use ($branch, $startDate, $endDate, $segment) {
//                return  $this->depositRepository->getModel()
//                    ->whereIn('branch_id', $branch)
//                    ->where('brand_id', $segment->id)
//                    ->whereBetween('trans_date', [$startDate->toDateString(), $endDate->toDateString()])
////                    ->where('month', $month)->where('year', $year)
//                    ->sum('amount');
//            });
//
//            $totalDeposits = Cache::remember($userId.$segment->id.'_totalDeposits', $this->cacheMinute, function () use ($branch, $startDate, $endDate) {
//                return $this->depositRepository->getModel()
//                    ->whereIn('branch_id', $branch)
//                    ->whereBetween('trans_date', [$startDate->toDateString(), $endDate->toDateString()])
////                    ->where('month', $month)->where('year', $year)
//                    ->sum('amount');
//            });
//
//            $deposits_row->prepend([
//                'brand_name' => $segment->name,
//                'deposits' => $depositsPie,
//                'total' => $totalDeposits
//            ]);
//        }
//        $depositsPieRow = $deposits_row;
//
//        return Cache::remember($userId.'_segment_summary', $this->cacheMinute,
//            function () use (
//                $branch,
//                $salesPieRow,
//                $collectionsPieRow,
//                $depositsPieRow
//            ) {
//                return preg_replace('/\s+/', ' ',  view('backend.dashboard.partial.segment_summary')
//                    ->with('branch', $branch)
//                    ->with('salesPieRow', $salesPieRow)
//                    ->with('collectionsPieRow', $collectionsPieRow)
//                    ->with('depositsPieRow', $depositsPieRow)
//                    ->render());
//            });
//    }
//
//    /**
//     * @return string
//     * @throws \Throwable
//     */
//    public function loadMonthWiseSummary()
//    {
////        $month = Carbon::now()->month;
////        $year = Carbon::now()->year;
////        $userId = Auth::user()->id;
////
////        $branch = Cache::remember($userId.'_branches', $this->cacheMinute, function () {
////            return $this->getBranchIds();
////        });
////
////        $end = Carbon::parse('first day of last month');
//////        $end = Carbon::now();
////        $start = Carbon::now()->subMonth(7)->firstOfMonth();
////
////        $interval = CarbonInterval::createFromDateString('1 month');
////        $periods = array_reverse(CarbonPeriod::create($start->format('Y-m-d'), $interval,
////            $end->format('Y-m-d'))->toArray());
////
////        //    Carbon::now()->lastOfMonth()->toDateString();
////
////        $allSales = new Collection();
////        $allCollections = new Collection();
////        $allDeposits = new Collection();
////        $allExpenseCq = new Collection();
////        $closing = new Collection();
////
//////        $allOpening = $this->openingRepository->getModel()
//////            ->select(['branch_id', 'collection'])
//////            ->whereIn('branch_id', $branch)
//////            ->get();
////
////        $closing_balance = 0;
////
////        foreach ($periods as $period) {
////            $sales = $this->outstandingRepository->makeModel()
////                ->whereIn('branch_id', $branch)
////                ->whereBetween('trans_date', [$period->toDateString(), $period->lastOfMonth()->toDateString()])
////                ->where('trans_date', '>=', '2019-07-01')
////                ->where('net_sales_total', '!=', 0)
////                ->sum('net_sales_total');
////
////            $collections = $this->dateWiseRepository->makeModel()
////                ->whereIn('branch_id', $branch)
////                ->whereBetween('trans_date', [$period->toDateString(), $period->lastOfMonth()->toDateString()])
////                ->where('trans_date', '>=', '2019-07-01')
////                ->where('collection', '!=', 0)
////                ->sum('collection');
////
////            $deposits = $this->depositRepository->makeModel()
////                ->whereIn('branch_id', $branch)
////                ->whereBetween('trans_date', [$period->toDateString(), $period->lastOfMonth()->toDateString()])
////                ->where('amount', '!=', 0)
////                ->sum('amount');
////
////            $all_expense_cq = $this->pettyCashRepository->makeModel()
////                ->selectRaw('(sum(total_expense_cq) + sum(total_exp)) as total_cq')
////                // ->select(['month', 'year', 'branch_id', 'total_expense_cq', 'total_exp'])
////                ->whereIn('branch_id', $branch)
////                ->whereBetween('trans_date', [$period->toDateString(), $period->lastOfMonth()->toDateString()])
////                ->where('total_expense_cq', '!=', 0)
////                ->orWhere('total_exp', '!=', 0)
////                ->first()
////                ->total_cq;
////
////            $allSales->put($period->format('Ymd'), $sales);
////            $allCollections->put($period->format('Ymd'), $collections);
////            $allDeposits->put($period->format('Ymd'), $deposits);
////            $allExpenseCq->put($period->format('Ymd'), $all_expense_cq);
////
////            $closing_balance = $opening + $sales - $collections;
////            $closing->put($period->format('Ymd'), $closing_balance);
////        }
////
//////        $allSales = $this->outstandingRepository->getModel()
//////            ->addSelect(['month', 'year', 'branch_id', 'net_sales_total'])
//////            ->whereIn('branch_id', $branch)
//////            ->whereBetween('month', [$start->format('n'), $end->format('n')])
//////            ->whereBetween('year', [$start->format('Y'), $end->format('Y')])
//////            ->where('net_sales_total', '!=', 0)
//////            ->get();
////
//////        $all_collections = $this->dateWiseRepository->getModel()
//////            ->select(['month', 'year', 'branch_id', 'collection'])
//////            ->whereIn('branch_id', $branch)
//////            ->whereBetween('month', [$start->format('n'), $end->format('n')])
//////            ->whereBetween('year', [$start->format('Y'), $end->format('Y')])
//////            ->where('collection', '!=', 0)
//////            ->get();
////
//////        $all_deposits = $this->depositRepository->getModel()
//////            ->select(['month', 'year', 'branch_id', 'amount'])
//////            ->whereIn('branch_id', $branch)
//////            ->whereBetween('month', [$start->format('n'), $end->format('n')])
//////            ->whereBetween('year', [$start->format('Y'), $end->format('Y')])
//////            ->where('amount', '!=', 0)
//////            ->get();
////
//////        $all_expense_cq = $this->pettyCashRepository->getModel()
//////            ->select(['month', 'year', 'branch_id', 'total_expense_cq', 'total_exp'])
//////            ->whereIn('branch_id', $branch)
//////            ->whereBetween('month', [$start->format('n'), $end->format('n')])
//////            ->whereBetween('year', [$start->format('Y'), $end->format('Y')])
//////            ->where('total_expense_cq', '!=', 0)
//////            ->orWhere('total_exp', '!=', 0)
//////            ->get();
//
//        $month = Carbon::now()->month;
//        $year = Carbon::now()->year;
//        $userId = Auth::user()->id;
//
//        $branch = Cache::remember($userId.'_branches', $this->cacheMinute, function () {
//            return $this->getBranchIds();
//        });
//
//        $opening = Cache::remember($userId.'_opening', $this->cacheMinute, function () use ($branch) {
//            return $this->openingRepository->getModel()->whereIn('branch_id', $branch)->sum('collection');
//        });
//
//        $end = Carbon::parse('first day of last month');
////        $end = Carbon::now();
//        $start = Carbon::now()->subMonth(7)->firstOfMonth();
//
//        $interval = CarbonInterval::createFromDateString('1 month');
//        $period = array_reverse(CarbonPeriod::create($start->format('Y-m-d'), $interval,
//            $end->format('Y-m-d'))->toArray());
//
//        $allSales = Cache::remember($userId.'_allSales', $this->cacheMinute, function () use ($branch, $start, $end) {
//            return $this->outstandingRepository->getModel()
//                ->selectRaw('concat(year,month) as yearmonth, sum(net_sales_total) as net_sales_total')
//                //  ->addSelect(['month', 'year', 'trans_date','net_sales_total'])
//                ->whereIn('branch_id', $branch)
//                ->whereBetween('trans_date', [$start->toDateString(), $end->toDateString()])
//                //  ->whereBetween('month', [$start->format('n'), $end->format('n')])
//                //  ->whereBetween('year', [$start->format('Y'), $end->format('Y')])
//                //   ->where('net_sales_total', '!=', 0)
//                ->groupBy('yearmonth')
//                ->get()
//                ->toArray();
//        });
//        $allSales = Arr::pluck($allSales, 'net_sales_total', 'yearmonth');
//
//        $allCollections = Cache::remember($userId.'_all_collections', $this->cacheMinute, function () use ($branch, $start, $end) {
//            return $this->dateWiseRepository->getModel()
//                ->selectRaw('concat(year,month) as yearmonth, sum(collection) as collection')
//                //  ->addSelect(['month', 'year', 'trans_date','net_sales_total'])
//                ->whereIn('branch_id', $branch)
//                ->whereBetween('trans_date', [$start->toDateString(), $end->toDateString()])
//                //  ->whereBetween('month', [$start->format('n'), $end->format('n')])
//                //  ->whereBetween('year', [$start->format('Y'), $end->format('Y')])
//                // ->where('collection', '<>', 0)
//                ->groupBy('yearmonth')
//                ->get()
//                ->toArray();
////                ->select(['month', 'year', 'branch_id', 'collection'])
////                ->whereIn('branch_id', $branch)
////                ->whereBetween('trans_date', [$start->toDateString(), $end->toDateString()])
////               // ->whereBetween('month', [$start->format('n'), $end->format('n')])
////             //   ->whereBetween('year', [$start->format('Y'), $end->format('Y')])
////                ->where('collection', '!=', 0)
////                ->get();
//        });
//        $allCollections = Arr::pluck($allCollections, 'collection', 'yearmonth');
//
//        $allDeposits = Cache::remember($userId.'_all_deposits', $this->cacheMinute, function () use ($branch, $start, $end) {
//            return $this->depositRepository->getModel()
//                ->selectRaw('concat(year,month) as yearmonth, sum(amount) as amount')
//                //  ->addSelect(['month', 'year', 'trans_date','net_sales_total'])
//                ->whereIn('branch_id', $branch)
//                ->whereBetween('trans_date', [$start->toDateString(), $end->toDateString()])
//                //  ->whereBetween('month', [$start->format('n'), $end->format('n')])
//                //  ->whereBetween('year', [$start->format('Y'), $end->format('Y')])
//                // ->where('collection', '<>', 0)
//                ->groupBy('yearmonth')
//                ->get();
////                ->select(['month', 'year', 'branch_id', 'amount'])
////                ->whereIn('branch_id', $branch)
////                ->whereBetween('trans_date', [$start->toDateString(), $end->toDateString()])
//////                ->whereBetween('month', [$start->format('n'), $end->format('n')])
//////                ->whereBetween('year', [$start->format('Y'), $end->format('Y')])
////                ->where('amount', '!=', 0)
////                ->get();
//        });
//        $allDeposits = Arr::pluck($allDeposits, 'amount', 'yearmonth');
//
//        $allExpCq = Cache::remember($userId.'_all_expense_cq', $this->cacheMinute, function () use ($branch, $start, $end) {
//            return $this->pettyCashRepository->getModel()
//                ->selectRaw('concat(year,month) as yearmonth, (sum(total_expense_cq) + sum(total_exp)) as total_expense_cq')
//                //  ->addSelect(['month', 'year', 'trans_date','net_sales_total'])
//                ->whereIn('branch_id', $branch)
//                ->whereBetween('trans_date', [$start->toDateString(), $end->toDateString()])
//                //  ->whereBetween('month', [$start->format('n'), $end->format('n')])
//                //  ->whereBetween('year', [$start->format('Y'), $end->format('Y')])
//                // ->where('collection', '<>', 0)
//                ->groupBy('yearmonth')
//                ->get()
//                ->toArray();
////                ->select(['month', 'year', 'branch_id', 'total_expense_cq', 'total_exp'])
////                ->whereIn('branch_id', $branch)
////                ->whereBetween('trans_date', [$start->toDateString(), $end->toDateString()])
//////                ->whereBetween('month', [$start->format('n'), $end->format('n')])
//////                ->whereBetween('year', [$start->format('Y'), $end->format('Y')])
////                ->where('total_expense_cq', '!=', 0)
////                ->orWhere('total_exp', '!=', 0)
////                ->get();
//        });
//        $allExpCq = Arr::pluck($allExpCq, 'total_expense_cq', 'yearmonth');
//
//
//        $allOpening = Cache::remember($userId.'_all_opening', $this->cacheMinute, function () use ($branch) {
//            return (float) $this->openingRepository->getModel()
//                ->selectRaw('sum(collection) as collection')
//                //->select(['branch_id', 'collection'])
//                ->whereIn('branch_id', $branch)
////                ->groupBy('yearmonth')
//                ->get()
//                ->first()
//                ->collection;
//        });
//        //  $allExpCq = Arr::pluck($allExpCq, 'collection', 'yearmonth');
//
//
//        return Cache::remember($userId.'month_summary', $this->cacheMinute,
//            function () use (
//                $period,
//                $branch,
//                $opening,
//                $allSales,
//                $allDeposits,
//                $allExpCq,
//                $allOpening,
//                $allCollections
//            ) {
//                return preg_replace('/\s+/', ' ',  view('backend.dashboard.partial.month_summary')
//                    ->with('carbon', Carbon::now())
//                    ->with('period', $period)
//                    ->with('branch', $branch)
//                    ->with('opening', $opening)
//                    ->with('all_sales', $allSales)
//                    ->with('all_deposits', $allDeposits)
//                    ->with('all_expense_cq', $allExpCq)
//                    ->with('initial_opening', $allOpening)
//                    ->with('all_collections', $allCollections)
//                    ->render());
//            });
//    }
//


//    /**
//     * @return View
//     * @throws GeneralException
//     */
//    public function index()
//    {
////        ini_set('memory_limit', '600M');
//
//        $module_parent = $this->module_parent;
//        $module_name = $this->module_name;
//        $module_name_singular = str_singular($this->module_name);
//        $module_icon = $this->module_icon;
//        $module_action = 'index';
//        $module_route = $this->module_route;
//        $module_view = $this->module_view;
//
//        return view('backend.dashboard.dashboard',
//            compact('module_name', $module_name, 'module_name_singular', 'module_parent', 'module_icon',
//                'module_action', 'module_view', 'module_route'))
//            ->with('carbon', Carbon::now())
////            ->with('period', $periods)
////            ->with('branch', $branch)
////            ->with('opening', $opening)
////            ->with('collections', $totalCollection)
////            ->with('deposits', $totalDeposit)
////            ->with('sales', $totalSales)
////            ->with('expenses', $totalExpense)
////            ->with('outstanding', $outstanding)
//////            ->with('salesPieRow', $salesPieRow)
//////            ->with('collectionsPieRow', $collectionsPieRow)
//////            ->with('depositsPieRow', $depositsPieRow)
////            ->with('all_sales', $allSales)
////            ->with('all_deposits', $allDeposits)
////            ->with('all_expense_cq', $allExpenseCq)
//////            ->with('all_opening', $all_opening)
////            ->with('all_collections', $allCollections)
//            ;
//    }
}
