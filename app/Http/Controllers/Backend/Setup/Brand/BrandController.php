<?php

namespace App\Http\Controllers\Backend\Setup\Brand;


use App\Exceptions\GeneralException;
use App\Http\Controllers\BaseController;
use App\Repositories\Backend\Setup\Brand\BrandRepository;
use Illuminate\Contracts\View\Factory;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\View\View;
use Throwable;

/**
 * Class BrandController
 *
 * @package App\Http\Controllers\Backend\Setup\Brand
 */
final class BrandController extends BaseController
{
    /**
     * BrandController constructor.
     *
     * @param  BrandRepository  $brandRepository
     */
    public function __construct(BrandRepository $brandRepository)
    {
        $this->repository = $brandRepository;

        $this->module_action = '';
        $this->module_parent = 'Setup';
        $this->module_name = 'brands';
        $this->module_icon = 'mdi-hospital-building';
        $this->module_route = 'admin.setup.'.$this->module_name;
        $this->module_view = 'backend.setup.'.$this->module_name;
    }

    /**
     * @return Factory|View
     */
    public function index()
    {
        $module_parent = $this->module_parent;
        $module_name = $this->module_name;
        $module_name_singular = str_singular($this->module_name);
        $module_icon = $this->module_icon;
        $module_action = 'index';
        $module_route = $this->module_route;
        $module_view = $this->module_view;

        $page_heading = $this->module_parent.' - View All';

        $$module_name = $this->repository->getAll();
        return view($module_view.'.'.$module_action,
            compact('module_name', $module_name, 'module_name_singular', 'module_parent', 'module_icon', 'page_heading',
                'module_action', 'module_view', 'module_route'));
    }

    /**
     * @return Factory|View
     */
    public function create()
    {
        $module_parent = $this->module_parent;
        $module_name = $this->module_name;
        $module_name_singular = str_singular($this->module_name);
        $module_icon = $this->module_icon;
        $module_action = 'create';
        $module_route = $this->module_route;
        $module_view = $this->module_view;

        $page_heading = $this->module_parent.' - Add New';

        return view($module_view.'.'.$module_action,
            compact('module_name', 'module_name_singular', 'module_parent', 'module_icon', 'page_heading',
                'module_action', 'module_view', 'module_route'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  Request  $request
     * @return Response
     * @throws Throwable
     */
    public function store(Request $request)
    {
        $module_route = $this->module_route;

        $this->repository->create([
            'data' => $request->except(['_token'])
        ]);

        return redirect()->route($module_route.'.index')->withFlashSuccess('Record successfully saved');
    }

    /**
     * Display the specified resource.
     *
     * @param $id
     * @return Response
     * @internal param int $id
     */
    public function show($id)
    {
        $module_parent = $this->module_parent;
        $module_name = $this->module_name;
        $module_name_singular = str_singular($this->module_name);
        $module_icon = $this->module_icon;
        $module_action = 'show';
        $module_route = $this->module_route;
        $module_view = $this->module_view;

        $page_heading = $this->module_parent.' - View';

        $$module_name_singular = $this->repository->find($id);

        return view($module_view.'.show',
            compact('module_name', 'module_name_singular', 'module_parent', 'module_icon', 'page_heading',
                'module_action', 'module_view', 'module_route'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
        $module_parent = $this->module_parent;
        $module_name = $this->module_name;
        $module_name_singular = str_singular($this->module_name);
        $module_icon = $this->module_icon;
        $module_action = 'edit';
        $module_route = $this->module_route;
        $module_view = $this->module_view;

        $page_heading = $this->module_parent.' - Edit';

        $$module_name_singular = $this->repository->find($id);
        // echo "<pre/>";
        // print_r($$module_name_singular);
        // exit();
        return view($module_view.'.edit',
            compact('module_name', $module_name_singular, 'module_name_singular', 'module_parent', 'module_icon',
                'page_heading', 'module_action', 'module_view', 'module_route'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  Request  $request
     * @param  int      $id
     * @return Response
     * @throws Throwable
     */
    public function update($id, Request $request)
    {
        $module_route = $this->module_route;

        $this->repository->update($id, [
            'data' => $request->except(
                '_token',
                '_method'
            ),
        ]);

        return redirect()->route($module_route.'.index')->withFlashSuccess(trans('Record successfully updated'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     * @throws Throwable
     */
    public function destroy($id)
    {
        $module_route = $this->module_route;

        $this->repository->delete($id);

        return redirect()->route($module_route.'.deleted')->withFlashSuccess(trans('Record successfully deleted'));
    }

    /**
     * @param $id
     * @return mixed
     * @throws GeneralException
     * @throws Throwable
     */
    public function restore($id)
    {
        $module_route = $this->module_route;

        $this->repository->restore($id);

        return redirect()->route($module_route.'.index')->withFlashSuccess(trans('Record successfully restored'));
    }

    /**
     * @param $id
     * @return mixed
     * @throws GeneralException
     * @throws Throwable
     */
    public function delete($id)
    {
        $module_route = $this->module_route;
        $this->repository->forceDelete($id);

        return redirect()->route($module_route.'.deleted')->withFlashSuccess(trans('Records deleted permanently'));
    }

    /**
     * @param $id
     * @param $status
     * @return mixed
     * @throws GeneralException
     */
    public function mark($id, $status)
    {
        $module_route = $this->module_route;

        $this->repository->mark($id, $status);

        return redirect()->route($status === 1 ? $module_route.'.index' : $module_route.'.deactivated')->withFlashSuccess(trans('Records updated successfully'));
    }

    /**
     * @param  Request  $request
     * @return Factory|View
     */
    public function getDeactivated(Request $request)
    {
        $module_name = $this->module_name;
        $module_name_singular = str_singular($this->module_name);
        $module_action = 'deactivated';
        $module_route = $this->module_route;
        $module_parent = $this->module_parent;
        $module_view = $this->module_view;
        $module_icon = $this->module_icon;

        $page_heading = page_title($this->module_parent, $module_name, 'All Deactivated');

        $$module_name = $this->repository->getDeactivated();

        return view($module_view.'.deactivated', compact('module_name', $module_name,
            'module_name_singular', 'module_parent', 'module_icon', 'page_heading', 'module_action', 'module_view',
            'module_route'));
    }

    /**
     * @return Factory|View
     */
    public function trashed()
    {
        $module_name = $this->module_name;
        $module_name_singular = str_singular($this->module_name);
        $module_action = 'deleted';
        $module_route = $this->module_route;
        $module_parent = $this->module_parent;
        $module_view = $this->module_view;
        $module_icon = $this->module_icon;

        $page_heading = page_title($this->module_parent, $module_name, 'All Deleted');

        $$module_name = $this->repository->getDeleted();

        return view($module_view.'.deleted',
            compact('module_name', $module_name, 'module_name_singular', 'module_parent', 'module_icon', 'page_heading',
                'module_action', 'module_view', 'module_route'));
    }
}
