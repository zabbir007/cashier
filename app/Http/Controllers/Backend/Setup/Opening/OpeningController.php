<?php

namespace App\Http\Controllers\Backend\Setup\Opening;


use App\Exceptions\GeneralException;
use App\Http\Controllers\BaseController;
use App\Models\Branch\Branch;
use App\Repositories\Backend\Setup\Brand\BrandRepository;
use App\Repositories\Backend\Setup\Opening\OpeningSummaryRepository;
use App\Repositories\Backend\Setup\Product\ProductRepository;
use Carbon\Carbon;
use Illuminate\Contracts\View\Factory;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\View\View;
use Throwable;


/**
 * Class OpeningController
 *
 * @package App\Http\Controllers\Backend\Setup\Opening
 */
final class OpeningController extends BaseController
{
    /**
     * @var ProductRepository
     */
    private $products;
    /**
     * @var BrandRepository
     */
    private $brands;

    /**
     * OpeningController constructor.
     *
     * @param  OpeningSummaryRepository  $openingSummaryRepository
     * @param  ProductRepository         $productRepository
     * @param  BrandRepository           $brandRepository
     */
    public function __construct(
        OpeningSummaryRepository $openingSummaryRepository,
        ProductRepository $productRepository,
        BrandRepository $brandRepository
    ) {
        $this->repository = $openingSummaryRepository;
        $this->products = $productRepository;
        $this->brands = $brandRepository;

        $this->module_action = '';
        $this->module_parent = 'Transaction Date Wise';
        $this->module_name = 'openings';
        $this->module_icon = 'mdi-hospital-building';
        $this->module_route = 'admin.setup.'.$this->module_name;
        $this->module_view = 'backend.setup.'.$this->module_name;
    }

    /**
     * @return Factory|View
     * @throws GeneralException
     */
    public function index()
    {
        $module_parent = $this->module_parent;
        $module_name = $this->module_name;
        $module_name_singular = str_singular($this->module_name);
        $module_icon = $this->module_icon;
        $module_action = 'index';
        $module_route = $this->module_route;
        $module_view = $this->module_view;

        $page_heading = $this->module_parent.' - View All';

        $$module_name = $this->repository->getAllByBranch($this->getBranchIds());

//        Log::alert("Browse : ".ucfirst($module_parent) . "  " . ucfirst($module_action) . " By User : " .   Auth::user()->name);
        return view($module_view.'.index',
            compact('module_name', $module_name, 'module_name_singular', 'module_parent', 'module_icon', 'page_heading',
                'module_action', 'module_view', 'module_route'))
            ->with('carbon', Carbon::now());
    }

    /**
     * @return $this
     * @throws GeneralException
     */
    public function create()
    {
        $module_parent = $this->module_parent;
        $module_name = $this->module_name;
        $module_name_singular = str_singular($this->module_name);
        $module_icon = $this->module_icon;
        $module_action = 'create';
        $module_route = $this->module_route;
        $module_view = $this->module_view;

        $page_heading = $this->module_parent.' - Add New';

        $brands = $this->brands->getAll();

        $branch = $this->getBranch();
        $branches = Branch::all()->pluck('name', 'id');

        $branchProduct = $branch->products->pluck('id');
//        $branchProduct = Product::all()->pluck('id');

        foreach ($brands as $brand) {
            $products = $this->products->getAll()->where('brand_id', $brand->id)->whereIn('id', $branchProduct);

            $table = [
                'brand_id' => $brand->id,
                'brand_name' => $brand->name,
                'all' => $products,
            ];
            $product_all[] = $table;
        }

//        Log::alert("Create : ".ucfirst($module_parent) . "  " . ucfirst($module_action) . " By User : " .   Auth::user()->name);

        return view($module_view.'.create',
            compact('module_name', 'module_name_singular', 'module_parent', 'module_icon', 'brand',
                'page_heading', 'module_action', 'module_view', 'module_route'))
            ->with('carbon', Carbon::now())
            ->with('products', $product_all)
            ->with('branches', $branches);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  Request  $request
     * @return Response
     * @throws GeneralException
     */
    public function store(Request $request)
    {
        $module_route = $this->module_route;

        $this->repository->create([
            'data' => $request->except(['_token']),
        ]);

//        \Log::info(ucfirst($module_action) . " '$module_name': '" . $request->name . ", ID:" . $request->id . " ' by User:" . $this->user->name);
        return redirect()->route($module_route.'.index')->withFlashSuccess('Record successfully saved');
    }

    /**
     * Display the specified resource.
     *
     * @param $id
     * @return Response
     * @throws GeneralException
     * @internal param int $id
     */
    public function show($id)
    {
        $module_parent = $this->module_parent;
        $module_name = $this->module_name;
        $module_name_singular = str_singular($this->module_name);
        $module_icon = $this->module_icon;
        $module_action = 'show';
        $module_route = $this->module_route;
        $module_view = $this->module_view;

        $page_heading = $this->module_parent.' - View';

        $brands = $this->brands->getAll();

        $branch = $this->getBranch();
        $branchProduct = $branch->products->pluck('id');

        $product_all = [];

        foreach ($brands as $brand) {
            $products = $this->products->getAll()->where('brand_id', $brand->id)->whereIn('id', $branchProduct);

            $table = [
                'brand_id' => $brand->id,
                'brand_name' => $brand->name,
                'all' => $products,
            ];
            $product_all[] = $table;
        }

        $module_name_singular = $this->repository->getById($id);

        return view($module_view.'.show',
            compact('module_name', 'module_name_singular', 'module_parent', 'module_icon', 'page_heading',
                'module_action', 'module_view', 'module_route'))
            ->with('partial', 'overview')
            ->with('carbon', Carbon::now())
            ->with('products', $product_all)
            ->with('branches', $branch);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     * @throws GeneralException
     */
    public function edit($id)
    {
        $module_parent = $this->module_parent;
        $module_name = $this->module_name;
        $module_name_singular = str_singular($this->module_name);
        $module_icon = $this->module_icon;
        $module_action = 'Edit';
        $module_route = $this->module_route;
        $module_view = $this->module_view;

        $page_heading = $this->module_parent.' - Edit';

        $brands = $this->brands->getAll();

        $branch = $this->getBranch();

        $branchProduct = $branch->products->pluck('id');

        $product_all = [];
        foreach ($brands as $brand) {
            $products = $this->products->getAll()->where('brand_id', $brand->id)->whereIn('id', $branchProduct);

            $table = [
                'brand_id' => $brand->id,
                'brand_name' => $brand->name,
                'all' => $products,
            ];
            $product_all[] = $table;
        }

        $module_name_singular = $this->repository->find($id);

//        Log::alert("Edit : ".ucfirst($module_parent) . "  " . ucfirst($module_action) . " By User : " .   Auth::user()->name);
        return view($module_view.'.edit',
            compact('module_name', $module_name_singular, 'module_name_singular', 'module_parent', 'module_icon',
                'brand', 'page_heading', 'module_action', 'module_view', 'module_route'))
            ->with('carbon', Carbon::now())
            ->with('products', $product_all)
            ->with('branches', $branch);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  Request  $request
     * @param  int      $id
     * @return Response
     * @throws GeneralException
     */
    public function update($id, Request $request)
    {
        $module_route = $this->module_route;

        $this->repository->update($id, [
            'data' => $request->except(
                '_token',
                '_method'
            ),
            'banks' => $request->only('bank_list'),
        ]);

        return redirect()->route($module_route.'.index')->withFlashSuccess(trans('Record successfully updated'));
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return Response
     * @throws Throwable
     */
    public function approve($id)
    {
        $module_route = $this->module_route;

        $this->repository->approve($id);

        return redirect()->route($module_route.'.deleted')->withFlashSuccess(trans('Record successfully deleted'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     * @throws Throwable
     */
    public function destroy($id)
    {
        $module_route = $this->module_route;

        $this->repository->delete($id);

        return redirect()->route($module_route.'.deleted')->withFlashSuccess(trans('Record successfully deleted'));
    }

    /**
     * @param $id
     * @return mixed
     * @throws GeneralException
     * @throws Throwable
     */
    public function restore($id)
    {
        $module_route = $this->module_route;

        $this->repository->restore($id);

        return redirect()->route($module_route.'.index')->withFlashSuccess(trans('Record successfully restored'));
    }

    /**
     * @param $id
     * @return mixed
     * @throws GeneralException
     * @throws Throwable
     */
    public function delete($id)
    {
        $module_route = $this->module_route;
        $this->repository->forceDelete($id);

        return redirect()->route($module_route.'.deleted')->withFlashSuccess(trans('Records deleted permanently'));
    }

    /**
     * @param $id
     * @param $status
     * @return mixed
     * @throws GeneralException
     */
    public function mark($id, $status)
    {
        $module_route = $this->module_route;

        $this->repository->mark($id, $status);

        return redirect()->route($status === 1 ? $module_route.'.index' : $module_route.'.deactivated')->withFlashSuccess(trans('Records updated successfully'));
    }

    /**
     * @return Factory|View
     */
    public function getDeactivated()
    {
        $module_name = $this->module_name;
        $module_name_singular = str_singular($this->module_name);
        $module_action = 'deactivated';
        $module_route = $this->module_route;
        $module_parent = $this->module_parent;
        $module_view = $this->module_view;
        $module_icon = $this->module_icon;

        $page_heading = page_title($this->module_parent, $module_name, 'All Deactivated');

        $$module_name = $this->repository->getDeactivated();

        return view($module_view.'.deactivated', compact('module_name', $module_name,
            'module_name_singular', 'module_parent', 'module_icon', 'page_heading', 'module_action', 'module_view',
            'module_route'));
    }

    /**
     * @return Factory|View
     */
    public function trashed()
    {
        $module_name = $this->module_name;
        $module_name_singular = str_singular($this->module_name);
        $module_action = 'deleted';
        $module_route = $this->module_route;
        $module_parent = $this->module_parent;
        $module_view = $this->module_view;
        $module_icon = $this->module_icon;

        $page_heading = page_title($this->module_parent, $module_name, 'All Deleted');

        $$module_name = $this->repository->getDeleted();

        return view($module_view.'.deleted',
            compact('module_name', $module_name, 'module_name_singular', 'module_parent', 'module_icon', 'page_heading',
                'module_action', 'module_view', 'module_route'));
    }
}
