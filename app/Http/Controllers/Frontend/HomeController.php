<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\BaseController;
use Illuminate\Http\RedirectResponse;
use Redirect;

/**
 * Class HomeController.
 */
class HomeController extends BaseController
{
    public function __construct()
    {
        $this->module_action = '';
        $this->module_parent = 'Dashboard';
        $this->module_name = '';
        $this->module_icon = 'mdi-hospital-building';
        $this->module_route = '';
    }

    /**
     * @return RedirectResponse
     */
    public function index()
    {
        return Redirect::route('admin.dashboard');
    }
}
