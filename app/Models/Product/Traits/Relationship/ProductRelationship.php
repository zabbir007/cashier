<?php

namespace App\Models\Product\Traits\Relationship;

use App\Models\Branch\Branch;


/**
 * Class UserRelationship.
 */
trait ProductRelationship
{
    public function branches()
    {
        return $this->belongsToMany(Branch::class, "product_branch", 'product_id', 'branch_id');
    }

}
