<?php

namespace App\Models\Product\Traits\Scope;

/**
 * Class UserScope.
 */
trait ProductScope
{
    /**
     * @param        $query
     * @param  bool  $status
     *
     * @return mixed
     */
    public function scopeActive($query, $status = true)
    {
        return $query->where('status', $status);
    }


}
