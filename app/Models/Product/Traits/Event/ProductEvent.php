<?php

namespace App\Models\Product\Traits\Event;

use Auth;
use Carbon\Carbon;

/**
 * Class UserRelationship.
 */
trait ProductEvent
{
    public static function boot()
    {
        parent::boot();

        self::creating(function ($model) {
            $model->created_at = Carbon::now();
            $model->created_by = Auth::id();
        });

        self::updating(function ($model) {
            $model->updated_at = Carbon::now();
            $model->updated_by = Auth::id();
        });

        self::deleting(function ($model) {
            $model->deleted_at = Carbon::now();
            $model->deleted_by = Auth::id();
        });

    }
}
