<?php

namespace App\Models\Product;

use App\Models\BaseModel;
use App\Models\Product\Traits\Attribute\ProductAttribute;
use App\Models\Product\Traits\Event\ProductEvent;
use App\Models\Product\Traits\Relationship\ProductRelationship;
use App\Models\Product\Traits\Scope\ProductScope;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Product
 *
 * @package App\Models\Product
 */
final class Product extends BaseModel
{
    use ProductScope,
        SoftDeletes,
        ProductAttribute,
        ProductRelationship,
        ProductEvent;

    /**
     * @var array
     */
    protected $guarded = [
        'id',
        'updated_at',
        '_token',
        '_method'
    ];

    /**
     * @var string
     */
    protected $table = 'products';

    /**
     * @param  string  $eventName
     * @return string
     */
    public function getDescriptionForEvent(string $eventName): string
    {
        return 'Setup: Product: '.$this->trans_sl." is {$eventName}";
    }
}

