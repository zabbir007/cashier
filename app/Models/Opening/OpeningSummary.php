<?php

namespace App\Models\Opening;

use App\Models\BaseModel;
use App\Models\Opening\Traits\Attribute\OpeningSummaryAttribute;
use App\Models\Opening\Traits\Event\OpeningSummaryEvent;
use App\Models\Opening\Traits\Relationship\OpeningSummaryRelationship;
use App\Models\Opening\Traits\Scope\OpeningSummaryScope;
use Illuminate\Database\Eloquent\SoftDeletes;


/**
 * Class OpeningSummary
 *
 * @package App\Models\Opening
 */
final class OpeningSummary extends BaseModel
{
    use OpeningSummaryEvent,
        OpeningSummaryAttribute,
        SoftDeletes,
        OpeningSummaryRelationship,
        OpeningSummaryScope;

    /**
     * @var string
     */
    protected $table = 'setup_opening_summary';

    /**
     * @var array
     */
    protected $guarded = [
        'id',
        'updated_at',
        '_token',
        '_method'
    ];

    /**
     * @var array
     */
    protected $dates = [
        'deleted_at',
        'requisition_at',
        'transfer_at',
        'adjustment_at',
    ];

//    protected $primaryKey = 'sl';
//    public $keyType = "string";

    /**
     * @param  string  $eventName
     * @return string
     */
    public function getDescriptionForEvent(string $eventName): string
    {
        return 'Setup: Opening Balance: '.$this->sl." is {$eventName}";
    }
}
