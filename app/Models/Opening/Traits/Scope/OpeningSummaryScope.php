<?php

namespace App\Models\Opening\Traits\Scope;

/**
 * Class UserScope.
 */
trait OpeningSummaryScope
{
    /**
     * @param        $query
     * @param  bool  $status
     *
     * @return mixed
     */
    public function scopeActive($query, $status = true)
    {
        return $query->where('status', $status);
    }
}
