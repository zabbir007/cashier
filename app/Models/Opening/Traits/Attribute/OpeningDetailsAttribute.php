<?php

namespace App\Models\Opening\Traits\Attribute;


/**
 * Trait OpeningDetailsAttribute
 *
 * @package App\Models\Opening\Traits\Attribute
 */
trait OpeningDetailsAttribute
{
    /**
     * @return string
     */
    public function getStatusLabelAttribute()
    {
        if ($this->isActive()) {
            return "<label class='label label-success'>".$this->statuses->name.'</label>';
        }

        return "<label class='label label-danger'>".$this->statuses->name.'</label>';
    }

    /**
     * @return bool
     */
    public function isActive()
    {
        return $this->status == 1;
    }
}
