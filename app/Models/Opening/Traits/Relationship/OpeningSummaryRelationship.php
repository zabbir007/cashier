<?php

namespace App\Models\Opening\Traits\Relationship;

use App\Models\Opening\OpeningDetails;

/**
 * Trait OpeningSummaryRelationship
 *
 * @package App\Models\Opening\Traits\Relationship
 */
trait OpeningSummaryRelationship
{

    /**
     * @return mixed
     */
    public function details()
    {
        return $this->hasMany(OpeningDetails::class, 'trans_sl', 'sl');
    }
    

}
