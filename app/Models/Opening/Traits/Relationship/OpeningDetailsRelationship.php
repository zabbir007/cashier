<?php

namespace App\Models\Opening\Traits\Relationship;

use App\Models\Opening\OpeningSummary;


/**
 * Class UserRelationship.
 */
trait OpeningDetailsRelationship
{

    public function summary()
    {
        return $this->hasOne(OpeningSummary::class, 'sl', 'trans_sl');
    }
}
