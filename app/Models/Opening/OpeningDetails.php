<?php

namespace App\Models\Opening;

use App\Models\BaseModel;
use App\Models\Opening\Traits\Attribute\OpeningDetailsAttribute;
use App\Models\Opening\Traits\Event\OpeningDetailsEvent;
use App\Models\Opening\Traits\Relationship\OpeningDetailsRelationship;
use App\Models\Opening\Traits\Scope\OpeningDetailsScope;
use Illuminate\Database\Eloquent\SoftDeletes;


/**
 * Class OpeningDetails
 *
 * @package App\Models\Opening
 */
final class OpeningDetails extends BaseModel
{
    use OpeningDetailsScope,
        OpeningDetailsAttribute,
        SoftDeletes,
        OpeningDetailsRelationship,
        OpeningDetailsEvent;

    /**
     * @var string
     */
    protected $table = 'setup_opening_details';
    /**
     * @var array
     */
    protected $guarded = [
        'id',
        'updated_at',
        '_token',
        '_method'
    ];
    /**
     * @var array
     */
    protected $dates = [
        'deleted_at',
    ];

    /**
     * @param  string  $eventName
     * @return string
     */
    public function getDescriptionForEvent(string $eventName): string
    {
        return 'Setup: Opening Balance: '.$this->trans_sl." is {$eventName}";
    }
}
