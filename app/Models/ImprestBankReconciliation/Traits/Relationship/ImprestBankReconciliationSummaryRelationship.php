<?php

namespace App\Models\ImprestBankReconciliation\Traits\Relationship;

use App\Models\Access\User\User;
use App\Models\Bank\Bank;
use App\Models\Branch\Branch;
use App\Models\Customer\Customer;
use App\Models\ImprestBankReconciliation\ImprestBankReconciliationDetails;


/**
 * Trait ImprestBankReconciliationSummaryRelationship
 *
 * @package App\Models\ImprestBankReconciliation\Traits\Relationship
 */
trait ImprestBankReconciliationSummaryRelationship
{

    /**
     * @return mixed
     */
    public function details()
    {
        return $this->hasMany(ImprestBankReconciliationDetails::class, 'trans_sl', 'sl');
    }

    /**
     * @return mixed
     */
    public function branch()
    {
        return $this->belongsTo(Branch::class, 'branch_id', 'id');
    }

    /**
     * @return mixed
     */
    public function bank()
    {
        return $this->belongsTo(Bank::class, 'bank_id', 'id');
    }

    /**
     * @return mixed
     */
    public function createBy()
    {
        return $this->hasOne(User::class, 'id', 'created_by');
    }

    /**
     * @return mixed
     */
    public function approveBy()
    {
        return $this->hasOne(User::class, 'id', 'created_by');
    }
}
