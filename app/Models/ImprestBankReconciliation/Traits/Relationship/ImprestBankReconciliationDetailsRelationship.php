<?php

namespace App\Models\ImprestBankReconciliation\Traits\Relationship;

use App\Models\ImprestBankReconciliation\OutstandingReconciliationSummary;


/**
 * Trait ImprestBankReconciliationDetailsRelationship
 *
 * @package App\Models\ImprestBankReconciliation\Traits\Relationship
 */
trait ImprestBankReconciliationDetailsRelationship
{

    /**
     * @return mixed
     */
    public function summary()
    {
        return $this->hasOne(OutstandingReconciliationSummary::class, 'sl', 'transfer_sl');
    }
}
