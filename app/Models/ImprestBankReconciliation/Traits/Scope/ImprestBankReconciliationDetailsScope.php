<?php

namespace App\Models\ImprestBankReconciliation\Traits\Scope;


/**
 * Trait ImprestBankReconciliationDetailsScope
 *
 * @package App\Models\ImprestBankReconciliation\Traits\Scope
 */
trait ImprestBankReconciliationDetailsScope
{
    /**
     * @param        $query
     * @param  bool  $status
     *
     * @return mixed
     */
    public function scopeActive($query, $status = true)
    {
        return $query->where('status', $status);
    }
}
