<?php

namespace App\Models\ImprestBankReconciliation\Traits\Scope;

use function request;

/**
 * Trait ImprestBankReconciliationSummaryScope
 *
 * @package App\Models\ImprestBankReconciliation\Traits\Scope
 */
trait ImprestBankReconciliationSummaryScope
{
    /**
     * @param        $query
     * @param  bool  $status
     *
     * @return mixed
     */
    public function scopeActive($query, $status = true)
    {
        return $query->where('status', $status);
    }

    /**
     * @param $query
     * @return mixed
     */
    public function scopeBranch($query)
    {
        return $query->whereIn('branch_id', request()->user()->branches->pluck('id'));
    }
}
