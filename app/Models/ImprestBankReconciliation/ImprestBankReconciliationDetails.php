<?php

namespace App\Models\ImprestBankReconciliation;

use App\Models\BaseModel;
use App\Models\ImprestBankReconciliation\Traits\Attribute\ImprestBankReconciliationDetailsAttribute;
use App\Models\ImprestBankReconciliation\Traits\Event\ImprestBankReconciliationDetailsEvent;
use App\Models\ImprestBankReconciliation\Traits\Relationship\ImprestBankReconciliationDetailsRelationship;
use App\Models\ImprestBankReconciliation\Traits\Scope\ImprestBankReconciliationDetailsScope;
use Illuminate\Database\Eloquent\SoftDeletes;


/**
 * Class ImprestBankReconciliationDetails
 *
 * @package App\Models\ImprestBankReconciliation
 */
final class ImprestBankReconciliationDetails extends BaseModel
{
    use ImprestBankReconciliationDetailsScope,
        ImprestBankReconciliationDetailsAttribute,
        SoftDeletes,
        ImprestBankReconciliationDetailsRelationship,
        ImprestBankReconciliationDetailsEvent;

    /**
     * @var string
     */
    protected $table = 'trns_impr_bnk_details';
    /**
     * @var array
     */
    protected $guarded = [
        'id',
        'updated_at',
        '_token',
        '_method'
    ];
    /**
     * @var array
     */
    protected $dates = [
        'deleted_at',
    ];

    /**
     * @param  string  $eventName
     * @return string
     */
    public function getDescriptionForEvent(string $eventName): string
    {
        return 'Transaction: Imprest Bank Reconciliation: '.$this->trans_sl." is {$eventName}";
    }
}
