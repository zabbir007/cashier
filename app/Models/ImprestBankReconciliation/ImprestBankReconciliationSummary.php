<?php

namespace App\Models\ImprestBankReconciliation;

use App\Models\BaseModel;
use App\Models\ImprestBankReconciliation\Traits\Attribute\ImprestBankReconciliationSummaryAttribute;
use App\Models\ImprestBankReconciliation\Traits\Event\ImprestBankReconciliationSummaryEvent;
use App\Models\ImprestBankReconciliation\Traits\Relationship\ImprestBankReconciliationSummaryRelationship;
use App\Models\ImprestBankReconciliation\Traits\Scope\ImprestBankReconciliationSummaryScope;
use App\Models\TransactionMethod;
use Illuminate\Database\Eloquent\SoftDeletes;


/**
 * Class ImprestBankReconciliationSummary
 *
 * @package App\Models\ImprestBankReconciliation
 */
final class ImprestBankReconciliationSummary extends BaseModel
{
    use ImprestBankReconciliationSummaryEvent,
        ImprestBankReconciliationSummaryAttribute,
        SoftDeletes,
        ImprestBankReconciliationSummaryRelationship,
        ImprestBankReconciliationSummaryScope,
        TransactionMethod;

    /**
     * @var string
     */
    protected $table = 'trns_impr_bnk_summary';

    /**
     * @var array
     */
    protected $guarded = [
        'id',
        'updated_at',
        '_token',
        '_method'
    ];

    /**
     * @var array
     */
    protected $dates = [
        'deleted_at',
        'requisition_at',
        'transfer_at',
        'adjustment_at',
    ];

    /**
     * @param  string  $eventName
     * @return string
     */
    public function getDescriptionForEvent(string $eventName): string
    {
        return 'Transaction: Imprest Bank Reconciliation: '.$this->sl." is {$eventName}";
    }

}
