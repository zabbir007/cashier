<?php

namespace App\Models\Employee;

use App\Models\BaseModel;
use App\Models\Employee\Traits\Attribute\EmployeeAttribute;
use App\Models\Employee\Traits\Event\EmployeeEvent;
use App\Models\Employee\Traits\Relationship\EmployeeRelationship;
use App\Models\Employee\Traits\Scope\EmployeeScope;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Employee
 *
 * @package App\Models\Employee
 */
final class Employee extends BaseModel
{
    use EmployeeScope,
        SoftDeletes,
        EmployeeAttribute,
        EmployeeRelationship,
        EmployeeEvent;

    /**
     * @var array
     */
    protected $guarded = [
        'id',
        'updated_at',
        '_token',
        '_method'
    ];

    /**
     * @var string
     */
    protected $table = 'employees';

    /**
     * @param  string  $eventName
     * @return string
     */
    public function getDescriptionForEvent(string $eventName): string
    {
        return 'Setup: Employee: '.$this->name." is {$eventName}";
    }
}

