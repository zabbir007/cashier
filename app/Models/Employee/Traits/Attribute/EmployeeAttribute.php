<?php

namespace App\Models\Employee\Traits\Attribute;

/**
 * Class UserAttribute.
 */
trait EmployeeAttribute
{
    /**
     * @return string
     */
    public function getStatusLabelAttribute()
    {
        if ($this->isActive()) {
            return "<label class='label label-success'>".'Active'.'</label>';
        }

        return "<label class='label label-danger'>"."Inactive".'</label>';
    }

//    function getFullNameAttribute()
//    {
//        return $this->code . ' || ' . $this->name;
//    }

    /**
     * @return bool
     */
    public function isActive()
    {
        return $this->status == 1;
    }

    /**
     * @return string
     */
    public function getShowButtonAttribute()
    {
        return '<a href="'.route("admin.setup.employees.show",
                $this).'" class=""><i class="si si-eye" data-toggle="tooltip" data-placement="top" title="'.trans('buttons.general.crud.view').'"></i></a> ';
    }

    /**
     * @return string
     */
    public function getEditButtonAttribute()
    {
        return '<a href="'.route("admin.setup.employees.edit",
                $this).'" class=""><i class="si si-note" data-toggle="tooltip" data-placement="top" title="'.trans('buttons.general.crud.edit').'"></i></a> ';
    }

    /**
     * @return string
     */
    public function getStatusButtonAttribute()
    {
        switch ($this->status) {
            case 2:
                return '<a href="'.route('admin.setup.employees.mark', [
                        $this,
                        1,
                    ]).'" class=""><i class="si si-control-play" data-toggle="tooltip" data-placement="top" title="'.trans('buttons.backend.access.users.activate').'"></i></a> ';
            // No break

            case 1:
                return '<a href="'.route('admin.setup.employees.mark', [
                        $this,
                        2,
                    ]).'" class=""><i class="si si-control-pause" data-toggle="tooltip" data-placement="top" title="'.trans('buttons.backend.access.users.deactivate').'"></i></a> ';
            // No break

            default:
                return '';
            // No break
        }
    }

    /**
     * @return string
     */
    public function getDeleteButtonAttribute()
    {
        return '<a href="'.route("admin.setup.employees.destroy", $this).'"
                 data-method="delete"
                 data-trans-button-cancel="'.trans('buttons.general.cancel').'"
                 data-trans-button-confirm="'.trans('buttons.general.crud.delete').'"
                 data-trans-title="'.trans('strings.backend.general.are_you_sure').'"
                 class=""><i class="si si-trash" data-toggle="tooltip" data-placement="top" title="'.trans('buttons.general.crud.delete').'"></i></a> ';
    }

    /**
     * @return string
     */
    public function getRestoreButtonAttribute()
    {
        return '<a href="'.route("admin.setup.employees.restore",
                $this).'" name="restore_user" class=""><i class="si si-refresh" data-toggle="tooltip" data-placement="top" title="'.trans('buttons.general.crud.restore').'"></i></a> ';
    }

    /**
     * @return string
     */
    public function getDeletePermanentlyButtonAttribute()
    {
        return '<a href="'.route("admin.setup.employees.delete-permanently",
                $this).'" name="delete_user_perm" class=""><i class="si si-close" data-toggle="tooltip" data-placement="top" title="'.trans('buttons.general.crud.delete_permanently').'"></i></a> ';
    }

    /**
     * @return string
     */
    public function getActionButtonsAttribute()
    {
        if ($this->trashed()) {
            return $this->restore_button.$this->delete_permanently_button;
        }

        return
            $this->show_button.
            $this->edit_button.
            $this->status_button.
            $this->delete_button;
    }
}
