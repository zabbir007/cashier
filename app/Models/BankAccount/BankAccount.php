<?php

namespace App\Models\BankAccount;

use App\Models\BankAccount\Traits\Attribute\BankAccountAttribute;
use App\Models\BankAccount\Traits\Event\BankAccountEvent;
use App\Models\BankAccount\Traits\Relationship\BankAccountRelationship;
use App\Models\BankAccount\Traits\Scope\BankAccountScope;
use App\Models\BaseModel;
use Illuminate\Database\Eloquent\SoftDeletes;


/**
 * Class BankAccount
 *
 * @package App\Models\Bank
 */
final class BankAccount extends BaseModel
{
    use BankAccountScope,
        SoftDeletes,
        BankAccountAttribute,
        BankAccountRelationship,
        BankAccountEvent;

    /**
     * @var array
     */
    protected $guarded = [
        'id',
        'updated_at',
        '_token',
        '_method'
    ];

    /**
     * @var string
     */
    protected $table = 'bank_accounts';

    /**
     * @param  string  $eventName
     * @return string
     */
    public function getDescriptionForEvent(string $eventName): string
    {
        return 'Setup: Bank Account: '.$this->account_number." is {$eventName}";
    }
}

