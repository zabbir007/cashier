<?php

namespace App\Models\BankAccount\Traits\Event;

use Auth;
use Carbon\Carbon;

/**
 * Trait BankEvent
 *
 * @package App\Models\BankAccount\Traits\Event
 */
trait BankAccountEvent
{
    /**
     *
     */
    public static function boot()
    {
        parent::boot();

        self::creating(function ($model) {
            $model->created_by = Auth::id();
            $model->created_at = Carbon::now();

        });

        self::updating(function ($model) {
            $model->updated_by = Auth::id();
            $model->updated_at = Carbon::now();
        });

        self::deleting(function ($model) {
            $model->deleted_by = Auth::id();
            $model->deleted_at = Carbon::now();
        });

    }
}
