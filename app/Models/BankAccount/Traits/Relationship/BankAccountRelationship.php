<?php

namespace App\Models\BankAccount\Traits\Relationship;


use App\Models\Bank\Bank;
use App\Models\Branch\Branch;

/**
 * Trait BankRelationship
 *
 * @package App\Models\Bank\Traits\Relationship
 */
trait BankAccountRelationship
{
    /**
     * @return mixed
     */
    public function branch()
    {
        return $this->belongsTo(Branch::class, 'branch_id', 'id');
    }

    /**
     * @return mixed
     */
    public function bank()
    {
        return $this->belongsTo(Bank::class, 'bank_id', 'id');
    }
}
