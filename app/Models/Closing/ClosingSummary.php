<?php

namespace App\Models\Closing;

use App\Models\Closing\Traits\Attribute\ClosingSummaryAttribute;
use App\Models\Closing\Traits\Event\ClosingSummaryEvent;
use App\Models\Closing\Traits\Relationship\ClosingSummaryRelationship;
use App\Models\Closing\Traits\Scope\ClosingSummaryScope;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;


/**
 * Class ClosingSummary
 *
 * @package App\Models\Closing
 */
final class ClosingSummary extends Model
{
    use ClosingSummaryEvent,
        ClosingSummaryAttribute,
        SoftDeletes,
        ClosingSummaryRelationship,
        ClosingSummaryScope;

    /**
     * @var string
     */
    protected $table = 'setup_closing_summary';

    /**
     * @var array
     */
    protected $guarded = [
        'id',
        'updated_at',
        '_token',
        '_method'
    ];

    /**
     * @var array
     */
    protected $dates = [
        'deleted_at',
        'requisition_at',
        'transfer_at',
        'adjustment_at',
    ];

//    protected $primaryKey = 'sl';
//    public $keyType = "string";

    /**
     * @param  string  $eventName
     * @return string
     */
    public function getDescriptionForEvent(string $eventName): string
    {
        return 'Setup: Closing Balance: '.$this->sl." is {$eventName}";
    }
}
