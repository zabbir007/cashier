<?php

namespace App\Models\Closing\Traits\Relationship;

use App\Models\Closing\ClosingSummary;


/**
 * Class UserRelationship.
 */
trait ClosingDetailsRelationship
{

    public function summary()
    {
        return $this->hasOne(ClosingSummary::class, 'sl', 'transfer_sl');
    }
}
