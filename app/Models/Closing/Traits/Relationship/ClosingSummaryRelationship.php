<?php

namespace App\Models\Closing\Traits\Relationship;


use App\Models\Closing\ClosingDetails;


/**
 * Class UserRelationship.
 */
trait ClosingSummaryRelationship
{

    public function details()
    {
        return $this->hasMany(ClosingDetails::class, 'trans_sl', 'sl');
    }

}
