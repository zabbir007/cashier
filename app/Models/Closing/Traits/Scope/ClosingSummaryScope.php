<?php

namespace App\Models\Closing\Traits\Scope;

/**
 * Class UserScope.
 */
trait ClosingSummaryScope
{
    /**
     * @param        $query
     * @param  bool  $status
     *
     * @return mixed
     */
    public function scopeActive($query, $status = true)
    {
        return $query->where('status', $status);
    }
}
