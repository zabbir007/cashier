<?php

namespace App\Models\Closing;

use App\Models\Closing\Traits\Attribute\ClosingDetailsAttribute;
use App\Models\Closing\Traits\Event\ClosingDetailsEvent;
use App\Models\Closing\Traits\Relationship\ClosingDetailsRelationship;
use App\Models\Closing\Traits\Scope\ClosingDetailsScope;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;


/**
 * Class ClosingDetails
 *
 * @package App\Models\Closing
 */
final class ClosingDetails extends Model
{
    use ClosingDetailsScope,
        ClosingDetailsAttribute,
        SoftDeletes,
        ClosingDetailsRelationship,
        ClosingDetailsEvent;

    /**
     * @var string
     */
    protected $table = 'setup_closing_details';
    /**
     * @var array
     */
    protected $guarded = [
        'id',
        'updated_at',
        '_token',
        '_method'
    ];
    /**
     * @var array
     */
    protected $dates = [
        'deleted_at',
    ];

    /**
     * @param  string  $eventName
     * @return string
     */
    public function getDescriptionForEvent(string $eventName): string
    {
        return 'Setup: Closing Balance: '.$this->trans_sl." is {$eventName}";
    }
}
