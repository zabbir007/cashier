<?php

namespace App\Models\Access\User\Traits\Relationship;

use App\Models\Access\User\PasswordHistory;
use App\Models\Access\User\SocialLogin;
use App\Models\Branch\Branch;
use App\Models\System\Session;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;

/**
 * Class UserRelationship.
 */
trait UserRelationship
{
    /**
     * Many-to-Many relations with Role.
     *
     * @return BelongsToMany
     */
    public function roles()
    {
        return $this->belongsToMany(config('access.role'), config('access.role_user_table'), 'user_id', 'role_id');
    }

    /**
     * @return mixed
     */
    public function sessions()
    {
        return $this->hasMany(Session::class);
    }

    /**
     * @return mixed
     */
    public function branches()
    {
        return $this->belongsToMany(Branch::class, 'user_branch', 'user_id', 'branch_id')->select([
            'branches.id', 'branches.name', 'branches.short_name'
        ]);
    }

    /**
     * @return mixed
     */
    public function passwordHistories()
    {
        return $this->hasMany(PasswordHistory::class);
    }

//    public function getBranchListAttribute(){
//        return array_map('intval', $this->branches()->pluck('id')->toArray());
//    }
}
