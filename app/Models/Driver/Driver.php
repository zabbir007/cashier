<?php

namespace App\Models\Driver;

use App\Models\Driver\Traits\Attribute\DriverAttribute;
use App\Models\Driver\Traits\Event\DriverEvent;
use App\Models\Driver\Traits\Scope\DriverScope;
use App\Models\BaseModel;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Driver
 *
 * @package App\Models\Driver
 */
final class Driver extends BaseModel
{
    use DriverScope,
        SoftDeletes,
        DriverAttribute,
        DriverEvent;

    /**
     * @var string
     */
    protected static $logName = 'activity';
    /**
     * @var array
     */
    protected $guarded = [
        'id',
        'updated_at',
        '_token',
        '_method'
    ];
    /**
     * @var string
     */
    protected $table = 'drivers';

    /**
     * @param  string  $eventName
     * @return string
     */
    public function getDescriptionForEvent(string $eventName): string
    {
        return 'Setup: Driver: '.$this->name." is {$eventName}";
    }

}

