<?php

namespace App\Models\Driver\Traits\Scope;

/**
 * Class UserScope.
 */
trait DriverScope
{
    /**
     * @param        $query
     * @param  bool  $status
     *
     * @return mixed
     */
    public function scopeActive($query, $status = true)
    {
        return $query->where('status', $status);
    }

    /**
     * @param $query
     * @return mixed
     */
    public function scopeDriver($query)
    {
        $banks = access()->user()->banks;
        $bank_code = array_flatten($banks->pluck('code')->toArray());

        return $query->whereIn('bank_id', $bank_code);
    }
}
