<?php

namespace App\Models\Driver\Traits\Event;

use Carbon\Carbon;

/**
 * Class DriverEvent.
 */
trait DriverEvent
{
    /**
     *
     */
    public static function boot()
    {
        parent::boot();

        self::creating(function ($model) {
            $model->created_at = Carbon::now();
            $model->created_by = access()->user()->id;

        });

        self::updating(function ($model) {
            $model->updated_at = Carbon::now();
            $model->updated_by = access()->user()->id;
        });

        self::deleting(function ($model) {
            $model->deleted_at = Carbon::now();
            $model->deleted_by = access()->user()->id;
        });

    }
}
