<?php

namespace App\Models\History\Traits\Relationship;

use App\Models\Access\User\User;
use App\Models\History\HistoryType;
use Illuminate\Database\Eloquent\Relations\HasOne;

/**
 * Class HistoryRelationship.
 */
trait HistoryRelationship
{
    /**
     * @return HasOne
     */
    public function user()
    {
        return $this->hasOne(User::class, 'id', 'user_id');
    }

    /**
     * @return HasOne
     */
    public function type()
    {
        return $this->hasOne(HistoryType::class, 'id', 'type_id');
    }
}
