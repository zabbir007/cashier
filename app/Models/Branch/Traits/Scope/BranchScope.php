<?php

namespace App\Models\Branch\Traits\Scope;

/**
 * Class UserScope.
 */
trait BranchScope
{
    /**
     * @param        $query
     * @param  bool  $status
     *
     * @return mixed
     */
    public function scopeActive($query, $status = true)
    {
        return $query->where('status', $status);
    }

    /**
     * @param $query
     * @return mixed
     */
    public function scopeBranch($query)
    {
        $branches = access()->user()->branch;
        $branch_code = array_flatten($branches->pluck('code')->toArray());

        return $query->whereIn('branch_id', $branch_code);
    }
}
