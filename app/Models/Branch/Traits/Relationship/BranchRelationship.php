<?php

namespace App\Models\Branch\Traits\Relationship;

use App\Models\Bank\Bank;
use App\Models\Product\Product;


/**
 * Class BranchRelationship.
 */
trait BranchRelationship
{
    /**
     * @return mixed
     */
    public function banks()
    {
        return $this->belongsToMany(Bank::class, 'branch_bank', 'branch_id', 'bank_id');
    }

    /**
     * @return mixed
     */
    public function products()
    {
        return $this->belongsToMany(Product::class, 'product_branch', 'branch_id', 'product_id');
    }
//    public function getBankListAttribute(){
//        return array_map('intval', $this->banks()->pluck('id')->toArray());
//    }
}
