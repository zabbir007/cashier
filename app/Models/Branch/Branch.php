<?php

namespace App\Models\Branch;

use App\Models\BaseModel;
use App\Models\Branch\Traits\Attribute\BranchAttribute;
use App\Models\Branch\Traits\Event\BranchEvent;
use App\Models\Branch\Traits\Relationship\BranchRelationship;
use App\Models\Branch\Traits\Scope\BranchScope;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Branch
 *
 * @package App\Models\Branch
 */
final class Branch extends BaseModel
{
    use BranchScope,
        SoftDeletes,
        BranchAttribute,
        BranchRelationship,
        BranchEvent;

    /**
     * @var array
     */
    protected $guarded = [
        'id',
        'updated_at',
        '_token',
        '_method'
    ];

    /**
     * @var string
     */
    protected $table = 'branches';

    /**
     * @param  string  $eventName
     * @return string
     */
    public function getDescriptionForEvent(string $eventName): string
    {
        return 'Setup: Branch: '.$this->name." is {$eventName}";
    }
}

