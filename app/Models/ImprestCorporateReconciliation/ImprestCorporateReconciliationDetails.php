<?php

namespace App\Models\ImprestCorporateReconciliation;

use App\Models\BaseModel;
use App\Models\ImprestCorporateReconciliation\Traits\Attribute\ImprestCorporateReconciliationDetailsAttribute;
use App\Models\ImprestCorporateReconciliation\Traits\Event\ImprestCorporateReconciliationDetailsEvent;
use App\Models\ImprestCorporateReconciliation\Traits\Relationship\ImprestCorporateReconciliationDetailsRelationship;
use App\Models\ImprestCorporateReconciliation\Traits\Scope\ImprestCorporateReconciliationDetailsScope;
use Illuminate\Database\Eloquent\SoftDeletes;


/**
 * Class ImprestCorporateReconciliationDetails
 *
 * @package App\Models\ImprestCorporateReconciliation
 */
final class ImprestCorporateReconciliationDetails extends BaseModel
{
    use ImprestCorporateReconciliationDetailsScope,
        ImprestCorporateReconciliationDetailsAttribute,
        SoftDeletes,
        ImprestCorporateReconciliationDetailsRelationship,
        ImprestCorporateReconciliationDetailsEvent;

    /**
     * @var string
     */
    protected $table = 'trns_impr_cor_details';
    /**
     * @var array
     */
    protected $guarded = [
        'id',
        'updated_at',
        '_token',
        '_method'
    ];
    /**
     * @var array
     */
    protected $dates = [
        'deleted_at',
    ];

    /**
     * @param  string  $eventName
     * @return string
     */
    public function getDescriptionForEvent(string $eventName): string
    {
        return 'Transaction: Imprest Corporate Reconciliation: '.$this->trans_sl." is {$eventName}";
    }
}
