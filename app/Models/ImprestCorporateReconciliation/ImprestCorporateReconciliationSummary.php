<?php

namespace App\Models\ImprestCorporateReconciliation;

use App\Models\BaseModel;
use App\Models\ImprestCorporateReconciliation\Traits\Attribute\ImprestCorporateReconciliationSummaryAttribute;
use App\Models\ImprestCorporateReconciliation\Traits\Event\ImprestCorporateReconciliationSummaryEvent;
use App\Models\ImprestCorporateReconciliation\Traits\Relationship\ImprestCorporateReconciliationSummaryRelationship;
use App\Models\ImprestCorporateReconciliation\Traits\Scope\ImprestCorporateReconciliationSummaryScope;
use App\Models\TransactionMethod;
use Illuminate\Database\Eloquent\SoftDeletes;


/**
 * Class ImprestCorporateReconciliationSummary
 *
 * @package App\Models\ImprestCorporateReconciliation
 */
final class ImprestCorporateReconciliationSummary extends BaseModel
{
    use ImprestCorporateReconciliationSummaryEvent,
        ImprestCorporateReconciliationSummaryAttribute,
        SoftDeletes,
        ImprestCorporateReconciliationSummaryRelationship,
        ImprestCorporateReconciliationSummaryScope,
        TransactionMethod;

    /**
     * @var string
     */
    protected $table = 'trns_impr_cor_summary';

    /**
     * @var array
     */
    protected $guarded = [
        'id',
        'updated_at',
        '_token',
        '_method'
    ];

    /**
     * @var array
     */
    protected $dates = [
        'deleted_at',
        'requisition_at',
        'transfer_at',
        'adjustment_at',
    ];

    /**
     * @param  string  $eventName
     * @return string
     */
    public function getDescriptionForEvent(string $eventName): string
    {
        return 'Transaction: Imprest Corporate Reconciliation: '.$this->sl." is {$eventName}";
    }

}
