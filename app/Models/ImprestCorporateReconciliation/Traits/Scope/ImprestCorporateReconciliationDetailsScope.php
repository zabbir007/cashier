<?php

namespace App\Models\ImprestCorporateReconciliation\Traits\Scope;


/**
 * Trait ImprestCorporateReconciliationDetailsScope
 *
 * @package App\Models\ImprestCorporateReconciliation\Traits\Scope
 */
trait ImprestCorporateReconciliationDetailsScope
{
    /**
     * @param        $query
     * @param  bool  $status
     *
     * @return mixed
     */
    public function scopeActive($query, $status = true)
    {
        return $query->where('status', $status);
    }
}
