<?php

namespace App\Models\ImprestCorporateReconciliation\Traits\Relationship;

use App\Models\ImprestCorporateReconciliation\OutstandingReconciliationSummary;


/**
 * Trait ImprestCorporateReconciliationDetailsRelationship
 *
 * @package App\Models\ImprestCorporateReconciliation\Traits\Relationship
 */
trait ImprestCorporateReconciliationDetailsRelationship
{

    /**
     * @return mixed
     */
    public function summary()
    {
        return $this->hasOne(OutstandingReconciliationSummary::class, 'sl', 'transfer_sl');
    }
}
