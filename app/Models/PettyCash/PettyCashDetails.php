<?php

namespace App\Models\PettyCash;

use App\Models\BaseModel;
use App\Models\PettyCash\Traits\Attribute\PettyCashDetailsAttribute;
use App\Models\PettyCash\Traits\Event\PettyCashDetailsEvent;
use App\Models\PettyCash\Traits\Relationship\PettyCashDetailsRelationship;
use App\Models\PettyCash\Traits\Scope\PettyCashDetailsScope;
use Illuminate\Database\Eloquent\SoftDeletes;


/**
 * Class PettyCashDetails
 *
 * @package App\Models\PettyCash
 */
final class PettyCashDetails extends BaseModel
{
    use PettyCashDetailsScope,
        PettyCashDetailsAttribute,
        SoftDeletes,
        PettyCashDetailsRelationship,
        PettyCashDetailsEvent;

    /**
     * @var string
     */
    protected $table = 'transactions_petty_cash_details';
    /**
     * @var array
     */
    protected $guarded = [
        'id',
        'updated_at',
        '_token',
        '_method'
    ];
    /**
     * @var array
     */
    protected $dates = [
        'deleted_at',
    ];

    /**
     * @param  string  $eventName
     * @return string
     */
    public function getDescriptionForEvent(string $eventName): string
    {
        return 'Transaction: Petty Cash: '.$this->trans_sl." is {$eventName}";
    }
}
