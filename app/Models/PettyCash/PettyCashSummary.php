<?php

namespace App\Models\PettyCash;

use App\Models\BaseModel;
use App\Models\PettyCash\Traits\Attribute\PettyCashSummaryAttribute;
use App\Models\PettyCash\Traits\Event\PettyCashSummaryEvent;
use App\Models\PettyCash\Traits\Relationship\PettyCashSummaryRelationship;
use App\Models\PettyCash\Traits\Scope\PettyCashSummaryScope;
use App\Models\TransactionMethod;
use Illuminate\Database\Eloquent\SoftDeletes;


/**
 * Class PettyCashSummary
 *
 * @package App\Models\PettyCash
 */
final class PettyCashSummary extends BaseModel
{
    use PettyCashSummaryEvent,
        PettyCashSummaryAttribute,
        SoftDeletes,
        PettyCashSummaryRelationship,
        PettyCashSummaryScope,
        TransactionMethod;

    /**
     * @var string
     */
    protected $table = 'transactions_petty_cash_summary';

    /**
     * @var array
     */
    protected $guarded = [
        'id',
        'updated_at',
        '_token',
        '_method'
    ];

    /**
     * @var array
     */
    protected $dates = [
        'deleted_at'
    ];

//    protected $primaryKey = 'sl';
//    public $keyType = "string";

    /**
     * @param  string  $eventName
     * @return string
     */
    public function getDescriptionForEvent(string $eventName): string
    {
        return 'Transaction: Petty Cash: '.$this->sl." is {$eventName}";
    }
}
