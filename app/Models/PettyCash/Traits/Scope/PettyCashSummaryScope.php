<?php

namespace App\Models\PettyCash\Traits\Scope;

use function request;

/**
 * Class UserScope.
 */
trait PettyCashSummaryScope
{
    /**
     * @param        $query
     * @param  bool  $status
     *
     * @return mixed
     */
    public function scopeActive($query, $status = true)
    {
        return $query->where('status', $status);
    }

    /**
     * @param $query
     * @return mixed
     */
    public function scopeBranch($query)
    {
        return $query->whereIn('branch_id', request()->user()->branches->pluck('id'));
    }
}
