<?php

namespace App\Models\PettyCash\Traits\Scope;


/**
 * Trait PettyCashDetailsScope
 *
 * @package App\Models\PettyCash\Traits\Scope
 */
trait PettyCashDetailsScope
{
    /**
     * @param        $query
     * @param  bool  $status
     *
     * @return mixed
     */
    public function scopeActive($query, $status = true)
    {
        return $query->where('status', $status);
    }
}
