<?php

namespace App\Models\PettyCash\Traits\Relationship;

use App\Models\PettyCash\PettyCashSummary;


/**
 * Trait PettyCashDetailsRelationship
 *
 * @package App\Models\PettyCash\Traits\Relationship
 */
trait PettyCashDetailsRelationship
{

    /**
     * @return mixed
     */
    public function summary()
    {
        return $this->hasOne(PettyCashSummary::class, 'sl', 'transfer_sl');
    }
}
