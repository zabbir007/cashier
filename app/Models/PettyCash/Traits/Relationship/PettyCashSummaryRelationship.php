<?php

namespace App\Models\PettyCash\Traits\Relationship;


use App\Models\Access\User\User;
use App\Models\PettyCash\PettyCashDetails;

/**
 * Trait PettyCashSummaryRelationship
 *
 * @package App\Models\PettyCash\Traits\Relationship
 */
trait PettyCashSummaryRelationship
{

    /**
     * @return mixed
     */
    public function details()
    {
        return $this->hasMany(PettyCashDetails::class, 'trans_sl', 'sl');
    }

    /**
     * @return mixed
     */
    public function createBy()
    {
        return $this->hasOne(User::class, 'id', 'created_by');
    }

    /**
     * @return mixed
     */
    public function approveBy()
    {
        return $this->hasOne(User::class, 'id', 'created_by');
    }
}
