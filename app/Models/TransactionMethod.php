<?php

namespace App\Models;

use App\Exceptions\GeneralException;
use App\Repositories\Backend\System\TransactionSettingRepository;

/**
 * Trait CompanyMethod
 *
 * @package App\Models\Master\Traits\Method
 */
trait TransactionMethod
{
    /**
     * @return bool
     * @throws GeneralException
     */
    public function hasPermission()
    {
        $settingsRepo = new TransactionSettingRepository();

        return $settingsRepo->checkIfAllowed($this->trans_date, $this->branch_id);
    }
}
