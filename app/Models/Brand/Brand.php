<?php

namespace App\Models\Brand;

use App\Models\BaseModel;
use App\Models\Brand\Traits\Attribute\BrandAttribute;
use App\Models\Brand\Traits\Event\BrandEvent;
use App\Models\Brand\Traits\Relationship\BrandRelationship;
use App\Models\Brand\Traits\Scope\BrandScope;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Brand
 *
 * @package App\Models\Brand
 */
final class Brand extends BaseModel
{
    use BrandScope,
        SoftDeletes,
        BrandAttribute,
        BrandRelationship,
        BrandEvent;

    /**
     * @var array
     */
    protected $guarded = [
        'id',
        'updated_at',
        '_token',
        '_method'
    ];

    /**
     * @var string
     */
    protected $table = 'brands';

    /**
     * @param  string  $eventName
     * @return string
     */
    public function getDescriptionForEvent(string $eventName): string
    {
        return 'Setup: Brand: '.$this->name." is {$eventName}";
    }
}

