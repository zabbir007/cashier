<?php

namespace App\Models\Brand\Traits\Scope;

/**
 * Class UserScope.
 */
trait BrandScope
{
    /**
     * @param        $query
     * @param  bool  $status
     *
     * @return mixed
     */
    public function scopeActive($query, $status = true)
    {
        return $query->where('status', $status);
    }


}
