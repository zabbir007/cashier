<?php

namespace App\Models\System;

use App\Models\Access\User\User;
use App\Models\BaseModel;
use App\Models\Branch\Branch;
use Carbon\Carbon;

/**
 * Class TransactionSetting
 *
 * @package App\Models\System
 */
final class TransactionSetting extends BaseModel
{

    /**
     * @var string
     */
    protected $table = 'system_transaction_settings';

    /**
     * @var array
     */
    protected $guarded = [
        'id',
        'updated_at',
        '_token',
        '_method'
    ];

    /**
     * @var array
     */
    protected $dates = [
        'deleted_at',
        'expire_at',
        'from_date',
        'to_date'
    ];


    /**
     * @return mixed
     */
    public function user()
    {
        return $this->hasOne(User::class, 'id', 'user_id')->withDefault();
    }

    /**
     * @return mixed
     */
    public function branch()
    {
        return $this->hasOne(Branch::class, 'id', 'branch_id')->withDefault();
    }


    /**
     * @return bool
     */
    public function isPrevious()
    {
        return $this->previous == 1;
    }

    /**
     * @return string
     */
    public function getExpireLabelAttribute()
    {
        if (!$this->isExpired()) {
            return "<label class='badge badge-success'>Active</label>";
        }

        return "<label class='badge badge-danger'>Expired</label>";
    }

    /**
     * @return bool
     */
    public function isExpired()
    {
        return Carbon::parse($this->expire_at)->isPast();
    }

    /**
     * @return string
     */
    public function getEditButtonAttribute()
    {
        if (access()->allow('button-edit')) {
            return '<a href="'.route('admin.system.transaction_settings.edit',
                    $this).'" class=""><i class="si si-note" data-toggle="tooltip" data-placement="top" title="'.trans('buttons.general.crud.edit').'"></i></a> ';
        }

        return null;
    }

    /**
     * @return mixed|string
     */
    public function getActionButtonsAttribute()
    {
        return
            $this->edit_button;
    }
}
