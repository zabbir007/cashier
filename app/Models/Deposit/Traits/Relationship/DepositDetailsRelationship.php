<?php

namespace App\Models\Deposit\Traits\Relationship;

use App\Models\Deposit\DepositSummary;


/**
 * Class UserRelationship.
 */
trait DepositDetailsRelationship
{

    public function summary()
    {
        return $this->hasOne(DepositSummary::class, 'sl', 'transfer_sl');
    }
}
