<?php

namespace App\Models\Deposit\Traits\Relationship;

use App\Models\Access\User\User;
use App\Models\Deposit\DepositDetails;


/**
 * Trait DepositSummaryRelationship
 *
 * @package App\Models\Deposit\Traits\Relationship
 */
trait DepositSummaryRelationship
{

    /**
     * @return mixed
     */
    public function details()
    {
        return $this->hasMany(DepositDetails::class, 'trans_sl', 'sl');
    }

    /**
     * @return mixed
     */
    public function createBy()
    {
        return $this->hasOne(User::class, 'id', 'created_by');
    }

    /**
     * @return mixed
     */
    public function approveBy()
    {
        return $this->hasOne(User::class, 'id', 'created_by');
    }
}
