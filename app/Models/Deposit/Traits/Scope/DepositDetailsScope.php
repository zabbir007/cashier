<?php

namespace App\Models\Deposit\Traits\Scope;

/**
 * Class UserScope.
 */
trait DepositDetailsScope
{
    /**
     * @param        $query
     * @param  bool  $status
     *
     * @return mixed
     */
    public function scopeActive($query, $status = true)
    {
        return $query->where('status', $status);
    }
}
