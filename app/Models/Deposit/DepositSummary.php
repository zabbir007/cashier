<?php

namespace App\Models\Deposit;

use App\Models\BaseModel;
use App\Models\Deposit\Traits\Attribute\DepositSummaryAttribute;
use App\Models\Deposit\Traits\Event\DepositSummaryEvent;
use App\Models\Deposit\Traits\Relationship\DepositSummaryRelationship;
use App\Models\Deposit\Traits\Scope\DepositSummaryScope;
use App\Models\TransactionMethod;
use Illuminate\Database\Eloquent\SoftDeletes;


/**
 * Class DepositSummary
 *
 * @package App\Models\Deposit
 */
final class DepositSummary extends BaseModel
{
    use DepositSummaryEvent,
        DepositSummaryAttribute,
        SoftDeletes,
        DepositSummaryRelationship,
        DepositSummaryScope,
        TransactionMethod;

    /**
     * @var string
     */
    protected $table = 'transactions_deposit_summary';

    /**
     * @var array
     */
    protected $guarded = [
        'id',
        'updated_at',
        '_token',
        '_method'
    ];

    /**
     * @var array
     */
    protected $dates = [
        'deleted_at'
    ];

    /**
     * @param  string  $eventName
     * @return string
     */
    public function getDescriptionForEvent(string $eventName): string
    {
        return 'Transaction: Deposit: '.$this->sl." is {$eventName}";
    }
}
