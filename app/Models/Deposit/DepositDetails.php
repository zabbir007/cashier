<?php

namespace App\Models\Deposit;

use App\Models\BaseModel;
use App\Models\Deposit\Traits\Attribute\DepositDetailsAttribute;
use App\Models\Deposit\Traits\Event\DepositDetailsEvent;
use App\Models\Deposit\Traits\Relationship\DepositDetailsRelationship;
use App\Models\Deposit\Traits\Scope\DepositDetailsScope;
use Illuminate\Database\Eloquent\SoftDeletes;


/**
 * Class DepositDetails
 *
 * @package App\Models\Deposit
 */
final class DepositDetails extends BaseModel
{
    use DepositDetailsScope,
        DepositDetailsAttribute,
        SoftDeletes,
        DepositDetailsRelationship,
        DepositDetailsEvent;

    /**
     * @var string
     */
    protected $table = 'transactions_deposit_details';
    /**
     * @var array
     */
    protected $guarded = [
        'id',
        'updated_at',
        '_token',
        '_method'
    ];
    /**
     * @var array
     */
    protected $dates = [
        'deleted_at',
    ];

    /**
     * @param  string  $eventName
     * @return string
     */
    public function getDescriptionForEvent(string $eventName): string
    {
        return 'Transaction: Deposit: '.$this->trans_sl." is {$eventName}";
    }
}
