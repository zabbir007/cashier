<?php

namespace App\Models\OutstandingReconciliation\Traits\Scope;

/**
 * Class UserScope.
 */
trait OutstandingReconciliationDetailsScope
{
    /**
     * @param        $query
     * @param  bool  $status
     *
     * @return mixed
     */
    public function scopeActive($query, $status = true)
    {
        return $query->where('status', $status);
    }
}
