<?php

namespace App\Models\OutstandingReconciliation\Traits\Event;

use Carbon\Carbon;

/**
 * Class UserRelationship.
 */
trait OutstandingReconciliationSummaryEvent
{
    public static function boot()
    {
        parent::boot();

        self::creating(function ($model) {
            $model->created_by = access()->user()->id;
            $model->created_at = Carbon::now();

        });

        self::updating(function ($model) {
            $model->updated_by = access()->user()->id;
            // $model->updated_by_name = $user->username;
            $model->updated_at = Carbon::now();
        });

        self::deleting(function ($model) {
            $model->deleted_by = access()->user()->id;
            //$model->deleted_by_name = $user->username;
            $model->deleted_at = Carbon::now();
        });
    }
}
