<?php

namespace App\Models\OutstandingReconciliation\Traits\Relationship;

use App\Models\Access\User\User;
use App\Models\OutstandingReconciliation\OutstandingReconciliationDetails;

/**
 * Trait OutstandingReconciliationSummaryRelationship
 *
 * @package App\Models\OutstandingReconciliation\Traits\Relationship
 */
trait OutstandingReconciliationSummaryRelationship
{

    /**
     * @return mixed
     */
    public function details()
    {
        return $this->hasMany(OutstandingReconciliationDetails::class, 'trans_sl', 'sl');
    }

    /**
     * @return mixed
     */
    public function createBy()
    {
        return $this->hasOne(User::class, 'id', 'created_by');
    }

    /**
     * @return mixed
     */
    public function approveBy()
    {
        return $this->hasOne(User::class, 'id', 'created_by');
    }
}
