<?php

namespace App\Models\OutstandingReconciliation\Traits\Relationship;

use App\Models\OutstandingReconciliation\OutstandingReconciliationSummary;


/**
 * Trait OutstandingReconciliationDetailsRelationship
 *
 * @package App\Models\OutstandingReconciliation\Traits\Relationship
 */
trait OutstandingReconciliationDetailsRelationship
{

    /**
     * @return mixed
     */
    public function summary()
    {
        return $this->hasOne(OutstandingReconciliationSummary::class, 'sl', 'transfer_sl');
    }
}
