<?php

namespace App\Models\OutstandingReconciliation\Traits\Attribute;


/**
 * Trait OutstandingReconciliationDetailsAttribute
 *
 * @package App\Models\OutstandingReconciliation\Traits\Attribute
 */
trait OutstandingReconciliationDetailsAttribute
{

    /**
     * @return string
     */
    public function getStatusLabelAttribute()
    {
        if ($this->isActive()) {
            return "<label class='label label-success'>".$this->statuses->name.'</label>';
        }

        return "<label class='label label-danger'>".$this->statuses->name.'</label>';
    }

    /**
     * @return bool
     */
    public function isActive()
    {
        return $this->status == 1;
    }

    /**
     * @return string
     */
    function getYearMonthAttribute()
    {
        return $this->year.$this->month;
    }

    /**
     * @return string
     */
    public function getShowButtonAttribute()
    {
        return '<a href="'.route('admin.setup.transaction.invoice_terms.show',
                $this).'" class=""><i class="cus-application-view-detail" data-toggle="tooltip" data-placement="top" title="'.trans('buttons.general.crud.view').'"></i></a> ';
    }

    /**
     * @return string
     */
    public function getEditButtonAttribute()
    {
        return '<a href="'.route('admin.setup.transaction.invoice_terms.edit',
                $this).'" class=""><i class="cus-application-edit" data-toggle="tooltip" data-placement="top" title="'.trans('buttons.general.crud.edit').'"></i></a> ';
    }

    /**
     * @return string
     */
    public function getStatusButtonAttribute()
    {
        switch ($this->status) {
            case 2:
                return '<a href="'.route('admin.setup.transaction.invoice_terms.mark', [
                        $this,
                        1,
                    ]).'" class=""><i class="cus-control-play" data-toggle="tooltip" data-placement="top" title="'.trans('buttons.backend.access.users.activate').'"></i></a> ';
            // No break

            case 1:
                return '<a href="'.route('admin.setup.transaction.invoice_terms.mark', [
                        $this,
                        2,
                    ]).'" class=""><i class="cus-control-pause" data-toggle="tooltip" data-placement="top" title="'.trans('buttons.backend.access.users.deactivate').'"></i></a> ';
            // No break

            default:
                return '';
            // No break
        }
    }

    /**
     * @return string
     */
    public function getDeleteButtonAttribute()
    {
        return '<a href="'.route("admin.setup.transaction.invoice_terms.destroy", $this).'"
                 data-method="delete"
                 data-trans-button-cancel="'.trans('buttons.general.cancel').'"
                 data-trans-button-confirm="'.trans('buttons.general.crud.delete').'"
                 data-trans-title="'.trans('strings.backend.general.are_you_sure').'"
                 class=""><i class="cus-bin-closed" data-toggle="tooltip" data-placement="top" title="'.trans('buttons.general.crud.delete').'"></i></a> ';
    }

    /**
     * @return string
     */
    public function getRestoreButtonAttribute()
    {
        return '<a href="'.route("admin.setup.transaction.invoice_terms.restore",
                $this).'" name="restore_user" class=""><i class="cus-arrow-refresh" data-toggle="tooltip" data-placement="top" title="'.trans('buttons.general.crud.restore').'"></i></a> ';
    }

    /**
     * @return string
     */
    public function getDeletePermanentlyButtonAttribute()
    {
        return '<a href="'.route("admin.transaction.invoice_terms.delete-permanently",
                $this).'" name="delete_user_perm" class=""><i class="cus-bin-closed" data-toggle="tooltip" data-placement="top" title="'.trans('buttons.general.crud.delete_permanently').'"></i></a> ';
    }

    /**
     * @return string
     */
    public function getActionButtonsAttribute()
    {
        if ($this->trashed()) {
            return $this->restore_button.$this->delete_permanently_button;
        }

        return
            //  $this->show_button.
            // $this->edit_button.
            // $this->status_button.
            $this->delete_button;
    }
}
