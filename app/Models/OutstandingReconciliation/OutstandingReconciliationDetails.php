<?php

namespace App\Models\OutstandingReconciliation;

use App\Models\BaseModel;
use App\Models\OutstandingReconciliation\Traits\Attribute\OutstandingReconciliationDetailsAttribute;
use App\Models\OutstandingReconciliation\Traits\Event\OutstandingReconciliationDetailsEvent;
use App\Models\OutstandingReconciliation\Traits\Relationship\OutstandingReconciliationDetailsRelationship;
use App\Models\OutstandingReconciliation\Traits\Scope\OutstandingReconciliationDetailsScope;
use Illuminate\Database\Eloquent\SoftDeletes;


/**
 * Class OutstandingReconciliationDetails
 *
 * @package App\Models\OutstandingReconciliation
 */
final class OutstandingReconciliationDetails extends BaseModel
{
    use OutstandingReconciliationDetailsScope,
        OutstandingReconciliationDetailsAttribute,
        SoftDeletes,
        OutstandingReconciliationDetailsRelationship,
        OutstandingReconciliationDetailsEvent;

    /**
     * @var string
     */
    protected $table = 'transactions_os_reconciliation_details';
    /**
     * @var array
     */
    protected $guarded = [
        'id',
        'updated_at',
        '_token',
        '_method'
    ];
    /**
     * @var array
     */
    protected $dates = [
        'deleted_at',
    ];

    /**
     * @param  string  $eventName
     * @return string
     */
    public function getDescriptionForEvent(string $eventName): string
    {
        return 'Setup: Outstanding Reconciliation: '.$this->trans_sl." is {$eventName}";
    }
}
