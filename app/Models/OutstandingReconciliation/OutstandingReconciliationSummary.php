<?php

namespace App\Models\OutstandingReconciliation;

use App\Models\BaseModel;
use App\Models\OutstandingReconciliation\Traits\Attribute\OutstandingReconciliationSummaryAttribute;
use App\Models\OutstandingReconciliation\Traits\Event\OutstandingReconciliationSummaryEvent;
use App\Models\OutstandingReconciliation\Traits\Relationship\OutstandingReconciliationSummaryRelationship;
use App\Models\OutstandingReconciliation\Traits\Scope\OutstandingReconciliationSummaryScope;
use App\Models\TransactionMethod;
use Illuminate\Database\Eloquent\SoftDeletes;


/**
 * Class OutstandingReconciliationSummary
 *
 * @package App\Models\OutstandingReconciliation
 */
final class OutstandingReconciliationSummary extends BaseModel
{
    use OutstandingReconciliationSummaryEvent,
        OutstandingReconciliationSummaryAttribute,
        SoftDeletes,
        OutstandingReconciliationSummaryRelationship,
        OutstandingReconciliationSummaryScope,
        TransactionMethod;

    /**
     * @var string
     */
    protected $table = 'transactions_os_reconciliation_summary';

    /**
     * @var array
     */
    protected $guarded = [
        'id',
        'updated_at',
        '_token',
        '_method'
    ];

    /**
     * @var array
     */
    protected $dates = [
        'deleted_at',
        'requisition_at',
        'transfer_at',
        'adjustment_at',
    ];

//    protected $primaryKey = 'sl';
//    public $keyType = "string";

    /**
     * @param  string  $eventName
     * @return string
     */
    public function getDescriptionForEvent(string $eventName): string
    {
        return 'Setup: Outstanding Reconciliation: '.$this->sl." is {$eventName}";
    }
}
