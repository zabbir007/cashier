<?php

namespace App\Models\BankCharge;

use App\Models\BankCharge\Traits\Attribute\BankChargeSummaryAttribute;
use App\Models\BankCharge\Traits\Event\BankChargeSummaryEvent;
use App\Models\BankCharge\Traits\Relationship\BankChargeSummaryRelationship;
use App\Models\BankCharge\Traits\Scope\BankChargeSummaryScope;
use App\Models\BaseModel;
use App\Models\TransactionMethod;
use Illuminate\Database\Eloquent\SoftDeletes;


/**
 * Class BankChargeSummary
 *
 * @package App\Models\BankCharge
 */
final class BankChargeSummary extends BaseModel
{
    use BankChargeSummaryEvent,
        BankChargeSummaryAttribute,
        SoftDeletes,
        BankChargeSummaryRelationship,
        BankChargeSummaryScope,
        TransactionMethod;

    /**
     * @var string
     */
    protected $table = 'transactions_bank_charge_summary';

    /**
     * @var array
     */
    protected $guarded = [
        'id',
        'updated_at',
        '_token',
        '_method'
    ];

    /**
     * @var array
     */
    protected $dates = [
        'deleted_at',
        'requisition_at',
        'transfer_at',
        'adjustment_at',
    ];

//    protected $primaryKey = 'sl';
//    public $keyType = "string";

    /**
     * @param  string  $eventName
     * @return string
     */
    public function getDescriptionForEvent(string $eventName): string
    {
        return 'Transaction: Bank Charge: '.$this->sl." is {$eventName}";
    }
}
