<?php

namespace App\Models\BankCharge\Traits\Attribute;

/**
 * Trait BankChargeSummaryAttribute
 *
 * @package App\Models\BankCharge\Traits\Attribute
 */
trait BankChargeSummaryAttribute
{
    /**
     * @return string
     */
    public function getStatusLabelAttribute()
    {
        if ($this->isActive()) {
            return "<label class='label label-success'>".'Active'.'</label>';
        }

        return "<label class='label label-danger'>".'InActive'.'</label>';
    }

    /**
     * @return bool
     */
    public function isActive()
    {
        return $this->status == 1;
    }

    /**
     * @return string
     */
    function getYearMonthAttribute()
    {
        return $this->year.$this->month;
    }

    /**
     * @return string
     */
    public function getShowButtonAttribute()
    {
        if (access()->allow('menu-transaction-bank-charge-show')) {
            return '<a href="'.route('admin.transaction.bankcharges.show',
                    $this).'" class=""><i class="si si-eye" data-toggle="tooltip" data-placement="top" title="'.trans('buttons.general.crud.view').'"></i></a> ';
        } else {
            return false;
        }
    }

    /**
     * @return string
     */
    public function getEditButtonAttribute()
    {
        if ($this->hasPermission()) {
            if (access()->allow('menu-transaction-bank-charge-edit')) {
                return '<a href="'.route('admin.transaction.bankcharges.edit',
                        $this).'" class=""><i class="si si-note" data-toggle="tooltip" data-placement="top" title="'.trans('buttons.general.crud.edit').'"></i></a> ';
            }
        }

        return null;
    }

    /**
     * @return string
     */
    public function getApproveButtonAttribute()
    {
        if (access()->allow('menu-transaction-bank-charge-approve')) {
            return '<a href="'.route('admin.transaction.bankcharges.approve',
                    $this).'" class="primary"><i class="si si-check" data-toggle="tooltip" data-placement="top" title="'.trans('Approve').'"></i></a> ';
        } else {
            return null;
        }
    }

    /**
     * @return string
     */
    public function getStatusButtonAttribute()
    {
        switch ($this->status) {
            case 2:
                return '<a href="'.route('admin.transaction.bankcharges.mark', [
                        $this,
                        1,
                    ]).'" class=""><i class="si si-control-play" data-toggle="tooltip" data-placement="top" title="'.trans('buttons.backend.access.users.activate').'"></i></a> ';
            // No break

            case 1:
                return '<a href="'.route('admin.transaction.bankcharges.mark', [
                        $this,
                        2,
                    ]).'" class=""><i class="si si-control-pause" data-toggle="tooltip" data-placement="top" title="'.trans('buttons.backend.access.users.deactivate').'"></i></a> ';
            // No break

            default:
                return '';
            // No break
        }
    }

    /**
     * @return string
     */
    public function getDeleteButtonAttribute()
    {
        if (access()->allow('menu-transaction-bank-charge-delete')) {
            return '<a href="'.route("admin.transaction.bankcharges.destroy", $this).'"
                 data-method="delete"
                 data-trans-button-cancel="'.trans('buttons.general.cancel').'"
                 data-trans-button-confirm="'.trans('buttons.general.crud.delete').'"
                 data-trans-title="'.trans('strings.backend.general.are_you_sure').'"
                 class=""><i class="si si-trash" data-toggle="tooltip" data-placement="top" title="'.trans('buttons.general.crud.delete').'"></i></a> ';
        } else {
            return null;
        }
    }

    /**
     * @return string
     */
    public function getRestoreButtonAttribute()
    {
        if (access()->allow('menu-transaction-bank-charge-delete')) {
            return '<a href="'.route("admin.transaction.bankcharges.restore",
                    $this).'" name="restore_user" class=""><i class="si si-refresh" data-toggle="tooltip" data-placement="top" title="'.trans('buttons.general.crud.restore').'"></i></a> ';
        } else {
            return null;
        }
    }

    /**
     * @return string
     */
    public function getDeletePermanentlyButtonAttribute()
    {
        return '<a href="'.route("menu-transaction-bank-charge-delete",
                $this).'" name="delete_user_perm" class=""><i class="si si-close" data-toggle="tooltip" data-placement="top" title="'.trans('buttons.general.crud.delete_permanently').'"></i></a> ';
    }

    /**
     * @return string
     */


    public function getActionButtonsAttribute()
    {
        if ($this->trashed()) {
            return $this->restore_button.$this->delete_permanently_button;
        }

        if ($this->isApprove()) {
            return $this->show_button;
        }

        return
            $this->show_button.
            $this->edit_button.
            $this->approve_button.
            // $this->status_button.
            $this->delete_button;
    }

    /**
     * @return bool
     */

    public function isApprove()
    {
        return $this->approve == 1;
    }
}

