<?php

namespace App\Models\BankCharge\Traits\Relationship;


use App\Models\Access\User\User;
use App\Models\BankCharge\BankChargeDetails;


/**
 * Class UserRelationship.
 */
trait BankChargeSummaryRelationship
{

    public function details()
    {
        return $this->hasMany(BankChargeDetails::class, 'trans_sl', 'sl');
    }

    public function createBy()
    {
        return $this->hasOne(User::class, 'id', 'created_by');
    }

    public function approveBy()
    {
        return $this->hasOne(User::class, 'id', 'created_by');
    }
}
