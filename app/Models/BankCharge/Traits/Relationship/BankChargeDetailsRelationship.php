<?php

namespace App\Models\BankCharge\Traits\Relationship;

use App\Models\BankCharge\BankChargeSummary;


/**
 * Class UserRelationship.
 */
trait BankChargeDetailsRelationship
{

    public function summary()
    {
        return $this->hasOne(BankChargeSummary::class, 'sl', 'transfer_sl');
    }
}
