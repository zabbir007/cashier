<?php

namespace App\Models\BankCharge\Traits\Scope;

/**
 * Class UserScope.
 */
trait BankChargeDetailsScope
{
    /**
     * @param        $query
     * @param  bool  $status
     *
     * @return mixed
     */
    public function scopeActive($query, $status = true)
    {
        return $query->where('status', $status);
    }
}
