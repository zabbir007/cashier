<?php

namespace App\Models\BankCharge\Traits\Scope;

/**
 * Class UserScope.
 */
trait BankChargeSummaryScope
{
    /**
     * @param        $query
     * @param  bool  $status
     *
     * @return mixed
     */
    public function scopeActive($query, $status = true)
    {
        return $query->where('status', $status);
    }

    /**
     * @param        $query
     * @param  bool  $status
     *
     * @return mixed
     */
    public function scopeBranch($query)
    {
        return $query->whereIn('branch_id', request()->user()->branches->pluck('id'));
    }
}
