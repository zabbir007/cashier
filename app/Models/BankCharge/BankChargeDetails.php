<?php

namespace App\Models\BankCharge;

use App\Models\BankCharge\Traits\Attribute\BankChargeDetailsAttribute;
use App\Models\BankCharge\Traits\Event\BankChargeDetailsEvent;
use App\Models\BankCharge\Traits\Relationship\BankChargeDetailsRelationship;
use App\Models\BankCharge\Traits\Scope\BankChargeDetailsScope;
use App\Models\BaseModel;
use Illuminate\Database\Eloquent\SoftDeletes;


/**
 * Class BankChargeDetails
 *
 * @package App\Models\BankCharge
 */
final class BankChargeDetails extends BaseModel
{
    use BankChargeDetailsScope,
        BankChargeDetailsAttribute,
        SoftDeletes,
        BankChargeDetailsRelationship,
        BankChargeDetailsEvent;

    /**
     * @var string
     */
    protected $table = 'transactions_bank_charge_details';
    /**
     * @var array
     */
    protected $guarded = [
        'id',
        'updated_at',
        '_token',
        '_method'
    ];
    /**
     * @var array
     */
    protected $dates = [
        'deleted_at',
    ];

    /**
     * @param  string  $eventName
     * @return string
     */
    public function getDescriptionForEvent(string $eventName): string
    {
        return 'Transaction: Bank Charge: '.$this->trans_sl." is {$eventName}";
    }
}
