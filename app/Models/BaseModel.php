<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;

/**
 * Class BaseModel
 *
 * @package App\Models
 */
class BaseModel extends Model
{
    use LogsActivity;

    /**
     * @var bool
     */
    protected static $logUnguarded = true;

    /**
     * @var array
     */
    protected static $logAttributes = ['*'];
    /**
     * @var bool
     */
    protected static $logOnlyDirty = true;
    protected static $logAttributesToIgnore = [
        'created_by', 'created_at', 'updated_by', 'updated_at', 'deleted_by', 'deleted_at'
    ];

    /**
     * @return string
     */
    public static function getTableName()
    {
        return with(new static)->getTable();
    }
}
