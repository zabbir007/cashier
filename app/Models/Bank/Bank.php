<?php

namespace App\Models\Bank;

use App\Models\Bank\Traits\Attribute\BankAttribute;
use App\Models\Bank\Traits\Event\BankEvent;
use App\Models\Bank\Traits\Relationship\BankRelationship;
use App\Models\Bank\Traits\Scope\BankScope;
use App\Models\BaseModel;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Bank
 *
 * @package App\Models\Bank
 */
final class Bank extends BaseModel
{
    use BankScope,
        SoftDeletes,
        BankAttribute,
        BankRelationship,
        BankEvent;

    /**
     * @var string
     */
    protected static $logName = 'activity';
    /**
     * @var array
     */
    protected $guarded = [
        'id',
        'updated_at',
        '_token',
        '_method'
    ];
    /**
     * @var string
     */
    protected $table = 'banks';

    /**
     * @param  string  $eventName
     * @return string
     */
    public function getDescriptionForEvent(string $eventName): string
    {
        return 'Setup: Bank: '.$this->name." is {$eventName}";
    }

}

