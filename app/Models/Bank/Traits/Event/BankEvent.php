<?php

namespace App\Models\Bank\Traits\Event;

use Carbon\Carbon;

/**
 * Class BankEvent.
 */
trait BankEvent
{
    /**
     *
     */
    public static function boot()
    {
        parent::boot();

        self::creating(function ($model) {
            $model->created_at = Carbon::now();

        });

        self::updating(function ($model) {
            $model->updated_at = Carbon::now();
        });

        self::deleting(function ($model) {
            $model->deleted_at = Carbon::now();
        });

    }
}
