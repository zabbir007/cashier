<?php

namespace App\Models\Bank\Traits\Relationship;


use App\Models\Branch\Branch;

/**
 * Trait BankRelationship
 *
 * @package App\Models\Bank\Traits\Relationship
 */
trait BankRelationship
{
    /**
     * @return mixed
     */
    public function branch()
    {
        return $this->belongsToMany(Branch::class, 'branch_bank', 'bank_id', 'branch_id');
    }
}
