<?php

namespace App\Models\Bank\Traits\Scope;

/**
 * Class UserScope.
 */
trait BankScope
{
    /**
     * @param        $query
     * @param  bool  $status
     *
     * @return mixed
     */
    public function scopeActive($query, $status = true)
    {
        return $query->where('status', $status);
    }

    /**
     * @param $query
     * @return mixed
     */
    public function scopeBank($query)
    {
        $banks = access()->user()->banks;
        $bank_code = array_flatten($banks->pluck('code')->toArray());

        return $query->whereIn('bank_id', $bank_code);
    }
}
