<?php

namespace App\Models\DateWise;

use App\Models\BaseModel;
use App\Models\DateWise\Traits\Attribute\DateWiseDetailsAttribute;
use App\Models\DateWise\Traits\Event\DateWiseDetailsEvent;
use App\Models\DateWise\Traits\Relationship\DateWiseDetailsRelationship;
use App\Models\DateWise\Traits\Scope\DateWiseDetailsScope;
use Illuminate\Database\Eloquent\SoftDeletes;


/**
 * Class DateWiseDetails
 *
 * @package App\Models\DateWise
 */
final class DateWiseDetails extends BaseModel
{
    use DateWiseDetailsScope,
        DateWiseDetailsAttribute,
        SoftDeletes,
        DateWiseDetailsRelationship,
        DateWiseDetailsEvent;

    /**
     * @var string
     */
    protected $table = 'transactions_date_wise_details';
    /**
     * @var array
     */
    protected $guarded = [
        'id',
        'updated_at',
        '_token',
        '_method'
    ];
    /**
     * @var array
     */
    protected $dates = [
        'deleted_at',
    ];

    /**
     * @param  string  $eventName
     * @return string
     */
    public function getDescriptionForEvent(string $eventName): string
    {
        return 'Transaction: Datewise Receipt: '.$this->trans_sl." is {$eventName}";
    }
}
