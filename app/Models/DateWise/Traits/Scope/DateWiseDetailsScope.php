<?php

namespace App\Models\DateWise\Traits\Scope;

/**
 * Class UserScope.
 */
trait DateWiseDetailsScope
{
    /**
     * @param        $query
     * @param  bool  $status
     *
     * @return mixed
     */
    public function scopeActive($query, $status = true)
    {
        return $query->where('status', $status);
    }

    /**
     * @param $query
     * @param $products
     * @return mixed
     */
    public function scopeProducts($query, $products)
    {
        return $query->whereIn('product_id', $products);
    }
}
