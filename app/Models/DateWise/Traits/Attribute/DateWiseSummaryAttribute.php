<?php

namespace App\Models\DateWise\Traits\Attribute;

/**
 * Class DateWiseSummaryAttribute.
 */
trait DateWiseSummaryAttribute
{
    /**
     * @return mixed
     */
    public function getTotalMoneyReceiptAttribute()
    {
        return $this->details->sum('money_receipt');
    }
    /**
     * @return mixed
     */
    public function getTotalMrReverseAttribute()
    {
        return $this->details->sum('mr_reverse');
    }

    /**
     * @return mixed
     */
    public function getTotalCollectionAttribute()
    {
        return $this->details->sum('collection');
    }

    /**
     * @return mixed
     */
    public function getTotalAdvanceReceiptAttribute()
    {
        return $this->details->sum('adv_receipt');
    }

    /**
     * @return mixed
     */
    public function getTotalShortReceiptAttribute()
    {
        return $this->details->sum('short_receipt');
    }

    /**
     * @return mixed
     */
    public function getTotalAdjPositiveAttribute()
    {
        return $this->details->sum('adj_plus');
    }

    /**
     * @return mixed
     */
    public function getTotalAdvanceAdjAttribute()
    {
        return $this->details->sum('adv_adj');
    }

    /**
     * @return mixed
     */
    public function getTotalAdjNegativeAttribute()
    {
        return $this->details->sum('adj_minus');
    }

    /**
     * @return string
     */
    public function getStatusLabelAttribute()
    {
        if ($this->isActive()) {
            return "<label class='label label-success'>"."Active".'</label>';
        }

        return "<label class='label label-danger'>"."InActive".'</label>';
    }

    /**
     * @return bool
     */
    public function isActive()
    {
        return $this->status == 1;
    }

    /**
     * @return string
     */
    function getYearMonthAttribute()
    {
        return $this->year.$this->month;
    }

    /**
     * @return string
     */
    public function getShowButtonAttribute()
    {
        if (access()->allow('menu-transaction-date-wise-receipt-show')) {
            return '<a href="'.route('admin.transaction.datewises.show',
                    $this).'" class=""><i class="si si-eye" data-toggle="tooltip" data-placement="top" title="'.trans('buttons.general.crud.view').'"></i></a> ';
        } else {
            return false;
        }
    }

    /**
     * @return string
     */
    public function getEditButtonAttribute()
    {
        if ($this->hasPermission() && access()->allow('menu-transaction-date-wise-receipt-edit')) {
            return '<a href="'.route('admin.transaction.datewises.edit',
                    $this).'" class=""><i class="si si-note" data-toggle="tooltip" data-placement="top" title="'.trans('buttons.general.crud.edit').'"></i></a> ';
        }

        return null;
    }

    /**
     * @return string
     */
    public function getApproveButtonAttribute()
    {
        if (access()->allow('menu-transaction-date-wise-receipt-approve')) {
            return '<a href="'.route("admin.transaction.datewises.approve",
                    $this).'" class="primary"><i class="si si-check" data-toggle="tooltip" data-placement="top" title="'.trans('Approve').'"></i></a> ';
        } else {
            return null;
        }
    }

    /**
     * @return string
     */
    public function getStatusButtonAttribute()
    {
        switch ($this->status) {
            case 2:
                return '<a href="'.route('admin.transaction.datewises.mark', [
                        $this,
                        1,
                    ]).'" class=""><i class="si si-control-play" data-toggle="tooltip" data-placement="top" title="'.trans('buttons.backend.access.users.activate').'"></i></a> ';
            // No break

            case 1:
                return '<a href="'.route('admin.transaction.datewises.mark', [
                        $this,
                        2,
                    ]).'" class=""><i class="si si-control-pause" data-toggle="tooltip" data-placement="top" title="'.trans('buttons.backend.access.users.deactivate').'"></i></a> ';
            // No break

            default:
                return '';
            // No break
        }
    }

    /**
     * @return string
     */
    public function getDeleteButtonAttribute()
    {
        if (access()->allow('menu-transaction-date-wise-receipt-delete')) {
            return '<a href="'.route("admin.transaction.datewises.destroy", $this).'"
                 data-method="delete"
                 data-trans-button-cancel="'.trans('buttons.general.cancel').'"
                 data-trans-button-confirm="'.trans('buttons.general.crud.delete').'"
                 data-trans-title="'.trans('strings.backend.general.are_you_sure').'"
                 class=""><i class="si si-trash" data-toggle="tooltip" data-placement="top" title="'.trans('buttons.general.crud.delete').'"></i></a> ';
        } else {
            return null;
        }
    }

    /**
     * @return string
     */
    public function getRestoreButtonAttribute()
    {
        if (access()->allow('menu-transaction-date-wise-receipt-delete')) {
            return '<a href="'.route("admin.transaction.datewises.restore",
                    $this).'" name="restore_user" class=""><i class="si si-refresh" data-toggle="tooltip" data-placement="top" title="'.trans('buttons.general.crud.restore').'"></i></a> ';
        } else {
            return null;
        }
    }

    /**
     * @return string
     */
    public function getDeletePermanentlyButtonAttribute()
    {
        return '<a href="'.route("admin.transaction.stock_adjustments.delete-permanently",
                $this).'" name="delete_user_perm" class=""><i class="si si-close" data-toggle="tooltip" data-placement="top" title="'.trans('buttons.general.crud.delete_permanently').'"></i></a> ';
    }

    /**
     * @return string
     */


    public function getActionButtonsAttribute()
    {
        if ($this->trashed()) {
            return $this->restore_button.$this->delete_permanently_button;
        }

        if ($this->isApprove()) {
            return
                $this->show_button;

        } else {
            return
                $this->show_button.
                $this->edit_button.
                $this->approve_button.
                // $this->status_button.
                $this->delete_button;
        }
    }

    /**
     * @return bool
     */

    public function isApprove()
    {
        return $this->approve == 1;
    }
}
