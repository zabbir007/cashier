<?php

namespace App\Models\DateWise\Traits\Relationship;

use App\Models\DateWise\DateWiseSummary;


/**
 * Class UserRelationship.
 */
trait DateWiseDetailsRelationship
{

    public function summary()
    {
        return $this->hasOne(DateWiseSummary::class, 'sl', 'transfer_sl');
    }
}
