<?php

namespace App\Models\DateWise\Traits\Relationship;

use App\Models\Access\User\User;
use App\Models\DateWise\DateWiseDetails;


/**
 * Trait DateWiseSummaryRelationship
 *
 * @package App\Models\DateWise\Traits\Relationship
 */
trait DateWiseSummaryRelationship
{

    /**
     * @return mixed
     */
    public function details()
    {
        return $this->hasMany(DateWiseDetails::class, 'trans_sl', 'sl');
    }
    
    /**
     * @return mixed
     */
    public function createBy()
    {
        return $this->hasOne(User::class, 'id', 'created_by');
    }

    /**
     * @return mixed
     */
    public function approveBy()
    {
        return $this->hasOne(User::class, 'id', 'created_by');
    }
}
