<?php

namespace App\Models\DateWise;

use App\Models\BaseModel;
use App\Models\DateWise\Traits\Attribute\DateWiseSummaryAttribute;
use App\Models\DateWise\Traits\Event\DateWiseSummaryEvent;
use App\Models\DateWise\Traits\Relationship\DateWiseSummaryRelationship;
use App\Models\DateWise\Traits\Scope\DateWiseSummaryScope;
use App\Models\TransactionMethod;
use Illuminate\Database\Eloquent\SoftDeletes;


/**
 * Class DateWiseSummary
 *
 * @package App\Models\DateWise
 */
final class DateWiseSummary extends BaseModel
{
    use DateWiseSummaryEvent,
        DateWiseSummaryAttribute,
        SoftDeletes,
        DateWiseSummaryRelationship,
        DateWiseSummaryScope,
        TransactionMethod;

    /**
     * @var string
     */
    protected $table = 'transactions_date_wise_summary';

    /**
     * @var array
     */
    protected $guarded = [
        'id',
        'updated_at',
        '_token',
        '_method'
    ];

    /**
     * @var array
     */
    protected $dates = [
        'deleted_at',
    ];

//    protected $primaryKey = 'sl';
//    public $keyType = "string";

    /**
     * @param  string  $eventName
     * @return string
     */
    public function getDescriptionForEvent(string $eventName): string
    {
        return 'Transaction: Datewise Receipt: '.$this->sl." is {$eventName}";
    }
}
