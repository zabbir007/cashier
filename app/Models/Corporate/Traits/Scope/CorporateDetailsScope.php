<?php

namespace App\Models\Corporate\Traits\Scope;

/**
 * Class UserScope.
 */
trait CorporateDetailsScope
{
    /**
     * @param        $query
     * @param  bool  $status
     *
     * @return mixed
     */
    public function scopeActive($query, $status = true)
    {
        return $query->where('status', $status);
    }
}
