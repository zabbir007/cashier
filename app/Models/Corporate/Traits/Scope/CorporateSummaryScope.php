<?php

namespace App\Models\Corporate\Traits\Scope;

/**
 * Class UserScope.
 */
trait CorporateSummaryScope
{
    /**
     * @param        $query
     * @param  bool  $status
     *
     * @return mixed
     */
    public function scopeActive($query, $status = true)
    {
        return $query->where('status', $status);
    }
}
