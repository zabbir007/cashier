<?php

namespace App\Models\Corporate\Traits\Relationship;

use App\Models\Corporate\CorporateSummary;


/**
 * Class UserRelationship.
 */
trait CorporateDetailsRelationship
{

    public function summary()
    {
        return $this->hasOne(CorporateSummary::class, 'sl', 'trans_sl');
    }
}
