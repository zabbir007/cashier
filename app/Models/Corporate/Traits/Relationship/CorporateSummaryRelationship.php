<?php

namespace App\Models\Corporate\Traits\Relationship;

use App\Models\Corporate\CorporateDetails;

/**
 * Trait CorporateSummaryRelationship
 *
 * @package App\Models\Corporate\Traits\Relationship
 */
trait CorporateSummaryRelationship
{

    /**
     * @return mixed
     */
    public function details()
    {
        return $this->hasMany(CorporateDetails::class, 'trans_sl', 'sl');
    }


}
