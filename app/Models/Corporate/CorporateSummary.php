<?php

namespace App\Models\Corporate;

use App\Models\BaseModel;
use App\Models\Corporate\Traits\Attribute\CorporateSummaryAttribute;
use App\Models\Corporate\Traits\Event\CorporateSummaryEvent;
use App\Models\Corporate\Traits\Relationship\CorporateSummaryRelationship;
use App\Models\Corporate\Traits\Scope\CorporateSummaryScope;
use Illuminate\Database\Eloquent\SoftDeletes;


/**
 * Class OpeningSummary
 *
 * @package App\Models\Corporate
 */
final class CorporateSummary extends BaseModel
{
    use CorporateSummaryEvent,
        CorporateSummaryAttribute,
        SoftDeletes,
        CorporateSummaryRelationship,
        CorporateSummaryScope;

    /**
     * @var string
     */
    protected $table = 'setup_corporate_summary';

    /**
     * @var array
     */
    protected $guarded = [
        'id',
        'updated_at',
        '_token',
        '_method'
    ];

    /**
     * @var array
     */
    protected $dates = [
        'deleted_at',
        'requisition_at',
        'transfer_at',
        'adjustment_at',
    ];

//    protected $primaryKey = 'sl';
//    public $keyType = "string";

    /**
     * @param  string  $eventName
     * @return string
     */
    public function getDescriptionForEvent(string $eventName): string
    {
        return 'Setup: Corporate Balance: '.$this->sl." is {$eventName}";
    }
}
