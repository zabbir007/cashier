<?php

namespace App\Models\Corporate;

use App\Models\BaseModel;
use App\Models\Corporate\Traits\Attribute\CorporateDetailsAttribute;
use App\Models\Corporate\Traits\Event\CorporateDetailsEvent;
use App\Models\Corporate\Traits\Relationship\CorporateDetailsRelationship;
use App\Models\Corporate\Traits\Scope\CorporateDetailsScope;
use Illuminate\Database\Eloquent\SoftDeletes;


/**
 * Class OpeningDetails
 *
 * @package App\Models\Corporate
 */
final class CorporateDetails extends BaseModel
{
    use CorporateDetailsScope,
        CorporateDetailsAttribute,
        SoftDeletes,
        CorporateDetailsRelationship,
        CorporateDetailsEvent;

    /**
     * @var string
     */
    protected $table = 'setup_corporate_details';
    /**
     * @var array
     */
    protected $guarded = [
        'id',
        'updated_at',
        '_token',
        '_method'
    ];
    /**
     * @var array
     */
    protected $dates = [
        'deleted_at',
    ];

    /**
     * @param  string  $eventName
     * @return string
     */
    public function getDescriptionForEvent(string $eventName): string
    {
        return 'Setup: Corporate Balance: '.$this->trans_sl." is {$eventName}";
    }
}
