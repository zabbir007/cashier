<?php

namespace App\Exceptions;

use Exception;


/**
 * Class RecordExistsException
 *
 * @package App\Exceptions
 */
final class RecordExistsException extends Exception
{
}
