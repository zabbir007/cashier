<?php

namespace App\Imports;


use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\ToCollection;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Carbon;

class ExcelImport implements ToCollection
{
    public function __construct($selectedBranchCode, $selectedBranchId, $selectedBankId, $selectedAccount, $selectedYear, $selectedMonth, $selectedYearMonth, $created_by, $created_at, $trans_date)
    {
        $this->selectedBranchCode = $selectedBranchCode;
        $this->selectedBranchId = $selectedBranchId;
        $this->selectedBankId = $selectedBankId;
        $this->selectedAccount = $selectedAccount;
        $this->selectedYear = $selectedYear;
        $this->selectedMonth = $selectedMonth;
        $this->selectedYearMonth = $selectedYearMonth;
        $this->created_by = $created_by;
        $this->created_at = $created_at;
        $this->trans_date = $trans_date;


    }

    public function transformDate($value, $format = 'Y-m-d')
    {
        try {
            return \Carbon\Carbon::instance(\PhpOffice\PhpSpreadsheet\Shared\Date::excelToDateTimeObject($value));
        } catch (\ErrorException $e) {
            return \Carbon\Carbon::createFromFormat($format, $value);
        }
    }

    public function collection(Collection $collection)
    {
        foreach ($collection as $key => $value) {
            if ($key > 0) {

                if ($value[0] == 'b') {
                    $type_value = 'b';
                } else if ($value[0] == 'c') {
                    $type_value = 'c';
                } else if ($value[0] == 'f') {
                    $type_value = 'f';
                } else if ($value[0] == 'g') {
                    $type_value = 'g';
                } else if ($value[0] == 'h') {
                    $type_value = 'h';
                }else {
                    $type_value = $type_value;
                }
//                if ($value[1]==''){
//                    $value_1='0/0/0020';
//                }else{
//                    $value_1=$value[1];
//                }
//                if ($value[2]==''){
//                    $value_2='1';
//                }else{
//                    $value_2=$value[2];
//                }
//                if ($value[3]==''){
//                    $value_3='1';
//                }else{
//                    $value_3=$value[3];
//                }
//                if ($value[4]==''){
//                    $value_4='1';
//                }else{
//                    $value_4=$value[4];
//                }
//                if ($value[5]==''){
//                    $value_5='1';
//                }else{
//                    $value_5=$value[5];
//                }
//                if ($value[6]==''){
//                    $value_6='1';
//                }else{
//                    $value_6=$value[6];
//                }
//                exit();
//                echo $type_value;
//                  DB::table('accpac_excel')->insert(['date' => $this->transformDate($value[1]),'total_a'=>1]);
//                DB::table('trns_impr_cor_check_details')->insert(['entry_date' => $this->transformDate($value[1]), 'party_name' => $this->$value[2], 'cheque_no' => $this->$value[3], 'source_batch' => $this->$value[4], 'amount' => $this->$value[5], 'ref_no' => $this->$value[6], 'branch_id' => $this->selectedBranchId, 'branch_code' => $this->selectedBranchCode,'types' => $type_value, 'bank_id' => $this->selectedBankId, 'account_number' => $this->selectedAccount, 'trans_date' => $this->trans_date, 'year' => $this->selectedYear, 'month' => $this->selectedMonth, 'yearmonth' => $this->selectedYearMonth, 'created_by' => $this->created_by, 'created_at' => $this->created_at]);
                DB::table('trns_impr_cor_check_details')->insert(['entry_date' => $this->transformDate($value[1]), 'party_name' => $value[2], 'cheque_no' => $value[3], 'source_batch' => $value[4], 'amount' => $value[5], 'ref_no' => $value[6], 'branch_id' => $this->selectedBranchId, 'branch_code' => $this->selectedBranchCode, 'types' => $type_value, 'bank_id' => $this->selectedBankId, 'account_number' => $this->selectedAccount, 'trans_date' => $this->trans_date, 'year' => $this->selectedYear, 'month' => $this->selectedMonth, 'yearmonth' => $this->selectedYearMonth, 'created_by' => $this->created_by, 'created_at' => $this->created_at]);
            }

        }


    }

}


?>
