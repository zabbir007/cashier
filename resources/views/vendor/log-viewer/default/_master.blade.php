<!doctype html>
<html class="no-js" lang="{{ app()->getLocale() }}">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
    <link rel="shortcut icon" sizes="196x196" href="{{asset('../assets/images/logo.png')}}">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta name="description" content="LogViewer">
    <meta name="author" content="ARCANEDEV">
    <title>LogViewer - Created by ARCANEDEV</title>

    <title>@yield('title', app_name())</title>
    @yield('meta')

@yield('before-styles')

{{ Html::style(mix('css/app.css')) }}
@yield('after-styles')
    <style>
        html {
            position: relative;
            min-height: 100%;
        }

        body {
            font-size: .875rem;
            margin-bottom: 60px;
        }

        .main-footer {
            position: absolute;
            bottom: 0;
            width: 100%;
            height: 60px;
            line-height: 60px;
            background-color: #E8EAF6;
        }

        .main-footer p {
            margin-bottom: 0;
        }

        .main-footer .fa.fa-heart {
            color: #C62828;
        }

        .page-header {
            border-bottom: 1px solid #8a8a8a;
        }

        /*
         * Navbar
         */

        .navbar-brand {
            padding: .75rem 1rem;
            font-size: 1rem;
        }

        .navbar-nav .nav-link {
            padding-right: .5rem;
            padding-left: .5rem;
        }

        /*
         * Boxes
         */

        .box {
            display: block;
            padding: 0;
            min-height: 70px;
            background: #fff;
            width: 100%;
            box-shadow: 0 1px 1px rgba(0,0,0,0.1);
            border-radius: .25rem;
        }

        .box > .box-icon > i,
        .box .box-content .box-text,
        .box .box-content .box-number {
            color: #FFF;
            text-shadow: 0 1px 1px rgba(0, 0, 0, 0.3);
        }

        .box > .box-icon {
            border-radius: 2px 0 0 2px;
            display: block;
            float: left;
            height: 70px; width: 70px;
            text-align: center;
            font-size: 40px;
            line-height: 70px;
            background: rgba(0,0,0,0.2);
        }

        .box .box-content {
            padding: 5px 10px;
            margin-left: 70px;
        }

        .box .box-content .box-text {
            display: block;
            font-size: 1rem;
            white-space: nowrap;
            overflow: hidden;
            text-overflow: ellipsis;
            font-weight: 600;
        }

        .box .box-content .box-number {
            display: block;
        }

        .box .box-content .progress {
            background: rgba(0,0,0,0.2);
            margin: 5px -10px 5px -10px;
        }

        .box .box-content .progress .progress-bar {
            background-color: #FFF;
        }

        /*
         * Log Menu
         */

        .log-menu .list-group-item.disabled {
            cursor: not-allowed;
        }

        .log-menu .list-group-item.disabled .level-name {
            color: #D1D1D1;
        }

        /*
         * Log Entry
         */

        .stack-content {
            color: #AE0E0E;
            font-family: consolas, Menlo, Courier, monospace;
            white-space: pre-line;
            font-size: .8rem;
        }

        /*
         * Colors: Badge & Infobox
         */

        .badge.badge-env,
        .badge.badge-level-all,
        .badge.badge-level-emergency,
        .badge.badge-level-alert,
        .badge.badge-level-critical,
        .badge.badge-level-error,
        .badge.badge-level-warning,
        .badge.badge-level-notice,
        .badge.badge-level-info,
        .badge.badge-level-debug,
        .badge.empty {
            color: #FFF;
            text-shadow: 0 1px 1px rgba(0, 0, 0, 0.3);
        }

        .badge.badge-level-all,
        .box.level-all {
            background-color: {{ log_styler()->color('all') }};
        }

        .badge.badge-level-emergency,
        .box.level-emergency {
            background-color: {{ log_styler()->color('emergency') }};
        }

        .badge.badge-level-alert,
        .box.level-alert  {
            background-color: {{ log_styler()->color('alert') }};
        }

        .badge.badge-level-critical,
        .box.level-critical {
            background-color: {{ log_styler()->color('critical') }};
        }

        .badge.badge-level-error,
        .box.level-error {
            background-color: {{ log_styler()->color('error') }};
        }

        .badge.badge-level-warning,
        .box.level-warning {
            background-color: {{ log_styler()->color('warning') }};
        }

        .badge.badge-level-notice,
        .box.level-notice {
            background-color: {{ log_styler()->color('notice') }};
        }

        .badge.badge-level-info,
        .box.level-info {
            background-color: {{ log_styler()->color('info') }};
        }

        .badge.badge-level-debug,
        .box.level-debug {
            background-color: {{ log_styler()->color('debug') }};
        }

        .badge.empty,
        .box.empty {
            background-color: {{ log_styler()->color('empty') }};
        }

        .badge.badge-env {
            background-color: #6A1B9A;
        }
    </style>

    <!--[if lt IE 9]>
    {{ Html::script('https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js') }}
    {{ Html::script('https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js') }}
    <![endif]-->

    <!-- Scripts -->
    <script>
        window.Laravel = '{!! json_encode([
            'csrfToken' => csrf_token(),
        ]) !!}';
    </script>
</head>

<body class="menubar-left menubar-unfold menubar-light theme-primary">
<div id="app">
    <div id="page-container" class="sidebar-o side-scroll page-header-fixed sidebar-inverse">
        <div id="page-loader" class="show"></div>

        <nav class="navbar navbar-expand-md navbar-dark sticky-top bg-dark p-0">
            <a href="{{ route('log-viewer::dashboard') }}" class="navbar-brand mr-0">
                <i class="fa fa-fw fa-book"></i> LogViewer
            </a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav"
                    aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarNav">
                <ul class="navbar-nav mr-auto">
                    <li class="nav-item {{ Route::is('log-viewer::dashboard') ? 'active' : '' }}">
                        <a href="{{ route('log-viewer::dashboard') }}" class="nav-link">
                            <i class="fa fa-dashboard"></i> Dashboard
                        </a>
                    </li>
                    <li class="nav-item {{ Route::is('log-viewer::logs.list') ? 'active' : '' }}">
                        <a href="{{ route('log-viewer::logs.list') }}" class="nav-link">
                            <i class="fa fa-archive"></i> Logs
                        </a>
                    </li>
                </ul>
            </div>
        </nav>

        @include('backend.includes.sidebar.sidebar')

        @include('backend.includes.header')

        <main id="main-container">
            <!-- Page Content -->
            <div class="content">
                @include('includes.partials.messages')
                @include('includes.partials.logged-in-as')

                <div style="background: #fff;">
                    @yield('content')
                </div>
            </div>
            <!-- END Page Content -->
        </main>

        @include('backend.includes.footer')


        {{-- Footer --}}
        {{--<footer class="main-footer">--}}
            {{--<div class="container-fluid">--}}
                {{--<p class="text-muted pull-left">--}}
                    {{--LogViewer - <span class="badge badge-info">version {{ log_viewer()->version() }}</span>--}}
                {{--</p>--}}
                {{--<p class="text-muted pull-right">--}}
                    {{--Created with <i class="fa fa-heart"></i> by ARCANEDEV <sup>&copy;</sup>--}}
                {{--</p>--}}
            {{--</div>--}}
        {{--</footer>--}}

        @yield('modals')

    </div><!-- .wrap -->
</div>

@yield('modals')
@yield('before-scripts')
{{ Html::script(mix('js/manifest.js')) }}
{{ Html::script(mix('js/vendor.js')) }}
{{ Html::script(mix('js/app.js')) }}

@yield('after-scripts')

</body>
</html>
