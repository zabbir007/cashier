<!doctype html>
<html class="no-js" lang="{{ app()->getLocale() }}">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
    <link rel="shortcut icon" sizes="196x196" href="../assets/images/logo.png">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>@yield('title', app_name())</title>
    <meta name="description" content="@yield('meta_description', 'Employee Profile Management')">
    <meta name="author" content="@yield('meta_author', 'Md. Aftab Uddin')">
@yield('meta')
@yield('before-styles')
<!-- Check if the language is set to RTL, so apply the RTL layouts -->
    <!-- Otherwise apply the normal LTR layouts -->
    @langRTL
{{ Html::style(getRtlCss(mix('css/app.css'))) }}
@else
    {{ Html::style(mix('css/app.css')) }}
@endif
{{ Html::style("https://fonts.googleapis.com/css?family=Raleway:400,500,600,700,800,900,300") }}
@yield('after-styles')
<!-- Icons -->
    <!-- The following icons can be replaced with your own, they are used by desktop and mobile browsers -->
{{ Html::style("assets/img/favicons/favicon.png") }}
{{ Html::style("assets/img/favicons/favicon-192x192.png") }}
{{ Html::style("assets/img/favicons/apple-touch-icon-180x180.png") }}
<!-- END Icons -->

    <!-- Stylesheets -->
    <!-- Codebase framework -->
{{ Html::style("assets/css/codebase.min.css") }}

<!-- You can include a specific file from css/themes/ folder to alter the default color theme of the template. eg: -->
    <!-- <link rel="stylesheet" id="css-theme" href="assets/css/themes/flat.min.css"> -->
    <!-- END Stylesheets -->
    <!-- Html5 Shim and Respond.js IE8 support of Html5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    {{ Html::script('https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js') }}
    {{ Html::script('https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js') }}
    <![endif]-->

    <!-- Scripts -->
    <script>
        window.Laravel = <?php echo json_encode([
            'csrfToken' => csrf_token(),
        ]); ?>
    </script>


</head>

<body id="page-container" class="sidebar-o side-scroll page-header-fixed">
<!-- APP NAVBAR ==========-->
@include('backend.includes.nav')
<!--========== END app navbar -->

<!-- APP ASIDE ==========-->
@include('backend.includes.sidebar.sidebar')
<!-- APP ASIDE ==========-->

<!-- Header ==========-->
@include('backend.includes.header')
<!-- Header ==========-->
<!-- Main Container -->
<main id="main-container">
    <!-- Page Content -->
    <div class="content">
        @yield('content')
    </div>
    <!-- END Page Content -->
</main>
<!-- END Main Container -->

<!-- APP FOOTER -->
@include('backend.includes.footer')
<!-- /#app-footer -->
<!--========== END app main -->
{{--@role(1)--}}
{{--<!-- APP CUSTOMIZER -->--}}
{{--@include('backend.includes.customizer')--}}
{{--<!-- #app-customizer -->--}}
{{--@endauth--}}

<!-- SIDE PANEL -->
@include('backend.includes.sidebar.right_sidebar')
<!-- /#side-panel -->
</div><!-- .wrap -->

        <!-- JavaScripts -->
        @yield('before-scripts-end')
        {{ Html::script(mix('js/app.js')) }}
        {{ Html::script("assets/js/core/jquery.min.js") }}
        {{ Html::script("assets/js/core/popper.min.js") }}
        {{ Html::script("assets/js/core/bootstrap.min.js") }}
        {{ Html::script("assets/js/core/jquery.slimscroll.min.js") }}
        {{ Html::script("assets/js/core/jquery.scrollLock.min.js" ) }}
        {{ Html::script("assets/js/core/jquery.appear.min.js") }}
        {{ Html::script("assets/js/core/jquery.countTo.min.js") }}
        {{ Html::script("assets/js/core/js.cookie.min.js") }}
        {{ Html::script("assets/js/codebase.js") }}

        <!-- Page JS Plugins -->
        {{ Html::script("assets/js/plugins/chartjs/Chart.bundle.min.js") }}

        <!-- Page JS Code -->
        {{ Html::script("assets/js/pages/be_pages_dashboard.js") }}
        @yield('after-scripts-end')

        <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.11.1/moment-with-locales.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.3.0/Chart.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.15.35/js/bootstrap-datetimepicker.min.js"></script>
        <script>
            Chart.defaults.global.responsive      = true;
            Chart.defaults.global.scaleFontFamily = "'Source Sans Pro'";
            Chart.defaults.global.animationEasing = "easeOutQuart";
        </script>
        @yield('modals')
        @yield('scripts')
    </body>
</html>
