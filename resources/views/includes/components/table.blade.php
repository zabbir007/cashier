<div class="table-responsive">
    <table id="{{ $id }}" class="{{ $class }}" style="{{ isset($style) ? $style : '' }}" cellspacing="0" width="100%">
        <thead>
        @isset($thead)
            <tr class="{{ $tr_class ?? '' }}">
                {{ $thead }}
            </tr>
        @endisset
        </thead>
        @isset($tbody)
            <tbody>
            {{ $tbody }}
            </tbody>
        @endisset
    </table>
</div>
