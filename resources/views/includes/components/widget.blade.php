<div class="block">
    <header class="block-header">
        <h4 class="block-title">{{ $title }}</h4>
        <div class="block-option">{{ $action_buttons }}</div>
    </header><!-- .widget-header -->
    <hr class="m-0">
    <div class="block-content">
        {{ $slot }}
    </div><!-- .widget-body -->
</div><!-- .widget -->
