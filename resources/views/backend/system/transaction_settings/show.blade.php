@extends ('backend.layouts.app')

@section ('title', $page_heading)

@section('page-header')
    <h1>
        {{ $page_heading }}
    </h1>
@endsection

@section('content')
    <div class="row">
        <div class="col-md-12">
            @component('includes.components.widget')

                @slot('title')
                    {{ $page_heading }}
                @endslot

                @slot('action_buttons')
                    @include($module_view.'.header-buttons')
                @endslot

                @component('includes.components.table',['class'=>'table table-condensed table-hover table-striped','id'=>'users-table'])
                    @slot('tbody')
                        <tr>
                            <td><strong>Name</strong></td>
                            <td>{{$module_name_singular->name}}</td>
                        </tr>
                        <tr>
                            <td><strong>Code</strong></td>
                            <td>{{$module_name_singular->code}}</td>
                        </tr>
                        <tr>
                            <td><strong>Address 1</strong></td>
                            <td>{{$module_name_singular->address1}}</td>
                        </tr>
                        <tr>
                            <td><strong>Address 2</strong></td>
                            <td>{{$module_name_singular->address2}}</td>
                        </tr>
                        <tr>
                            <td><strong>Address 3</strong></td>
                            <td>{{$module_name_singular->address3}}</td>
                        </tr>
                        <tr>
                            <td><strong>Address 4</strong></td>
                            <td>{{$module_name_singular->address4}}</td>
                        </tr>
                        <tr>
                            <td><strong>Location</strong></td>
                            <td>{{$module_name_singular->location}}</td>
                        </tr>
                        <tr>
                            <td><strong>Status</strong></td>
                            <td>
                                <span class="badge badge-success">{!! $module_name_singular->status_label !!}</span>
                            </td>
                        </tr>
                    @endslot
                @endcomponent

            @endcomponent
        </div><!-- END column -->
    </div><!-- .row -->
@endsection