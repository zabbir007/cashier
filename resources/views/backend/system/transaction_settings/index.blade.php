@extends ('backend.layouts.app')

@section ('title', $page_heading)

@section('after-styles')
    <style type="text/css">
        table td, table tr {
            white-space: nowrap;
        }

        div.dataTables_filter {
            text-align: left !important;
        }

        .dataTables_wrapper .dataTables_processing {
            position: absolute;
            top: 50%;
            left: 50%;
            width: 300px;
            height: 60px;
            margin-left: -50%;
            margin-top: -25px;
            padding-top: 20px;
            text-align: center;
            font-size: 1.2em;
            border: 1px solid rgb(51, 51, 51) !important;
            background: #eeeeee linear-gradient(to right, rgba(255, 255, 255, 0) 0%, rgba(255, 255, 255, 0.9) 25%, rgba(255, 255, 255, 0.9) 75%, rgba(255, 255, 255, 0) 100%) !important;
        }
    </style>
@endsection

@section('content')
    <div class="row">
        <!-- DOM dataTable -->
        <div class="col-md-12">
            @component('includes.components.widget')

                @slot('title')
                    {{ $page_heading }}
                @endslot

                @slot('action_buttons')
                    @include($module_view.'.header-buttons')
                @endslot

                {{--Widget body start--}}
                @component('includes.components.table',['class'=>'table table-condensed table-hover table-striped table-bordered','id'=>'datatable'])
                    @slot('thead')
                        <th style="font-size: 14px">#</th>
                        <th style="font-size: 14px">Branch</th>
                        <th style="font-size: 14px">User</th>
                            <th style="font-size: 14px">From Date</th>
                            <th style="font-size: 14px">To Date</th>
                            <th style="font-size: 14px">Allowed Days For</th>
                        <th style="font-size: 14px">Expire At</th>
                            <th style="font-size: 14px">Activation Remain</th>
                        <th style="font-size: 14px">Activation Status</th>
                        <th>{{ __('labels.general.actions') }}</th>
                    @endslot

                        @slot('tbody')
                            @foreach($module_name_singular as $module)
                                <tr class="{{ $module->isExpired() ? 'bg-danger-light' : 'bg-success-light' }}">
                                    <td>{{ $loop->iteration }}</td>
                                    <td>{{ $module->branch->name }}</td>
                                    <td>{{ $module->user->full_name }}</td>
                                    <td>{{ $module->from_date != null ? $module->from_date->format('d M Y') : 'Not Set' }}</td>
                                    <td>{{ $module->to_date != null ? $module->to_date->format('d M Y') : 'Not Set' }}</td>
                                    <td>{{ ($module->interval == 0 ? 1 : $module->interval).' '.($module->day == 'd' ? 'Days' : 'Months')}}</td>
                                    <td>{{ $module->expire_at->format('d M Y h:i a') }}</td>
                                    <td>{{ !$module->expire_at->isPast() ? $module->expire_at->diffForHumans() : 'Expired' }}</td>
                                    <td>{!! $module->expire_label !!}</td>
                                    <td>{!! $module->action_buttons !!}</td>
                                </tr>
                            @endforeach
                        @endslot
                @endcomponent

            @endcomponent
        </div><!-- END column -->
    </div><!-- .row -->
@endsection

@section('after-scripts')
    <script type="text/javascript">
        $(function () {
            $('#datatable').DataTable({
                // dom: "<row  lfrtip",
                dom: "<'row'<'col-sm-12 col-md-4 text-left'f><'col-sm-12 col-md-4'><'col-sm-12 col-md-4 text-right'l>>" +
                    "<'row'<'col-sm-12'tr>>" +
                    "<'row'<'col-sm-12 col-md-5'i><'col-sm-12 col-md-7'p>>",
                language: {
                    'loadingRecords': '&nbsp;',
                    'processing': '<i class="fa fa-spinner fa-spin fa-2x fa-fw"></i>Loading...',
                },
                fixedHeader: true,
                pageLength: 25,
                // processing: true,
                // serverSide: true,
                autoWidth: false,
                {{--ajax: {--}}
                    {{--    url: '{{ route($module_route.'.get') }}',--}}
                    {{--    type: 'post',--}}
                    {{--    data: {status: 1, trashed: false},--}}
                    {{--    error: function (xhr, err) {--}}
                    {{--        if (err === 'parsererror')--}}
                    {{--            location.reload();--}}
                    {{--    }--}}
                    {{--},--}}
                    {{--columns: [--}}
                    {{--    {data: 'DT_RowIndex', searchable: false, sortable: false},--}}
                    {{--    {data: 'branch_name', name:'branch.name',searchable: true, sortable: true},--}}
                    {{--    {data: 'user_name', name:'user.last_name',searchable: true, sortable: true},--}}
                    {{--    {data: 'allowed_days', searchable: false, sortable: false},--}}
                    {{--    {data: 'expire_at', searchable: false, sortable: false},--}}
                    {{--    {data: 'expire_label', searchable: false, sortable: false},--}}
                    {{--    {data: 'actions', searchable: false, sortable: false}--}}
                    {{--],--}}

            });
        });
    </script>
@endsection
