<div class="row">
    <div class="col-sm-8">
        <div class="form-group form-row has-feedback mb-2">
            {!! Form::label('branch_name', 'Branch:',['class' => 'col-lg-4 col-form-label']) !!}
            <div class="col-lg-8">
                {!! Form::text('branch_name', $module_name_singular->branch->name,  ['class' => 'form-control','readonly']) !!}
                <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
                <div class="help-block with-errors"></div>
            </div>
        </div>
        <div class="form-group form-row has-feedback mb-2">
            {!! Form::label('name', 'Applied for User:',['class' => 'col-lg-4 col-form-label']) !!}
            <div class="col-lg-8">
                {!! Form::text('name', $module_name_singular->user->first_name . ' '. $module_name_singular->user->last_name, ['class' => 'form-control','placeholder' => '', 'readonly']) !!}
                <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
                <div class="help-block with-errors"></div>
            </div>
        </div>
        <div class="form-group form-row has-feedback mb-2">
            {!! Form::label('date', 'Allow Entry For:',['class' => 'col-lg-4 col-form-label']) !!}
            <div class="col-lg-8">
                <div class="input-group">
                    {!! Form::text('from_date', $module_name_singular->from_date == null ? now()->subDays(3)->format('d-M-Y') : $module_name_singular->from_date->format('d-M-Y'), ['class' => 'form-control datepicker','data-week-start'=>'6', 'data-autoclose'=>'true', 'data-today-highlight'=>'true', 'data-date-format'=>'dd-M-yyyy', 'placeholder'=>'mm/dd/yy']) !!}

                    <div class="input-group-append ml-3 mr-3">
                        <span class="input-group-text">
                            <i class="fa fa-arrow-right"></i>
                        </span>
                    </div>

                    {!! Form::text('to_date', $module_name_singular->to_date == null ? now()->format('d-M-Y') : $module_name_singular->to_date->format('d-M-Y'), ['class' => 'form-control datepicker','data-week-start'=>'6', 'data-autoclose'=>'true', 'data-today-highlight'=>'true', 'data-date-format'=>'dd-M-yyyy', 'placeholder'=>'mm/dd/yy']) !!}
                </div>
            </div>
        </div>

        {{--        <div class="form-group row has-feedback">--}}
        {{--            {!! Form::hidden('previous', true) !!}--}}
        {{--            {!! Form::label('interval', 'Allow Entry For',['class' => 'col-lg-4 col-form-label']) !!}--}}

        {{--            <div class="col-lg-2">--}}
        {{--                {!! Form::label('', 'Previous',['class' => 'col-form-label']) !!}--}}
        {{--            </div>--}}

        {{--            <div class="col-lg-3">--}}
        {{--                {!! Form::number('interval', old('interval') , ['class' => 'form-control','placeholder' => '2','required']) !!}--}}
        {{--            </div>--}}

        {{--            <div class="col-lg-3">--}}
        {{--                {!! Form::select('days', ['d' => 'Days', 'm' => 'Months'], old('days'),  ['class' => 'form-control']) !!}--}}
        {{--            </div>--}}

        {{--        </div>--}}

        <div class="form-group form-row has-feedback">
            {!! Form::label('date', 'Allow Release Upto:',['class' => 'col-lg-4 col-form-label']) !!}
            <div class="col-lg-4">
                {!! Form::text('date', $module_name_singular->expire_at == null || $module_name_singular->expire_at->format('Ymd') == '20190909' ? now()->format('d-M-Y') : $module_name_singular->expire_at->format('d-M-Y'), ['class' => 'form-control datepicker2','data-week-start'=>'6', 'data-autoclose'=>'true', 'data-today-highlight'=>'true', 'data-date-format'=>'dd-M-yyyy', 'placeholder'=>'mm/dd/yy']) !!}
            </div>
            <div class="col-lg-4">
                {!! Form::select('time', [
                '06:00 AM' => '06:00 AM',
                '06:30 AM' => '06:30 AM',
                '07:00 AM' => '07:00 AM',
                '07:30 AM' => '07:30 AM',
                '08:00 AM' => '08:00 AM',
                '08:30 AM' => '08:30 AM',
                '09:00 AM' => '09:00 AM',
                '09:30 AM' => '09:30 AM',
                '10:00 AM' => '10:00 AM',
                '10:30 AM' =>  '10:30 AM',
                '11:00 AM' => '11:00 AM',
                '11:30 AM' => '11:30 AM',
                '12:00 PM' => '12:00 PM',
                '12:30 PM' => '12:30 PM',
                '01:00 PM' => '01:00 PM',
                '01:30 PM' => '01:30 PM',
                '02:00 PM' => '02:00 PM',
                '02:30 PM' => '02:30 PM',
                '03:00 PM' => '03:00 PM',
                '03:30 PM' => '03:30 PM',
                '04:00 PM' => '04:00 PM',
                '04:30 PM' => '04:30 PM',
                '05:00 PM' => '05:00 PM',
                '05:30 PM' => '05:30 PM',
                '06:00 PM' => '06:00 PM',
                '06:30 PM' => '06:30 PM',
                '07:00 PM' => '07:00 PM',
                '07:30 PM' => '07:30 PM',
                '08:00 PM' => '08:00 PM',
                '08:30 PM' => '08:30 PM',
                '09:00 PM' => '09:00 PM',
                '09:30 PM' => '09:30 PM',
                '10:00 PM' => '10:00 PM',
                '10:30 PM' => '10:30 PM',
                '11:00 PM' => '11:00 PM',
                '11:30 PM' => '11:30 PM'], $module_name_singular->expire_at->format('h:i A'), ['class' => 'form-control js-select2','style'=>'width:150px']) !!}
            </div>
        </div>
    </div>
</div>

<br>
<br>
