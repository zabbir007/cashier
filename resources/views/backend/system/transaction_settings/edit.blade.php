@extends ('backend.layouts.app')

@section ('title', $page_heading)

@section('page-header')
    <h1>
        {{ $page_heading }}
    </h1>
@endsection

@section('content')
    @component('includes.components.widget')
        @slot('title')
            {{ $page_heading }}
        @endslot

        @slot('action_buttons')
            @include($module_view.'.header-buttons')
        @endslot

        {{ Form::model($module_name_singular, ['route' => [$module_route.'.update', $module_name_singular], 'class' => 'form-horizontal', 'role' => 'form', 'method' => 'PATCH']) }}

        @include($module_view.'.form')

        <div class="row">
            <div class="col-lg-6">
                {!! Form::button(('<i class="fa fa-check fa-fw"></i>&nbsp; Set Restriction'), ['class' => 'btn btn-success pull-right','type'=>'submit']) !!}
            </div>

            <div class="col-lg-6">
                {!! link_to(route($module_route.'.index'), ('<i class="fa fa-mail-reply"></i>&nbsp;'.trans('buttons.general.cancel')), ['class' => 'btn btn-danger pull-left'], null, false) !!}
            </div><!--col-lg-1-->
        </div><!--form control-->
        <br><br>
        {{ Form::close() }}

    @endcomponent
@endsection
