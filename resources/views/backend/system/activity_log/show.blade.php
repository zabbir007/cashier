@extends ('backend.layouts.app')

@section ('title', $page_heading)

@section('page-header')
    <h1>
        {{ $page_heading }}
    </h1>
@endsection

@section('content')
    <div class="row">
        <div class="col-md-12">
            @component('includes.components.widget')

                @slot('title')
                    {{ $page_heading }}
                @endslot

                @slot('action_buttons')
                    {{--@include($module_view.'.header-buttons')--}}
                @endslot

                <div class="row">
                    <div class="col-md-8">
                        <div class="block block-themed">
                            <div class="block-header">
                                <h3 class="block-title">Changes by {{ $$module_name_singular->causer->full_name }}</h3>
                                <div class="block-options">
                                    <button type="button" class="btn-block-option">
                                        <i class="si si-wrench"></i>
                                    </button>
                                </div>
                            </div>
                            <div class="block-content">
                                <ul class="list list-activity">
                                    @foreach($revision_history_by_causer as $history)
                                        <li>
                                            <i class="si si-arrow-right-circle text-success"></i>
                                            <div class="font-w600">
                                                {{ $history->description }}
                                                <span
                                                    class="font-size-xs text-muted text-right">{{$history->created_at->diffForHumans() }}</span>
                                            </div>

                                            <div>
                                                <a href="javascript:void(0)">{{ $$module_name_singular->subject->name }}</a>
                                            </div>

                                            <ul class="nav-users push">
                                                @foreach(array_get($history->changes,'attributes') as $column => $newVal)
                                                    <li>
                                                        <i class="fa fa-circle text-success"></i>{{ title_case($column) }}
                                                        <div
                                                            class="font-w400 font-size-xs text-muted">{{ $newVal }}</div>
                                                    </li>
                                                @endforeach

                                                @foreach(array_get($history->changes,'old') as $column => $newVal)
                                                    <li>
                                                        <i class="fa fa-circle text-warning"></i>{{ title_case($column) }}
                                                        <div
                                                            class="font-w400 font-size-xs text-muted">{{ $newVal }}</div>
                                                    </li>
                                                @endforeach

                                            </ul>
                                        </li>
                                    @endforeach
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="block block-themed">
                            <div class="block-header bg-success">
                                <h3 class="block-title">Revision History</h3>
                                <div class="block-options">
                                    <button type="button" class="btn-block-option">
                                        <i class="si si-pencil"></i>
                                    </button>
                                </div>
                            </div>
                            <div class="block-content">
                                <ul class="list list-activity">
                                    @foreach($revision_history as $history)
                                        <li>
                                            <div class="font-w600">
                                                {{ $history->description }}
                                                <span
                                                    class="font-size-xs text-muted text-right">{{$history->created_at->diffForHumans() }}</span>
                                            </div>

                                            <div>
                                                <a href="javascript:void(0)">{{ $history->causer->full_name }}</a>
                                            </div>
                                        </li>
                                    @endforeach
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>


                {{--@component('includes.components.table',['class'=>'table table-condensed table-hover table-striped','id'=>'users-table'])--}}
                {{--@slot('tbody')--}}
                {{--<tr>--}}
                {{--<td><strong>Name</strong></td>--}}
                {{--<td>{{$module_name_singular->name}}</td>--}}
                {{--</tr>--}}
                {{--<tr>--}}
                {{--<td><strong>Code</strong></td>--}}
                {{--<td>{{$module_name_singular->code}}</td>--}}
                {{--</tr>--}}
                {{--<tr>--}}
                {{--<td><strong>Address 1</strong></td>--}}
                {{--<td>{{$module_name_singular->address1}}</td>--}}
                {{--</tr>--}}
                {{--<tr>--}}
                {{--<td><strong>Address 2</strong></td>--}}
                {{--<td>{{$module_name_singular->address2}}</td>--}}
                {{--</tr>--}}
                {{--<tr>--}}
                {{--<td><strong>Address 3</strong></td>--}}
                {{--<td>{{$module_name_singular->address3}}</td>--}}
                {{--</tr>--}}
                {{--<tr>--}}
                {{--<td><strong>Address 4</strong></td>--}}
                {{--<td>{{$module_name_singular->address4}}</td>--}}
                {{--</tr>--}}
                {{--<tr>--}}
                {{--<td><strong>Location</strong></td>--}}
                {{--<td>{{$module_name_singular->location}}</td>--}}
                {{--</tr>--}}
                {{--<tr>--}}
                {{--<td><strong>Status</strong></td>--}}
                {{--<td>--}}
                {{--<span class="badge badge-success">{!! $module_name_singular->status_label !!}</span>--}}
                {{--</td>--}}
                {{--</tr>--}}
                {{--@endslot--}}
                {{--@endcomponent--}}

            @endcomponent
        </div><!-- END column -->
    </div><!-- .row -->
@endsection
