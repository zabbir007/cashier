@extends ('backend.layouts.app')

@section ('title', $page_heading)

@section('content')
    <div class="row">
        <!-- DOM dataTable -->
        <div class="col-md-12">
            @component('includes.components.widget')

                @slot('title')
                    {{ $page_heading }}
                @endslot

                @slot('action_buttons')
                    {{--@include($module_view.'.header-buttons')--}}
                @endslot

                {{--Widget body start--}}
                @component('includes.components.table',['class'=>'table table-condensed table-hover table-striped table-bordered','id'=>''])
                    @slot('thead')
                        <th>#</th>
                        <th>Type</th>
                        <th>Description</th>
                        <th>User</th>
                        <th>Log Time</th>
                        <th>{{ __('labels.general.actions') }}</th>
                    @endslot

                    @slot('tbody')
                        @foreach($logs as $module_name)
                            <tr>
                                <td>{{ $loop->iteration }}</td>
                                <td>{{ $module_name->log_name }}</td>
                                <td>{{ $module_name->description }}</td>
                                <td><a href="{{ route('admin.access.user.show', $module_name->causer) }}"
                                       target="_blank">{{  $module_name->causer->full_name }}</a></td>
                                <td>{{ $module_name->created_at }}</td>
                                <td><a href="{{ route($module_route.'.show', $module_name) }}" target="_blank"><i
                                            class="si si-refresh"></i></a></td>
                            </tr>
                        @endforeach
                    @endslot
                @endcomponent

                <div class="pull-right">
                    {!! $logs->render() !!}
                </div>

            @endcomponent
        </div><!-- END column -->
    </div><!-- .row -->
@endsection
