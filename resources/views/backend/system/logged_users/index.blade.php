@extends ('backend.layouts.app')

@section ('title', trans('labels.backend.access.users.management'))

@section('page-header')
    <h1>
        {{ trans('labels.backend.access.users.management') }}
        <small>{{ trans('labels.backend.access.users.active') }}</small>
    </h1>
@endsection

@section('content')
    <div class="row">
        <!-- DOM dataTable -->
        <div class="col-md-12">
            @component('includes.components.widget')

                @slot('title')
                    Currently Logged in Users
                @endslot

                @slot('action_buttons')
                    {{--                    @include('backend.access.includes.partials.user-header-buttons')--}}
                @endslot

                {{--Widget body start--}}
                @component('includes.components.table',['class'=>'table table-condensed table-hover table-striped','id'=>'users-table'])
                    @slot('thead')
                        <th>{{ trans('labels.backend.access.users.table.name') }}</th>
                        <th>{{ trans('labels.backend.access.users.table.username') }}</th>
                        <th>Branch</th>
                        <th>{{ trans('labels.backend.access.users.table.roles') }}</th>
                        <th>Last Logged</th>
                        <th>Logged IP</th>
                        {{--<th>{{ trans('labels.backend.access.users.table.social') }}</th>--}}
                        {{--<th>{{ trans('labels.backend.access.users.table.created') }}</th>--}}
                        {{--                        <th>{{ trans('labels.backend.access.users.table.last_updated') }}</th>--}}
                        {{--                        <th>{{ trans('labels.general.actions') }}</th>--}}
                    @endslot
                @endcomponent

            @endcomponent
        </div><!-- END column -->
    </div><!-- .row -->
@endsection

@section('after-scripts')
    <script>
        $(function () {
            $('#users-table').DataTable({
                dom: 'lfrtip',
                processing: false,
                serverSide: true,
                autoWidth: false,
                ajax: {
                    url: '{{ route($module_route.'.get') }}',
                    type: 'post',
                    data: {status: 1, trashed: false},
                    error: function (xhr, err) {
                        if (err === 'parsererror')
                            location.reload();
                    }
                },
                columns: [
                    {data: 'name', name: '{{config('access.users_table')}}.last_name'},
                    {data: 'username', name: '{{config('access.users_table')}}.username'},
                    {data: 'branches', name: 'branches', sortable: true},
                    {data: 'roles', name: '{{config('access.roles_table')}}.name', sortable: false},
                        {{--{data: 'social', name: 'social', sortable: false},--}}
                        {{--{data: 'created_at', name: '{{config('access.users_table')}}.created_at'},--}}
                    {
                        data: 'last_login_at', name: 'last_login_at'
                    },
                    {data: 'last_login_ip', name: 'last_login_at', class: 'text-center'},
                    {{--{data: 'updated_at', name: '{{config('access.users_table')}}.updated_at'},--}}
                    // {data: 'actions', name: 'actions', searchable: false, sortable: false}
                ],
                order: [[0, "asc"]],
                searchDelay: 500
            });
        });
    </script>
@endsection
