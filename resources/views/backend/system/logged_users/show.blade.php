@extends ('backend.layouts.app')

@section ('title', trans('labels.backend.access.users.management') . ' | ' . trans('labels.backend.access.users.view'))

@section('page-header')
    <h1>
        {{ trans('labels.backend.access.users.management') }}
        <small>{{ trans('labels.backend.access.users.view') }}</small>
    </h1>
@endsection

@section('content')
    @component('includes.components.widget')
        @slot('title')
            {{ trans('labels.backend.access.users.view') }}
        @endslot

        @slot('action_buttons')
            @include('backend.access.includes.partials.user-header-buttons')
        @endslot

        <div role="tabpanel">

            <!-- Nav tabs -->
            <ul class="nav nav-tabs" role="tablist">
                <li role="presentation" class="active">
                    <a href="#overview" aria-controls="overview" role="tab"
                       data-toggle="tab">{{ trans('labels.backend.access.users.tabs.titles.overview') }}</a>
                </li>

                <li role="presentation">
                    <a href="#history" aria-controls="history" role="tab"
                       data-toggle="tab">{{ trans('labels.backend.access.users.tabs.titles.history') }}</a>
                </li>
            </ul>

            <div class="tab-content">

                <div role="tabpanel" class="tab-pane mt-30 active" id="overview">
                    @include('backend.access.show.tabs.overview')
                </div><!--tab overview profile-->

                <div role="tabpanel" class="tab-pane mt-30" id="history">
                    @include('backend.access.show.tabs.history')
                </div><!--tab panel history-->

            </div><!--tab content-->

        </div><!--tab panel-->

    @endcomponent

@endsection