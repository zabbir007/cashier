@extends ('backend.layouts.app')

@section ('title', $page_heading)

@section('after-styles')
    <style type="text/css">
        @media print {
            .block-header, .block-title, .block-option {
                display: none;
            }

            .block-content {
                overflow-x: visible;
                height: 100%;
            }

            .text-center {
                text-align: center;
            }

            .table > thead > tr > th {
                vertical-align: middle;
            }

            table {
                overflow-x: visible;
                white-space: nowrap;
                background: #fff;
            }

            .table th {
                font-size: 10px;
                font-weight: 600;
            }

            .widget-body {
                padding: 5px;
                padding-top: 0;
            }

            .table > tbody > tr > td, .table > tfoot > tr > th, .table > tfoot > tr > td {
                padding: 0 !important;
                /* font-size: 13px; */
                color: #0b0b0b;
                background: #fff;
            }

            .table > tbody > tr > td, .table > tbody > tr > th, .table > tfoot > tr > td, .table > tfoot > tr > th, .table > thead > tr > td, .table > thead > tr > th {
                vertical-align: middle;
            }

            .widget-separator {
                margin: 0;
            }

            h4, h5, h6 {
                margin-top: 1px;
                margin-bottom: 1px;
            }

            @page {
                size: auto;
                margin: 0.24in;
                margin-top: 0;
                padding-top: 0.24in;
                /*width: 21cm;*/
                /*height: 29.7cm;*/
                page-break-after: always;
            }

            body {
                margin: 0;
                font-size: 11px;
                padding-top: 0.24in;
                overflow: auto;
                width: 100%;
                height: 100%;
            }

            html {
                background-color: #FFFFFF;
                margin: 0;
                overflow: auto;
            }
        }
    </style>
@endsection

@section('page-header')
    <h1>
        {{ $page_heading }}
    </h1>
@endsection

@section('content')
    @component('includes.components.widget')
        @slot('title')
            {{ $page_heading }}
        @endslot
        @slot('action_buttons')

            <a href="{{ route($module_route.'.index') }}" class="btn-block-option"><i
                    class="si si-grid"></i>&nbsp;All {{ title_case($module_name) }}
            </a>
            <button type="button" class="btn-block-option" onclick="printNow();">
                <i class="si si-printer"></i> Print
            </button>
            <button type="button" class="btn-block-option" data-toggle="block-option"
                    data-action="fullscreen_toggle"></button>
        @endslot

        <div class="row" ref="inputForm" id="printDiv">
            {{ Form::hidden('branch_id', $selected_branch_id) }}
                    {{ Form::hidden('branch_code', $selected_branch_code) }}
                    {{ Form::hidden('branch_name', $selected_branch_name) }}
                    {{ Form::hidden('bank_id', $selected_bank_id) }}
                    {{ Form::hidden('month', $selected_month_id) }}
                    {{ Form::hidden('account_number', $selected_account) }}

                    {{ Form::hidden('opening_balance', 0) }}



                    {{ Form::hidden('closing_balance_book', $openingBalance->opening_balance,['v-model'=>'closingBalanceAsPerBankBook']) }}
                    {{ Form::hidden('balance_as_accpac', 0,['v-model'=>'balance_as_accpac']) }}
                    {{ Form::hidden('cheques_issued_old_reconciliation', 0,['v-model'=>'cheques_issued_old_reconciliation']) }}
                    {{ Form::hidden('cheques_issued_new_reconciliation', 0,['v-model'=>'cheques_issued_new_reconciliation']) }}
                    {{ Form::hidden('total_d', 0,['v-model'=>'total_d']) }}
                    {{ Form::hidden('total_e', 0,['v-model'=>'total_e']) }}
                    {{ Form::hidden('credit_bank_not_adjusted', 0,['v-model'=>'credit_bank_not_adjusted']) }}
                    {{ Form::hidden('credit_bank_not_adjusted1', 0,['v-model'=>'credit_bank_not_adjusted1']) }}
                    {{ Form::hidden('debit_bank_not_adjusted', 0,['v-model'=>'debit_bank_not_adjusted']) }}
                    {{ Form::hidden('total_f', 0,['v-model'=>'total_f']) }}
                    {{ Form::hidden('total_g', 0,['v-model'=>'total_g']) }}
                    {{ Form::hidden('total_h', 0,['v-model'=>'total_h']) }}
                    {{ Form::hidden('total_i', 0,['v-model'=>'total_i']) }}
                    <input type="hidden" name="balance_for_a" value="0" v-model="balance_for_a">
            @php
                $all_received_from_ho = collect($all_received_from_ho);
             $selected_month_dates = collect($selected_month_dates);
            @endphp

            <div class="col-12 d-none d-print-block text-center">
                <h4 class="mb-0 text-center" style="text-align: center;">TRANSCOM DISTRIBUTION COMPANY
                    LIMITED</h4>
            </div>

            <div class="col-12 d-none d-print-block text-center">
                <h6 class="mb-1 text-center" style="text-align: center;">IMPREST BANK RECONCILIATION
                    STATEMENT</h6>
            </div>

            <div class="col-12">
                <table class="table table-condensed">
                    <tbody>
                    {{--                                <tr class="d-none d-print-block text-center" style="text-align: center;border: none;border: #fff;">--}}
                    {{--                                    <td colspan="7" class="no-top-border text-center" style="text-align: center;border: none;border: #fff;">--}}
                    {{--                                        --}}
                    {{--                                    </td>--}}
                    {{--                                </tr>--}}
                    {{--                                <tr class="d-none d-print-block text-center" style="text-align: center;border: none;border: #fff;">--}}
                    {{--                                    <td colspan="7" class="no-top-border text-center" style="text-align: center;border: none;border: #fff;">--}}
                    {{--                                        <h4 class="mb-0 text-center" style="text-align: center;">IMPREST BANK RECONCILIATION STATEMENT</h4>--}}
                    {{--                                    </td>--}}
                    {{--                                </tr>--}}
                    <tr class="d-print-none no-print">
                        <td colspan="2" class="no-top-border">
                            <h6 class="mb-0">TRANSCOM DISTRIBUTION COMPANY LIMITED</h6>
                        </td>

                        <td class="no-top-border" width="130">&nbsp;</td>

                        <td colspan="2" class="no-top-border">
                            <h6 class="mb-0">IMPREST BANK RECONCILIATION STATEMENT</h6>
                        </td>
                    </tr>


                    <tr style="white-space: nowrap;" class="block-content">
                        <td>&nbsp;<strong>BRANCH NAME:</strong></td>
                        <td><strong>{{ $selected_branch_name }}</strong>&nbsp;</td>

                        <td class="no-top-border" width="130">&nbsp;</td>

                        <td>&nbsp;<strong>ACCOUNT NUMBER:</strong></td>
                        <td><strong>{{ $selected_account_number }}</strong>&nbsp;</td>
                    </tr>
                    <tr style="white-space: nowrap;">
                        <td>&nbsp;<strong>BANK NAME:</strong></td>
                        <td><strong>{{ $selected_bank_name }}</strong>&nbsp;</td>

                        <td class="no-top-border" width="130">&nbsp;</td>


                    </tr>
                    <tr>
                        <td><strong>MONTH:</strong></td>
                        <td><strong>{{ $selected_month_name }}</strong></td>

                        <td class="no-top-border" width="130">&nbsp;</td>

                        <td style="font-weight: bold; background: #00b050 none repeat scroll 0 0; ">
                            <strong>CLOSING BALANCE :</strong></td>
                        <td class="text-right" style=""><strong>@{{ closingBalanceAsPerBankBook | money_format
                                }}</strong></td>
                    </tr>
                    </tbody>
                </table>

                <table class="table table-bordered table-condensed">
                    <tbody>

                    {{-- Start Opening Balance Input--}}
                    <tr>
                        <td style="white-space: nowrap;font-weight: bold; background: #c4bd97 none repeat scroll 0 0;"
                            colspan="6"><strong>(A) Balance as per ACCPAc ledger as on :
                                ({{ title_case($selected_month_name) }})</strong></td>
                        <td style="white-space: nowrap; border: 0.5pt solid #d9d9d9; background: #ffffff none repeat scroll 0 0; text-align: right;font-weight: bold;">
                            @{{ closingBalanceAsPerBankBook | money_format }}
                        </td>
                    </tr>
                    {{-- End Opening Balance Input--}}

                    <tr style="margin: 0;padding: 0;">
                        <td colspan="7" style="margin: 0;padding: 0;">&nbsp;</td>
                    </tr>

                    {{-- Start Deposited/Received--}}
                    <tr>
                        <td style="white-space: nowrap;color: #002060; font-weight: bold; background: #9bbb59 none repeat scroll 0 0;"
                            colspan="6">
                            <strong>(B) Add: Cheques Issued but not presented (Old reconciliation)
                                ({{ title_case($selected_month_name) }}):</strong>
                        </td>

                        <td style="white-space: nowrap;color: #002060; font-weight: bold; background: #d8e4bc none repeat scroll 0 0; text-align: right;">
                            @{{ totalReceivedFromHo | money_format }}
                        </td>
                    </tr>

                    <tr style="background: #dbe6c4 ">
                        {{-- Start column for loop --}}
                        @for($i=1;$i<=1;$i++)
                            <td style="font-weight: bold;text-align: center;">Date</td>
                            <td style="font-weight: bold;text-align: right;">Name of the party</td>
                            <td style="font-weight: bold;text-align: center;">Cheque No</td>
                            <td style="font-weight: bold;text-align: right;">Source Batch</td>
                            <td style="font-weight: bold;text-align: center;">Amount</td>
                        @endfor
                        {{-- End column for loop --}}

                        <td style="font-weight: bold;">&nbsp;</td>
                    </tr>

                    {{-- Start for loop for each--}}
                    @php
                        $sumRow1 = $sumRow2 = 0;
                    @endphp

                    {{-- tr used style: background: #dbe6c4 none repeat scroll 0 0; --}}
                    @foreach($accpacData_b_data as $index =>$accpacData_b)
                        <?php
                        if($accpacData_b->entry_date=='1970-01-01'){
                            $accpacData_b_d='0';
                        }else{
                            $accpacData_b_d=$accpacData_b->entry_date;
                        }
                        ?>
                        <tr style="background: #dbe6c4 none repeat scroll 0 0;">
                            @for($i=1;$i<=1;$i++)
                                <td style="white-space: nowrap;text-align: center;">
                                    {{ Form::text('deposited_received_from_corporate['.$i.']['.$index.'][date]', $accpacData_b_d, ['class' => 'datepicker9','readonly' => 'true','data-week-start'=>'6', 'data-autoclose'=>'true', 'data-today-highlight'=>'true', 'data-date-format'=>'dd-M-yyyy', 'placeholder'=>'select date','style'=>'padding:0','autocomplete'=>'off']) }}
                                </td>
                                <td>{{ Form::text('deposited_received_from_corporate['.$i.']['.$index.'][party_name]', $accpacData_b->party_name, ['readonly' => 'true']) }}</td>
                                <td>{{ Form::text('deposited_received_from_corporate['.$i.']['.$index.'][cheque]', $accpacData_b->cheque_no, ['readonly' => 'true']) }}</td>
                                <td>{{ Form::text('deposited_received_from_corporate['.$i.']['.$index.'][batch]', $accpacData_b->source_batch, ['readonly' => 'true']) }}</td>
                                {{--                                            {{ Form::hidden('deposited_received_from_corporate['.$date->format('d-M-Y').'][date]', $date->format('Y-m-d')) }}--}}
                                <td>{{ Form::number('deposited_received_from_corporate['.$i.']['.$index.'][amount]', $accpacData_b->amount, ['class' => 'deposited_received_from_corporate-'.$i,'min'=>0,'step'=>'any','readonly' => 'true','@input'=>'calculateDepositedReceivedFromCorporate()']) }}</td>
                            @endfor
                            <td>&nbsp;</td>
                        </tr>
                    @endforeach

                    <tr style="background: #c4d79b none repeat scroll 0 0;">

                        <td>&nbsp;</td>
                        <td class="text-right">
                            {{--                                    <strong>--}}
                            {{--                                        @{{ sumDepositedReceivedFromCorporateRow2 | money_format--}}
                            {{--                                        }}</strong>--}}
                        </td>
                        <td>&nbsp;</td>

                        <td>&nbsp;</td>
                        <td class="text-right"><strong>@{{ sumDepositedReceivedFromCorporateRow1 | money_format
                                }}</strong></td>
                        <td>&nbsp;</td>

                        <td>&nbsp;</td>
                    </tr>
                    {{-- End Deposited/Received--}}


                    <tr style="margin: 0;padding: 0;">
                        <td colspan="7" style="margin: 0;padding: 0;">&nbsp;</td>
                    </tr>

                    {{-- Start Bank Interest--}}
                    {{--                            <tr>--}}
                    {{--                                <td colspan="5"--}}
                    {{--                                    style="text-align: left;white-space: nowrap;color: #0070c0; background: #c4d79b none repeat scroll 0 0;">--}}
                    {{--                                    <strong>Add: Bank Interest for the Month ({{ title_case($selected_month_name) }})--}}
                    {{--                                        (<span style="font-style: italic;">if Any</span>):</strong>--}}
                    {{--                                </td>--}}
                    {{--                                <td>{{ Form::number('bank_interest', null, ['class' => '','v-model'=>'bankInterest','min'=>0,'step'=>'any']) }}</td>--}}
                    {{--                                <td style="white-space: nowrap;text-align: right;color: #002060; font-weight: bold; background: #d8e4bc none repeat scroll 0 0;">--}}
                    {{--                                    @{{ bankInterest | money_format }}--}}
                    {{--                                </td>--}}
                    {{--                            </tr>--}}
                    {{-- End Bank Interest--}}
                    <tr style="margin: 0;padding: 0">
                        <td colspan="7" style="margin: 0;padding: 0">&nbsp;</td>
                    </tr>

                    {{-- Start Cheque issued (Tranf. to Cash)--}}
                    <tr style="white-space: nowrap;font-weight: bold; background: #4bacc6 none repeat scroll 0 0; ">
                        <td style="text-align: left;" colspan="6"><span style="font-style: italic;"> (C) Add: Cheques Issued but not presented (New reconciliation)</span>
                            <span> for the Month ({{ title_case($selected_month_name) }}):</span>
                        </td>
                        <td style="text-align: right">@{{ totalChequeIssued | money_format }}</td>
                    </tr>

                    @php
                        $sumRow1 = $sumRow2 = 0;
                    @endphp

                    <tr style="background: #b7dee8 none repeat scroll 0 0;">
                        {{-- Start column for loop --}}
                        @for($i=1;$i<=1;$i++)
                            <td style="font-weight: bold;text-align: center;">Date</td>
                            <td style="font-weight: bold;text-align: right;">Name of the party</td>
                            <td style="font-weight: bold;text-align: center;">Cheque No</td>
                            <td style="font-weight: bold;text-align: right;">Source Batch</td>
                            <td style="font-weight: bold;text-align: center;">Amount</td>
                        @endfor
                        {{-- End column for loop --}}

                        <td style="font-weight: bold;">&nbsp;</td>
                    </tr>

                    {{-- Start for loop for each--}}
                    @php
                        $sumRow1 = $sumRow2 = 0;
                    @endphp

                    @foreach($accpacData_c_data as $index =>$accpacData_c)
                        <?php
                        if($accpacData_c->entry_date=='1970-01-01'){
                            $accpacData_c_d='0';
                        }else{
                            $accpacData_c_d=$accpacData_c->entry_date;
                        }
                        ?>
                        <tr style="background: #dbe6c4 none repeat scroll 0 0;">
                            @for($i=1;$i<=1;$i++)
                                <td style="white-space: nowrap;text-align: center;">
                                    {{ Form::text('less_check_issued['.$i.']['.$index.'][date]',$accpacData_c_d, ['class' => 'datepicker9','readonly' => 'true','data-week-start'=>'6', 'data-autoclose'=>'true', 'data-today-highlight'=>'true', 'data-date-format'=>'dd-M-yyyy', 'placeholder'=>'select date','style'=>'padding:0','autocomplete'=>'off']) }}
                                </td>
                                <td>{{ Form::text('less_check_issued['.$i.']['.$index.'][party_name]', $accpacData_c->party_name, ['readonly' => 'true']) }}</td>
                                <td>{{ Form::text('less_check_issued['.$i.']['.$index.'][cheque]', $accpacData_c->cheque_no, ['readonly' => 'true']) }}</td>
                                <td>{{ Form::text('less_check_issued['.$i.']['.$index.'][batch]', $accpacData_c->source_batch, ['readonly' => 'true']) }}</td>
                                {{--                                            {{ Form::hidden('deposited_received_from_corporate['.$date->format('d-M-Y').'][date]', $date->format('Y-m-d')) }}--}}
                                <td>{{ Form::number('less_check_issued['.$i.']['.$index.'][amount]', $accpacData_c->amount, ['class' => 'less_check_issued-'.$i,'min'=>0,'step'=>'any','readonly' => 'true','@input'=>'calculateLessCheckIssued()']) }}</td>

                            @endfor
                            <td>&nbsp;</td>
                        </tr>
                    @endforeach

                    <tr style="background: #4bacc6 none repeat scroll 0 0;">
                        <td>&nbsp;</td>
                        <td class="text-right">
                            {{--                                    <strong>@{{ sumLessChequeIssuedRow1 | money_format }}</strong>--}}
                        </td>
                        <td>&nbsp;</td>

                        <td>&nbsp;</td>
                        <td class="text-right"><strong>@{{ sumLessChequeIssuedRow1 | money_format }}</strong>
                        </td>
                        <td>&nbsp;</td>

                        <td>&nbsp;</td>
                    </tr>
                    {{-- End Cheque issued (Tranf. to Cash)--}}

                    <tr style="margin: 0;padding: 0;">
                        <td colspan="7" style="margin: 0;padding: 0;">&nbsp;</td>
                    </tr>

                    {{--                            D count--}}

                    <tr style="background: #4bacc6 none repeat scroll 0 0;">
                        <td>&nbsp;</td>
                        <td class="text-right">
                            Total D =
                        </td>
                        <td>&nbsp;
                            (B+C)
                        </td>

                        <td>&nbsp;</td>

                        <td>&nbsp;</td>


                        <td>&nbsp;</td>
                        <td class="text-right"><strong>@{{ totalValueD | money_format }}</strong>
                        </td>
                    </tr>
                    <tr style="margin: 0;padding: 0;">
                        <td colspan="7" style="margin: 0;padding: 0;">&nbsp;</td>
                    </tr>

                    {{--                            E count--}}

                    <tr style="background: #4bacc6 none repeat scroll 0 0;">
                        <td>&nbsp;</td>
                        <td class="text-right">
                            Total E =
                        </td>
                        <td>&nbsp;
                            (A+D)
                        </td>

                        <td>&nbsp;</td>

                        <td>&nbsp;</td>


                        <td>&nbsp;</td>
                        <td class="text-right"><strong>@{{ totalValueE | money_format }}</strong>
                        </td>
                    </tr>
                    <tr style="margin: 0;padding: 0;">
                        <td colspan="7" style="margin: 0;padding: 0;">&nbsp;</td>
                    </tr>

                    <br>

                    {{-- Start Cheque issued (Payment By CQ)--}}

                    {{-- End Cheque issued (Payment By CQ)--}}

                    <tr style="margin: 0;padding: 0;">
                        <td colspan="7" style="margin: 0;padding: 0;">&nbsp;</td>
                    </tr>


                    {{-- Start Bank Charges--}}

                    {{-- End Bank Charges--}}



                    {{-- Start Closing Balance--}}

                    {{-- End Closing Balance--}}



                    {{-- Start Difference between Bank--}}

                    {{-- End Difference between Bank--}}



                    {{-- Start Closing Balance--}}

                    {{-- End Closing Balance--}}




                    {{-- Start Deposited but not shown--}}

                    {{-- End Deposited but not shown--}}



                    {{-- Start Withdrawn but not shown--}}
                    <tr style="white-space: nowrap;background: #79f9ad none repeat scroll 0 0;color: #002060;">
                        <td style="text-align: left;" colspan="6"><strong>(F) Less: Debited by the bank but not adjusted in due time</strong>
                        </td>
                        <td style="white-space: nowrap;text-align: right;font-weight: bold">
                            @{{ totalWithdrawn1 | money_format }}
                        </td>
                    </tr>
                    <tr style="white-space: nowrap;background: #cefcfe none repeat scroll 0 0;">
                        {{-- Start column for loop --}}
                        @for($i=1;$i<=1;$i++)
                            <td style="font-weight: bold;" colspan="2">Date</td>
                            <td style="font-weight: bold;" colspan="2">Reference No.</td>
                            <td style="font-weight: bold;" colspan="2">Amount in Tk</td>

                        @endfor
                        {{-- End column for loop --}}

                    </tr>

                    @foreach($accpacData_f_data as $index =>$accpacData_e1)
                        <?php
                        if($accpacData_e1->entry_date=='1970-01-01'){
                            $accpacData_e_d='0';
                        }else{
                            $accpacData_e_d=$accpacData_e1->entry_date;
                        }
                        ?>
                        {{-- tr style used: background: #cefcfe none repeat scroll 0 0;--}}
                        <tr>
                            @for($j=1;$j<=1;$j++)
                                <td colspan="2">{{ Form::text('bank_book_withdrawn1['.$index.']['.$j.'][date]', $accpacData_e_d, ['class' => 'datepicker','data-week-start'=>'6', 'data-autoclose'=>'true', 'data-today-highlight'=>'true', 'data-date-format'=>'dd-M-yyyy', 'placeholder'=>'select date','style'=>'padding:0','autocomplete'=>'off','readonly' => 'true']) }}</td>
                                <td colspan="2">{{ Form::text('bank_book_withdrawn1['.$index.']['.$j.'][ref_no]', $accpacData_e1->ref_no, ['readonly' => 'true']) }}</td>
                                <td colspan="2">{{ Form::number('bank_book_withdrawn1['.$index.']['.$j.'][amount]', $accpacData_e1->amount, ['class' => 'bank_book_withdrawn1-'.$j,'min'=>0,'step'=>'any','readonly' => 'true','@input'=>'calculateBankBookWithdrawn()']) }}</td>
                            @endfor
                        </tr>
                    @endforeach


                    <tr style="white-space: nowrap;background: #79f9ad none repeat scroll 0 0;">

                        <td>&nbsp;</td>

                        <td>&nbsp;</td>
                        <td class="text-right" colspan="4"><strong>@{{ totalWithdrawn1 | money_format }}</strong></td>



                    </tr>
                    {{-- End Withdrawn but not shown--}}

                    {{-- Start Cheque issued--}}
                    <tr style="white-space: nowrap;background: #e26b0a none repeat scroll 0 0;">
                        <td colspan="6"><strong>(G) Less: Bank Charges & Commission</strong></td>
                        <td style="white-space: nowrap;text-align: right;color: #974706; font-weight: bold;">
                            @{{ totalBankChequeIssued | money_format }}
                        </td>
                    </tr>

                    <tr style="background: #fcd5b4 none repeat scroll 0 0;">
                        {{-- Start column for loop --}}
                        @for($i=1;$i<=1;$i++)
                            <td style="font-weight: bold;" colspan="2">Date</td>

                            <td style="font-weight: bold;" colspan="2">Reference No.</td>
                            <td style="font-weight: bold;" colspan="2">Amount in Tk,</td>
                        @endfor
                        {{-- End column for loop --}}

                    </tr>
                    @foreach($accpacData_g_data as $index =>$accpacData_e2)
                        <?php
                        if($accpacData_e2->entry_date=='1970-01-01'){
                            $accpacData_e2_d='0';
                        }else{
                            $accpacData_e2_d=$accpacData_e2->entry_date;
                        }
                        ?>
                        {{-- used tr style: background: #fcd5b4 none repeat scroll 0 0; --}}
                        <tr>
                            @for($j=1;$j<=1;$j++)
                                <td colspan="2">{{ Form::text('cheque_issued['.$index.']['.$j.'][date]', $accpacData_e2_d, ['class' => 'datepicker','data-week-start'=>'6', 'data-autoclose'=>'true', 'data-today-highlight'=>'true', 'data-date-format'=>'dd-M-yyyy', 'placeholder'=>'select date','style'=>'padding:0','autocomplete'=>'off','readonly' => 'true']) }}</td>

                                <td colspan="2">{{ Form::text('cheque_issued['.$index.']['.$j.'][ref_no]', $accpacData_e2->ref_no, ['readonly' => 'true']) }}</td>
                                <td colspan="2">{{ Form::number('cheque_issued['.$index.']['.$j.'][amount]', $accpacData_e2->amount, ['class' => 'cheque_issued-'.$j,'min'=>0,'step'=>'any','readonly' => 'true','@input'=>'calculateChequeIssued()']) }}</td>
                            @endfor

                        </tr>
                    @endforeach

                    <tr style="background: #e26b0a;">

                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                        <td class="text-right" colspan="4"><strong>@{{ sumChequeIssuedRow1 | money_format }}</strong></td>
                    </tr>

                    {{-- End Cheque issued--}}
                    <tr>

                    </tr>
                    <tr style="white-space: nowrap;background: #79f9ad none repeat scroll 0 0;color: #002060;">
                        <td style="text-align: left;" colspan="6"><strong>(H) Less: Excess PO charges by Bank</strong>
                        </td>
                        <td style="white-space: nowrap;text-align: right;font-weight: bold">
                            @{{ totalWithdrawn | money_format }}
                        </td>
                    </tr>
                    <tr style="white-space: nowrap;background: #cefcfe none repeat scroll 0 0;">
                        {{-- Start column for loop --}}
                        @for($i=1;$i<=1;$i++)
                            <td style="font-weight: bold;" colspan="2">Date</td>
                            <td style="font-weight: bold;" colspan="2">Reference No.</td>
                            <td style="font-weight: bold;" colspan="2">Amount in Tk</td>

                        @endfor
                        {{-- End column for loop --}}

                    </tr>

                    @foreach($accpacData_h_data as $index =>$accpacData_e1)
                        <?php
                        if($accpacData_e1->entry_date=='1970-01-01'){
                            $accpacData_e1_d='0';
                        }else{
                            $accpacData_e1_d=$accpacData_e1->entry_date;
                        }
                        ?>
                        {{-- tr style used: background: #cefcfe none repeat scroll 0 0;--}}
                        <tr>
                            @for($j=1;$j<=1;$j++)
                                <td colspan="2">{{ Form::text('bank_book_withdrawn['.$index.']['.$j.'][date]', $accpacData_e1_d, ['class' => 'datepicker','data-week-start'=>'6', 'data-autoclose'=>'true', 'data-today-highlight'=>'true', 'data-date-format'=>'dd-M-yyyy', 'placeholder'=>'select date','style'=>'padding:0','autocomplete'=>'off','readonly' => 'true']) }}</td>
                                <td colspan="2">{{ Form::text('bank_book_withdrawn['.$index.']['.$j.'][ref_no]', $accpacData_e1->ref_no, ['readonly' => 'true']) }}</td>
                                <td colspan="2">{{ Form::number('bank_book_withdrawn['.$index.']['.$j.'][amount]', $accpacData_e1->amount, ['class' => 'bank_book_withdrawn-'.$j,'min'=>0,'step'=>'any','readonly' => 'true','@input'=>'calculateBankBookWithdrawn()']) }}</td>
                            @endfor
                        </tr>
                    @endforeach


                    <tr style="white-space: nowrap;background: #79f9ad none repeat scroll 0 0;">

                        <td>&nbsp;</td>

                        <td>&nbsp;</td>
                        <td class="text-right" colspan="4"><strong>@{{ sumWithdrawnRow1 | money_format }}</strong></td>



                    </tr>
                    <tr style="margin: 0;padding: 0;">
                        <td colspan="7" style="margin: 0;padding: 0;">&nbsp;</td>
                    </tr>

                    <tr>

                    </tr>
                    <tr style="margin: 0;padding: 0;">
                        <td colspan="7" style="margin: 0;padding: 0;">&nbsp;</td>
                    </tr>
                    {{-- Start Adjisted closing bank--}}
                    <tr>
                        <td style="white-space: nowrap;text-align: center;color: #002060; background: #b1a0c7 none repeat scroll 0 0;">
                            <strong>I</strong>
                        </td>
                        <td style="white-space: nowrap;text-align: left;color: #002060; background: #b1a0c7 none repeat scroll 0 0;"
                            colspan="5">
                            <strong>Balance as per bank statement I=(E-F-G-H) </strong>
                        </td>
                        <td style="white-space: nowrap;text-align: right;color: #974706; background: #ccc0da none repeat scroll 0 0;font-weight: bold">
                            @{{ adjustedClosingBookBalance | money_format }}
                        </td>
                    </tr>
                    <tr style="margin: 0;padding: 0;">
                        <td colspan="" style="margin: 0;padding: 0;">&nbsp;</td>
                        <td colspan="5" style="margin: 0;padding: 0;">&nbsp;</td>
                        <td colspan="" style="margin: 0;padding: 0;">&nbsp;</td>
                    </tr>
                    <tr style="margin: 0;padding: 0;">
                        <td colspan="1" style="margin: 0;padding: 0;">
                            <label>Preparation Date:</label>
                        </td>
                        <td colspan="" style="margin: 0;padding: 0;">&nbsp;</td>

                        <td colspan="" style="margin: 0;padding: 0;">&nbsp;</td>

                        <td colspan="" style="margin: 0;padding: 0;">&nbsp;</td>
                        <td colspan="" style="margin: 0;padding: 0;">&nbsp;</td>
                        <td colspan="" style="margin: 0;padding: 0;">&nbsp;</td>
                        <td colspan="" style="margin: 0;padding: 0;"></td>
                    </tr>
                    <tr style="margin: 0;padding: 0;">
                        <td colspan="1" style="margin: 0;padding: 0;">
                            <label>Date:</label>
                        </td>
                        <td colspan="" style="margin: 0;padding: 0;">&nbsp;</td>

                        <td colspan="" style="margin: 0;padding: 0;">&nbsp;</td>

                        <td colspan="" style="margin: 0;padding: 0;">&nbsp;</td>
                        <td colspan="" style="margin: 0;padding: 0;">&nbsp;</td>
                        <td colspan="" style="margin: 0;padding: 0;">&nbsp;</td>
                        <td colspan="" style="margin: 0;padding: 0;">&nbsp;</td>
                    </tr>
                    <tr style="margin: 0;padding: 0;">
                        <td colspan="" style="margin: 0;padding: 0;">&nbsp;</td>
                        <td colspan="5" style="margin: 0;padding: 0;">&nbsp;</td>
                        <td colspan="" style="margin: 0;padding: 0;">&nbsp;</td>
                    </tr>
                    <tr style="margin: 0;padding: 0;">
                        <td colspan="" style="margin: 0;padding: 0;">&nbsp;</td>
                        <td colspan="5" style="margin: 0;padding: 0;">&nbsp;</td>
                        <td colspan="" style="margin: 0;padding: 0;">&nbsp;</td>
                    </tr>
                    <tr style="margin: 0;padding: 0;">
                        <td colspan="1" style="margin: 0;padding: 0;text-align: left;">Prepared by</td>
                        <td colspan="" style="margin: 0;padding: 0;">&nbsp;</td>
                        <td colspan="1" style="margin: 0;padding: 0; text-align: center;">Checked by</td>
                        <td colspan="" style="margin: 0;padding: 0;">&nbsp;</td>
                        <td colspan="1" style="margin: 0;padding: 0;text-align: center;">Approved by</td>
                        <td colspan="" style="margin: 0;padding: 0;">&nbsp;</td>
                        <td colspan="" style="margin: 0;padding: 0;">&nbsp;</td>
                    </tr>
                    <tr style="margin: 0;padding: 0;">
                        <td colspan="1" style="margin: 0;padding: 0;text-align: left;">(Subodh Kumar Baroi)</td>
                        <td colspan="1" style="margin: 0;padding: 0;text-align: center;"></td>
                        <td colspan="" style="margin: 0;padding: 0; text-align: center;">(Md.Abdullah Mian)</td>
                        <td colspan="" style="margin: 0;padding: 0;">&nbsp;</td>
                        <td colspan="1" style="margin: 0;padding: 0;">&nbsp;</td>
                        <td colspan="" style="margin: 0;padding: 0;">&nbsp;</td>
                        <td colspan="" style="margin: 0;padding: 0;">&nbsp;</td>
                    </tr>

                    {{-- End Adjisted closing bank--}}


                    {{-- Start Discrepancy--}}
                    {{--                            <tr>--}}
                    {{--                                <td style="white-space: nowrap;text-align: right;color: #ff0000;" colspan="4">--}}
                    {{--                                    Discrepancy (should be zero)--}}
                    {{--                                </td>--}}
                    {{--                                <td>&nbsp;</td>--}}
                    {{--                                <td style="white-space: nowrap;text-align: right;color: red; background: black none repeat scroll 0 0;font-weight: bold;"--}}
                    {{--                                    colspan="2">@{{ discrepancy | money_format}}--}}
                    {{--                                </td>--}}
                    {{--                            </tr>--}}
                    {{-- End Discrepancy--}}


                    {{-- Start Signature--}}
                    {{--<tr style="margin: 0;padding: 0;">--}}
                    {{--<td colspan="7" style="margin: 0;padding: 0;">&nbsp;</td>--}}
                    {{--</tr>--}}
                    {{--<tr style="margin: 0;padding: 0;">--}}
                    {{--<td colspan="7" style="margin: 0;padding: 0;">&nbsp;</td>--}}
                    {{--</tr>--}}

                    {{--<tr style="border:none;">--}}
                    {{--<td style="border: none;font-weight: bold;text-align: center;" colspan="2">&nbsp;Accounts In-Charge</td>--}}
                    {{--<td style="border: none;font-weight: bold;text-align: center;" colspan="2">&nbsp;Dy.Branch-in-Charge--}}
                    {{--</td>--}}
                    {{--<td style="border: none;font-weight: bold;text-align: center;" colspan="2">&nbsp;Branch-in-Charge</td>--}}
                    {{--</tr>--}}
                    {{-- End Signature--}}

                    </tbody>
                </table>
            </div>
        </div>

    @endcomponent
@endsection

@section('after-scripts')
    <script type="text/javascript">
        new Vue({
            el: '#app',
            data: {
                loading: false,
                accountNumber: {},
                selectedBankId: '',
                selectedBranchId: '',
                selectedMonth: '',
                selectedYear: '',

                openingBalance: {{ $opening_balance ?? '0' }},

                totalReceivedFromHo: 0,
                bankInterest: '',

                totalChequeIssued: 0,
                totalChequeIssuedCQ: 0,
                bankCharge: '',
                balance_as_accpac:0,
                cheques_issued_old_reconciliation:0,
                cheques_issued_new_reconciliation:0,
                total_d:0,
                total_e:0,
                credit_bank_not_adjusted:0,
                credit_bank_not_adjusted1:0,
                debit_bank_not_adjusted:0,
                total_f:0,
                total_g:0,
                total_h:0,
                total_i:0,
                closingBalanceAsPerBankBook: {{$openingBalance->opening_balance ?? '0'}},
                balance_for_a: 0,
                diffBookAndStatement: '',
                closingBalanceAsPerBankStatement: '',

                totalDeposited: '',
                totalWithdrawn: '',
                totalWithdrawn1: '',
                totalBankChequeIssued: '',

                sumBankBookDepositedRow1: '',
                sumBankBookDepositedRow2: '',
                sumWithdrawnRow1: '',
                sumWithdrawnRow2: '',
                sumChequeIssuedRow1: '',
                sumChequeIssuedRow2: '',
                sumLessChequeIssuedRow1: '',
                sumLessChequeIssuedRow2: '',
                sumLessChequeIssuedCQRow1: '',
                sumLessChequeIssuedCQRow2: '',
                sumDepositedReceivedFromCorporateRow1: '',
                sumDepositedReceivedFromCorporateRow2: '',
                totalValueD: '',
                totalValueE: '',
                adjustedClosingBookBalance: '',
                discrepancy: '',

                disableSubmitBtn: false
            },
            mounted: function () {
                this.calculateClosingBalance();
                this.calculateDepositedReceivedFromCorporate();
                this.calculateLessCheckIssuedCQ();
                this.calculateLessCheckIssued();
                this.calculateChequeIssued();
                this.calculateBankBookWithdrawn();
                this.calculateBankBookWithdrawn1();
                this.calculateBankBookDeposit();
                let $select2 = {width: '100%'};
                this.loading = false;
                this.disableSubmitBtn = true;
                $("#branch").select2($select2).on("change", (e) => {
                    this.selectedBranchId = '1';
                    this.getBanks();
                });
                $("#banks").select2($select2).on("change", (e) => {
                    this.selectedBankId = $(e.currentTarget).val();
                    this.getAccountNumber();
                })
            },
            methods: {
                getBanks: function () {
                    this.loading = true;
                    this.disableSubmitBtn = true;
                    // $("#banks").prop("disabled", true);

                    let config = {
                        //  headers: {'Authorization': 'JWT ' + this.$store.state.token},
                        params: {
                            branch: this.selectedBranchId,
                        }
                    };

                    axios.get('{{ url()->current() }}', config)
                        .then((response) => {
                            this.loading = false;
                            this.disableSubmitBtn = false;
                            let data = response.data.map((obj) => {
                                obj.id = obj.id || obj.pk;
                                obj.text = obj.name;
                                return obj;
                            });

                            this.populateBanks(data);
                        }, (error) => {
                            this.loading = false;
                            this.disableSubmitBtn = true;
                            console.log(error);
                        })
                },
                getAccountNumber: function () {
                    this.loading = true;
                    this.disableSubmitBtn = true;
                    // $("#banks").prop("disabled", true);

                    let config = {
                        //  headers: {'Authorization': 'JWT ' + this.$store.state.token},
                        params: {
                            branch: this.selectedBranchId,
                            bank: this.selectedBankId
                        }
                    };
                    axios.get('{{ route($module_route.'.get_account_number') }}', config)
                        .then((response) => {

                            this.loading = false;
                            this.disableSubmitBtn = false;
                            if (_.isEmpty(response.data)) {
                                swal.fire('Warning', 'No Account Number Found!', 'warning');
                                this.accountNumber = '';
                                this.disableSubmitBtn = true;
                            } else {
                                console.log(response.data);
                                this.accountNumber = response.data;
                                this.disableSubmitBtn = false;
                            }
                        })
                        .catch(error => {
                            this.loading = false;
                            this.disableSubmitBtn = true;
                        })
                },
                populateBanks: function (data) {
                    let div = $("#banks");
                    div.prop("disabled", false);
                    div.select2().empty();
                    div.select2({
                        data: data,
                        width: '100%'
                    }).trigger("change");
                },
                checkRecord: function () {
                    if (_.isEmpty(this.accountNumber)) {
                        swal.fire('Warning', 'Account number should not be empty!', 'warning');
                        return false;
                    }

                    if (_.isEmpty(this.selectedMonth)) {
                        swal.fire('Warning', 'Please select a month!', 'warning');
                        return false;
                    }

                    this.loading = true;
                    this.disableSubmitBtn = false;

                    let config = {
                        //  headers: {'Authorization': 'JWT ' + this.$store.state.token},
                        params: {
                            branch: this.selectedBranchId,
                            bank: this.selectedBankId,
                            month: this.selectedMonth,
                            year: this.selectedYear
                        }
                    };
                    axios.get('{{ route($module_route.'.check_entry') }}', config)
                        .then((response) => {
                            this.loading = false;
                            this.disableSubmitBtn = false;
                            // console.log(response.data);

                            if (response.data == 0) {

                                $("#submitBtn").prop("disabled", false);
                                this.$refs.checkForm.submit();
                            } else {

                                $("#submitBtn").prop("disabled", false);
                                this.$refs.checkForm.submit();
                                // swal.fire('Error', 'Already Data Entered For Given Month!', 'error');

                            }
                        })
                        .catch(error => {
                            this.loading = false;
                            this.disableSubmitBtn = true;
                        })
                },

                calculateClosingBalance: function () {
                    // this.closingBalanceAsPerBankBook = this.openingBalance + this.totalReceivedFromHo + this.bankInterest - this.totalChequeIssued - this.totalChequeIssuedCQ - this.bankCharge;
                    this.closingBalanceAsPerBankBook = this.closingBalanceAsPerBankBook;
                    this.diffBookAndStatement = this.closingBalanceAsPerBankBook - this.closingBalanceAsPerBankStatement;
                    this.discrepancy = this.closingBalanceAsPerBankStatement - this.adjustedClosingBookBalance;
                    this.totalValueD = this.totalChequeIssued + this.totalReceivedFromHo;
                    this.totalValueE =  (this.closingBalanceAsPerBankBook+this.totalValueD);
                    this.total_d=this.totalChequeIssued + this.totalReceivedFromHo;
                    this.total_e = (this.closingBalanceAsPerBankBook+this.totalValueD);
                    this.total_g =this.totalBankChequeIssued;
                    this.total_h =this.totalWithdrawn;
                    this.balance_for_a=this.closingBalanceAsPerBankBook;
                    this.adjustedClosingBookBalance =(this.total_e-this.total_f-this.total_g-this.total_h);
                    this.total_i =(this.total_e-this.total_f-this.total_g-this.total_h);
                    this.balance_as_accpac=this.openingBalance;
                },

                calculateBankBookDeposit: function () {
                    let sumRow1 = sumRow2 = 0;
                    $('.bank_book_deposited-1').each(function () {
                        let val = $(this).val();
                        if (val === '') val = 0;
                        sumRow1 += parseFloat(val);
                    });
                    $('.bank_book_deposited-2').each(function () {
                        let val = $(this).val();
                        if (val === '') val = 0;
                        sumRow2 += parseFloat(val);
                    });
                    this.sumBankBookDepositedRow1 = sumRow1;
                    this.sumBankBookDepositedRow2 = sumRow2;
                    this.totalDeposited = this.sumBankBookDepositedRow1 + this.sumBankBookDepositedRow2;
                    this.calculateClosingBalance();
                },
                calculateBankBookWithdrawn: function () {
                    let sumRow1 = sumRow2 = 0;
                    $('.bank_book_withdrawn-1').each(function () {
                        let val = $(this).val();
                        if (val === '') val = 0;
                        sumRow1 += parseFloat(val);
                    });
                    $('.bank_book_withdrawn-2').each(function () {
                        let val = $(this).val();
                        if (val === '') val = 0;
                        sumRow2 += parseFloat(val);
                    });
                    this.sumWithdrawnRow1 = sumRow1;
                    this.sumWithdrawnRow2 = sumRow2;
                    this.totalWithdrawn = this.sumWithdrawnRow1 + this.sumWithdrawnRow2;
                    this.credit_bank_not_adjusted = this.sumWithdrawnRow1 + this.sumWithdrawnRow2;
                    this.calculateClosingBalance();
                },
                calculateBankBookWithdrawn1: function () {
                    let sumRow1 = sumRow2 = 0;
                    $('.bank_book_withdrawn1-1').each(function () {
                        let val = $(this).val();
                        if (val === '') val = 0;
                        sumRow1 += parseFloat(val);
                    });
                    $('.bank_book_withdrawn1-2').each(function () {
                        let val = $(this).val();
                        if (val === '') val = 0;
                        sumRow2 += parseFloat(val);
                    });
                    this.sumWithdrawnRow11 = sumRow1;
                    this.sumWithdrawnRow21 = sumRow2;
                    this.totalWithdrawn1 = this.sumWithdrawnRow11 + this.sumWithdrawnRow21;
                    this.total_f = this.sumWithdrawnRow11 + this.sumWithdrawnRow21;
                    this.credit_bank_not_adjusted1 = this.sumWithdrawnRow11 + this.sumWithdrawnRow21;
                    this.calculateClosingBalance();
                },
                calculateChequeIssued: function () {
                    let sumRow1 = sumRow2 = 0;
                    $('.cheque_issued-1').each(function () {
                        let val = $(this).val();
                        if (val === '') val = 0;
                        sumRow1 += parseFloat(val);
                    });
                    $('.cheque_issued-2').each(function () {
                        let val = $(this).val();
                        if (val === '') val = 0;
                        sumRow2 += parseFloat(val);
                    });
                    this.sumChequeIssuedRow1 = sumRow1;
                    this.sumChequeIssuedRow2 = sumRow2;
                    this.totalBankChequeIssued = this.sumChequeIssuedRow1 + this.sumChequeIssuedRow2;
                    this.debit_bank_not_adjusted = this.sumChequeIssuedRow1 + this.sumChequeIssuedRow2;
                    this.calculateClosingBalance();
                },
                calculateLessCheckIssued: function () {
                    let sumRow1 = sumRow2 = 0;
                    $('.less_check_issued-1').each(function () {
                        let val = $(this).val();
                        if (val === '') val = 0;
                        sumRow1 += parseFloat(val);
                    });
                    $('.less_check_issued-2').each(function () {
                        let val = $(this).val();
                        if (val === '') val = 0;
                        sumRow2 += parseFloat(val);
                    });
                    this.sumLessChequeIssuedRow1 = sumRow1;
                    this.sumLessChequeIssuedRow2 = sumRow2;
                    this.totalChequeIssued = this.sumLessChequeIssuedRow1 + this.sumLessChequeIssuedRow2;
                    this.cheques_issued_new_reconciliation = this.sumLessChequeIssuedRow1 + this.sumLessChequeIssuedRow2;
                    this.calculateClosingBalance();
                },
                calculateLessCheckIssuedCQ: function () {
                    let sumRow1 = sumRow2 = 0;
                    $('.less_check_issuedCQ-1').each(function () {
                        let val = $(this).val();
                        if (val === '') val = 0;
                        sumRow1 += parseFloat(val);
                    });
                    $('.less_check_issuedCQ-2').each(function () {
                        let val = $(this).val();
                        if (val === '') val = 0;
                        sumRow2 += parseFloat(val);
                    });
                    this.sumLessChequeIssuedCQRow1 = sumRow1;
                    this.sumLessChequeIssuedCQRow2 = sumRow2;
                    this.totalChequeIssuedCQ = this.sumLessChequeIssuedCQRow1 + this.sumLessChequeIssuedCQRow2;
                    this.calculateClosingBalance();
                },
                calculateDepositedReceivedFromCorporate: function () {
                    let sumRow1 = sumRow2 = 0;
                    $('.deposited_received_from_corporate-1').each(function () {
                        let val = $(this).val();
                        if (val === '') val = 0;
                        sumRow1 += parseFloat(val);
                    });
                    $('.deposited_received_from_corporate-2').each(function () {
                        let val = $(this).val();
                        if (val === '') val = 0;
                        sumRow2 += parseFloat(val);
                    });
                    this.sumDepositedReceivedFromCorporateRow1 = sumRow1;
                    this.sumDepositedReceivedFromCorporateRow2 = sumRow2;
                    this.totalReceivedFromHo = this.sumDepositedReceivedFromCorporateRow1 + this.sumDepositedReceivedFromCorporateRow2;
                    this.cheques_issued_old_reconciliation=this.sumDepositedReceivedFromCorporateRow1 + this.sumDepositedReceivedFromCorporateRow2;
                    this.calculateClosingBalance();

                }

            },
            watch: {
                loading: function (val) {

                },
                bankInterest: function (newVal) {
                    this.bankInterest = isNaN(newVal) ? 0 : parseFloat(newVal);
                    this.calculateClosingBalance();
                },
                bankCharge: function () {
                    this.calculateClosingBalance();
                },
                closingBalanceAsPerBankStatement: function () {
                    this.calculateClosingBalance();
                },
                discrepancy: function (value) {
                    this.disableSubmitBtn = value !== 0;
                }
            },
            filters: {
                money_format: function (x) {
                    return '৳ ' + parseFloat(x === '' ? 0 : x).toLocaleString();
                }
            }
        });

        let el = $(".datepicker9");
        el.datepicker({
            weekStart: el.data('week-start') || 0,
            autoclose: el.data('autoclose') || false,
            todayHighlight: el.data('today-highlight') || false,
            orientation: 'bottom' // Position issue when using BS4, set it to bottom until officially supported
        });

        //add required

        // $("input[required='required'], select").each(function () {
        //     $(this).before('<span class="required">*</span>');
        // });

        function printNow() {
            $("#printDiv").print({
                globalStyles: false,
                mediaPrint: true,
                stylesheet: null,
                noPrintSelector: ".no-print",
                iframe: true,
                append: null,
                prepend: null,
                manuallyCopyFormValues: true,
                timeout: 750,
                doctype: '<!doctype html>',
                deferred: $.Deferred()
            });
        }

        //add required

        // $("input[required='required'], select").each(function () {
        //     $(this).before('<span class="required">*</span>');
        // });
    </script>
@endsection
