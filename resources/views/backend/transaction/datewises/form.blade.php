<div class="row" >
    <div class="col-sm-6">
        <div class="form-group row has-feedback">
            {!! Form::label('trans_date', 'Transaction Date',['class' => 'col-lg-4 col-form-label']) !!}
            <div class="col-lg-8">
                {{--{!! Form::text('trans_date',$carbon->format('d-M-Y'),  ['class' => 'form-control','readonly']) !!}--}}
                {!! Form::text('trans_date',$carbon->format('d-M-Y'),  ['class' => 'form-control js-datepicker','data-week-start'=>"6", 'data-autoclose'=>"true", 'data-today-highlight'=>"true", 'data-date-format'=>"dd-M-yyyy", 'placeholder'=>"mm/dd/yy",'readonly']) !!}
                <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
                <div class="help-block with-errors"></div>
            </div>
        </div>
    </div>

    @if(count(access()->user()->branches)>1)
        <div class="col-sm-6">
            <div class="form-group row has-feedback">
                {!! Form::label('branch_name', 'Branch Name',['class' => 'col-lg-4 col-form-label']) !!}
                <div class="col-lg-8">
                    {!! Form::select('branch_id',$branches,old('branch_name'),  ['class' => 'form-control','readonly']) !!}
                    <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
                    <div class="help-block with-errors"></div>
                </div>
            </div>
        </div>
    @else
        <div class="col-sm-6">
            <div class="form-group row has-feedback">
                {!! Form::label('branch_name', 'Branch Name',['class' => 'col-lg-4 col-form-label']) !!}
                <div class="col-lg-8">
                    {!! Form::hidden('branch_id',$branches->id,  ['class' => 'form-control','readonly']) !!}
                    {!! Form::text('branch_name',$branches->name,  ['class' => 'form-control','readonly']) !!}
                    <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
                    <div class="help-block with-errors"></div>
                </div>
            </div>
        </div>
    @endif

</div>
<br>
<div class="row" style="overflow-x: scroll">
    <div class="col-lg-12">
        @component('includes.components.table',['class'=>'table  table-bordered fixed_headers','style'=> 'font-size:12px','id'=>''])
            @slot('thead')
                <th style="font-size: 12px">Brand</th>
                <th style="font-size: 12px">Collection</th>
                <th style="font-size: 12px">Money Receipt</th>
                <th style="font-size: 12px">Advance Receipt</th>
                <th style="font-size: 12px"
                    data-toggle="tooltip" data-placement="top"
                    data-html="true" title="Short/Excess Receipt"> Srt/Ex Receipt
                </th>
                <th style="font-size: 12px" data-toggle="tooltip" data-placement="top"
                    data-html="true" title="Advance Adjustment"> Adv Adjustment
                </th>
                <th style="font-size: 12px">AR Adjustment (+)</th>
                <th style="font-size: 12px">AR Adjustment (-)</th>
                <th style="font-size: 12px">Mr Reverse</th>
            @endslot

            @slot('tbody')
                <tr class="bg-gray-darker text-white">
                    <td style="font-weight: bold">
                        Total
                    </td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                </tr>
                @foreach($products as $t)
                    <tr class="bg-gray text-navy" style="font-weight: bold">
                        <td>
                            {{$t['brand_name']}}
                        </td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                    </tr>
                    @foreach($t['all'] as $product)
                        {{ Form::hidden('product['.$product->id.'][brand_id]', $product->brand_id) }}
                        {{ Form::hidden('product['.$product->id.'][brand_name]', $product->brand_name) }}
                            {{ Form::hidden('product['.$product->id.'][product_id]', $product->id) }}
                        {{ Form::hidden('product['.$product->id.'][product_name]', $product->name) }}

                        <tr>
                            <td width="150px"> {{ $product->name }}</td>
                            <td width="50px">
                                {{ Form::number('product['.$product->id.'][collection]', null, [
                                'class' => 'form-control1 text-right',
                                'placeholder' => '',
                                'min' => 0, 'step' => 'any'
                                ])
                               }}
                            </td>
                            <td width="50px">
                                {{ Form::number('product['.$product->id.'][money_receipt]', null, [
                                'class' => 'form-control1 text-right',
                                'placeholder' => '',
                                'min' => 0 , 'step' => 'any'
                                ])
                               }}
                            </td>
                            <td width="50px">
                                {{ Form::number('product['.$product->id.'][adv_receipt]', null, [
                                'class' => 'form-control1 text-right',
                                'placeholder' => '',
                                'min' => 0    , 'step' => 'any'
                                ])
                               }}
                            </td>
                            <td width="50px">
                                {{ Form::number('product['.$product->id.'][short_receipt]', null, [
                                'class' => 'form-control1 text-right',
                                'placeholder' => '','step' => 'any'
                                ])
                               }}
                            </td>
                            <td width="50px">
                                {{ Form::number('product['.$product->id.'][adv_adj]', null, [
                                'class' => 'form-control1 text-right',
                                'placeholder' => '',
                                'min' => 0     , 'step' => 'any'
                                ])
                               }}
                            </td>
                            <td width="50px">
                                {{ Form::number('product['.$product->id.'][adj_plus]', null, [
                                'class' => 'form-control1 text-right',
                                'placeholder' => '',
                                'min' => 0    , 'step' => 'any'
                                ])
                               }}
                            </td>
                            <td width="50px">
                                {{ Form::number('product['.$product->id.'][adj_minus]', null, [
                                'class' => 'form-control1 text-right',
                                'placeholder' => '',
                                'min' => 0    , 'step' => 'any'
                                ])
                               }}
                            </td>
                            <td width="50px">
                                {{ Form::number('product['.$product->id.'][mr_reverse]', null, [
                                'class' => 'form-control1 text-right',
                                'placeholder' => '',
                                'min' => 0    , 'step' => 'any'
                                ])
                               }}
                            </td>
                        </tr>
                    @endforeach
                @endforeach


            @endslot
        @endcomponent

        <br>

    </div>
</div>
<br><br>
