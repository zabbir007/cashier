@extends ('backend.layouts.app')

@section ('title', $page_heading)

@section('page-header')
    <h1>
        {{ $page_heading }}
    </h1>

@endsection

@section('content')
    <div class="row">
        <div class="col-md-12">
            @component('includes.components.widget')

                @slot('title')
                    {{ $page_heading }}
                @endslot

                @slot('action_buttons')
                    @if ($module_name_singular->hasPermission() && access()->allow('menu-transaction-date-wise-receipt-edit'))
                        <a href="{{ route($module_route.'.edit', $module_name_singular) }}"
                           class="btn btn-primary btn-block-option"><i
                                    class="si si-note" data-toggle="tooltip"
                                    data-placement="top"></i>&nbsp;{{ trans('buttons.general.crud.edit') }}</a>
                    @endif

                    <a href="{{ route($module_route.'.index') }}" class="btn btn-block-option"><i
                                class="si si-grid"></i>&nbsp;All Transactions
                    </a>
                    <button type="button" class="btn btn-success btn-block-option"
                            onclick="Codebase.helpers('print-page');">
                        <i class="si si-printer"></i> Print
                    </button>
                    <button type="button" class="btn btn-primary btn-block-option" data-toggle="block-option"
                            data-action="fullscreen_toggle"></button>
                    <button type="button" class="btn btn-primary btn-block-option" data-toggle="block-option"
                            data-action="state_toggle"
                            data-action-mode="demo">
                        <i class="si si-refresh"></i>
                    </button>
                @endslot
                {{ Form::model($module_name_singular, ['route' => [$module_route.'.update', $module_name_singular], 'class' => 'form-horizontal', 'role' => 'form', 'method' => 'PATCH']) }}

                <div class="row">
                    <div class="col-lg-8">
                        @component('includes.components.table',['class'=>'table table-borderless','style'=> 'font-size:14px','id'=>''])

                            @slot('tbody')
                                <tr>
                                    <td style="font-weight: bold">
                                        Transaction Date
                                    </td>
                                    <td> :</td>
                                    <td>{{ date('d-M-Y',strtotime($module_name_singular->trans_date)) }}</td>
                                    <td style="font-weight: bold">
                                        Branch Name
                                    </td>
                                    <td> :</td>
                                    <td>{{ $module_name_singular->branch_name }}</td>


                                </tr>
                            @endslot
                        @endcomponent
                    </div>
                    <div class="col-lg-12">
                        @component('includes.components.table',['class'=>'table  table-bordered fixed_headers','style'=>'font-size:12px','id'=>''])
                            @slot('thead')
                                <th style="font-size: 14px">Brand</th>
                                <th style="font-size: 14px">Collection</th>
                                <th style="font-size: 14px">Money Receipt</th>
                                <th style="font-size: 14px">Advance Receipt</th>
                                <th style="font-size: 14px"
                                    data-toggle="tooltip" data-placement="top"
                                    data-html="true" title="Short/Excess Receipt"> Srt/Ex Receipt
                                </th>
                                <th style="font-size: 14px" data-toggle="tooltip" data-placement="top"
                                    data-html="true" title="Advance Adjustment"> Adv Adjustment
                                </th>
                                <th style="font-size: 14px">Adjustment (+)</th>
                                <th style="font-size: 14px">Adjustment (-)</th>
                                <th style="font-size: 14px">Mr Reverse </th>
                            @endslot

                            @slot('tbody')
                                @php
                                    $dt = $module_name_singular->details;
                                @endphp
                                <tr class="bg-gray-darker text-white" style="font-weight: bold">
                                    <td>
                                        Total
                                    </td>
                                    <td class="text-right ">{{ number_format($dt->sum('collection'), 2) }} </td>
                                    <td class="text-right ">{{ number_format($dt->sum('money_receipt'),2) }} </td>
                                    <td class="text-right ">{{ number_format($dt->sum('adv_receipt'),2) }} </td>
                                    <td class="text-right ">{{ number_format($dt->sum('short_receipt'),2) }} </td>
                                    <td class="text-right ">{{ number_format($dt->sum('adv_adj'),2) }} </td>
                                    <td class="text-right ">{{ number_format($dt->sum('adj_plus'),2) }} </td>
                                    <td class="text-right ">{{ number_format($dt->sum('adj_minus'),2) }} </td>
                                    <td class="text-right ">{{ number_format($dt->sum('mr_reverse'),2) }} </td>

                                </tr>
                                @foreach($products as $t)

                                    <tr class="bg-gray text-navy" style="font-weight: bold">
                                        <td>
                                            {{$t["brand_name"]}}
                                        </td>

                                        <td class="text-right ">{{ number_format($dt->where('brand_id',$t["brand_id"])->sum('collection'),2)}} </td>
                                        <td class="text-right ">{{ number_format($dt->where('brand_id',$t["brand_id"])->sum('money_receipt'),2) }} </td>
                                        <td class="text-right ">{{ number_format($dt->where('brand_id',$t["brand_id"])->sum('adv_receipt'),2) }} </td>
                                        <td class="text-right ">{{ number_format($dt->where('brand_id',$t["brand_id"])->sum('short_receipt'),2) }} </td>
                                        <td class="text-right ">{{ number_format($dt->where('brand_id',$t["brand_id"])->sum('adv_adj'),2) }} </td>
                                        <td class="text-right ">{{ number_format($dt->where('brand_id',$t["brand_id"])->sum('adj_plus'),2) }} </td>
                                        <td class="text-right ">{{ number_format($dt->where('brand_id',$t["brand_id"])->sum('adj_minus'),2) }} </td>
                                        <td class="text-right ">{{ number_format($dt->where('brand_id',$t["brand_id"])->sum('mr_reverse'),2) }} </td>
                                    </tr>
                                    @foreach($t["all"] as $product)
                                        @php
                                            $details = $module_name_singular->details->where('product_id',$product->id);
                                     $dtSumColl = $dtSumMR = $dtSumAdvR = $dtSumSR = $dtSumAdvA = $dtSumAdjP = $dtSumAdjM = 0;

                                    if($details){
                                         $dtSumColl = $details->sum('collection') ;
                                         $dtSumMR = $details->sum('money_receipt');
                                         $dtSumAdvR = $details->sum('adv_receipt');
                                         $dtSumSR = $details->sum('short_receipt');
                                         $dtSumAdvA = $details->sum('adv_adj');
                                         $dtSumAdjP = $details->sum('adj_plus');
                                         $dtSumAdjM = $details->sum('adj_minus');
                                         $dtSumMrR = $details->sum('mr_reverse');
                                     }
                                        @endphp
                                        <tr class="text-right">
                                            <td width="150px" class="text-left"> {{ $product->name }}</td>
                                            <td width="50px">
                                                {{ number_format($dtSumColl,2) }}
                                            </td>
                                            <td width="50px">
                                                {{ number_format($dtSumMR,2) }}
                                            </td>
                                            <td width="50px">
                                                {{ number_format($dtSumAdvR,2) }}
                                            </td>
                                            <td width="50px">
                                                {{ number_format($dtSumSR,2) }}
                                            </td>
                                            <td width="50px">
                                                {{ number_format($dtSumAdvA,2) }}
                                            </td>
                                            <td width="50px">
                                                {{ number_format($dtSumAdjP,2) }}
                                            </td>
                                            <td width="50px">
                                                {{ number_format($dtSumAdjM,2) }}
                                            </td>
                                            <td width="50px">
                                                {{ number_format($dtSumMrR,2) }}
                                            </td>
                                        </tr>
                                    @endforeach
                                @endforeach
                            @endslot
                        @endcomponent

                        <br>

                    </div>
                </div>
                <br><br>
                {{ Form::close() }}
            @endcomponent
        </div><!-- END column -->
    </div><!-- .row -->
@endsection
