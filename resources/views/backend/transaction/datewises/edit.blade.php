@extends ('backend.layouts.app')

@section ('title', $page_heading)

@section('page-header')
    <h1>
        {{ $page_heading }}
    </h1>
@endsection

@section('content')
    @component('includes.components.widget')
        @slot('title')
            {{ $page_heading }}
        @endslot

        @slot('action_buttons')
            <div class="btn-group float-right">
                @if(access()->allow('menu-transaction-date-wise-receipt-show'))
                    <a href="{{ route('admin.transaction.datewises.show', $module_name_singular) }}"
                       class="btn btn-alt-primary btn-sm" data-toggle="click-ripple"><i class="si si-eye"></i>&nbsp;View</a>
                @endif
                <a href="{{ route($module_route.'.index') }}" class="btn  btn-alt-primary btn-sm"
                   data-toggle="click-ripple"><i class="si si-grid"></i>&nbsp;All Transactions</a>
                <a href="{{ route($module_route.'.create') }}" class="btn  btn-alt-success btn-sm"
                   data-toggle="click-ripple"><i class="si si-plus"></i>&nbsp;Add New</a>
            </div><!--pull right-->

        @endslot

        {{ Form::model($module_name_singular, ['route' => [$module_route.'.update', $module_name_singular], 'class' => 'form-horizontal', 'role' => 'form', 'method' => 'PATCH']) }}

        <div class="row">
            <div class="col-sm-6">
                <div class="form-group row has-feedback">
                    {!! Form::label('trans_date', 'Transaction Date',['class' => 'col-lg-4 col-form-label']) !!}
                    <div class="col-lg-8">
                        {!! Form::hidden('sl', $module_name_singular->sl, ['class' => 'form-control', 'placeholder'=>'XXXX-XXXX','readonly']) !!}
                        {!! Form::text('trans_date',date('d-M-Y',strtotime($module_name_singular->trans_date)),  ['class' => 'form-control','readonly']) !!}
                        <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
                        <div class="help-block with-errors"></div>
                    </div>
                </div>
            </div>


            <div class="col-sm-6">
                <div class="form-group row has-feedback">
                    {!! Form::label('branch_name', 'Branch Name',['class' => 'col-lg-4 col-form-label']) !!}
                    <div class="col-lg-8">
                        {!! Form::hidden('branch_id',$module_name_singular->branch_id,  ['class' => 'form-control','readonly']) !!}
                        {!! Form::text('branch_name',$module_name_singular->branch_name,  ['class' => 'form-control','readonly']) !!}
                        <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
                        <div class="help-block with-errors"></div>
                    </div>
                </div>
            </div>


        </div>
        <br>
        <div class="row" style="overflow-x: scroll">
            <div class="col-lg-12">
                @component('includes.components.table',['class'=>'table  table-bordered fixed_headers','style'=> 'font-size:12px','id'=>''])
                    @slot('thead')
                        <th style="font-size: 14px">Brand</th>
                        <th style="font-size: 14px">Collection</th>
                        <th style="font-size: 14px">Money Receipt</th>
                        <th style="font-size: 14px">Advance Receipt</th>
                        <th style="font-size: 14px"
                            data-toggle="tooltip" data-placement="top"
                            data-html="true" title="Short/Excess Receipt"> Srt/Ex Receipt
                        </th>
                        <th style="font-size: 14px" data-toggle="tooltip" data-placement="top"
                            data-html="true" title="Advance Adjustment"> Adv Adjustment
                        </th>
                        <th style="font-size: 14px">Adjustment (+)</th>
                        <th style="font-size: 14px">Adjustment (-)</th>
                        <th style="font-size: 14px">Mr Reverse</th>
                    @endslot

                    @slot('tbody')
                        @php
                            $details = collect($module_name_singular->details);
                        @endphp
                        <tr class="bg-gray-darker text-white" style="font-weight: bold">
                            <td>
                                Total
                            </td>

                            <td class="text-right ">{{ number_format($details->sum('collection'), 2, '.', '')}}</td>
                            <td class="text-right ">{{ number_format($details->sum('money_receipt'), 2, '.', '')}}</td>
                            <td class="text-right ">{{ number_format($details->sum('adv_receipt'), 2, '.', '')}}</td>
                            <td class="text-right ">{{ number_format($details->sum('short_receipt'), 2, '.', '') }}</td>
                            <td class="text-right ">{{ number_format($details->sum('adv_adj'), 2, '.', '')}}</td>
                            <td class="text-right ">{{ number_format($details->sum('adj_plus'), 2, '.', '')}}</td>
                            <td class="text-right ">{{ number_format($details->sum('adj_minus'), 2, '.', '')}}</td>
                            <td class="text-right ">{{ number_format($details->sum('mr_reverse'), 2, '.', '')}}</td>
                        </tr>
                        @foreach($products as $t)
                            <tr class="bg-gray text-navy" style="font-weight: bold">
                                <tr class="bg-gray text-navy" style="font-weight: bold;">
                                <td>
                                    {{ array_get($t,'brand_name')}}
                                </td>
                                <td class="text-right ">{{ number_format($details->where('brand_id',$t["brand_id"])->sum('collection'), 2, '.', '')}}</td>
                                <td class="text-right ">{{ number_format($details->where('brand_id',$t["brand_id"])->sum('money_receipt'), 2, '.', '')}}</td>
                                <td class="text-right ">{{ number_format($details->where('brand_id',$t["brand_id"])->sum('adv_receipt'), 2, '.', '') }}</td>
                                <td class="text-right ">{{ number_format($details->where('brand_id',$t["brand_id"])->sum('short_receipt'), 2, '.', '') }}</td>
                                <td class="text-right ">{{ number_format($details->where('brand_id',$t["brand_id"])->sum('adv_adj'), 2, '.', '')}}</td>
                                <td class="text-right ">{{ number_format($details->where('brand_id',$t["brand_id"])->sum('adj_plus'), 2, '.', '')}}</td>
                                <td class="text-right ">{{ number_format($details->where('brand_id',$t["brand_id"])->sum('adj_minus'), 2, '.', '')}}</td>
                                <td class="text-right ">{{ number_format($details->where('brand_id',$t["brand_id"])->sum('mr_reverse'), 2, '.', '')}}</td>
                            </tr>

                                @foreach(array_get($t,'all') as $product)
                                @php
                                    $detail = $details->where('product_id', $product->id);

                                     // dd($detail);
                                     $dtSumColl = $dtSumMR = $dtSumAdvR = $dtSumSR = $dtSumAdvA = $dtSumAdjP = $dtSumAdjM = 0;
                                     if($detail){
                                         $dtSumColl = $detail->sum('collection') ;
                                         $dtSumMR = $detail->sum('money_receipt');
                                         $dtSumAdvR = $detail->sum('adv_receipt');
                                         $dtSumSR = $detail->sum('short_receipt');
                                         $dtSumAdvA = $detail->sum('adv_adj');
                                         $dtSumAdjP = $detail->sum('adj_plus');
                                         $dtSumAdjM = $detail->sum('adj_minus');
                                         $dtSumMrR = $detail->sum('mr_reverse');
                                     }

                                @endphp
                                    {{ Form::hidden('product['.$product->id.'][brand_id]', $product->brand_id) }}
                                    {{ Form::hidden('product['.$product->id.'][brand_name]', $product->brand_name) }}
                                    {{ Form::hidden('product['.$product->id.'][product_id]', $product->id) }}
                                    {{ Form::hidden('product['.$product->id.'][product_name]', $product->name) }}

                                <tr>
                                    <td width="150px"> {{ $product->name }}</td>
                                    <td width="50px">
                                        {{ Form::number("product[".$product->id."][collection]", $dtSumColl, ['class' => 'form-control1 text-right',
                                'min' => 0 , 'step' => 'any'])}}
                                    </td>
                                    <td width="50px">
                                        {{ Form::number("product[".$product->id."][money_receipt]", $dtSumMR, [
                                        'class' => 'form-control1 text-right',
                                'min' => 0 , 'step' => 'any'
                                        ])
                                       }}
                                    </td>
                                    <td width="50px">
                                        {{ Form::number("product[".$product->id."][adv_receipt]", $dtSumAdvR, [
                                        'class' => 'form-control1 text-right' ,
                                'min' => 0 , 'step' => 'any'
                                        ])
                                       }}
                                    </td>
                                    <td width="50px">
                                        {{ Form::number("product[".$product->id."][short_receipt]", $dtSumSR, [
                                        'class' => 'form-control1 text-right','step' => 'any'
                                        ])
                                       }}
                                    </td>
                                    <td width="50px">
                                        {{ Form::number("product[".$product->id."][adv_adj]", $dtSumAdvA, [
                                        'class' => 'form-control1 text-right',
                                'min' => 0 , 'step' => 'any'
                                        ])
                                       }}
                                    </td>
                                    <td width="50px">
                                        {{ Form::number("product[".$product->id."][adj_plus]", $dtSumAdjP, [
                                        'class' => 'form-control1 text-right',
                                'min' => 0 , 'step' => 'any'
                                        ])
                                       }}
                                    </td>
                                    <td width="50px">
                                        {{ Form::number("product[".$product->id."][adj_minus]", $dtSumAdjM, [
                                        'class' => 'form-control1 text-right',
                                'min' => 0 , 'step' => 'any'
                                        ])
                                       }}
                                    </td>
                                    <td width="50px">
                                        {{ Form::number("product[".$product->id."][mr_reverse]", $dtSumMrR, [
                                        'class' => 'form-control1 text-right',
                                'min' => 0 , 'step' => 'any'
                                        ])
                                       }}
                                    </td>
                                </tr>
                            @endforeach

                        @endforeach

                    @endslot
                @endcomponent

                <br>

            </div>
        </div>

        <div class="row">
            <div class="col-lg-6">
                @if($module_name_singular->hasPermission())
                    {!! Form::button(('<i class="fa fa-check fa-fw"></i>&nbsp;'.$module_action), ['class' => 'btn btn-success pull-right','type'=>'submit']) !!}
                @endif
            </div>

            <div class="col-lg-6">
                {!! link_to(route($module_route.'.index'), ('<i class="fa fa-mail-reply"></i>&nbsp;'.trans('buttons.general.cancel')), ['class' => 'btn btn-danger pull-left'], null, false) !!}
            </div><!--col-lg-1-->
        </div><!--form control-->
        <br><br>
        {{ Form::close() }}

    @endcomponent
@endsection
