@extends ('backend.layouts.app')

@section ('title', $page_heading)

@section('after-styles')
    <style type="text/css">
        table td, table tr{
            white-space: nowrap;
        }
        div.dataTables_filter {
            text-align: left !important;
        }

        .dataTables_wrapper .dataTables_processing {
            position: absolute;
            top: 50%;
            left: 50%;
            width: 300px;
            height: 60px;
            margin-left: -50%;
            margin-top: -25px;
            padding-top: 20px;
            text-align: center;
            font-size: 1.2em;
            border: 1px solid rgb(51, 51, 51) !important;
            background: #eeeeee linear-gradient(to right, rgba(255, 255, 255, 0) 0%, rgba(255, 255, 255, 0.9) 25%, rgba(255, 255, 255, 0.9) 75%, rgba(255, 255, 255, 0) 100%)  !important;
        }
    </style>
@endsection

@section('content')
    <div class="row">
        <!-- DOM dataTable -->
        <div class="col-md-12">
            @component('includes.components.widget')

                @slot('title')
                    {{ $page_heading }}
                @endslot

                @slot('action_buttons')
                    @include($module_view.'.header-buttons')
                @endslot

                {{--Widget body start--}}
                @component('includes.components.table',['class'=>'table table-condensed table-hover table-striped table-bordered','id'=>'datatable'])
                    @slot('thead')

                        <th style="font-size: 14px">Date</th>
                        <th style="font-size: 14px">SL</th>
                        <th style="font-size: 14px">Branch</th>
                        <th style="font-size: 14px">Total Amount</th>
                        <th>{{ __('labels.general.actions') }}</th>
                    @endslot

                    @slot('tbody')
                        {{--@foreach($$module_name as $module_name_singular)--}}
                            {{--<tr class= {{ $module_name_singular->isApprove() ? 'bg-success-light' : '' }}>--}}
                                {{--<td> {{ date('d-M-Y',strtotime($module_name_singular->trans_date)) }}</td>--}}
                                {{--<td class="text-left"--}}
                                    {{--style="font-weight: bold; color: #0d89ed">{{ $module_name_singular->sl }}</td>--}}
                                {{--<td> {{ $module_name_singular->branch_name }}</td>--}}
                                {{--<td class="text-right"> {{ number_format($module_name_singular->details->sum('amount'),2) }}</td>--}}
                                {{--<td> {!! $module_name_singular->action_buttons !!}</td>--}}
                            {{--</tr>--}}

                        {{--@endforeach--}}
                    @endslot
                @endcomponent

            @endcomponent
        </div><!-- END column -->
    </div><!-- .row -->
@endsection

@section('after-scripts')
    <script type="text/javascript">
        $(function () {
            $('#datatable').DataTable({
                // dom: "<row  lfrtip",
                dom: "<'row'<'col-sm-12 col-md-4 text-left'f><'col-sm-12 col-md-4'><'col-sm-12 col-md-4 text-right'l>>" +
                    "<'row'<'col-sm-12'tr>>" +
                    "<'row'<'col-sm-12 col-md-5'i><'col-sm-12 col-md-7'p>>",
                language: {
                    'loadingRecords': '&nbsp;',
                    'processing': '<i class="fa fa-spinner fa-spin fa-2x fa-fw"></i>Loading...',
                },
                fixedHeader: true,
                pageLength: 25,
                processing: true,
                serverSide: true,
                autoWidth: false,
                ajax: {
                    url: '{{ route($module_route.'.get') }}',
                    type: 'post',
                    data: {status: 1, trashed: false},
                    error: function (xhr, err) {
                        if (err === 'parsererror')
                            location.reload();
                    }
                },
                columns: [
                    {data: 'sl', searchable: true, sortable: true},
                    {data: 'trans_date', searchable: true, sortable: true},
                    {data: 'branch_name', searchable: true, sortable: true},
                    {data: 'amount', searchable: false, sortable: false, className: 'text-right'},
                    {data: 'actions', searchable: false, sortable: false}
                ],
                order: [
                    [1, "desc"],
                    [2, "asc"],
                ]
            });
        });
    </script>
@endsection
