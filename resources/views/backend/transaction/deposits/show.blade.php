@extends ('backend.layouts.app')

@section ('title', $page_heading)

@section('page-header')
    <h1>
        {{ $page_heading }}
    </h1>

@endsection

@section('content')
    <div class="row">
        <div class="col-md-12">
            @component('includes.components.widget')

                @slot('title')
                    {{ $page_heading }}
                @endslot

                @slot('action_buttons')
                    @if($module_name_singular->hasPermission() && access()->allow('menu-transaction-deposits-edit'))
                        <a href="{{ route($module_route.'.edit', $module_name_singular) }}" class="btn-block-option"><i
                                    class="si si-note"></i>&nbsp;Edit</a>
                    @endif

                    <a href="{{ route($module_route.'.index') }}" class="btn-block-option"><i class="si si-grid"></i>&nbsp;All {{ title_case($module_name) }}
                    </a>
                    <button type="button" class="btn-block-option" onclick="Codebase.helpers('print-page');">
                        <i class="si si-printer"></i> Print Invoice
                    </button>
                    <button type="button" class="btn-block-option" data-toggle="block-option"
                            data-action="fullscreen_toggle"></button>
                    <button type="button" class="btn-block-option" data-toggle="block-option" data-action="state_toggle"
                            data-action-mode="demo">
                        <i class="si si-refresh"></i>
                    </button>
                @endslot
                {{ Form::model($module_name_singular, ['route' => [$module_route.'.update', $module_name_singular], 'class' => 'form-horizontal', 'role' => 'form', 'method' => 'PATCH']) }}

                <div class="row">
                    <div class="col-lg-8">
                        @component('includes.components.table',['class'=>'table table-borderless','style'=> 'font-size:14px','id'=>''])
                            @slot('tbody')
                                <tr>
                                    <td style="font-weight: bold">Transaction Date</td>
                                    <td> :</td>
                                    <td>{{ date('d-M-Y',strtotime($module_name_singular->trans_date)) }}</td>
                                    <td style="font-weight: bold">Branch Name</td>
                                    <td> :</td>
                                    <td>{{ $module_name_singular->branch_name }}</td>
                                </tr>
                            @endslot
                        @endcomponent
                    </div>
                    <div class="col-lg-12">
                        @component('includes.components.table',['class'=>'table  table-bordered fixed_headers','style'=> 'font-size:12px','id'=>''])
                            @slot('thead')
                                <th style="font-size: 14px">Brand</th>
                                @foreach($banks as $bank)
                                    <th style="font-size: 14px"
                                        data-toggle="tooltip" data-placement="top"
                                        data-html="true" title="{{$bank->name}}"> {{$bank->short_name}}
                                    </th>
                                @endforeach
                            @endslot

                            @slot('tbody')
                                <tr class="bg-gray-darker text-white">
                                    <td style="font-weight: bold">Total</td>
                                    @foreach($banks as $bank)
                                        <td class="text-right">{{ number_format($module_name_singular->details->where('bank_id', $bank->id)->sum('amount'), 2) }}</td>
                                    @endforeach
                                </tr>
                                @foreach($products as $t)
                                    <tr class="bg-gray text-navy" style="font-weight: bold">
                                        <td>{{ array_get($t,'brand_name')}}</td>
                                        @foreach($banks as $bank)
                                            <td class="text-right">{{ number_format($module_name_singular->details->where('bank_id', $bank->id)->where('brand_id', array_get($t,'brand_id'))->sum('amount'), 2) }}</td>
                                        @endforeach

                                    </tr>

                                    @foreach(array_get($t, 'all') as $product)
                                        <tr>
                                            <td width="150px"> {{ $product->name }}</td>
                                            @foreach($banks as $bank)
                                                @php
                                                    $details = $module_name_singular->details->where('product_id',$product->id)->where('bank_id',$bank->id);
                                                @endphp
                                                <td width="50px" class="text-right">
                                                    <span>{{ number_format($details->sum('amount'),2) }}</span>
                                                </td>
                                            @endforeach
                                        </tr>
                                    @endforeach
                                @endforeach

                            @endslot
                        @endcomponent
                        <br>

                    </div>
                </div>
                <br><br>
                {{ Form::close() }}
            @endcomponent
        </div><!-- END column -->
    </div><!-- .row -->
@endsection
