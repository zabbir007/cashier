<div class="row">
    <div class="col-sm-6">
        <div class="form-group row has-feedback">
            {!! Form::label('trans_date', 'Transaction Date',['class' => 'col-lg-4 col-form-label']) !!}
            <div class="col-lg-8">
                {{--{!! Form::text('trans_date',$carbon->format('d-M-Y'),  ['class' => 'form-control','readonly']) !!}--}}
                {!! Form::text('trans_date',$carbon->format('d-M-Y'),  ['class' => 'form-control js-datepicker','data-week-start'=>'6', 'data-autoclose'=>'true', 'data-today-highlight'=>'true', 'data-date-format'=>'dd-M-yyyy', 'placeholder'=>'mm/dd/yy','readonly']) !!}
                <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
                <div class="help-block with-errors"></div>
            </div>
        </div>
    </div>

    @if(count(access()->user()->branches)>1)
        <div class="col-sm-6">
            <div class="form-group row has-feedback">
                {!! Form::label('branch_name', 'Branch Name',['class' => 'col-lg-4 col-form-label']) !!}
                <div class="col-lg-8">
                    {!! Form::select('branch_id',$branches,old('branch_name'),  ['class' => 'form-control','readonly']) !!}
                    <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
                    <div class="help-block with-errors"></div>
                </div>
            </div>
        </div>
    @else
        <div class="col-sm-6">
            <div class="form-group row has-feedback">
                {!! Form::label('branch_name', 'Branch Name',['class' => 'col-lg-4 col-form-label']) !!}
                <div class="col-lg-8">
                    {!! Form::hidden('branch_id',$branches->id,  ['class' => 'form-control','readonly']) !!}
                    {!! Form::text('branch_name',$branches->name,  ['class' => 'form-control','readonly']) !!}
                    <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
                    <div class="help-block with-errors"></div>
                </div>
            </div>
        </div>
    @endif

</div>
<br>
<div class="row">
    <div class="col-lg-12">
        @component('includes.components.table',['class'=>'table table-responsive table-bordered fixed_headers','style'=> 'font-size:12px','id'=>''])
            @slot('thead')
                <th style="font-size: 14px">Brand</th>
                @foreach($banks as $bank)
                    <th style="font-size: 14px"
                        data-toggle="tooltip" data-placement="top"
                        data-html="true" title="{{ $bank->name }}">
                        {{ $bank->short_name }}
                    </th>
                @endforeach
            @endslot

            @slot('tbody')
                <tr class="bg-gray-darker text-white">
                    <td style="font-weight: bold">Total</td>
                    @php
                        $count = $banks->count();
                    @endphp

                    @for ($i = 1; $i <= ($count); $i++)
                        <td></td>
                    @endfor
                </tr>
                @foreach($products as $t)
                    <tr class="bg-gray text-navy" style="font-weight: bold">
                        <td>{{array_get($t, 'brand_name')}}</td>
                        @for ($i = 1; $i <= ($count); $i++)
                            <td></td>
                        @endfor
                    </tr>
                    @foreach(array_get($t, 'all') as $product)
                        <tr>
                            <td width="150px"> {{ $product->name }}</td>
                            @foreach($banks as $bank)
                                {{ Form::hidden('data['.$product->id.']['.$loop->iteration.'][brand_id]', array_get($t, 'brand_id')) }}
                                {{ Form::hidden('data['.$product->id.']['.$loop->iteration.'][brand_name]', array_get($t, 'brand_name')) }}

                                {{ Form::hidden('data['.$product->id.']['.$loop->iteration.'][product_id]', $product->id) }}
                                {{ Form::hidden('data['.$product->id.']['.$loop->iteration.'][product_name]', $product->name) }}

                                {{ Form::hidden('data['.$product->id.']['.$loop->iteration.'][bank_id]', $bank->id) }}
                                {{ Form::hidden('data['.$product->id.']['.$loop->iteration.'][bank_name]', $bank->name) }}

                                <td width="50px">
                                    {{ Form::number('data['.$product->id.']['.$loop->iteration.'][amount]', null, [
                                    'class' => 'form-control1 text-right',
                                    'placeholder' => '',
                                    'min' => 0,
                                    'step' => 'any'
                                    ])
                                   }}
                                </td>
                            @endforeach
                        </tr>
                    @endforeach
                @endforeach

            @endslot
        @endcomponent

        <br>

    </div>
</div>
<br><br>
