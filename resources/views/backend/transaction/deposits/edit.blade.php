@extends ('backend.layouts.app')

@section ('title', $page_heading)

@section('page-header')
    <h1>
        {{ $page_heading }}
    </h1>
@endsection

@section('content')
    @component('includes.components.widget')
        @slot('title')
            {{ $page_heading }}
        @endslot

        @slot('action_buttons')
            <div class="btn-group float-right">
                @if(access()->allow('menu-transaction-deposits-show'))
                    <a href="{{ route($module_route.'.show', $module_name_singular) }}"
                       class="btn  btn-alt-primary btn-sm" data-toggle="click-ripple"><i class="si si-eye"></i>&nbsp;View</a>
                @endif

                <a href="{{ route($module_route.'.index') }}" class="btn  btn-alt-primary btn-sm"
                   data-toggle="click-ripple"><i class="si si-grid"></i>&nbsp;All Transactions</a>
                <a href="{{ route($module_route.'.create') }}" class="btn  btn-alt-success btn-sm"
                   data-toggle="click-ripple"><i class="si si-plus"></i>&nbsp;Add New</a>
            </div><!--pull right-->
        @endslot

        {{ Form::model($module_name_singular, ['route' => [$module_route.'.update', $module_name_singular], 'class' => 'form-horizontal', 'role' => 'form', 'method' => 'PATCH']) }}

        <div class="row">
            <div class="col-sm-6">
                <div class="form-group row has-feedback">
                    {!! Form::label('trans_date', 'Transaction Date',['class' => 'col-lg-5 col-form-label']) !!}
                    <div class="col-lg-7">
                        {!! Form::hidden('trans_sl', $module_name_singular->trans_sl, ['class' => 'form-control', 'placeholder'=>'XXXX-XXXX','readonly']) !!}
                        {!! Form::text('trans_date',date('d-M-Y',strtotime($module_name_singular->trans_date)),  ['class' => 'form-control','readonly']) !!}
                        <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
                        <div class="help-block with-errors"></div>
                    </div>
                </div>
            </div>

            <div class="col-sm-6">
                <div class="form-group row has-feedback">
                    {!! Form::label('branch_name', 'Branch Name',['class' => 'col-lg-5 col-form-label']) !!}
                    <div class="col-lg-7">
                        {!! Form::hidden('branch_id',$module_name_singular->branch_id,  ['class' => 'form-control','readonly']) !!}
                        {!! Form::text('branch_name',$module_name_singular->branch_name,  ['class' => 'form-control','readonly']) !!}
                        <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
                        <div class="help-block with-errors"></div>
                    </div>
                </div>
            </div>
        </div>
        <br>
        <div class="row">
            <div class="col-lg-12">
                @component('includes.components.table',['class'=>'table table-responsive table-bordered fixed_headers','style'=> 'font-size:12px','id'=>''])
                    @slot('thead')
                        <th style="font-size: 14px">Brand</th>
                        @foreach($banks as $bank)
                            <th style="font-size: 14px"
                                data-toggle="tooltip" data-placement="top"
                                data-html="true" title="{{$bank->name}}">&nbsp;{{ $bank->short_name }}
                            </th>
                        @endforeach
                    @endslot

                    @slot('tbody')
                        <tr class="bg-gray-darker text-white">
                            <td style="font-weight: bold">Total</td>
                            @foreach($banks as $bank)
                                <td class="text-right">{{ number_format($module_name_singular->details->where('bank_id', $bank->id)->sum('amount'), 2) }}</td>
                            @endforeach

                        </tr>
                        @foreach($products as $t)
                            <tr class="bg-gray text-navy" style="font-weight: bold">
                                <td>{{ array_get($t, 'brand_name') }}</td>
                                @foreach($banks as $bank)
                                    <td class="text-right">{{ number_format($module_name_singular->details->where('bank_id', $bank->id)->where('brand_id', $t['brand_id'])->sum('amount'), 2) }}</td>
                                @endforeach

                            </tr>
                            @foreach(array_get($t, 'all') as $product)

                                <tr>
                                    <td width="150px"> {{ $product->name }}</td>
                                    @foreach($banks as $bank)
                                        @php
                                            $details = $module_name_singular->details->where('product_id',$product->id)->where('bank_id',$bank->id);
                                        @endphp
                                        <td width="50px">
                                            {{ Form::hidden('data['.$product->id.']['.$loop->iteration.'][brand_id]', array_get($t, 'brand_id')) }}
                                            {{ Form::hidden('data['.$product->id.']['.$loop->iteration.'][brand_name]', array_get($t, 'brand_name')) }}
                                            {{ Form::hidden('data['.$product->id.']['.$loop->iteration.'][product_id]', $product->id) }}
                                            {{ Form::hidden('data['.$product->id.']['.$loop->iteration.'][product_name]', $product->name) }}
                                            {{ Form::hidden('data['.$product->id.']['.$loop->iteration.'][bank_id]', $bank->id) }}
                                            {{ Form::hidden('data['.$product->id.']['.$loop->iteration.'][bank_name]', $bank->name) }}

                                            {{ Form::number('data['.$product->id.']['.$loop->iteration.'][amount]',                                             $details->sum('amount'), [
                                            'class' => 'form-control1 text-right',
                                            'placeholder' => '',
                                            'min' => 0,
                                            'step' => 'any'
                                            ])
                                           }}
                                        </td>
                                    @endforeach
                                </tr>
                            @endforeach
                        @endforeach

                    @endslot
                @endcomponent

                <br>

            </div>
        </div>
        <br>

        <div class="row">
            <div class="col-lg-6">
                @if($module_name_singular->hasPermission())
                    {!! Form::button(('<i class="fa fa-check fa-fw"></i>&nbsp;'.$module_action), ['class' => 'btn btn-success pull-right','type'=>'submit']) !!}
                @endif
            </div>

            <div class="col-lg-6">
                {!! link_to(route($module_route.'.index'), ('<i class="fa fa-mail-reply"></i>&nbsp;'.trans('buttons.general.cancel')), ['class' => 'btn btn-danger pull-left'], null, false) !!}
            </div><!--col-lg-1-->
        </div><!--form control-->
        <br><br>
        {{ Form::close() }}

    @endcomponent
@endsection
