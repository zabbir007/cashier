@extends ('backend.layouts.app')

@section ('title', $page_heading)

@section('page-header')
    <h1>
        {{ $page_heading }}
    </h1>

@endsection

@section('content')
    <div class="row">
        <div class="col-md-12">
            @component('includes.components.widget')

                @slot('title')
                    {{ $page_heading }}
                @endslot

                @slot('action_buttons')
                    @if($module_name_singular->hasPermission() && access()->allow('menu-transaction-petty-cash-edit'))
                        <a href="{{ route($module_route.'.edit',$module_name_singular) }}" class="btn-block-option"><i
                                    class="si si-note"></i>&nbsp;Edit</a>
                    @endif

                    <a href="{{ route($module_route.'.index') }}" class="btn-block-option"><i class="si si-grid"></i>&nbsp;All {{ title_case($module_name) }}
                    </a>
                    <button type="button" class="btn-block-option" onclick="Codebase.helpers('print-page');">
                        <i class="si si-printer"></i> Print
                    </button>
                    <button type="button" class="btn-block-option" data-toggle="block-option"
                            data-action="fullscreen_toggle"></button>
                    <button type="button" class="btn-block-option" data-toggle="block-option" data-action="state_toggle"
                            data-action-mode="demo">
                        <i class="si si-refresh"></i>
                    </button>
                @endslot
                {{ Form::model($module_name_singular, ['route' => [$module_route.'.update', $module_name_singular], 'class' => 'form-horizontal', 'role' => 'form', 'method' => 'PATCH']) }}

                <div class="row">
                    <div class="col-lg-7">
                        @component('includes.components.table',['class'=>'table table-borderless','style'=> 'font-size:14px','id'=>''])
                            @slot('tbody')
                                <tr>
                                    <td style="font-weight: bold">
                                        Transaction Date
                                    </td>
                                    <td> :</td>
                                    <td>{{ date('d-M-Y',strtotime($module_name_singular->trans_date)) }}</td>
                                    <td style="font-weight: bold">
                                        Branch Name
                                    </td>
                                    <td> :</td>
                                    <td>{{ $module_name_singular->branch_name }}</td>
                                </tr>
                            @endslot
                        @endcomponent
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-7">
                        @component('includes.components.table',['class'=>'table','id'=>''])

                            @slot('tbody')
                                <tr>
                                    <td class="text-left"> Receipt From HQ</td>
                                    <td class="text-right">
                                        <strong>{{ $module_name_singular->details->sum('total_receipt_hq') }} </strong>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="text-left"> Transfer to Imprest Cash</td>
                                    <td class="text-right">
                                        <strong>{{ $module_name_singular->details->sum('transfer_imprest_cash') }} </strong>
                                    </td>
                                <tr>
                                </tr>
                                <td class="text-left"> Misc. Receipt</td>
                                <td class="text-right">
                                    <strong>{{ $module_name_singular->details->sum('m_receipt') }} </strong></td>
                                <tr>
                                </tr>
                                <td class="text-left"> Expenses Through CQ</td>
                                <td class="text-right">
                                    <strong>{{ $module_name_singular->details->sum('total_expense_cq') }} </strong></td>
                                <tr>
                                </tr>
                                <td class="text-left"> Expenses Through Cash</td>
                                <td class="text-right">
                                    <strong>{{ $module_name_singular->details->sum('total_exp') }} </strong></td>
                                </tr>
                            @endslot
                        @endcomponent
                        <br>
                    </div>
                </div>

                {{ Form::close() }}
            @endcomponent
        </div><!-- END column -->
    </div><!-- .row -->
@endsection
