@extends ('backend.layouts.app')

@section ('title', $page_heading)

@section('page-header')
    <h1>
        {{ $page_heading }}
    </h1>
@endsection

@section('content')
    @component('includes.components.widget')
        @slot('title')
            {{ $page_heading }}
        @endslot

        @slot('action_buttons')
            <div class="btn-group float-right">
                @if(access()->allow('menu-transaction-petty-cash-show'))
                    <a href="{{ route($module_route.'.show', $module_name_singular) }}"
                       class="btn  btn-alt-primary btn-sm" data-toggle="click-ripple"><i
                            class="si si-eye"></i>&nbsp;{{ trans('buttons.general.crud.view') }}</a>
                @endif

                <a href="{{ route($module_route.'.index') }}" class="btn  btn-alt-primary btn-sm"
                   data-toggle="click-ripple"><i class="si si-grid"></i>&nbsp;All Transactions</a>
                <a href="{{ route($module_route.'.create') }}" class="btn  btn-alt-success btn-sm"
                   data-toggle="click-ripple"><i class="si si-plus"></i>&nbsp;Add New</a>
            </div><!--pull right-->

        @endslot

        {{ Form::model($module_name_singular, ['route' => [$module_route.'.update', $module_name_singular], 'class' => 'form-horizontal', 'role' => 'form', 'method' => 'PATCH']) }}

        <div class="row">
            <div class="col-sm-6">
                <div class="form-group row has-feedback">
                    {!! Form::label('trans_date', 'Transaction Date',['class' => 'col-lg-5 col-form-label']) !!}
                    <div class="col-lg-7">
                        {!! Form::hidden('trans_sl', $module_name_singular->trans_sl, ['class' => 'form-control', 'placeholder'=>'XXXX-XXXX','readonly']) !!}
                        {!! Form::text('trans_date',date('d-M-Y',strtotime($module_name_singular->trans_date)),  ['class' => 'form-control','readonly']) !!}
                        <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
                        <div class="help-block with-errors"></div>
                    </div>
                </div>
            </div>


            <div class="col-sm-6">
                <div class="form-group row has-feedback">
                    {!! Form::label('branch_name', 'Branch Name',['class' => 'col-lg-5 col-form-label']) !!}
                    <div class="col-lg-7">
                        {!! Form::hidden('branch_id',$module_name_singular->branch_id,  ['class' => 'form-control','readonly']) !!}
                        {!! Form::text('branch_name',$module_name_singular->branch_name,  ['class' => 'form-control','readonly']) !!}
                        <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
                        <div class="help-block with-errors"></div>
                    </div>
                </div>
            </div>


        </div>

        <div class="row">

            <div class="col-sm-6">
                <div class="form-group row has-feedback">
                    {!! Form::label('trans_date', 'Total Receipt From HQ',['class' => 'col-lg-5 col-form-label']) !!}
                    <div class="col-lg-7">
                        {!! Form::number(' total_receipt_hq',$module_name_singular->details->sum('total_receipt_hq'),  ['class' => 'form-control text-right','min' => 0,'step' => 'any']) !!}
                        <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
                        <div class="help-block with-errors"></div>
                    </div>
                </div>

                <div class="form-group row has-feedback">
                    {!! Form::label('transfer_imprest_cash', 'Transfer to Imprest Cash (C)',['class' => 'col-lg-5 col-form-label']) !!}
                    <div class="col-lg-7">
                        {!! Form::number(' transfer_imprest_cash',$module_name_singular->details->sum('transfer_imprest_cash'),  ['class' => 'form-control text-right','min' => 0,'step' => 'any']) !!}
                        <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
                        <div class="help-block with-errors"></div>
                    </div>
                </div>

                <div class="form-group row has-feedback">
                    {!! Form::label('m_receipt', 'Miss. Receipt',['class' => 'col-lg-5 col-form-label']) !!}
                    <div class="col-lg-7">
                        {!! Form::number('m_receipt',$module_name_singular->details->sum('m_receipt'),  ['class' => 'form-control text-right','min' => 0]) !!}
                        <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
                        <div class="help-block with-errors"></div>
                    </div>
                </div>
            </div>
            <div class="col-sm-6">
                <div class="form-group row has-feedback">
                    {!! Form::label('total_expense_cq', 'Total Expenses Through CQ',['class' => 'col-lg-5 col-form-label']) !!}
                    <div class="col-lg-7">
                        {!! Form::number('total_expense_cq',$module_name_singular->details->sum('total_expense_cq'),  ['class' => 'form-control text-right','min' => 0,'step' => 'any']) !!}
                        <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
                        <div class="help-block with-errors"></div>
                    </div>
                </div>

                <div class="form-group row has-feedback">
                    {!! Form::label('total_exp', 'Total Expenses Through Cash',['class' => 'col-lg-5 col-form-label']) !!}
                    <div class="col-lg-7">
                        {!! Form::number(' total_exp',$module_name_singular->details->sum('total_exp'),  ['class' => 'form-control text-right','min' => 0,'step' => 'any']) !!}
                        <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
                        <div class="help-block with-errors"></div>
                    </div>
                </div>

            </div>
        </div>
        <br>
        <div class="row">
            <div class="col-lg-6">
                @if($module_name_singular->hasPermission())
                    {!! Form::button(('<i class="fa fa-check fa-fw"></i>&nbsp;'.$module_action), ['class' => 'btn btn-success pull-right','type'=>'submit']) !!}
                @endif
            </div>

            <div class="col-lg-6">
                {!! link_to(route($module_route.'.index'), ('<i class="fa fa-mail-reply"></i>&nbsp;'.trans('buttons.general.cancel')), ['class' => 'btn btn-danger pull-left'], null, false) !!}
            </div><!--col-lg-1-->
        </div><!--form control-->
        <br><br>
        {{ Form::close() }}

    @endcomponent
@endsection
