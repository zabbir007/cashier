<div class="row">
    <div class="col-sm-6">
        <div class="form-group row has-feedback">
            {!! Form::label('trans_date', 'Transaction Date',['class' => 'col-lg-5 col-form-label']) !!}
            <div class="col-lg-7">
                {{--{!! Form::text('trans_date',$carbon->format('d-M-Y'),  ['class' => 'form-control','readonly']) !!}--}}
                {!! Form::text('trans_date',$carbon->format('d-M-Y'),  ['class' => 'form-control js-datepicker','data-week-start'=>"6", 'data-autoclose'=>'true', 'data-today-highlight'=>"true", 'data-date-format'=>"dd-M-yyyy", 'placeholder'=>"mm/dd/yy",'readonly']) !!}
                <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
                <div class="help-block with-errors"></div>
            </div>
        </div>
    </div>

    @if(count(access()->user()->branches)>1)
        <div class="col-sm-6">
            <div class="form-group row has-feedback">
                {!! Form::label('branch_name', 'Branch Name',['class' => 'col-lg-5 col-form-label']) !!}
                <div class="col-lg-7">
                    {!! Form::select('branch_id',$branches,old('branch_name'),  ['class' => 'form-control','readonly']) !!}
                    <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
                    <div class="help-block with-errors"></div>
                </div>
            </div>
        </div>
    @else
        <div class="col-sm-6">
            <div class="form-group row has-feedback">
                {!! Form::label('branch_name', 'Branch Name',['class' => 'col-lg-5 col-form-label']) !!}
                <div class="col-lg-7">
                    {!! Form::hidden('branch_id',$branches->id,  ['class' => 'form-control']) !!}
                    {!! Form::text('branch_name',$branches->name,  ['class' => 'form-control','readonly']) !!}
                    <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
                    <div class="help-block with-errors"></div>
                </div>
            </div>
        </div>
    @endif

</div>

<div class="row">

    <div class="col-sm-6">
        <div class="form-group row has-feedback">
            {!! Form::label('trans_date', 'Total Receipt From HO',['class' => 'col-lg-5 col-form-label']) !!}
            <div class="col-lg-7">
                {!! Form::number('total_receipt_hq',old('total_receipt_hq'),  ['class' => 'form-control text-right','min' => 0     , 'step' => 'any']) !!}
                <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
                <div class="help-block with-errors"></div>
            </div>
        </div>

        <div class="form-group row has-feedback">
            {!! Form::label('transfer_imprest_cash', 'Transfer to Imprest Cash (C)',['class' => 'col-lg-5 col-form-label']) !!}
            <div class="col-lg-7">
                {!! Form::number(' transfer_imprest_cash',old('transfer_imprest_cash'),  ['class' => 'form-control text-right','min' => 0     , 'step' => 'any']) !!}
                <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
                <div class="help-block with-errors"></div>
            </div>
        </div>

        <div class="form-group row has-feedback">
            {!! Form::label('m_receipt', 'Miss. Receipt',['class' => 'col-lg-5 col-form-label']) !!}
            <div class="col-lg-7">
                {!! Form::number('m_receipt',old('m_receipt'),  ['class' => 'form-control text-right','min' => 0     , 'step' => 'any']) !!}
                <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
                <div class="help-block with-errors"></div>
            </div>
        </div>
    </div>
    <div class="col-sm-6">
        <div class="form-group row has-feedback">
            {!! Form::label('total_expense_cq', 'Total Expenses Through CQ',['class' => 'col-lg-5 col-form-label']) !!}
            <div class="col-lg-7">
                {!! Form::number('total_expense_cq',old('total_expense_cq'),  ['class' => 'form-control text-right','min' => 0     , 'step' => 'any']) !!}
                <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
                <div class="help-block with-errors"></div>
            </div>
        </div>

        <div class="form-group row has-feedback">
            {!! Form::label('total_exp', 'Total Expenses Through Cash',['class' => 'col-lg-5 col-form-label']) !!}
            <div class="col-lg-7">
                {!! Form::number(' total_exp',old('total_exp'),  ['class' => 'form-control text-right','min' => 0     , 'step' => 'any']) !!}
                <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
                <div class="help-block with-errors"></div>
            </div>
        </div>

    </div>
</div>
<br><br>
