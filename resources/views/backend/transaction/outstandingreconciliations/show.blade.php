@extends ('backend.layouts.app')

@section ('title', $page_heading)

@section('page-header')
    <h1>
        {{ $page_heading }}
    </h1>

@endsection

@section('content')
    <div class="row">
        <div class="col-md-12">
            @component('includes.components.widget')

                @slot('title')
                    {{ $page_heading }}
                @endslot

                @slot('action_buttons')
                    @if ($module_name_singular->hasPermission() && access()->allow('menu-transaction-outstanding-reconciliation-edit'))
                        <a href="{{ route($module_route.'.edit', $module_name_singular) }}" class="btn-block-option"><i
                                    class="si si-note"></i>&nbsp;Edit</a>
                    @endif
                    <a href="{{ route($module_route.'.index') }}" class="btn-block-option"><i class="si si-grid"></i>&nbsp;All {{ title_case($module_name) }}
                    </a>
                    <button type="button" class="btn-block-option" onclick="Codebase.helpers('print-page');">
                        <i class="si si-printer"></i> Print
                    </button>
                    <button type="button" class="btn-block-option" data-toggle="block-option"
                            data-action="fullscreen_toggle"></button>
                    <button type="button" class="btn-block-option" data-toggle="block-option" data-action="state_toggle"
                            data-action-mode="demo">
                        <i class="si si-refresh"></i>
                    </button>
                @endslot
                {{ Form::model($module_name_singular, ['route' => [$module_route.'.update', $module_name_singular], 'class' => 'form-horizontal', 'role' => 'form', 'method' => 'PATCH']) }}

                <div class="row">
                    <div class="col-lg-8">
                        @component('includes.components.table',['class'=>'table table-borderless','style'=> 'font-size:14px','id'=>''])

                            @slot('tbody')
                                <tr>
                                    <td style="font-weight: bold">
                                        Transaction Date
                                    </td>
                                    <td> :</td>
                                    <td>{{ date('d-M-Y',strtotime($module_name_singular->trans_date)) }}</td>
                                    <td style="font-weight: bold">
                                        Branch Name
                                    </td>
                                    <td> :</td>
                                    <td>{{ $module_name_singular->branch_name }}</td>

                                </tr>
                            @endslot
                        @endcomponent
                    </div>
                    <table class="table table-bordered" style='font-size:12px'>
                        <tr>
                            <th rowspan="2" style="font-size: 14px; vertical-align: middle" class="text-center">Brand
                            </th>
                            <th colspan="4" style="font-size: 14px" class="text-center">Sales (BD)</th>
                            <th rowspan="2" style="font-size: 14px; vertical-align: middle"
                                class="text-center bg-success-light">Sales (BD)
                            </th>
                            <th colspan="4" style="font-size: 14px" class="text-center">Return</th>
                            <th rowspan="2" style="font-size: 14px;vertical-align: middle"
                                class="text-center bg-primary-light">Return
                            </th>
                            <th colspan="4" style="font-size: 14px" class="text-center">Net Sales (AD)</th>
                            <th rowspan="2" style="font-size: 14px;vertical-align: middle"
                                class="text-center bg-warning-light">Net Sales (AD)
                            </th>
                        </tr>

                        <tr>
                            <th style="font-size: 14px" class="text-center">TP</th>
                            <th style="font-size: 14px" class="text-center">VAT</th>
                            <th style="font-size: 14px" class="text-center">Discount</th>
                            <th style="font-size: 14px" class="text-center">SP. Disc</th>

                            <th style="font-size: 14px" class="text-center">TP</th>
                            <th style="font-size: 14px" class="text-center">VAT</th>
                            <th style="font-size: 14px" class="text-center">Discount</th>
                            <th style="font-size: 14px" class="text-center">SP. Disc</th>

                            <th style="font-size: 14px" class="text-center">TP</th>
                            <th style="font-size: 14px" class="text-center">VAT</th>
                            <th style="font-size: 14px" class="text-center">Discount</th>
                            <th style="font-size: 14px" class="text-center">SP. Disc</th>
                        </tr>

                        {{-- Grand Total --}}
                        <tr class="bg-gray-darker text-white" style="font-weight: bold">
                            <td style="font-weight: bold">
                                Total
                            </td>
                            <td class="text-right ">{{ number_format($module_name_singular->details->sum('sales_tp'), 2) }}</td>
                            <td class="text-right ">{{ number_format($module_name_singular->details->sum('sales_vat'),2) }}</td>
                            <td class="text-right ">{{ number_format($module_name_singular->details->sum('sales_discount'),2) }}</td>
                            <td class="text-right ">{{ number_format($module_name_singular->details->sum('sales_sp_disc'),2) }}</td>

                            <td class="text-right ">{{ number_format((($module_name_singular->details->sum('sales_tp')+$module_name_singular->details->sum('sales_vat'))-($module_name_singular->details->sum('sales_discount')+$module_name_singular->details->sum('sales_sp_disc'))),2) }}</td>

                            <td class="text-right ">{{ number_format($module_name_singular->details->sum('return_tp'),2) }}</td>
                            <td class="text-right ">{{ number_format($module_name_singular->details->sum('return_vat'),2) }}</td>
                            <td class="text-right ">{{ number_format($module_name_singular->details->sum('return_discount'),2) }}</td>
                            <td class="text-right ">{{ number_format($module_name_singular->details->sum('return_sp_disc'),2) }}</td>

                            <td class="text-right ">{{ number_format((($module_name_singular->details->sum('return_tp')+$module_name_singular->details->sum('return_vat'))-($module_name_singular->details->sum('return_discount')+$module_name_singular->details->sum('return_sp_disc'))),2) }}</td>

                            <td class="text-right ">{{ number_format($module_name_singular->details->sum('net_sales_tp'),2) }}</td>
                            <td class="text-right ">{{ number_format($module_name_singular->details->sum('net_sales_vat'),2) }}</td>
                            <td class="text-right ">{{ number_format($module_name_singular->details->sum('net_sales_discount'),2) }}</td>
                            <td class="text-right ">{{ number_format($module_name_singular->details->sum('net_sales_sp_disc') ,2)}}</td>

                            <td class="text-right ">{{ number_format((($module_name_singular->details->sum('net_sales_tp')+$module_name_singular->details->sum('net_sales_vat'))-($module_name_singular->details->sum('net_sales_discount')+$module_name_singular->details->sum('net_sales_sp_disc'))),2) }}</td>
                        </tr>
                        {{-- End Grand Total --}}

                        @foreach($products as $t)
                            {{-- Brand Total --}}
                            <tr class="bg-gray text-navy" style="font-weight: bold;">
                                <td>
                                    {{ array_get($t, 'brand_name') }}
                                </td>
                                @php
                                    $brand = $module_name_singular->details->where('brand_id', array_get($t, 'brand_id'));
                                @endphp
                                <td class="text-right ">{{ number_format($brand->sum('sales_tp'), 2) }}</td>
                                <td class="text-right ">{{ number_format($brand->sum('sales_vat'), 2) }}</td>
                                <td class="text-right ">{{ number_format($brand->sum('sales_discount'), 2) }}</td>
                                <td class="text-right ">{{ number_format($brand->sum('sales_sp_disc'), 2) }}</td>

                                <td class="text-right ">{{ number_format((($brand->sum('sales_tp') + $brand->sum('sales_vat'))-($brand->sum('sales_discount') + $brand->sum('sales_sp_disc'))), 2) }}</td>

                                <td class="text-right ">{{ number_format($brand->sum('return_tp'), 2) }}</td>
                                <td class="text-right ">{{ number_format($brand->sum('return_vat'), 2) }}</td>
                                <td class="text-right ">{{ number_format($brand->sum('return_discount'), 2) }}</td>
                                <td class="text-right ">{{ number_format($brand->sum('return_sp_disc'), 2) }}</td>

                                <td class="text-right ">{{ number_format((($brand->sum('return_tp') + $brand->sum('return_vat'))-($brand->sum('return_discount') + $brand->sum('return_sp_disc'))), 2) }}</td>

                                <td class="text-right ">{{ number_format($brand->sum('net_sales_tp'), 2) }}</td>
                                <td class="text-right ">{{ number_format($brand->sum('net_sales_vat'), 2) }}</td>
                                <td class="text-right ">{{ number_format($brand->sum('net_sales_discount'), 2) }}</td>
                                <td class="text-right ">{{ number_format($brand->sum('net_sales_sp_disc'), 2) }}</td>

                                <td class="text-right ">{{ number_format((($brand->sum('net_sales_tp') + $brand->sum('net_sales_vat'))-($brand->sum('net_sales_discount') + $brand->sum('net_sales_sp_disc'))),2) }}</td>
                            </tr>
                            {{-- End Brand Total --}}

                            @foreach(array_get($t, 'all') as $product)
                                @php
                                    $details = $module_name_singular->details->where('brand_id', array_get($t, 'brand_id'))->where('product_id',$product->id);
                                @endphp
                                <tr>
                                    <td width="150px"> {{ $product->name }}</td>

                                    <td width="50px" class="text-right">
                                        {{ number_format($details->sum('sales_tp') ,2) }}
                                    </td>
                                    <td width="50px" class="text-right">
                                        {{ number_format($details->sum('sales_vat') ,2) }}
                                    </td>
                                    <td width="50px" class="text-right">
                                        {{ number_format($details->sum('sales_discount') ,2) }}
                                    </td>
                                    <td width="50px" class="text-right">
                                        {{ number_format($details->sum('sales_sp_disc') ,2) }}
                                    </td>
                                    <td width="50px" class="text-right bg-success-light">
                                        {{ number_format((($details->sum('sales_tp')+$details->sum('sales_vat'))-($details->sum('sales_discount')+$details->sum('sales_sp_disc'))),2) }}
                                    </td>

                                    <td width="50px" class="text-right">
                                        {{ number_format($details->sum('return_tp') ,2) }}
                                    </td>
                                    <td width="50px" class="text-right">
                                        {{ number_format($details->sum('return_vat') ,2) }}
                                    </td>
                                    <td width="50px" class="text-right">
                                        {{ number_format($details->sum('return_discount') ,2) }}
                                    </td>
                                    <td width="50px" class="text-right">
                                        {{ number_format($details->sum('return_sp_disc') ,2) }}
                                    </td>
                                    <td width="50px" class="text-right bg-primary-light">
                                        {{ number_format((($details->sum('return_tp')+$details->sum('return_vat'))-($details->sum('return_discount')+$details->sum('return_sp_disc'))),2) }}
                                    </td>

                                    <td width="50px" class="text-right">
                                        {{ number_format($details->sum('net_sales_tp') ,2) }}
                                    </td>
                                    <td width="50px" class="text-right">
                                        {{ number_format($details->sum('net_sales_vat') ,2) }}
                                    </td>
                                    <td width="50px" class="text-right">
                                        {{ number_format($details->sum('net_sales_discount') ,2) }}
                                    </td>
                                    <td width="50px" class="text-right">
                                        {{ number_format($details->sum('net_sales_sp_disc') ,2) }}
                                    </td>
                                    <td width="50px" class="text-right bg-warning-light">
                                        {{ number_format((($details->sum('net_sales_tp')+$details->sum('net_sales_vat'))-($details->sum('net_sales_discount')+$details->sum('net_sales_sp_disc'))),2) }}
                                    </td>
                                </tr>
                            @endforeach

                        @endforeach
                    </table>
                </div>
                <br><br>
                {{ Form::close() }}
            @endcomponent
        </div><!-- END column -->
    </div><!-- .row -->
@endsection
