@extends ('backend.layouts.app')

@section ('title',$page_heading)

@section('after-styles')
    <style type="text/css">
        .input-group-addon {
            padding: 3px 5px !important;
        }

        .required {
            content: "*";
            position: absolute;
            top: 11px;
            right: 3px;
            font-size: 15px;
            color: red;
            font-weight: 700;
        }

        .input-group .required {
            right: -9px;
        }

        td {
            width: auto;
        }

        td.min {
            width: 1%;
            white-space: nowrap;
        }
    </style>
@endsection

@section('content')
    @component('includes.components.widget')
        @slot('title', $page_heading)

        @slot('action_buttons')
            @include($module_view.'.header-buttons')
        @endslot

        {{ Form::open(['route' => $module_route.'.store','class' => 'form-horizontal', 'role' => 'form', 'method' => 'post']) }}

        @include($module_view.'.form')

        <div class="row">
            <div class="col-lg-6">
                {!! Form::button(('<i class="fa fa-check fa-fw"></i>&nbsp;'.$module_action), ['class' => 'btn btn-success pull-right','type'=>'submit']) !!}
            </div>

            <div class="col-lg-6">
                {!! link_to(route($module_route.'.index'), ('<i class="fa fa-mail-reply"></i>&nbsp;'.trans('buttons.general.cancel')), ['class' => 'btn btn-danger pull-left'], null, false) !!}
            </div><!--col-lg-1-->
        </div><!--form control-->

        <br>
        {{ Form::close() }}

    @endcomponent
@endsection

@section('after-scripts')
    <script type="text/javascript">
        $(document).ready(function () {
            //add required

            $("input[required='required'], select").each(function () {
                $(this).before('<span class="required">*</span>');
            });


            $(".input").on('keyup', function (e) {
                let input = $(this).attr('id');
                let div = input.split("-")[0];
                let id = input.split("-")[1];

                //assign values
                let salesTp = parseFloat($("#sales_tp-" + id).val()) || 0;
                let salesVat = parseFloat($("#sales_vat-" + id).val()) || 0;
                let salesDis = parseFloat($("#sales_discount-" + id).val()) || 0;
                let salesSpDis = parseFloat($("#sales_sp_discount-" + id).val()) || 0;
                let salesNet = $("#sales_net-" + id);

                let returnTp = parseFloat($("#return_tp-" + id).val()) || 0;
                let returnVat = parseFloat($("#return_vat-" + id).val()) || 0;
                let returnDis = parseFloat($("#return_discount-" + id).val()) || 0;
                let returnSpDis = parseFloat($("#return_sp_discount-" + id).val()) || 0;
                let returnNet = $("#return_net-" + id);

                let netTp = $("#net_sales_tp-" + id);
                let netVat = $("#net_sales_vat-" + id);
                let netDis = $("#net_sales_discount-" + id);
                let netSpDis = $("#net_sales_sp_discount-" + id);
                let netSales = $("#net_sales-" + id);

                //calculate and set values
                let salesNetValue = (salesTp + salesVat) - (salesDis + salesSpDis);
                let returnNetValue = (returnTp + returnVat) - (returnDis + returnSpDis);

                salesNet.val(salesNetValue);
                returnNet.val(returnNetValue);

                netTp.val(salesTp - returnTp);
                netVat.val(salesVat - returnVat);
                netDis.val(salesDis - returnDis);
                netSpDis.val(salesSpDis - returnSpDis);

                netSales.val((parseFloat(netTp.val()) + parseFloat(netVat.val())) - (parseFloat(netDis.val()) + parseFloat(netSpDis.val())));
            });
        });
    </script>
@endsection