@extends ('backend.layouts.app')

@section ('title', $page_heading)

@section('page-header')
    <h1>
        {{ $page_heading }}
    </h1>
@endsection

@section('content')
    @component('includes.components.widget')
        @slot('title')
            {{ $page_heading }}
        @endslot

        @slot('action_buttons')
            <div class="btn-group float-right">
                @if (access()->allow('menu-transaction-outstanding-reconciliation-show'))
                    <a href="{{ route($module_route.'.show', $module_name_singular) }}"
                       class="btn btn-alt-primary btn-sm" data-toggle="click-ripple"><i class="si si-eye"></i>&nbsp;View</a>
                @endif

                <a href="{{ route($module_route.'.index') }}" class="btn btn-alt-primary btn-sm"
                   data-toggle="click-ripple"><i class="si si-grid"></i>&nbsp;All Transactions</a>
                <a href="{{ route($module_route.'.create') }}" class="btn btn-alt-success btn-sm"
                   data-toggle="click-ripple"><i class="si si-plus"></i>&nbsp;Add New</a>
            </div><!--pull right-->

        @endslot

        {{ Form::model($module_name_singular, ['route' => [$module_route.'.update', $module_name_singular], 'class' => 'form-horizontal', 'role' => 'form', 'method' => 'PATCH']) }}

        <div class="row">
            <div class="col-sm-6">
                <div class="form-group row has-feedback">
                    {!! Form::label('trans_date', 'Transaction Date',['class' => 'col-lg-4 col-form-label']) !!}
                    <div class="col-lg-8">
                        {!! Form::hidden('sl', $module_name_singular->sl, ['class' => 'form-control', 'placeholder'=>'XXXX-XXXX','readonly']) !!}
                        {!! Form::text('trans_date',date('d-M-Y',strtotime($module_name_singular->trans_date)),  ['class' => 'form-control','readonly']) !!}
                        <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
                        <div class="help-block with-errors"></div>
                    </div>
                </div>
            </div>


            <div class="col-sm-6">
                <div class="form-group row has-feedback">
                    {!! Form::label('branch_name', 'Branch Name',['class' => 'col-lg-4 col-form-label']) !!}
                    <div class="col-lg-8">
                        {!! Form::hidden('branch_id',$module_name_singular->branch_id,  ['class' => 'form-control','readonly']) !!}
                        {!! Form::text('branch_name',$module_name_singular->branch_name,  ['class' => 'form-control','readonly']) !!}
                        <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
                        <div class="help-block with-errors"></div>
                    </div>
                </div>
            </div>
        </div>
        <br>

        <div class="row" style="overflow-x: scroll">
            <table class="table table-bordered" style='font-size:12px'>
                <tr>
                    <th rowspan="2" style="font-size: 14px; vertical-align: middle" class="text-center">Brand</th>
                    <th colspan="5" style="font-size: 14px" class="text-center">Sales (BD)</th>
                    <th colspan="5" style="font-size: 14px" class="text-center">Return</th>
                    <th colspan="5" style="font-size: 14px" class="text-center">Net Sales (AD)</th>
                </tr>

                <tr>
                    <th style="font-size: 14px" class="text-center">TP</th>
                    <th style="font-size: 14px" class="text-center">VAT</th>
                    <th style="font-size: 14px" class="text-center">Discount</th>
                    <th style="font-size: 14px" class="text-center">SP. Disc</th>
                    <th style="font-size: 14px" class="text-center">Net</th>

                    <th style="font-size: 14px" class="text-center">TP</th>
                    <th style="font-size: 14px" class="text-center">VAT</th>
                    <th style="font-size: 14px" class="text-center">Discount</th>
                    <th style="font-size: 14px" class="text-center">SP. Disc</th>
                    <th style="font-size: 14px" class="text-center">Net</th>

                    <th style="font-size: 14px" class="text-center">TP</th>
                    <th style="font-size: 14px" class="text-center">VAT</th>
                    <th style="font-size: 14px" class="text-center">Discount</th>
                    <th style="font-size: 14px" class="text-center">SP. Disc</th>
                    <th style="font-size: 14px" class="text-center">Net</th>
                </tr>
                <tr class="bg-gray-darker text-white" style="font-weight: bold">
                    <td style="font-weight: bold">
                        Total
                    </td>
                    <td class="text-right ">{{ $module_name_singular->details->sum('sales_tp') }}</td>
                    <td class="text-right ">{{ $module_name_singular->details->sum('sales_vat') }}</td>
                    <td class="text-right ">{{ $module_name_singular->details->sum('sales_discount') }}</td>
                    <td class="text-right ">{{ $module_name_singular->details->sum('sales_sp_disc') }}</td>
                    <td class="text-right ">{{ ($module_name_singular->details->sum('sales_tp') + $module_name_singular->details->sum('sales_vat')) - ($module_name_singular->details->sum('sales_discount') + $module_name_singular->details->sum('sales_sp_disc'))  }}</td>

                    <td class="text-right ">{{ $module_name_singular->details->sum('return_tp') }}</td>
                    <td class="text-right ">{{ $module_name_singular->details->sum('return_vat') }}</td>
                    <td class="text-right ">{{ $module_name_singular->details->sum('return_discount') }}</td>
                    <td class="text-right ">{{ $module_name_singular->details->sum('return_sp_disc') }}</td>
                    <td class="text-right ">{{ ($module_name_singular->details->sum('return_tp') + $module_name_singular->details->sum('return_vat')) - ($module_name_singular->details->sum('return_discount') + $module_name_singular->details->sum('return_sp_disc'))  }}</td>

                    <td class="text-right ">{{ $module_name_singular->details->sum('net_sales_tp') }}</td>
                    <td class="text-right ">{{ $module_name_singular->details->sum('net_sales_vat') }}</td>
                    <td class="text-right ">{{ $module_name_singular->details->sum('net_sales_discount') }}</td>
                    <td class="text-right ">{{ $module_name_singular->details->sum('net_sales_sp_disc') }}</td>
                    <td class="text-right ">{{ ($module_name_singular->details->sum('net_sales_tp') + $module_name_singular->details->sum('net_sales_vat')) - ($module_name_singular->details->sum('net_sales_discount') + $module_name_singular->details->sum('net_sales_sp_disc'))  }}</td>
                </tr>

                @foreach($products as $t)

                    <tr class="bg-gray text-navy" style="font-weight: bold">
                        <td>{{ array_get($t, 'brand_name')}}</td>
                        <td class="text-right ">{{ $module_name_singular->details->where('brand_id', array_get($t, 'brand_id'))->sum('sales_tp') }}</td>
                        <td class="text-right ">{{ $module_name_singular->details->where('brand_id', array_get($t, 'brand_id'))->sum('sales_vat') }}</td>
                        <td class="text-right ">{{ $module_name_singular->details->where('brand_id',array_get($t, 'brand_id'))->sum('sales_discount') }}</td>
                        <td class="text-right ">{{ $module_name_singular->details->where('brand_id',array_get($t, 'brand_id'))->sum('sales_sp_disc') }}</td>
                        <td class="text-right ">{{ ($module_name_singular->details->where('brand_id',array_get($t, 'brand_id'))->sum('sales_tp') + $module_name_singular->details->where('brand_id',array_get($t, 'brand_id'))->sum('sales_vat')) - ($module_name_singular->details->where('brand_id',array_get($t, 'brand_id'))->sum('sales_discount') + $module_name_singular->details->where('brand_id',array_get($t, 'brand_id'))->sum('sales_sp_disc')) }}</td>

                        <td class="text-right ">{{ $module_name_singular->details->where('brand_id',array_get($t, 'brand_id'))->sum('return_tp') }}</td>
                        <td class="text-right ">{{ $module_name_singular->details->where('brand_id',array_get($t, 'brand_id'))->sum('return_vat') }}</td>
                        <td class="text-right ">{{ $module_name_singular->details->where('brand_id',array_get($t, 'brand_id'))->sum('return_discount') }}</td>
                        <td class="text-right ">{{ $module_name_singular->details->where('brand_id',array_get($t, 'brand_id'))->sum('return_sp_disc') }}</td>
                        <td class="text-right ">{{ ($module_name_singular->details->where('brand_id',array_get($t, 'brand_id'))->sum('return_tp') + $module_name_singular->details->where('brand_id',array_get($t, 'brand_id'))->sum('return_vat')) - ($module_name_singular->details->where('brand_id',array_get($t, 'brand_id'))->sum('return_discount') + $module_name_singular->details->where('brand_id',array_get($t, 'brand_id'))->sum('return_sp_disc')) }}</td>

                        <td class="text-right ">{{ $module_name_singular->details->where('brand_id',array_get($t, 'brand_id'))->sum('net_sales_tp') }}</td>
                        <td class="text-right ">{{ $module_name_singular->details->where('brand_id',array_get($t, 'brand_id'))->sum('net_sales_vat') }}</td>
                        <td class="text-right ">{{ $module_name_singular->details->where('brand_id',array_get($t, 'brand_id'))->sum('net_sales_discount') }}</td>
                        <td class="text-right ">{{ $module_name_singular->details->where('brand_id',array_get($t, 'brand_id'))->sum('net_sales_sp_disc') }}</td>
                        <td class="text-right ">{{ ($module_name_singular->details->where('brand_id',array_get($t, 'brand_id'))->sum('net_sales_tp') + $module_name_singular->details->where('brand_id',array_get($t, 'brand_id'))->sum('net_sales_vat')) - ($module_name_singular->details->where('brand_id',array_get($t, 'brand_id'))->sum('net_sales_discount') + $module_name_singular->details->where('brand_id',array_get($t, 'brand_id'))->sum('net_sales_sp_disc')) }}</td>
                    </tr>
                    @foreach(array_get($t, 'all') as $product)
                        @php
                            $details = collect($module_name_singular->details->where('product_id',$product->id));
                        @endphp
                        {{ Form::hidden('data['.$product->id.'][product_id]', $product->id) }}
                        {{ Form::hidden('data['.$product->id.'][product_name]', $product->name) }}
                        {{ Form::hidden('data['.$product->id.'][brand_id]', array_get($t, 'brand_id')) }}
                        {{ Form::hidden('data['.$product->id.'][brand_name]', array_get($t, 'brand_name')) }}

                        <tr>
                            <td width="150px"> {{ $product->name }}</td>

                            {{-- Start Sales --}}
                            <td width="50px">
                                {{ Form::number('data['.$product->id.'][sales_tp]', $details->sum('sales_tp'), [
                                'class' => 'form-control1 input text-right',
                                'placeholder' => '','style'=>'width: 60px',
                                'min' => 0, 'step' => 'any',
                                'id' => 'sales_tp-'.$product->id
                                ])
                               }}
                            </td>
                            <td width="50px">
                                {{ Form::number('data['.$product->id.'][sales_vat]', $details->sum('sales_vat'), [
                                'class' => 'form-control1 input text-right',
                                'style'=>'width: 78px',
                                'placeholder' => '',
                                'min' => 0 , 'step' => 'any',
                                'id' => 'sales_vat-'.$product->id
                                ])
                               }}
                            </td>
                            <td width="50px">
                                {{ Form::number('data['.$product->id.'][sales_discount]', $details->sum('sales_discount'), [
                                'class' => 'form-control1 input text-right',
                                'placeholder' => '','style'=>'width: 78px',
                                'min' => 0    , 'step' => 'any',
                                'id' => 'sales_discount-'.$product->id
                                ])
                               }}
                            </td>
                            <td width="50px">
                                {{ Form::number('data['.$product->id.'][sales_sp_discount]', $details->sum('sales_sp_disc'), [
                               'class' => 'form-control1 input text-right',
                                'placeholder' => '','style'=>'width: 78px',
                                'min' => 0     , 'step' => 'any',
                                'id' => 'sales_sp_discount-'.$product->id
                                ])
                               }}
                            </td>
                            <td width="50px">
                                {{ Form::number('', ($details->sum('sales_tp') + $details->sum('sales_vat')) - ($details->sum('sales_discount') + $details->sum('sales_sp_disc')), [
                               'class' => 'form-control1 input text-right',
                                'placeholder' => '','style'=>'width: 78px',
                                'min' => 0     , 'step' => 'any',
                                'id' => 'sales_net-'.$product->id,
                                'readonly',
                                'disabled'
                                ])
                               }}
                            </td>
                            {{-- End Sales --}}


                            {{-- Start Return --}}
                            <td width="50px">
                                {{ Form::number('data['.$product->id.'][return_tp]', $details->sum('return_tp'), [
                               'class' => 'form-control1 input text-right',
                                'placeholder' => '','style'=>'width: 78px',
                                'min' => 0, 'step' => 'any',
                                'id' => 'return_tp-'.$product->id
                                ])
                               }}
                            </td>
                            <td width="50px">
                                {{ Form::number('data['.$product->id.'][return_vat]', $details->sum('return_vat'), [
                                'class' => 'form-control1 input text-right',
                                'style'=>'width: 78px',
                                'placeholder' => '',
                                'min' => 0 , 'step' => 'any',
                                'id' => 'return_vat-'.$product->id
                                ])
                               }}
                            </td>
                            <td width="50px">
                                {{ Form::number('data['.$product->id.'][return_discount]', $details->sum('return_discount'), [
                                'class' => 'form-control1 input text-right',
                                'placeholder' => '','style'=>'width: 78px',
                                'min' => 0    , 'step' => 'any',
                                'id' => 'return_discount-'.$product->id
                                ])
                               }}
                            </td>
                            <td width="50px">
                                {{ Form::number('data['.$product->id.'][return_sp_discount]', $details->sum('return_sp_disc'), [
                                'class' => 'form-control1 text-right',
                                'placeholder' => '','style'=>'width: 78px',
                                'min' => 0     , 'step' => 'any',
                                'id' => 'return_sp_discount-'.$product->id
                                ])
                               }}
                            </td>
                            <td width="50px">
                                {{ Form::number('', ($details->sum('return_tp') + $details->sum('return_vat')) - ($details->sum('return_discount') + $details->sum('return_sp_disc')), [
                                'class' => 'form-control1 text-right',
                                'placeholder' => '',
                                'style'=>'width: 78px',
                                'min' => 0,
                                'step' => 'any',
                                'id' => 'return_net-'.$product->id,
                                'readonly',
                                'disabled'
                                ])
                               }}
                            </td>
                            {{-- End Return --}}


                            <td width="50px">
                                {{ Form::number('data['.$product->id.'][net_sales_tp]', $details->sum('net_sales_tp'), [
                                'class' => 'form-control1 text-right',
                                'placeholder' => '','style'=>'width: 78px',
                                'min' => 0, 'step' => 'any',
                               'id' => 'net_sales_tp-'.$product->id,'readonly'
                                ])
                               }}
                            </td>
                            <td width="50px">
                                {{ Form::number('data['.$product->id.'][net_sales_vat]', $details->sum('net_sales_vat'), [
                                'class' => 'form-control1 text-right',
                                'style'=>'width: 78px',
                                'placeholder' => '',
                                'min' => 0 , 'step' => 'any',
                                'id' => 'net_sales_vat-'.$product->id,'readonly'
                                ])
                               }}
                            </td>
                            <td width="50px">
                                {{ Form::number('data['.$product->id.'][net_sales_discount]', $details->sum('net_sales_discount'), [
                                'class' => 'form-control1 text-right',
                                'placeholder' => '','style'=>'width: 78px',
                                'min' => 0    , 'step' => 'any',
                                'id' => 'net_sales_discount-'.$product->id,'readonly'
                                ])
                               }}
                            </td>
                            <td width="50px">
                                {{ Form::number('data['.$product->id.'][net_sales_sp_discount]', $details->sum('net_sales_sp_disc'), [
                                'class' => 'form-control1 text-right',
                                'placeholder' => '','style'=>'width: 78px',
                                'min' => 0     , 'step' => 'any',
                                'id' => 'net_sales_sp_discount-'.$product->id,'readonly'
                                ])
                               }}
                            </td>

                            <td width="50px">
                                {{ Form::number('', ($details->sum('net_sales_tp') + $details->sum('net_sales_vat')) - ($details->sum('net_sales_discount') + $details->sum('net_sales_sp_disc')), [
                               'class' => 'form-control1 input text-right',
                                'placeholder' => '','style'=>'width: 78px',
                                'min' => 0     , 'step' => 'any',
                                'id' => 'net_sales-'.$product->id,
                                'readonly','disabled'
                                ])
                               }}
                            </td>
                        </tr>
                    @endforeach

                @endforeach
            </table>


        </div>

        <div class="row">
            <div class="col-lg-6">
                @if($module_name_singular->hasPermission())
                    {!! Form::button(('<i class="fa fa-check fa-fw"></i>&nbsp;'.$module_action), ['class' => 'btn btn-success pull-right','type'=>'submit']) !!}
                @endif
            </div>

            <div class="col-lg-6">
                {!! link_to(route($module_route.'.index'), ('<i class="fa fa-mail-reply"></i>&nbsp;'.trans('buttons.general.cancel')), ['class' => 'btn btn-danger pull-left'], null, false) !!}
            </div><!--col-lg-1-->
        </div><!--form control-->
        <br><br>
        {{ Form::close() }}

    @endcomponent
@endsection

@section('after-scripts')

    <script type="text/javascript">
        $(document).ready(function () {
            //add required

            $("input[required='required'], select").each(function () {
                $(this).before('<span class="required">*</span>');
            });


            $(".input").on('keyup', function (e) {
                let input = $(this).attr('id');
                let div = input.split("-")[0];
                let id = input.split("-")[1];

                //assign values
                let salesTp = parseFloat($("#sales_tp-" + id).val()) || 0;
                let salesVat = parseFloat($("#sales_vat-" + id).val()) || 0;
                let salesDis = parseFloat($("#sales_discount-" + id).val()) || 0;
                let salesSpDis = parseFloat($("#sales_sp_discount-" + id).val()) || 0;
                let salesNet = $("#sales_net-" + id);

                let returnTp = parseFloat($("#return_tp-" + id).val()) || 0;
                let returnVat = parseFloat($("#return_vat-" + id).val()) || 0;
                let returnDis = parseFloat($("#return_discount-" + id).val()) || 0;
                let returnSpDis = parseFloat($("#return_sp_discount-" + id).val()) || 0;
                let returnNet = $("#return_net-" + id);

                let netTp = $("#net_sales_tp-" + id);
                let netVat = $("#net_sales_vat-" + id);
                let netDis = $("#net_sales_discount-" + id);
                let netSpDis = $("#net_sales_sp_discount-" + id);
                let netSales = $("#net_sales-" + id);

                //calculate and set values
                let salesNetValue = (salesTp + salesVat) - (salesDis + salesSpDis);
                let returnNetValue = (returnTp + returnVat) - (returnDis + returnSpDis);

                salesNet.val(salesNetValue);
                returnNet.val(returnNetValue);

                netTp.val(salesTp - returnTp);
                netVat.val(salesVat - returnVat);
                netDis.val(salesDis - returnDis);
                netSpDis.val(salesSpDis - returnSpDis);

                netSales.val((parseFloat(netTp.val()) + parseFloat(netVat.val())) - (parseFloat(netDis.val()) + parseFloat(netSpDis.val())));
            });
        });
    </script>

@endsection
