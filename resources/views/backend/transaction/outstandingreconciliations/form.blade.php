<div class="row">
    <div class="col-sm-6">
        <div class="form-group row has-feedback">
            {!! Form::label('trans_date', 'Transaction Date',['class' => 'col-lg-4 col-form-label']) !!}
            <div class="col-lg-8">
                {{--{!! Form::text('trans_date',$carbon->format('d-M-Y'),  ['class' => 'form-control','readonly']) !!}--}}
                {!! Form::text('trans_date',$carbon->format('d-M-Y'),  ['class' => 'form-control js-datepicker','data-week-start'=>'6', 'data-autoclose'=>'true', 'data-today-highlight'=>'true', 'data-date-format'=>'dd-M-yyyy', 'placeholder'=>'mm/dd/yy','readonly']) !!}
                <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
                <div class="help-block with-errors"></div>
            </div>
        </div>
    </div>

    {{-- Start Heading --}}
    @if(count(access()->user()->branches)>1)
        <div class="col-sm-6">
            <div class="form-group row has-feedback">
                {!! Form::label('branch_name', 'Branch Name',['class' => 'col-lg-4 col-form-label']) !!}
                <div class="col-lg-8">
                    {!! Form::select('branch_id',$branches,old('branch_name'),  ['class' => 'form-control','readonly']) !!}
                    <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
                    <div class="help-block with-errors"></div>
                </div>
            </div>
        </div>
    @else
        <div class="col-sm-6">
            <div class="form-group row has-feedback">
                {!! Form::label('branch_name', 'Branch Name',['class' => 'col-lg-4 col-form-label']) !!}
                <div class="col-lg-8">
                    {!! Form::hidden('branch_id',$branches->id,  ['class' => 'form-control','readonly']) !!}
                    {!! Form::text('branch_name',$branches->name,  ['class' => 'form-control','readonly']) !!}
                    <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
                    <div class="help-block with-errors"></div>
                </div>
            </div>
        </div>
    @endif

    {{-- End Heading --}}
</div>

<br>
<div class="row" style="overflow-x: scroll">
    {{-- Start Table --}}
    <table class="table table-bordered" style='font-size:12px'>
        {{-- Start Table Heading --}}
        <tr>
            <th rowspan="2" style="font-size: 14px; vertical-align: middle" class="text-center">Brand</th>
            <th colspan="5" style="font-size: 14px" class="text-center">Sales (BD)</th>
            <th colspan="5" style="font-size: 14px" class="text-center">Return</th>
            <th colspan="4" style="font-size: 14px" class="text-center">Net Sales (AD)</th>
        </tr>

        <tr>
            <th style="font-size: 14px" class="text-center">TP</th>
            <th style="font-size: 14px" class="text-center">VAT</th>
            <th style="font-size: 14px" class="text-center">Discount</th>
            <th style="font-size: 14px" class="text-center">SP. Disc</th>
            <th style="font-size: 14px" class="text-center">Total Sale</th>

            <th style="font-size: 14px" class="text-center">TP</th>
            <th style="font-size: 14px" class="text-center">VAT</th>
            <th style="font-size: 14px" class="text-center">Discount</th>
            <th style="font-size: 14px" class="text-center">SP. Disc</th>
            <th style="font-size: 14px" class="text-center">Total Return</th>

            <th style="font-size: 14px" class="text-center">TP</th>
            <th style="font-size: 14px" class="text-center">VAT</th>
            <th style="font-size: 14px" class="text-center">Discount</th>
            <th style="font-size: 14px" class="text-center">SP. Disc</th>
            <th style="font-size: 14px" class="text-center">Net Sale</th>
        </tr>

        {{-- End Table Heading--}}

        <tbody>
        {{-- Start Total --}}
        <tr class="bg-gray-darker text-white">
            <td style="font-weight: bold">Total</td>

            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>

        </tr>
        {{-- End Total --}}

        @foreach($products as $t)
            <tr class="bg-gray text-navy" style="font-weight: bold">
                <td style="font-weight: bold"> {{ array_get($t,'brand_name') }} </td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
            </tr>

            @foreach(array_get($t,'all') as $product)
                <tr>
                    <td width="150px"> {{ $product->name }}</td>
                    {{ Form::hidden('data['.$product->id.'][product_id]', $product->id) }}
                    {{ Form::hidden('data['.$product->id.'][product_name]', $product->name) }}
                    {{ Form::hidden('data['.$product->id.'][brand_id]', array_get($t,'brand_id')) }}
                    {{ Form::hidden('data['.$product->id.'][brand_name]', array_get($t,'brand_name')) }}

                    {{-- Start Sales--}}
                    <td width="50px">
                        {{ Form::number('data['.$product->id.'][sales_tp]', null, [
                        'class' => 'form-control1 input text-right',
                        'placeholder' => '','style'=>'width: 60px',
                        'min' => 0, 'step' => 'any',
                        'id' => 'sales_tp-'.$product->id
                        ])
                       }}
                    </td>
                    <td width="50px">
                        {{ Form::number('data['.$product->id.'][sales_vat]', null, [
                        'class' => 'form-control1 input text-right','style'=>'width: 78px',
                        'placeholder' => '',
                        'min' => 0 , 'step' => 'any',
                        'id' => 'sales_vat-'.$product->id
                        ])
                       }}
                    </td>
                    <td width="50px">
                        {{ Form::number('data['.$product->id.'][sales_discount]', null, [
                        'class' => 'form-control1 input text-right',
                        'placeholder' => '','style'=>'width: 78px',
                        'min' => 0    , 'step' => 'any',
                        'id' => 'sales_discount-'.$product->id
                        ])
                       }}
                    </td>
                    <td width="50px">
                        {{ Form::number('data['.$product->id.'][sales_sp_discount]', null, [
                        'class' => 'form-control1 input text-right',
                        'placeholder' => '','style'=>'width: 78px',
                        'min' => 0     , 'step' => 'any',
                        'id' => 'sales_sp_discount-'.$product->id
                        ])
                       }}
                    </td>
                    <td width="50px">
                        {{ Form::number('', 0, [
                        'class' => 'form-control1 input text-right',
                        'placeholder' => '','style'=>'width: 78px',
                        'min' => 0, 'step' => 'any',
                        'id' => 'sales_net-'.$product->id,
                        'readonly',
                        'disabled'
                        ])
                       }}
                    </td>
                    {{-- End Sales--}}

                    {{-- Start Return --}}
                    <td width="50px">
                        {{ Form::number('data['.$product->id.'][return_tp]', null, [
                        'class' => 'form-control1 input text-right',
                        'placeholder' => '','style'=>'width: 78px',
                        'min' => 0, 'step' => 'any',
                        'id' => 'return_tp-'.$product->id
                        ])
                       }}
                    </td>
                    <td width="50px">
                        {{ Form::number('data['.$product->id.'][return_vat]', null, [
                        'class' => 'form-control1 input text-right','style'=>'width: 78px',
                        'placeholder' => '',
                        'min' => 0 , 'step' => 'any',
                        'id' => 'return_vat-'.$product->id
                        ])
                       }}
                    </td>
                    <td width="50px">
                        {{ Form::number('data['.$product->id.'][return_discount]', null, [
                        'class' => 'form-control1 input text-right',
                        'placeholder' => '','style'=>'width: 78px',
                        'min' => 0    , 'step' => 'any',
                        'id' => 'return_discount-'.$product->id
                        ])
                       }}
                    </td>
                    <td width="50px">
                        {{ Form::number('data['.$product->id.'][return_sp_discount]', null, [
                        'class' => 'form-control1 input text-right',
                        'placeholder' => '','style'=>'width: 78px',
                        'min' => 0     , 'step' => 'any',
                        'id' => 'return_sp_discount-'.$product->id
                        ])
                       }}
                    </td>
                    <td width="50px">
                        {{ Form::number('', 0, [
                        'class' => 'form-control1 input text-right',
                        'placeholder' => '','style'=>'width: 78px',
                        'min' => 0, 'step' => 'any',
                        'id' => 'return_net-'.$product->id,
                        'readonly',
                        'disabled'
                        ])
                       }}
                    </td>
                    {{-- End Return --}}

                    {{-- Start Net --}}
                    <td width="50px">
                        {{ Form::number('data['.$product->id.'][net_sales_tp]', null, [
                        'class' => 'form-control1 text-right',
                        'placeholder' => '','style'=>'width: 78px',
                        'min' => 0, 'step' => 'any',
                        'id' => 'net_sales_tp-'.$product->id,
                        'readonly'
                        ])
                       }}
                    </td>
                    <td width="50px">
                        {{ Form::number('data['.$product->id.'][net_sales_vat]', null, [
                        'class' => 'form-control1 text-right','style'=>'width: 78px',
                        'placeholder' => '',
                        'min' => 0 , 'step' => 'any',
                        'id' => 'net_sales_vat-'.$product->id,
                        'readonly'
                        ])
                       }}
                    </td>
                    <td width="50px">
                        {{ Form::number('data['.$product->id.'][net_sales_discount]', null, [
                        'class' => 'form-control1 text-right',
                        'placeholder' => '','style'=>'width: 78px',
                        'min' => 0    , 'step' => 'any',
                        'id' => 'net_sales_discount-'.$product->id,
                        'readonly'
                        ])
                       }}
                    </td>
                    <td width="50px">
                        {{ Form::number('data['.$product->id.'][net_sales_sp_discount]', null, [
                        'class' => 'form-control1 text-right',
                        'placeholder' => '','style'=>'width: 78px',
                        'min' => 0     , 'step' => 'any',
                        'id' => 'net_sales_sp_discount-'.$product->id,
                        'readonly'
                        ])
                       }}
                    </td>
                    <td width="50px">
                        {{ Form::number('', 0, [
                        'class' => 'form-control1 input text-right',
                        'placeholder' => '','style'=>'width: 78px',
                        'min' => 0, 'step' => 'any',
                        'id' => 'net_sales-'.$product->id,
                        'readonly',
                        'disabled'
                        ])
                       }}
                    </td>
                    {{-- End Net--}}

                </tr>
            @endforeach
        @endforeach
        </tbody>
    </table>
</div>
<br><br>
