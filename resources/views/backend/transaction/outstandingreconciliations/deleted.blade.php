@extends ('backend.layouts.app')

@section ('title', $page_heading)

@section('after-styles')
    {{ Html::style("https://cdn.datatables.net/v/bs/dt-1.10.15/datatables.min.css") }}
@endsection

@section('page-header')
    <h1>
        {{ $page_heading }}
    </h1>
@endsection

@section('content')
    @component('includes.components.widget')
        @slot('title')
            {{ $page_heading }}
        @endslot

        @slot('action_buttons')
            @include($module_view.'.header-buttons')
        @endslot

        @component('includes.components.table',['class'=>'table table-condensed table-hover table-striped','id'=>''])
            @slot('thead')
                <th>#</th>
                <th>Name</th>
                <th>Code</th>
                <th>Status</th>
                <th>{{ trans('labels.general.actions') }}</th>
            @endslot

            @slot('tbody')
                @foreach($$module_name as $module_name)
                    <tr>
                        <td>{{ $loop->iteration }}</td>
                        <td> {{ $module_name->name }}</td>
                        <td> {{ $module_name->code }}</td>
                        <td> {!! $module_name->status_label !!} </td>
                        <td> {!! $module_name->action_buttons !!}</td>
                    </tr>
                @endforeach
            @endslot
        @endcomponent

    @endcomponent

@endsection

@section('after-scripts')
    {{ Html::script("https://cdn.datatables.net/v/bs/dt-1.10.15/datatables.min.js") }}
    {{ Html::script("js/backend/plugin/datatables/dataTables-extend.js") }}

    <script>
        $(function () {
            $('#users-table').DataTable({
                dom: 'lfrtip',
                processing: false,
                serverSide: true,
                autoWidth: false,
                ajax: {
                    url: '{{ route("admin.access.user.get") }}',
                    type: 'post',
                    data: {status: 0, trashed: true},
                    error: function (xhr, err) {
                        if (err === 'parsererror')
                            location.reload();
                    }
                },
                columns: [
                    {data: 'last_name', name: '{{config('access.users_table')}}.last_name'},
                    {data: 'first_name', name: '{{config('access.users_table')}}.first_name'},
                    {data: 'email', name: '{{config('access.users_table')}}.email'},
                    {data: 'confirmed', name: '{{config('access.users_table')}}.confirmed'},
                    {data: 'roles', name: '{{config('access.roles_table')}}.name', sortable: false},
                    {data: 'created_at', name: '{{config('access.users_table')}}.created_at'},
                    {data: 'updated_at', name: '{{config('access.users_table')}}.updated_at'},
                    {data: 'actions', name: 'actions', searchable: false, sortable: false}
                ],
                order: [[0, "asc"]],
                searchDelay: 500
            });
        });
    </script>
@endsection