@extends ('backend.layouts.app')

@section ('title', $page_heading)

@section('page-header')
    <h1>
        {{ $page_heading }}
    </h1>

@endsection

@section('content')
    <div class="row">
        <div class="col-md-12">
            @component('includes.components.widget')

                @slot('title')
                    {{ $page_heading }}
                @endslot

                @slot('action_buttons')
                    @if (access()->allow('menu-transaction-outstanding-reconciliation-edit'))
                        <a href="{{ route($module_route.'.edit', $module_name_singular) }}" class="btn-block-option"><i
                                class="si si-note"></i>&nbsp;Edit</a>
                    @endif
                    <a href="{{ route($module_route.'.index') }}" class="btn-block-option"><i class="si si-grid"></i>&nbsp;All {{ title_case($module_name) }}
                    </a>
                    <button type="button" class="btn-block-option" onclick="Codebase.helpers('print-page');">
                        <i class="si si-printer"></i> Print
                    </button>
                    <button type="button" class="btn-block-option" data-toggle="block-option"
                            data-action="fullscreen_toggle"></button>
                    <button type="button" class="btn-block-option" data-toggle="block-option" data-action="state_toggle"
                            data-action-mode="demo">
                        <i class="si si-refresh"></i>
                    </button>
                @endslot

                {{ Form::model($$module_name_singular, ['route' => [$module_route.'.update', $$module_name_singular], 'class' => 'form-horizontal', 'role' => 'form', 'method' => 'PATCH']) }}

                <br>
                <div class="row" ref="inputForm">

                    @php
                        $all_received_from_ho = collect($all_received_from_ho);
                     $selected_month_dates = collect($selected_month_dates);
                    @endphp

                    <div class="col-sm-12">
                        <table class="table">
                            <tbody>
                            <tr>
                                <td colspan="2" class="no-top-border">
                                    <h6 class="mb-0">TRANSCOM DISTRIBUTION COMPANY LIMITED</h6>
                                </td>

                                <td class="no-top-border" width="130">&nbsp;</td>

                                <td colspan="2" class="no-top-border">
                                    <h6 class="mb-0">IMPREST BANK RECONCILIATION STATEMENT</h6>
                                </td>
                            </tr>
                            <tr>
                                <td><strong>BRANCH NAME:</strong></td>
                                <td><strong>{{ $selected_branch_name }}</strong></td>

                                <td class="no-top-border" width="130">&nbsp;</td>

                                <td><strong>ACCOUNT NUMBER:</strong></td>
                                <td><strong>{{ $selected_account_number }}</strong></td>
                            </tr>
                            <tr>
                                <td><strong>BANK NAME:</strong></td>
                                <td><strong>{{ $selected_bank_name }}</strong></td>

                                <td class="no-top-border" width="130">&nbsp;</td>

                                <td style="font-weight: bold; background: #00b050 none repeat scroll 0 0; ">
                                    <strong>CLOSING BALANCE AS PER BANK:</strong></td>
                                <td class="text-right" style="font-weight: bold">@{{ closingBalanceAsPerBankStatement |
                                    money_format }}
                                </td>
                            </tr>
                            <tr>
                                <td><strong>MONTH:</strong></td>
                                <td><strong>{{ $selected_month_name }}</strong></td>

                                <td class="no-top-border" width="130">&nbsp;</td>

                                <td style="font-weight: bold; background: #00b050 none repeat scroll 0 0; ">
                                    <strong>CLOSING BALANCE AS PER BANK BOOK:</strong></td>
                                <td class="text-right" style="font-weight: bold">@{{ closingBalanceAsPerBankBook |
                                    money_format }}
                                </td>
                            </tr>
                            </tbody>
                        </table>

                        <table class="table table-bordered table-condensed">
                            <tbody>

                            {{-- Start Opening Balance Input--}}
                            <tr>
                                <td style="white-space: nowrap;font-weight: bold; background: #c4bd97 none repeat scroll 0 0;"
                                    colspan="6"><strong>Opening Balance as per Bank Book:</strong></td>
                                <td style="white-space: nowrap; border: 0.5pt solid #d9d9d9; background: #ffffff none repeat scroll 0 0; text-align: right;font-weight: bold;">
                                    @{{ openingBalance | money_format }}
                                </td>
                            </tr>
                            {{-- End Opening Balance Input--}}

                            <tr style="margin: 0;padding: 0;">
                                <td colspan="7" style="margin: 0;padding: 0;">&nbsp;</td>
                            </tr>

                            {{-- Start Deposited/Received--}}
                            <tr>
                                <td style="white-space: nowrap;color: #002060; font-weight: bold; background: #9bbb59 none repeat scroll 0 0;"
                                    colspan="6">
                                    <strong>Add: Deposited/Received from Corporate Office during This Month
                                        ({{ title_case($selected_month_name) }}):</strong>
                                </td>

                                <td style="white-space: nowrap;color: #002060; font-weight: bold; background: #d8e4bc none repeat scroll 0 0; text-align: right;">
                                    @{{ totalReceivedFromHo | money_format }}
                                </td>
                            </tr>

                            <tr style="background: #dbe6c4 none repeat scroll 0 0;">
                                {{-- Start column for loop --}}
                                @for($i=1;$i<=2;$i++)
                                    <td style="font-weight: bold;text-align: center;">Date</td>
                                    <td style="font-weight: bold;text-align: right;">Amount in Tk,</td>
                                    <td style="font-weight: bold;text-align: center;">Reference No.</td>
                                @endfor
                                {{-- End column for loop --}}

                                <td style="font-weight: bold;">&nbsp;</td>
                            </tr>

                            {{-- Start for loop for each--}}
                            @php
                                $sumRow1 = $sumRow2 = 0;
                            @endphp

                            @foreach($deposited_received_from_corporates->sortBy('entry_date')->chunk(2) as $chunk)
                                <tr style="background: #b7dee8 none repeat scroll 0 0;">
                                    @foreach($chunk as $item)
                                        {{ Form::hidden('bank_book_withdrawn['.$loop->parent->iteration.']['.$loop->iteration.'][line_number]', $item->id) }}
                                        <td style="white-space: nowrap;text-align: center;">
                                            {{ Form::text('bank_book_withdrawn['.$loop->parent->iteration.']['.$loop->iteration.'][date]', $item->entry_date === null ? null : $item->entry_date_format, ['class' => 'datepicker','data-week-start'=>'6', 'data-autoclose'=>'true', 'data-today-highlight'=>'true', 'data-date-format'=>'dd-M-yyyy', 'placeholder'=>'select date','style'=>'padding:0','autocomplete'=>'off','readonly' => 'readonly','disabled'=>'disabled']) }}</td>
                                        <td style="white-space: nowrap;text-align: right;">
                                            {{ Form::number('bank_book_withdrawn['.$loop->parent->iteration.']['.$loop->iteration.'][amount]', $item->amount, ['class' => 'deposited_received_from_corporate-'.$loop->iteration,'min'=>0,'','@input'=>'calculateDepositedReceivedFromCorporate()','readonly' => 'readonly','disabled'=>'disabled']) }}</td>
                                        <td style="white-space: nowrap;text-align: center;">
                                            {{ Form::text('bank_book_withdrawn['.$loop->parent->iteration.']['.$loop->iteration.'][ref_no]', $item->ref_no, ['class' => '','readonly' => 'readonly','disabled'=>'disabled']) }}
                                        </td>

                                        @if($loop->first === true)
                                            @php
                                                $sumRow1 += $item->amount;
                                            @endphp
                                        @endif

                                        @if($loop->last === true)
                                            @php
                                                $sumRow2 += $item->amount;
                                            @endphp
                                        @endif

                                    @endforeach
                                    <td>&nbsp;</td>
                                </tr>
                            @endforeach

                            <tr style="background: #c4d79b none repeat scroll 0 0;">

                                <td>&nbsp;</td>
                                <td class="text-right"><strong>{{ _money_format($sumRow1) }}</strong></td>
                                <td>&nbsp;</td>

                                <td>&nbsp;</td>
                                <td class="text-right"><strong>{{ _money_format($sumRow2) }}</strong></td>
                                <td>&nbsp;</td>

                                <td>&nbsp;</td>
                            </tr>
                            {{-- End Deposited/Received--}}


                            <tr style="margin: 0;padding: 0;">
                                <td colspan="7" style="margin: 0;padding: 0;">&nbsp;</td>
                            </tr>

                            {{-- Start Bank Interest--}}
                            <tr>
                                <td colspan="5"
                                    style="white-space: nowrap;color: #0070c0; background: #c4d79b none repeat scroll 0 0;">
                                    <strong>Add: Bank Interest for the Month ({{ title_case($selected_month_name) }})
                                        (<span style="font-style: italic;">if Any</span>):</strong>
                                </td>
                                <td>{{ Form::number('bank_interest', $$module_name_singular->bank_interest, ['class' => '','v-model'=>'bankInterest','min'=>0,'readonly' => 'readonly','disabled'=>'disabled']) }}</td>
                                <td style="white-space: nowrap;text-align: right;color: #002060; font-weight: bold; background: #d8e4bc none repeat scroll 0 0;">
                                    @{{ bankInterest | money_format }}
                                </td>
                            </tr>
                            {{-- End Bank Interest--}}

                            <tr style="margin: 0;padding: 0">
                                <td colspan="7" style="margin: 0;padding: 0">&nbsp;</td>
                            </tr>

                            {{-- Start Cheque issued (Tranf. to Cash)--}}
                            <tr>
                                <td style="white-space: nowrap;text-align: left; font-weight: bold; background: #4bacc6 none repeat scroll 0 0; "
                                    colspan="6">
                                    Less: <span style="font-weight: bold; font-style: italic;"> Cheque issued (Tranf. to Cash)</span>
                                    <span style="font-weight: bold;"> for the Month ({{ title_case($selected_month_name) }}):</span>
                                </td>
                                <td style="white-space: nowrap;text-align: right;color: #002060; font-weight: bold; background: #92cddc none repeat scroll 0 0;">
                                    @{{ totalChequeIssued | money_format }}
                                </td>
                            </tr>

                            <tr style="background: #b7dee8 none repeat scroll 0 0;">
                                {{-- Start column for loop --}}
                                @for($i=1;$i<=2;$i++)
                                    <td style="font-weight: bold;text-align: center;">Date</td>
                                    <td style="font-weight: bold;text-align: right;">Amount in Tk,</td>
                                    <td style="font-weight: bold;text-align: center;">Reference No.</td>
                                @endfor
                                {{-- End column for loop --}}

                                <td style="font-weight: bold;">&nbsp;</td>
                            </tr>

                            @php
                                $sumRow1 = $sumRow2 = 0;
                            @endphp

                            @foreach($less_issued_checks->sortBy('entry_date')->chunk(2) as $chunk)
                                <tr style="background: #b7dee8 none repeat scroll 0 0;">
                                    @foreach($chunk as $item)
                                        {{ Form::hidden('bank_book_withdrawn['.$loop->parent->iteration.']['.$loop->iteration.'][line_number]', $item->id) }}
                                        <td style="white-space: nowrap;text-align: center;">
                                            {{ Form::text('bank_book_withdrawn['.$loop->parent->iteration.']['.$loop->iteration.'][date]', $item->entry_date === null ? null : $item->entry_date_format, ['class' => 'datepicker','data-week-start'=>'6', 'data-autoclose'=>'true', 'data-today-highlight'=>'true', 'data-date-format'=>'dd-M-yyyy', 'placeholder'=>'select date','style'=>'padding:0','autocomplete'=>'off','readonly' => 'readonly','disabled'=>'disabled']) }}</td>
                                        <td style="white-space: nowrap;text-align: right;">
                                            {{ Form::number('bank_book_withdrawn['.$loop->parent->iteration.']['.$loop->iteration.'][amount]', $item->amount, ['class' => 'less_check_issued-'.$loop->iteration,'min'=>0,'','@input'=>'calculateLessCheckIssued()','readonly' => 'readonly','disabled'=>'disabled']) }}</td>
                                        <td style="white-space: nowrap;text-align: center;">
                                            {{ Form::text('bank_book_withdrawn['.$loop->parent->iteration.']['.$loop->iteration.'][ref_no]', $item->ref_no, ['class' => '','readonly' => 'readonly','disabled'=>'disabled']) }}
                                        </td>

                                        @if($loop->first === true)
                                            @php
                                                $sumRow1 += $item->amount;
                                            @endphp
                                        @endif

                                        @if($loop->last === true)
                                            @php
                                                $sumRow2 += $item->amount;
                                            @endphp
                                        @endif

                                    @endforeach
                                    <td>&nbsp;</td>
                                </tr>
                            @endforeach


                            <tr style="background: #4bacc6 none repeat scroll 0 0;">
                                <td>&nbsp;</td>
                                <td class="text-right"><strong>{{ _money_format($sumRow1) }}</strong></td>
                                <td>&nbsp;</td>

                                <td>&nbsp;</td>
                                <td class="text-right"><strong>{{ _money_format($sumRow2) }}</strong></td>
                                <td>&nbsp;</td>

                                <td>&nbsp;</td>
                            </tr>
                            {{-- End Cheque issued (Tranf. to Cash)--}}

                            <tr style="margin: 0;padding: 0">
                                <td colspan="7" style="margin: 0;padding: 0">&nbsp;</td>
                            </tr>


                            {{-- Start Cheque issued (Tranf. to Cash)--}}
                            <tr>
                                <td style="white-space: nowrap;text-align: left; font-weight: bold; background: #4bacc6 none repeat scroll 0 0; "
                                    colspan="6">
                                    Less: <span style="font-weight: bold; font-style: italic;"> Cheque issued (Payment By CQ)</span>
                                    <span style="font-weight: bold;"> for the Month ({{ title_case($selected_month_name) }}):</span>
                                </td>
                                <td style="white-space: nowrap;text-align: right;color: #002060; font-weight: bold; background: #92cddc none repeat scroll 0 0;">
                                    @{{ totalChequeIssuedCQ | money_format }}
                                </td>
                            </tr>

                            <tr style="background: #b7dee8 none repeat scroll 0 0;">
                                {{-- Start column for loop --}}
                                @for($i=1;$i<=2;$i++)
                                    <td style="font-weight: bold;text-align: center;">Date</td>
                                    <td style="font-weight: bold;text-align: right;">Amount in Tk,</td>
                                    <td style="font-weight: bold;text-align: center;">Reference No.</td>
                                @endfor
                                {{-- End column for loop --}}

                                <td style="font-weight: bold;">&nbsp;</td>
                            </tr>

                            @php
                                $sumRow1 = $sumRow2 = 0;
                            @endphp

                            @foreach($less_issued_cq_checks->sortBy('entry_date')->chunk(2) as $chunk)
                                <tr style="background: #b7dee8 none repeat scroll 0 0;">
                                    @foreach($chunk as $item)
                                        {{ Form::hidden('bank_book_withdrawn['.$loop->parent->iteration.']['.$loop->iteration.'][line_number]', $item->id) }}
                                        <td style="white-space: nowrap;text-align: center;">
                                            {{ Form::text('bank_book_withdrawn['.$loop->parent->iteration.']['.$loop->iteration.'][date]', $item->entry_date === null ? null : $item->entry_date_format, ['class' => 'datepicker','data-week-start'=>'6', 'data-autoclose'=>'true', 'data-today-highlight'=>'true', 'data-date-format'=>'dd-M-yyyy', 'placeholder'=>'select date','style'=>'padding:0','autocomplete'=>'off','readonly' => 'readonly','disabled'=>'disabled']) }}</td>
                                        <td style="white-space: nowrap;text-align: right;">
                                            {{ Form::number('bank_book_withdrawn['.$loop->parent->iteration.']['.$loop->iteration.'][amount]', $item->amount, ['class' => 'less_check_issuedCQ-'.$loop->iteration,'min'=>0,'','@input'=>'calculateLessCheckIssuedCQ()','readonly' => 'readonly','disabled'=>'disabled']) }}</td>
                                        <td style="white-space: nowrap;text-align: center;">
                                            {{ Form::text('bank_book_withdrawn['.$loop->parent->iteration.']['.$loop->iteration.'][ref_no]', $item->ref_no, ['class' => '','readonly' => 'readonly','disabled'=>'disabled']) }}
                                        </td>

                                        @if($loop->first === true)
                                            @php
                                                $sumRow1 += $item->amount;
                                            @endphp
                                        @endif

                                        @if($loop->last === true)
                                            @php
                                                $sumRow2 += $item->amount;
                                            @endphp
                                        @endif

                                    @endforeach
                                    <td>&nbsp;</td>
                                </tr>
                            @endforeach


                            <tr style="background: #4bacc6 none repeat scroll 0 0;">
                                <td>&nbsp;</td>
                                <td class="text-right"><strong>{{ _money_format($sumRow1) }}</strong></td>
                                <td>&nbsp;</td>

                                <td>&nbsp;</td>
                                <td class="text-right"><strong>{{ _money_format($sumRow2) }}</strong></td>
                                <td>&nbsp;</td>

                                <td>&nbsp;</td>
                            </tr>
                            {{-- End Cheque issued (Payment By CQ)--}}

                            <tr style="margin: 0;padding: 0">
                                <td colspan="7" style="margin: 0;padding: 0">&nbsp;</td>
                            </tr>


                            {{-- Start Bank Charges--}}
                            <tr>
                                <td style="text-align: left;color: #0070c0; font-weight: bold; background: #e6b8b7 none repeat scroll 0 0; white-space: nowrap;"
                                    colspan="5">
                                    <strong>Less: Bank Charges/AIT/Excise Duty for the Month
                                        ({{ title_case($selected_month_name) }}) (if Any):</strong>
                                </td>
                                <td>{{ Form::number('bank_charge', $$module_name_singular->bank_charge,['class'=>'','v-model'=>'bankCharge','min'=>0,'readonly' => 'readonly','disabled'=>'disabled']) }}</td>
                                <td style="white-space: nowrap;text-align: right;color: #002060; font-weight: bold; background: #e6b8b7 none repeat scroll 0 0;">
                                    @{{ bankCharge | money_format }}
                                </td>
                            </tr>
                            {{-- End Bank Charges--}}

                            <tr style="margin: 0;padding: 0">
                                <td colspan="7" style="margin: 0;padding: 0">&nbsp;</td>
                            </tr>

                            {{-- Start Closing Balance--}}
                            <tr style="white-space: nowrap;color: #002060; background: #b8cce4 none repeat scroll 0 0;">
                                <td style="text-align: left;" colspan="6">
                                    <strong>Closing Balance as per Bank Book (A):</strong>
                                </td>
                                <td style="white-space: nowrap;text-align: right;font-weight: bold;">@{{
                                    closingBalanceAsPerBankBook | money_format }}
                                </td>
                            </tr>
                            {{-- End Closing Balance--}}

                            <tr style="margin: 0;padding: 0">
                                <td colspan="7" style="margin: 0;padding: 0">&nbsp;</td>
                            </tr>

                            {{-- Start Difference between Bank--}}
                            <tr style="white-space: nowrap;text-align: right;background: #bfbfbf none repeat scroll 0 0;">
                                <td colspan="6" style="text-align: right;color: #974706; font-weight: bold;">Difference
                                    between Bank Book &amp; Bank Statement (B-A):
                                </td>
                                <td style="white-space: nowrap;text-align: right;color: #974706; font-weight: bold;">@{{
                                    diffBookAndStatement | money_format }}
                                </td>
                            </tr>
                            {{-- End Difference between Bank--}}

                            <tr style="margin: 0;padding: 0;">
                                <td colspan="7" style="margin: 0;padding: 0;">&nbsp;</td>
                            </tr>

                            {{-- Start Closing Balance--}}
                            <tr style="white-space: nowrap;background: #fabf8f none repeat scroll 0 0;">
                                <td colspan="5" style="text-align: left;color: #002060; font-weight: bold;">
                                    <strong>Closing Balance as per Bank Statement (B):</strong></td>
                                <td>{{ Form::number('bank_statement_closing_balance', $$module_name_singular->closing_balance, ['class'=>'', 'v-model'=>'closingBalanceAsPerBankStatement','readonly' => 'readonly','disabled'=>'disabled']) }}</td>
                                <td style="white-space: nowrap;text-align: right;font-weight: bold;">@{{
                                    closingBalanceAsPerBankStatement | money_format }}
                                </td>
                            </tr>
                            {{-- End Closing Balance--}}

                            <tr style="margin: 0;padding: 0">
                                <td colspan="7" style="margin: 0;padding: 0">&nbsp;</td>
                            </tr>


                            {{-- Start Deposited but not shown--}}
                            <tr style="white-space: nowrap;background: #8064a2 none repeat scroll 0 0;color: #002060;">
                                <td style="text-align: left;" colspan="6"><strong>Add: Deposited but not shown in Bank
                                        Book:</strong></td>
                                <td style="white-space: nowrap;text-align: right;font-weight: bold;">
                                    @{{ totalDeposited | money_format }}
                                </td>
                            </tr>
                            <tr style="background: #ccc0da none repeat scroll 0 0;">
                                {{-- Start column for loop --}}
                                @for($i=1;$i<=2;$i++)
                                    <td style="font-weight: bold;text-align: center;">Date</td>
                                    <td style="font-weight: bold;text-align: right;">Amount in Tk,</td>
                                    <td style="font-weight: bold;text-align: center;">Reference No.</td>
                                @endfor
                                {{-- End column for loop --}}

                                <td style="font-weight: bold;">&nbsp;</td>
                            </tr>

                            @php
                                $sumBankBookDepositedRow1 = $sumBankBookDepositedRow2 = 0;
                            @endphp

                            @foreach($all_deposits->sortBy('entry_date')->chunk(2) as $chunk)
                                <tr style="background: #b7dee8 none repeat scroll 0 0;">
                                    @foreach($chunk as $item)
                                        {{ Form::hidden('bank_book_deposited['.$loop->parent->iteration.']['.$loop->iteration.'][line_number]', $item->id) }}
                                        <td style="white-space: nowrap;text-align: center;">
                                            {{ Form::text('bank_book_deposited['.$loop->parent->iteration.']['.$loop->iteration.'][date]', $item->entry_date === null ? null : $item->entry_date_format, ['class' => 'datepicker','data-week-start'=>'6', 'data-autoclose'=>'true', 'data-today-highlight'=>'true', 'data-date-format'=>'dd-M-yyyy', 'placeholder'=>'select date','style'=>'padding:0','autocomplete'=>'off','readonly' => 'readonly','disabled'=>'disabled']) }}</td>
                                        <td style="white-space: nowrap;text-align: right;">
                                            {{ Form::number('bank_book_deposited['.$loop->parent->iteration.']['.$loop->iteration.'][amount]', $item->amount, ['class' => 'bank_book_deposited-'.$loop->iteration,'min'=>0,'','@input'=>'calculateBankBookDeposit()','readonly' => 'readonly','disabled'=>'disabled']) }}</td>
                                        <td style="white-space: nowrap;text-align: center;">
                                            {{ Form::text('bank_book_deposited['.$loop->parent->iteration.']['.$loop->iteration.'][ref_no]', $item->ref_no, ['class' => '','readonly' => 'readonly','disabled'=>'disabled']) }}
                                        </td>

                                        @if($loop->first === true)
                                            @php
                                                $sumBankBookDepositedRow1 += $item->amount;
                                            @endphp
                                        @endif

                                        @if($loop->last === true)
                                            @php
                                                $sumBankBookDepositedRow2 += $item->amount;
                                            @endphp
                                        @endif

                                    @endforeach
                                    <td>&nbsp;</td>
                                </tr>
                            @endforeach

                            <tr style="background: #8064a2 none repeat scroll 0 0;color: #000;">
                                <td>&nbsp;</td>
                                <td class="text-right;" style="font-weight: bold;">@{{ sumBankBookDepositedRow1 |
                                    mooney_format }}
                                </td>
                                <td>&nbsp;</td>

                                <td>&nbsp;</td>
                                <td class="text-right;" style="font-weight: bold;">@{{ sumBankBookDepositedRow2 |
                                    mooney_format }}
                                </td>
                                <td>&nbsp;</td>

                                <td>&nbsp;</td>
                            </tr>
                            {{-- End Deposited but not shown--}}


                            <tr style="margin: 0;padding: 0;">
                                <td colspan="7" style="margin: 0;padding: 0;">&nbsp;</td>
                            </tr>

                            {{-- Start Withdrawn but not shown--}}
                            <tr style="white-space: nowrap;background: #79f9ad none repeat scroll 0 0;color: #002060;">
                                <td style="text-align: left;" colspan="6"><strong>Add: Withdrawn not shown in Bank
                                        Book:</strong>
                                </td>
                                <td style="white-space: nowrap;text-align: right;font-weight: bold">
                                    @{{ totalWithdrawn | money_format }}
                                </td>
                            </tr>

                            <tr style="white-space: nowrap;background: #cefcfe none repeat scroll 0 0;">
                                {{-- Start column for loop --}}
                                @for($i=1;$i<=2;$i++)
                                    <td style="font-weight: bold;text-align: center;">Date</td>
                                    <td style="font-weight: bold;text-align: right;">Amount in Tk,</td>
                                    <td style="font-weight: bold;text-align: center;">Reference No.</td>
                                @endfor
                                {{-- End column for loop --}}

                                <td>&nbsp;</td>
                            </tr>

                            @php
                                $sumWithdrawnRow1 = $sumWithdrawnRow2 = 0;
                            @endphp

                            @foreach($all_withdrawn->sortBy('entry_date')->chunk(2) as $chunk)
                                <tr style="background: #b7dee8 none repeat scroll 0 0;">
                                    @foreach($chunk as $item)
                                        {{ Form::hidden('bank_book_withdrawn['.$loop->parent->iteration.']['.$loop->iteration.'][line_number]', $item->id) }}
                                        <td style="white-space: nowrap;text-align: center;">
                                            {{ Form::text('bank_book_withdrawn['.$loop->parent->iteration.']['.$loop->iteration.'][date]', $item->entry_date === null ? null : $item->entry_date_format, ['class' => 'datepicker','data-week-start'=>'6', 'data-autoclose'=>'true', 'data-today-highlight'=>'true', 'data-date-format'=>'dd-M-yyyy', 'placeholder'=>'select date','style'=>'padding:0','autocomplete'=>'off','readonly' => 'readonly','disabled'=>'disabled']) }}</td>
                                        <td style="white-space: nowrap;text-align: right;">
                                            {{ Form::number('bank_book_withdrawn['.$loop->parent->iteration.']['.$loop->iteration.'][amount]', $item->amount, ['class' => 'bank_book_withdrawn-'.$loop->iteration,'min'=>0,'','@input'=>'calculateBankBookWithdrawn()','readonly' => 'readonly','disabled'=>'disabled']) }}</td>
                                        <td style="white-space: nowrap;text-align: center;">
                                            {{ Form::text('bank_book_withdrawn['.$loop->parent->iteration.']['.$loop->iteration.'][ref_no]', $item->ref_no, ['class' => '','readonly' => 'readonly','disabled'=>'disabled']) }}
                                        </td>

                                        @if($loop->first === true)
                                            @php
                                                $sumWithdrawnRow1 += $item->amount;
                                            @endphp
                                        @endif

                                        @if($loop->last === true)
                                            @php
                                                $sumWithdrawnRow2 += $item->amount;
                                            @endphp
                                        @endif

                                    @endforeach
                                    <td>&nbsp;</td>
                                </tr>
                            @endforeach

                            <tr style="white-space: nowrap;background: #79f9ad none repeat scroll 0 0;color:#000;">
                                <td>&nbsp;</td>
                                <td class="text-right;" style="font-weight: bold;">@{{ sumWithdrawnRow1 | mooney_format
                                    }}
                                </td>
                                <td>&nbsp;</td>

                                <td>&nbsp;</td>
                                <td class="text-right;" style="font-weight: bold;">@{{ sumWithdrawnRow2 | mooney_format
                                    }}
                                </td>
                                <td>&nbsp;</td>

                                <td>&nbsp;</td>
                            </tr>
                            {{-- End Withdrawn but not shown--}}


                            <tr style="margin: 0;padding: 0;">
                                <td colspan="7" style="margin: 0;padding: 0">&nbsp;</td>
                            </tr>

                            {{-- Start Cheque issued--}}
                            <tr style="white-space: nowrap;background: #e26b0a none repeat scroll 0 0;">
                                <td colspan="6"><strong>Add: Cheque issued but not Debited by Bank:</strong></td>
                                <td style="white-space: nowrap;text-align: right;color: #974706; font-weight: bold;">
                                    @{{ totalBankChequeIssued | money_format }}
                                </td>
                            </tr>
                            <tr style="white-space: nowrap;background: #fcd5b4;">
                                {{-- Start column for loop --}}
                                @for($i=1;$i<=2;$i++)
                                    <td style="font-weight: bold;text-align: center;">Date</td>
                                    <td style="font-weight: bold;text-align: right;">Amount in Tk,</td>
                                    <td style="font-weight: bold;text-align: center;">Reference No.</td>
                                @endfor
                                {{-- End column for loop --}}

                                <td>&nbsp;</td>
                            </tr>
                            @php
                                $sumChequeIssuedRow1 = $sumChequeIssuedRow2 = 0;
                            @endphp

                            @foreach($all_cheque_issues->sortBy('entry_date')->chunk(2) as $chunk)
                                <tr style="background: #b7dee8 none repeat scroll 0 0;">
                                    @foreach($chunk as $item)
                                        {{ Form::hidden('cheque_issued['.$loop->parent->iteration.']['.$loop->iteration.'][line_number]', $item->id) }}
                                        <td style="white-space: nowrap;text-align: center;">
                                            {{ Form::text('cheque_issued['.$loop->parent->iteration.']['.$loop->iteration.'][date]', $item->entry_date === null ? null : $item->entry_date_format, ['class' => 'datepicker','data-week-start'=>'6', 'data-autoclose'=>'true', 'data-today-highlight'=>'true', 'data-date-format'=>'dd-M-yyyy', 'placeholder'=>'select date','style'=>'padding:0','autocomplete'=>'off','readonly' => 'readonly','disabled'=>'disabled']) }}</td>
                                        <td style="white-space: nowrap;text-align: right;">
                                            {{ Form::number('cheque_issued['.$loop->parent->iteration.']['.$loop->iteration.'][amount]', $item->amount, ['class' => 'cheque_issued-'.$loop->iteration,'min'=>0,'','@input'=>'calculateChequeIssued()','readonly' => 'readonly','disabled'=>'disabled']) }}</td>
                                        <td style="white-space: nowrap;text-align: center;">
                                            {{ Form::text('cheque_issued['.$loop->parent->iteration.']['.$loop->iteration.'][ref_no]', $item->ref_no, ['class' => '','readonly' => 'readonly','disabled'=>'disabled']) }}
                                        </td>

                                        @if($loop->first === true)
                                            @php
                                                $sumChequeIssuedRow1 += $item->amount;
                                            @endphp
                                        @endif

                                        @if($loop->last === true)
                                            @php
                                                $sumChequeIssuedRow2 += $item->amount;
                                            @endphp
                                        @endif

                                    @endforeach
                                    <td>&nbsp;</td>
                                </tr>
                            @endforeach


                            <tr style="background: #fcd5b4;">
                                <td>&nbsp;</td>
                                <td class="text-right;" style="font-weight: bold;">@{{ sumChequeIssuedRow1 |
                                    mooney_format }}
                                </td>
                                <td>&nbsp;</td>

                                <td>&nbsp;</td>
                                <td class="text-right;" style="font-weight: bold;">@{{ sumChequeIssuedRow2 |
                                    mooney_format }}
                                </td>
                                <td>&nbsp;</td>

                                <td>&nbsp;</td>
                            </tr>
                            {{-- End Cheque issued--}}


                            {{-- Start Adjisted closing bank--}}
                            <tr>
                                <td style="white-space: nowrap;text-align: left;color: #002060; background: #b1a0c7 none repeat scroll 0 0;"
                                    colspan="6">
                                    <strong>Adjusted Closing Bank Book Balance:</strong>
                                </td>
                                <td style="white-space: nowrap;text-align: right;color: #974706; background: #ccc0da none repeat scroll 0 0;font-weight: bold;">
                                    @{{ adjustedClosingBookBalance | money_format }}
                                </td>
                            </tr>
                            {{-- End Adjisted closing bank--}}


                            {{-- Start Discrepancy--}}
                            <tr>
                                <td style="white-space: nowrap;text-align: right;color: #ff0000;" colspan="4">
                                    Discrepancy (should be zero)
                                </td>
                                <td>&nbsp;</td>
                                <td style="white-space: nowrap;text-align: right;color: red; background: black none repeat scroll 0 0;font-weight: bold"
                                    colspan="2">@{{ discrepancy | money_format}}
                                </td>
                            </tr>
                            {{-- End Discrepancy--}}


                            {{-- Start Signature--}}
                            <tr style="margin: 0;padding: 0;border: none">
                                <td colspan="7" style="margin: 0;padding: 0;">&nbsp;</td>
                            </tr>
                            <tr style="margin: 0;padding: 0;">
                                <td colspan="7" style="margin: 0;padding: 0;">&nbsp;</td>
                            </tr>

                            <tr style="border:none;;border: none">
                                <td style="border: none;font-weight: bold;text-align: center;" colspan="2">&nbsp;Accounts
                                    In-Charge
                                </td>
                                <td style="border: none;font-weight: bold;text-align: center;" colspan="2">&nbsp;Dy.Branch-in-Charge
                                </td>
                                <td style="border: none;font-weight: bold;text-align: center;" colspan="2">&nbsp;Branch-in-Charge</td>
                            </tr>
                            {{-- End Signature--}}

                            </tbody>
                        </table>
                    </div>

                </div>
                <br>

                <br><br>
                {{ Form::close() }}

            @endcomponent
        </div><!-- END column -->
    </div><!-- .row -->
@endsection

@section('after-scripts')
    <script type="text/javascript">
        new Vue({
            el: '#app',
            data: {
                loading: false,
                accountNumber: '',
                selectedBankId: '',
                selectedBranchId: '',
                selectedMonth: '',

                openingBalance: {{ $opening_balance ?? '0' }},
                totalReceivedFromHo: {{ $deposited_received_from_corporates->sum('amount') ?? '0' }},
                bankInterest: {{ $$module_name_singular->bank_interest }},
                totalChequeIssued: {{ $less_issued_checks->sum('amount') ?? '0' }},
                totalChequeIssuedCQ: {{ $less_issued_cq_checks->sum('amount') ?? '0' }},
                bankCharge: {{ $$module_name_singular->bank_charge }},

                totalDeposited: 0,
                totalWithdrawn: 0,
                totalBankChequeIssued: 0,

                closingBalanceAsPerBankBook: {{ $$module_name_singular->closing_per_bank_book }},
                diffBookAndStatement: '',
                closingBalanceAsPerBankStatement: {{ $$module_name_singular->closing_balance }},

                sumBankBookDepositedRow1: {{ $sumBankBookDepositedRow1 }},
                sumBankBookDepositedRow2: {{ $sumBankBookDepositedRow2 }},

                sumWithdrawnRow1: {{ $sumWithdrawnRow1 }},
                sumWithdrawnRow2: {{ $sumWithdrawnRow2 }},

                sumChequeIssuedRow1: {{ $sumChequeIssuedRow1 }},
                sumChequeIssuedRow2: {{ $sumChequeIssuedRow2 }},

                sumLessChequeIssuedRow1: {{ $sumRow1 }},
                sumLessChequeIssuedRow2: {{ $sumRow2 }},

                sumLessChequeIssuedCQRow1: {{ $sumRow1 }},
                sumLessChequeIssuedCQRow2: {{ $sumRow2 }},

                adjustedClosingBookBalance: '',
                discrepancy: '',

                disableSubmitBtn: false
            },
            mounted: function () {
                this.calculateClosingBalance();
                this.calculateBankBookDeposit();
                this.calculateBankBookWithdrawn();
                this.calculateChequeIssued();

                let $select2 = {width: '100%'};
                this.loading = false;
                this.disableSubmitBtn = false;
                $("#branch").select2($select2).on("change", (e) => {
                    this.selectedBranchId = $(e.currentTarget).val();
                    this.getBanks();
                });
                $("#banks").select2($select2).on("change", (e) => {
                    this.selectedBankId = $(e.currentTarget).val();
                    this.getAccountNumber();
                });
            },
            methods: {
                getBanks: function () {
                    this.loading = true;
                    this.disableSubmitBtn = true;
                    // $("#banks").prop("disabled", true);

                    let config = {
                        //  headers: {'Authorization': 'JWT ' + this.$store.state.token},
                        params: {
                            branch: this.selectedBranchId,
                        }
                    };

                    axios.get('{{ url()->current() }}', config)
                        .then((response) => {
                            this.loading = false;
                            this.disableSubmitBtn = false;
                            let data = response.data.map((obj) => {
                                obj.id = obj.id || obj.pk;
                                obj.text = obj.name;
                                return obj;
                            });

                            this.populateBanks(data);
                        }, (error) => {
                            this.loading = false;
                            this.disableSubmitBtn = true;
                            console.log(error);
                        })
                },
                getAccountNumber: function () {
                    this.loading = true;
                    this.disableSubmitBtn = true;
                    // $("#banks").prop("disabled", true);

                    let config = {
                        //  headers: {'Authorization': 'JWT ' + this.$store.state.token},
                        params: {
                            branch: this.selectedBranchId,
                            bank: this.selectedBankId
                        }
                    };
                    axios.get('{{ route($module_route.'.get_account_number') }}', config)
                        .then((response) => {
                            this.loading = false;
                            this.disableSubmitBtn = false;
                            if (_.isEmpty(response.data)) {
                                swal('No Account Number Found!');
                                this.accountNumber = '';
                                this.disableSubmitBtn = true;
                            } else {
                                this.accountNumber = response.data;
                                this.disableSubmitBtn = false;
                            }
                        })
                        .catch(error => {
                            this.loading = false;
                            this.disableSubmitBtn = true;
                        })
                },
                populateBanks: function (data) {
                    let div = $("#banks");
                    div.prop("disabled", false);
                    div.select2().empty();
                    div.select2({
                        data: data,
                        width: '100%'
                    }).trigger("change");
                },
                checkRecord: function () {
                    if (_.isEmpty(this.accountNumber)) {
                        swal('Account number should not be empty!');
                        return false;
                    }

                    if (_.isEmpty(this.selectedMonth)) {
                        swal('Please select a month!');
                        return false;
                    }

                    this.loading = true;
                    this.disableSubmitBtn = true;

                    let config = {
                        //  headers: {'Authorization': 'JWT ' + this.$store.state.token},
                        params: {
                            branch: this.selectedBranchId,
                            bank: this.selectedBankId,
                            month: this.selectedMonth,
                            accountNumber: this.accountNumber
                        }
                    };
                    axios.get('{{ route($module_route.'.check_entry') }}', config)
                        .then((response) => {
                            this.loading = false;
                            this.disableSubmitBtn = false;
                            console.log(response.data);

                            this.$refs.checkForm.submit();


                            // if (_.isEmpty(response.data)) {
                            //     swal('No Account Number Found!');
                            //     this.accountNumber = '';
                            //     $("#submitBtn").prop("disabled", true);
                            // }
                            // else {
                            //     this.accountNumber = response.data;
                            //     $("#submitBtn").prop("disabled", false);
                            // }
                        })
                        .catch(error => {
                            this.loading = false;
                            this.disableSubmitBtn = true;
                        })
                },

                calculateClosingBalance: function () {
                    this.closingBalanceAsPerBankBook = this.openingBalance + this.totalReceivedFromHo + this.bankInterest - this.totalChequeIssued - this.totalChequeIssuedCQ - this.bankCharge;

                    this.diffBookAndStatement = this.closingBalanceAsPerBankBook - this.closingBalanceAsPerBankStatement;

                    this.adjustedClosingBookBalance = this.closingBalanceAsPerBankBook + this.totalDeposited - this.totalWithdrawn + this.totalBankChequeIssued;

                    this.discrepancy = this.closingBalanceAsPerBankStatement - this.adjustedClosingBookBalance;
                },

                calculateBankBookDeposit: function () {
                    let sumRow1 = sumRow2 = 0;
                    $('.bank_book_deposited-1').each(function () {
                        let val = $(this).val();
                        if (val === '') val = 0;
                        sumRow1 += parseFloat(val);
                    });
                    $('.bank_book_deposited-2').each(function () {
                        let val = $(this).val();
                        if (val === '') val = 0;
                        sumRow2 += parseFloat(val);
                    });
                    this.sumBankBookDepositedRow1 = sumRow1;
                    this.sumBankBookDepositedRow2 = sumRow2;
                    this.totalDeposited = this.sumBankBookDepositedRow1 + this.sumBankBookDepositedRow2;
                    this.calculateClosingBalance();
                },
                calculateBankBookWithdrawn: function () {
                    let sumRow1 = sumRow2 = 0;
                    $('.bank_book_withdrawn-1').each(function () {
                        let val = $(this).val();
                        if (val === '') val = 0;
                        sumRow1 += parseFloat(val);
                    });
                    $('.bank_book_withdrawn-2').each(function () {
                        let val = $(this).val();
                        if (val === '') val = 0;
                        sumRow2 += parseFloat(val);
                    });
                    this.sumWithdrawnRow1 = sumRow1;
                    this.sumWithdrawnRow2 = sumRow2;
                    this.totalWithdrawn = this.sumWithdrawnRow1 + this.sumWithdrawnRow2;
                    this.calculateClosingBalance();
                },
                calculateChequeIssued: function () {
                    let sumRow1 = sumRow2 = 0;
                    $('.cheque_issued-1').each(function () {
                        let val = $(this).val();
                        if (val === '') val = 0;
                        sumRow1 += parseFloat(val);
                    });
                    $('.cheque_issued-2').each(function () {
                        let val = $(this).val();
                        if (val === '') val = 0;
                        sumRow2 += parseFloat(val);
                    });
                    this.sumChequeIssuedRow1 = sumRow1;
                    this.sumChequeIssuedRow2 = sumRow2;
                    this.totalBankChequeIssued = this.sumChequeIssuedRow1 + this.sumChequeIssuedRow2;
                    this.calculateClosingBalance();
                },
                calculateLessCheckIssuedCQ: function () {
                    let sumRow1 = sumRow2 = 0;
                    $('.less_check_issuedCQ-1').each(function () {
                        let val = $(this).val();
                        if (val === '') val = 0;
                        sumRow1 += parseFloat(val);
                    });
                    $('.less_check_issuedCQ-2').each(function () {
                        let val = $(this).val();
                        if (val === '') val = 0;
                        sumRow2 += parseFloat(val);
                    });
                    this.sumLessChequeIssuedCQRow1 = sumRow1;
                    this.sumLessChequeIssuedCQRow2 = sumRow2;
                    this.totalChequeIssuedCQ = this.sumLessChequeIssuedCQRow1 + this.sumLessChequeIssuedCQRow2;
                    this.calculateClosingBalance();
                },
                calculateDepositedReceivedFromCorporate: function () {
                    let sumRow1 = sumRow2 = 0;
                    $('.deposited_received_from_corporate-1').each(function () {
                        let val = $(this).val();
                        if (val === '') val = 0;
                        sumRow1 += parseFloat(val);
                    });
                    $('.deposited_received_from_corporate-2').each(function () {
                        let val = $(this).val();
                        if (val === '') val = 0;
                        sumRow2 += parseFloat(val);
                    });
                    this.sumDepositedReceivedFromCorporateRow1 = sumRow1;
                    this.sumDepositedReceivedFromCorporateRow2 = sumRow2;
                    this.totalReceivedFromHo = this.sumDepositedReceivedFromCorporateRow1 + this.sumDepositedReceivedFromCorporateRow2;
                    this.calculateClosingBalance();
                },
                calculateLessCheckIssued: function () {
                    let sumRow1 = sumRow2 = 0;
                    $('.less_check_issued-1').each(function () {
                        let val = $(this).val();
                        if (val === '') val = 0;
                        sumRow1 += parseFloat(val);
                    });
                    $('.less_check_issued-2').each(function () {
                        let val = $(this).val();
                        if (val === '') val = 0;
                        sumRow2 += parseFloat(val);
                    });
                    this.sumLessChequeIssuedRow1 = sumRow1;
                    this.sumLessChequeIssuedRow2 = sumRow2;
                    this.totalChequeIssued = this.sumLessChequeIssuedRow1 + this.sumLessChequeIssuedRow2;
                    this.calculateClosingBalance();
                },
            },
            watch: {
                bankInterest: function () {
                    this.calculateClosingBalance();
                },
                bankCharge: function () {
                    this.calculateClosingBalance();
                },
                closingBalanceAsPerBankStatement: function () {
                    this.calculateClosingBalance();
                },
                discrepancy: function (value) {
                    // this.disableSubmitBtn = value !== 0;
                }
            },
            filters: {
                money_format: function (x) {
                    return '৳ ' + parseFloat(x === '' ? 0 : x).toLocaleString();
                }
            }
        });

        //add required

        // $("input[required='required'], select").each(function () {
        //     $(this).before('<span class="required">*</span>');
        // });
    </script>
@endsection
