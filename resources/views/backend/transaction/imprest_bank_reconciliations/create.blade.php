@extends ('backend.layouts.app')

@section ('title',$page_heading)

@section('after-styles')
    <style type="text/css">
        .input-group-addon {
            padding: 3px 5px !important;
        }

        .required {
            content: "*";
            position: absolute;
            top: 11px;
            right: 3px;
            font-size: 15px;
            color: red;
            font-weight: 700;
        }

        .input-group .required {
            right: -9px;
        }

        .no-top-border {
            border-top: none !important;
        }

        .no-bottom-border {
            border-bottom: none !important;
        }

        .table th, .table td {
            vertical-align: middle;
        }

        table {
            color: #000;
        }

        .datepicker9 {
            padding: 2px;
            border-radius: 0;
            direction: ltr;
            background-color: #fff;
            border: 1px solid #7a7a7a;
            margin: 0;
            font-family: inherit;
            font-size: inherit;
            line-height: inherit;
            height: 27px;
        }
    </style>
@endsection

@section('content')
    @component('includes.components.widget')
        @slot('title', $page_heading)

        @slot('action_buttons')
            @include($module_view.'.header-buttons')
        @endslot
        <div id="transactionForm">
            @unless(!$show_check_form)
                {{ Form::open(['route' => $module_route.'.generate','class' => 'form-horizontal', 'role' => 'form', 'method' => 'post','ref'=>'checkForm']) }}
                <div class="row">
                    <div class="col-sm-12">
                        <table class="table table-bordered">
                            <tbody>

                            <tr>
                                <td><strong>BRANCH:</strong></td>
                                <td> {!! Form::select('branch_id', $branches, null, ['class' => 'form-control exclude-select2','id'=>'branch','placeholder'=>'Select a branch','v-model'=>'selectedBranchId']) !!}</td>

                                <td><strong>BANK:</strong></td>
                                <td>
                                    {!! Form::select('bank_id', [], null, ['class' => 'form-control exclude-select2','id'=>'banks','placeholder'=>'Select a bank','v-model'=>'selectedBankId']) !!}
                                </td>

                                <td><strong>ACCOUNT NO.:</strong></td>
                                <td>{!! Form::text('account_number', null, ['class' => 'form-control exclude-select2','id'=>'account_number','readonly'=>'readonly','v-model'=>'accountNumber']) !!}</td>
                            </tr>

                            <tr>
                                <td><strong>YEAR:</strong></td>
                                <td>
                                    {!! Form::selectRange('year', 2018, now()->year, now()->year-1, ['class' => 'form-control exclude-select2','id'=>'year','placeholder'=>'Select a year', 'v-model'=>'selectedYear']) !!}
                                </td>

                                <td><strong>MONTH:</strong></td>
                                <td>
                                    {!! Form::selectMonth('month', null, ['class' => 'form-control exclude-select2','id'=>'month','placeholder'=>'Select a month','v-model'=>'selectedMonth']) !!}
                                </td>

                                <td>&nbsp;</td>
                                <td class="text-left">
                                    {{ Form::button('Check',['class'=>'btn btn-success','@click'=>'checkRecord',':disabled'=>'disableSubmitBtn === true']) }}
                                </td>

                            </tr>

                            </tbody>
                        </table>
                    </div>
                </div>
                {{ Form::close() }}
            @endunless

            @unless(!$show_input_form)
                {{ Form::open(['route' => $module_route.'.store', 'role' => 'form', 'method' => 'post']) }}

                <br>
                <div class="row">
                    {{ Form::hidden('branch_id', $selected_branch_id) }}
                    {{ Form::hidden('branch_code', $selected_branch_code) }}
                    {{ Form::hidden('branch_name', $selected_branch_name) }}
                    {{ Form::hidden('branch_short_name', $selected_branch_short_name) }}
                    {{ Form::hidden('bank_id', $selected_bank_id) }}
                    {{ Form::hidden('year', $selected_year) }}
                    {{ Form::hidden('month', $selected_month_id) }}
                    {{ Form::hidden('account_number', $selected_account) }}

                    {{ Form::hidden('opening_balance', $opening_balance) }}
                    {{ Form::hidden('total_received_from_ho', $total_all_received_from_ho) }}
                    {{ Form::hidden('total_cheque_issued', $total_cheque_issued, ['v-model'=>'totalChequeIssued']) }}

                    {{ Form::hidden('closing_balance_book', 0,['v-model'=>'closingBalanceAsPerBankBook']) }}

                    @php
                        $all_received_from_ho = collect($all_received_from_ho);
                        $selected_month_dates = collect($selected_month_dates);
                    @endphp

                    <div class="col-sm-12">
                        <table class="table">
                            <tbody>
                            <tr>
                                <td colspan="2" class="no-top-border">
                                    <h6 class="mb-0">TRANSCOM DISTRIBUTION COMPANY LIMITED</h6>
                                </td>

                                <td class="no-top-border" width="130">&nbsp;</td>

                                <td colspan="2" class="no-top-border">
                                    <h6 class="mb-0">IMPREST BANK RECONCILIATION STATEMENT</h6>
                                </td>
                            </tr>
                            <tr>
                                <td><strong>BRANCH NAME:</strong></td>
                                <td><strong>{{ $selected_branch_name_code }}</strong></td>

                                <td class="no-top-border" width="130">&nbsp;</td>

                                <td><strong>ACCOUNT NUMBER:</strong></td>
                                <td><strong>{{ $selected_account_number }}</strong></td>
                            </tr>
                            <tr>
                                <td><strong>BANK NAME:</strong></td>
                                <td><strong>{{ $selected_bank_name }}</strong></td>

                                <td class="no-top-border" width="130">&nbsp;</td>

                                <td style="font-weight: bold; background: #00b050 none repeat scroll 0 0; ">
                                    <strong>CLOSING BALANCE AS PER BANK:</strong></td>
                                <td class="text-right" style=""><strong>@{{ closingBalanceAsPerBankStatement |
                                        money_format }}</strong></td>
                            </tr>
                            <tr>
                                <td><strong>MONTH:</strong></td>
                                <td><strong>{{ $selected_month_name }}</strong></td>

                                <td class="no-top-border" width="130">&nbsp;</td>

                                <td style="font-weight: bold; background: #00b050 none repeat scroll 0 0; ">
                                    <strong>CLOSING BALANCE AS PER BANK BOOK:</strong></td>
                                <td class="text-right" style=""><strong>@{{ closingBalanceAsPerBankBook | money_format
                                        }}</strong></td>
                            </tr>
                            </tbody>
                        </table>

                        <table class="table table-bordered table-condensed">
                            <tbody>

                            {{-- Start Opening Balance Input--}}
                            <tr>
                                <td style="white-space: nowrap;font-weight: bold; background: #c4bd97 none repeat scroll 0 0;"
                                    colspan="6"><strong>Opening Balance as per Bank Book:</strong></td>
                                <td style="white-space: nowrap; border: 0.5pt solid #d9d9d9; background: #ffffff none repeat scroll 0 0; text-align: right;font-weight: bold;">
                                    @{{ openingBalance | money_format }}
                                </td>
                            </tr>
                            {{-- End Opening Balance Input--}}

                            <tr style="margin: 0;padding: 0;">
                                <td colspan="7" style="margin: 0;padding: 0;">&nbsp;</td>
                            </tr>

                            {{-- Start Deposited/Received--}}
                            <tr>
                                <td style="white-space: nowrap;color: #002060; font-weight: bold; background: #9bbb59 none repeat scroll 0 0;"
                                    colspan="6">
                                    <strong>Add: Deposited/Received from Corporate Office during This Month
                                        ({{ title_case($selected_month_name) }}):</strong>
                                </td>

                                <td style="white-space: nowrap;color: #002060; font-weight: bold; background: #d8e4bc none repeat scroll 0 0; text-align: right;">
                                    @{{ totalReceivedFromHo | money_format }}
                                </td>
                            </tr>

                            <tr style="background: #dbe6c4 none repeat scroll 0 0;">
                                {{-- Start column for loop --}}
                                @for($i=1;$i<=2;$i++)
                                    <td style="font-weight: bold;text-align: center;">Date</td>
                                    <td style="font-weight: bold;text-align: right;">Amount in Tk</td>
                                    <td style="font-weight: bold;text-align: center;">Reference No.</td>
                                @endfor
                                {{-- End column for loop --}}

                                <td style="font-weight: bold;">&nbsp;</td>
                            </tr>

                            {{-- Start for loop for each--}}
                            @php
                                $sumRow1 = $sumRow2 = 0;
                            @endphp

                            {{-- tr used style: background: #dbe6c4 none repeat scroll 0 0; --}}
                            @for($j=0;$j<5;$j++)
                                <tr style="background: #dbe6c4 none repeat scroll 0 0;">
                                    @for($i=1;$i<=2;$i++)
                                        <td style="white-space: nowrap;text-align: center;">
                                            {{ Form::text('deposited_received_from_corporate['.$i.']['.$j.'][date]', null, ['class' => 'datepicker9','data-week-start'=>'6', 'data-autoclose'=>'true', 'data-today-highlight'=>'true', 'data-date-format'=>'dd-M-yyyy', 'placeholder'=>'select date','style'=>'padding:0','autocomplete'=>'off']) }}
                                        </td>
                                        {{--                                            {{ Form::hidden('deposited_received_from_corporate['.$date->format('d-M-Y').'][date]', $date->format('Y-m-d')) }}--}}
                                        <td>{{ Form::number('deposited_received_from_corporate['.$i.']['.$j.'][amount]', null, ['class' => 'deposited_received_from_corporate-'.$i,'min'=>0,'step'=>'any','@input'=>'calculateDepositedReceivedFromCorporate()']) }}</td>
                                        <td>{{ Form::text('deposited_received_from_corporate['.$i.']['.$j.'][ref_no]', null, ['class' => '']) }}</td>
                                    @endfor
                                    <td>&nbsp;</td>
                                </tr>
                            @endfor

                            <tr style="background: #c4d79b none repeat scroll 0 0;">

                                <td>&nbsp;</td>
                                <td class="text-right"><strong>@{{ sumDepositedReceivedFromCorporateRow1 | money_format
                                        }}</strong></td>
                                <td>&nbsp;</td>

                                <td>&nbsp;</td>
                                <td class="text-right"><strong>@{{ sumDepositedReceivedFromCorporateRow2 | money_format
                                        }}</strong></td>
                                <td>&nbsp;</td>

                                <td>&nbsp;</td>
                            </tr>
                            {{-- End Deposited/Received--}}


                            <tr style="margin: 0;padding: 0;">
                                <td colspan="7" style="margin: 0;padding: 0;">&nbsp;</td>
                            </tr>

                            {{-- Start Bank Interest--}}
                            <tr>
                                <td colspan="5"
                                    style="text-align: left;white-space: nowrap;color: #0070c0; background: #c4d79b none repeat scroll 0 0;">
                                    <strong>Add: Bank Interest for the Month ({{ title_case($selected_month_name) }})
                                        (<span style="font-style: italic;">if Any</span>):</strong>
                                </td>
                                <td>{{ Form::number('bank_interest', null, ['class' => '','v-model'=>'bankInterest','min'=>0,'step'=>'any']) }}</td>
                                <td style="white-space: nowrap;text-align: right;color: #002060; font-weight: bold; background: #d8e4bc none repeat scroll 0 0;">
                                    @{{ bankInterest | money_format }}
                                </td>
                            </tr>
                            {{-- End Bank Interest--}}

                            <tr style="margin: 0;padding: 0">
                                <td colspan="7" style="margin: 0;padding: 0">&nbsp;</td>
                            </tr>

                            {{-- Start Cheque issued (Tranf. to Cash)--}}
                            <tr style="white-space: nowrap;font-weight: bold; background: #4bacc6 none repeat scroll 0 0; ">
                                <td style="text-align: left;" colspan="6">Less: <span style="font-style: italic;"> Cheque issued (Tranf. to Cash)</span>
                                    <span> for the Month ({{ title_case($selected_month_name) }}):</span>
                                </td>
                                <td style="text-align: right">@{{ totalChequeIssued | money_format }}</td>
                            </tr>

                            @php
                                $sumRow1 = $sumRow2 = 0;
                            @endphp

                            <tr style="background: #b7dee8 none repeat scroll 0 0;">
                                {{-- Start column for loop --}}
                                @for($i=1;$i<=2;$i++)
                                    <td style="font-weight: bold;text-align: center;">Date</td>
                                    <td style="font-weight: bold;text-align: right;">Amount in Tk</td>
                                    <td style="font-weight: bold;text-align: center;">Reference No.</td>
                                @endfor
                                {{-- End column for loop --}}

                                <td style="font-weight: bold;">&nbsp;</td>
                            </tr>

                            {{-- Start for loop for each--}}
                            @php
                                $sumRow1 = $sumRow2 = 0;
                            @endphp

                            @for($j=0;$j<5;$j++)
                                <tr style="background: #dbe6c4 none repeat scroll 0 0;">
                                    @for($i=1;$i<=2;$i++)
                                        <td style="white-space: nowrap;text-align: center;">
                                            {{ Form::text('less_check_issued['.$i.']['.$j.'][date]', null, ['class' => 'datepicker9','data-week-start'=>'6', 'data-autoclose'=>'true', 'data-today-highlight'=>'true', 'data-date-format'=>'dd-M-yyyy', 'placeholder'=>'select date','style'=>'padding:0','autocomplete'=>'off']) }}
                                        </td>
                                        {{--                                            {{ Form::hidden('deposited_received_from_corporate['.$date->format('d-M-Y').'][date]', $date->format('Y-m-d')) }}--}}
                                        <td>{{ Form::number('less_check_issued['.$i.']['.$j.'][amount]', null, ['class' => 'less_check_issued-'.$i,'min'=>0,'step'=>'any','@input'=>'calculateLessCheckIssued()']) }}</td>
                                        <td>{{ Form::text('less_check_issued['.$i.']['.$j.'][ref_no]', null, ['class' => '']) }}</td>
                                    @endfor
                                    <td>&nbsp;</td>
                                </tr>
                            @endfor

                            <tr style="background: #4bacc6 none repeat scroll 0 0;">
                                <td>&nbsp;</td>
                                <td class="text-right"><strong>@{{ sumLessChequeIssuedRow1 | money_format }}</strong>
                                </td>
                                <td>&nbsp;</td>

                                <td>&nbsp;</td>
                                <td class="text-right"><strong>@{{ sumLessChequeIssuedRow2 | money_format }}</strong>
                                </td>
                                <td>&nbsp;</td>

                                <td>&nbsp;</td>
                            </tr>
                            {{-- End Cheque issued (Tranf. to Cash)--}}

                            <tr style="margin: 0;padding: 0;">
                                <td colspan="7" style="margin: 0;padding: 0;">&nbsp;</td>
                            </tr>

                            <br>

                            {{-- Start Cheque issued (Payment By CQ)--}}
                            <tr style="white-space: nowrap;font-weight: bold; background: #4bacc6 none repeat scroll 0 0; ">
                                <td style="text-align: left;" colspan="6">Less: <span style="font-style: italic;"> Cheque issued (Payment By CQ)</span>
                                    <span> for the Month ({{ title_case($selected_month_name) }}):</span>
                                </td>
                                <td style="text-align: right">@{{ totalChequeIssuedCQ | money_format }}</td>
                            </tr>

                            @php
                                $sumRow1 = $sumRow2 = 0;
                            @endphp

                            <tr style="background: #b7dee8 none repeat scroll 0 0;">
                                {{-- Start column for loop --}}
                                @for($i=1;$i<=2;$i++)
                                    <td style="font-weight: bold;text-align: center;">Date</td>
                                    <td style="font-weight: bold;text-align: right;">Amount in Tk</td>
                                    <td style="font-weight: bold;text-align: center;">Reference No.</td>
                                @endfor
                                {{-- End column for loop --}}

                                <td style="font-weight: bold;">&nbsp;</td>
                            </tr>

                            {{-- Start for loop for each--}}
                            @php
                                $sumRow1 = $sumRow2 = 0;
                            @endphp

                            @for($j=0;$j<5;$j++)
                                <tr style="background: #dbe6c4 none repeat scroll 0 0;">
                                    @for($i=1;$i<=2;$i++)
                                        <td style="white-space: nowrap;text-align: center;">
                                            {{ Form::text('less_check_issuedCQ['.$i.']['.$j.'][date]', null, ['class' => 'datepicker9','data-week-start'=>'6', 'data-autoclose'=>'true', 'data-today-highlight'=>'true', 'data-date-format'=>'dd-M-yyyy', 'placeholder'=>'select date','style'=>'padding:0','autocomplete'=>'off']) }}
                                        </td>
                                        {{--                                            {{ Form::hidden('deposited_received_from_corporate['.$date->format('d-M-Y').'][date]', $date->format('Y-m-d')) }}--}}
                                        <td>{{ Form::number('less_check_issuedCQ['.$i.']['.$j.'][amount]', null, ['class' => 'less_check_issuedCQ-'.$i,'min'=>0,'step'=>'any','@input'=>'calculateLessCheckIssuedCQ()']) }}</td>
                                        <td>{{ Form::text('less_check_issuedCQ['.$i.']['.$j.'][ref_no]', null, ['class' => '']) }}</td>
                                    @endfor
                                    <td>&nbsp;</td>
                                </tr>
                            @endfor

                            <tr style="background: #4bacc6 none repeat scroll 0 0;">
                                <td>&nbsp;</td>
                                <td class="text-right"><strong>@{{ sumLessChequeIssuedCQRow1 | money_format }}</strong>
                                </td>
                                <td>&nbsp;</td>

                                <td>&nbsp;</td>
                                <td class="text-right"><strong>@{{ sumLessChequeIssuedCQRow2 | money_format }}</strong>
                                </td>
                                <td>&nbsp;</td>

                                <td>&nbsp;</td>
                            </tr>
                            {{-- End Cheque issued (Payment By CQ)--}}

                            <tr style="margin: 0;padding: 0;">
                                <td colspan="7" style="margin: 0;padding: 0;">&nbsp;</td>
                            </tr>


                            {{-- Start Bank Charges--}}
                            <tr>
                                <td style="text-align: left;color: #0070c0; font-weight: bold; background: #e6b8b7 none repeat scroll 0 0; white-space: nowrap;"
                                    colspan="5">
                                    <strong>Less: Bank Charges/AIT/Excise Duty for the Month
                                        ({{ title_case($selected_month_name) }}) (if Any):</strong>
                                </td>
                                <td>{{ Form::number('bank_charge',null,['class'=>'','v-model'=>'bankCharge','min'=>0,'step'=>'any']) }}</td>
                                <td style="white-space: nowrap;text-align: right;color: #002060; font-weight: bold; background: #e6b8b7 none repeat scroll 0 0;">
                                    @{{ bankCharge | money_format }}
                                </td>
                            </tr>
                            {{-- End Bank Charges--}}

                            <tr style="margin: 0;padding: 0;">
                                <td colspan="7" style="margin: 0;padding: 0">&nbsp;</td>
                            </tr>

                            {{-- Start Closing Balance--}}
                            <tr style="white-space: nowrap;color: #002060; background: #b8cce4 none repeat scroll 0 0;">
                                <td style="text-align: left;" colspan="6">
                                    <strong>Closing Balance as per Bank Book (A):</strong>
                                </td>
                                <td style="white-space: nowrap;text-align: right;font-weight: bold;">@{{
                                    closingBalanceAsPerBankBook | money_format }}
                                </td>
                            </tr>
                            {{-- End Closing Balance--}}

                            <tr style="margin: 0;padding: 0;">
                                <td colspan="7" style="margin: 0;padding: 0">&nbsp;</td>
                            </tr>

                            {{-- Start Difference between Bank--}}
                            <tr style="white-space: nowrap;text-align: right;background: #bfbfbf none repeat scroll 0 0;">
                                <td colspan="6" style="text-align: right;color: #974706; font-weight: bold;">Difference
                                    between Bank Book &amp; Bank Statement (B-A):
                                </td>
                                <td style="white-space: nowrap;text-align: right;color: #974706; font-weight: bold;">@{{
                                    diffBookAndStatement | money_format
                                    }}
                                </td>
                            </tr>
                            {{-- End Difference between Bank--}}

                            <tr style="margin: 0;padding: 0;">
                                <td colspan="7" style="margin: 0;padding: 0;">&nbsp;</td>
                            </tr>

                            {{-- Start Closing Balance--}}
                            <tr style="white-space: nowrap;background: #fabf8f none repeat scroll 0 0;">
                                <td colspan="5" style="text-align: left;color: #002060; font-weight: bold;">
                                    <strong>Closing Balance as per Bank Statement (B):</strong></td>
                                <td>{{ Form::number('bank_statement_closing_balance', null, ['class'=>'', 'v-model'=>'closingBalanceAsPerBankStatement','min'=>0,'step'=>'any']) }}</td>
                                <td style="white-space: nowrap;text-align: right;font-weight: bold;">@{{
                                    closingBalanceAsPerBankStatement | money_format }}
                                </td>
                            </tr>
                            {{-- End Closing Balance--}}

                            <tr style="margin: 0;padding: 0">
                                <td colspan="7" style="margin: 0;padding: 0">&nbsp;</td>
                            </tr>


                            {{-- Start Deposited but not shown--}}
                            <tr style="white-space: nowrap;background: #8064a2 none repeat scroll 0 0;color: #002060;">
                                <td style="text-align: left;" colspan="6"><strong>Add: Deposited but not shown in Bank
                                        Book:</strong></td>
                                <td style="white-space: nowrap;text-align: right;font-weight: bold;">
                                    @{{ totalDeposited | money_format }}
                                </td>
                            </tr>
                            <tr style="background: #ccc0da none repeat scroll 0 0;">
                                {{-- Start column for loop --}}
                                @for($i=1;$i<=2;$i++)
                                    <td style="font-weight: bold;text-align: center;">Date</td>
                                    <td style="font-weight: bold;text-align: right;">Amount in Tk</td>
                                    <td style="font-weight: bold;text-align: center;">Reference No.</td>
                                @endfor
                                {{-- End column for loop --}}

                                <td style="font-weight: bold;">&nbsp;</td>
                            </tr>

                            @for($i=1;$i<=5;$i++)
                                {{-- used tr style: background: #ccc0da none repeat scroll 0 0; --}}
                                <tr>
                                    @for($j=1;$j<=2;$j++)
                                        <td>{{ Form::text('bank_book_deposited['.$i.']['.$j.'][date]', null, ['class' => 'datepicker','data-week-start'=>'6', 'data-autoclose'=>'true', 'data-today-highlight'=>'true', 'data-date-format'=>'dd-M-yyyy', 'placeholder'=>'select date','style'=>'padding:0','autocomplete'=>'off']) }}</td>
                                        <td>{{ Form::number('bank_book_deposited['.$i.']['.$j.'][amount]', null, ['class' => 'bank_book_deposited-'.$j,'min'=>0,'step'=>'any','@input'=>'calculateBankBookDeposit()']) }}</td>
                                        <td>{{ Form::text('bank_book_deposited['.$i.']['.$j.'][ref_no]', null, ['class' => '']) }}</td>
                                    @endfor

                                    <td>&nbsp;</td>
                                </tr>
                            @endfor

                            <tr style="background: rgb(128,100,162) none repeat scroll 0 0;">

                                <td>&nbsp;</td>
                                <td class="text-right"><strong>@{{ sumBankBookDepositedRow1 | money_format }}</strong>
                                </td>
                                <td>&nbsp;</td>

                                <td>&nbsp;</td>
                                <td class="text-right"><strong>@{{ sumBankBookDepositedRow2 | money_format }}</strong>
                                </td>
                                <td>&nbsp;</td>

                                <td>&nbsp;</td>
                            </tr>
                            {{-- End Deposited but not shown--}}

                            <tr style="margin: 0;padding: 0">
                                <td colspan="7" style="margin: 0;padding: 0">&nbsp;</td>
                            </tr>

                            {{-- Start Withdrawn but not shown--}}
                            <tr style="white-space: nowrap;background: #79f9ad none repeat scroll 0 0;color: #002060;">
                                <td style="text-align: left;" colspan="6"><strong>Less: Debited by Bank but not
                                        showing in Bank Book:</strong>
                                </td>
                                <td style="white-space: nowrap;text-align: right;font-weight: bold">
                                    @{{ totalWithdrawn | money_format }}
                                </td>
                            </tr>
                            <tr style="white-space: nowrap;background: #cefcfe none repeat scroll 0 0;">
                                {{-- Start column for loop --}}
                                @for($i=1;$i<=2;$i++)
                                    <td style="font-weight: bold;text-align: center;">Date</td>
                                    <td style="font-weight: bold;text-align: right;">Amount in Tk</td>
                                    <td style="font-weight: bold;text-align: center;">Reference No.</td>
                                @endfor
                                {{-- End column for loop --}}

                                <td>&nbsp;</td>
                            </tr>

                            @for($i=1;$i<=5;$i++)
                                {{-- tr style used: background: #cefcfe none repeat scroll 0 0;--}}
                                <tr>
                                    @for($j=1;$j<=2;$j++)
                                        <td>{{ Form::text('bank_book_withdrawn['.$i.']['.$j.'][date]', null, ['class' => 'datepicker','data-week-start'=>'6', 'data-autoclose'=>'true', 'data-today-highlight'=>'true', 'data-date-format'=>'dd-M-yyyy', 'placeholder'=>'select date','style'=>'padding:0','autocomplete'=>'off']) }}</td>
                                        <td>{{ Form::number('bank_book_withdrawn['.$i.']['.$j.'][amount]', null, ['class' => 'bank_book_withdrawn-'.$j,'min'=>0,'step'=>'any','@input'=>'calculateBankBookWithdrawn()']) }}</td>
                                        <td>{{ Form::text('bank_book_withdrawn['.$i.']['.$j.'][ref_no]', null, ['class' => '']) }}</td>
                                    @endfor

                                    <td>&nbsp;</td>
                                </tr>
                            @endfor

                            <tr style="white-space: nowrap;background: #79f9ad none repeat scroll 0 0;">

                                <td>&nbsp;</td>
                                <td class="text-right"><strong>@{{ sumWithdrawnRow1 | money_format }}</strong></td>
                                <td>&nbsp;</td>

                                <td>&nbsp;</td>
                                <td class="text-right"><strong>@{{ sumWithdrawnRow2 | money_format }}</strong></td>
                                <td>&nbsp;</td>

                                <td>&nbsp;</td>
                            </tr>
                            {{-- End Withdrawn but not shown--}}

                            <tr style="margin: 0;padding: 0;">
                                <td colspan="7" style="margin: 0;padding: 0">&nbsp;</td>
                            </tr>

                            {{-- Start Cheque issued--}}
                            <tr style="white-space: nowrap;background: #e26b0a none repeat scroll 0 0;">
                                <td colspan="6"><strong>Add: Cheque issued but not Debited by Bank:</strong></td>
                                <td style="white-space: nowrap;text-align: right;color: #974706; font-weight: bold;">
                                    @{{ totalBankChequeIssued | money_format }}
                                </td>
                            </tr>

                            <tr style="background: #fcd5b4 none repeat scroll 0 0;">
                                {{-- Start column for loop --}}
                                @for($i=1;$i<=2;$i++)
                                    <td style="font-weight: bold;text-align: center;">Date</td>
                                    <td style="font-weight: bold;text-align: right;">Amount in Tk,</td>
                                    <td style="font-weight: bold;text-align: center;">Reference No.</td>
                                @endfor
                                {{-- End column for loop --}}

                                <td>&nbsp;</td>
                            </tr>
                            @for($i=1;$i<=5;$i++)
                                {{-- used tr style: background: #fcd5b4 none repeat scroll 0 0; --}}
                                <tr>
                                    @for($j=1;$j<=2;$j++)
                                        <td>{{ Form::text('cheque_issued['.$i.']['.$j.'][date]', null, ['class' => 'datepicker','data-week-start'=>'6', 'data-autoclose'=>'true', 'data-today-highlight'=>'true', 'data-date-format'=>'dd-M-yyyy', 'placeholder'=>'select date','style'=>'padding:0','autocomplete'=>'off']) }}</td>
                                        <td>{{ Form::number('cheque_issued['.$i.']['.$j.'][amount]', null, ['class' => 'cheque_issued-'.$j,'min'=>0,'step'=>'any','@input'=>'calculateChequeIssued()']) }}</td>
                                        <td>{{ Form::text('cheque_issued['.$i.']['.$j.'][ref_no]', null, ['class' => '']) }}</td>
                                    @endfor

                                    <td>&nbsp;</td>
                                </tr>
                            @endfor

                            <tr style="background: #e26b0a;">

                                <td>&nbsp;</td>
                                <td class="text-right"><strong>@{{ sumChequeIssuedRow1 | money_format }}</strong></td>
                                <td>&nbsp;</td>

                                <td>&nbsp;</td>
                                <td class="text-right"><strong>@{{ sumChequeIssuedRow2 | money_format }}</strong></td>
                                <td>&nbsp;</td>

                                <td>&nbsp;</td>
                            </tr>
                            {{-- End Cheque issued--}}


                            {{-- Start Adjisted closing bank--}}
                            <tr>
                                <td style="white-space: nowrap;text-align: left;color: #002060; background: #b1a0c7 none repeat scroll 0 0;"
                                    colspan="6">
                                    <strong>Adjusted Closing Bank Book Balance:</strong>
                                </td>
                                <td style="white-space: nowrap;text-align: right;color: #974706; background: #ccc0da none repeat scroll 0 0;font-weight: bold">
                                    @{{ adjustedClosingBookBalance | money_format }}
                                </td>
                            </tr>
                            {{-- End Adjisted closing bank--}}


                            {{-- Start Discrepancy--}}
                            <tr>
                                <td style="white-space: nowrap;text-align: right;color: #ff0000;" colspan="4">
                                    Discrepancy (should be zero)
                                </td>
                                <td>&nbsp;</td>
                                <td style="white-space: nowrap;text-align: right;color: red; background: black none repeat scroll 0 0;font-weight: bold;"
                                    colspan="2">@{{ discrepancy | money_format}}
                                </td>
                            </tr>
                            {{-- End Discrepancy--}}


                            {{-- Start Signature--}}
                            {{--<tr style="margin: 0;padding: 0;">--}}
                            {{--<td colspan="7" style="margin: 0;padding: 0;">&nbsp;</td>--}}
                            {{--</tr>--}}
                            {{--<tr style="margin: 0;padding: 0;">--}}
                            {{--<td colspan="7" style="margin: 0;padding: 0;">&nbsp;</td>--}}
                            {{--</tr>--}}

                            {{--<tr style="border:none;">--}}
                            {{--<td style="border: none;font-weight: bold;text-align: center;" colspan="2">&nbsp;Accounts In-Charge</td>--}}
                            {{--<td style="border: none;font-weight: bold;text-align: center;" colspan="2">&nbsp;Dy.Branch-in-Charge--}}
                            {{--</td>--}}
                            {{--<td style="border: none;font-weight: bold;text-align: center;" colspan="2">&nbsp;Branch-in-Charge</td>--}}
                            {{--</tr>--}}
                            {{-- End Signature--}}

                            </tbody>
                        </table>
                    </div>

                    <br><br>
                </div>

                <div class="row">
                    <div class="col-lg-6">
                        {!! Form::button(('<i class="fa fa-check fa-fw"></i>&nbsp;'.$module_action), ['class' => 'btn btn-success pull-right','type'=>'submit','ref'=>'submitBtn']) !!}
                        {{--{!! Form::button(('<i class="fa fa-check fa-fw"></i>&nbsp;'.$module_action), ['class' => 'btn btn-success pull-right','type'=>'submit',':disabled'=>'disableSubmitBtn1','ref'=>'submitBtn']) !!}--}}
                    </div>

                    <div class="col-lg-6">
                        {!! link_to(route($module_route.'.index'), ('<i class="fa fa-mail-reply"></i>&nbsp;'.trans('buttons.general.cancel')), ['class' => 'btn btn-danger pull-left'], null, false) !!}
                    </div><!--col-lg-1-->
                </div><!--form control-->
                <br>

                {{ Form::close() }}
            @endunless
        </div>
    @endcomponent
@endsection

@section('after-scripts')
    <script type="text/javascript">
        new Vue({
            el: '#app',
            data: {
                loading: false,
                accountNumber: '',
                selectedBankId: '',
                selectedBranchId: '',
                selectedMonth: '',
                selectedYear: '',

                openingBalance: {{ $opening_balance ?? '0' }},
                {{--totalReceivedFromHo: {{ $total_all_received_from_ho ?? '0' }},--}}
                totalReceivedFromHo: 0,
                bankInterest: '',
                {{--totalChequeIssued: {{ $total_cheque_issued ?? '0' }},--}}
                totalChequeIssued: 0,
                totalChequeIssuedCQ: 0,
                bankCharge: '',

                closingBalanceAsPerBankBook: '',
                diffBookAndStatement: '',
                closingBalanceAsPerBankStatement: '',

                totalDeposited: '',
                totalWithdrawn: '',
                totalBankChequeIssued: '',

                sumBankBookDepositedRow1: '',
                sumBankBookDepositedRow2: '',
                sumWithdrawnRow1: '',
                sumWithdrawnRow2: '',
                sumChequeIssuedRow1: '',
                sumChequeIssuedRow2: '',
                sumLessChequeIssuedRow1: '',
                sumLessChequeIssuedRow2: '',
                sumLessChequeIssuedCQRow1: '',
                sumLessChequeIssuedCQRow2: '',
                sumDepositedReceivedFromCorporateRow1: '',
                sumDepositedReceivedFromCorporateRow2: '',

                adjustedClosingBookBalance: '',
                discrepancy: '',

                disableSubmitBtn: false
            },
            mounted: function () {
                this.calculateClosingBalance();

                let $select2 = {width: '100%'};
                this.loading = false;
                this.disableSubmitBtn = true;
                $("#branch").select2($select2).on("change", (e) => {
                    this.selectedBranchId = $(e.currentTarget).val();
                    this.getBanks();
                });
                $("#banks").select2($select2).on("change", (e) => {
                    this.selectedBankId = $(e.currentTarget).val();
                    this.getAccountNumber();
                })
            },
            methods: {
                getBanks: function () {
                    this.loading = true;
                    this.disableSubmitBtn = true;
                    // $("#banks").prop("disabled", true);

                    let config = {
                        //  headers: {'Authorization': 'JWT ' + this.$store.state.token},
                        params: {
                            branch: this.selectedBranchId,
                        }
                    };

                    axios.get('{{ url()->current() }}', config)
                        .then((response) => {
                            this.loading = false;
                            this.disableSubmitBtn = false;
                            let data = response.data.map((obj) => {
                                obj.id = obj.id || obj.pk;
                                obj.text = obj.name;
                                return obj;
                            });

                            this.populateBanks(data);
                        }, (error) => {
                            this.loading = false;
                            this.disableSubmitBtn = true;
                            console.log(error);
                        })
                },
                getAccountNumber: function () {
                    this.loading = true;
                    this.disableSubmitBtn = true;
                    // $("#banks").prop("disabled", true);

                    let config = {
                        //  headers: {'Authorization': 'JWT ' + this.$store.state.token},
                        params: {
                            branch: this.selectedBranchId,
                            bank: this.selectedBankId
                        }
                    };
                    axios.get('{{ route($module_route.'.get_account_number') }}', config)
                        .then((response) => {
                            this.loading = false;
                            this.disableSubmitBtn = false;
                            if (_.isEmpty(response.data)) {
                                swal.fire('Warning','No Account Number Found!','warning');
                                this.accountNumber = '';
                                this.disableSubmitBtn = true;
                            } else {
                                this.accountNumber = response.data;
                                this.disableSubmitBtn = false;
                            }
                        })
                        .catch(error => {
                            this.loading = false;
                            this.disableSubmitBtn = true;
                        })
                },
                populateBanks: function (data) {
                    let div = $("#banks");
                    div.prop("disabled", false);
                    div.select2().empty();
                    div.select2({
                        data: data,
                        width: '100%'
                    }).trigger("change");
                },
                checkRecord: function () {
                    if (_.isEmpty(this.accountNumber)) {
                        swal.fire('Warning','Account number should not be empty!','warning');
                        return false;
                    }

                    if (_.isEmpty(this.selectedMonth)) {
                        swal.fire('Warning','Please select a month!','warning');
                        return false;
                    }

                    this.loading = true;
                    this.disableSubmitBtn = true;

                    let config = {
                        //  headers: {'Authorization': 'JWT ' + this.$store.state.token},
                        params: {
                            branch: this.selectedBranchId,
                            bank: this.selectedBankId,
                            month: this.selectedMonth,
                            year: this.selectedYear
                        }
                    };
                    axios.get('{{ route($module_route.'.check_entry') }}', config)
                        .then((response) => {
                            this.loading = false;
                            this.disableSubmitBtn = false;
                            // console.log(response.data);

                            if (response.data == 0) {
                                // this.accountNumber = '';
                                $("#submitBtn").prop("disabled", false);
                                this.$refs.checkForm.submit();
                            } else {
                                // this.accountNumber = response.data;
                                swal.fire('Error','Already Data Entered For Given Month!','error');
                                // $("#submitBtn").prop("disabled", true);
                            }
                        })
                        .catch(error => {
                            this.loading = false;
                            this.disableSubmitBtn = true;
                        })
                },

                calculateClosingBalance: function () {
                    this.closingBalanceAsPerBankBook = this.openingBalance + this.totalReceivedFromHo + this.bankInterest - this.totalChequeIssued - this.totalChequeIssuedCQ - this.bankCharge;

                    this.diffBookAndStatement = this.closingBalanceAsPerBankBook - this.closingBalanceAsPerBankStatement;

                    this.adjustedClosingBookBalance = this.closingBalanceAsPerBankBook + this.totalDeposited - this.totalWithdrawn + this.totalBankChequeIssued;

                    this.discrepancy = this.closingBalanceAsPerBankStatement - this.adjustedClosingBookBalance;
                },

                calculateBankBookDeposit: function () {
                    let sumRow1 = sumRow2 = 0;
                    $('.bank_book_deposited-1').each(function () {
                        let val = $(this).val();
                        if (val === '') val = 0;
                        sumRow1 += parseFloat(val);
                    });
                    $('.bank_book_deposited-2').each(function () {
                        let val = $(this).val();
                        if (val === '') val = 0;
                        sumRow2 += parseFloat(val);
                    });
                    this.sumBankBookDepositedRow1 = sumRow1;
                    this.sumBankBookDepositedRow2 = sumRow2;
                    this.totalDeposited = this.sumBankBookDepositedRow1 + this.sumBankBookDepositedRow2;
                    this.calculateClosingBalance();
                },
                calculateBankBookWithdrawn: function () {
                    let sumRow1 = sumRow2 = 0;
                    $('.bank_book_withdrawn-1').each(function () {
                        let val = $(this).val();
                        if (val === '') val = 0;
                        sumRow1 += parseFloat(val);
                    });
                    $('.bank_book_withdrawn-2').each(function () {
                        let val = $(this).val();
                        if (val === '') val = 0;
                        sumRow2 += parseFloat(val);
                    });
                    this.sumWithdrawnRow1 = sumRow1;
                    this.sumWithdrawnRow2 = sumRow2;
                    this.totalWithdrawn = this.sumWithdrawnRow1 + this.sumWithdrawnRow2;
                    this.calculateClosingBalance();
                },
                calculateChequeIssued: function () {
                    let sumRow1 = sumRow2 = 0;
                    $('.cheque_issued-1').each(function () {
                        let val = $(this).val();
                        if (val === '') val = 0;
                        sumRow1 += parseFloat(val);
                    });
                    $('.cheque_issued-2').each(function () {
                        let val = $(this).val();
                        if (val === '') val = 0;
                        sumRow2 += parseFloat(val);
                    });
                    this.sumChequeIssuedRow1 = sumRow1;
                    this.sumChequeIssuedRow2 = sumRow2;
                    this.totalBankChequeIssued = this.sumChequeIssuedRow1 + this.sumChequeIssuedRow2;
                    this.calculateClosingBalance();
                },
                calculateLessCheckIssued: function () {
                    let sumRow1 = sumRow2 = 0;
                    $('.less_check_issued-1').each(function () {
                        let val = $(this).val();
                        if (val === '') val = 0;
                        sumRow1 += parseFloat(val);
                    });
                    $('.less_check_issued-2').each(function () {
                        let val = $(this).val();
                        if (val === '') val = 0;
                        sumRow2 += parseFloat(val);
                    });
                    this.sumLessChequeIssuedRow1 = sumRow1;
                    this.sumLessChequeIssuedRow2 = sumRow2;
                    this.totalChequeIssued = this.sumLessChequeIssuedRow1 + this.sumLessChequeIssuedRow2;
                    this.calculateClosingBalance();
                },
                calculateLessCheckIssuedCQ: function () {
                    let sumRow1 = sumRow2 = 0;
                    $('.less_check_issuedCQ-1').each(function () {
                        let val = $(this).val();
                        if (val === '') val = 0;
                        sumRow1 += parseFloat(val);
                    });
                    $('.less_check_issuedCQ-2').each(function () {
                        let val = $(this).val();
                        if (val === '') val = 0;
                        sumRow2 += parseFloat(val);
                    });
                    this.sumLessChequeIssuedCQRow1 = sumRow1;
                    this.sumLessChequeIssuedCQRow2 = sumRow2;
                    this.totalChequeIssuedCQ = this.sumLessChequeIssuedCQRow1 + this.sumLessChequeIssuedCQRow2;
                    this.calculateClosingBalance();
                },
                calculateDepositedReceivedFromCorporate: function () {
                    let sumRow1 = sumRow2 = 0;
                    $('.deposited_received_from_corporate-1').each(function () {
                        let val = $(this).val();
                        if (val === '') val = 0;
                        sumRow1 += parseFloat(val);
                    });
                    $('.deposited_received_from_corporate-2').each(function () {
                        let val = $(this).val();
                        if (val === '') val = 0;
                        sumRow2 += parseFloat(val);
                    });
                    this.sumDepositedReceivedFromCorporateRow1 = sumRow1;
                    this.sumDepositedReceivedFromCorporateRow2 = sumRow2;
                    this.totalReceivedFromHo = this.sumDepositedReceivedFromCorporateRow1 + this.sumDepositedReceivedFromCorporateRow2;
                    this.calculateClosingBalance();
                }

            },
            watch: {
                loading: function (val) {

                },
                bankInterest: function (newVal) {
                    this.bankInterest = isNaN(newVal) ? 0 : parseFloat(newVal);
                    this.calculateClosingBalance();
                },
                bankCharge: function () {
                    this.calculateClosingBalance();
                },
                closingBalanceAsPerBankStatement: function () {
                    this.calculateClosingBalance();
                },
                discrepancy: function (value) {
                    this.disableSubmitBtn = value !== 0;
                }
            },
            filters: {
                money_format: function (x) {
                    return '৳ ' + parseFloat(x === '' ? 0 : x).toLocaleString();
                }
            }
        });

        let el = $(".datepicker9");
        el.datepicker({
            weekStart: el.data('week-start') || 0,
            autoclose: el.data('autoclose') || false,
            todayHighlight: el.data('today-highlight') || false,
            orientation: 'bottom' // Position issue when using BS4, set it to bottom until officially supported
        });

        //add required

        // $("input[required='required'], select").each(function () {
        //     $(this).before('<span class="required">*</span>');
        // });
    </script>
@endsection
