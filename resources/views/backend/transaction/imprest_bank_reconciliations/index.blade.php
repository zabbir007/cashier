@extends ('backend.layouts.app')

@section ('title', $page_heading)

@section('after-styles')
    <style type="text/css">
        table td, table tr {
            white-space: nowrap;
        }

        div.dataTables_filter {
            text-align: left !important;
        }

        .dataTables_wrapper .dataTables_processing {
            position: absolute;
            top: 50%;
            left: 50%;
            width: 300px;
            height: 60px;
            margin-left: -50%;
            margin-top: -25px;
            padding-top: 20px;
            text-align: center;
            font-size: 1.2em;
            border: 1px solid rgb(51, 51, 51) !important;
            background: #eeeeee linear-gradient(to right, rgba(255, 255, 255, 0) 0%, rgba(255, 255, 255, 0.9) 25%, rgba(255, 255, 255, 0.9) 75%, rgba(255, 255, 255, 0) 100%) !important;
        }

        table.dataTable thead .sorting::before, table.dataTable thead .sorting_asc::before, table.dataTable thead .sorting_desc::before, table.dataTable thead .sorting_asc_disabled::before, table.dataTable thead .sorting_desc_disabled::before {
            right: 0;
            content: "\2191";
        }
        .block-content{
            overflow-x: auto;
        }
    </style>
@endsection

@section('content')
    <div class="row">
        <!-- DOM dataTable -->
        <div class="col-md-12">
            @component('includes.components.widget')

                @slot('title')
                    {{ $page_heading }}
                @endslot

                @slot('action_buttons')
                    @include($module_view.'.header-buttons')
                @endslot

                {{--Widget body start--}}
                <div class="table-responsive">
                    <table id="datatable" class="table table-condensed table-hover table-striped table-bordered"
                           style="{{ isset($style) ? $style : '' }}" cellspacing="0" width="100%">
                        <thead>
                        <tr>
                            <th rowspan="2" style="font-size: 14px;vertical-align: middle;" class="text-center">SL</th>
                            <th rowspan="2" style="font-size: 14px;vertical-align: middle;" class="text-center">Date</th>
                            <th rowspan="2" style="font-size: 14px;vertical-align: middle;" class="text-center">Branch</th>
                            <th rowspan="2" style="font-size: 14px;vertical-align: middle;" class="text-center">Bank</th>
                            <th rowspan="2" style="font-size: 14px;vertical-align: middle;" class="text-center">Account Number</th>
                            <th rowspan="2" style="font-size: 14px;vertical-align: middle;" class="text-center">Opening Balance</th>
                            <th colspan="2" style="font-size: 14px;vertical-align: middle;" class="text-center">Closing Balance</th>
                            <th rowspan="2" style="font-size: 14px;vertical-align: middle;">Month</th>
                            <th rowspan="2" style="font-size: 14px;vertical-align: middle;">{{ __('labels.general.actions') }}</th>
                        </tr>
                        <tr>
                            <td>As Per Statement</td>
                            <td>As Per Book</td>
                            <td></td>
                        </tr>
                        </thead>

                    </table>
                </div>

            @endcomponent
        </div><!-- END column -->
    </div><!-- .row -->
@endsection

@section('after-scripts')
    <script type="text/javascript">
        $(function () {
            $('#datatable').DataTable({
                dom: "<'row'<'col-sm-12 col-md-4 text-left'f><'col-sm-12 col-md-4'><'col-sm-12 col-md-4 text-right'l>>" +
                    "<'row'<'col-sm-12'tr>>" +
                    "<'row'<'col-sm-12 col-md-5'i><'col-sm-12 col-md-7'p>>",
                language: {
                    'loadingRecords': '&nbsp;',
                    'processing': '<i class="fa fa-spinner fa-spin fa-2x fa-fw"></i>Loading...',
                },
                fixedHeader: true,
                pageLength: 25,
                processing: true,
                serverSide: true,
                autoWidth: false,
                ajax: {
                    url: '{{ route($module_route.'.get') }}',
                    type: 'post',
                    data: {status: 1, trashed: false},
                    error: function (xhr, err) {
                        if (err === 'parsererror')
                            location.reload();
                    }
                },
                columns: [
                    {data: 'sl', searchable: true, sortable: true},
                    {data: 'trans_date', searchable: true, sortable: true},
                    {data: 'branch_name', searchable: true, sortable: true},
                    {data: 'bank_name', searchable: true, sortable: true},
                    {data: 'account_number', searchable: true, sortable: true},
                    {data: 'opening_balance', searchable: false, sortable: true, className: 'text-right'},
                    {data: 'closing_per_bank', searchable: false, sortable: true, className: 'text-right'},
                    {data: 'closing_per_bank_book', searchable: false, sortable: true, className: 'text-right'},
                    {data: 'month_name', searchable: true, sortable: true},
                    {data: 'actions', searchable: false, sortable: false}
                ],
                order: [
                    [1, "desc"],
                    [2, "asc"],
                ]
            });
        });
    </script>
@endsection
