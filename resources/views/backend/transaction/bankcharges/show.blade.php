@extends ('backend.layouts.app')

@section ('title', $page_heading)

@section('page-header')
    <h1>
        {{ $page_heading }}
    </h1>

@endsection

@section('content')
    <div class="row">
        <div class="col-md-12">
            @component('includes.components.widget')

                @slot('title')
                    {{ $page_heading }}
                @endslot

                @slot('action_buttons')
                    @if($module_name_singular->hasPermission() && access()->allow('menu-transaction-bank-charge-edit'))
                        <a href="{{ route($module_route.'.edit', $module_name_singular) }}" class="btn-block-option"><i
                                    class="si si-note" data-toggle="tooltip"
                                    data-placement="top"></i>&nbsp;{{ trans('buttons.general.crud.edit') }}</a>
                    @endif
                    @if(access()->allow('menu-transaction-bank-charge-approve'))
                        <a href="{{ route($module_route.'.approve', $module_name_singular) }}" class="btn-block-option"><i
                                    class="si si-check" data-toggle="tooltip"
                                    data-placement="top"></i>&nbsp;Approve</a>
                    @endif

                    <a href="{{ route($module_route.'.index') }}" class="btn-block-option"><i class="si si-grid"></i>&nbsp;All {{ title_case($module_name) }}
                    </a>
                    <button type="button" class="btn-block-option" onclick="Codebase.helpers('print-page');">
                        <i class="si si-printer"></i> Print
                    </button>
                    <button type="button" class="btn-block-option" data-toggle="block-option"
                            data-action="fullscreen_toggle"></button>
                    <button type="button" class="btn-block-option" data-toggle="block-option" data-action="state_toggle"
                            data-action-mode="demo">
                        <i class="si si-refresh"></i>
                    </button>
                @endslot
                {{ Form::model($module_name_singular, ['route' => [$module_route.'.update', $module_name_singular], 'class' => 'form-horizontal', 'role' => 'form', 'method' => 'PATCH']) }}

                <div class="row">
                    <div class="col-lg-7">
                        @component('includes.components.table',['class'=>'table table-borderless','style'=> 'font-size:14px','id'=>''])

                            @slot('tbody')
                                <tr>
                                    <td style="font-weight: bold">
                                        Transaction Date
                                    </td>
                                    <td> :</td>
                                    <td>{{ date('d-M-Y',strtotime($module_name_singular->trans_date)) }}</td>
                                    <td style="font-weight: bold">
                                        Branch Name
                                    </td>
                                    <td> :</td>
                                    <td>{{ $module_name_singular->branch_name }}</td>

                                </tr>
                            @endslot
                        @endcomponent
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-7">
                        @component('includes.components.table',['class'=>'table','id'=>''])
                            @slot('thead')
                                <th style="font-size: 14px">SN</th>
                                <th style="font-size: 14px">Bank Name</th>
                                <th style="font-size: 14px" class="text-right">Amount</th>
                            @endslot

                            @slot('tbody')
                                @foreach($banks as $bank)
                                    @php
                                        $amount = $module_name_singular->details->where('bank_id',$bank->id)->sum('amount');
                                    @endphp
                                    <tr>
                                        <td width="15px"> {{ $loop->iteration }}</td>
                                        <td width="50px"> {{ $bank->name }}</td>
                                        <td width="50px" class="text-right">
                                            <strong>{{ number_format($amount,2) }}</strong>
                                        </td>
                                    </tr>
                                @endforeach

                                <tr>
                                    <td colspan="2" class="text-right"><strong>Grand Total:</strong></td>
                                    <td class="text-right">
                                        <strong>{{ number_format($module_name_singular->details->sum('amount'), 2) }}</strong>
                                    </td>
                                </tr>
                            @endslot
                        @endcomponent

                        <br>

                    </div>
                </div>
                {{ Form::close() }}
            @endcomponent
        </div><!-- END column -->
    </div><!-- .row -->
@endsection
