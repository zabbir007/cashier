@extends ('backend.layouts.app')

@section ('title', $page_heading)

@section('page-header')
    <h1>
        {{ $page_heading }}
    </h1>
@endsection

@section('content')
    @component('includes.components.widget')
        @slot('title')
            {{ $page_heading }}
        @endslot

        @slot('action_buttons')
            <div class="btn-group float-right">
                @if(access()->allow('menu-transaction-bank-charge-show'))
                    <a href="{{ route($module_route.'.show', $module_name_singular) }}"
                       class="btn btn-alt-success btn-sm"><i
                            class="si si-eye" data-toggle="tooltip"
                            data-placement="top"></i>&nbsp;Show</a>
                @endif
                <a href="{{ route($module_route.'.index') }}" class="btn btn-alt-primary btn-sm"
                   data-toggle="click-ripple"><i class="si si-grid"></i>&nbsp;All Transactions</a>
                <a href="{{ route($module_route.'.create') }}" class="btn btn-alt-success btn-sm"
                   data-toggle="click-ripple"><i class="si si-plus"></i>&nbsp;Add New</a>
            </div><!--pull right-->

        @endslot

        {{ Form::model($module_name_singular, ['route' => [$module_route.'.update', $module_name_singular], 'class' => 'form-horizontal', 'role' => 'form', 'method' => 'PATCH']) }}

        <div class="row">
            <div class="col-sm-6">
                <div class="form-group row has-feedback">
                    {!! Form::label('trans_date', 'Transaction Date',['class' => 'col-lg-4 col-form-label']) !!}
                    <div class="col-lg-8">
                        {!! Form::hidden('sl', $module_name_singular->sl, ['class' => 'form-control', 'placeholder'=>'XXXX-XXXX','readonly']) !!}
                        {!! Form::text('trans_date',date('d-M-Y',strtotime($module_name_singular->trans_date)),  ['class' => 'form-control','readonly']) !!}
                        <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
                        <div class="help-block with-errors"></div>
                    </div>
                </div>
            </div>

            <div class="col-sm-6">
                <div class="form-group row has-feedback">
                    {!! Form::label('branch_name', 'Branch Name',['class' => 'col-lg-4 col-form-label']) !!}
                    <div class="col-lg-8">
                        {!! Form::hidden('branch_id',$module_name_singular->branch_id,  ['class' => 'form-control','readonly']) !!}
                        {!! Form::text('branch_name',$module_name_singular->branch_name,  ['class' => 'form-control','readonly']) !!}
                        <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
                        <div class="help-block with-errors"></div>
                    </div>
                </div>
            </div>

        </div>
        <br>
        <div class="row">
            <div class="col-lg-12">
                @component('includes.components.table',['class'=>'table  table-bordered fixed_headers','style'=> 'font-size:12px','id'=>''])
                    @slot('thead')
                        <th style="font-size: 14px">SN</th>
                        <th style="font-size: 14px">Bank Name</th>
                        <th style="font-size: 14px">Amount</th>
                    @endslot

                    @slot('tbody')
                        @foreach($banks as $bank)
                            {{ Form::hidden('data['.$loop->iteration.'][bank_id]', $bank->id) }}
                            {{ Form::hidden('data['.$loop->iteration.'][bank_name]', $bank->name) }}

                            @php
                                $amount = $module_name_singular->details->where('bank_id',$bank->id)->sum('amount');
                            @endphp
                            <tr>
                                <td width="15px"> {{ $loop->iteration }}</td>
                                <td width="50px"> {{ $bank->name }}</td>
                                <td width="50px">
                                    {{ Form::hidden('data['.$loop->iteration.'][old_amount]', $amount) }}
                                    {{ Form::number('data['.$loop->iteration.'][new_amount]', $amount, [
                                    'class' => 'form-control1 text-right',
                                    'min' => 0 ,
                                    'step' => 'any'
                                    ])
                                   }}
                                </td>
                            </tr>
                        @endforeach
                    @endslot
                @endcomponent

                <br>

            </div>
        </div>

        <div class="row">
            <div class="col-lg-6">
                @if($module_name_singular->hasPermission())
                    {!! Form::button(('<i class="fa fa-check fa-fw"></i>&nbsp;'.$module_action), ['class' => 'btn btn-success pull-right','type'=>'submit']) !!}
                @endif
            </div>

            <div class="col-lg-6">
                {!! link_to(route($module_route.'.index'), ('<i class="fa fa-mail-reply"></i>&nbsp;'.trans('buttons.general.cancel')), ['class' => 'btn btn-danger pull-left'], null, false) !!}
            </div><!--col-lg-1-->
        </div><!--form control-->
        <br><br>
        {{ Form::close() }}

    @endcomponent
@endsection
