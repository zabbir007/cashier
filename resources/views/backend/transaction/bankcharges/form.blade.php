<div class="row">
    <div class="col-sm-6">
        <div class="form-group row has-feedback">
            {!! Form::label('trans_date', 'Transaction Date',['class' => 'col-lg-4 col-form-label']) !!}
            <div class="col-lg-8">
                {{--{!! Form::text('trans_date',$carbon->format('d-M-Y'),  ['class' => 'form-control','readonly']) !!}--}}
                {!! Form::text('trans_date',$carbon->format('d-M-Y'),  ['class' => 'form-control js-datepicker','data-week-start'=>'6', 'data-autoclose'=>'true', 'data-today-highlight'=>'true', 'data-date-format'=>'dd-M-yyyy', 'placeholder'=>'mm/dd/yy','readonly']) !!}
                <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
                <div class="help-block with-errors"></div>
            </div>
        </div>
    </div>

    @if(count(access()->user()->branches)>1)
        <div class="col-sm-6">
            <div class="form-group row has-feedback">
                {!! Form::label('branch_name', 'Branch Name',['class' => 'col-lg-4 col-form-label']) !!}
                <div class="col-lg-8">
                    {!! Form::select('branch_id',$branches,old('branch_name'),  ['class' => 'form-control','readonly']) !!}
                    <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
                    <div class="help-block with-errors"></div>
                </div>
            </div>
        </div>
    @else
        <div class="col-sm-6">
            <div class="form-group row has-feedback">
                {!! Form::label('branch_name', 'Branch Name',['class' => 'col-lg-4 col-form-label']) !!}
                <div class="col-lg-8">
                    {!! Form::hidden('branch_id',$branches->id,  ['class' => 'form-control','readonly']) !!}
                    {!! Form::text('branch_name',$branches->name,  ['class' => 'form-control','readonly']) !!}
                    <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
                    <div class="help-block with-errors"></div>
                </div>
            </div>
        </div>
    @endif

</div>
<br>
<div class="row">
    <div class="col-lg-12">
        @component('includes.components.table',['class'=>'table  table-bordered fixed_headers','style'=> 'font-size:12px','id'=>''])
            @slot('thead')
                <th style="font-size: 14px">SN</th>
                <th style="font-size: 14px">Bank Name</th>
                <th style="font-size: 14px">Amount</th>
            @endslot

            @slot('tbody')
                @foreach($banks as $bank)
                    {{ Form::hidden('data['.$loop->iteration.'][bank_id]', $bank->id) }}
                    {{ Form::hidden('data['.$loop->iteration.'][bank_name]', $bank->name) }}

                    <tr>
                        <td width="15px"> {{ $loop->iteration }}</td>
                        <td width="50px"> {{ $bank->name }}</td>
                        <td width="50px">
                            {{ Form::number('data['.$loop->iteration.'][amount]', null, [
                            'class' => 'form-control1 text-right',
                            'placeholder' => '',
                            'min' => 0
                            ,'step' => 'any'
                            ])
                           }}
                        </td>
                    </tr>
                @endforeach
            @endslot
        @endcomponent

        <br>

    </div>
</div>
<br><br>
