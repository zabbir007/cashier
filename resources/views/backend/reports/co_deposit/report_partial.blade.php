<div class="text-center">
    <p class="text-center" style="font-size: 18px">
        Branch Name : <strong>{{ $branch_name }}</strong>
        <br>

        <span style="font-size: 11px">
            {{ $page_heading }}
        </span>
    </p>
</div>

<div class="row">
    <div class="col-lg-12">

        @component('includes.components.table',['class'=>'table table-condensed  table-bordered','id'=>'','tr_class'=>'bg-primary text-bold text-black'])
            @slot('thead')
                <th width="8" class="text-center" style="font-size: 11px">DATE</th>
                {{--@unless(count($branches) > 1)--}}
                {{--<th width="100" class="text-center" style="font-size: 11px">TIME</th>--}}
                {{--<th width="280" class="text-center" style="font-size: 11px">CHECKED BY</th>--}}
                {{--<th class="text-center" style="font-size: 11px">VERIFIED BY (AIC)</th>--}}
                {{--@endunless--}}
                <th width="170" class="text-center" style="font-size: 11px">OPENING BALANCE</th>
                <th width="140" class="text-center" style="font-size: 11px">TOTAL COLLECTION</th>
                <th width="140" class="text-center" style="font-size: 11px">TOTAL DEPOSIT</th>
                <th width="140" class="text-center" style="font-size: 11px">PAYMENT TO IMPREST</th>
                <th width="140" class="text-center" style="font-size: 11px">CLOSING BALANCE</th>

                <th width="140" class="text-center" style="font-size: 11px">ADVANCE RECEIPT</th>
                <th width="140" class="text-center" style="font-size: 11px">ADV ADJUSTMENT</th>

                <th width="140" class="text-center" style="font-size: 11px">AS PER PHYSICAL (BIC/DBIC)</th>
                <th width="140" class="text-center" style="font-size: 11px">DIFFERENCE</th>

            @endslot

            @slot('tbody')
                @php
                    $closing = $total_collection = $total_deposit = $total_adv_receipt = $total_adv_adj = 0;

					$initial_opening = collect($initial_opening);
					$receiptClosing = collect($receiptClosing);
					$depositClosing = collect($depositClosing);
					$allReceipts = collect($receipts);
					$allDeposits = collect($deposits);
                    //updated   $receiptClosing->sum('total_short_receipt')
				//	$current_opening = $initial_opening->sum('total_collection') + $receiptClosing->sum('total_collection')  -
// $receiptClosing->sum('total_short_receipt')  - $depositClosing->sum('total_amount');
                    $current_opening = $initial_opening->sum('total_collection') + $receiptClosing->sum('total_collection') - $depositClosing->sum('total_amount');

                @endphp
                @foreach($dates as $date)
                    @php
                        $collection = $allReceipts->where('trans_date',$date->toDateString())->sum('total_collection');
                        //updated  $shortR = $allReceipts->where('trans_date',$date->toDateString())->sum('total_short_receipt');
                        // $shortR = $allReceipts->where('trans_date',$date->toDateString())->sum('total_short_receipt');
                        //$total_collection += $collection - $shortR;
                        $total_collection += $collection ;

                        $deposit_amount = $allDeposits->where('trans_date',$date->toDateString())->sum('total_amount');
                        $total_deposit += $deposit_amount;

                        $advance_receipt = $allReceipts->where('trans_date',$date->toDateString())->sum('total_advance_receipt');
                        $total_adv_receipt += $advance_receipt;

                        $advance_adjustment = $allReceipts->where('trans_date',$date->toDateString())->sum('total_advance_adj');
                        $total_adv_adj += $advance_adjustment;

                        //$advance_adjustment = $allReceipts->where('trans_date',$date)->sum('total_advance_adj');

                        //$closing_balance = $current_opening + $collection + $advance_receipt - $advance_adjustment - $deposit_amount;
                        $closing_balance = $current_opening + $collection - $deposit_amount;

                    @endphp
                    <tr>
                        <td style="font-size: 10px">&nbsp;{{ $date->format( 'd-m-Y' ) }}&nbsp;</td>
                        {{--@unless(count($branches) > 1)--}}
                        {{--<td class="text-center"></td>--}}
                        {{--<td class="text-center" style="font-size: 10px">&nbsp;{{$createBy}}&nbsp;</td>--}}
                        {{--<td class="text-center" style="font-size: 10px">&nbsp;{{$approveBy}}&nbsp;</td>--}}
                        {{--@endunless--}}
                        <td class="text-right" style="font-size: 10px">&nbsp;{{ number_format($current_opening, 2) }}
                            &nbsp;
                        </td>
                        <td class="text-right" style="font-size: 10px">&nbsp;{{ number_format($collection, 2) }}&nbsp;
                        </td>
                        <td class="text-right" style="font-size: 10px">&nbsp;{{ number_format($deposit_amount, 2) }}
                            &nbsp;
                        </td>

                        <td class="text-center" style="font-size: 10px">&nbsp;-&nbsp;</td>
                        <td class="text-right" style="font-size: 10px">&nbsp;{{ number_format($closing_balance, 2) }}
                            &nbsp;
                        </td>
                        <td class="text-right" style="font-size: 10px">&nbsp;{{ number_format($advance_receipt, 2) }}
                            &nbsp;
                        </td>
                        <td class="text-right" style="font-size: 10px">&nbsp;{{ number_format($advance_adjustment, 2) }}
                            &nbsp;
                        </td>
                        <td class="text-center" style="font-size: 10px">&nbsp;-&nbsp;</td>
                        <td class="text-right" style="font-size: 10px">&nbsp;{{ number_format($closing_balance, 2) }}
                            &nbsp;
                        </td>
                    </tr>
                    @php
                        $current_opening = $closing_balance;
                    @endphp
                @endforeach
                <tr class="bg-success text-bold text-black">
                    <td colspan="2" class="text-right">Total</td>
                    <td class="text-right" style="font-size: 11px">
                        <strong>{{ number_format($total_collection, 2) }}</strong></td>
                    <td class="text-right" style="font-size: 11px">
                        <strong>{{ number_format($total_deposit, 2) }}</strong></td>

                    <td class="text-right">-</td>
                    <td class="text-right">-</td>
                    <td class="text-right" style="font-size: 11px">
                        <strong>{{ number_format($total_adv_receipt, 2) }}</strong></td>
                    <td class="text-right" style="font-size: 11px">
                        <strong>{{ number_format($total_adv_adj, 2) }}</strong></td>
                    <td class="text-right"></td>
                    <td class="text-right"></td>
                </tr>
            @endslot
        @endcomponent

    </div>
</div>
