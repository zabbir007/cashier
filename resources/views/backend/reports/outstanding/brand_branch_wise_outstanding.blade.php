<div class="text-center">
    <p class="text-center" style="font-size: 18px">
        {{ $page_heading }}
        <br>

        <span style="font-size: 15px;">
           As of Date : {{ $to_date->format('d M Y') }}
        </span>
    </p>
</div>

<div class="row">
    <div class="col-lg-12" style="overflow-x: scroll">
        <div class="table-responsive">
            <table class="table table-condensed table-bordered" style="{{ isset($style) ? $style : '' }}"
                   cellspacing="0" width="100%">
                <thead>
                <tr class="bg-primary text-bold text-black">
                    <th style="font-size: 11px; vertical-align: middle" class="text-left">BRAND</th>
                    <th style="font-size: 11px; vertical-align: middle" class="text-right">TOTAL</th>
                    @foreach($branches as $branch)
                        <th style="font-size: 11px; vertical-align: middle"
                            class="text-center">{{ strtoupper($branch->short_name) }}</th>
                    @endforeach
                </tr>
                </thead>

                <tbody>
                @foreach($products as $product)
                    @php
                        $product_total = collect();
                        $rows->each(function ($item) use($product_total, $product){
                            $product_total->push($item[$product->name]);
                        });
                    @endphp
                    <tr>
                        <td class="text-left" style="font-size: 10px">&nbsp;{{ $product->name }}&nbsp;</td>
                        <td class="text-right" style="font-size: 10px">
                            &nbsp;{{ number_format($product_total->sum('closing'), 2) }}</td>
                        @foreach($branches as $branch)
                            <td class="text-right"
                                style="font-size: 10px">{{ number_format($rows[$branch->short_name][$product->name]['closing'], 2) }}</td>
                        @endforeach
                    </tr>
                @endforeach

                <tr>
                    <td class="text-right" style="font-size: 10px">&nbsp;TOTAL</td>
                    <td class="text-right" style="font-size: 10px">&nbsp;-</td>
                    @foreach($branches as $branch)
                        <td class="text-right"
                            style="font-size: 10px">{{ number_format($branch_total[$branch->short_name], 2) }}</td>
                        {{--                        <td class="text-right" style="font-size: 10px">{{ number_format($rows[$branch->short_name]->sum('closing'), 2) }}</td>--}}
                    @endforeach
                </tr>
                </tbody>
            </table>
        </div>
    </div>
</div>
