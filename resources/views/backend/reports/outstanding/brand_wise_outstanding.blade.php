<div class="text-center">
    <p class="text-center" style="font-size: 18px;">
        {{ $page_heading }}
        <br>
        <span
            style="font-size: 15px;">Date Range : <strong>{{ $from_date->format('d M Y') . ' - ' . $to_date->format('d M Y') }}</strong></span>
        <br>

        <span style="font-size: 15px;">Branch Name : <strong>{{ $branch_name }}</strong></span>
        <br>
    </p>
</div>

<div class="row">
    <div class="col-lg-12" style="overflow-x: scroll">
        <div class="table-responsive">
            <table class="table table-condensed table-bordered" cellspacing="0" width="100%">
                <thead>
                <tr class="text-bold text-black">
                    <th rowspan="2"
                        style="font-size: 11px; vertical-align: middle;background:#c4d79b;font-weight: bold;"
                        class="text-center">&nbsp;BRAND&nbsp;
                    </th>
                    <th rowspan="2" width="100" class="text-center"
                        style="font-size: 11px;background:#c4d79b;font-weight: bold;">&nbsp;OPENING OUTSTANDING&nbsp;
                    </th>
                    <th colspan="4" style="font-size: 11px;background:#fde9d9;font-weight: bold;" class="text-center">
                        &nbsp;SALES (BD)&nbsp;
                    </th>
                    <th rowspan="2"
                        style="font-size: 11px; vertical-align: middle;background:#fabf8f;font-weight: bold;"
                        class="text-center">&nbsp;TOTAL SALES (BD)&nbsp;
                    </th>
                    <th colspan="4" style="font-size: 11px;background: #e4dfec;font-weight: bold;" class="text-center">
                        &nbsp;RETURN&nbsp;
                    </th>
                    <th rowspan="2"
                        style="font-size: 11px; vertical-align: middle;background: #ccc0da;font-weight: bold;"
                        class="text-center">&nbsp;TOTAL RETURN&nbsp;
                    </th>
                    <th colspan="4" style="font-size: 11px;background: #ebf1de;font-weight: bold;" class="text-center">
                        &nbsp;NET SALES (AD)&nbsp;
                    </th>
                    <th rowspan="2"
                        style="font-size: 11px; vertical-align: middle;font-weight: bold;background: #c4d79b;"
                        class="text-center">&nbsp;TOTAL NET SALES (AD)&nbsp;
                    </th>
                    <th rowspan="2" width="140" class="text-center"
                        style="font-size: 11px;font-weight: bold;background: #b7dee8;">&nbsp;NET COLLECTION&nbsp;
                    </th>
                    <th rowspan="2" width="140" class="text-center"
                        style="font-size: 11px;font-weight: bold;background: #c4d79b;">&nbsp;MR REVERSE&nbsp;
                    </th>
                    <th rowspan="2" width="140" class="text-center"
                        style="font-size: 11px;font-weight: bold;background: #c4d79b;">&nbsp;ADJUSTMENT(+)&nbsp;
                    </th>
                    <th rowspan="2" width="140" class="text-center"
                        style="font-size: 11px;font-weight: bold;background: #c4d79b;">&nbsp;ADJUSTMENT (-)&nbsp;
                    </th>
                    <th rowspan="2" width="140" class="text-center"
                        style="font-size: 11px;font-weight: bold;background: #e6b8b7;">&nbsp;CLOSING OUTSTANDING&nbsp;
                    </th>
                </tr>

                <tr>
                    <th style="font-size: 11px;background:#fde9d9;font-weight: bold;" class="text-center">TP</th>
                    <th style="font-size: 11px;background:#fde9d9;font-weight: bold;" class="text-center">VAT</th>
                    <th style="font-size: 11px;background:#fde9d9;font-weight: bold;" class="text-center">DISCOUNT</th>
                    <th style="font-size: 11px;background:#fde9d9;font-weight: bold;" class="text-center">SP. DISCOUNT
                    </th>

                    <th style="font-size: 11px;background: #e4dfec;font-weight: bold;" class="text-center">TP</th>
                    <th style="font-size: 11px;background: #e4dfec;font-weight: bold;" class="text-center">VAT</th>
                    <th style="font-size: 11px;background: #e4dfec;font-weight: bold;" class="text-center">DISCOUNT</th>
                    <th style="font-size: 11px;background: #e4dfec;font-weight: bold;" class="text-center">SP.
                        DISCOUNT
                    </th>

                    <th style="font-size: 11px;background: #ebf1de;font-weight: bold;" class="text-center">TP</th>
                    <th style="font-size: 11px;background: #ebf1de;font-weight: bold;" class="text-center">VAT</th>
                    <th style="font-size: 11px;background: #ebf1de;font-weight: bold;" class="text-center">Discount</th>
                    <th style="font-size: 11px;background: #ebf1de;font-weight: bold;" class="text-center">SP. Disc</th>
                </tr>
                </thead>

                <tbody>
                @foreach($rows as $product => $row)
                    <tr>
                        <td class="text-left" style="font-size: 10px;">&nbsp;{{ $product }}&nbsp;</td>
                        <td class="text-right" style="font-size: 10px;">{{ number_format($row->get('opening'),2) }}</td>
                        <td class="text-right" style="font-size: 10px;">
                            &nbsp;{{ number_format($row->get('sales_tp'),2) }}
                            &nbsp;
                        </td>
                        <td class="text-right" style="font-size: 10px;">
                            &nbsp;{{ number_format($row->get('sales_vat'),2) }}
                            &nbsp;
                        </td>
                        <td class="text-right" style="font-size: 10px;">
                            &nbsp;{{ number_format($row->get('sales_discount'),2) }}
                            &nbsp;
                        </td>
                        <td class="text-right" style="font-size: 10px;">
                            &nbsp;{{ number_format($row->get('sales_sp_disc'),2) }}
                            &nbsp;
                        </td>
                        <td class="text-right" style="font-size: 10px;">
                            &nbsp;{{ number_format($row->get('total_sales'), 2) }}&nbsp;
                        </td>
                        <td class="text-right" style="font-size: 10px;">
                            &nbsp;{{ number_format($row->get('return_tp'),2) }}
                            &nbsp;
                        </td>
                        <td class="text-right" style="font-size: 10px;">
                            &nbsp;{{ number_format($row->get('return_vat'),2) }}
                            &nbsp;
                        </td>
                        <td class="text-right" style="font-size: 10px;">
                            &nbsp;{{ number_format($row->get('return_discount'),2) }}
                            &nbsp;
                        </td>
                        <td class="text-right" style="font-size: 10px;">
                            &nbsp;{{ number_format($row->get('return_sp_disc'),2) }}
                            &nbsp;
                        </td>
                        <td class="text-right" style="font-size: 10px;">
                            &nbsp;{{ number_format($row->get('total_return'),2) }}&nbsp;
                        </td>
                        <td class="text-right " style="font-size: 10px;">
                            &nbsp;{{ number_format($row->get('net_sales_tp'),2) }}
                            &nbsp;
                        </td>
                        <td class="text-right " style="font-size: 10px;">
                            &nbsp;{{ number_format($row->get('net_sales_vat'),2) }}
                            &nbsp;
                        </td>
                        <td class="text-right " style="font-size: 10px;">
                            &nbsp;{{ number_format($row->get('net_sales_discount'),2) }}
                            &nbsp;
                        </td>
                        <td class="text-right " style="font-size: 10px;">
                            &nbsp;{{ number_format($row->get('net_sales_sp_disc'),2) }}
                            &nbsp;
                        </td>
                        <td class="text-right" style="font-size: 10px;">
                            &nbsp;{{ number_format($row->get('net_total'),2) }}&nbsp;
                        </td>
                        <td class="text-right" style="font-size: 10px;">
                            &nbsp;{{ number_format($row->get('net_collection'),2) }}&nbsp;
                        </td>
                        <td class="text-center" style="font-size: 10px;">
                            &nbsp;{{ number_format($row->get('mr_reverse'),2) }}&nbsp;
                        </td>
                        <td class="text-center" style="font-size: 10px;">
                            &nbsp;{{ number_format($row->get('adj_positive'),2) }}&nbsp;
                        </td>
                        <td class="text-center" style="font-size: 10px;">
                            &nbsp;{{ number_format($row->get('adj_negative'),2) }}&nbsp;
                        </td>
                        <td class="text-center" style="font-size: 10px;">
                            <strong>{{ number_format($row->get('closing'),2) }}</strong></td>
                    </tr>
                @endforeach

                <td class="text-right" style="font-size: 10px;font-weight: bold;background:#c4d79b;">&nbsp;TOTAL&nbsp;
                </td>
                <td class="text-right"
                    style="font-size: 10px;background:#c4d79b;">{{ number_format($rows->sum('opening'),2) }}</td>
                <td class="text-right" style="font-size: 10px;background:#fde9d9;">
                    &nbsp;{{ number_format($rows->sum('sales_tp'),2) }}
                    &nbsp;
                </td>
                <td class="text-right" style="font-size: 10px;background:#fde9d9;">
                    &nbsp;{{ number_format($rows->sum('sales_vat'),2) }}
                    &nbsp;
                </td>
                <td class="text-right" style="font-size: 10px;background:#fde9d9;">
                    &nbsp;{{ number_format($rows->sum('sales_discount'),2) }}
                    &nbsp;
                </td>
                <td class="text-right" style="font-size: 10px;background:#fde9d9;">
                    &nbsp;{{ number_format($rows->sum('sales_sp_disc'),2) }}
                    &nbsp;
                </td>
                <td class="text-right" style="font-size: 10px;background:#fabf8f;">
                    &nbsp;{{ number_format($rows->sum('total_sales'), 2) }}&nbsp;
                </td>
                <td class="text-right" style="font-size: 10px;background: #e4dfec;">
                    &nbsp;{{ number_format($rows->sum('return_tp'),2) }}
                    &nbsp;
                </td>
                <td class="text-right" style="font-size: 10px;background: #e4dfec;">
                    &nbsp;{{ number_format($rows->sum('return_vat'),2) }}
                    &nbsp;
                </td>
                <td class="text-right" style="font-size: 10px;background: #e4dfec;">
                    &nbsp;{{ number_format($rows->sum('return_discount'),2) }}
                    &nbsp;
                </td>
                <td class="text-right" style="font-size: 10px;background: #e4dfec;">
                    &nbsp;{{ number_format($rows->sum('return_sp_disc'),2) }}
                    &nbsp;
                </td>
                <td class="text-right" style="font-size: 10px;background: #ccc0da;">
                    &nbsp;{{ number_format($rows->sum('total_return'),2) }}&nbsp;
                </td>
                <td class="text-right " style="font-size: 10px;background: #ebf1de;">
                    &nbsp;{{ number_format($rows->sum('net_sales_tp'),2) }}
                    &nbsp;
                </td>
                <td class="text-right " style="font-size: 10px;background: #ebf1de;">
                    &nbsp;{{ number_format($rows->sum('net_sales_vat'),2) }}
                    &nbsp;
                </td>
                <td class="text-right " style="font-size: 10px;background: #ebf1de;">
                    &nbsp;{{ number_format($rows->sum('net_sales_discount'),2) }}
                    &nbsp;
                </td>
                <td class="text-right " style="font-size: 10px;background: #ebf1de;">
                    &nbsp;{{ number_format($rows->sum('net_sales_sp_disc'),2) }}
                    &nbsp;
                </td>
                <td class="text-right" style="font-size: 10px;background: #c4d79b;">
                    &nbsp;{{ number_format($rows->sum('net_total'),2) }}&nbsp;
                </td>
                <td class="text-right" style="font-size: 10px;background: #b7dee8;">
                    &nbsp;{{ number_format($rows->sum('net_collection'),2) }}&nbsp;
                </td>
                <td class="text-center bg-warning" style="font-size: 10px;background: #c4d79b;">
                    &nbsp;{{ number_format($rows->sum('mr_reverse'),2) }}&nbsp;
                </td>
                <td class="text-center" style="font-size: 10px;background: #c4d79b;">
                    &nbsp;{{ number_format($rows->sum('adj_positive'),2) }}&nbsp;
                </td>
                <td class="text-center" style="font-size: 10px;background: #c4d79b;">
                    &nbsp;{{ number_format($rows->sum('adj_negative'),2) }}&nbsp;
                </td>
                <td class="text-center bg-success" style="font-size: 10px;background: #e6b8b7;">
                    <strong>{{ number_format($rows->sum('closing'),2) }}</strong></td>

                </tbody>
            </table>
        </div>
    </div>
</div>
