<div class="text-center">
    <p class="text-center" style="font-size: 18px">
        Branch Name : <strong>{{ $branch_name }}</strong>
        <br>

        <span style="font-size: 11px">
            {{ $page_heading }}
        </span>
    </p>
</div>

<div class="row">
    <div class="col-lg-12" style="overflow-x: scroll">
        <div class="table-responsive">
            <table class="table table-condensed table-bordered" style="{{ isset($style) ? $style : '' }}"
                   cellspacing="0" width="100%">
                <thead>
                <tr class="bg-primary text-bold text-black">
                    <th rowspan="2" style="font-size: 11px; vertical-align: middle" class="text-center">DATE</th>
                    <th rowspan="2" style="font-size: 11px; vertical-align: middle" class="text-center">BRANCH</th>
                    <th rowspan="2" width="100" class="text-center" style="font-size: 11px">OPENING OUTSTANDING</th>
                    <th colspan="4" style="font-size: 11px" class="text-center bg-primary-light">Sales (BD)</th>
                    <th rowspan="2" style="font-size: 11px; vertical-align: middle" class="text-center">Sales (BD)</th>
                    <th colspan="4" style="font-size: 11px" class="text-center bg-primary-light">Return</th>
                    <th rowspan="2" style="font-size: 11px; vertical-align: middle" class="text-center">Return</th>
                    <th colspan="4" style="font-size: 11px" class="text-center bg-primary-light">Net Sales (AD)</th>
                    <th rowspan="2" style="font-size: 11px; vertical-align: middle" class="text-center">Net Sales (AD)
                    </th>
                    <th rowspan="2" width="140" class="text-center" style="font-size: 11px">NET COLLECTION</th>
                    <th rowspan="2" width="140" class="text-center" style="font-size: 11px">MR REVERSE</th>
                    <th rowspan="2" width="140" class="text-center" style="font-size: 11px">ADJUSTMENT(+)</th>
                    <th rowspan="2" width="140" class="text-center" style="font-size: 11px">ADJUSTMENT (-)</th>
                    <th rowspan="2" width="140" class="text-center bg-success-light" style="font-size: 11px">CLOSING
                        OUTSTANDING
                    </th>

                    <th rowspan="2" width="140" class="text-center" style="font-size: 11px">AS PER PDF</th>
                    <th rowspan="2" width="140" class="text-center" style="font-size: 11px">DIFF.</th>
                    <th rowspan="2" width="140" class="text-center" style="font-size: 11px">AS PER SALES</th>
                    <th rowspan="2" width="140" class="text-center" style="font-size: 11px">DIFF.</th>
                    <th rowspan="2" width="140" class="text-center" style="font-size: 11px">REMARKS</th>
                </tr>

                <tr>
                    <th style="font-size: 11px" class="text-center">TP</th>
                    <th style="font-size: 11px" class="text-center">VAT</th>
                    <th style="font-size: 11px" class="text-center">Discount</th>
                    <th style="font-size: 11px" class="text-center">SP. Disc</th>

                    <th style="font-size: 11px" class="text-center">TP</th>
                    <th style="font-size: 11px" class="text-center">VAT</th>
                    <th style="font-size: 11px" class="text-center">Discount</th>
                    <th style="font-size: 11px" class="text-center">SP. Disc</th>

                    <th style="font-size: 11px" class="text-center">TP</th>
                    <th style="font-size: 11px" class="text-center">VAT</th>
                    <th style="font-size: 11px" class="text-center">Discount</th>
                    <th style="font-size: 11px" class="text-center">SP. Disc</th>
                </tr>
                </thead>

                <tbody>
                {{--                @php--}}
                {{--                    $initial_opening = collect($initial_opening);--}}
                {{--                    $outstanding = collect($outstanding);--}}
                {{--                    $receipts = collect($receipts);--}}
                {{--                    $receipt_closing = collect($receipt_closing);--}}
                {{--                    $outstanding_closing = collect($outstanding_closing);--}}
                {{--                @endphp--}}

                @foreach($branches as $branch )
                    @php
                        $_first_opening = $_net_sales = $_adj_positive = $_adj_negative = $_net_collection = 0;
                        $closing_balance = $current_opening = 0;

                        $br_wise_opening = $initial_opening->filter(function ($value, $key) use($branch) {
                            return $key === $branch->id;
                        })->first();
                        $br_wise_outstanding_closing = $outstanding_closing->filter(function ($value, $key) use($branch) {
                            return $key === $branch->id;
                        })->first();
                        $br_wise_receipt_closing = $receipt_closing->filter(function ($value, $key) use($branch) {
                            return $key === $branch->id;
                        })->first();

                        $_first_opening = $br_wise_opening ? $br_wise_opening->sum('total_outstanding') : 0;

                        $_net_sales = ($br_wise_outstanding_closing ? $br_wise_outstanding_closing->sum('sales_total_net') : 0) - ($br_wise_outstanding_closing ? $br_wise_outstanding_closing->sum('return_total_net') : 0);
                        $_adj_positive = $br_wise_receipt_closing ? $br_wise_receipt_closing->sum('total_adj_positive') : 0;
                        $_adj_negative = $br_wise_receipt_closing ? $br_wise_receipt_closing->sum('total_adj_negative') : 0;
                        $_net_collection = $br_wise_receipt_closing ? $br_wise_receipt_closing->sum('total_money_receipt') :0;
                        $_mr_reverse = $br_wise_receipt_closing ? $br_wise_receipt_closing->sum('total_mr_reverse') :0;

                    $current_opening = $_first_opening + $_net_sales + $_adj_positive - $_adj_negative + $_mr_reverse - $_net_collection;
                   // dd($current_opening, $_first_opening, $_net_sales, $_net_collection);

                    $br_wise_outstanding = $outstanding->filter(function ($value, $key) use($branch) {
                            return $key === $branch->id;
                    })->first();
                    $br_wise_receipts = $receipts->filter(function ($value, $key) use($branch) {
                            return $key === $branch->id;
                    })->first();
                    @endphp

                    {{--<tr>--}}
                    {{--<td colspan="26" width="8" class="text-left bg-corporate-light" style="font-size: 11px">--}}
                    {{--&nbsp;<strong>{{ $branch->name }}</strong></td>--}}
                    {{--</tr>--}}

                    @foreach($dates as $date)
                        @php
                            $_date = $date->toDateString();

                            $sales = $br_wise_outstanding ? $br_wise_outstanding->groupBy('trans_date')->filter(function ($value, $key) use($_date) {
                                return $key === $_date;
                            })->first() : collect();

                            //$current_opening = $current_opening + $sales->sum('net_sales_total') - $sales->sum('collection') + $sales->sum('adj_plus') - $sales->sum('adj_minus');

                            //dump($sales->sum('net_sales_total'),$sales->sum('collection'),$sales->sum('adj_plus'),$sales->sum('adj_plus'),$sales->sum('adj_minus'));
                            $sales_outstanding = $br_wise_outstanding ? $br_wise_outstanding->groupBy('trans_date')->filter(function ($value, $key) use($_date) {
                                return $key === $_date;
                            })->first() : collect();

                            $sales_tp = $sales_outstanding ? $sales_outstanding->sum('sales_total_tp') : 0;
                            $sales_vat = $sales_outstanding ? $sales_outstanding->sum('sales_total_vat'):0;
                            $sales_disc = $sales_outstanding ? $sales_outstanding->sum('sales_total_discount'): 0;
                            $sales_sp_disc = $sales_outstanding ? $sales_outstanding->sum('sales_total_sp_discount'): 0;
                            $sales = $sales_tp + $sales_vat - $sales_disc - $sales_sp_disc;

                            $returns_tp = $sales_outstanding ? $sales_outstanding->sum('return_total_tp') : 0;
                            $returns_vat = $sales_outstanding ? $sales_outstanding->sum('return_total_vat') : 0;
                            $returns_disc = $sales_outstanding ? $sales_outstanding->sum('return_total_discount') : 0;
                            $returns_sp_disc = $sales_outstanding ? $sales_outstanding->sum('return_total_sp_discount') : 0;
                           // $returns = $sales_outstanding->sum('return_total_net');
                            $returns = $returns_tp + $returns_vat - $returns_disc - $returns_sp_disc;

                            $net_sales_tp = $sales_tp - $returns_tp;
                            $net_sales_vat = $sales_vat - $returns_vat;
                            $net_sales_disc = $sales_disc - $returns_disc;
                            $net_sales_sp_disc = $sales_sp_disc - $returns_sp_disc;
                            $net_sales = $sales - $returns;

                            $receipt = $br_wise_receipts ? $br_wise_receipts->groupBy('trans_date')->filter(function ($value, $key) use($_date) {
                                return $key === $_date;
                            })->first() : collect();

                            $adv_receipt = $receipt ? $receipt->sum('adv_receipt') : 0;
                            $adj_positive = $receipt ? $receipt->sum('adj_plus') : 0;
                            $adj_negative = $receipt ? $receipt->sum('adj_minus') : 0;

                            $mr_reverse = $receipt ? $receipt->sum('mr_reverse') : 0;
                            $net_collection = $receipt ? $receipt->sum('money_receipt') : 0;
                        @endphp
                        <tr>
                            <td class="text-center" style="font-size: 10px">&nbsp;{{ $date->format( 'd-m-Y' ) }}&nbsp;
                            </td>
                            <td class="text-left" style="font-size: 10px">&nbsp;<strong>{{ $branch->name }}</strong>&nbsp;
                            </td>
                            <td class="text-right" style="font-size: 10px">{{ number_format($current_opening,2) }}</td>
                            <td class="text-right" style="font-size: 10px">&nbsp;{{ number_format($sales_tp,2) }}
                                &nbsp;
                            </td>
                            <td class="text-right" style="font-size: 10px">&nbsp;{{ number_format($sales_vat,2) }}
                                &nbsp;
                            </td>
                            <td class="text-right" style="font-size: 10px">&nbsp;{{ number_format($sales_disc,2) }}
                                &nbsp;
                            </td>
                            <td class="text-right" style="font-size: 10px">&nbsp;{{ number_format($sales_sp_disc,2) }}
                                &nbsp;
                            </td>
                            <td class="text-right bg-success-light" style="font-size: 10px">
                                &nbsp;{{ number_format($sales, 2) }}&nbsp;
                            </td>
                            <td class="text-right" style="font-size: 10px">&nbsp;{{ number_format($returns_tp,2) }}
                                &nbsp;
                            </td>
                            <td class="text-right" style="font-size: 10px">&nbsp;{{ number_format($returns_vat,2) }}
                                &nbsp;
                            </td>
                            <td class="text-right" style="font-size: 10px">&nbsp;{{ number_format($returns_disc,2) }}
                                &nbsp;
                            </td>
                            <td class="text-right" style="font-size: 10px">&nbsp;{{ number_format($returns_sp_disc,2) }}
                                &nbsp;
                            </td>
                            <td class="text-right bg-success-light" style="font-size: 10px">
                                &nbsp;{{ number_format($returns,2) }}&nbsp;
                            </td>
                            <td class="text-right " style="font-size: 10px">&nbsp;{{ number_format($net_sales_tp,2) }}
                                &nbsp;
                            </td>
                            <td class="text-right " style="font-size: 10px">&nbsp;{{ number_format($net_sales_vat,2) }}
                                &nbsp;
                            </td>
                            <td class="text-right " style="font-size: 10px">&nbsp;{{ number_format($net_sales_disc,2) }}
                                &nbsp;
                            </td>
                            <td class="text-right " style="font-size: 10px">
                                &nbsp;{{ number_format($net_sales_sp_disc,2) }}
                                &nbsp;
                            </td>
                            <td class="text-right bg-success-light" style="font-size: 10px">
                                &nbsp;{{ number_format($net_sales,2) }}&nbsp;
                            </td>
                            
                            <td class="text-right bg-success-light" style="font-size: 10px">
                                &nbsp;{{ number_format($net_collection,2) }}&nbsp;
                            </td>
                            
                            <td class="text-right bg-success-light" style="font-size: 10px">
                                &nbsp;{{ number_format($mr_reverse,2) }}&nbsp;
                            </td>
                            
                            <td class="text-center bg-success-light" style="font-size: 10px">
                                &nbsp;{{ number_format($adj_positive,2) }}&nbsp;
                            </td>
                            <td class="text-center bg-success-light" style="font-size: 10px">
                                &nbsp;{{ number_format($adj_negative,2) }}&nbsp;
                            </td>

                            @php
                                $closing_balance = $current_opening + $net_sales + $adj_positive - $adj_negative + $mr_reverse - $net_collection;
                            $current_opening = $closing_balance;
                            @endphp
                            <td class="text-center bg-success" style="font-size: 10px">
                                <strong>{{ number_format($closing_balance,2) }}</strong></td>
                            <td class="text-center">-</td>
                            <td class="text-center">-</td>
                            <td class="text-center">-</td>
                            <td class="text-center">-</td>
                            <td class="text-center">-</td>
                            <td class="text-center">-</td>
                        </tr>
                    @endforeach

                @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>
