<div class="text-center">
    <p class="text-center" style="font-size: 18px">
        Branch Name : <strong>{{ $branch_name }}</strong>
        <br>

        <span style="font-size: 11px">
            {{ $page_heading }}
        </span>
    </p>
</div>

<div class="row">
    <div class="col-lg-12" style="overflow-x: scroll">
        <div class="table-responsive">
            <table class="table table-condensed table-bordered" style="{{ isset($style) ? $style : '' }}"
                   cellspacing="0" width="100%">
                <thead>
                <tr class="bg-primary text-bold text-black">
                    <th rowspan="2" style="font-size: 11px; vertical-align: middle" class="text-center">DATE</th>
                    <th rowspan="2" style="font-size: 11px; vertical-align: middle" class="text-center">BRANCH</th>
                    <th rowspan="2" width="100" class="text-center" style="font-size: 11px">OPENING OUTSTANDING</th>
                    <th colspan="4" style="font-size: 11px" class="text-center bg-primary-light">Sales (BD)</th>
                    <th rowspan="2" style="font-size: 11px; vertical-align: middle" class="text-center">Sales (BD)</th>
                    <th colspan="4" style="font-size: 11px" class="text-center bg-primary-light">Return</th>
                    <th rowspan="2" style="font-size: 11px; vertical-align: middle" class="text-center">Return</th>
                    <th colspan="4" style="font-size: 11px" class="text-center bg-primary-light">Net Sales (AD)</th>
                    <th rowspan="2" style="font-size: 11px; vertical-align: middle" class="text-center">Net Sales (AD)
                    </th>
                    <th rowspan="2" width="140" class="text-center" style="font-size: 11px">NET COLLECTION</th>
                    <th rowspan="2" width="140" class="text-center" style="font-size: 11px">ADJUSTMENT(+)</th>
                    <th rowspan="2" width="140" class="text-center" style="font-size: 11px">ADJUSTMENT (-)</th>
                    <th rowspan="2" width="140" class="text-center bg-success-light" style="font-size: 11px">CLOSING
                        OUTSTANDING
                    </th>

                    <th rowspan="2" width="140" class="text-center" style="font-size: 11px">AS PER PDF</th>
                    <th rowspan="2" width="140" class="text-center" style="font-size: 11px">DIFF.</th>
                    <th rowspan="2" width="140" class="text-center" style="font-size: 11px">AS PER SALES</th>
                    <th rowspan="2" width="140" class="text-center" style="font-size: 11px">DIFF.</th>
                    <th rowspan="2" width="140" class="text-center" style="font-size: 11px">REMARKS</th>
                </tr>

                <tr>
                    <th style="font-size: 11px" class="text-center">TP</th>
                    <th style="font-size: 11px" class="text-center">VAT</th>
                    <th style="font-size: 11px" class="text-center">Discount</th>
                    <th style="font-size: 11px" class="text-center">SP. Disc</th>

                    <th style="font-size: 11px" class="text-center">TP</th>
                    <th style="font-size: 11px" class="text-center">VAT</th>
                    <th style="font-size: 11px" class="text-center">Discount</th>
                    <th style="font-size: 11px" class="text-center">SP. Disc</th>

                    <th style="font-size: 11px" class="text-center">TP</th>
                    <th style="font-size: 11px" class="text-center">VAT</th>
                    <th style="font-size: 11px" class="text-center">Discount</th>
                    <th style="font-size: 11px" class="text-center">SP. Disc</th>
                </tr>
                </thead>

                <tbody>
{{--                @php--}}
{{--                    $initial_opening = collect($initial_opening);--}}
{{--                    $outstanding = collect($outstanding);--}}
{{--                    $receipts = collect($receipts);--}}
{{--                    $receipt_closing = collect($receipt_closing);--}}
{{--                    $outstanding_closing = collect($outstanding_closing);--}}
{{--                @endphp--}}

                @foreach($branches as $branch )
                    @php

                            $_first_opening = $_net_sales = $_adj_positive = $_adj_negative = $_net_collection = 0;
                            $closing_balance = $current_opening = 0;

                            $_first_opening = $initial_opening->where('branch_id', $branch->id)->sum('total_outstanding');


                            $_net_sales = $outstanding_closing->where('branch_id', $branch->id)->sum('sales_total_net') - $outstanding_closing->where('branch_id', $branch->id)->sum('return_total_net');

                            $_adj_positive = $receipt_closing->where('branch_id', $branch->id)->sum('total_adj_positive');
                            $_adj_negative = $receipt_closing->where('branch_id', $branch->id)->sum('total_adj_negative');
                            $_net_collection = $receipt_closing->where('branch_id', $branch->id)->sum('total_money_receipt');

    //dd($_first_opening, $_net_sales, $_adj_positive, $_adj_negative, $_net_collection);

                        $current_opening = $_first_opening + $_net_sales + $_adj_positive - $_adj_negative - $_net_collection;

                    @endphp

                    {{--<tr>--}}
                    {{--<td colspan="26" width="8" class="text-left bg-corporate-light" style="font-size: 11px">--}}
                    {{--&nbsp;<strong>{{ $branch->name }}</strong></td>--}}
                    {{--</tr>--}}

                    @foreach($dates as $date)
                        @php
                            $sales = $outstanding
                                ->where('branch_id',$branch->id)
                                ->where('trans_date', $date->toDateString());

                            //$current_opening = $current_opening + $sales->sum('net_sales_total') - $sales->sum('collection') + $sales->sum('adj_plus') - $sales->sum('adj_minus');

                            //dump($sales->sum('net_sales_total'),$sales->sum('collection'),$sales->sum('adj_plus'),$sales->sum('adj_plus'),$sales->sum('adj_minus'));
                            $sales_outstanding = $outstanding->where('branch_id',$branch->id)->where('trans_date', $date->toDateString());

                            $sales_tp = $sales_outstanding->sum('sales_total_tp');
                            $sales_vat = $sales_outstanding->sum('sales_total_vat');
                            $sales_disc = $sales_outstanding->sum('sales_total_discount');
                            $sales_sp_disc = $sales_outstanding->sum('sales_total_sp_discount');
                            $sales = $sales_tp + $sales_vat - $sales_disc - $sales_sp_disc;

                            $returns_tp = $sales_outstanding->sum('return_total_tp');
                            $returns_vat = $sales_outstanding->sum('return_total_vat');
                            $returns_disc = $sales_outstanding->sum('return_total_discount');
                            $returns_sp_disc = $sales_outstanding->sum('return_total_sp_discount');
                           // $returns = $sales_outstanding->sum('return_total_net');
                            $returns = $returns_tp + $returns_vat - $returns_disc - $returns_sp_disc;

                            $net_sales_tp = $sales_tp - $returns_tp;
                            $net_sales_vat = $sales_vat - $returns_vat;
                            $net_sales_disc = $sales_disc - $returns_disc;
                            $net_sales_sp_disc = $sales_sp_disc - $returns_sp_disc;
                            $net_sales = $sales - $returns;

                            $receipt = $receipts->where('branch_id',$branch->id)->where('trans_date', $date->toDateString());

                            $adv_receipt = $receipt->sum('adv_receipt');
                            $adj_positive = $receipt->sum('adj_plus');
                            $adj_negative = $receipt->sum('adj_minus');

                            $net_collection = $receipt->sum('money_receipt');
                        @endphp
                        <tr>
                            <td class="text-center" style="font-size: 10px">&nbsp;{{ $date->format( 'd-m-Y' ) }}&nbsp;
                            </td>
                            <td class="text-left" style="font-size: 10px">&nbsp;<strong>{{ $branch->name }}</strong>&nbsp;
                            </td>
                            <td class="text-right" style="font-size: 10px">{{ number_format($current_opening,2) }}</td>
                            <td class="text-right" style="font-size: 10px">&nbsp;{{ number_format($sales_tp,2) }}
                                &nbsp;
                            </td>
                            <td class="text-right" style="font-size: 10px">&nbsp;{{ number_format($sales_vat,2) }}
                                &nbsp;
                            </td>
                            <td class="text-right" style="font-size: 10px">&nbsp;{{ number_format($sales_disc,2) }}
                                &nbsp;
                            </td>
                            <td class="text-right" style="font-size: 10px">&nbsp;{{ number_format($sales_sp_disc,2) }}
                                &nbsp;
                            </td>
                            <td class="text-right bg-success-light" style="font-size: 10px">
                                &nbsp;{{ number_format($sales, 2) }}&nbsp;
                            </td>
                            <td class="text-right" style="font-size: 10px">&nbsp;{{ number_format($returns_tp,2) }}
                                &nbsp;
                            </td>
                            <td class="text-right" style="font-size: 10px">&nbsp;{{ number_format($returns_vat,2) }}
                                &nbsp;
                            </td>
                            <td class="text-right" style="font-size: 10px">&nbsp;{{ number_format($returns_disc,2) }}
                                &nbsp;
                            </td>
                            <td class="text-right" style="font-size: 10px">&nbsp;{{ number_format($returns_sp_disc,2) }}
                                &nbsp;
                            </td>
                            <td class="text-right bg-success-light" style="font-size: 10px">
                                &nbsp;{{ number_format($returns,2) }}&nbsp;
                            </td>
                            <td class="text-right " style="font-size: 10px">&nbsp;{{ number_format($net_sales_tp,2) }}
                                &nbsp;
                            </td>
                            <td class="text-right " style="font-size: 10px">&nbsp;{{ number_format($net_sales_vat,2) }}
                                &nbsp;
                            </td>
                            <td class="text-right " style="font-size: 10px">&nbsp;{{ number_format($net_sales_disc,2) }}
                                &nbsp;
                            </td>
                            <td class="text-right " style="font-size: 10px">
                                &nbsp;{{ number_format($net_sales_sp_disc,2) }}
                                &nbsp;
                            </td>
                            <td class="text-right bg-success-light" style="font-size: 10px">
                                &nbsp;{{ number_format($net_sales,2) }}&nbsp;
                            </td>
                            <td class="text-right bg-success-light" style="font-size: 10px">
                                &nbsp;{{ number_format($net_collection,2) }}&nbsp;
                            </td>
                            <td class="text-center bg-success-light" style="font-size: 10px">
                                &nbsp;{{ number_format($adj_positive,2) }}&nbsp;
                            </td>
                            <td class="text-center bg-success-light" style="font-size: 10px">
                                &nbsp;{{ number_format($adj_negative,2) }}&nbsp;
                            </td>

                            @php
                                $closing_balance = $current_opening + $net_sales + $adj_positive - $adj_negative - $net_collection;
                            $current_opening = $closing_balance;
                            @endphp
                            <td class="text-center bg-success" style="font-size: 10px">
                                <strong>{{ number_format($closing_balance,2) }}</strong></td>
                            <td class="text-center">-</td>
                            <td class="text-center">-</td>
                            <td class="text-center">-</td>
                            <td class="text-center">-</td>
                            <td class="text-center">-</td>
                        </tr>
                    @endforeach

                @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>
