<div class="col-sm-8">
    <div class="form-group row">
        {{ Form::label('branch_id', 'Branch:', ['class' => 'col-sm-1 control-label text-left', 'style' => 'padding-top: 2px']) }}
        <div class="col-sm-9">
            {!! Form::select('branch_list[]', $branches , null, ['class' => 'form-control select2',  'required' => 'required','multiple','id'=>'branches']) !!}
        </div>
        <div class="col-sm-2">
            <input type="checkbox" id="branch" onclick="selectAll(this, 'branches')"> All
        </div>
    </div>
</div>

<div class="col-sm-4">
    <div class="form-group row">
        {{ Form::label('segment_id', 'Segment:', ['class' => 'col-sm-2 control-label text-left', 'style' => 'padding-top: 2px']) }}

        <div class="col-sm-7">
            {!! Form::select('segment_list[]', [] , null, ['class' => 'form-control branch select2',  'required' => 'required','multiple','id'=>'segments']) !!}
        </div>
        <div class="col-sm-3">
            <input type="checkbox" id="segment" onclick="selectAll(this, 'segments')"> All
        </div>
    </div>
</div>

<div class="col-sm-8">
    <div class="form-group row">
        {{ Form::label('brand_id', 'Brand:', ['class' => 'col-sm-1 control-label text-left', 'style' => 'padding-top: 2px']) }}

        <div class="col-sm-9">
            {!! Form::select('brand_list[]', [] , null, ['class' => 'form-control select2',  'required' => 'required','multiple','id'=>'brands']) !!}
        </div>
        <div class="col-sm-2">
            <input type="checkbox" id="brand" onclick="selectAll(this, 'brands')"> All
        </div>
    </div>
</div>

{{--<div class="col-sm-4">--}}
{{--<div class="form-group row">--}}
{{--{{ Form::label('month', 'Month:', ['class' => 'col-sm-2 control-label text-left', 'style' => "padding-top: 2px"]) }}--}}

{{--<div class="col-sm-5">--}}
{{--{!! Form::select('month', $months , null, ['class' => 'form-control select2',  'required' => 'required']) !!}--}}
{{--</div>--}}
{{--<div class="col-sm-5">--}}
{{--{!! Form::select('year', $years , $now->year, ['class' => 'form-control select2',  'required' => 'required']) !!}--}}
{{--</div>--}}
{{--</div>--}}
{{--</div>--}}

<div class="col-sm-4">
    <div class="form-group row">
        {{ Form::label('date', 'Date:', ['class' => 'col-sm-2 control-label text-left', 'style' => 'padding-top: 2px']) }}

        <div class="col-sm-5">
            {!! Form::text('from_date', $now->format('d-M-Y'),  ['class' => 'form-control datepicker','data-week-start'=>'6', 'data-autoclose'=>'true', 'data-today-highlight'=>'true', 'data-date-format'=>'dd-M-yyyy', 'placeholder'=>'mm/dd/yy','style'=>'height: 33px']) !!}
        </div>

        <div class="col-sm-5">
            {!! Form::text('to_date', $now->format('d-M-Y'),  ['class' => 'form-control datepicker','data-week-start'=>'6', 'data-autoclose'=>'true', 'data-today-highlight'=>'true', 'data-date-format'=>'dd-M-yyyy', 'placeholder'=>'mm/dd/yy','style'=>'height: 33px']) !!}
        </div>
    </div>
</div>

<div class="col-sm-8">
    <div class="form-group row">
        {{ Form::label('year', 'Year:', ['class' => 'col-sm-1 control-label text-left', 'style' => 'padding-top: 2px']) }}

        <div class="col-sm-4">
            {!! Form::selectRange('year', 2018, now()->year, now()->year-1, ['class' => 'form-control exclude-select2','id'=>'year','placeholder'=>'Select a Year']) !!}
        </div>

        {{ Form::label('month', 'Month:', ['class' => 'col-sm-1 control-label text-left', 'style' => 'padding-top: 2px']) }}
        <div class="col-sm-4">
            {!! Form::selectMonth('month', null, ['class' => 'form-control exclude-select2','id'=>'month','placeholder'=>'Select a month']) !!}
        </div>

        <em><small>for 5.1 report</small></em>
    </div>
</div>
