<div class="text-center">
    <p class="text-center" style="font-size: 18px">
        Branch Name : <strong>{{ $branch_name }}</strong>
        <br>
        <span style="font-size: 11px">
            {{ $page_heading }}
        </span>
    </p>
</div>

<div class="row">
    <div class="col-lg-12">
        @component('includes.components.table',['class'=>'table table-condensed  table-bordered','id'=>'','tr_class'=>'bg-primary text-bold text-black'])
            @slot('thead')
                    <th width="8" class="text-center" style="font-size: 11px">DATE</th>
                    {{--@unless(count($branches) > 1)--}}
                    {{--<th width="100" class="text-center" style="font-size: 11px">TIME</th>--}}
                    {{--<th width="280" class="text-center" style="font-size: 11px">CHECKED BY</th>--}}
                    {{--<th class="text-center" style="font-size: 11px">VERIFIED BY (AIC)</th>--}}
                    {{--@endunless--}}
                    <th width="170" class="text-center" style="font-size: 11px">OPENING BALANCE</th>
                    <th width="140" class="text-center" style="font-size: 11px">TOTAL MR</th>
                    <th width="140" class="text-center" style="font-size: 11px">MR REVERSE</th>
                    @foreach($brands as $brand)
                        <th width="140" class="text-center" style="font-size: 11px"> {{ $brand->name }}&nbsp;(+)</th>
                        <th width="140" class="text-center" style="font-size: 11px"> {{ $brand->name }}&nbsp;(-)</th>
                    @endforeach
                    <th width="140" class="text-center" style="font-size: 11px">TOTAL DEPOSIT</th>
                    <th width="140" class="text-center" style="font-size: 11px">CLOSING BALANCE</th>
            @endslot

            @slot('tbody')
                @php
                    $closing = $total_money_receipt = $total_mr_reverse = $total_deposit = 0;

                    $initial_opening = collect($initial_opening);
                    $receiptClosing = collect($receiptClosing);
                    $depositClosing = collect($depositClosing);
                    $allReceipts = collect($receipts);
                    $allDeposits = collect($deposits);

                    $current_opening = $initial_opening->sum('total_collection') + $receiptClosing->sum('total_money_receipt') - $receiptClosing->sum('total_mr_reverse') - $depositClosing->sum('total_amount') + ($receiptClosing->sum('total_advance_receipt') + $receiptClosing->sum('total_short_receipt')) - ($receiptClosing->sum('total_advance_adj'));

                    //dd($current_opening, $receiptClosing, $depositClosing);


                //	$current_opening = $initial_opening->sum('total_collection') + $receiptClosing->sum('total_money_receipt') - $depositClosing->sum('total_amount') + ($receiptClosing->sum('total_advance_receipt') + $receiptClosing->sum('total_short_receipt') + $receiptClosing->sum('total_adj_positive')) - ($receiptClosing->sum('total_advance_adj') + $receiptClosing->sum('total_adj_negative'));

                //	dump('op:'.$initial_opening->sum('total_collection'), 'mr:'.$receiptClosing->sum('total_money_receipt'), 'dp:'.$depositClosing->sum('total_amount'), 'adr:'.$receiptClosing->sum('total_advance_receipt'), 'sht:'.$receiptClosing->sum('total_short_receipt'), 'adp:'.$receiptClosing->sum('total_adj_positive'),'ada:'.$receiptClosing->sum('total_advance_adj'),  $receiptClosing->sum('total_adj_negative'),$current_opening  );
                @endphp

                @foreach($dates as $date)
                    @php
                        //$receipt = $receipts->where('trans_date',$date)->first();
                        //$deposit = $deposits->where('trans_date',$date)->first();

                        //$money_receipt = $receipt ? $receipt->details->sum('money_receipt') : 0;

                        $money_receipt = $allReceipts->where('trans_date',$date->toDateString())->sum('total_money_receipt');
                        $total_money_receipt += $money_receipt;
                        
                        $mr_reverse = $allReceipts->where('trans_date',$date->toDateString())->sum('total_mr_reverse');
                        $total_mr_reverse += $mr_reverse;

                        //$deposit_amount = $deposit ? $deposit->details->sum('amount') : 0;
                        $deposit_amount = $allDeposits->where('trans_date',$date->toDateString())->sum('total_amount');
                        $total_deposit += $deposit_amount;

                        $closing_balance = $current_opening + $money_receipt - $mr_reverse - $deposit_amount;
                    @endphp

                    <tr>
                        <td style="font-size: 10px">&nbsp;{{ $date->format( 'd-m-Y' ) }}&nbsp;</td>
                        <td class="text-right" style="font-size: 10px">&nbsp;{{ number_format($current_opening, 2) }}
                            &nbsp;
                        </td>
                        <td class="text-right" style="font-size: 10px">&nbsp;{{ number_format($money_receipt, 2) }}
                            &nbsp;
                        </td>
                        <td class="text-right" style="font-size: 10px">&nbsp;{{number_format($mr_reverse, 2) }}
                            &nbsp;
                        </td>
                        @foreach($brands as $brand)
                            @php
                                $positive = $negative = 0;
                                $adjustments = $allReceipts->where('trans_date',$date->toDateString())->pluck('details');

                                if($adjustments->count() > 0){

                                    $adv_receipt = $adjustments->sum(function ($collect) use($brand){
                                        return $collect->where('brand_id', $brand->id)->sum('adv_receipt');
                                    });

                                    $short_receipt = $adjustments->sum(function ($collect) use($brand){
                                        return $collect->where('brand_id', $brand->id)->sum('short_receipt');

                                    });

                                    $adj_plus = $adjustments->sum(function ($collect) use($brand){
                                        return $collect->where('brand_id', $brand->id)->sum('adj_plus');
                                    });

                                    $adv_adj = $adjustments->sum(function ($collect) use($brand){
                                        return $collect->where('brand_id', $brand->id)->sum('adv_adj');
                                    });

                                    $adj_minus = $adjustments->sum(function ($collect) use($brand){
                                        return $collect->where('brand_id', $brand->id)->sum('adj_minus');
                                    });

                                    //dd($adv_receipt, $short_receipt, $adj_plus);

                            //	$positive = $adv_receipt + $short_receipt + $adj_plus;
                            //	$negative = $adv_adj + $adj_minus;
                                $positive = $adv_receipt + $short_receipt ;
                                $negative = $adv_adj ;
                                }

                                $closing_balance += $positive;
                                $closing_balance -= $negative;

                                $current_opening = $closing_balance;
                            @endphp
                            <td class="text-right" style="font-size: 10px">
                                &nbsp;{{ number_format($positive, 2) }}&nbsp;
                            </td>
                            <td class="text-right" style="font-size: 10px">
                                &nbsp;{{ number_format($negative, 2) }}&nbsp;
                            </td>
                        @endforeach
                        <td class="text-right" style="font-size: 10px">&nbsp;{{ number_format($deposit_amount, 2) }}
                            &nbsp;
                        </td>
                        <td class="text-right" style="font-size: 10px">
                            &nbsp;{{ number_format($closing_balance, 2) }}
                            &nbsp;
                        </td>
                    </tr>
                @endforeach

                <tr class="bg-success text-bold text-black">
                    <td colspan="2" class="text-right"><b>Total &nbsp</b></td>
                    <td class="text-right" style="font-size: 11px">
                        <strong>{{ number_format($total_money_receipt, 2) }}</strong>&nbsp;
                    </td>
                    <td class="bg-warning text-right" style="font-size: 11px">
                        <strong>{{ number_format($total_mr_reverse, 2) }}</strong>&nbsp;
                    </td>
                    @foreach($brands as $brand)
                        @php
                            $positive = $negative = 0;
                            $adjustments = null;

                            $adjustments = $allReceipts->pluck('details');
                            if($adjustments){
                                $adv_receipt = $adjustments->sum(function ($collect) use($brand){
                                    return $collect->where('brand_id', $brand->id)->sum('adv_receipt');
                                });
                                $short_receipt = $adjustments->sum(function ($collect) use($brand){
                                    return $collect->where('brand_id', $brand->id)->sum('short_receipt');
                                });
                                $adj_plus = $adjustments->sum(function ($collect) use($brand){
                                    return $collect->where('brand_id', $brand->id)->sum('adj_plus');
                                });
                                $adv_adj = $adjustments->sum(function ($collect) use($brand){
                                    return $collect->where('brand_id', $brand->id)->sum('adv_adj');
                                });
                                $adj_minus = $adjustments->sum(function ($collect) use($brand){
                                    return $collect->where('brand_id', $brand->id)->sum('adj_minus');
                                });

                            //	$positive = $adv_receipt + $short_receipt + $adj_plus;
                            //	$negative = $adv_adj + $adj_minus;
                                $positive = $adv_receipt + $short_receipt ;
                                $negative = $adv_adj ;
                            }
                        @endphp
                        <td class="text-right" style="font-size: 11px">
                            &nbsp;{{ number_format($positive, 2) }}&nbsp;
                        </td>
                        <td class="text-right" style="font-size: 11px">
                            &nbsp;{{ number_format($negative, 2) }}&nbsp;
                        </td>
                    @endforeach
                    <td class="text-right" style="font-size: 11px">
                        <strong>{{ number_format($total_deposit, 2) }}</strong>&nbsp;
                    </td>
                    <td></td>
                </tr>
            @endslot
        @endcomponent
    </div>
</div>
