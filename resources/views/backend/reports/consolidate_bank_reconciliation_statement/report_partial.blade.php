<div class="text-center">
    <p class="text-center" style="font-size: 18px">
        <strong>Transcom Distribution Company Limited</strong>
        <br>
        <strong>
            {{ $month_name }}, {{ $year }}
        </strong>
        <br>
        <span style="font-size: 11px">
            {{ $page_heading }}
        </span>
    </p>
</div>

<div class="row">
    <div class="col-lg-12" style="overflow-x: scroll">
        <div class="table-responsive">
            <table class="table table-condensed table-bordered" style="{{ isset($style) ? $style : '' }}"
                   cellspacing="0" width="100%">
                <thead>
                <tr class="bg-primary text-bold text-black">
                    <th rowspan="2" style="font-size: 11px; vertical-align: middle" class="text-center">SL</th>
                    <th rowspan="2" style="font-size: 11px; vertical-align: middle" class="text-center">BRANCH<br> Code</th>
                    <th rowspan="2" style="font-size: 11px; vertical-align: middle" class="text-center">BRANCH<br> NAME </th>
                    <th rowspan="2" width="100" class="text-center" style="font-size: 11px">Opening<br> Balance As<br> Per Bank<br> Book</th>
                    <th rowspan="2" style="font-size: 11px; vertical-align: middle" class="text-center">Add.<br> Deposited/Received<br> From HQ</th>
                    <th rowspan="2" style="font-size: 11px; vertical-align: middle" class="text-center">Add. Bank<br> Interest<br> (If Any)</th>
                    <th colspan="2" style="font-size: 11px" class="text-center bg-primary-light">Less. Cheque Issued</th>
                    <th rowspan="2" style="font-size: 11px; vertical-align: middle" class="text-center">Less. Bank<br> Charges/AIT/Excise<br> Duty<br> (If Any)</th>
                    <th rowspan="2" style="font-size: 11px; vertical-align: middle" class="text-center">Closing<br> Balance As<br> Per Bank<br> Book</th>
                    <th rowspan="2" style="font-size: 11px; vertical-align: middle" class="text-center">Closing<br> Balance As<br> Per Bank</th>
                    <th rowspan="2" width="140" class="text-center" style="font-size: 11px">Difference</th>
                    <th rowspan="2" style="font-size: 11px; vertical-align: middle" class="text-center">Add: Credited<br> By Bank<br> But Not<br> Showing In<br> Bank Book</th>
                    <th rowspan="2" style="font-size: 11px; vertical-align: middle" class="text-center">Add: Cheque<br> Issued By<br> But Not<br> Debited<br> By Bank </th>
                    <th rowspan="2" style="font-size: 11px; vertical-align: middle" class="text-center">Less: Debited<br> By Bank<br> But Not<br> Showing<br> In The<br> Bank Book </th>
                    <th rowspan="2" width="140" class="text-center" style="font-size: 11px">Adjusted<br> Closing<br> Balance As<br> Per Bank<br> Book</th>
                    <th rowspan="2" width="140" class="text-center" style="font-size: 11px">Discrepancy<br> (Should Be Zero)</th>
                </tr>

                <tr>
                    <th style="font-size: 11px" class="text-center">Tranf. to Cash</th>
                    <th style="font-size: 11px" class="text-center">Payment By CQ</th>
                </tr>
                </thead>

                <tbody>
                    @foreach($consolidate_bank_reconciliation_statements as $data)
                        {{--@dd($data->details->where('month', 3)->where('types',4)->where('branch_id', 5))--}}
                        @php
                            $closing_balance_as_per_bb = $data->closing_per_bank_book;
                            $add_credited_by_bank = $data->details->where('types', 1)->sum('amount');
                            $add_cheque_issued = $data->details->where('types', 3)->sum('amount');
                            $less_debited_by_bank = $data->details->where('types', 2)->sum('amount');
                            $adjusted_closing_balance_as_per_bb = $closing_balance_as_per_bb+$add_credited_by_bank+$add_cheque_issued-$less_debited_by_bank;
                        @endphp
                        <tr>
                            <td class="text-center" style="font-size: 10px">&nbsp;{{ $loop->iteration }}&nbsp;</td>
                            <td class="text-center" style="font-size: 10px">&nbsp;{{ $data->branch->code }}&nbsp;</td>
                            <td class="text-left" style="font-size: 10px">&nbsp;<strong>{{ $data->branch->name }}</strong>&nbsp;</td>
                            <td class="text-right" style="font-size: 10px">&nbsp;{{ number_format($data->opening_balance,2) }}</td>
                            <td class="text-right" style="font-size: 10px">&nbsp;{{ number_format($data->details->where('types', 4)->sum('amount'),2) }}</td>
                            <td class="text-right" style="font-size: 10px">&nbsp;{{ number_format($data->bank_interest,2) }}</td>
                            <td class="text-right" style="font-size: 10px">{{ number_format($data->details->where('types', 5)->sum('amount'),2) }}</td>
                            <td class="text-right" style="font-size: 10px">{{ number_format($data->details->where('types', 6)->sum('amount'),2) }}</td>
                            <td class="text-right" style="font-size: 10px">&nbsp;{{ number_format($data->bank_charge,2) }}</td>
                            <td class="text-right" style="font-size: 10px">&nbsp;{{ number_format($data->closing_per_bank_book,2) }}</td>
                            <td class="text-right" style="font-size: 10px">&nbsp;{{ number_format($data->closing_per_bank,2) }}</td>
                            <td class="text-right " style="font-size: 10px">{{ number_format(($data->closing_per_bank_book-$data->closing_per_bank),2) }} &nbsp;</td>
                            <td class="text-right " style="font-size: 10px">&nbsp;{{ number_format($add_credited_by_bank,2) }}</td>
                            <td class="text-right " style="font-size: 10px">&nbsp;{{ number_format($add_cheque_issued,2) }}</td>
                            <td class="text-right " style="font-size: 10px">&nbsp;{{ number_format($less_debited_by_bank,2) }}</td>
                            <td class="text-right " style="font-size: 10px">{{ number_format($adjusted_closing_balance_as_per_bb,2) }}</td>
                            <td class="text-right " style="font-size: 10px">{{ number_format(($data->closing_per_bank-$adjusted_closing_balance_as_per_bb),2) }}</td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>
