<div class="btn-group pull-right">
    <a class="btn btn-success  btn-sm " style="margin-left: 2px;"
       href="{{ url()->current().'?export=xlsx&action=download' }}"><i class="fa fa-fw fa-file-excel-o"></i>Excel</a>
    <a class="btn btn-success  btn-sm " style="margin-left: 2px;"
       href="{{ url()->current().'?export=csv&action=download' }}"><i class="fa fa-fw fa-file-excel-o"></i>CSV</a>
    {{--<a class="btn btn-primary btn-sm" style="margin-left: 2px;"--}}
    {{--href="{{ url()->current().'?export=csv&action=download' }}"><i class="fa fa-fw fa-file-excel-o"></i>CSV</a>--}}

    {{--    <a class="btn btn-primary btn-sm" style="margin-left: 2px;"--}}
    {{--    href="{{ url()->current().'?export=pdf&action=download' }}"><i class="fa fa-fw fa-file-pdf-o"></i>PDF</a>--}}

    <button onclick="printNow(); return false;" class="btn btn-secondary btn-sm" style="margin-left: 2px;"><i
                class="fa fa-fw fa-print"></i>Print
    </button>
</div>
