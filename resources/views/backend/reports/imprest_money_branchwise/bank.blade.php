<div class="text-center">
    <p class="text-center" style="font-size: 18px">
        {{ $page_heading }}
        <br>

        <span style="font-size: 13px">
             Date Range: {{ $from_date }} - {{ $to_date }}
        </span>
    </p>
</div>

<div class="row">
    <div class="col-lg-12">

        @component('includes.components.table',['class'=>'table table-condensed  table-bordered','id'=>'','tr_class'=>'bg-primary text-bold text-black'])
            @slot('thead')
                    <th width="8" class="text-center" style="font-size: 11px">BRANCH</th>
                    <th width="170" class="text-center" style="font-size: 11px">OPENING BALANCE</th>
                    <th width="140" class="text-center" style="font-size: 11px">TOTAL RECEIPT FROM HQ</th>

                    <th width="140" class="text-center" style="font-size: 11px">TRANSFER TO IMPREST CASH(C)</th>
                    <th width="140" class="text-center" style="font-size: 11px">TOTAL EXPENSES THROUGH CQ</th>
                    <th width="140" class="text-center" style="font-size: 11px">CLOSING BALANCE</th>
            @endslot

            @slot('tbody')
                @foreach($rows as $row)
                    <tr>
                        <td style="font-size: 10px">&nbsp;{{ $row->get('branch') }}&nbsp;</td>
                        <td class="text-right" style="font-size: 10px">
                            &nbsp;{{ number_format($row->get('current_opening'), 2) }}
                            &nbsp;
                        </td>
                        <td class="text-right" style="font-size: 10px">
                            &nbsp;{{ number_format($row->get('receipt_hq'), 2) }}&nbsp;
                        </td>

                        <td class="text-right" style="font-size: 10px">
                            &nbsp;{{ number_format($row->get('imprest_cash'), 2) }}
                            &nbsp;
                        </td>
                        <td class="text-right" style="font-size: 10px">
                            &nbsp;{{ number_format($row->get('expense_cq'), 2) }}&nbsp;
                        </td>
                        <td class="text-right" style="font-size: 10px">
                            &nbsp;{{ number_format($row->get('closing_balance'), 2) }}
                            &nbsp;
                        </td>
                    </tr>
                @endforeach
                <tr class="bg-success text-bold text-black">
                    <td></td>
                    <td class="text-right" style="font-size: 11px">&nbsp;Total&nbsp;</td>
                    <td class="text-right" style="font-size: 11px">
                        <strong>{{ number_format($grand_total->get('total_m_receipt'), 2) }}&nbsp;</strong></td>
                    <td class="text-right" style="font-size: 11px">
                        <strong>{{ number_format($grand_total->get('total_imprest_cash'), 2) }}&nbsp;</strong></td>
                    <td class="text-right" style="font-size: 11px">
                        <strong>{{ number_format($grand_total->get('total_expense'), 2) }}&nbsp;</strong></td>
                    <td class="text-right" style="font-size: 11px">
                        <strong>{{ number_format($grand_total->get('total_closing_balance'), 2) }}&nbsp;</strong></td>
                </tr>
            @endslot
        @endcomponent

    </div>
</div>
