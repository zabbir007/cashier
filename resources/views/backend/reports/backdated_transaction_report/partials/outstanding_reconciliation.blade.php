<div class="text-center">
    <p class="text-center" style="">
        <span style="font-size: 18px;">
            {{ $page_heading }}
        </span>
        <br/>
        <span>Date : <strong>{{ $from_date->toFormattedDateString() }} - {{ $to_date->toFormattedDateString() }}</strong></span>
    </p>
</div>

<div class="row">
    <div class="col-lg-12" style="overflow-x: scroll">
        <table class="table table-condensed  table-bordered">
            <thead>
            <tr class="text-bold text-black">
                <th class="text-center" style="font-size: 14px;" rowspan="2">&nbsp;Branch&nbsp;</th>
                <th class="text-center" style="font-size: 14px;" rowspan="2">&nbsp;Brand&nbsp;</th>
                <th class="text-center" style="font-size: 14px;" rowspan="2">&nbsp;Product&nbsp;</th>
                <th class="text-center" style="font-size: 14px;" colspan="15">&nbsp;Before Adjustment&nbsp;</th>
                <th class="text-center" style="font-size: 14px;" colspan="15">&nbsp;Adjusted&nbsp;</th>
                <th class="text-center" style="font-size: 14px;" colspan="15">&nbsp;After Adjustment&nbsp;</th>
                {{--                    <th class="text-center" style="font-size: 14px;" rowspan="2">&nbsp;Updated By&nbsp;</th>--}}
            </tr>
            <tr class="text-bold text-black">
                <th class="text-center" style="font-size: 14px;">Sales TP</th>
                <th class="text-center" style="font-size: 14px;">Sales VAT</th>
                <th class="text-center" style="font-size: 14px;">Sales Discount</th>
                <th class="text-center" style="font-size: 14px;">Sales SP Disc</th>
                <th class="text-center" style="font-size: 14px;">Sales Total</th>
                <th class="text-center" style="font-size: 14px;">Return TP</th>
                <th class="text-center" style="font-size: 14px;">Return VAT</th>
                <th class="text-center" style="font-size: 14px;">Return SP Disc.</th>
                <th class="text-center" style="font-size: 14px;">Return Dis.</th>
                <th class="text-center" style="font-size: 14px;">Return Total</th>
                <th class="text-center" style="font-size: 14px;">Net TP</th>
                <th class="text-center" style="font-size: 14px;">Net VAT</th>
                <th class="text-center" style="font-size: 14px;">Net Discount</th>
                <th class="text-center" style="font-size: 14px;">Net SP Disc</th>
                <th class="text-center" style="font-size: 14px;">Net Total</th>

                <th class="text-center" style="font-size: 14px;">Sales TP</th>
                <th class="text-center" style="font-size: 14px;">Sales VAT</th>
                <th class="text-center" style="font-size: 14px;">Sales Discount</th>
                <th class="text-center" style="font-size: 14px;">Sales SP Disc</th>
                <th class="text-center" style="font-size: 14px;">Sales Total</th>
                <th class="text-center" style="font-size: 14px;">Return TP</th>
                <th class="text-center" style="font-size: 14px;">Return VAT</th>
                <th class="text-center" style="font-size: 14px;">Return SP Disc.</th>
                <th class="text-center" style="font-size: 14px;">Return Dis.</th>
                <th class="text-center" style="font-size: 14px;">Return Total</th>
                <th class="text-center" style="font-size: 14px;">Net TP</th>
                <th class="text-center" style="font-size: 14px;">Net VAT</th>
                <th class="text-center" style="font-size: 14px;">Net Discount</th>
                <th class="text-center" style="font-size: 14px;">Net SP Disc</th>
                <th class="text-center" style="font-size: 14px;">Net Total</th>

                <th class="text-center" style="font-size: 14px;">Sales TP</th>
                <th class="text-center" style="font-size: 14px;">Sales VAT</th>
                <th class="text-center" style="font-size: 14px;">Sales Discount</th>
                <th class="text-center" style="font-size: 14px;">Sales SP Disc</th>
                <th class="text-center" style="font-size: 14px;">Sales Total</th>
                <th class="text-center" style="font-size: 14px;">Return TP</th>
                <th class="text-center" style="font-size: 14px;">Return VAT</th>
                <th class="text-center" style="font-size: 14px;">Return SP Disc.</th>
                <th class="text-center" style="font-size: 14px;">Return Dis.</th>
                <th class="text-center" style="font-size: 14px;">Return Total</th>
                <th class="text-center" style="font-size: 14px;">Net TP</th>
                <th class="text-center" style="font-size: 14px;">Net VAT</th>
                <th class="text-center" style="font-size: 14px;">Net Discount</th>
                <th class="text-center" style="font-size: 14px;">Net SP Disc</th>
                <th class="text-center" style="font-size: 14px;">Net Total</th>
            </tr>
            </thead>
            <tbody>
            @foreach($old_transactions as $uniq => $transaction)
                @isset($modified_transactions[$uniq])
                    <tr>
                        <td width="11" style="font-size: 10px;">&nbsp;{{ $transaction->branch_name }}&nbsp;</td>
                        <td width="11" style="font-size: 10px;">&nbsp;{{ $transaction->brand_name }}&nbsp;</td>
                        <td width="11" style="font-size: 10px;">&nbsp;{{ $transaction->product_name }}&nbsp;</td>

                        <td width="11" style="font-size: 10px;text-align: right;">
                            &nbsp;{{ number_format($transaction->sales_tp) }}&nbsp;
                        </td>
                        <td width="11" style="font-size: 10px;text-align: right;">
                            &nbsp;{{ number_format($transaction->sales_vat) }}&nbsp;
                        </td>
                        <td width="11" style="font-size: 10px;text-align: right;">
                            &nbsp;{{ number_format($transaction->sales_discount) }}&nbsp;
                        </td>
                        <td width="11" style="font-size: 10px;text-align: right;">
                            &nbsp;{{ number_format($transaction->sales_sp_disc) }}&nbsp;
                        </td>
                        <td width="11" style="font-size: 10px;text-align: right;">
                            &nbsp;{{ number_format($transaction->sales_total) }}&nbsp;
                        </td>
                        <td width="11" style="font-size: 10px;text-align: right;">
                            &nbsp;{{ number_format($transaction->return_tp) }}&nbsp;
                        </td>
                        <td width="11" style="font-size: 10px;text-align: right;">
                            &nbsp;{{ number_format($transaction->return_vat) }}&nbsp;
                        </td>
                        <td width="11" style="font-size: 10px;text-align: right;">
                            &nbsp;{{ number_format($transaction->return_sp_disc) }}&nbsp;
                        </td>
                        <td width="11" style="font-size: 10px;text-align: right;">
                            &nbsp;{{ number_format($transaction->return_discount) }}&nbsp;
                        </td>
                        <td width="11" style="font-size: 10px;text-align: right;">
                            &nbsp;{{ number_format($transaction->return_total) }}&nbsp;
                        </td>
                        <td width="11" style="font-size: 10px;text-align: right;">
                            &nbsp;{{ number_format($transaction->net_sales_tp) }}&nbsp;
                        </td>
                        <td width="11" style="font-size: 10px;text-align: right;">
                            &nbsp;{{ number_format($transaction->net_sales_vat) }}&nbsp;
                        </td>
                        <td width="11" style="font-size: 10px;text-align: right;">
                            &nbsp;{{ number_format($transaction->net_sales_discount) }}&nbsp;
                        </td>
                        <td width="11" style="font-size: 10px;text-align: right;">
                            &nbsp;{{ number_format($transaction->net_sales_sp_disc) }}&nbsp;
                        </td>
                        <td width="11" style="font-size: 10px;text-align: right;">
                            &nbsp;{{ number_format($transaction->net_sales_total) }}&nbsp;
                        </td>

                        <td width="11" style="font-size: 10px;text-align: right;">
                            &nbsp;{{ number_format($modified_transactions[$uniq]->sales_tp) }}&nbsp;
                        </td>
                        <td width="11" style="font-size: 10px;text-align: right;">
                            &nbsp;{{ number_format($modified_transactions[$uniq]->sales_vat) }}&nbsp;
                        </td>
                        <td width="11" style="font-size: 10px;text-align: right;">
                            &nbsp;{{ number_format($modified_transactions[$uniq]->sales_discount) }}&nbsp;
                        </td>
                        <td width="11" style="font-size: 10px;text-align: right;">
                            &nbsp;{{ number_format($modified_transactions[$uniq]->sales_sp_disc) }}&nbsp;
                        </td>
                        <td width="11" style="font-size: 10px;text-align: right;">
                            &nbsp;{{ number_format($modified_transactions[$uniq]->sales_total) }}&nbsp;
                        </td>
                        <td width="11" style="font-size: 10px;text-align: right;">
                            &nbsp;{{ number_format($modified_transactions[$uniq]->return_tp) }}&nbsp;
                        </td>
                        <td width="11" style="font-size: 10px;text-align: right;">
                            &nbsp;{{ number_format($modified_transactions[$uniq]->return_vat) }}&nbsp;
                        </td>
                        <td width="11" style="font-size: 10px;text-align: right;">
                            &nbsp;{{ number_format($modified_transactions[$uniq]->return_sp_disc) }}&nbsp;
                        </td>
                        <td width="11" style="font-size: 10px;text-align: right;">
                            &nbsp;{{ number_format($modified_transactions[$uniq]->return_discount) }}&nbsp;
                        </td>
                        <td width="11" style="font-size: 10px;text-align: right;">
                            &nbsp;{{ number_format($modified_transactions[$uniq]->return_total) }}&nbsp;
                        </td>
                        <td width="11" style="font-size: 10px;text-align: right;">
                            &nbsp;{{ number_format($modified_transactions[$uniq]->net_sales_tp) }}&nbsp;
                        </td>
                        <td width="11" style="font-size: 10px;text-align: right;">
                            &nbsp;{{ number_format($modified_transactions[$uniq]->net_sales_vat) }}&nbsp;
                        </td>
                        <td width="11" style="font-size: 10px;text-align: right;">
                            &nbsp;{{ number_format($modified_transactions[$uniq]->net_sales_discount) }}&nbsp;
                        </td>
                        <td width="11" style="font-size: 10px;text-align: right;">
                            &nbsp;{{ number_format($modified_transactions[$uniq]->net_sales_sp_disc) }}&nbsp;
                        </td>
                        <td width="11" style="font-size: 10px;text-align: right;">
                            &nbsp;{{ number_format($modified_transactions[$uniq]->net_sales_total) }}&nbsp;
                        </td>

                        <td width="11" style="font-size: 10px;text-align: right;">
                            &nbsp;{{ number_format($modified_transactions[$uniq]->sales_tp) }}&nbsp;
                        </td>
                        <td width="11" style="font-size: 10px;text-align: right;">
                            &nbsp;{{ number_format($modified_transactions[$uniq]->sales_vat) }}&nbsp;
                        </td>
                        <td width="11" style="font-size: 10px;text-align: right;">
                            &nbsp;{{ number_format($modified_transactions[$uniq]->sales_discount) }}&nbsp;
                        </td>
                        <td width="11" style="font-size: 10px;text-align: right;">
                            &nbsp;{{ number_format($modified_transactions[$uniq]->sales_sp_disc) }}&nbsp;
                        </td>
                        <td width="11" style="font-size: 10px;text-align: right;">
                            &nbsp;{{ number_format($modified_transactions[$uniq]->sales_total) }}&nbsp;
                        </td>
                        <td width="11" style="font-size: 10px;text-align: right;">
                            &nbsp;{{ number_format($modified_transactions[$uniq]->return_tp) }}&nbsp;
                        </td>
                        <td width="11" style="font-size: 10px;text-align: right;">
                            &nbsp;{{ number_format($modified_transactions[$uniq]->return_vat) }}&nbsp;
                        </td>
                        <td width="11" style="font-size: 10px;text-align: right;">
                            &nbsp;{{ number_format($modified_transactions[$uniq]->return_sp_disc) }}&nbsp;
                        </td>
                        <td width="11" style="font-size: 10px;text-align: right;">
                            &nbsp;{{ number_format($modified_transactions[$uniq]->return_discount) }}&nbsp;
                        </td>
                        <td width="11" style="font-size: 10px;text-align: right;">
                            &nbsp;{{ number_format($modified_transactions[$uniq]->return_total) }}&nbsp;
                        </td>
                        <td width="11" style="font-size: 10px;text-align: right;">
                            &nbsp;{{ number_format($modified_transactions[$uniq]->net_sales_tp) }}&nbsp;
                        </td>
                        <td width="11" style="font-size: 10px;text-align: right;">
                            &nbsp;{{ number_format($modified_transactions[$uniq]->net_sales_vat) }}&nbsp;
                        </td>
                        <td width="11" style="font-size: 10px;text-align: right;">
                            &nbsp;{{ number_format($modified_transactions[$uniq]->net_sales_discount) }}&nbsp;
                        </td>
                        <td width="11" style="font-size: 10px;text-align: right;">
                            &nbsp;{{ number_format($modified_transactions[$uniq]->net_sales_sp_disc) }}&nbsp;
                        </td>
                        <td width="11" style="font-size: 10px;text-align: right;">
                            &nbsp;{{ number_format($modified_transactions[$uniq]->net_sales_total) }}&nbsp;
                        </td>

                        <td width="11" style="font-size: 10px;text-align: right;">
                            &nbsp;{{ number_format((float) $transaction->sales_tp - (float) $modified_transactions[$uniq]->sales_tp) }}
                            &nbsp;
                        </td>
                        <td width="11" style="font-size: 10px;text-align: right;">
                            &nbsp;{{ number_format((float) $transaction->sales_vat - (float) $modified_transactions[$uniq]->sales_vat) }}
                            &nbsp;
                        </td>
                        <td width="11" style="font-size: 10px;text-align: right;">
                            &nbsp;{{ number_format((float) $transaction->sales_discount - (float) $modified_transactions[$uniq]->sales_discount) }}
                            &nbsp;
                        </td>
                        <td width="11" style="font-size: 10px;text-align: right;">
                            &nbsp;{{ number_format((float) $transaction->sales_sp_disc - (float) $modified_transactions[$uniq]->sales_sp_disc) }}
                            &nbsp;
                        </td>
                        <td width="11" style="font-size: 10px;text-align: right;">
                            &nbsp;{{ number_format((float) $transaction->sales_total - (float) $modified_transactions[$uniq]->sales_total) }}
                            &nbsp;
                        </td>
                        <td width="11" style="font-size: 10px;text-align: right;">
                            &nbsp;{{ number_format((float) $transaction->return_tp - (float) $modified_transactions[$uniq]->return_tp) }}
                            &nbsp;
                        </td>
                        <td width="11" style="font-size: 10px;text-align: right;">
                            &nbsp;{{ number_format((float) $transaction->return_vat - (float) $modified_transactions[$uniq]->return_vat) }}
                            &nbsp;
                        </td>
                        <td width="11" style="font-size: 10px;text-align: right;">
                            &nbsp;{{ number_format((float) $transaction->return_sp_disc - (float) $modified_transactions[$uniq]->return_sp_disc) }}
                            &nbsp;
                        </td>
                        <td width="11" style="font-size: 10px;text-align: right;">
                            &nbsp;{{ number_format((float) $transaction->return_discount - (float) $modified_transactions[$uniq]->return_discount) }}
                            &nbsp;
                        </td>
                        <td width="11" style="font-size: 10px;text-align: right;">
                            &nbsp;{{ number_format((float) $transaction->return_total - (float) $modified_transactions[$uniq]->return_total) }}
                            &nbsp;
                        </td>
                        <td width="11" style="font-size: 10px;text-align: right;">
                            &nbsp;{{ number_format((float) $transaction->net_sales_tp - (float) $modified_transactions[$uniq]->net_sales_tp) }}
                            &nbsp;
                        </td>
                        <td width="11" style="font-size: 10px;text-align: right;">
                            &nbsp;{{ number_format((float) $transaction->net_sales_vat - (float) $modified_transactions[$uniq]->net_sales_vat) }}
                            &nbsp;
                        </td>
                        <td width="11" style="font-size: 10px;text-align: right;">
                            &nbsp;{{ number_format((float) $transaction->net_sales_discount - (float) $modified_transactions[$uniq]->net_sales_discount) }}
                            &nbsp;
                        </td>
                        <td width="11" style="font-size: 10px;text-align: right;">
                            &nbsp;{{ number_format((float) $transaction->net_sales_sp_disc - (float) $modified_transactions[$uniq]->net_sales_sp_disc) }}
                            &nbsp;
                        </td>
                        <td width="11" style="font-size: 10px;text-align: right;">
                            &nbsp;{{ number_format((float) $transaction->net_sales_total - (float) $modified_transactions[$uniq]->net_sales_total) }}
                            &nbsp;
                        </td>

                        {{--                        <td>AIC COM</td>--}}
                    </tr>
                @endisset
            @endforeach
            </tbody>
        </table>
    </div>
</div>
