<div class="text-center">
    <p class="text-center" style="">
        <span style="font-size: 18px;">
            {{ $page_heading }}
        </span>
        <br/>
        <span>Date : <strong>{{ $from_date->toFormattedDateString() }} - {{ $to_date->toFormattedDateString() }}</strong></span>
    </p>
</div>

<div class="row">
    <div class="col-lg-12" style="overflow-x: scroll">
        <table class="table table-condensed  table-bordered">
            <thead>
            <tr class="text-bold text-black">
                <th class="text-center" style="font-size: 14px;" rowspan="2">&nbsp;Branch&nbsp;</th>
                <th class="text-center" style="font-size: 14px;" rowspan="2">&nbsp;Brand&nbsp;</th>
                <th class="text-center" style="font-size: 14px;" rowspan="2">&nbsp;Product&nbsp;</th>
                <th class="text-center" style="font-size: 14px;" colspan="3">&nbsp;Amount&nbsp;</th>
                {{--                    <th class="text-center" style="font-size: 14px;" rowspan="2">&nbsp;Updated By&nbsp;</th>--}}
            </tr>
            <tr class="text-bold text-black">
                <th class="text-center" style="font-size: 14px;">&nbsp;Before Adj.&nbsp;</th>
                <th class="text-center" style="font-size: 14px;">&nbsp;Adjusted&nbsp;</th>
                <th class="text-center" style="font-size: 14px;">&nbsp;After Adj.&nbsp;</th>
            </tr>
            </thead>
            <tbody>
            @foreach($old_transactions as $uniq => $transaction)
                @isset($modified_transactions[$uniq])
                    <tr>
                        <td width="11" style="font-size: 10px;">&nbsp;{{ $transaction->branch_name }}&nbsp;</td>
                        <td width="11" style="font-size: 10px;">&nbsp;{{ $transaction->brand_name }}&nbsp;</td>
                        <td width="11" style="font-size: 10px;">&nbsp;{{ $transaction->product_name }}&nbsp;</td>

                        <td width="11" style="font-size: 10px;text-align: right;">
                            &nbsp;{{ number_format($transaction->amount) }}&nbsp;
                        </td>

                        <td width="11" style="font-size: 10px;text-align: right;">
                            &nbsp;{{ number_format($modified_transactions[$uniq]->amount) }}&nbsp;
                        </td>

                        <td width="11" style="font-size: 10px;text-align: right;">
                            &nbsp;{{ number_format((float) $transaction->amount - (float) $modified_transactions[$uniq]->amount) }}
                            &nbsp;
                        </td>

                        {{--                        <td>AIC COM</td>--}}
                    </tr>
                @endisset
            @endforeach
            </tbody>
        </table>

    </div>
</div>
