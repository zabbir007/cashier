<div class="text-center">
    <p class="text-center" style="">
        <span style="font-size: 18px;">
            {{ $page_heading }}
        </span>
        <br/>
        <span>Date : <strong>{{ $from_date->toFormattedDateString() }} - {{ $to_date->toFormattedDateString() }}</strong></span>
    </p>
</div>

<div class="row">
    <div class="col-lg-12" style="overflow-x: scroll">
        <table class="table table-condensed  table-bordered">
            <thead>
            <tr class="text-bold text-black">
                <th class="text-center" style="font-size: 14px;" rowspan="2">&nbsp;Branch&nbsp;</th>
                <th class="text-center" style="font-size: 14px;" rowspan="2">&nbsp;Brand&nbsp;</th>
                <th class="text-center" style="font-size: 14px;" rowspan="2">&nbsp;Product&nbsp;</th>
                <th class="text-center" style="font-size: 14px;" colspan="7">&nbsp;Before Adjustment&nbsp;</th>
                <th class="text-center" style="font-size: 14px;" colspan="7">&nbsp;Adjusted&nbsp;</th>
                <th class="text-center" style="font-size: 14px;" colspan="7">&nbsp;After Adjustment&nbsp;</th>
                {{--                    <th class="text-center" style="font-size: 14px;" rowspan="2">&nbsp;Updated By&nbsp;</th>--}}
            </tr>
            <tr class="text-bold text-black">
                <th class="text-center" style="font-size: 14px;">&nbsp;Collection&nbsp;</th>
                <th class="text-center" style="font-size: 14px;">&nbsp;Money Receipt&nbsp;</th>
                <th class="text-center" style="font-size: 14px;">&nbsp;Adv. Receipt&nbsp;</th>
                <th class="text-center" style="font-size: 14px;">&nbsp;Short Receipt&nbsp;</th>
                <th class="text-center" style="font-size: 14px;">&nbsp;Adv. Adjustment&nbsp;</th>
                <th class="text-center" style="font-size: 14px;">&nbsp;Adj. Positive&nbsp;</th>
                <th class="text-center" style="font-size: 14px;">&nbsp;Adj. Negative&nbsp;</th>
                <th class="text-center" style="font-size: 14px;">&nbsp;Collection&nbsp;</th>
                <th class="text-center" style="font-size: 14px;">&nbsp;Money Receipt&nbsp;</th>
                <th class="text-center" style="font-size: 14px;">&nbsp;Adv. Receipt&nbsp;</th>
                <th class="text-center" style="font-size: 14px;">&nbsp;Short Receipt&nbsp;</th>
                <th class="text-center" style="font-size: 14px;">&nbsp;Adv. Adjustment&nbsp;</th>
                <th class="text-center" style="font-size: 14px;">&nbsp;Adj. Positive&nbsp;</th>
                <th class="text-center" style="font-size: 14px;">&nbsp;Adj. Negative&nbsp;</th>
                <th class="text-center" style="font-size: 14px;">&nbsp;Collection&nbsp;</th>
                <th class="text-center" style="font-size: 14px;">&nbsp;Money Receipt&nbsp;</th>
                <th class="text-center" style="font-size: 14px;">&nbsp;Adv. Receipt&nbsp;</th>
                <th class="text-center" style="font-size: 14px;">&nbsp;Short Receipt&nbsp;</th>
                <th class="text-center" style="font-size: 14px;">&nbsp;Adv. Adjustment&nbsp;</th>
                <th class="text-center" style="font-size: 14px;">&nbsp;Adj. Positive&nbsp;</th>
                <th class="text-center" style="font-size: 14px;">&nbsp;Adj. Negative&nbsp;</th>
            </tr>
            </thead>
            <tbody>
            @foreach($old_transactions as $uniq => $transaction)
                @isset($modified_transactions[$uniq])
                    <tr>
                        <td width="11" style="font-size: 10px;">&nbsp;{{ $transaction->branch_name }}&nbsp;</td>
                        <td width="11" style="font-size: 10px;">&nbsp;{{ $transaction->brand_name }}&nbsp;</td>
                        <td width="11" style="font-size: 10px;">&nbsp;{{ $transaction->product_name }}&nbsp;</td>

                        <td width="11" style="font-size: 10px;text-align: right;">
                            &nbsp;{{ number_format($transaction->collection) }}&nbsp;
                        </td>
                        <td width="11" style="font-size: 10px;text-align: right;">
                            &nbsp;{{ number_format($transaction->money_receipt) }}&nbsp;
                        </td>
                        <td width="11" style="font-size: 10px;text-align: right;">
                            &nbsp;{{ number_format($transaction->adv_receipt) }}&nbsp;
                        </td>
                        <td width="11" style="font-size: 10px;text-align: right;">
                            &nbsp;{{ number_format($transaction->short_receipt) }}&nbsp;
                        </td>
                        <td width="11" style="font-size: 10px;text-align: right;">
                            &nbsp;{{ number_format($transaction->adv_adj) }}&nbsp;
                        </td>
                        <td width="11" style="font-size: 10px;text-align: right;">
                            &nbsp;{{ number_format($transaction->adj_plus) }}&nbsp;
                        </td>
                        <td width="11" style="font-size: 10px;text-align: right;">
                            &nbsp;{{ number_format($transaction->adj_minus) }}&nbsp;
                        </td>

                        <td width="11" style="font-size: 10px;text-align: right;">
                            &nbsp;{{ number_format($modified_transactions[$uniq]->collection) }}&nbsp;
                        </td>
                        <td width="11" style="font-size: 10px;text-align: right;">
                            &nbsp;{{ number_format($modified_transactions[$uniq]->money_receipt) }}&nbsp;
                        </td>
                        <td width="11" style="font-size: 10px;text-align: right;">
                            &nbsp;{{ number_format($modified_transactions[$uniq]->adv_receipt) }}&nbsp;
                        </td>
                        <td width="11" style="font-size: 10px;text-align: right;">
                            &nbsp;{{ number_format($modified_transactions[$uniq]->short_receipt) }}&nbsp;
                        </td>
                        <td width="11" style="font-size: 10px;text-align: right;">
                            &nbsp;{{ number_format($modified_transactions[$uniq]->adv_adj) }}&nbsp;
                        </td>
                        <td width="11" style="font-size: 10px;text-align: right;">
                            &nbsp;{{ number_format($modified_transactions[$uniq]->adj_plus) }}&nbsp;
                        </td>
                        <td width="11" style="font-size: 10px;text-align: right;">
                            &nbsp;{{ number_format($modified_transactions[$uniq]->adj_minus) }}&nbsp;
                        </td>

                        <td width="11" style="font-size: 10px;text-align: right;">
                            &nbsp;{{ number_format((float) $transaction->collection - (float) $modified_transactions[$uniq]->collection) }}
                            &nbsp;
                        </td>
                        <td width="11" style="font-size: 10px;text-align: right;">
                            &nbsp;{{ number_format((float) $transaction->money_receipt - (float) $modified_transactions[$uniq]->money_receipt) }}
                            &nbsp;
                        </td>
                        <td width="11" style="font-size: 10px;text-align: right;">
                            &nbsp;{{ number_format((float) $transaction->adv_receipt - (float) $modified_transactions[$uniq]->adv_receipt) }}
                            &nbsp;
                        </td>
                        <td width="11" style="font-size: 10px;text-align: right;">
                            &nbsp;{{ number_format((float) $transaction->short_receipt - (float) $modified_transactions[$uniq]->short_receipt) }}
                            &nbsp;
                        </td>
                        <td width="11" style="font-size: 10px;text-align: right;">
                            &nbsp;{{ number_format((float) $transaction->adv_adj - (float) $modified_transactions[$uniq]->adv_adj) }}
                            &nbsp;
                        </td>
                        <td width="11" style="font-size: 10px;text-align: right;">
                            &nbsp;{{ number_format((float) $transaction->adj_plus - (float) $modified_transactions[$uniq]->adj_plus) }}
                            &nbsp;
                        </td>
                        <td width="11" style="font-size: 10px;text-align: right;">
                            &nbsp;{{ number_format((float) $transaction->adj_minus - (float) $modified_transactions[$uniq]->adj_minus) }}
                            &nbsp;
                        </td>

                        {{--                        <td>AIC COM</td>--}}
                    </tr>
                @endisset
            @endforeach
            </tbody>
        </table>

    </div>
</div>
