<div class="text-center">
    <p class="text-center" style="">
        <span style="font-size: 18px;">
            {{ $page_heading }}
        </span>
        <br/>
        <span>Date : <strong>{{ $from_date->toFormattedDateString() }} - {{ $to_date->toFormattedDateString() }}</strong></span>
    </p>
</div>

<div class="row">
    <div class="col-lg-12" style="overflow-x: scroll">
        <table class="table table-condensed table-bordered">
            <thead>
            <tr class="bg-primary text-bold text-black">
                <th class="text-center" style="font-size: 14px;">#</th>
                <th class="text-center" style="font-size: 14px;">Branch</th>
                <th class="text-center" style="font-size: 14px;">User</th>
                <th class="text-center" style="font-size: 14px;">Total</th>
            </tr>
            </thead>
            <tbody>
            @foreach($activity_logs->sortBy('branch') as $activity_log)
                <tr>
                    <td class="text-center">{{ $loop->iteration }}</td>
                    <td>{{ $activity_log->get('branch') }}</td>
                    <td>{{ $activity_log->get('user') }}</td>
                    <td>{{ number_format($activity_log->get('total')) }}</td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>
</div>
