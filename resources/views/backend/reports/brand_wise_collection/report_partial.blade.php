<div class="text-center">
    <p class="text-center" style="font-size: 18px;">
        Branch Name : <strong>{{ $branch_name }}</strong>
        <br>

        <span style="font-size: 11px">
            {{ $page_heading }}
        </span>
    </p>
</div>

<div class="row">
    <div class="col-lg-12" style="overflow-x: scroll">
        @component('includes.components.table',['class'=>'table table-condensed  table-bordered','id'=>'','tr_class'=>'bg-primary text-bold text-black'])
            @slot('thead')

                    <th style="font-size: 11px; vertical-align: middle" class="text-center">DATE</th>
                    <th style="font-size: 11px; vertical-align: middle" class="text-center">BRANCH</th>
                    {{--<th style="font-size: 11px" class="text-center">BRANCH</th>--}}
                    <th style="font-size: 11px" class="text-center">&nbsp;OPENING BALANCE&nbsp;</th>

                    {{-- Start brands--}}
                    @foreach($brands as $brand)
                        @foreach($products->where('brand_id', $brand->id) as $product)
                            <th style="font-size: 11px" class="text-center">{{ $product->name }}</th>
                        @endforeach

                        <th style="font-size: 11px" class="text-center">&nbsp;TOTAL COLLECTION
                            ({{ strtoupper($brand->name) }} BRAND)&nbsp;
                        </th>
                    @endforeach
                    {{-- End brands --}}

                    <th style="font-size: 11px" class="text-center">&nbsp;TOTAL COLLECTION (ALL BRAND)&nbsp;</th>

                    <th style="font-size: 11px" class="text-center">&nbsp;EXCESS/SHORT COLLECTION (IF ANY)&nbsp;</th>
                    <th style="font-size: 11px" class="text-center">&nbsp;(+) ADVANCE RECEIVED (IF ANY)&nbsp;</th>
                    <th style="font-size: 11px" class="text-center">&nbsp;(-) ADVANCE ADJUSTED (IF ANY)&nbsp;</th>
                    <th style="font-size: 11px" class="text-center">&nbsp;GRAND TOTAL COLLECTION (ALL BRAND) INCLUDING
                        OPENING&nbsp;
                    </th>

                    {{-- Start banks --}}
                    @foreach($banks as $bank)
                        <th style="font-size: 11px" class="text-center">&nbsp;{{ $bank->short_name }}&nbsp;</th>
                    @endforeach
                    {{-- End banks --}}

                    <th style="font-size: 11px" class="text-center">&nbsp;TOTAL DEPOSIT&nbsp;</th>

                    <th style="font-size: 11px" class="text-center">&nbsp;BANK CHARGE/INTEREST&nbsp;</th>
                    <th style="font-size: 11px" class="text-center">&nbsp;CLOSING BALANCE as per Calculation&nbsp;</th>
            @endslot

            @slot('tbody')
                @php
                    $closing = $_first_opening = $_total_collection = $total_collection = $_total_adv_receipt = $_total_adv_adj = $_total_deposit_amount = $total_money_receipt = $total_deposit = 0;

                  //   $initial_opening = collect($initial_opening);
                  //   $receiptClosing = collect($receiptClosing);
                  //   $depositClosing = collect($depositClosing);
                  //   $allReceipts = $receipts;
                 //    $allDeposits = $deposits;
                  //   $bankCharges = $bankCharges;
                @endphp
                    @foreach($branches as $branch)
                    @php
                        $br_wise_opening = $initial_opening->filter(function ($value, $key) use($branch) {
                            return $key === $branch->id;
                        })->first();
                        $br_wise_receipt_closing = $receiptClosing->filter(function ($value, $key) use($branch) {
                            return $key === $branch->id;
                        })->first();
                        $br_wise_deposit_closing = $depositClosing->filter(function ($value, $key) use($branch) {
                            return $key === $branch->id;
                        })->first();

                        $_total_grand_collection = 0;

                        $_first_opening = $br_wise_opening ? $br_wise_opening->sum('collection') : 0;
                        $_total_collection = $br_wise_receipt_closing ? $br_wise_receipt_closing->sum('money_receipt') : 0;
                        //Added short receipt
                        $_total_short_receipt = $br_wise_receipt_closing ? $br_wise_receipt_closing->sum('short_receipt') : 0;

                        $_total_adv_receipt = $br_wise_receipt_closing ? $br_wise_receipt_closing->sum('adv_receipt') : 0;
                        $_total_adv_adj = $br_wise_receipt_closing ? $br_wise_receipt_closing->sum('adv_adj') : 0;
                        $_total_deposit_amount = $br_wise_deposit_closing ? $br_wise_deposit_closing->sum('amount') : 0;

                        $current_opening = $_first_opening + $_total_collection + $_total_short_receipt + $_total_adv_receipt - $_total_adv_adj - $_total_deposit_amount;


                        $br_wise_receipt = $allReceipts ? $allReceipts->filter(function ($value, $key) use($branch) {
                            return $key === $branch->id;
                        })->first() : collect();

                        $br_wise_deposit = $allDeposits ? $allDeposits->filter(function ($value, $key) use($branch) {
                            return $key === $branch->id;
                        })->first() : collect();

                       $br_wise_bank_charge = $bankCharges ? $bankCharges->filter(function ($value, $key) use($branch) {
                            return $key === $branch->id;
                        })->first() : collect();
                    @endphp

                    {{--<tr>--}}
                        {{--<td colspan="26" width="8" class="text-left bg-corporate-light" style="font-size: 11px">--}}
                        {{--&nbsp;<strong>{{ $branch->name }}</strong></td>--}}
                    {{--</tr>--}}

                    @foreach($dates as $date)
                        @php
                                //$createBy = ($date_wise->where('trans_date',$date->format( "Y-m-d" ))->count() > 0) ? $date_wise->where('trans_date',$date->format( "Y-m-d" ))->first()->createBy->name : '-';
                              //  $approveBy = ($date_wise->where('trans_date',$date->format( "Y-m-d" ))->count() > 0) ? $date_wise->where('trans_date',$date->format( "Y-m-d" ))->first()->approveBy->name : '-';
                              //  $cb = $CurrentOpening+$collection+$aReceipt-$aAdjustment-$dp;
                              //$collection = $allReceipts

                                $_date = $date->toDateString();

                                $date_wise_receipt = $br_wise_receipt ? $br_wise_receipt->groupBy('trans_date')->filter(function ($value, $key) use($_date) {
                                    return $key === $_date;
                                })->first() : collect();

                                $date_wise_deposit = $br_wise_deposit ? $br_wise_deposit->groupBy('trans_date')->filter(function ($value, $key) use($_date) {
                                    return $key === $_date;
                                })
                                ->first() : collect();

                                $date_wise_bank_charges = $br_wise_bank_charge ? $br_wise_bank_charge->groupBy('trans_date')->filter(function ($value, $key) use($_date) {
                                    return $key === $_date;
                                })->first() : collect();

                                $collection = $date_wise_receipt ? $date_wise_receipt->sum('money_receipt') : 0;
                                $total_collection += $collection;

                               // $money_receipt = $date_wise_receipt ? $date_wise_receipt->sum('money_receipt') : 0;

                                //$total_money_receipt += $money_receipt;

                                $deposit_amount = $date_wise_deposit ? $date_wise_deposit->sum('total_amount') : 0;
                                $total_deposit += $deposit_amount;

                                //$closing_balance = $current_opening + $collection + $short_receipt  - $deposit_amount;


                        @endphp
                        <tr>
                            <td width="11" style="font-size: 10px">&nbsp;{{ $date->format('d M Y') }}&nbsp;</td>
                            <td width="11" style="font-size: 10px">&nbsp;<strong>{{ $branch->name }}</strong>&nbsp;</td>
                            {{--<td style="font-size: 10px">{{ $branch->name }}&nbsp;</td>--}}
                            <td class="text-right"
                                style="font-size: 10px">{{ number_format($current_opening, 2) }}
                                &nbsp;
                            </td>

                            {{-- Start brands--}}
                            @foreach($brands as $brand)
                                @foreach($products->where('brand_id', $brand->id) as $product)

                                    <td class="text-right"
                                        style="font-size: 10px">{{ number_format($date_wise_receipt ? $date_wise_receipt->where('product_id', $product->id)->sum('money_receipt') : 0 , 2) }}
                                        &nbsp;
                                    </td>
                                @endforeach

                                <td class="text-right"
                                    style="font-size: 10px">{{ number_format($date_wise_receipt ? $date_wise_receipt->where('brand_id', $brand->id)->sum('money_receipt') : 0, 2) }}
                                    &nbsp;
                                </td>
                            @endforeach
                            {{-- End brands --}}

                            @php
                                $total_all_brand_collection = $date_wise_receipt ? $date_wise_receipt->where('branch_id', $branch->id)
                            ->sum('money_receipt') : 0;

                                //Added short receipt
                                $short_receipt = $date_wise_receipt ? $date_wise_receipt->sum('short_receipt') : 0;

                                $adv_receipt = $date_wise_receipt ? $date_wise_receipt->sum('adv_receipt') : 0;

                                $adv_adj = $date_wise_receipt ? $date_wise_receipt->sum('adv_adj') : 0;

                                $total_grand_collection = $current_opening + $total_all_brand_collection + $short_receipt + $adv_receipt - $adv_adj;
                            $_total_grand_collection += $total_grand_collection;
                            @endphp
                            <td class="text-right"
                                style="font-size: 10px">{{ number_format($total_all_brand_collection, 2) }}
                                &nbsp;
                            </td>
                            <td class="text-right"
                                style="font-size: 10px">{{ number_format($date_wise_receipt ? $date_wise_receipt->sum('short_receipt') : 0, 2) }}
                                &nbsp;
                            </td>
                            <td class="text-right"
                                style="font-size: 10px">{{ number_format($date_wise_receipt ? $date_wise_receipt->sum('adv_receipt') : 0, 2) }}
                                &nbsp;
                            </td>
                            <td class="text-right"
                                style="font-size: 10px">{{ number_format($date_wise_receipt ? $date_wise_receipt->sum('adv_adj') : 0, 2) }}
                                &nbsp;
                            </td>
                            <td class="text-right"
                                style="font-size: 10px">{{ number_format($total_grand_collection, 2) }}</td>

                            {{-- Start banks --}}
                            @foreach($banks as $bank)
                                <td class="text-right"
                                    style="font-size: 10px">{{ number_format($date_wise_deposit ? $date_wise_deposit->where('bank_id',$bank->id)->sum('amount') : 0, 2) }}
                                    &nbsp;
                                </td>
                            @endforeach
                            {{-- End banks --}}

                            @php
                                $total_deposit = $date_wise_deposit ? $date_wise_deposit->sum('amount') : 0;

                                $total_bank_charge = $date_wise_bank_charges ? $date_wise_bank_charges->sum('amount') : 0;
                            @endphp
                            <td class="text-right"
                                style="font-size: 10px">{{ number_format($total_deposit, 2) }}
                                &nbsp;
                            </td>
                            <td class="text-right"
                                style="font-size: 10px">{{ number_format($total_bank_charge, 2) }}
                                &nbsp;
                            </td>

                            @php
                                $closing_balance = $total_grand_collection - $total_deposit - $total_bank_charge;
                                $current_opening = $closing_balance;
                            @endphp

                            <td class="text-right"
                                style="font-size: 10px">{{ number_format($closing_balance, 2)  }}
                                &nbsp;
                            </td>
                        </tr>
                    @endforeach

                    <tr>
                        <td colspan="3">Total:</td>

                        {{-- Start brands--}}
                        @foreach($brands as $brand)
                            @php
                                $brand_wise_receipt = $br_wise_receipt ? $br_wise_receipt->groupBy('brand_id')->filter(function($value, $key) use($brand){
                                    return $key === $brand->id;
                                })->first() : collect();
                            @endphp

                            @foreach($products->where('brand_id', $brand->id) as $product)
                                @php
                                    $product_wise_receipt = $brand_wise_receipt->where('product_id', $product->id);
                                       // $product_wise_receipt = $br_wise_receipt->filter(function($value, $key) use($product){
                                       // return $value->product_id === $product->id;
                                      //  })

                                @endphp

                                <td class="text-right"
                                    style="font-size: 10px">{{ number_format($product_wise_receipt ? $product_wise_receipt->sum('money_receipt') : 0, 2) }}
                                    &nbsp;
                                </td>
                            @endforeach

                            <td class="text-right"
                                style="font-size: 10px">{{ number_format($brand_wise_receipt ? $brand_wise_receipt->sum('money_receipt') : 0, 2) }}
                                &nbsp;
                            </td>
                        @endforeach
                        {{-- End brands --}}

                        @php
                            $_total_all_brand_collection = $br_wise_receipt ? $br_wise_receipt->sum('money_receipt') : 0;
                        @endphp
                        <td class="text-right"
                            style="font-size: 10px">{{ number_format($_total_all_brand_collection, 2) }}
                            &nbsp;
                        </td>

                        <td class="text-right"
                            style="font-size: 10px">{{ number_format($br_wise_receipt ? $br_wise_receipt->sum('short_receipt') : 0, 2) }}
                            &nbsp;
                        </td>

                        <td class="text-right"
                            style="font-size: 10px">{{ number_format($br_wise_receipt ? $br_wise_receipt->sum('adv_receipt') : 0, 2) }}
                            &nbsp;
                        </td>
                        <td class="text-right"
                            style="font-size: 10px">{{ number_format($br_wise_receipt ? $br_wise_receipt->sum('adv_adj') : 0, 2) }}
                            &nbsp;
                        </td>
                        <td class="text-right"
                            style="font-size: 10px">{{ number_format($_total_grand_collection, 2)}}</td>

                        {{-- Start banks --}}
                        @foreach($banks as $bank)
                            <td class="text-right"
                                style="font-size: 10px">{{ number_format($br_wise_deposit ? $br_wise_deposit->where('bank_id',$bank->id)->sum('amount') : 0, 2) }}
                                &nbsp;
                            </td>
                        @endforeach
                        {{-- End banks --}}

                        <td class="text-right"
                            style="font-size: 10px">{{ number_format($br_wise_deposit ? $br_wise_deposit->sum('amount') : 0, 2)}}
                            &nbsp;
                        </td>

                        <td class="text-right"
                            style="font-size: 10px">{{ number_format($br_wise_bank_charge ? $br_wise_bank_charge->sum('amount') : 0, 2) }}
                            &nbsp;
                        </td>

                        <td class="text-right">{{ number_format($closing_balance, 2)  }}
                            &nbsp;
                        </td>
                    </tr>

                @endforeach
            @endslot
        @endcomponent
    </div>
</div>
