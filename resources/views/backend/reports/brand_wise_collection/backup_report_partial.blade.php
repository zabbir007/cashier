<div class="text-center">
    <p class="text-center" style="font-size: 18px;">
        Branch Name : <strong>{{ $branch_name }}</strong>
        <br>

        <span style="font-size: 11px">
            {{ $page_heading }}
        </span>
    </p>
</div>

<div class="row">
    <div class="col-lg-12" style="overflow-x: scroll">
        @component('includes.components.table',['class'=>'table table-condensed  table-bordered','id'=>''])
            @slot('thead')
                <tr class="bg-primary text-bold text-black">
                    <th rowspan="2" style="font-size: 11px; vertical-align: middle" class="text-center">DATE</th>
                    <th rowspan="2" style="font-size: 11px; vertical-align: middle" class="text-center">BRANCH</th>
                    {{--<th style="font-size: 11px" class="text-center">BRANCH</th>--}}
                    <th style="font-size: 11px" class="text-center">&nbsp;OPENING BALANCE&nbsp;</th>

                    {{-- Start brands--}}
                    @foreach($brands as $brand)
                        @foreach($products->where('brand_id', $brand->id) as $product)
                            <th style="font-size: 11px" class="text-center">{{ $product->name }}</th>
                        @endforeach

                        <th style="font-size: 11px" class="text-center">&nbsp;TOTAL COLLECTION
                            ({{ strtoupper($brand->name) }} BRAND)&nbsp;
                        </th>
                    @endforeach
                    {{-- End brands --}}

                    <th style="font-size: 11px" class="text-center">&nbsp;TOTAL COLLECTION (ALL BRAND)&nbsp;</th>

                    <th style="font-size: 11px" class="text-center">&nbsp;EXCESS/SHORT COLLECTION (IF ANY)&nbsp;</th>
                    <th style="font-size: 11px" class="text-center">&nbsp;(+) ADVANCE RECEIVED (IF ANY)&nbsp;</th>
                    <th style="font-size: 11px" class="text-center">&nbsp;(-) ADVANCE ADJUSTED (IF ANY)&nbsp;</th>
                    <th style="font-size: 11px" class="text-center">&nbsp;GRAND TOTAL COLLECTION (ALL BRAND) INCLUDING
                        OPENING&nbsp;
                    </th>

                    {{-- Start banks --}}
                    @foreach($banks as $bank)
                        <th style="font-size: 11px" class="text-center">&nbsp;{{ $bank->short_name }}&nbsp;</th>
                    @endforeach
                    {{-- End banks --}}

                    <th style="font-size: 11px" class="text-center">&nbsp;TOTAL DEPOSIT&nbsp;</th>

                    <th style="font-size: 11px" class="text-center">&nbsp;BANK CHARGE/INTEREST&nbsp;</th>
                    <th style="font-size: 11px" class="text-center">&nbsp;CLOSING BALANCE as per Calculation&nbsp;</th>
                </tr>
            @endslot

            @slot('tbody')
                @php
                    $closing = $_first_opening = $_total_collection = $total_collection = $_total_adv_receipt = $_total_adv_adj = $_total_deposit_amount = $total_money_receipt = $total_deposit = 0;

                  //   $initial_opening = collect($initial_opening);
                  //   $receiptClosing = collect($receiptClosing);
                  //   $depositClosing = collect($depositClosing);
                  //   $allReceipts = $receipts;
                 //    $allDeposits = $deposits;
                  //   $bankCharges = $bankCharges;
                @endphp

                @foreach($branches as $branch )
                    @php
                        $_total_grand_collection = 0;

                        $_first_opening = $initial_opening->where('branch_id', $branch->id)->sum('collection');
                        $_total_collection = $receiptClosing->where('branch_id', $branch->id)->sum('money_receipt');
                        $_total_adv_receipt = $receiptClosing->where('branch_id', $branch->id)->sum('adv_receipt');
                        $_total_adv_adj = $receiptClosing->where('branch_id', $branch->id)->sum('adv_adj');
                        $_total_deposit_amount = $depositClosing->where('branch_id', $branch->id)->sum('amount');

                            $current_opening = $_first_opening + $_total_collection + $_total_adv_receipt - $_total_adv_adj - $_total_deposit_amount;

                    @endphp

                    {{--<tr>--}}
                        {{--<td colspan="26" width="8" class="text-left bg-corporate-light" style="font-size: 11px">--}}
                        {{--&nbsp;<strong>{{ $branch->name }}</strong></td>--}}
                    {{--</tr>--}}

                    @foreach($dates as $date)

                        @php
                            //$createBy = ($date_wise->where('trans_date',$date->format( "Y-m-d" ))->count() > 0) ? $date_wise->where('trans_date',$date->format( "Y-m-d" ))->first()->createBy->name : '-';
                          //  $approveBy = ($date_wise->where('trans_date',$date->format( "Y-m-d" ))->count() > 0) ? $date_wise->where('trans_date',$date->format( "Y-m-d" ))->first()->approveBy->name : '-';
                          //  $cb = $CurrentOpening+$collection+$aReceipt-$aAdjustment-$dp;

                            $collection = $allReceipts->where('branch_id', $branch->id)
                            ->where('trans_date',$date->toDateString())
                            ->sum('collection');
                            $total_collection += $collection;

                            $money_receipt = $allReceipts->where('branch_id', $branch->id)
                            ->where('trans_date',$date->toDateString())
                            ->sum('money_receipt');
                            $total_money_receipt += $money_receipt;

                            $deposit_amount = $allDeposits->where('branch_id', $branch->id)
                            ->where('trans_date',$date->toDateString())
                            ->sum('total_amount');
                            $total_deposit += $deposit_amount;

                            $closing_balance = $current_opening + $collection + $money_receipt - $deposit_amount;
                        @endphp
                        <tr>
                            <td width="11" style="font-size: 10px">&nbsp;{{ $date->format('d M Y') }}&nbsp;</td>
                            <td width="11" style="font-size: 10px">&nbsp;<strong>{{ $branch->name }}</strong>&nbsp;</td>
                            {{--<td style="font-size: 10px">{{ $branch->name }}&nbsp;</td>--}}
                            <td class="text-right"
                                style="font-size: 10px">{{ number_format($current_opening, 2) }}
                                &nbsp;
                            </td>

                            {{-- Start brands--}}
                            @foreach($brands as $brand)
                                @foreach($products->where('brand_id', $brand->id) as $product)

                                    <td class="text-right"
                                        style="font-size: 10px">{{ number_format($allReceipts->where('branch_id', $branch->id)
                            ->where('trans_date', $date->toDateString())->where('product_id', $product->id)->sum('money_receipt'), 2) }}
                                        &nbsp;
                                    </td>
                                @endforeach

                                <td class="text-right"
                                    style="font-size: 10px">{{ number_format($allReceipts ->where('branch_id', $branch->id)
                                    ->where('brand_id', $brand->id)
                                     ->where('trans_date', $date->toDateString())
                                     ->sum('money_receipt'), 2) }}
                                    &nbsp;
                                </td>
                            @endforeach
                            {{-- End brands --}}

                            @php
                                $total_all_brand_collection = $allReceipts ->where('branch_id', $branch->id)
                            ->where('trans_date', $date->toDateString())
                            ->sum('money_receipt');

                                $adv_receipt = $allReceipts ->where('branch_id', $branch->id)
                            ->where('trans_date', $date->toDateString())
                            ->sum('adv_receipt');

                                $adv_adj = $allReceipts ->where('branch_id', $branch->id)
                            ->where('trans_date', $date->toDateString())
                            ->sum('adv_adj');

                                $total_grand_collection = $current_opening + $total_all_brand_collection + $adv_receipt - $adv_adj;
                            $_total_grand_collection += $total_grand_collection;
                            @endphp
                            <td class="text-right"
                                style="font-size: 10px">{{ number_format($total_all_brand_collection, 2) }}
                                &nbsp;
                            </td>
                            <td class="text-right"
                                style="font-size: 10px">{{ number_format($allReceipts ->where('branch_id', $branch->id)
                            ->where('trans_date', $date->toDateString())->sum('short_receipt'), 2) }}
                                &nbsp;
                            </td>
                            <td class="text-right"
                                style="font-size: 10px">{{ number_format($allReceipts ->where('branch_id', $branch->id)
                            ->where('trans_date', $date->toDateString())->sum('adv_receipt'), 2) }}
                                &nbsp;
                            </td>
                            <td class="text-right"
                                style="font-size: 10px">{{ number_format($allReceipts ->where('branch_id', $branch->id)
                            ->where('trans_date', $date->toDateString())->sum('adv_adj'), 2) }}
                                &nbsp;
                            </td>
                            <td class="text-right"
                                style="font-size: 10px">{{ number_format($total_grand_collection, 2) }}</td>

                            {{-- Start banks --}}
                            @foreach($banks as $bank)
                                <td class="text-right"
                                    style="font-size: 10px">{{ number_format($allDeposits->where('branch_id', $branch->id)->where('trans_date',$date->toDateString())->where('bank_id',$bank->id)->sum('amount'), 2) }}
                                    &nbsp;
                                </td>
                            @endforeach
                            {{-- End banks --}}

                            @php
                                $total_deposit = $allDeposits->where('branch_id', $branch->id)
                                ->where('trans_date',$date->toDateString())
                                ->sum('amount');

                                $total_bank_charge = $bankCharges->where('branch_id', $branch->id)
                            ->where('trans_date',$date->toDateString())->sum('amount');
                            @endphp
                            <td class="text-right"
                                style="font-size: 10px">{{ number_format($total_deposit, 2) }}
                                &nbsp;
                            </td>
                            <td class="text-right"
                                style="font-size: 10px">{{ number_format($total_bank_charge, 2) }}
                                &nbsp;
                            </td>

                            @php
                                $closing_balance = $total_grand_collection - $total_deposit - $total_bank_charge;
                                $current_opening = $closing_balance;
                            @endphp

                            <td class="text-right"
                                style="font-size: 10px">{{ number_format($closing_balance, 2)  }}
                                &nbsp;
                            </td>
                        </tr>
                    @endforeach


                    <tr>
                        <td colspan="3">Total:</td>

                        {{-- Start brands--}}
                        @foreach($brands as $brand)
                            @foreach($products->where('brand_id', $brand->id) as $product)

                                <td class="text-right"
                                    style="font-size: 10px">{{ number_format($allReceipts->where('branch_id', $branch->id)->where('product_id', $product->id)->sum('money_receipt'), 2) }}
                                    &nbsp;
                                </td>
                            @endforeach

                            <td class="text-right"
                                style="font-size: 10px">{{ number_format($allReceipts->where('branch_id', $branch->id)->where('brand_id', $brand->id)->sum('money_receipt'), 2) }}
                                &nbsp;
                            </td>
                        @endforeach
                        {{-- End brands --}}

                        @php
                            $_total_all_brand_collection = $allReceipts->where('branch_id', $branch->id)->sum('money_receipt');
                        @endphp
                        <td class="text-right"
                            style="font-size: 10px">{{ number_format($_total_all_brand_collection, 2) }}
                            &nbsp;
                        </td>

                        <td class="text-right"
                            style="font-size: 10px">{{ number_format($allReceipts->where('branch_id', $branch->id)->sum('short_receipt'), 2) }}
                            &nbsp;
                        </td>

                        <td class="text-right"
                            style="font-size: 10px">{{ number_format($allReceipts->where('branch_id', $branch->id)->sum('adv_receipt'), 2) }}
                            &nbsp;
                        </td>
                        <td class="text-right"
                            style="font-size: 10px">{{ number_format($allReceipts->where('branch_id', $branch->id)->sum('adv_adj'), 2) }}
                            &nbsp;
                        </td>
                        <td class="text-right"
                            style="font-size: 10px">{{ number_format($_total_grand_collection, 2)}}</td>

                        {{-- Start banks --}}
                        @foreach($banks as $bank)
                            <td class="text-right"
                                style="font-size: 10px">{{ number_format($allDeposits->where('branch_id', $branch->id)->where('bank_id',$bank->id)->sum('amount'), 2) }}
                                &nbsp;
                            </td>
                        @endforeach
                        {{-- End banks --}}

                        <td class="text-right"
                            style="font-size: 10px">{{ number_format($allDeposits->where('branch_id', $branch->id)->sum('amount'), 2)}}
                            &nbsp;
                        </td>

                        <td class="text-right"
                            style="font-size: 10px">{{ number_format($bankCharges->where('branch_id', $branch->id)->sum('amount'), 2) }}
                            &nbsp;
                        </td>

                        <td class="text-right">{{ number_format($closing_balance, 2)  }}
                            &nbsp;
                        </td>
                    </tr>

                @endforeach
            @endslot
        @endcomponent
    </div>
</div>
