@extends ('backend.layouts.report')

@section ('title', substr($page_heading, 0, 31))

@section('content')
    @include($module_view.'.report_partial')
@endsection