@extends('backend.layouts.app')

@section('page-header')
    <h1>
        {{ app_name() }}
        <small>{{ trans('strings.backend.dashboard.title') }}</small>
    </h1>
@endsection

@section('after-styles')
    <style type="text/css">
        .alert-title, .initialism, .panel-title {
            text-transform: capitalize;
            font-weight: bold;
        }

        .btn-sm {
            padding: 3px 10px;
        }

        .input-group-addon {
            padding: 5px 10px;
        }

        .form-horizontal .checkbox, .form-horizontal .checkbox-inline, .form-horizontal .radio, .form-horizontal .radio-inline {
            padding-top: 0;
        }

        .input-group {
            margin-top: 5px;
            height: 31px;

        }

        .input-group-addon:first-child {
            border: 0;
            color: #060606;
        }

        input[type="checkbox"], input[type="radio"] {
            margin: 8px 4px 6px;
        }
    </style>
@endsection

@section('content')
    <div class="row">
        <div class="col-sm-12">
            <div class="block">
                <ul class="nav nav-tabs nav-tabs-block align-items-center js-tabs-enabled" data-toggle="tabs"
                    role="tablist">
                    <li class="nav-item">
                        <a class="nav-link active" href="#one" aria-expanded="true">Statement</a>
                    </li>
                </ul>
                <div class="block-content tab-content">
                    <div class="tab-pane active" id="one" role="tabpanel" aria-expanded="true">

                        {{ Form::open(['route' => $module_route.'.show', 'class' => 'form-horizontal', 'id'=>'module_form', 'role' => 'form', 'method' => 'post', 'target' => '_blank']) }}
                        <div class="row">
                            @include('backend.reports.form')

                            <span></span>
                            <div class="col-sm-12" style="padding-top: 10px">
                                <div class="form-group row">
                                    {{ Form::label('statement', 'Reports:', ['class' => 'col-sm-1 control-label text-left', 'style' => 'padding-top: 2px']) }}
                                    <div class="col-sm-10">
                                        <table class="table">
                                            <tbody>
                                            <tr>
                                                <td width="175">
                                                    <a href="javascript:void(0)"
                                                       class="btn btn-sm btn-noborder btn-secondary btn-block text-left"
                                                       onclick="openReport('mr_deposit','',false)">1.1 MR & Deposit
                                                        Report</a>
                                                </td>
                                                <td width="175">
                                                    <a href="javascript:void(0)"
                                                       class="btn btn-sm btn-noborder btn-secondary btn-block text-left"
                                                       onclick="openReport('co_deposit',false)">1.2 Collection & Deposit
                                                        Report</a>
                                                </td>
                                            </tr>

                                            <tr class="m-0 p-0">
                                                <td class="m-0 p-0">&nbsp;</td>
                                                <td class="m-0 p-0">&nbsp;</td>
                                            </tr>
                                            <tr>
                                                <td width="175">
                                                    <a href="javascript:void(0)"
                                                       class="btn btn-sm btn-noborder btn-secondary btn-block text-left"
                                                       onclick="openReport('imprest_money_datewise','cash',false)">2.1
                                                        Date wise Imprest Money (Cash) Report</a>
                                                </td>
                                                <td width="175">
                                                    <a href="javascript:void(0)"
                                                       class="btn btn-sm btn-noborder btn-secondary btn-block text-left"
                                                       onclick="openReport('imprest_money_datewise','bank',false)">2.2
                                                        Date wise Imprest Money (Bank) Report</a>
                                                </td>
                                            </tr>

                                            <tr>
                                                <td width="175">
                                                    <a href="javascript:void(0)"
                                                       class="btn btn-sm btn-noborder btn-secondary btn-block text-left"
                                                       onclick="openReport('imprest_money_branchwise','cash',false)">2.3
                                                        Branch wise Imprest Money (Cash) Reconciliation Report</a>
                                                </td>
                                                <td width="175">
                                                    <a href="javascript:void(0)"
                                                       class="btn btn-sm btn-noborder btn-secondary btn-block text-left"
                                                       onclick="openReport('imprest_money_branchwise','bank',false)">2.4
                                                        Branch wise Imprest Money (Bank) Reconciliation Report</a>
                                                </td>
                                            </tr>

                                            <tr class="m-0 p-0">
                                                <td class="m-0 p-0">&nbsp;</td>
                                                <td class="m-0 p-0">&nbsp;</td>
                                            </tr>

                                            <tr>
                                                <td width="175">
                                                    <a href="javascript:void(0)"
                                                       class="btn btn-sm btn-noborder btn-secondary btn-block text-left"
                                                       onclick="openReport('branch_wise_outstanding','',false)">3.1
                                                        Branch wise Outstanding Report</a>
                                                </td>
                                                <td width="175">
                                                    <a href="javascript:void(0)"
                                                       class="btn btn-sm btn-noborder btn-secondary btn-block text-left"
                                                       onclick="openReport('brand_wise_outstanding','',false)">3.2 Brand
                                                        wise Outstanding Reconciliation Report</a>
                                                </td>

                                            </tr>

                                            <tr>
                                                <td width="175">
                                                    <a href="javascript:void(0)"
                                                       class="btn btn-sm btn-noborder btn-secondary btn-block text-left"
                                                       onclick="openReport('branch_brand_wise_outstanding','',false)">3.3
                                                        Branch & Brand wise Outstanding Summary Report</a>
                                                </td>
                                            </tr>

                                            <tr class="m-0 p-0">
                                                <td class="m-0 p-0">&nbsp;</td>
                                                <td class="m-0 p-0">&nbsp;</td>
                                            </tr>

                                            <tr>
                                                <td width="175">
                                                    <a href="javascript:void(0)"
                                                       class="btn btn-sm btn-noborder btn-secondary btn-block text-left"
                                                       onclick="openReport('brand_wise_collection','',false)">4.1 Brand
                                                        Wise Collection Reconciliation Report</a>
                                                </td>
                                            </tr>

                                            <tr class="m-0 p-0">
                                                <td class="m-0 p-0">&nbsp;</td>
                                                <td class="m-0 p-0">&nbsp;</td>
                                            </tr>

                                            <tr>
                                                <td width="175">
                                                    <a href="javascript:void(0)"
                                                       class="btn btn-sm btn-noborder btn-secondary btn-block text-left"
                                                       onclick="openReport('consolidate_bank_reconciliation_statement','',true)">5.1
                                                        Consolidate Bank Reconciliation Statement</a>
                                                </td>
                                            </tr>
                                            @permission('system-manage-transaction-settings')
                                            <tr>
                                                <td width="175">
                                                    <a href="javascript:void(0)"
                                                       class="btn btn-sm btn-noborder btn-secondary btn-block text-left" onclick="openReport2('transaction_restriction_report','',true)">0
                                                        Transaction Restriction Report</a>
                                                </td>
                                            </tr>
                                            @endpermission

                                            @permission('system-manage-transaction-settings')
                                            <tr>
                                                <td>&nbsp;</td>
                                                <td>&nbsp;</td>
                                            </tr>

                                            <tr>
                                                <td>
                                                    <div class="col-sm-8">
                                                        <div class="form-group row">
                                                            {{ Form::label('transaction', 'Transaction:', ['class' => 'col-sm-4 control-label text-left', 'style' => 'padding-top: 2px']) }}

                                                            <div class="col-sm-8">
                                                                {!! Form::select('transaction', $transaction_types, '', ['class' => 'form-control exclude-select2','id'=>'year','placeholder'=>'Select a Transaction']) !!}
                                                            </div>

                                                        </div>
                                                    </div>
                                                </td>
                                                <td width="175">
                                                    <a href="javascript:void(0)"
                                                       class="btn btn-sm btn-noborder btn-secondary btn-block text-left" onclick="openReport2('backdated_transaction_report','',true)">0
                                                       Backdated Transaction Report</a>
                                                </td>
                                            </tr>
                                            @endpermission

                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                                <br>
                            </div>
                        </div>
                        {{ Form::close() }}

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('after-scripts')
    <script type="text/javascript">
        function loadDistributor(a) {
            var branch = (a.value || a.options[a.selectedIndex].value);  //crossbrowser solution =)

            $.get('{{ url()->current() }}/branch?branch_id=' + branch, function (data) {
                $('.distributors').empty();
                $.each(data, function (index, name) {
                    $('.distributors').append('<option value="' + index + '">' + name + '</option>');
                });
            });
        }

        function openReport($reportName, $params = null, $skipFilter = false) {
            let brand = $("#brands");
            let branch = $("#branches");
            let segments = $("#segments");
            let year = $("#year");
            let month = $("#month");

            if ($skipFilter == true) {
                if (year.val() == '' || branch.val() == '' || month.val() == '') {
                    let title = 'Error';

                    if (year.val() == '') title = 'Please select a Year';
                    if (branch.val() == '') title = 'Please select a Branch';
                    if (month.val() == '') title = 'Please select a Month';

                    return swal.fire({
                        title: title,
                        type: "warning",
                        closeOnConfirm: true
                    });
                } else {
                    return submitReportForm($reportName, $params);
                }
            } else {
                if ((brand.val() == "" || branch.val() == "" || segments.val() == "")) {
                    let title = 'Error1';

                    if (brand.val() == '') title = 'Please select a Brand';
                    if (branch.val() == '') title = 'Please select a Branch';
                    if (segments.val() == '') title = 'Please select a Segment';

                    return swal.fire({
                        title: title,
                        type: "warning",
                        closeOnConfirm: true
                    });

                    //swal('Error','Select required fields!');
                } else {
                    return submitReportForm($reportName, $params);
                }
            }
        }

        function openReport2($reportName) {
            return submitReportForm($reportName, null);
        }

        function submitReportForm($reportName, $params) {
            let form = $("#module_form");
            form.append('<input type="hidden" name="report_name" value="' + $reportName + '" />');
            form.append('<input type="hidden" name="report_param" value="' + $params + '" />');
            form.submit();
        }

        function selectAll($ev, $div) {
            $ev = $ev || window.event;
            let select2divOption = $("select#" + $div + " > option");

            if ($ev.checked) select2divOption.prop("selected", true);
            else select2divOption.prop("selected", false);

            $("select#" + $div).trigger("change");

            if ($div === 'segments') {
                $("#segments").trigger("change");
            }
        }

        function processBrands(data) {
            $("#brands").find('option').not(':selected').remove();

            $("#brands").select2({
                data: data,
                width: '100%',
                allowClear: true
            });
        }

        function processSegments() {
            let filterBrands = null;
            let selectedSegments = $("#segments").val();

            if (selectedSegments.length > 0) {
                let brands = $.grep({!! $brands !!}, function (n, i) {
                    if ($.inArray(n.brand_id, selectedSegments) !== -1) return n;
                });

                filterBrands = $.map(brands, function (obj) {
                    obj.id = obj.id || obj.pk;
                    obj.text = obj.text || obj.name;
                    return obj;
                });
            }

            processBrands(filterBrands);
        }

        $(function () {
            let data = $.map({!! $segments !!}, function (obj) {
                obj.id = obj.id || obj.pk;
                obj.text = obj.text || obj.name;

                return obj;
            });

            let select2 = $("#segments").select2({
                data: data,
                width: '100%'
            });

            select2.on('change', function (e) {
                processSegments();
            });
        });
    </script>
@endsection
