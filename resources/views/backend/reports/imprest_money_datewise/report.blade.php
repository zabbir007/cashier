@extends ('backend.layouts.app')

@section ('title', $page_heading)

@section('page-header')
    <h1>
        {{ $page_heading }}
    </h1>
@endsection

@section('after-styles')
    <style type="text/css">
        .table > thead > tr > th {
            vertical-align: middle;
        }

        table {
            overflow-x: hidden;
            white-space: nowrap;
            width: 100%;
        }

        .table th {
            font-size: 10px;
            font-weight: 600;
        }

        .widget-body {
            padding: 5px;
            padding-top: 0;
        }

        .table > tbody > tr > td, .table > tfoot > tr > th, .table > tfoot > tr > td {
            padding: 0 !important;
            /* font-size: 13px; */
            color: #0b0b0b;
        }

        .table > tbody > tr > td, .table > tbody > tr > th, .table > tfoot > tr > td, .table > tfoot > tr > th, .table > thead > tr > td, .table > thead > tr > th {
            vertical-align: middle;
        }

        .widget-separator {
            margin: 0;
        }

        @media print {
            h4, h5, h6 {
                margin-top: 1px;
                margin-bottom: 1px;
            }

            @page {
                size: portrait;

                margin: 0.24in;
                margin-top: 0;
                padding-top: 0.24in;
                width: 21cm;
                height: 29.7cm;
            }

            body {
                margin: 0;
                padding-top: 0.24in;
            }

            html {
                background-color: #FFFFFF;
                margin: 0;
            }
        }
    </style>
@endsection

@section('content')
    @component('includes.components.widget')
        <div class="no-print">
            @slot('title')

            @endslot

            @slot('action_buttons')
                @include('backend.reports.header_action_buttons')
            @endslot
        </div>

        <div id="printDiv">
            @include($module_view.'.'.$report)
        </div>
    @endcomponent
@endsection

@section('after-scripts')
    <script type="text/javascript">
        function printNow() {
            $("#printDiv").print({
                globalStyles: true,
                mediaPrint: false,
                stylesheet: null,
                noPrintSelector: ".no-print",
                iframe: true,
                append: null,
                prepend: null,
                manuallyCopyFormValues: true,
                timeout: 750,
                doctype: '<!doctype html>',
                deferred: $.Deferred()
            });
        }
    </script>
@endsection