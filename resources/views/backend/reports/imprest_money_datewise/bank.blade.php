<div class="text-center">
    <p class="text-center" style="font-size: 18px">
        Branch Name : <strong>{{ $branch_name }}</strong>
        <br>

        <span style="font-size: 11px">
            {{ $page_heading }}
        </span>
    </p>
</div>

<div class="row">
    <div class="col-lg-12">

        @component('includes.components.table',['class'=>'table table-condensed  table-bordered','id'=>'','tr_class'=>'bg-primary text-bold text-black'])
            @slot('thead')
                    <th width="8" class="text-center" style="font-size: 11px">DATE</th>
                    @unless(count($branches) > 1)
                        <th width="100" class="text-center" style="font-size: 11px">TIME</th>
                        <th width="280" class="text-center" style="font-size: 11px">CHECKED BY</th>
                        <th class="text-center" style="font-size: 11px">VERIFIED BY (AIC)</th>
                    @endunless
                    <th width="170" class="text-center" style="font-size: 11px">OPENING BALANCE</th>
                    <th width="140" class="text-center" style="font-size: 11px">TOTAL RECEIPT FROM HQ</th>

                    <th width="140" class="text-center" style="font-size: 11px">TRANSFER TO IMPREST CASH(C)</th>
                    <th width="140" class="text-center" style="font-size: 11px">TOTAL EXPENSES THROUGH CQ</th>
                    <th width="140" class="text-center" style="font-size: 11px">CLOSING BALANCE</th>
            @endslot

            @slot('tbody')
                @php
                    $closing = $total_imprest_cash = $total_m_receipt = $total_expense = 0;

                    $current_opening = $initial_opening->sum('pc_bank_amount') + $pettyCashClosing->sum('receipt_hq') - $pettyCashClosing->sum('total_imprest_cash')  - $pettyCashClosing->sum('expense_cq');

                 //  dump($initial_opening->sum('pc_bank_amount'), $pettyCashClosing->sum('receipt_hq'), $pettyCashClosing->sum('total_imprest_cash'), $pettyCashClosing->sum('expense_cq'), $current_opening);
                @endphp
                @foreach($dates as $date)
                    @php
                        $receipt_hq = $petty_cash->where('trans_date',$date->toDateString())->sum('receipt_hq');
                        $total_imprest_cash += $receipt_hq;

                        $imprest_cash = $petty_cash->where('trans_date',$date->toDateString())->sum('total_imprest_cash');
                        $total_m_receipt += $imprest_cash;

                        $expense_cq = $petty_cash->where('trans_date',$date->toDateString())->sum('expense_cq');
                        $total_expense += $expense_cq;

                        $createBy = ($petty_cash->count() > 0) ? $petty_cash->first()->createBy->name : '-';
                        $approveBy = ($petty_cash->count() > 0) ? $petty_cash->first()->approveBy->name : '-';

                        $closing_balance = $current_opening + $receipt_hq - $imprest_cash - $expense_cq;

                        //$pettyCash = $petty_cash->where('trans_date',$date->format( "Y-m-d" ));
                        //$pettyCashDetails = $pettyCash;
                    @endphp
                    <tr>
                        <td style="font-size: 10px">&nbsp;{{ $date->format( "d-m-Y" ) }}&nbsp;</td>
                        @unless(count($branches) > 1)
                            <td class="text-center"></td>
                            <td class="text-center" style="font-size: 10px">&nbsp;{{ $createBy }}&nbsp;</td>
                            <td class="text-center" style="font-size: 10px">&nbsp;{{ $approveBy }}&nbsp;</td>
                        @endunless
                        <td class="text-right" style="font-size: 10px">&nbsp;{{ number_format($current_opening, 2) }}
                            &nbsp;
                        </td>
                        <td class="text-right" style="font-size: 10px">&nbsp;{{ number_format($receipt_hq, 2) }}&nbsp;
                        </td>

                        <td class="text-right" style="font-size: 10px">&nbsp;{{ number_format($imprest_cash, 2) }}
                            &nbsp;
                        </td>
                        <td class="text-right" style="font-size: 10px">&nbsp;{{ number_format($expense_cq, 2) }}&nbsp;
                        </td>
                        <td class="text-center" style="font-size: 10px">&nbsp;{{ number_format($closing_balance, 2) }}
                            &nbsp;
                        </td>
                    </tr>
                    @php
                        $current_opening = $closing_balance;
                    @endphp
                @endforeach
                <tr class="bg-success text-bold text-black">
                    <td></td>
                    @unless(count($branches) > 1)
                        <td class="text-center"></td>
                        <td class="text-center" style="font-size: 10px">&nbsp;&nbsp;</td>
                        <td class="text-center" style="font-size: 10px">&nbsp;&nbsp;</td>
                    @endunless
                    <td class="text-right" style="font-size: 11px">&nbsp;Total&nbsp;</td>
                    <td class="text-right" style="font-size: 11px">
                        <strong>{{ number_format($total_imprest_cash, 2) }}</strong></td>
                    <td class="text-right" style="font-size: 11px">
                        <strong>{{ number_format($total_m_receipt, 2) }}</strong></td>
                    <td class="text-right" style="font-size: 11px">
                        <strong>{{ number_format($total_expense, 2) }}</strong></td>
                    <td></td>
                </tr>
            @endslot
        @endcomponent

    </div>
</div>
