<!doctype html>
<html class="no-js" lang="{{ app()->getLocale() }}">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
    <link rel="shortcut icon" sizes="196x196" href="{{asset('../assets/images/logo.png')}}">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>@yield('title', app_name())</title>
    <meta name="description" content="@yield('meta_description', 'Employee Profile Management')">
    <meta name="author" content="@yield('meta_author', 'Md. Aftab Uddin')">
    @yield('meta')
@yield('before-styles')

{{ Html::style(mix('css/app.css')) }}
@yield('after-styles')

    <!--[if lt IE 9]>
    {{ Html::script('https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js') }}
    {{ Html::script('https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js') }}
    <![endif]-->

    <!-- Scripts -->
    <script>
        window.Laravel = '{!! json_encode([
            'csrfToken' => csrf_token(),
        ]) !!}';
    </script>
</head>

{{--<body class="menubar-left menubar-unfold menubar-light theme-primary">--}}
<body class="menubar-left menubar-unfold menubar-light theme-primary">
<div id="app">
    <!--============= start main area -->
    <!-- APP MAIN ==========-->
    {{--<div id="page-container" class="sidebar-o side-scroll page-header-modern main-content-boxed">--}}
    <div id="page-container"
         class="{{ active_class(if_route_pattern('admin.reports.*.show'), '', 'sidebar-o') }} side-scroll page-header-fixed sidebar-inverse">
        <div id="page-loader" class="show"></div>
        <!-- APP NAVBAR ==========-->
    @include('backend.includes.nav')
    <!--========== END app navbar -->

        <!-- APP ASIDE ==========-->
    @include('backend.includes.sidebar.sidebar')
    <!-- APP ASIDE ==========-->

        <!-- Header ==========-->
    @include('backend.includes.header')
    <!-- Header ==========-->
        <!-- Main Container -->
        <main id="main-container">
            <!-- Page Content -->
            <div class="content">
                @include('includes.partials.messages')
                @include('includes.partials.logged-in-as')

                <div style="background: #fff;">
                    @yield('content')
                </div>
            </div>
            <!-- END Page Content -->
        </main>
        <!-- END Main Container -->

        <!-- APP FOOTER -->
    @include('backend.includes.footer')
    <!-- /#app-footer -->
        <!--========== END app main -->

    {{--@role(1)--}}
    {{--<!-- APP CUSTOMIZER -->--}}
    {{--@include('backend.includes.customizer')--}}
    {{--<!-- #app-customizer -->--}}
    {{--@endauth--}}

    <!-- SIDE PANEL -->
    {{--@include('backend.includes.sidebar.right_sidebar')--}}
    <!-- /#side-panel -->
    </div><!-- .wrap -->

</div>
@yield('before-scripts')
{{ Html::script(mix('js/manifest.js')) }}
{{ Html::script(mix('js/vendor.js')) }}
{{ Html::script(mix('js/app.js')) }}
<script type="application/javascript">
    function formatDate(d) {
        let day = String(d.getDate())
        //add leading zero if day is is single digit
        if (day.length == 1)
            day = '0' + day
        let month = String((d.getMonth() + 1))
        //add leading zero if month is is single digit
        if (month.length == 1)
            month = '0' + month
        return day + "-" + month + "-" + d.getFullYear()
    }

    // Init datepicker (with .js-datepicker and .input-daterange class)
    jQuery('.js-datepicker:not(.js-datepicker-enabled)').add('.input-daterange:not(.js-datepicker-enabled)').each(function () {
        let el = jQuery(this);

        let enableDays = "{!! $enable_dates ?? ',' !!}".split(',');

        // Add .js-datepicker-enabled class to tag it as activated
        el.addClass('js-datepicker-enabled');

        // Init
        el.datepicker({
            beforeShowDay: function (date) {
                if (enableDays.indexOf(formatDate(date)) < 0)
                    return {
                        enabled: false
                    };
                else
                    return {
                        enabled: true
                    }
            },
            format: '{{ config('application.js_date_format') }}',
            weekStart: el.data('week-start') || 0,
            autoclose: el.data('autoclose') || false,
            todayHighlight: el.data('today-highlight') || false,
            orientation: 'bottom' // Position issue when using BS4, set it to bottom until officially supported
        });
    });

    $(function () {
        // Init
        let el = $(".datepicker");
        el.datepicker({
            endDate: new Date(),
            weekStart: el.data('week-start') || 0,
            autoclose: el.data('autoclose') || false,
            todayHighlight: el.data('today-highlight') || false,
            orientation: 'bottom' // Position issue when using BS4, set it to bottom until officially supported
        });
    });

    $(function () {
        // Init
        let el = $(".datepicker2");
        el.datepicker({
            weekStart: el.data('week-start') || 0,
            autoclose: el.data('autoclose') || false,
            todayHighlight: el.data('today-highlight') || false,
            orientation: 'bottom' // Position issue when using BS4, set it to bottom until officially supported
        });
    });
</script>
@yield('after-scripts')
</body>
</html>
