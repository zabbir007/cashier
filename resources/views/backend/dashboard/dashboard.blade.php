@extends('backend.layouts.app')

@section('page-header')
    <h1>
        {{ app_name() }}
        <small>{{ trans('strings.backend.dashboard.title') }}</small>
    </h1>
@endsection

@section('content')
    <div id="dashboardContent">
        <div style="height: 90px;" ref="totalSummary">
            <i class="fa fa-spin fa-spinner fa-2x text-center"
               style="position: relative;top: 32%;bottom: 48%;left: 50%;" v-show="loadTotalSummaryLoading"></i>
        </div>
        <div style="height: 136px;" ref="segmentSummary">
            <i class="fa fa-spin fa-spinner fa-2x text-center"
               style="position: relative;top: 32%;bottom: 48%;left: 50%;" v-show="loadSegmentSummaryLoading"></i>
        </div>
        <div style="height: 136px;" ref="monthWiseSummary">
            <i class="fa fa-spin fa-spinner fa-2x text-center"
               style="position: relative;top: 32%;bottom: 48%;left: 50%;" v-show="loadMonthWiseSummaryLoading"></i>
        </div>
        <em class="text-muted text-right" v-show="loadSegmentSummaryLoading">* Last
            updated {{ now()->subMinutes($cache_minute)->diffForHumans() }}</em>
    </div>
@endsection

@section('after-scripts')
    <script type="text/javascript">
        const vm = new Vue({
            el: '#dashboardContent',
            data: {
                loadTotalSummaryLoading: false,
                loadSegmentSummaryLoading: false,
                loadMonthWiseSummaryLoading: false,
            },
            methods: {
                initDashboard() {
                    this.loadTotalSummary();
                    this.loadSegmentSummary();
                    this.loadMonthWiseSummary();
                },

                loadTotalSummary() {
                    this.loadTotalSummaryLoading = true;
                    axios.get('{{ route($module_route.'.total_summary') }}').then(response => {
                        this.$refs.totalSummary.innerHTML = response.data.report;
                        this.loadTotalSummaryLoading = false;
                    });
                },
                loadSegmentSummary() {
                    this.loadSegmentSummaryLoading = true;
                    axios.get('{{ route($module_route.'.segment_summary') }}').then(response => {
                        this.$refs.segmentSummary.innerHTML = response.data.report;
                        this.loadSegmentSummaryLoading = false;
                    });
                },
                loadMonthWiseSummary() {
                    this.loadMonthWiseSummaryLoading = true;
                    axios.get('{{ route($module_route.'.month_wise_summary') }}').then(response => {
                        this.$refs.monthWiseSummary.innerHTML = response.data.report;
                        this.loadMonthWiseSummaryLoading = false;
                    });
                }
            },
            // mounted: function() {
            //     this.loadTotalSummary();
            // }
        });
        jQuery(function () {
            var lpageLoader = jQuery('#page-loader');
            if (!lpageLoader.hasClass('show')) {
                vm.initDashboard();
            }
        });
    </script>
@endsection
