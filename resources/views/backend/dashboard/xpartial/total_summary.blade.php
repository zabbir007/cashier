<div class="row gutters-tiny" data-toggle="appear">
    <!-- Row #1 -->
    <div class="col-6 col-xl-2">
        <a class="block block-link-shadow text-right bg-success" href="javascript:void(0)">
            <div class="block-content block-content-full clearfix">
                <div class="float-left mt-5 d-none d-sm-block">

                    <span class="font-size-h5 font-w600 text-dark">{{now()->format('M')}},{{now()->format('y')}}</span>
                </div>
                @if($branch->count() > 1)
                    <div class="font-size-h5 text-white font-w600"> All</div>
                @else
                    <div class="font-size-h5 text-white font-w600">  {{access()->user()->branches->first()->name}} </div>
                @endif
                <div class="font-size-sm text-white font-w600 text-uppercase text-muted">
                    Branch
                </div>
            </div>
        </a>
    </div>
    <div class="col-6 col-xl-2">
        <a class="block block-link-shadow text-right bg-warning-light" href="javascript:void(0)">
            <div class="block-content block-content-full clearfix">

                <div class="font-size-h5 font-w600">&nbsp;{{ _money_format($sales,true) }}<strong>M</strong></div>
                <div class="font-size-sm font-w600 text-uppercase text-muted">
                    Sales
                </div>
            </div>
        </a>
    </div>
    <div class="col-6 col-xl-2">
        <a class="block block-link-shadow text-right" href="javascript:void(0)">
            <div class="block-content block-content-full clearfix">
                <div class="font-size-h5 font-w600">&nbsp;{{_money_format($collections, true)}}<strong>M</strong></div>
                <div class="font-size-sm font-w600 text-uppercase text-muted">
                    Collection
                </div>
            </div>
        </a>
    </div>
    <div class="col-6 col-xl-2">
        <a class="block block-link-shadow text-right bg-info-light" href="javascript:void(0)">
            <div class="block-content block-content-full clearfix">
                <div class="font-size-h5 font-w600">&nbsp;{{_money_format($deposits, true)}}<strong>M</strong></div>
                <div class="font-size-sm font-w600 text-uppercase text-muted">
                    Deposit
                </div>
            </div>
        </a>
    </div>
    <div class="col-6 col-xl-2">
        <a class="block block-link-shadow text-right bg-danger-light" href="javascript:void(0)">
            <div class="block-content block-content-full clearfix">
                <div class="font-size-h5 font-w600">&nbsp;{{_money_format($expenses, true)}}<strong>M</strong></div>
                <div class="font-size-sm font-w600 text-uppercase text-muted">
                    Expense
                </div>
            </div>
        </a>
    </div>
    <div class="col-6 col-xl-2">
        <a class="block block-link-shadow text-right bg-success-light" href="javascript:void(0)">
            <div class="block-content block-content-full clearfix">
                <div class="font-size-h5 font-w600">&nbsp;{{_money_format($outstanding, true)}}<strong>M</strong></div>
                <div class="font-size-sm font-w600 text-uppercase text-muted">
                    Outstanding
                </div>
            </div>
        </a>
    </div>
    <!-- End Row #1 -->
</div>
