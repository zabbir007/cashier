<div class="row gutters-tiny" data-toggle="appear">
    <div class="col-12 col-xl-12">
        <a class="block block-link-shadow text-right" href="javascript:void(0)">
            <div class="block-content block-content-full clearfix" style="padding-bottom: 5px">
                <table class='table table-condensed table-hover table-striped table-bordered'
                       style="font-size: 12px">
                    <tbody>
                    <tr class="bg-corporate-darker">
                        <td class="font-size-h5 font-w600 text-right">&nbsp;</td>

                        @foreach ($period as $date)
                            <td class="font-size-h5 font-w600 text-white text-right"><strong>{{$date->format('F, y')}} &nbsp</strong></td>
                        @endforeach
                    </tr>
                    <tr class="bg-warning-light">
                        <td class="bg-warning font-size-h5 font-w600 text-left">&nbsp;Sales </td>
                        @foreach ($period as $date)
                            <td class="font-size-md font-w600 text-right"><strong>৳
                                    {{number_format(($all_sales
                                        ->where('month', $date->format('n'))
                                        ->where('year', $date->format('Y'))
                                        ->whereIn('branch_id', $branch)
                                        ->sum('net_sales_total'))/$million,2)}}M</strong>
                            </td>
                        @endforeach
                    </tr>
                    <tr class="">
                        <td class="font-size-h5 font-w600 text-left">&nbsp;Collection</td>
                        @foreach ($period as $date)
                            <td class="font-size-md font-w600 text-right"><strong>৳
                                    {{number_format(($all_collections
                                        ->where('month', $date->format('n'))
                                        ->where('year', $date->format('Y'))
                                        ->whereIn('branch_id', $branch)
                                        ->sum('collection'))/$million,2)}}M</strong>
                            </td>
                        @endforeach
                    </tr>
                    <tr class="bg-info-light">
                        <td class="bg-info font-size-h5 font-w600 text-left">&nbsp;Deposit</td>
                        @foreach ($period as $date)
                            <td class="font-size-md font-w600 text-right"><strong>৳
                                    {{number_format(($all_deposits
                                        ->where('month', $date->format('n'))
                                        ->where('year', $date->format('Y'))
                                        ->whereIn('branch_id', $branch)
                                        ->sum('amount'))/$million,2)}}M</strong>
                            </td>
                        @endforeach
                    </tr>
                    <tr class="bg-danger-light">
                        <td class="bg-danger font-size-h5 font-w600 text-left">&nbsp;Expense</td>
                        @foreach ($period as $date)
                            <td class="font-size-md font-w600 text-right"><strong>৳
                                    {{number_format((($all_expense_cq
                                        ->where('month', $date->format('n'))
                                        ->where('year', $date->format('Y'))
                                        ->whereIn('branch_id', $branch)
                                        ->sum('total_expense_cq')) + ($all_expense_cq
                                        ->where('month', $date->format('n'))
                                        ->where('year', $date->format('Y'))
                                        ->whereIn('branch_id', $branch)
                                        ->sum('total_exp')))/$million,2)}}M</strong>
                            </td>
                        @endforeach
                    </tr>
                    <tr class="bg-success-light">
                        <td class="bg-success font-size-h5 font-w600 text-left">&nbsp;Outstanding</td>
                        @php
                            $total_opening = $all_opening->whereIn('branch_id', $branch)
                                         ->sum('collection');
                        @endphp

                        @foreach ($period as $date)
                            @php
                                $total_sales = $all_sales
                                       ->where('month', $date->format('n'))
                                       ->where('year', $date->format('Y'))
                                       ->whereIn('branch_id', $branch)
                                        ->where('trans_date', '>=', '2019-07-01')
                                       ->sum('net_sales_total');

                                $total_collection = $all_collections
                                       ->where('month', $date->format('n'))
                                       ->where('year', $date->format('Y'))
                                       ->whereIn('branch_id', $branch)
                                        ->where('trans_date', '>=', '2019-07-01')
                                       ->sum('collection');
                            @endphp
                            <td class="font-size-md font-w600 text-right">
                                <strong>৳
                                    {{number_format((($opening + $total_sales - $total_collection)/$million),2)}}M
                                </strong>
                            </td>
                        @endforeach
                    </tr>
                    </tbody>
                </table>
            </div>
        </a>
    </div>
    <!-- Row #1 -->
    <!-- END Row #1 -->
</div>
