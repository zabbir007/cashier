<div class="row gutters-tiny" data-toggle="appear">
    <!-- Row #1 -->
    <div class="col-6 col-xl-4">
        <a class="block block-link-shadow text-right bg-warning-light" href="javascript:void(0)">
            <div class="block-content block-content-full clearfix" style="padding-bottom: 5px">
                <table class='table table-condensed table-hover table-striped table-bordered'
                       style="font-size: 12px">
                    <tbody>
                    @foreach($salesPieRow as $sales)
                        <tr>
                            <td class="font-w600 text-left"><strong>{{$sales['brand_name']}}</strong></td>
                            <td class="text-right">{{number_format($sales['sales'],2)}}</td>
                        </tr>
                    @endforeach
                    <tr class="bg-success-light">
                        <td class="text-right"><strong>Total Sales : </strong></td>
                        <td class="text-right"><strong>{{number_format($sales['total'],2)}}</strong></td>
                    </tr>
                    </tbody>
                </table>
            </div>
        </a>
    </div>
    <div class="col-6 col-xl-2">
        <a class="block block-link-shadow text-right" href="javascript:void(0)">
            <div class="block-content block-content-full clearfix" style="padding-bottom: 5px">
                <table class='table table-condensed table-hover table-striped table-bordered'
                       style="font-size: 12px">
                    <tbody>
                    @foreach($collectionsPieRow as $coll)
                        <tr>
                            <td class="text-right">{{number_format($coll['collections'])}}</td>
                        </tr>
                    @endforeach
                    <tr class="bg-success-light">
                        <td class="text-right"><strong>{{number_format($coll['total'])}}</strong></td>
                    </tr>
                    </tbody>
                </table>
            </div>
        </a>
    </div>
    <div class="col-6 col-xl-2">
        <a class="block block-link-shadow text-right bg-info-light" href="javascript:void(0)">
            <div class="block-content block-content-full clearfix" style="padding-bottom: 5px">

                <table class='table table-condensed table-hover table-striped table-bordered'
                       style="font-size: 12px">
                    <tbody>
                    @foreach($depositsPieRow as $deposit)
                        <tr>
                            <td class="text-right">{{number_format($deposit['deposits'])}}</td>
                        </tr>
                    @endforeach
                    <tr class="bg-success-light">
                        <td class="text-right"><strong>{{number_format($deposit['total'])}}</strong></td>
                    </tr>
                    </tbody>
                </table>
            </div>
        </a>
    </div>

    <!-- END Row #1 -->
</div>
