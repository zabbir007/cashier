@php
    $million = 1000000;
@endphp

<div class="row gutters-tiny" data-toggle="appear">
    <!-- Row #1 -->
    <div class="col-12 col-xl-12">
        <a class="block block-link-shadow text-right" href="javascript:void(0)">
            <div class="block-content block-content-full clearfix" style="padding-bottom: 5px">
                <table class='table table-condensed table-hover table-striped table-bordered'
                       style="font-size: 12px">
                    <tbody>
                    <tr class="bg-corporate-darker">
                        <td class="font-size-h5 font-w600 text-right">&nbsp;</td>

                        @foreach ($period as $date)
                            <td class="font-size-h5 font-w600 text-white text-right"><strong>{{$date->format('F, y')}}
                                    &nbsp</strong></td>
                        @endforeach
                    </tr>
                    <tr class="bg-warning-light">
                        <td class="bg-warning font-size-h5 font-w600 text-left">&nbsp;Sales</td>
                        @foreach ($period as $date)
                            <td class="font-size-md font-w600 text-right" title="{{ _money_format_in_crore($all_sales[$date->format('Yn')] ?? 0) }}">
                                <strong>{{ _money_format($all_sales[$date->format('Yn')] ?? 0, true) }}M</strong>
                            </td>
                        @endforeach
                    </tr>
                    <tr class="">
                        <td class="font-size-h5 font-w600 text-left">&nbsp;Collection</td>
                        @foreach ($period as $date)
                            <td class="font-size-md font-w600 text-right" title="{{ _money_format_in_crore($all_collections[$date->format('Yn')] ?? 0) }}"><strong>
                                    {{ _money_format($all_collections[$date->format('Yn')] ?? 0, true) }}M</strong>
                            </td>
                        @endforeach
                    </tr>
                    <tr class="bg-info-light">
                        <td class="bg-info font-size-h5 font-w600 text-left">&nbsp;Deposit</td>
                        @foreach ($period as $date)
                            <td class="font-size-md font-w600 text-right" title="{{ _money_format_in_crore($all_deposits[$date->format('Yn')] ?? 0) }}"><strong>
                                    {{ _money_format($all_deposits[$date->format('Yn')] ?? 0, true) }}M</strong>
                            </td>
                        @endforeach
                    </tr>
                    <tr class="bg-danger-light">
                        <td class="bg-danger font-size-h5 font-w600 text-left">&nbsp;Expense</td>
                        @foreach ($period as $date)
                            <td class="font-size-md font-w600 text-right" title="{{ _money_format_in_crore($all_expense_cq[$date->format('Yn')] ?? 0) }}"><strong>
                                    {{ _money_format($all_expense_cq[$date->format('Yn')] ?? 0, true) }}M</strong>
                            </td>
                        @endforeach
                    </tr>
                    <tr class="bg-success-light">
                        <td class="bg-success font-size-h5 font-w600 text-left">&nbsp;Outstanding</td>
{{--                        @php--}}
{{--                            $total_opening = $all_opening->whereIn('branch_id', $branch)--}}
{{--                                         ->sum('collection');--}}
{{--                        @endphp--}}

                        @foreach ($period as $date)
                            @php
                                $total_sales = $all_sales[$date->format('Yn')] ?? 0;
                                $total_collection = $all_collections[$date->format('Yn')] ?? 0;
                                $closing = $initial_opening + $total_sales - $total_collection;
                            @endphp
                            <td class="font-size-md font-w600 text-right" title="{{ _money_format_in_crore($closing) }}">
                                <strong>{{_money_format($closing, true)}}M
                                </strong>
                            </td>
                        @endforeach
                    </tr>
                    </tbody>
                </table>
            </div>
        </a>
    </div>
</div>
<!-- END Row #1 -->
