@extends ('backend.layouts.app')

@section ('title', trans('labels.backend.access.permissions.management') . ' | ' . trans('labels.backend.access.permissions.edit'))

@section('page-header')
    <h1>
        {{ trans('labels.backend.access.permissions.management') }}
        <small>{{ trans('labels.backend.access.permissions.edit') }}</small>
    </h1>
@endsection

@section('content')
    @component('includes.components.widget')
        @slot('title')
            {{ trans('labels.backend.access.permissions.edit') }}
        @endslot

        @slot('action_buttons')
            @include('backend.access.includes.partials.permission-header-buttons')
        @endslot

        {{ Form::model($permission, ['route' => ['admin.access.permission.update', $permission], 'class' => 'form-horizontal', 'permission' => 'form', 'method' => 'PATCH', 'id' => 'edit-permission']) }}

        <div class="row">
            <div class="col-lg-6">
                <div class="form-group">
                    {{ Form::label('display_name', trans('validation.attributes.backend.access.permissions.display_name'), ['class' => 'col-lg-5 control-label']) }}

                    <div class="col-lg-7">
                        {{ Form::text('display_name', null, ['class' => 'form-control', 'maxlength' => '191', 'required' => 'required', 'autofocus' => 'autofocus', 'placeholder' => trans('validation.attributes.backend.access.permissions.display_name')]) }}
                    </div><!--col-lg-10-->
                </div><!--form control-->
            </div>
        </div>
        <div class="row">
            <div class="col-lg-6">
                <div class="form-group">
                    {{ Form::label('name', trans('validation.attributes.backend.access.permissions.name'), ['class' => 'col-lg-5 control-label']) }}

                    <div class="col-lg-7">
                        {{ Form::text('name', null, ['class' => 'form-control', 'maxlength' => '191', 'required' => 'required', 'autofocus' => 'autofocus', 'placeholder' => trans('validation.attributes.backend.access.permissions.name')]) }}
                    </div><!--col-lg-10-->
                </div><!--form control-->
            </div>
        </div>

        {{ link_to_route('admin.access.permission.index', trans('buttons.general.cancel'), [], ['class' => 'btn btn-danger']) }}

        {{ Form::submit(trans('buttons.general.crud.update'), ['class' => 'btn btn-success']) }}

        {{ Form::close() }}

    @endcomponent

    {{ Form::close() }}
@endsection

@section('after-scripts')
    {{ Html::script('js/backend/access/permissions/script.js') }}
@endsection