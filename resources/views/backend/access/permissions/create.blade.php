@extends ('backend.layouts.app')

@section ('title', trans('labels.backend.access.permissions.management') . ' | ' . trans('labels.backend.access.permissions.create'))

@section('page-header')
    <h1>
        {{ trans('labels.backend.access.permissions.management') }}
        <small>{{ trans('labels.backend.access.permissions.create') }}</small>
    </h1>
@endsection

@section('content')
    @component('includes.components.widget')
        @slot('title')
            {{ trans('labels.backend.access.permissions.create') }}
        @endslot

        @slot('action_buttons')
            @include('backend.access.includes.partials.permission-header-buttons')
        @endslot

        {{ Form::open(['route' => 'admin.access.permission.store', 'class' => 'form-horizontal', 'role' => 'form', 'method' => 'post', 'id' => 'create-permission']) }}

        @include('backend.access.permissions.form')

        {{ link_to_route('admin.access.permission.index', trans('buttons.general.cancel'), [], ['class' => 'btn btn-danger']) }}

        {{ Form::submit(trans('buttons.general.crud.create'), ['class' => 'btn btn-success']) }}

        {{ Form::close() }}

    @endcomponent

@endsection

@section('after-scripts')
    {{ Html::script('js/backend/access/permissions/script.js') }}
@endsection
