@extends ('backend.layouts.app')

@section ('title', trans('labels.backend.access.permissions.management'))

@section('after-styles')
    {{ Html::style("https://cdn.datatables.net/v/bs/dt-1.10.15/datatables.min.css") }}
@endsection

@section('page-header')
    <h1>{{ trans('labels.backend.access.permissions.management') }}</h1>
@endsection

@section('content')
    <div class="row">
        <!-- DOM dataTable -->
        <div class="col-md-12">
            @component('includes.components.widget')

                @slot('title')
                    {{ trans('labels.backend.access.permissions.management') }}
                @endslot

                @slot('action_buttons')
                    @include('backend.access.includes.partials.permission-header-buttons')
                @endslot

                @component('includes.components.table',['class'=>'table table-condensed table-hover table-striped','id'=>'permissions-table'])
                    @slot('thead')
                        <th>{{ trans('labels.backend.access.permissions.table.permission') }}</th>
                        <th>{{ trans('labels.backend.access.permissions.table.display_name') }}</th>
                        <th>{{ trans('labels.general.actions') }}</th>
                    @endslot
                @endcomponent

            @endcomponent
        </div><!-- END column -->
    </div><!-- .row -->
@endsection

@section('after-scripts')
    {{ Html::script("https://cdn.datatables.net/v/bs/dt-1.10.15/datatables.min.js") }}
    {{ Html::script("js/backend/plugin/datatables/dataTables-extend.js") }}

    <script>
        $(function () {
            $('#permissions-table').DataTable({
                dom: 'lfrtip',
                processing: false,
                serverSide: true,
                autoWidth: false,
                ajax: {
                    url: '{{ route("admin.access.permission.get") }}',
                    type: 'post',
                    error: function (xhr, err) {
                        if (err === 'parsererror')
                            location.reload();
                    }
                },
                columns: [
                    {data: 'name', name: '{{config('access.permissions_table')}}.name'},
                    {
                        data: 'display_name',
                        name: '{{config('access.permissions_table')}}.display_name',
                        sortable: false
                    },
                    {data: 'actions', name: 'actions', searchable: false, sortable: false}

                ],
                order: [[1, "asc"]]
            });
        });
    </script>
@endsection
