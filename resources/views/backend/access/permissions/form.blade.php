<div class="row">
    <div class="col-lg-6">
        <div class="form-group">
            {{ Form::label('display_name', trans('validation.attributes.backend.access.permissions.display_name'), ['class' => 'col-lg-5 control-label']) }}

            <div class="col-lg-7">
                {{ Form::text('display_name', null, ['class' => 'form-control', 'maxlength' => '191', 'required' => 'required', 'autofocus' => 'autofocus', 'placeholder' => trans('validation.attributes.backend.access.permissions.display_name')]) }}
            </div><!--col-lg-10-->
        </div><!--form control-->
    </div>
</div>