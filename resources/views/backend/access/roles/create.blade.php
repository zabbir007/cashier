@extends ('backend.layouts.app')

@section ('title', trans('labels.backend.access.roles.management') . ' | ' . trans('labels.backend.access.roles.create'))

@section('page-header')
    <h1>
        {{ trans('labels.backend.access.roles.management') }}
        <small>{{ trans('labels.backend.access.roles.create') }}</small>
    </h1>
@endsection

@section('content')

    @component('includes.components.widget')
        @slot('title')
            {{ trans('labels.backend.access.roles.create') }}
        @endslot

        @slot('action_buttons')
            @include('backend.access.includes.partials.role-header-buttons')
        @endslot

        {{ Form::open(['route' => 'admin.access.role.store', 'class' => 'form-horizontal', 'role' => 'form', 'method' => 'post', 'id' => 'create-role']) }}

        @include('backend.access.roles.form')

        {{ link_to_route('admin.access.role.index', trans('buttons.general.cancel'), [], ['class' => 'btn btn-danger']) }}

        {{ Form::submit(trans('buttons.general.crud.create'), ['class' => 'btn btn-success']) }}

        {{ Form::close() }}

    @endcomponent

@endsection

@section('after-scripts')
    {{ Html::script('js/backend/access/roles/script.js') }}
@endsection
