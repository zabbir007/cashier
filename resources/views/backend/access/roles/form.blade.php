<div class="row">
    <div class="col-lg-6">
        <div class="form-group">
            {{ Form::label('name', trans('validation.attributes.backend.access.roles.name'), ['class' => 'col-lg-5 control-label']) }}

            <div class="col-lg-7">
                {{ Form::text('name', null, ['class' => 'form-control', 'maxlength' => '191', 'required' => 'required', 'autofocus' => 'autofocus', 'placeholder' => trans('validation.attributes.backend.access.roles.name')]) }}
            </div><!--col-lg-10-->
        </div><!--form control-->
    </div>
    <div class="col-lg-6">
        <div class="form-group">
            {{ Form::label('sort', trans('validation.attributes.backend.access.roles.sort'), ['class' => 'col-lg-5 control-label']) }}

            <div class="col-lg-7">
                {{ Form::text('sort', ($roles+1), ['class' => 'form-control', 'placeholder' => trans('validation.attributes.backend.access.roles.sort')]) }}
            </div><!--col-lg-10-->
        </div><!--form control-->
    </div>
</div>

<div class="row">
    <div class="col-lg-6">
        <div class="form-group">
            {{ Form::label('associated-permissions', trans('validation.attributes.backend.access.roles.associated_permissions'), ['class' => 'col-lg-5 control-label']) }}

            <div class="col-lg-7">
                {{ Form::select('associated-permissions', array('all' => trans('labels.general.all'), 'custom' => trans('labels.general.custom')), 'all', ['class' => 'form-control']) }}

                <div id="available-permissions" class="hidden mt-20">
                    <div class="row">
                        <div class="col-xs-12">
                            @if ($permissions->count())
                                @foreach ($permissions as $perm)
                                    <input type="checkbox" name="permissions[{{ $perm->id }}]" value="{{ $perm->id }}"
                                           id="perm_{{ $perm->id }}" {{ is_array(old('permissions')) && in_array($perm->id, old('permissions')) ? 'checked' : '' }} />
                                    <label for="perm_{{ $perm->id }}">{{ $perm->display_name }}</label><br/>
                                @endforeach
                            @else
                                <p>There are no available permissions.</p>
                            @endif
                        </div><!--col-lg-6-->
                    </div><!--row-->
                </div><!--available permissions-->
            </div><!--col-lg-3-->
        </div><!--form control-->
    </div>
</div>
