<div class="pull-right mb-10 hidden-sm hidden-xs">
    {{ link_to_route('admin.access.user.index', trans('menus.backend.access.users.all'), [], ['class' => 'btn  btn-alt-primary btn-sm','data-toggle'=>"click-ripple"]) }}
    {{ link_to_route('admin.access.user.create', trans('menus.backend.access.users.create'), [], ['class' => 'btn  btn-alt-success btn-sm','data-toggle'=>"click-ripple"]) }}
    {{ link_to_route('admin.access.user.deactivated', trans('menus.backend.access.users.deactivated'), [], ['class' => 'btn  btn-alt-warning btn-sm','data-toggle'=>"click-ripple"]) }}
    {{ link_to_route('admin.access.user.deleted', trans('menus.backend.access.users.deleted'), [], ['class' => 'btn  btn-alt-danger btn-sm','data-toggle'=>"click-ripple"]) }}
</div><!--pull right-->


<div class="clearfix"></div>