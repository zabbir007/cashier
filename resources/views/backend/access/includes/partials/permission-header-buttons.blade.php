<div class="pull-right mb-10 hidden-sm hidden-xs">
    {{ link_to_route('admin.access.permission.index', trans('menus.backend.access.permissions.all'), [], ['class' => 'btn  btn-alt-primary btn-sm','data-toggle'=>"click-ripple"]) }}
    {{ link_to_route('admin.access.permission.create', trans('menus.backend.access.permissions.create'), [], ['class' => 'btn  btn-alt-success btn-sm','data-toggle'=>"click-ripple"]) }}
</div><!--pull right-->


<div class="clearfix"></div>