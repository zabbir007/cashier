@extends ('backend.layouts.app')

@section ('title', trans('labels.backend.access.users.management') . ' | ' . trans('labels.backend.access.users.edit'))

@section('page-header')
    <h1>
        {{ trans('labels.backend.access.users.management') }}
        <small>{{ trans('labels.backend.access.users.edit') }}</small>
    </h1>
@endsection

@section('content')
    @component('includes.components.widget')
        @slot('title')
            {{ trans('labels.backend.access.users.edit') }}
        @endslot

        @slot('action_buttons')
            @include('backend.access.includes.partials.user-header-buttons')
        @endslot

        {{ Form::model($user, ['route' => ['admin.access.user.update', $user], 'class' => 'form-horizontal', 'role' => 'form', 'method' => 'PATCH']) }}


        <div class="row">
            <div class="col-lg-6">
                <div class="form-group">
                    {{ Form::label('first_name', trans('validation.attributes.backend.access.users.first_name'), ['class' => 'col-lg-5 control-label']) }}

                    <div class="col-lg-7">
                        {{ Form::text('first_name', null, ['class' => 'form-control', 'maxlength' => '191', 'required' => 'required', 'autofocus' => 'autofocus', 'placeholder' => trans('validation.attributes.backend.access.users.first_name')]) }}
                    </div><!--col-lg-10-->
                </div><!--form control-->
            </div>
            <div class="col-lg-6">
                <div class="form-group">
                    {{ Form::label('last_name', trans('validation.attributes.backend.access.users.last_name'),
                     ['class' => 'col-lg-5 control-label']) }}

                    <div class="col-lg-7">
                        {{ Form::text('last_name', null, ['class' => 'form-control', 'maxlength' => '191', 'required' => 'required', 'placeholder' => trans('validation.attributes.backend.access.users.last_name')]) }}
                    </div><!--col-lg-10-->
                </div><!--form control-->
            </div>
        </div>

        <div class="row">
            <div class="col-lg-6">
                <div class="form-group">
                    {{ Form::label('username', trans('validation.attributes.backend.access.users.username'), ['class' => 'col-lg-5 control-label']) }}

                    <div class="col-lg-7">
                        {{ Form::text('username', null, ['class' => 'form-control', 'maxlength' => '191', 'required' => 'required', 'placeholder' => trans('validation.attributes.backend.access.users.username')]) }}
                    </div><!--col-lg-10-->
                </div><!--form control-->
            </div>
            <div class="col-lg-6">
                <div class="form-group">
                    {{ Form::label('email', trans('validation.attributes.backend.access.users.email'), ['class' => 'col-lg-5 control-label']) }}

                    <div class="col-lg-7">
                        {{ Form::email('email', null, ['class' => 'form-control', 'maxlength' => '191', 'required' => 'required', 'placeholder' => trans('validation.attributes.backend.access.users.email')]) }}
                    </div><!--col-lg-10-->
                </div><!--form control-->
            </div>
        </div>

        <div class="row">
            <div class="col-lg-12">
                <div class="form-group">
                    {{ Form::label('branch_id', trans('Branch List'), ['class' => 'col-lg-2 control-label']) }}

                    <div class="col-lg-8">
                        {!! Form::select('branch_list[]', $branches,$user->branches->pluck('id'), ['class' => 'form-control','id'=>'select', 'multiple']) !!}
                    </div><!--col-lg-10-->
                    <div class="col-lg-2">
                        <input type="checkbox" id="checkbox"> Select All
                    </div>
                </div><!--form control-->
            </div>
        </div>

        <div class="row">
            <div class="col-lg-6">
                <div class="form-group">
                    {{ Form::label('status', trans('validation.attributes.backend.access.users.active'), ['class' => 'col-lg-5 control-label']) }}

                    <div class="col-lg-7">
                        <div class="checkbox">
                            {{ Form::checkbox('status', '1', true,['data-switchery']) }}
                        </div>
                    </div><!--col-lg-1-->
                </div><!--form control-->
            </div>
        </div>

        <div class="row">
            <div class="col-lg-6">
                <div class="form-group">
                    {{ Form::label('confirmed', trans('validation.attributes.backend.access.users.confirmed'), ['class' => 'col-lg-5 control-label']) }}

                    <div class="col-lg-7">
                        {{ Form::checkbox('confirmed', '1', true) }}
                    </div><!--col-lg-1-->
                </div><!--form control-->
            </div>
        </div>


        @if (! config('access.users.requires_approval'))
            <div class="row">
                <div class="col-lg-6">
                    <div class="form-group">
                        <label class="col-lg-5 control-label">{{ trans('validation.attributes.backend.access.users.send_confirmation_email') }}
                            <br/>
                            <small>{{ trans('strings.backend.access.users.if_confirmed_off') }}</small>
                        </label>

                        <div class="col-lg-7">
                            {{ Form::checkbox('confirmation_email', '1') }}
                        </div><!--col-lg-1-->
                    </div><!--form control-->
                </div>
            </div>
        @endif

        <div class="row">
            <div class="col-lg-6">
                <div class="form-group">
                    {{ Form::label('associated_roles', trans('validation.attributes.backend.access.users.associated_roles'), ['class' => 'col-lg-5 control-label']) }}

                    <div class="col-lg-12">
                        @if (count($roles) > 0)
                            @foreach($roles as $role)
                                <input type="checkbox" value="{{ $role->id }}" name="assignees_roles[{{ $role->id }}]"
                                       id="role-{{ $role->id }}" {{ is_array(old('assignees_roles')) && in_array($role->id, old('assignees_roles')) ? 'checked' : '' }} />
                                <label for="role-{{ $role->id }}">{{ $role->name }}</label>
                                <a href="#" data-role="role_{{ $role->id }}" class="show-permissions small">
                                    (
                                    <span class="show-text">{{ trans('labels.general.show') }}</span>
                                    <span class="hide-text hidden">{{ trans('labels.general.hide') }}</span>
                                    {{ trans('labels.backend.access.users.permissions') }}
                                    )
                                </a>
                                <br/>
                                <div class="permission-list hidden" data-role="role_{{ $role->id }}">
                                    @if ($role->all)
                                        {{ trans('labels.backend.access.users.all_permissions') }}<br/><br/>
                                    @else
                                        @if (count($role->permissions) > 0)
                                            <blockquote class="small">{{--
                                        --}}@foreach ($role->permissions as $perm){{--
                                            --}}{{$perm->display_name}}<br/>
                                                @endforeach
                                            </blockquote>
                                        @else
                                            {{ trans('labels.backend.access.users.no_permissions') }}<br/><br/>
                                        @endif
                                    @endif
                                </div><!--permission list-->
                            @endforeach
                        @else
                            {{ trans('labels.backend.access.users.no_roles') }}
                        @endif
                    </div><!--col-lg-3-->
                </div><!--form control-->
            </div>
        </div>

        {{ link_to_route('admin.access.user.index', trans('buttons.general.cancel'), [], ['class' => 'btn btn-danger']) }}

        {{ Form::submit(trans('buttons.general.crud.update'), ['class' => 'btn btn-success']) }}

        @if ($user->id == 1)
            {{ Form::hidden('status', 1) }}
            {{ Form::hidden('assignees_roles[0]', 1) }}
        @endif

        {{ Form::close() }}

    @endcomponent
@endsection

@section('after-scripts')
    {{ Html::script('js/backend/access/users/script.js') }}

    <script type="text/javascript">
        $("#checkbox").click(function () {
            if ($("#checkbox").is(':checked')) {
                $("select > option").prop("selected", true);
                $("select").trigger("change");
            } else {
                $("select > option").prop("selected", false);
                $("select").trigger("change");
            }
        });

        $("#button").click(function () {
            alert($("select").val());
        });
    </script>
@endsection

