@extends ('backend.layouts.app')

@section ('title', trans('labels.backend.access.users.management') . ' | ' . trans('labels.backend.access.users.create'))

@section('page-header')
    <h1>
        {{ trans('labels.backend.access.users.management') }}
        <small>{{ trans('labels.backend.access.users.create') }}</small>
    </h1>
@endsection

@section('content')
    @component('includes.components.widget')
        @slot('title')
            {{ trans('labels.backend.access.users.create') }}
        @endslot

        @slot('action_buttons')
            @include('backend.access.includes.partials.user-header-buttons')
        @endslot

        {{ Form::open(['route' => 'admin.access.user.store', 'class' => 'form-horizontal', 'role' => 'form', 'method' => 'post']) }}

        @include('backend.access.form')

        {{ link_to_route('admin.access.user.index', trans('buttons.general.cancel'), [], ['class' => 'btn btn-danger']) }}

        {{ Form::submit(trans('buttons.general.crud.create'), ['class' => 'btn btn-success']) }}

        {{ Form::close() }}

    @endcomponent
@endsection

@section('after-scripts')
    {{ Html::script('js/backend/access/users/script.js') }}
@section('after-scripts')
    <script type="text/javascript">
        $("#checkbox").click(function () {
            if ($("#checkbox").is(':checked')) {
                $("select > option").prop("selected", true);
                $("select").trigger("change");
            } else {
                $("select > option").prop("selected", false);
                $("select").trigger("change");
            }
        });

        $("#button").click(function () {
            alert($("select").val());
        });
    </script>
@endsection
@endsection
