<div class="row">
    <div class="col-lg-6">
        <div class="form-group">
            {{ Form::label('first_name', trans('validation.attributes.backend.access.users.first_name'), ['class' => 'col-lg-5 control-label']) }}

            <div class="col-lg-7">
                {{ Form::text('first_name', null, ['class' => 'form-control', 'maxlength' => '191', 'required' => 'required', 'autofocus' => 'autofocus', 'placeholder' => trans('validation.attributes.backend.access.users.first_name')]) }}
            </div><!--col-lg-10-->
        </div><!--form control-->
    </div>
    <div class="col-lg-6">
        <div class="form-group">
            {{ Form::label('last_name', trans('validation.attributes.backend.access.users.last_name'),
             ['class' => 'col-lg-5 control-label']) }}

            <div class="col-lg-7">
                {{ Form::text('last_name', null, ['class' => 'form-control', 'maxlength' => '191', 'required' => 'required', 'placeholder' => trans('validation.attributes.backend.access.users.last_name')]) }}
            </div><!--col-lg-10-->
        </div><!--form control-->
    </div>
</div>

<div class="row">
    <div class="col-lg-6">
        <div class="form-group">
            {{ Form::label('username', trans('Username'), ['class' => 'col-lg-5 control-label']) }}

            <div class="col-lg-7">
                {{ Form::text('username', null, ['class' => 'form-control', 'maxlength' => '191', 'required' => 'required', 'placeholder' => trans('Username')]) }}
            </div><!--col-lg-10-->
        </div><!--form control-->
    </div>
    <div class="col-lg-6">
        <div class="form-group">
            {{ Form::label('email', trans('validation.attributes.backend.access.users.email'), ['class' => 'col-lg-5 control-label']) }}

            <div class="col-lg-7">
                {{ Form::email('email', null, ['class' => 'form-control', 'maxlength' => '191', 'required' => 'required', 'placeholder' => trans('validation.attributes.backend.access.users.email')]) }}
            </div><!--col-lg-10-->
        </div><!--form control-->
    </div>
</div>

<div class="row">
    <div class="col-lg-6">
        <div class="form-group">
            {{ Form::label('password', trans('validation.attributes.backend.access.users.password'), ['class' => 'col-lg-5 control-label']) }}

            <div class="col-lg-7">
                {{ Form::password('password', ['class' => 'form-control', 'required' => 'required', 'placeholder' => trans('validation.attributes.backend.access.users.password')]) }}
            </div><!--col-lg-10-->
        </div><!--form control-->
    </div>
    <div class="col-lg-6">
        <div class="form-group">
            {{ Form::label('password_confirmation', trans('validation.attributes.backend.access.users.password_confirmation'), ['class' => 'col-lg-5 control-label']) }}

            <div class="col-lg-7">
                {{ Form::password('password_confirmation', ['class' => 'form-control', 'required' => 'required', 'placeholder' => trans('validation.attributes.backend.access.users.password_confirmation')]) }}
            </div><!--col-lg-10-->
        </div><!--form control-->
    </div>
</div>

<div class="row">
    <div class="col-lg-12">
        <div class="form-group">
            {{ Form::label('branch_id', trans('Branch List'), ['class' => 'col-lg-2 control-label']) }}

            <div class="col-lg-8">
                {!! Form::select('branch_list[]', $branches, null, ['class' => 'form-control','id'=>'select', 'multiple']) !!}

            </div><!--col-lg-10-->
            <div class="col-lg-2">
                <input type="checkbox" id="checkbox"> Select All
            </div>
        </div><!--form control-->
    </div>
</div>


<div class="row">
    <div class="col-lg-6">
        <div class="form-group">
            {{ Form::label('status', trans('validation.attributes.backend.access.users.active'), ['class' => 'col-lg-5 control-label']) }}

            <div class="col-lg-7">
                <div class="checkbox">
                    {{ Form::checkbox('status', '1', true,['data-switchery']) }}
                </div>
            </div><!--col-lg-1-->
        </div><!--form control-->
    </div>
</div>

<div class="row">
    <div class="col-lg-6">
        <div class="form-group">
            {{ Form::label('confirmed', trans('validation.attributes.backend.access.users.confirmed'), ['class' => 'col-lg-5 control-label']) }}

            <div class="col-lg-7">
                {{ Form::checkbox('confirmed', '1', true) }}
            </div><!--col-lg-1-->
        </div><!--form control-->
    </div>
</div>


@if (! config('access.users.requires_approval'))
    <div class="row">
        <div class="col-lg-6">
            <div class="form-group">
                <label class="col-lg-5 control-label">{{ trans('validation.attributes.backend.access.users.send_confirmation_email') }}
                    <br/>
                    <small>{{ trans('strings.backend.access.users.if_confirmed_off') }}</small>
                </label>

                <div class="col-lg-7">
                    {{ Form::checkbox('confirmation_email', '1') }}
                </div><!--col-lg-1-->
            </div><!--form control-->
        </div>
    </div>
@endif

<div class="row">
    <div class="col-lg-6">
        <div class="form-group">
            {{ Form::label('associated_roles', trans('validation.attributes.backend.access.users.associated_roles'), ['class' => 'col-lg-5 control-label']) }}

            <div class="col-lg-7">
                @if (count($roles) > 0)
                    @foreach($roles as $role)
                        <input type="checkbox" value="{{ $role->id }}" name="assignees_roles[{{ $role->id }}]"
                               id="role-{{ $role->id }}" {{ is_array(old('assignees_roles')) && in_array($role->id, old('assignees_roles')) ? 'checked' : '' }} />
                        <label for="role-{{ $role->id }}">{{ $role->name }}</label>
                        <a href="#" data-role="role_{{ $role->id }}" class="show-permissions small">
                            (
                            <span class="show-text">{{ trans('labels.general.show') }}</span>
                            <span class="hide-text hidden">{{ trans('labels.general.hide') }}</span>
                            {{ trans('labels.backend.access.users.permissions') }}
                            )
                        </a>
                        <br/>
                        <div class="permission-list hidden" data-role="role_{{ $role->id }}">
                            @if ($role->all)
                                {{ trans('labels.backend.access.users.all_permissions') }}<br/><br/>
                            @else
                                @if (count($role->permissions) > 0)
                                    <blockquote class="small">{{--
                                        --}}@foreach ($role->permissions as $perm){{--
                                            --}}{{$perm->display_name}}<br/>
                                        @endforeach
                                    </blockquote>
                                @else
                                    {{ trans('labels.backend.access.users.no_permissions') }}<br/><br/>
                                @endif
                            @endif
                        </div><!--permission list-->
                    @endforeach
                @else
                    {{ trans('labels.backend.access.users.no_roles') }}
                @endif
            </div><!--col-lg-3-->
        </div><!--form control-->
    </div>
</div>