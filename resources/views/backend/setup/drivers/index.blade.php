@extends ('backend.layouts.app')

@section ('title', $page_heading)

@section('content')
    <div class="row">
        <!-- DOM dataTable -->
        <div class="col-md-12">
            @component('includes.components.widget')

                @slot('title')
                    {{ $page_heading }}
                @endslot

                @slot('action_buttons')
                    @include($module_view.'.header-buttons')
                @endslot

                {{--Widget body start--}}
                @component('includes.components.table',['class'=>'table table-condensed table-hover table-striped table-bordered','id'=>''])
                    @slot('thead')
                        <th>SN</th>
                        <th>Short Name</th>
                        <th>Name</th>
                        <th>Address</th>
                        <th>Status</th>
                        <th>{{ __('labels.general.actions') }}</th>
                    @endslot

                    @slot('tbody')
                        @foreach($$module_name as $module_name)
                            <tr>
                            <tr>
                                <td class="text-center">{{ $loop->iteration }}</td>
                                <td> {{ $module_name->short_name }}</td>
                                <td> {{ $module_name->name }}</td>
                                <td> {{ $module_name->address }}</td>
                                <td><span class="badge badge-success">{!! $module_name->status_label !!}</span></td>
                                <td> {!! $module_name->action_buttons !!}</td>
                            </tr>
                            </tr>
                        @endforeach
                    @endslot
                @endcomponent

            @endcomponent
        </div><!-- END column -->
    </div><!-- .row -->
@endsection
