@extends ('backend.layouts.app')

@section ('title', $page_heading)

@section('page-header')
    <h1>
        {{ $page_heading }}
    </h1>
@endsection

@section('content')
    @component('includes.components.widget')
        @slot('title')
            {{ $page_heading }}
        @endslot

        @slot('action_buttons')
            @include($module_view.'.header-buttons')
        @endslot

        {{ Form::model($module_name_singular, ['route' => [$module_route.'.update', $module_name_singular], 'class' => 'form-horizontal', 'role' => 'form', 'method' => 'PATCH']) }}

        <div class="row">
            <div class="col-sm-6">
                <div class="form-group row has-feedback">
                    {!! Form::label('name', 'Driver Name',['class' => 'col-lg-4 col-form-label']) !!}
                    <div class="col-lg-8">
                        {!! Form::text('name', $$module_name_singular->name , ['class' => 'form-control','placeholder' => 'Ex: Jabbir Hossain Joy','required']) !!}
                        <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
                        <div class="help-block with-errors"></div>
                    </div>
                </div>

                <div class="form-group row has-feedback">
                    {!! Form::label('short_name', 'Driver Short Name',['class' => 'col-lg-4 col-form-label']) !!}
                    <div class="col-lg-8">
                        {!! Form::text('short_name', $$module_name_singular->short_name , ['class' => 'form-control','placeholder' => 'Ex: joy','required']) !!}
                        <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
                        <div class="help-block with-errors"></div>
                    </div>
                </div>
            </div>
            <div class="col-sm-6">
                <div class="form-group row has-feedback">
                    {!! Form::label('address', 'Address',['class' => 'col-lg-4 col-form-label']) !!}
                    <div class="col-lg-8">
                        {!! Form::text('address', $$module_name_singular->address , ['class' => 'form-control', 'placeholder' => 'Dhaka , Bangladesh']) !!}
                        <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
                        <div class="help-block with-errors"></div>
                    </div>
                </div>

                <div class="form-group row has-feedback">
                    {!! Form::label('location', 'Location',['class' => 'col-lg-4 col-form-label']) !!}
                    <div class="col-lg-8">
                        {!! Form::text('remarks', $$module_name_singular->remarks , ['class' => 'form-control','placeholder' => 'Ex: road number: 12','required']) !!}
                        <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
                        <div class="help-block with-errors"></div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-6">
                <div class="form-group row has-feedback">
                    {!! Form::label('second_address', 'Second Address',['class' => 'col-lg-4 col-form-label']) !!}
                    <div class="col-lg-8">
                        {!! Form::text('second_address', $$module_name_singular->second_address , ['class' => 'form-control', 'placeholder' => 'Dhaka Gulshan-2 , Bangladesh']) !!}
                        <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
                        <div class="help-block with-errors"></div>
                    </div>
                </div>
                {!! Form::hidden('id', $$module_name_singular->id , ['class' => 'form-control']) !!}
                <div class="form-group row has-feedback">
                    {!! Form::label('phone', 'Phone Number',['class' => 'col-lg-4 col-form-label']) !!}
                    <div class="col-lg-8">
                        {!! Form::text('phone', $$module_name_singular->phone , ['class' => 'form-control','placeholder' => 'Ex: +8801613722564','required']) !!}
                        <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
                        <div class="help-block with-errors"></div>
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-lg-6">
                {!! Form::button(('<i class="fa fa-check fa-fw"></i>&nbsp;'.$module_action), ['class' => 'btn btn-success pull-right','type'=>'submit']) !!}
            </div>

            <div class="col-lg-6">
                {!! link_to(route($module_route.'.index'), ('<i class="fa fa-mail-reply"></i>&nbsp;'.trans('buttons.general.cancel')), ['class' => 'btn btn-danger pull-left'], null, false) !!}
            </div><!--col-lg-1-->
        </div><!--form control-->
        <br><br>
        {{ Form::close() }}

    @endcomponent
@endsection
