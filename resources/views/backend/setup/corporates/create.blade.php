@extends ('backend.layouts.app')

@section ('title',$page_heading)

@section('after-styles')
    <style type="text/css">
        .input-group-addon {
            padding: 3px 5px !important;
        }

        .required {
            content: "*";
            position: absolute;
            top: 11px;
            right: 3px;
            font-size: 15px;
            color: red;
            font-weight: 700;
        }

        .input-group .required {
            right: -9px;
        }
    </style>
@endsection

@section('content')
    @component('includes.components.widget')
        @slot('title')
            {{ $page_heading }}
        @endslot

        @slot('action_buttons')
            @include($module_view.'.header-buttons')
        @endslot

        {{ Form::open(['route' => $module_route.'.store','files' => true, 'class' => 'form-horizontal', 'role' => 'form', 'method' => 'post']) }}

        @include($module_view.'.form')

        <div class="row">
            <div class="col-lg-6">
                {!! Form::button(('<i class="fa fa-check fa-fw"></i>&nbsp;'.$module_action), ['class' => 'btn btn-success pull-right','type'=>'submit']) !!}
            </div>

            <div class="col-lg-6">
                {!! link_to(route($module_route.'.index'), ('<i class="fa fa-mail-reply"></i>&nbsp;'.trans('buttons.general.cancel')), ['class' => 'btn btn-danger pull-left'], null, false) !!}
            </div><!--col-lg-1-->
        </div><!--form control-->

        <br>
        {{ Form::close() }}

    @endcomponent
@endsection

@section('after-scripts')

    <script>
        new Vue({
            el: '#app',
            data: {
                selectedBankId: '',
                accountNumber:{}
            },
            mounted: function () {
                let $select2 = {width: '100%'};
                $("#banks").select2($select2).on("change", (e) => {
                    this.selectedBankId = $(e.currentTarget).val();
                    console.log($(e.currentTarget).val());
                    this.getAccountNumber();
                })
            },
            methods: {
                getAccountNumber: function () {
                    this.loading = true;
                    this.disableSubmitBtn = true;
                    // $("#banks").prop("disabled", true);

                    let config = {
                        //  headers: {'Authorization': 'JWT ' + this.$store.state.token},
                        params: {
                            branch: 1,
                            bank: this.selectedBankId
                        }
                    };
                    axios.get('{{ route('admin.setup.corporates.getAccount') }}', config)
                        .then((response) => {
                            if (_.isEmpty(response.data)) {
                                swal.fire('Warning', 'No Account Number Found!', 'warning');
                                this.accountNumber = '';
                                this.disableSubmitBtn = true;
                            } else {
                                this.accountNumber = response.data;

                            }
                        })
                        .catch(error => {

                        })
                }
            }
        })
    </script>

    <script type="text/javascript">
        $(document).ready(function () {
            //add required

            $("input[required='required'], select").each(function () {
                $(this).before('<span class="required">*</span>');
            });

            var $pwdField = $("#pwdField");
            $pwdField.val($pwdField.data('value'));
            $("#pwdChk").on('click', function (e) {
                if (!$(this).prop('checked')) {
                    $pwdField.removeAttr('readonly');
                    $pwdField.val('');
                    $pwdField.focus();
                } else {
                    $pwdField.prop('readonly', true);
                    $pwdField.val($pwdField.data('value'));
                }
            });
        });
    </script>

@endsection
