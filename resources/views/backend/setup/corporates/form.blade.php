<div class="row">
    <div class="col-sm-6">
        <div class="form-group row has-feedback">
            {!! Form::label('trans_date', 'Transaction Date',['class' => 'col-lg-4 col-form-label']) !!}
            <div class="col-lg-8">
                {{--{!! Form::text('trans_date',$carbon->format('d-M-Y'),  ['class' => 'form-control','readonly']) !!}--}}
                {!! Form::text('trans_date',$carbon->format('d-M-Y'),  ['class' => 'form-control datepicker','data-week-start'=>"6", 'data-autoclose'=>"true", 'data-today-highlight'=>"true", 'data-date-format'=>"dd-M-yyyy", 'placeholder'=>"mm/dd/yy",'readonly']) !!}
                <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
                <div class="help-block with-errors"></div>
            </div>
        </div>
    </div>

{{--    @if(count(access()->user()->branches)>1)--}}
{{--        <div class="col-sm-6">--}}
{{--            <div class="form-group row has-feedback">--}}
{{--                {!! Form::label('branch_name', 'Branch Name',['class' => 'col-lg-4 col-form-label']) !!}--}}
{{--                <div class="col-lg-8">--}}
{{--                    {!! Form::select('branch_id',$branches,old('branch_name'),  ['class' => 'form-control','readonly']) !!}--}}
{{--                    <span class="glyphicon form-control-feedback" aria-hidden="true"></span>--}}
{{--                    <div class="help-block with-errors"></div>--}}
{{--                </div>--}}
{{--            </div>--}}
{{--        </div>--}}
{{--    @else--}}
        <div class="col-sm-6">
            <div class="form-group row has-feedback">
                {!! Form::label('branch_name', 'Branch Name',['class' => 'col-lg-4 col-form-label']) !!}
                <div class="col-lg-8">
                    <p>{{$bran->name}}</p>
                    <input type="hidden" value="{{$bran->id}}" name="branch_id">
                </div>
            </div>
        </div>


{{--    @endif--}}

</div>



<br>
<div class="row">
    <div class="col-sm-6">
        <div class="form-group row has-feedback">
            {!! Form::label('bank_name', 'Bank Name',['class' => 'col-lg-4 col-form-label']) !!}
            <div class="col-lg-8">
                {{--                    {!! Form::hidden('branch_id',$branches->id,  ['class' => 'form-control','readonly']) !!}--}}
                {{--                    {!! Form::text('branch_name',$branches->name,  ['class' => 'form-control','readonly']) !!}--}}
                {!! Form::select('bank_id', $banks, null, ['class' => 'form-control exclude-select2','id'=>'banks','placeholder'=>'Select a bank','v-model'=>'selectedBankId']) !!}
                <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
                <div class="help-block with-errors"></div>
            </div>
        </div>
    </div>
    <div class="col-sm-6">
        <div class="form-group row has-feedback">

            <div class="col-lg-4">
                <label>Account Number</label>
            </div>

            {{--                <input type="text" v-model="">--}}
            <div class="col-lg-8">
                <select name="accountNumber" class="form-control exclude-select2">
                    <option value="">Select One</option>
                    <option v-for="account in accountNumber" v-bind:value="account.account_number">@{{account.account_number}}</option>
                </select>
            </div>
        </div>
    </div>
</div>
<br>
<div class="row">
    <div class="col-md-6">
        <div class="form-group row has-feedback">
            {!! Form::label('pc_bank_amount', 'Balance as accpac ledger',['class' => 'col-lg-4 col-form-label']) !!}
            <div class="col-lg-8">
                {!! Form::number('pc_bank_amount',old('pc_bank_amount'),  ['class' => 'form-control text-right','step' => 'any']) !!}
                <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
                <div class="help-block with-errors"></div>
            </div>
        </div>
    </div>
    <div class="col-md-6">
{{--        <div class="form-group row has-feedback">--}}
{{--            {!! Form::label('pc_cash_amount', 'Cash Amount (P/C)',['class' => 'col-lg-4 col-form-label']) !!}--}}
{{--            <div class="col-lg-8">--}}
{{--                {!! Form::number('pc_cash_amount',old('pc_cash_amount'),  ['class' => 'form-control text-right','min' => 0     , 'step' => 'any']) !!}--}}
{{--                <span class="glyphicon form-control-feedback" aria-hidden="true"></span>--}}
{{--                <div class="help-block with-errors"></div>--}}
{{--            </div>--}}
{{--        </div>--}}
    </div>


</div>
<br><br>
