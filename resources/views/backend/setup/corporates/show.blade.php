@extends ('backend.layouts.app')

@section ('title', $page_heading)

@section('page-header')
    <h1>
        {{ $page_heading }}
    </h1>

@endsection

@section('content')
    <div class="row">
        <div class="col-md-12">
            @component('includes.components.widget')

                @slot('title')
                    {{ $page_heading }}
                @endslot

                @slot('action_buttons')
                    <a href="{{ route($module_route.".index") }}" class="btn-block-option"><i class="si si-grid"></i>&nbsp;All {{ title_case($module_name) }}
                    </a>
                    <button type="button" class="btn-block-option" onclick="Codebase.helpers('print-page');">
                        <i class="si si-printer"></i> Print
                    </button>
                    <button type="button" class="btn-block-option" data-toggle="block-option"
                            data-action="fullscreen_toggle"></button>
                    <button type="button" class="btn-block-option" data-toggle="block-option" data-action="state_toggle"
                            data-action-mode="demo">
                        <i class="si si-refresh"></i>
                    </button>
                @endslot
                {{ Form::model($module_name_singular, ['route' => [$module_route.'.update', $module_name_singular], 'class' => 'form-horizontal', 'role' => 'form', 'method' => 'PATCH']) }}

                <div class="row">

                    <div class="col-lg-12">
                        @component('includes.components.table',['class'=>'table table-borderless','style'=> 'font-size:14px','id'=>''])

                            @slot('tbody')
                                <tr>
                                    <td style="font-weight: bold">
                                        Transaction Date
                                    </td>
                                    <td> :</td>
                                    <td>{{ date('d-M-Y',strtotime($module_name_singular->trans_date)) }}</td>
                                    <td style="font-weight: bold">
                                        Branch Name
                                    </td>
                                    <td> :</td>
                                    <td>{{ $module_name_singular->branch_name }}</td>
                                    <td style="font-weight: bold">
                                        Bank Name
                                    </td>
                                    <td> :</td>
                                    <td>{{ $module_name_singular->bank_name }}</td>
                                    <td style="font-weight: bold">
                                        Account Number
                                    </td>
                                    <td> :</td>
                                    <td>{{ $module_name_singular->account_number }}</td>


                                </tr>
                            @endslot
                        @endcomponent
                    </div>

                    <div class="col-lg-5">
                        @component('includes.components.table',['class'=>'table  table-bordered fixed_headers','style'=>'font-size:16px','id'=>''])

                            @slot('tbody')
                                @php
                                    $bank = $module_name_singular->pc_bank_amount;
                                    //$cash = $module_name_singular->pc_cash_amount;
                                @endphp
{{--                                <tr class="bg-gray-darker text-white" style="font-weight: bold">--}}
{{--                                    <td>--}}
{{--                                        Cash Amount (P/C)--}}
{{--                                    </td>--}}
{{--                                    <td class="text-right">{{ number_format($cash,2) }} Tk</td>--}}
{{--                                </tr>--}}
                                <tr class="bg-gray-darker text-white" style="font-weight: bold">
                                    <td>
                                        Balance as accpac ledger
                                    </td>
                                    <td class="text-right">{{ number_format($bank,2) }} Tk</td>
                                </tr>

                            @endslot
                        @endcomponent
                    </div>
                </div>
                <br><br>
                {{ Form::close() }}
            @endcomponent
        </div><!-- END column -->
    </div><!-- .row -->
@endsection
