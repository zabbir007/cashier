@extends ('backend.layouts.app')

@section ('title', $page_heading)

@section('page-header')
    <h1>
        {{ $page_heading }}
    </h1>

@endsection

@section('content')
    <div class="row">
        <div class="col-md-12">
            @component('includes.components.widget')

                @slot('title')
                    {{ $page_heading }}
                @endslot

                @slot('action_buttons')
                    <a href="{{ route($module_route.".index") }}" class="btn-block-option"><i class="si si-grid"></i>&nbsp;All {{ title_case($module_name) }}
                    </a>
                    <button type="button" class="btn-block-option" onclick="Codebase.helpers('print-page');">
                        <i class="si si-printer"></i> Print Invoice
                    </button>
                    <button type="button" class="btn-block-option" data-toggle="block-option"
                            data-action="fullscreen_toggle"></button>
                    <button type="button" class="btn-block-option" data-toggle="block-option" data-action="state_toggle"
                            data-action-mode="demo">
                        <i class="si si-refresh"></i>
                    </button>
                @endslot
                {{ Form::model($module_name_singular, ['route' => [$module_route.'.update', $module_name_singular], 'class' => 'form-horizontal', 'role' => 'form', 'method' => 'PATCH']) }}

                <div class="row">

                    <div class="col-lg-8">
                        @component('includes.components.table',['class'=>'table table-borderless','style'=> 'font-size:14px','id'=>''])

                            @slot('tbody')
                                <tr>
                                    <td style="font-weight: bold">
                                        Transaction Date
                                    </td>
                                    <td> :</td>
                                    <td>{{ date('d-M-Y',strtotime($module_name_singular->trans_date)) }}</td>
                                    <td style="font-weight: bold">
                                        Branch Name
                                    </td>
                                    <td> :</td>
                                    <td>{{ $module_name_singular->branch_name }}</td>


                                </tr>
                            @endslot
                        @endcomponent
                    </div>
                    <div class="col-lg-7">
                        @component('includes.components.table',['class'=>'table  table-bordered fixed_headers','style'=>'font-size:12px','id'=>''])
                            @slot('thead')
                                <th style="font-size: 14px">Brand</th>
                                <th class="text-right" style="font-size: 14px">Collection</th>
                                <th class="text-right" style="font-size: 14px">Outstanding</th>

                            @endslot

                            @slot('tbody')
                                @php
                                    $dt = $module_name_singular->details;
                                @endphp
                                <tr class="bg-gray-darker text-white" style="font-weight: bold">
                                    <td>
                                        Total
                                    </td>
                                    <td class="text-right ">{{ number_format(($dt->sum('collection')),2) }} Tk</td>
                                    <td class="text-right ">{{ number_format(($dt->sum('outstanding')),2) }} Tk</td>

                                </tr>
                                @foreach($products as $t)

                                    <tr class="bg-gray text-navy" style="font-weight: bold">
                                        <td>
                                            {{$t["brand_name"]}}
                                        </td>
                                        <td class="text-right ">{{ number_format(($dt->where('brand_id',$t["brand_id"])->sum('collection')),2) }}
                                            Tk
                                        </td>
                                        <td class="text-right ">{{ number_format(($dt->where('brand_id',$t["brand_id"])->sum('outstanding')),2) }}
                                            Tk
                                        </td>
                                    </tr>
                                    @foreach($t["all"] as $product)
                                        @php
                                            $details = $module_name_singular->details->where('product_id',$product->id)->first();
                                     $dtSumColl =  0;

                                     if($details){
                                         $dtSumColl = $details->collection ;
                                         $dtSumOut = $details->outstanding ;
                                     }
                                        @endphp
                                        <tr class="text-right">
                                            <td width="150px" class="text-left"> {{ $product->name }}</td>
                                            <td width="50px">
                                                {{ $dtSumColl }} Tk
                                            </td>
                                            <td width="50px">
                                                {{ $dtSumOut }} Tk
                                            </td>

                                        </tr>
                                    @endforeach
                                @endforeach
                            @endslot
                        @endcomponent

                        <br>

                    </div>
                    <div class="col-lg-5">
                        @component('includes.components.table',['class'=>'table  table-bordered fixed_headers','style'=>'font-size:16px','id'=>''])

                            @slot('tbody')
                                @php
                                    $bank = $module_name_singular->pc_bank_amount;
                                    $cash = $module_name_singular->pc_cash_amount;
                                @endphp
                                <tr class="bg-gray-darker text-white" style="font-weight: bold">
                                    <td>
                                        Cash Amount (P/C)
                                    </td>
                                    <td class="text-right">{{ number_format($cash,2) }} Tk</td>
                                </tr>
                                <tr class="bg-gray-darker text-white" style="font-weight: bold">
                                    <td>
                                        Bank Amount (P/C)
                                    </td>
                                    <td class="text-right">{{ number_format($bank,2) }} Tk</td>
                                </tr>

                            @endslot
                        @endcomponent
                    </div>
                </div>
                <br><br>
                {{ Form::close() }}
            @endcomponent
        </div><!-- END column -->
    </div><!-- .row -->
@endsection