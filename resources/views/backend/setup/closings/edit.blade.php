@extends ('backend.layouts.app')

@section ('title', $page_heading)

@section('page-header')
    <h1>
        {{ $page_heading }}
    </h1>
@endsection

@section('content')
    @component('includes.components.widget')
        @slot('title')
            {{ $page_heading }}
        @endslot

        @slot('action_buttons')
            @include($module_view.'.header-buttons')
        @endslot

        {{ Form::model($module_name_singular, ['route' => [$module_route.'.update', $module_name_singular], 'class' => 'form-horizontal', 'role' => 'form', 'method' => 'PATCH']) }}

        <div class="row">
            <div class="col-sm-6">
                <div class="form-group row has-feedback">
                    {!! Form::label('trans_date', 'Transaction Date',['class' => 'col-lg-4 col-form-label']) !!}
                    <div class="col-lg-8">
                        {!! Form::hidden('sl', $module_name_singular->sl, ['class' => 'form-control', 'placeholder'=>'XXXX-XXXX','readonly']) !!}
                        {!! Form::text('trans_date',date('d-M-Y',strtotime($module_name_singular->trans_date)),  ['class' => 'form-control','readonly']) !!}
                        <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
                        <div class="help-block with-errors"></div>
                    </div>
                </div>
            </div>


            <div class="col-sm-6">
                <div class="form-group row has-feedback">
                    {!! Form::label('branch_name', 'Branch Name',['class' => 'col-lg-4 col-form-label']) !!}
                    <div class="col-lg-8">
                        {!! Form::hidden('branch_id',$module_name_singular->branch_id,  ['class' => 'form-control','readonly']) !!}
                        {!! Form::text('branch_name',$module_name_singular->branch_name,  ['class' => 'form-control','readonly']) !!}
                        <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
                        <div class="help-block with-errors"></div>
                    </div>
                </div>
            </div>


        </div>
        <br>
        <div class="row">
            <div class="col-md-4"></div>
            <div class="col-lg-4">
                @component('includes.components.table',['class'=>'table  table-bordered fixed_headers','style'=> 'font-size:12px','id'=>''])
                    @slot('thead')
                        <th style="font-size: 14px">Brand</th>
                        <th class="text-right" style="font-size: 14px">Opening Amount</th>
                    @endslot

                    @slot('tbody')
                        @php
                            $dt = $module_name_singular->details;
                        @endphp
                        <tr class="bg-gray-darker text-white" style="font-weight: bold">
                            <td>
                                Total
                            </td>
                            <td class="text-right ">{{ number_format(($dt->sum('amount')),2) }}</td>

                        </tr>
                        @foreach($products as $t)
                            <tr class="bg-gray text-navy" style="font-weight: bold">
                            <tr class="bg-gray text-navy" style="font-weight: bold">
                                <td>
                                    {{$t["brand_name"]}}
                                </td>
                                <td class="text-right ">{{ number_format(($dt->where('brand_id',$t["brand_id"])->sum('amount')),2) }}</td>
                            </tr>
                            @foreach($t["all"] as $product)
                                @php
                                    $details = $module_name_singular->details->where('product_id',$product->id)->first();
                                     $dtSumAmount  = 0;

                                     if($details){
                                         $dtSumAmount = $details->amount ;
                                     }
                                @endphp

                                <tr>
                                    <td width="150px"> {{ $product->name }}</td>
                                    <td width="150px" class="text-right">
                                        {{ Form::number("product[".$product->id."][amount]", $dtSumAmount, ['class' => 'form-control1 text-right',
                                'min' => 0 , 'step' => 'any'])}}
                                    </td>

                                </tr>
                            @endforeach

                        @endforeach


                    @endslot
                @endcomponent

                <br>

            </div>
            <div class="col-md-4"></div>
        </div>

        <div class="row">
            <div class="col-lg-6">
                {!! Form::button(('<i class="fa fa-check fa-fw"></i>&nbsp;'.$module_action), ['class' => 'btn btn-success pull-right','type'=>'submit']) !!}
            </div>

            <div class="col-lg-6">
                {!! link_to(route($module_route.'.index'), ('<i class="fa fa-mail-reply"></i>&nbsp;'.trans('buttons.general.cancel')), ['class' => 'btn btn-danger pull-left'], null, false) !!}
            </div><!--col-lg-1-->
        </div><!--form control-->
        <br><br>
        {{ Form::close() }}

    @endcomponent
@endsection