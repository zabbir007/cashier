@extends ('backend.layouts.app')

@section ('title', $page_heading)

@section('page-header')
    <h1>
        {{ $page_heading }}
    </h1>
@endsection

@section('content')
    @component('includes.components.widget')
        @slot('title')
            {{ $page_heading }}
        @endslot

        @slot('action_buttons')
            @include($module_view.'.header-buttons')
        @endslot

        {{ Form::model($module_name_singular, ['route' => [$module_route.'.update', $module_name_singular], 'class' => 'form-horizontal', 'role' => 'form', 'method' => 'PATCH']) }}

        <div class="row">
            <div class="col-sm-6">
                <div class="form-group row has-feedback">
                    {!! Form::label('code', 'Code',['class' => 'col-lg-4 col-form-label']) !!}
                    <div class="col-lg-8">
                        {!! Form::text('code', old('code') , ['class' => 'form-control', 'placeholder' => 'Code','required']) !!}
                        <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
                        <div class="help-block with-errors"></div>
                    </div>
                </div>

                <div class="form-group row has-feedback">
                    {!! Form::label('name', 'Name',['class' => 'col-lg-4 col-form-label']) !!}
                    <div class="col-lg-8">
                        {!! Form::text('name', old('name') , ['class' => 'form-control', 'placeholder' => 'Name', 'required']) !!}
                        <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
                        <div class="help-block with-errors"></div>
                    </div>
                </div>

                <div class="form-group row has-feedback">
                    {!! Form::label('short_name', 'Short Name',['class' => 'col-lg-4 col-form-label']) !!}
                    <div class="col-lg-8">
                        {!! Form::text('short_name', old('short_name') , ['class' => 'form-control', 'placeholder' => 'Short Name', 'required']) !!}
                        <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
                        <div class="help-block with-errors"></div>
                    </div>
                </div>


            </div>

            <div class="col-sm-6">

                <div class="form-group row has-feedback">
                    {!! Form::label('brand_id', 'Brand Name',['class' => 'col-lg-4 col-form-label']) !!}
                    <div class="col-lg-8">
                        {!! Form::select('brand_id', $brands, old('brand_id') , ['class' => 'form-control']) !!}
                        <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
                        <div class="help-block with-errors"></div>
                    </div>
                </div>

                <div class="form-group row has-feedback">
                    {!! Form::label('remarks', 'Remarks',['class' => 'col-lg-4 col-form-label']) !!}
                    <div class="col-lg-8">
                        {!! Form::text('remarks', old('remarks') , ['class' => 'form-control']) !!}
                        <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
                        <div class="help-block with-errors"></div>
                    </div>
                </div>
                <div class="form-group row has-feedback">
                    {!! Form::label('order', 'Order',['class' => 'col-lg-4 col-form-label']) !!}
                    <div class="col-lg-8">
                        {!! Form::text('order', old('order') , ['class' => 'form-control']) !!}
                        <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
                        <div class="help-block with-errors"></div>
                    </div>
                </div>
            </div>
            <div class="col-lg-12">
                <div class="form-group row has-feedback">
                    {!! Form::label('branch_id', 'Branch List',['class' => 'col-lg-2 col-form-label']) !!}
                    <div class="col-lg-8">
                        {!! Form::select('branch_list[]', $branches, $module_name_singular->branches->pluck('id'), ['class' => 'form-control','id'=>'select', 'multiple']) !!}
                        <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
                        <div class="help-block with-errors"></div>
                    </div>
                    <div class="col-lg-2">
                        <input type="checkbox" id="checkbox"> Select All
                    </div>
                </div>
            </div>

        </div>
        <br><br>

        <div class="row">
            <div class="col-lg-6">
                {!! Form::button(('<i class="fa fa-check fa-fw"></i>&nbsp;'.$module_action), ['class' => 'btn btn-success pull-right','type'=>'submit']) !!}
            </div>

            <div class="col-lg-6">
                {!! link_to(route($module_route.'.index'), ('<i class="fa fa-mail-reply"></i>&nbsp;'.trans('buttons.general.cancel')), ['class' => 'btn btn-danger pull-left'], null, false) !!}
            </div><!--col-lg-1-->
        </div><!--form control-->
        <br><br>
        {{ Form::close() }}

    @endcomponent
@endsection

@section('after-scripts')
    <script type="text/javascript">
        $("#checkbox").click(function () {
            if ($("#checkbox").is(':checked')) {
                $("select > option").prop("selected", true);
                $("select").trigger("change");
            } else {
                $("select > option").prop("selected", false);
                $("select").trigger("change");
            }
        });

        $("#button").click(function () {
            alert($("select").val());
        });
    </script>
@endsection