<div class="row">
    <div class="col-sm-6">
        <div class="form-group row has-feedback">
            {!! Form::label('code', 'Branch Code',['class' => 'col-lg-4 col-form-label']) !!}
            <div class="col-lg-8">
                {!! Form::text('code', old('code') , ['class' => 'form-control', 'placeholder' => 'Branch Code','required']) !!}
                <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
                <div class="help-block with-errors"></div>
            </div>
        </div>

        <div class="form-group row has-feedback">
            {!! Form::label('name', 'Branch Name',['class' => 'col-lg-4 col-form-label']) !!}
            <div class="col-lg-8">
                {!! Form::text('name', old('name') , ['class' => 'form-control', 'placeholder' => 'Branch Name', 'required']) !!}
                <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
                <div class="help-block with-errors"></div>
            </div>
        </div>

        <div class="form-group row has-feedback">
            {!! Form::label('address1', 'Branch Address 1',['class' => 'col-lg-4 col-form-label']) !!}
            <div class="col-lg-8">
                {!! Form::text('address1', old('address1') , ['class' => 'form-control', 'placeholder' => 'Branch Address', 'required']) !!}
                <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
                <div class="help-block with-errors"></div>
            </div>
        </div>

        <div class="form-group row has-feedback">
            {!! Form::label('address2', 'Branch Address 2',['class' => 'col-lg-4 col-form-label']) !!}
            <div class="col-lg-8">
                {!! Form::text('address2', old('address2') , ['class' => 'form-control', 'placeholder' => 'Branch Address']) !!}
                <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
                <div class="help-block with-errors"></div>
            </div>
        </div>
    </div>
    <div class="col-sm-6">
        <div class="form-group row has-feedback">
            {!! Form::label('address3', 'Branch Address 3',['class' => 'col-lg-4 col-form-label']) !!}
            <div class="col-lg-8">
                {!! Form::text('address3', old('address3') , ['class' => 'form-control', 'placeholder' => 'Branch Address']) !!}
                <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
                <div class="help-block with-errors"></div>
            </div>
        </div>

        <div class="form-group row has-feedback">
            {!! Form::label('address4', 'Branch Address 4',['class' => 'col-lg-4 col-form-label']) !!}
            <div class="col-lg-8">
                {!! Form::text('address4', old('address4') , ['class' => 'form-control', 'placeholder' => 'Branch Address']) !!}
                <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
                <div class="help-block with-errors"></div>
            </div>
        </div>

        <div class="form-group row has-feedback">
            {!! Form::label('location', 'Location',['class' => 'col-lg-4 col-form-label']) !!}
            <div class="col-lg-8">
                {!! Form::text('location', old('location') , ['class' => 'form-control', 'placeholder' => 'Location']) !!}
                <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
                <div class="help-block with-errors"></div>
            </div>
        </div>

        <div class="form-group row has-feedback">
            {!! Form::label('email', 'Email',['class' => 'col-lg-4 col-form-label']) !!}
            <div class="col-lg-8">
                {!! Form::text('email', old('email') , ['class' => 'form-control', 'placeholder' => 'Email', 'required']) !!}
                <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
                <div class="help-block with-errors"></div>
            </div>
        </div>
    </div>
    <div class="col-sm-12">
        <div class="form-group row has-feedback">
            {!! Form::label('Bank', 'Bank Lists',['class' => 'col-lg-2 col-form-label']) !!}
            <div class="col-lg-8">
                {!! Form::select('bank_list[]', $banks, null, ['class' => 'form-control','id'=>'select', 'multiple']) !!}
                <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
                <div class="help-block with-errors"></div>
            </div>
            <div class="col-lg-2">
                <input type="checkbox" id="checkbox"> Select All
            </div>
        </div>

    </div>

</div>




