<div class="btn-group float-right">
    <a href="{{ route($module_route.'.index') }}" class="btn  btn-alt-primary btn-sm" data-toggle="click-ripple"><i
                class="si si-grid"></i>&nbsp;All {{ title_case($module_name) }}</a>
    <a href="{{ route($module_route.'.create') }}" class="btn  btn-alt-success btn-sm" data-toggle="click-ripple"><i
                class="si si-plus"></i>&nbsp;Add New</a>
</div><!--pull right-->
