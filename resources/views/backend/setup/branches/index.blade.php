@extends ('backend.layouts.app')

@section ('title', $page_heading)

@section('after-styles')
    {{ Html::style("https://cdn.datatables.net/v/bs/dt-1.10.15/datatables.min.css") }}
@endsection

@section('content')
    <div class="row">
        <!-- DOM dataTable -->
        <div class="col-md-12">
            @component('includes.components.widget')

                @slot('title')
                    {{ $page_heading }}
                @endslot

                @slot('action_buttons')
                    @include($module_view.'.header-buttons')
                @endslot

                {{--Widget body start--}}
                @component('includes.components.table',['class'=>'table table-condensed table-hover table-striped table-bordered','id'=>''])
                    @slot('thead')
                        <th>SN</th>
                        <th>Name</th>
                        <th>Address</th>
                        <th>Banks</th>
                        <th>{{ __('labels.general.actions') }}</th>
                    @endslot

                    @slot('tbody')
                        @foreach($$module_name as $module_name)

                            <tr>
                            <tr>
                                <td class="text-center">{{ $loop->iteration }}</td>
                                <td> {{ $module_name->name }}</td>
                                <td> {{ $module_name->address1 }} {{ $module_name->address2 }} {{ $module_name->address3 }} {{ $module_name->address4 }}</td>
                                <td>
                                    @foreach($module_name->banks as $bank)
                                        <span class="badge badge-info">{{$bank->short_name}}</span>
                                    @endforeach
                                </td>
                                <td> {!! $module_name->action_buttons !!}</td>
                            </tr>
                            </tr>
                        @endforeach
                    @endslot
                @endcomponent

            @endcomponent
        </div><!-- END column -->
    </div><!-- .row -->
@endsection

@section('after-scripts')

@endsection