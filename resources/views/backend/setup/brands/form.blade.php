<div class="row">
    <div class="col-sm-6">
        <div class="form-group row has-feedback">
            {!! Form::label('code', 'Code',['class' => 'col-lg-4 col-form-label']) !!}
            <div class="col-lg-8">
                {!! Form::text('code', old('code') , ['class' => 'form-control', 'placeholder' => 'Code','required']) !!}
                <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
                <div class="help-block with-errors"></div>
            </div>
        </div>

        <div class="form-group row has-feedback">
            {!! Form::label('name', 'Name',['class' => 'col-lg-4 col-form-label']) !!}
            <div class="col-lg-8">
                {!! Form::text('name', old('name') , ['class' => 'form-control', 'placeholder' => 'Name', 'required']) !!}
                <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
                <div class="help-block with-errors"></div>
            </div>
        </div>

    </div>
    <div class="col-sm-6">

        <div class="form-group row has-feedback">
            {!! Form::label('remarks', 'Remarks',['class' => 'col-lg-4 col-form-label']) !!}
            <div class="col-lg-8">
                {!! Form::text('remarks', old('remarks') , ['class' => 'form-control']) !!}
                <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
                <div class="help-block with-errors"></div>
            </div>
        </div>

        <div class="form-group row has-feedback">
            {!! Form::label('order', 'Order',['class' => 'col-lg-4 col-form-label']) !!}
            <div class="col-lg-8">
                {!! Form::text('order', old('order') , ['class' => 'form-control']) !!}
                <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
                <div class="help-block with-errors"></div>
            </div>
        </div>
    </div>

</div>

<br> <br>


