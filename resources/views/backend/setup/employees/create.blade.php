@extends ('backend.layouts.app')

@section ('title',$page_heading)

@section('after-styles')
    <style type="text/css">
        .input-group-addon {
            padding: 3px 5px !important;
        }

        .required {
            content: "*";
            position: absolute;
            top: 11px;
            right: 3px;
            font-size: 15px;
            color: red;
            font-weight: 700;
        }

        .input-group .required {
            right: -9px;
        }
    </style>
@endsection

@section('content')
    @component('includes.components.widget')
        @slot('title')
            {{ $page_heading }}
        @endslot

        @slot('action_buttons')
            @include($module_view.'.header-buttons')
        @endslot

        {{ Form::open(['route' => $module_route.'.store','files' => true, 'class' => 'form-horizontal', 'role' => 'form', 'method' => 'post']) }}

        <div class="row">
            <div class="col-sm-12">
                <div class="pull-right">
                    <i class="text-mute"><span style="color: red;font-weight: 700">*</span> are mandatory fields!</i>
                </div>
            </div>
        </div>

        @include($module_view.'.form')

        <div class="row">
            <div class="col-lg-6">
                {!! Form::button(('<i class="fa fa-check fa-fw"></i>&nbsp;'.$module_action), ['class' => 'btn btn-success pull-right','type'=>'submit']) !!}
            </div>

            <div class="col-lg-6">
                {!! link_to(route($module_route.'.index'), ('<i class="fa fa-mail-reply"></i>&nbsp;'.trans('buttons.general.cancel')), ['class' => 'btn btn-danger pull-left'], null, false) !!}
            </div><!--col-lg-1-->
        </div><!--form control-->

        <br>
        {{ Form::close() }}

    @endcomponent
@endsection

@section('after-scripts')

    <script type="text/javascript">
        $(document).ready(function () {
            //add required

            $("input[required='required'], select").each(function () {
                $(this).before('<span class="required">*</span>');
            });

            var $pwdField = $("#pwdField");
            $pwdField.val($pwdField.data('value'));
            $("#pwdChk").on('click', function (e) {
                if (!$(this).prop('checked')) {
                    $pwdField.removeAttr('readonly');
                    $pwdField.val('');
                    $pwdField.focus();
                } else {
                    $pwdField.prop('readonly', true);
                    $pwdField.val($pwdField.data('value'));
                }
            });
        });
    </script>

@endsection