<div class="row">
    <div class="col-sm-6">
        <div class="form-group row has-feedback">
            {!! Form::label('trans_date', 'Transaction Date',['class' => 'col-lg-4 col-form-label']) !!}
            <div class="col-lg-8">
                {{--{!! Form::text('trans_date',$carbon->format('d-M-Y'),  ['class' => 'form-control','readonly']) !!}--}}
                {!! Form::text('trans_date',$carbon->format('d-M-Y'),  ['class' => 'form-control datepicker','data-week-start'=>"6", 'data-autoclose'=>"true", 'data-today-highlight'=>"true", 'data-date-format'=>"dd-M-yyyy", 'placeholder'=>"mm/dd/yy",'readonly']) !!}
                <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
                <div class="help-block with-errors"></div>
            </div>
        </div>
    </div>

{{--    @if(count(access()->user()->branches)>1)--}}
{{--        <div class="col-sm-6">--}}
{{--            <div class="form-group row has-feedback">--}}
{{--                {!! Form::label('branch_name', 'Branch Name',['class' => 'col-lg-4 col-form-label']) !!}--}}
{{--                <div class="col-lg-8">--}}
{{--                    {!! Form::select('branch_id',$branches,old('branch_name'),  ['class' => 'form-control','readonly']) !!}--}}
{{--                    <span class="glyphicon form-control-feedback" aria-hidden="true"></span>--}}
{{--                    <div class="help-block with-errors"></div>--}}
{{--                </div>--}}
{{--            </div>--}}
{{--        </div>--}}
{{--    @else--}}
        <div class="col-sm-6">
            <div class="form-group row has-feedback">
                {!! Form::label('branch_name', 'Branch Name',['class' => 'col-lg-4 col-form-label']) !!}
                <div class="col-lg-8">
{{--                    {!! Form::hidden('branch_id',$branches->id,  ['class' => 'form-control','readonly']) !!}--}}
{{--                    {!! Form::text('branch_name',$branches->name,  ['class' => 'form-control','readonly']) !!}--}}
                    {!! Form::select('branch_id', $branches, null, ['class' => 'form-control','id'=>'branch','placeholder'=>'Select a branch']) !!}
                    <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
                    <div class="help-block with-errors"></div>
                </div>
            </div>
        </div>
{{--    @endif--}}

</div>
<div class="col-sm-6">

</div>

<br>
<div class="row">
    <div class="col-lg-7">
        @component('includes.components.table',['class'=>'table  table-bordered fixed_headers','style'=> 'font-size:12px','id'=>''])
            @slot('thead')
                <th style="font-size: 14px">Brand</th>
                <th style="font-size: 14px">Collection</th>
                <th style="font-size: 14px">Outstanding</th>
            @endslot

            @slot('tbody')
                {{--<tr class="bg-gray-darker text-white">--}}
                {{--<td style="font-weight: bold">--}}
                {{--Total--}}
                {{--</td>--}}
                {{--<td></td>--}}

                {{--</tr>--}}
                @foreach($products as $t)
                    <tr class="bg-gray text-navy" style="font-weight: bold">
                        <td>
                            {{$t["brand_name"]}}
                        </td>
                        <td></td>
                        <td></td>

                    </tr>
                    @foreach($t["all"] as $product)

                        <tr>
                            <td width="150px"> {{ $product->name }}</td>
                            <td width="150px">
                                {{ Form::number("product[".$product->id."][collection]", null, [
                                'class' => 'form-control1 text-right',
                                'placeholder' => '',
                                'min' => 0, 'step' => 'any'
                                ])
                               }}
                            </td>

                            <td width="150px">
                                {{ Form::number("product[".$product->id."][outstanding]", null, [
                                'class' => 'form-control1 text-right',
                                'placeholder' => '',
                                'min' => 0, 'step' => 'any'
                                ])
                               }}
                            </td>

                        </tr>
                    @endforeach
                @endforeach


            @endslot
        @endcomponent

        <br>

    </div>
    <div class="col-md-5">
        <div class="form-group row has-feedback">
            {!! Form::label('pc_cash_amount', 'Cash Amount (P/C)',['class' => 'col-lg-5 col-form-label']) !!}
            <div class="col-lg-7">
                {!! Form::number('pc_cash_amount',old('pc_cash_amount'),  ['class' => 'form-control text-right','min' => 0     , 'step' => 'any']) !!}
                <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
                <div class="help-block with-errors"></div>
            </div>
        </div>

        <div class="form-group row has-feedback">
            {!! Form::label('pc_bank_amount', 'Bank Amount (P/C)',['class' => 'col-lg-5 col-form-label']) !!}
            <div class="col-lg-7">
                {!! Form::number('pc_bank_amount',old('pc_bank_amount'),  ['class' => 'form-control text-right','min' => 0     , 'step' => 'any']) !!}
                <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
                <div class="help-block with-errors"></div>
            </div>
        </div>
    </div>
</div>
<br><br>
