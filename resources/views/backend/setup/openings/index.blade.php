@extends ('backend.layouts.app')

@section ('title', $page_heading)

@section('after-styles')
    {{ Html::style("https://cdn.datatables.net/v/bs/dt-1.10.15/datatables.min.css") }}
@endsection

@section('content')
    <div class="row">
        <!-- DOM dataTable -->
        <div class="col-md-12">
            @component('includes.components.widget')

                @slot('title')
                    {{ $page_heading }}
                @endslot

                @slot('action_buttons')
                    @include($module_view.'.header-buttons')
                @endslot

                {{--Widget body start--}}
                @component('includes.components.table',['class'=>'table table-condensed table-hover table-striped table-bordered','id'=>''])
                    @slot('thead')
                        <th class="text-left" style="font-size: 14px">SL</th>
                        <th class="text-center" style="font-size: 14px">Branch</th>
                        <th class="text-right" style="font-size: 14px">Collection</th>
                        <th class="text-right" style="font-size: 14px">Outstanding</th>
                        <th class="text-right" style="font-size: 14px">Cash Amount P/C</th>
                        <th class="text-right" style="font-size: 14px">Bank Amount P/C</th>


                        <th class="text-center">{{ __('labels.general.actions') }}</th>

                    @endslot

                    @slot('tbody')
                        @foreach($$module_name as $module_name)
                            @php
                                if($module_name->isApprove()){
                                   $bg = 'bg-success-light';
                                }else{
                                    $bg = '';
                                }
                            @endphp

                            <tr class= {{$bg}}>
                                <td class="text-left"
                                    style="font-weight: bold; color: #0d89ed">{{ $module_name->sl }}</td>
                                <td class="text-center"> {{ $module_name->branch_name }}</td>
                                @php
                                    $dtSumColl = $module_name->details->sum('collection');
                                    $dtSumOut = $module_name->details->sum('outstanding');
                                    $dtSumCash = $module_name->pc_cash_amount;
                                    $dtSumBank = $module_name->pc_bank_amount;
                                @endphp
                                <td class="text-right"> {{ number_format($dtSumColl,2) }}</td>
                                <td class="text-right"> {{ number_format($dtSumOut,2) }}</td>
                                <td class="text-right"> {{ number_format($dtSumCash,2) }}</td>
                                <td class="text-right"> {{ number_format($dtSumBank,2)}}</td>

                                <td> {!! $module_name->action_buttons !!}</td>

                            </tr>

                        @endforeach
                    @endslot
                @endcomponent

            @endcomponent
        </div><!-- END column -->
    </div><!-- .row -->
@endsection

@section('after-scripts')

@endsection