@extends ('backend.layouts.app')

@section ('title', $page_heading)

@section('page-header')
    <h1>
        {{ $page_heading }}
    </h1>
@endsection

@section('content')
    @component('includes.components.widget')
        @slot('title')
            {{ $page_heading }}
        @endslot

        @slot('action_buttons')
            @include($module_view.'.header-buttons')
        @endslot

        {{ Form::model($$module_name_singular, ['route' => [$module_route.'.update', $$module_name_singular], 'class' => 'form-horizontal', 'role' => 'form', 'method' => 'PATCH']) }}
        <div class="row">
            <div class="col-sm-7">
                <div class="form-group row has-feedback">
                    {!! Form::label('branch', 'Branch',['class' => 'col-lg-4 col-form-label']) !!}
                    <div class="col-lg-8">
                        {!! Form::text('', $$module_name_singular->branch->name, ['class' => 'form-control','readonly']) !!}
                        <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
                        <div class="help-block with-errors"></div>
                    </div>
                </div>

                <div class="form-group row has-feedback">
                    {!! Form::label('bank', 'Bank',['class' => 'col-lg-4 col-form-label']) !!}
                    <div class="col-lg-8">
                        {!! Form::text('', $$module_name_singular->bank->name, ['class' => 'form-control','readonly']) !!}
                        <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
                        <div class="help-block with-errors"></div>
                    </div>
                </div>

                <div class="form-group row has-feedback">
                    {!! Form::label('account_number', 'Account Number',['class' => 'col-lg-4 col-form-label']) !!}
                    <div class="col-lg-8">
                        {!! Form::text('account_number', null , ['class' => 'form-control','required']) !!}
                        <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
                        <div class="help-block with-errors"></div>
                    </div>
                </div>
            </div>
        </div>

        <br> <br>


        <div class="row">
            <div class="col-lg-6">
                {!! Form::button(('<i class="fa fa-check fa-fw"></i>&nbsp;'.$module_action), ['class' => 'btn btn-success pull-right','type'=>'submit']) !!}
            </div>

            <div class="col-lg-6">
                {!! link_to(route($module_route.'.index'), ('<i class="fa fa-mail-reply"></i>&nbsp;'.trans('buttons.general.cancel')), ['class' => 'btn btn-danger pull-left'], null, false) !!}
            </div><!--col-lg-1-->
        </div><!--form control-->
        <br><br>
        {{ Form::close() }}

    @endcomponent
@endsection

@section('after-scripts')
    <script type="text/javascript">
        new Vue({
            el: '#app',
            mounted: function () {
                let $select2 = {theme: "classic", width: '100%'};
                $("#loader").hide();
                $("#submitBtn").prop("disabled", true);
                $("#banks").prop("disabled", true);

                $("#branch").select2($select2).on("change", (e) => {
                    this.$getBanks($(e.currentTarget).val());
                });
            },
            methods: {
                $getBanks: function (branchId) {
                    $("#loader").show();
                    $("#submitBtn").prop("disabled", true);
                    $("#banks").prop("disabled", true);

                    let config = {
                        //  headers: {'Authorization': 'JWT ' + this.$store.state.token},
                        params: {
                            branch: branchId,
                        }
                    };

                    axios.get('{{ url()->current() }}', config)
                        .then((response) => {
                            $("#loader").hide();
                            $("#submitBtn").prop("disabled", false);
                            let data = response.data.map((obj) => {
                                obj.id = obj.id || obj.pk;
                                obj.text = obj.name;
                                return obj;
                            });

                            this.$populateBanks(data);
                        }, (error) => {
                            $("#loader").hide();
                            $("#submitBtn").prop("disabled", true);
                            console.log(error);
                        })
                },

                $populateBanks: function (data) {
                    let div = $("#banks");
                    div.prop("disabled", false);
                    div.select2().empty();
                    div.select2({
                        data: data,
                        width: '100%'
                    }).trigger("change");
                }
            }
        });
    </script>
@endsection