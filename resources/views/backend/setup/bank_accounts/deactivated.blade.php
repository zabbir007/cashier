@extends ('backend.layouts.app')

@section ('title', $page_heading)

@section('page-header')
    <h1>
        {{ $page_heading }}
    </h1>
@endsection

@section('content')
    <div class="row">
        <!-- DOM dataTable -->
        <div class="col-md-12">
            @component('includes.components.widget')

                @slot('title')
                    {{ $page_heading }}
                @endslot

                @slot('action_buttons')
                    @include($module_view.'.header-buttons')
                @endslot

                {{--Widget body start--}}
                @component('includes.components.table',['class'=>'table table-condensed table-hover table-striped table-bordered','id'=>''])
                    @slot('thead')
                        <th>&nbsp;SN</th>
                        <th>&nbsp;Branch</th>
                        <th>&nbsp;Bank</th>
                        <th>&nbsp;Account Number</th>
                        <th>&nbsp;Status</th>
                        <th>{{ __('labels.general.actions') }}</th>
                    @endslot

                    @slot('tbody')
                        @foreach($$module_name as $module_name)
                            <tr>
                                <td class="text-center">&nbsp;{{ $loop->iteration }}</td>
                                <td>&nbsp;{{ $module_name->branch!== null ? $module_name->branch->name : '' }}</td>
                                <td>&nbsp;{{ $module_name->bank!== null ? $module_name->bank->name : '' }}</td>
                                <td>&nbsp;{{ $module_name->account_number }}</td>
                                <td>&nbsp;{!! $module_name->status_label !!}</td>
                                <td>&nbsp;{!! $module_name->action_buttons !!}</td>
                            </tr>
                        @endforeach
                    @endslot
                @endcomponent

            @endcomponent
        </div><!-- END column -->
    </div><!-- .row -->
@endsection