@extends ('backend.layouts.app')

@section ('title',$page_heading)

@section('after-styles')
    <style type="text/css">
        .input-group-addon {
            padding: 3px 5px !important;
        }

        .required {
            content: "*";
            position: absolute;
            top: 11px;
            right: 3px;
            font-size: 15px;
            color: red;
            font-weight: 700;
        }

        .input-group .required {
            right: -9px;
        }
    </style>
@endsection

@section('content')
    @component('includes.components.widget')
        @slot('title')
            {{ $page_heading }}
        @endslot

        @slot('action_buttons')
            @include($module_view.'.header-buttons')
        @endslot

        {{ Form::open(['route' => $module_route.'.store','class' => 'form-horizontal', 'role' => 'form', 'method' => 'post']) }}

        <div class="row">
            <div class="col-sm-12">
                <div class="pull-right">
                    <i class="text-mute"><span style="color: red;font-weight: 700">*</span> are mandatory fields!</i>
                </div>
            </div>
        </div>

        @include($module_view.'.form')

        <div class="row">
            <div class="col-lg-6">
                {!! Form::button(('<i class="fa fa-check fa-fw"></i>&nbsp;'.$module_action), ['class' => 'btn btn-success pull-right','type'=>'submit','id'=>'submitBtn']) !!}
            </div>

            <div class="col-lg-6">
                {!! link_to(route($module_route.'.index'), ('<i class="fa fa-mail-reply"></i>&nbsp;'.trans('buttons.general.cancel')), ['class' => 'btn btn-danger pull-left'], null, false) !!}
            </div><!--col-lg-1-->
        </div><!--form control-->

        <br>
        {{ Form::close() }}

    @endcomponent
@endsection

@section('after-scripts')
    <script type="text/javascript">
        new Vue({
            el: '#app',
            mounted: function () {
                let $select2 = {theme: "classic", width: '100%'};
                $("#loader").hide();
                $("#submitBtn").prop("disabled", true);
                $("#banks").prop("disabled", true);

                $("#branch").select2($select2).on("change", (e) => {
                    this.$getBanks($(e.currentTarget).val());
                });
            },
            methods: {
                $getBanks: function (branchId) {
                    $("#loader").show();
                    $("#submitBtn").prop("disabled", true);
                    $("#banks").prop("disabled", true);

                    let config = {
                        //  headers: {'Authorization': 'JWT ' + this.$store.state.token},
                        params: {
                            branch: branchId,
                        }
                    };

                    axios.get('{{ url()->current() }}', config)
                        .then((response) => {
                            $("#loader").hide();
                            $("#submitBtn").prop("disabled", false);
                            let data = response.data.map((obj) => {
                                obj.id = obj.id || obj.pk;
                                obj.text = obj.name;
                                return obj;
                            });

                            this.$populateBanks(data);
                        }, (error) => {
                            $("#loader").hide();
                            $("#submitBtn").prop("disabled", true);
                            console.log(error);
                        })
                },

                $populateBanks: function (data) {
                    let div = $("#banks");
                    div.prop("disabled", false);
                    div.select2().empty();
                    div.select2({
                        data: data,
                        width: '100%'
                    }).trigger("change");
                }
            }
        });
    </script>
@endsection