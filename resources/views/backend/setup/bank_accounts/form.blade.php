<div class="row">
    <div class="col-sm-7">
        <div class="form-group row has-feedback">
            {!! Form::label('branch', 'Branch',['class' => 'col-lg-4 col-form-label']) !!}
            <div class="col-lg-8">
                {!! Form::select('branch_id', $branches, null, ['class' => 'form-control','id'=>'branch','placeholder'=>'Select a branch']) !!}
                <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
                <div class="help-block with-errors"></div>
            </div>
        </div>

        <div class="form-group row has-feedback">
            {!! Form::label('bank', 'Bank',['class' => 'col-lg-4 col-form-label']) !!}
            <div class="col-lg-7">
                <label for="banks" style="display: none;">Banks</label>
                <select class="form-control" id="banks" name="bank_id">
                    <option selected="selected" value="">Select a bank</option>
                </select>
                <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
                <div class="help-block with-errors"></div>
            </div>
            <div class="col-lg-1" id="loader">
                <i class="fa fa-fw fa-spinner fa-spin fa-2x"></i>
            </div>
        </div>

        <div class="form-group row has-feedback">
            {!! Form::label('account_number', 'Account Number',['class' => 'col-lg-4 col-form-label']) !!}
            <div class="col-lg-8">
                {!! Form::text('account_number', null , ['class' => 'form-control','required']) !!}
                <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
                <div class="help-block with-errors"></div>
            </div>
        </div>
    </div>
</div>

<br> <br>


