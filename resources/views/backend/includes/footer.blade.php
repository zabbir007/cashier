<!-- Footer -->
<hr class="m-0">
<footer id="page-footer" class="opacity-0">
    <div class="content py-20 font-size-xs clearfix">
        <div class="float-right">
            Developed by <span class="font-w600"><strong>ISA - ERP [Web Team]</strong> - Transcom Ltd</span>
        </div>
        <div class="float-left">
            <span class="font-w600"> &copy; <span class="js-year-copy">{{ date('Y') }}</span>,  {{ app_name() }} <small>v{{ config('application.version') }}</small></span>
        </div>
    </div>
</footer>
<!-- END Footer -->
