<!-- Side User -->
<div class="content-side content-side-full content-side-user px-10 align-parent">
    <!-- Visible only in mini mode -->
    <div class="sidebar-mini-visible-b align-v animated fadeIn">
        {{--<img class="img-avatar img-avatar32" src="{{ asset('assets/img/avatars/avatar15.jpg') }}" alt="" />--}}
        <i class="fa fa-fw fa-user-circle-o fa-5x"></i>
    </div>
    <!-- END Visible only in mini mode -->

    <!-- Visible only in normal mode -->
    <div class="sidebar-mini-hidden-b text-center">
        <ul class="list-inline mt-10">
            <li class="list-inline-item">
                <a class="link-effect text-dual-primary-dark font-size-xs font-w600 text-uppercase"
                   href="{{ route('admin.dashboard') }}">{{ $logged_in_user->full_name }}</a>
            </li>
        </ul>

        <!-- END Side User -->
        <div class="content-side-full text-center mt-5">
            @if($logged_in_user->branches->count() <= 30)
                @foreach($logged_in_user->branches as $branch )
                    <span class="badge badge-success text-black">{{$branch->name}}</span>
                @endforeach
            @else <span class="badge badge-primary">ALL</span>
            @endif
        </div>
    </div>
    <!-- END Visible only in normal mode -->
</div>
