<li class="{{ active_class(if_route_pattern('admin.transaction.*'), 'open') }}">
    <a class="nav-submenu {{ active_class(if_route_pattern('admin.transaction.*'), 'open') }}"
       data-toggle="nav-submenu" href="#"><i class="si si-calculator"></i><span
                class="sidebar-mini-hide">Transactions</span></a>
    <ul>
        @permission('menu-transaction-date-wise-receipt-index')
        <li>
            <a class="{{ active_class(if_route_pattern('admin.transaction.datewises.*')) }}"
               href="{{ route('admin.transaction.datewises.index') }}">
                <span class="sidebar-mini-hide">{{ trans('Date Wise Receipt') }}</span>
            </a>
        </li>
        @endpermission
        @permission('menu-transaction-bank-charge-index')
        <li>
            <a class="{{ active_class(if_route_pattern('admin.transaction.bankcharges.*')) }}"
               href="{{ route('admin.transaction.bankcharges.index') }}">
                <span class="sidebar-mini-hide">{{ trans('Bank Charges') }}</span>
            </a>
        </li>
        @endpermission
        @permission('menu-transaction-petty-cash-index')
        <li>
            <a class="{{ active_class(if_route_pattern('admin.transaction.pettycashes.*')) }}"
               href="{{ route('admin.transaction.pettycashes.index') }}">
                <span class="sidebar-mini-hide">{{ trans('Petty Cash') }}</span>
            </a>
        </li>
        @endpermission
        @permission('menu-transaction-deposits-index')
        <li>
            <a class="{{ active_class(if_route_pattern('admin.ransaction.deposits.*')) }}"
               href="{{ route('admin.transaction.deposits.index') }}">
                <span class="sidebar-mini-hide">{{ trans('Deposits') }}</span>
            </a>
        </li>
        @endpermission
        @permission('menu-transaction-outstanding-reconciliation-index')
        <li>
            <a class="{{ active_class(if_route_pattern('admin.transaction.outstandingreconciliations.*')) }}"
               href="{{ route('admin.transaction.outstandingreconciliations.index') }}">
                <span class="sidebar-mini-hide">{{ trans('Outstanding Reconciliation') }}</span>
            </a>
        </li>
        @endpermission

        @permission('menu-corporate-transaction')
        <li>
            <a class="{{ active_class(if_route_pattern('admin.transaction.imprest_corporate_reconciliation.*')) }}"
               href="{{ route('admin.transaction.imprest_corporate_reconciliations.index') }}">
                <span class="sidebar-mini-hide">Imprest Corporate Reconciliation</span>
            </a>
        </li>
        @endpermission
        @permission('menu-transaction')
        <li>
            <a class="{{ active_class(if_route_pattern('admin.transaction.imprest_bank_reconciliation.*')) }}"
               href="{{ route('admin.transaction.imprest_bank_reconciliations.index') }}">
                <span class="sidebar-mini-hide">Imprest Bank Reconciliation</span>
            </a>
        </li>
        @endpermission




    </ul>
</li>
