<li class="{{ active_class(if_route_pattern('admin.dashboard.*'),'open') }}">
    <a class="{{ active_class(if_route_pattern('admin.dashboard.*')) }}" href="{{ route('admin.dashboard') }}">
        <i class="si si-compass"></i><span class="sidebar-mini-hide">Dashboard</span>
    </a>
</li>
