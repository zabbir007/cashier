<li class="{{ active_class(if_route_pattern('admin.setup.*'), 'open') }}">
    <a class="nav-submenu {{ active_class(if_route_pattern('admin.setup.*'), 'open') }}"
       data-toggle="nav-submenu" href="#"><i class="si si-settings"></i><span
                class="sidebar-mini-hide">Master Setup</span></a>
    <ul>

        <li>
            <a class="{{ active_class(if_route_pattern('admin.setup.branches.*')) }}"
               href="{{ route('admin.setup.branches.index') }}">
                <span class="sidebar-mini-hide">Branches</span>
            </a>
        </li>

        <li>
            <a class="{{ active_class(if_route_pattern('admin.setup.banks.*')) }}"
               href="{{ route('admin.setup.banks.index') }}">
                <span class="sidebar-mini-hide">Banks</span>
            </a>
        </li>

        <li>
{{--            <a class="{{ active_class(if_route_pattern('admin.setup.banks.*')) }}"--}}
{{--               href="{{ route('admin.setup.drivers.index') }}">--}}
{{--                <span class="sidebar-mini-hide">Driver</span>--}}
{{--            </a>--}}
        </li>

        <li>
            <a class="{{ active_class(if_route_pattern('admin.setup.bank_accounts.*')) }}"
               href="{{ route('admin.setup.bank_accounts.index') }}">
                <span class="sidebar-mini-hide">Bank Accounts</span>
            </a>
        </li>

        <li>
            <a class="{{ active_class(if_route_pattern('admin.setup.employees.*')) }}"
               href="{{ route('admin.setup.employees.index') }}">
                <span class="sidebar-mini-hide">Employee</span>
            </a>
        </li>

        <li class="info">
            <a class="{{ active_class(if_route_pattern('admin.setup.brands.*')) }}"
               href="{{ route('admin.setup.brands.index') }}">
                <span class="sidebar-mini-hide">Segment</span>
            </a>
        </li>

        <li class="info">
            <a class="{{ active_class(if_route_pattern('admin.setup.products.*')) }}"
               href="{{ route('admin.setup.products.index') }}">
                <span class="sidebar-mini-hide">Brand</span>
            </a>
        </li>
        <li class="info">
            <a class="{{ active_class(if_route_pattern('admin.setup.openings.*')) }}"
               href="{{ route('admin.setup.openings.index') }}">
                <span class="sidebar-mini-hide">Opening Amount</span>
            </a>
        </li>
        <li class="info">
            <a class="{{ active_class(if_route_pattern('admin.setup.openings.*')) }}"
               href="{{ route('admin.setup.corporates.index') }}">
                <span class="sidebar-mini-hide">Corporate Opening Amount</span>
            </a>
        </li>
        {{--<li class="info">--}}
        {{--<a class="{{ active_class(if_route_pattern('admin/setup/closings*')) }}" href="{{ route('admin.setup.closings.index') }}">--}}
        {{--{{ trans('Closing Amount') }}--}}
        {{--</a>--}}
        {{--</li>--}}

    </ul>
</li>
