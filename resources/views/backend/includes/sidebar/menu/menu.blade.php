<!-- Side Navigation -->
<div class="content-side content-side-full">
    <ul class="nav-main">
        @include('backend.includes.sidebar.menu.dashboard')
        @permission('menu-access-setup')
        @include('backend.includes.sidebar.menu.access_management')
        @endauth

        @permission('menu-system-setup')
        @include('backend.includes.sidebar.menu.system_management')
        @endauth

        @permission('menu-master-setup')
        @include('backend.includes.sidebar.menu.setup')
        @endauth

        @permission('menu-transaction')
        @include('backend.includes.sidebar.menu.transaction')
        @endauth


{{--        @permission('menu-corporate-transaction')--}}
{{--        @include('backend.includes.sidebar.menu.corporateTransaction')--}}
{{--        @endauth--}}

        @permission('menu-reports')
        @include('backend.includes.sidebar.menu.report')
        @endauth
    </ul>
</div>
<!-- END Side Navigation -->
