{{--<li class="nav-main-heading"><span class="sidebar-mini-visible">SM</span><span class="sidebar-mini-hidden">System Management</span></li>--}}
<li class="{{ active_class(if_route_pattern('admin.system.*') || if_route_pattern('log-viewer::.*'), 'open') }}">
    <a class="nav-submenu {{ active_class(if_route_pattern('admin.system.*'), 'open') }}"
       data-toggle="nav-submenu" href="#">
        <i class="si si-shield"></i><span class="sidebar-mini-hide">System Management</span></a>
    <ul>
        @permission('access-management-log-viewer')
        <li class="{{ active_class(if_route('admin.system.activity_log.index')) }}">
            <a href="{{ route('admin.system.activity_log.index') }}">Activity Logs</a>
        </li>
        @endpermission

        @permission('access-management-log-viewer')
        <li class="{{ active_class(if_route_pattern('log-viewer::*')) }}">
            <a class="nav-submenu {{ active_class(if_route_pattern('log-viewer::*'), 'open') }}"
               data-toggle="nav-submenu" href="#">Log Viewer</a>
            <ul>
                <li class="{{ active_class(if_route('log-viewer::dashboard')) }}">
                    <a href="{{ route('log-viewer::dashboard') }}">Dashboard</a>
                </li>
                <li class="{{ active_class(if_route('log-viewer::logs.list')) }}">
                    <a href="{{ route('log-viewer::logs.list') }}">Log</a>
                </li>
            </ul>
        </li>
        @endpermission

        @permission('system-manage-transaction-settings')
        <li>
            <a class="{{ active_class(if_route_pattern('admin.system.transaction_settings.*')) }}"
               href="{{ route('admin.system.transaction_settings.index') }}">
                <span class="sidebar-mini-hide">Transaction Settings</span>
            </a>
        </li>
        @endpermission

        @permission('system-manage-transaction-settings')
        <li>
            <a class="{{ active_class(if_route_pattern('admin.system.logged_users.*')) }}"
               href="{{ route('admin.system.logged_users.index') }}">
                <span class="sidebar-mini-hide">Logged Users</span>
            </a>
        </li>
        @endpermission
    </ul>
</li>
