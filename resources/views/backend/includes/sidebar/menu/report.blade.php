<li class="{{ active_class(if_route_pattern('admin.reports.*'),'open') }}">
    <a class="{{ active_class(if_route_pattern('admin.reports.*')) }}" href="{{ route('admin.reports.index') }}">
        <i class="si si-book-open"></i><span class="sidebar-mini-hide">Reports</span>
    </a>
</li>
