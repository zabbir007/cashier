{{--<li class="nav-main-heading"><span class="sidebar-mini-visible">SM</span><span class="sidebar-mini-hidden">System Management</span></li>--}}
<li class="{{ active_class(if_route_pattern('admin.access.*'), 'open') }}">
    <a class="nav-submenu {{ active_class(if_route_pattern('admin.access.*'), 'open') }}"
       data-toggle="nav-submenu" href="#">
        <i class="si si-users"></i><span class="sidebar-mini-hide">Access Management</span></a>
    <ul>
        @permission('access-management-user-management')
        <li>
            <a class="{{ active_class(if_route_pattern('admin.access.user.*')) }}"
               href="{{ route('admin.access.user.index') }}">
                {{ trans('labels.backend.access.users.management') }}
            </a>
        </li>
        @endpermission
        @permission('access-management-role-management')
        <li>
            <a class="{{ active_class(if_route_pattern('admin.access.role.*')) }}"
               href="{{ route('admin.access.role.index') }}">
                {{ trans('labels.backend.access.roles.management') }}
            </a>
        </li>
        @endauth
        @permission('access-management-permission-management')
        <li>
            <a class="{{ active_class(if_route_pattern('admin.access.permission*')) }}"
               href="{{ route('admin.access.permission.index') }}">
                {{ trans('labels.backend.access.permissions.management') }}
            </a>
        </li>
        @endauth
    </ul>
</li>
