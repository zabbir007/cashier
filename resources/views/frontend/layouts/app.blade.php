<!doctype html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>@yield('title', app_name())</title>
    <meta name="description" content="@yield('meta_description', 'CASHier Branch Cash Manager')">
    <meta name="author" content="@yield('meta_author', 'Md. Aftab Uddin')">
@yield('meta')

<!-- Styles -->
@yield('before-styles')
<!-- Icons -->
    <!-- The following icons can be replaced with your own, they are used by desktop and mobile browsers -->
{{ Html::style(asset('img/favicon.ico'))}}
{{ Html::style('assets/img/favicons/favicon-192x192.png')}}
{{ Html::style('assets/img/favicons/apple-touch-icon-180x180.png')}}
<!-- END Icons -->

    <!-- Stylesheets -->
{{ Html::style(mix('css/frontend.css')) }}
@yield('after-styles')

</head>
<body>
<div id="page-container" class="main-content-boxed">
@include('includes.partials.logged-in-as')
<!-- Main Container -->
    <main id="main-container">
        <!-- Page Content -->
    @yield('content')
    <!-- END Page Content -->
    </main>
    <!-- END Main Container -->
</div>

<!-- Scripts -->
@yield('before-scripts')
{!! Html::script(mix('js/manifest.js')) !!}
{!! Html::script(mix('js/vendor.js')) !!}
{!! Html::script(mix('js/frontend.js')) !!}
@yield('after-scripts')

{{--@include('includes.partials.ga')--}}
</body>
</html>
