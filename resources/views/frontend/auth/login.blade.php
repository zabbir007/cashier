@extends('frontend.layouts.app')
@section('after-styles')
    <style>
        body {
            {{--background: url({{URL::asset('/img/login_bg.jpg')}}) no-repeat center center fixed;--}}
background: #188ae2;
            /*-webkit-background-size: cover;*/
            /*-moz-background-size: cover;*/
            /*-o-background-size: cover;*/
            /*background-size: cover;*/
        }
        .clock-bottom {
            background-color: transparent;
            position: absolute;
            left: 0;
            right: 0;
            bottom: 0;
            z-index: 10;
            padding: 20px 0 35px 35px;
            color: #191a24;
            text-shadow: 0 1px 1px rgba(0,0,0,.4);
            text-align: left;
            font-family: Open Sans;
            font-size: larger;
        }
        .login {
            background-color: rgba(67, 143, 165, 0.21) !important;
            border-radius: 4px;
            border: 0px solid transparent !important;
            box-shadow: 0 10px 10px rgba(0,0,0,.50)!important;
        }

        /*.btn {*/
        /*box-shadow: 0 10px 10px rgba(0,0,0,.25)!important;*/
        /*}*/

        /*a {*/
        /*color: #000000 !important;*/
        /*text-shadow: 0 10px 10px rgba(0,0,0,.95)!important;*/
        /*}*/

    </style>
@endsection
@section('content')
    @if (! $logged_in_user)
        <div id="page-container" class="main-content-boxed">
            <main id="main-container">
                <div class="bg-image" style="background-image: url('{{ asset('img/frontend/bg/photo34@2x.jpg') }}');">
                    <div class="row mx-0 bg-black-op">
                        <div class="hero-static col-md-6 col-xl-8 d-none d-md-flex align-items-md-end">
                            <div class="p-30 invisible" data-toggle="appear">
                                <p class="font-size-h3 font-w600 text-white">

                                </p>
                                <p class="font-italic text-white-op">
                                    Developed by <span class="font-w600"><strong>ISA - ERP [Web Team]</strong> - Transcom Ltd</span>
                                    <br>
                                    <span>Copyright &copy; <span class="js-year-copy">{{ date('Y') }}</span></span>
                                </p>
                            </div>
                        </div>
                        <div class="hero-static col-md-6 col-xl-4 d-flex align-items-center bg-white invisible" data-toggle="appear" data-class="animated fadeInRight">
                            <div class="content content-full pt-10">
                                {{ Html::image(asset('img/Transcom_Limited_mini.jpg'), 'Transcombd Logo',['class'=>'mx-auto d-block']) }}
                                @include('includes.partials.messages')
                                <br>
                                <br>
                            
                                <div class="px-30">
                                    <a class="link-effect font-w700 pb-5" href="{{ route('frontend.index') }}">
                                        <i class="si si-calculator fa-2x"></i>
                                        <span class="font-size-xl text-primary-dark">{{ substr(app_name(), 0, 4) }}</span><span class="font-size-xl">{{ substr(app_name(), 4, 7) }}</span>
                                    </a>
{{--                                    <h1 class="h3 font-w700 mt-30 mb-10">Welcome to Your Dashboard</h1>--}}
{{--                                    <h6 class="h6 font-w400 text-muted mb-0">Please sign in</h6>--}}
                                </div>
                                {{ Form::open(['route' => 'frontend.auth.login.post', 'class' => 'js-validation-signin px-30']) }}
                                    <div class="form-group row">
                                        <div class="col-12">
                                            <div class="form-material floating">
                                                {{ Form::text('login', null, ['class' => 'form-control', 'id'=>'login-username','maxlength' => '191', 'required' => 'required', 'autofocus' => 'autofocus']) }}
                                                <label for="login-username">Username</label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <div class="col-12">
                                            <div class="form-material floating">
                                                {!! Form::input('password', 'password', null, ['class' => 'form-control','id'=>'login-password']) !!}
                                                <label for="login-password">Password</label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <div class="col-12">
                                            <div class="custom-control custom-checkbox custom-control-inline">
                                                <input type="checkbox" class="custom-control-input" id="login-remember-me" name="remember">
                                                <label class="custom-control-label" for="login-remember-me">Remember Me</label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
{{--                                        {!! Form::submit(trans('labels.frontend.auth.login_button'), ['class' => 'btn btn-sm btn-hero btn-alt-primary']) !!}--}}
                                        <button type="submit" class="btn btn-sm btn-hero btn-alt-warning btn-block">
                                            <i class="si si-login mr-10"></i> Login
                                        </button>
                                    </div>
                                {!! Form::close() !!}

                                <div class="clock-bottom pull-right">
                                    <h2 style="font-size: 3rem"><span id="time">{!! $carbon->format('h:i').'<small>'.$carbon->format('s').'</small>'.' '.$carbon->format('A') !!}</span></h2>
                                    <small><strong><span id="weekday">{{ $carbon->format('d') }} </span>, <span id="today">{!! $carbon->format('F').', '.$carbon->format('d').', '.$carbon->format('Y')  !!}</span></strong></small>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </main>
        </div>

{{--        <div class="bg-image" style="background-image: url('assets/img/photos/photo34@2x.jpg');">--}}
{{--            <div class="row mx-0 bg-black-op">--}}
{{--                <div class="hero-static col-md-6 col-xl-8 d-none d-md-flex align-items-md-end">--}}
{{--                    <div class="p-30 invisible" data-toggle="appear">--}}
{{--                        <p class="font-w600 text-white">--}}

{{--                        </p>--}}
{{--                        <p class="font-italic text-white-op">--}}

{{--                        </p>--}}
{{--                    </div>--}}
{{--                </div>--}}
{{--                <div class="hero-static col-md-6 col-xl-4 d-flex align-items-center bg-white invisible" data-toggle="appear" data-class="animated fadeInRight">--}}
{{--                    <div class="content content-full">--}}
{{--                        --}}
{{--                        <div class="form-group">--}}
{{--                          --}}
{{--                            <a href="{{route('frontend.auth.password.email')}}">--}}
{{--                                <strong>Forgot Password</strong>--}}
{{--                            </a>--}}
{{--                            <div class="mt-30">--}}
{{--                                --}}{{--<a class="link-effect text-muted mr-10 mb-5 d-inline-block" href="op_auth_signup2.html">--}}
{{--                                --}}{{--<i class="fa fa-plus mr-5"></i> Create Account--}}
{{--                                --}}{{--</a>--}}
{{--                            </div>--}}
{{--                        </div>--}}
{{--                   --}}
{{--                    <!-- END Sign In Form -->--}}
{{--                       --}}
{{--                    </div>--}}
{{--                </div>--}}
{{--            </div>--}}
{{--        </div>--}}
    @endif
@endsection
