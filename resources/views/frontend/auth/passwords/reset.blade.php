@extends('frontend.layouts.app')

@section('title', app_name() . ' | Reset Password')
<style>
    body {
        background: url({{URL::asset('/img/login_bg.jpg')}}) no-repeat center center fixed;
        /*-webkit-background-size: cover;*/
        /*-moz-background-size: cover;*/
        /*-o-background-size: cover;*/
        /*background-size: cover;*/
    }
    .reset {
        background-color: rgba(67, 143, 165, 0.21) !important;
        border-radius: 4px;
        border: 0px solid transparent !important;
        box-shadow: 0 10px 10px rgba(0,0,0,.50)!important;
    }
    a {
        color: #000000 !important;
        text-shadow: 0 10px 10px rgba(0,0,0,.25)!important;
    }
</style>
@section('content')
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <img src="/uploads/logo.png" class="img-responsive center-block" alt="{{app_name()}}" style="padding-top: 150px;" />
            @if (session('status'))
                <div class="alert alert-success">
                    {{ session('status') }}
                </div>
            @endif

            <div class="panel panel-default reset">

                <div class="panel-heading">{{ trans('labels.frontend.passwords.reset_password_box_title') }}</div>

                <div class="panel-body">

                    {{ Form::open(['route' => 'frontend.auth.password.reset', 'class' => 'form-horizontal']) }}

                    <input type="hidden" name="token" value="{{ $token }}">

                    <div class="form-group">
                        {{ Form::label('email', trans('validation.attributes.frontend.email'), ['class' => 'col-md-4 control-label']) }}
                        <div class="col-md-6">
                            <p class="form-control-static">{{ $email }}</p>
                            {{ Form::hidden('email', $email, ['class' => 'form-control', 'placeholder' => trans('validation.attributes.frontend.email')]) }}
                        </div><!--col-md-6-->
                    </div><!--form-group-->

                    <div class="form-group">
                        {{ Form::label('password', trans('validation.attributes.frontend.password'), ['class' => 'col-md-4 control-label']) }}
                        <div class="col-md-6">
                            {{ Form::password('password', ['class' => 'form-control', 'required' => 'required', 'autofocus' => 'autofocus', 'placeholder' => trans('validation.attributes.frontend.password')]) }}
                        </div><!--col-md-6-->
                    </div><!--form-group-->

                    <div class="form-group">
                        {{ Form::label('password_confirmation', trans('validation.attributes.frontend.password_confirmation'), ['class' => 'col-md-4 control-label']) }}
                        <div class="col-md-6">
                            {{ Form::password('password_confirmation', ['class' => 'form-control', 'required' => 'required', 'placeholder' => trans('validation.attributes.frontend.password_confirmation')]) }}
                        </div><!--col-md-6-->
                    </div><!--form-group-->

                    <div class="form-group">
                        <div class="col-md-6 col-md-offset-4">
                            {{ Form::submit(trans('labels.frontend.passwords.reset_password_button'), ['class' => 'btn btn-primary']) }}
                        </div><!--col-md-6-->
                    </div><!--form-group-->

                    {{ Form::close() }}

                </div><!-- panel body -->

            </div><!-- panel -->

        </div><!-- col-md-8 -->

    </div><!-- row -->
@endsection
