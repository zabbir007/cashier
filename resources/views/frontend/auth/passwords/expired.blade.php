@extends('frontend.layouts.app')

@section('title', app_name() . ' | ' . __('labels.frontend.passwords.expired_password_box_title'))

@section('content')
    <div class="row justify-content-center align-items-center">
        <div class="col col-sm-6 align-self-center">
            <div class="card">
                <div class="card-header">
                    <strong>
                        @lang('labels.frontend.passwords.expired_password_box_title')
                    </strong>
                </div><!--card-header-->

                <div class="card-body">
                    {{ Form::open(['route' => 'frontend.auth.password.reset', 'class' => 'form-horizontal','method'=>'PATCH']) }}

                        <div class="row">
                            <div class="col">
                                <div class="form-group">
                                    {{ Form::label('old_password', __('validation.attributes.frontend.old_password'), ['class' => 'col-md-4 control-label']) }}
                                    {{ Form::password('old_password', ['class' => 'form-control', 'required' => 'required', 'autofocus' => 'autofocus', 'placeholder' => __('validation.attributes.frontend.old_password')]) }}
                                </div><!--form-group-->
                            </div><!--col-->
                        </div><!--row-->

                        <div class="row">
                            <div class="col">
                                <div class="form-group">
                                    {{ Form::label('password', __('validation.attributes.frontend.password'), ['class' => 'col-md-4 control-label']) }}
                                    {{ Form::password('password', ['class' => 'form-control', 'required' => 'required', 'autofocus' => 'autofocus', 'placeholder' => __('validation.attributes.frontend.password')]) }}
                                </div><!--form-group-->
                            </div><!--col-->
                        </div><!--row-->

                        <div class="row">
                            <div class="col">
                                <div class="form-group">
                                    {{ Form::label('password_confirmation', __('validation.attributes.frontend.password_confirmation'), ['class' => 'col-md-4 control-label']) }}
                                    {{ Form::password('password_confirmation', ['class' => 'form-control', 'required' => 'required', 'autofocus' => 'autofocus', 'placeholder' => __('validation.attributes.frontend.password_confirmation')]) }}
                                </div><!--form-group-->
                            </div><!--col-->
                        </div><!--row-->

                        <div class="row">
                            <div class="col">
                                <div class="form-group mb-0 clearfix">
                                    {{ Form::submit(trans('labels.frontend.passwords.update_password_button'), ['class' => 'btn btn-primary']) }}
                                </div><!--form-group-->
                            </div><!--col-->
                        </div><!--row-->

                    {{ Form::close() }}
                </div><!-- card-body -->
            </div><!-- card -->
        </div><!-- col-6 -->
    </div><!-- row -->
@endsection
