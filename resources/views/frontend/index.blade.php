@extends('frontend.layouts.app')
<style>
    body {
        {{--background: url({{URL::asset('/img/login_bg.jpg')}}) no-repeat center center fixed;--}}
        background: #188ae2;
        /*-webkit-background-size: cover;*/
        /*-moz-background-size: cover;*/
        /*-o-background-size: cover;*/
        /*background-size: cover;*/
    }
    .clock-bottom {
        background-color: transparent;
        position: absolute;
        left: 0;
        right: 0;
        bottom: 0;
        z-index: 10;
        padding: 20px 0 35px 35px;
        color: #191a24;
        text-shadow: 0 1px 1px rgba(0,0,0,.4);
        text-align: left;
        font-family: Open Sans;
        font-size: larger;
    }
    .login {
        background-color: rgba(67, 143, 165, 0.21) !important;
        border-radius: 4px;
        border: 0px solid transparent !important;
        box-shadow: 0 10px 10px rgba(0,0,0,.50)!important;
    }

    /*.btn {*/
        /*box-shadow: 0 10px 10px rgba(0,0,0,.25)!important;*/
    /*}*/

    /*a {*/
        /*color: #000000 !important;*/
        /*text-shadow: 0 10px 10px rgba(0,0,0,.95)!important;*/
    /*}*/

</style>
@section('content')
    @if (! $logged_in_user)

        <div class="bg-image" style="background-image: url('assets/img/photos/photo34@2x.jpg');">
            <div class="row mx-0 bg-black-op">
                <div class="hero-static col-md-6 col-xl-8 d-none d-md-flex align-items-md-end">
                    <div class="p-30 invisible" data-toggle="appear">
                        <p class="font-size-h3 font-w600 text-white">
                        
                        </p>
                        <p class="font-italic text-white-op">
                            Copyright &copy; <span class="js-year-copy">{{ date('Y') }}</span>
                        </p>
                    </div>
                </div>
                <div class="hero-static col-md-6 col-xl-4 d-flex align-items-center bg-white invisible" data-toggle="appear" data-class="animated fadeInRight">
                    <div class="content ">
                        <!-- Header -->
                        <div class="px-30 py-10">
                            <a class="link-effect font-w700" href="{{ route('frontend.index') }}">
                                <i class="si si-fire fa-2x"></i>
                                <span class="font-size-xl text-primary-dark">{{ substr(app_name(), 0, 2) }}</span><span class="font-size-xl">{{ substr(app_name(), 2, 7) }}</span>
                            </a>
                            {{--<h1 class="h3 font-w700 mt-30 mb-10">Welcome to Your Dashboard</h1>--}}
                            {{--<h2 class="h5 font-w400 text-muted mb-0">Please sign in</h2>--}}
                        </div>
                        <!-- END Header -->
                        {{ Form::open(['route' => 'frontend.auth.login.post', 'class' => 'form-horizontal js-validation-signin px-30']) }}
                            <div class="form-group row">
                                <div class="col-12">
                                    <div class="form-material floating">
                                        {{ Form::text('login', null, ['class' => 'form-control', 'id'=>'login-username','maxlength' => '191', 'required' => 'required', 'autofocus' => 'autofocus']) }}

                                        <label for="login-username">Username</label>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group row">
                                <div class="col-12">
                                    <div class="form-material floating">
                                        {!! Form::input('password', 'password', null, ['class' => 'form-control','id'=>'login-password']) !!}
                                        <label for="login-password">Password</label>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group row">
                                <div class="col-12">
                                    <label class="custom-control custom-checkbox">
                                        {{--{!! Form::checkbox('remember') !!}--}}
                                        <input type="checkbox" class="custom-control-input" id="login-remember-me" name="login-remember-me">
                                        <span class="custom-control-indicator"></span>
                                        <span class="custom-control-description">Remember Me</span>
                                    </label>
                                </div>
                            </div>
                            <div class="form-group">
                                {!! Form::submit(trans('labels.frontend.auth.login_button'), ['class' => 'btn btn-sm btn-hero btn-alt-primary']) !!}
                                <a href="{{route('frontend.auth.password.email')}}">
                                    <strong>Forgot Password</strong>
                                </a>
                                <div class="mt-30">
                                    {{--<a class="link-effect text-muted mr-10 mb-5 d-inline-block" href="op_auth_signup2.html">--}}
                                        {{--<i class="fa fa-plus mr-5"></i> Create Account--}}
                                    {{--</a>--}}
                                </div>
                            </div>
                            {!! Form::close() !!}
                        <!-- END Sign In Form -->
                        <div class="clock-bottom pull-right">
                            <h2 style="font-size: 3rem"><span id="time">{!! $carbon->format('h:i').'<small>'.$carbon->format('s').'</small>'.' '.$carbon->format('A') !!}</span></h2>
                            <small><strong><span id="weekday">{{ $carbon->format('d') }} </span>, <span id="today">{!! $carbon->format('F').', '.$carbon->format('d').', '.$carbon->format('Y')  !!}</span></strong></small>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    @endif
@endsection
@section('after-scripts')

    <script type="text/javascript">
        var d = new Date();
        var weekday = new Array(7);
        weekday[0] =  "Sunday";
        weekday[1] = "Monday";
        weekday[2] = "Tuesday";
        weekday[3] = "Wednesday";
        weekday[4] = "Thursday";
        weekday[5] = "Friday";
        weekday[6] = "Saturday";
        var x = document.getElementById("weekday");
        x.innerHTML = weekday[d.getDay()];

        var month = new Array();
        month[0] = "January";
        month[1] = "February";
        month[2] = "March";
        month[3] = "April";
        month[4] = "May";
        month[5] = "June";
        month[6] = "July";
        month[7] = "August";
        month[8] = "September";
        month[9] = "October";
        month[10] = "November";
        month[11] = "December";
        var x = document.getElementById("today");
        x.innerHTML = month[d.getMonth()] + ", " + d.getDate() + ", "+d.getFullYear();

        function addZero(i) {
            if (i < 10) i = "0" + i;
            return i;
        }

        var x = document.getElementById("time");
        setInterval(function(){
            var d = new Date();
            var h = d.getHours();
            var m = d.getMinutes();
            var s = d.getSeconds();
            var ampm = h >= 12 ? 'PM' : 'AM';
            h = h % 12;
            h = h ? (h < 10 ? '0'+h : h) : 12;
            m = m < 10 ? '0'+m : m;
            s = s < 10 ? '0'+s : s;
//            x.innerHTML = h + ":" + m + ":" + s + " "+ampm;
            x.innerHTML = h + ":" + m + "<small>"+ s +"</small> " +" "+ ampm +" ";
        }, 1000);

    </script>

@endsection