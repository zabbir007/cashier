/*
 *  Document   : op_auth_signin.js
 *  Author     : pixelcave
 *  Description: Custom JS code used in Sign In Page
 */
function addZero(i) {
    if (i < 10) i = "0" + i;
    return i;
}

const OpAuthSignIn = function () {
    // Init Sign In Form Validation, for more examples you can check out https://github.com/jzaefferer/jquery-validation
    // var initValidationSignIn = function(){
    //     jQuery('.js-validation-signin').validate({
    //         errorClass: 'invalid-feedback animated fadeInDown',
    //         errorElement: 'div',
    //         errorPlacement: function(error, e) {
    //             jQuery(e).parents('.form-group > div').append(error);
    //         },
    //         highlight: function(e) {
    //             jQuery(e).closest('.form-group').removeClass('is-invalid').addClass('is-invalid');
    //         },
    //         success: function(e) {
    //             jQuery(e).closest('.form-group').removeClass('is-invalid');
    //             jQuery(e).remove();
    //         },
    //         rules: {
    //             'login-username': {
    //                 required: true,
    //                 minlength: 3
    //             },
    //             'login-password': {
    //                 required: true,
    //                 minlength: 5
    //             }
    //         },
    //         messages: {
    //             'login-username': {
    //                 required: 'Please enter a username',
    //                 minlength: 'Your username must consist of at least 3 characters'
    //             },
    //             'login-password': {
    //                 required: 'Please provide a password',
    //                 minlength: 'Your password must be at least 5 characters long'
    //             }
    //         }
    //     });
    // };

    const initClock = function () {
        let d = new Date();
        let weekday = new Array(7);
        weekday[0] = "Sunday";
        weekday[1] = "Monday";
        weekday[2] = "Tuesday";
        weekday[3] = "Wednesday";
        weekday[4] = "Thursday";
        weekday[5] = "Friday";
        weekday[6] = "Saturday";
        let weekday_div = document.getElementById("weekday");
        weekday_div.innerHTML = weekday[d.getDay()];

        let month = [];
        month[0] = "January";
        month[1] = "February";
        month[2] = "March";
        month[3] = "April";
        month[4] = "May";
        month[5] = "June";
        month[6] = "July";
        month[7] = "August";
        month[8] = "September";
        month[9] = "October";
        month[10] = "November";
        month[11] = "December";
        let today_div = document.getElementById("today");
        today_div.innerHTML = month[d.getMonth()] + ", " + d.getDate() + ", " + d.getFullYear();

        let time_div = document.getElementById("time");
        setInterval(function () {
            let d = new Date();
            let h = d.getHours();
            let m = d.getMinutes();
            let s = d.getSeconds();
            const ampm = h >= 12 ? 'PM' : 'AM';
            h = h % 12;
            h = h ? (h < 10 ? '0' + h : h) : 12;
            m = m < 10 ? '0' + m : m;
            s = s < 10 ? '0' + s : s;
//            x.innerHTML = h + ":" + m + ":" + s + " "+ampm;
            time_div.innerHTML = h + ":" + m + "<small>" + s + "</small> " + " " + ampm + " ";
        }, 1000);
    };

    return {
        init: function () {
            // Init Sign In Form Validation
            // initValidationSignIn();
            initClock();
        }
    };
}();

// Initialize when page loads
jQuery(function(){ OpAuthSignIn.init(); });
