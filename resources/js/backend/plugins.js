import 'jquery.appear';
import 'bootstrap';
import 'popper.js';
import 'jquery-scroll-lock';
import 'jquery-countto';

require('../core/jquery.slimscroll.min.js');
require('../core/js.cookie.min.js');
require('../jQuery.print.js');
// require('../plugins.js');

// require('../plugins/datatables/jquery.dataTables.min.js');
// require('../plugins/datatables/dataTables.bootstrap4.min.js');
require('../plugins/chartjs/Chart.bundle.min.js');
require('../plugins/bootstrap-wizard/jquery.bootstrap.wizard.js');
require('../plugins/jquery-validation/jquery.validate.min.js');
require('../plugins/jquery-validation/additional-methods.min.js');

window.select2 = require('select2');
window.swal = require('sweetalert2');
window.datepicker = require('bootstrap-datepicker');
// import './plugins/bootstrap-datepicker/css/bootstrap-datepicker.min.css'
window.datatables = require('datatables.net');
require('datatables.net-bs4');

import '../pages/be_pages_dashboard.js';
import '../pages/be_tables_datatables.js';

$(document).on("wheel", "input[type=number]", function (e) {
    $(this).blur();
});
