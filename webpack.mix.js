const mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.setPublicPath('public');

mix.sass( 'resources/sass/backend/app.scss','public/css/app.css')
    .sass( 'resources/sass/frontend/app.scss','public/css/frontend.css')
    //.sass('resources/sass/backend/app.scss', 'css/backend.css')
   .js('resources/js/frontend/app.js', 'public/js/frontend.js')
   .js('resources/js/backend/app.js', 'public/js/app.js')
    // .js([
    //    // 'resources/js/app.js',
    //     'resources/js/core/popper.min.js',
    //     'resources/js/core/jquery.slimscroll.min.js',
    //     'resources/js/core/jquery.scrollLock.min.js',
    //     'resources/js/core/jquery.appear.min.js',
    //     'resources/js/core/jquery.countTo.min.js',
    //     'resources/js/core/js.cookie.min.js',
    //     'resources/js/jQuery.print.js',
    //     'resources/js/plugins.js' ,
    //     'resources/js/pages/be_pages_dashboard.js',
    //     'resources/js/pages/be_tables_datatables.js',
    //     'resources/js/plugins/datatables/jquery.dataTables.min.js',
    //     'resources/js/plugins/datatables/dataTables.bootstrap4.min.js',
    //     'resources/js/plugins/chartjs/Chart.bundle.min.js',
    //     'resources/js/plugins/bootstrap-wizard/jquery.bootstrap.wizard.js',
    //     'resources/js/plugins/jquery-validation/jquery.validate.min.js',
    //     'resources/js/plugins/jquery-validation/additional-methods.min.js'
    // ], 'public/js/app.js')
    .extract([
        'jquery',
        'bootstrap',
        'popper.js',
        'jquery.appear',
        'jquery-scroll-lock',
        'jquery-countto',
        'jquery-slimscroll',
        'js-cookie',
        'Chart.js',
        'jquery-bootstrap-wizard',
        'jquery-validation',
        'axios',
        'sweetalert2',
        'lodash',
        'vue',
        'select2',
        'bootstrap-datepicker',
        'datatables.net',
        '@fortawesome/fontawesome-svg-core',
        '@fortawesome/free-brands-svg-icons',
        '@fortawesome/free-regular-svg-icons',
        '@fortawesome/free-solid-svg-icons'
    ]);

if (mix.inProduction()) {
    mix.version()
        .options({
            // Optimize JS minification process
         //   terser: {
                cache: true,
                parallel: true,
                sourceMap: true,
                processCssUrls: true
        //    }
        });
} else {
    // Uses inline source-maps on development
    mix.webpackConfig({
        devtool: 'inline-source-map'
    });
}
