-- phpMyAdmin SQL Dump
-- version 4.7.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Aug 12, 2018 at 01:35 PM
-- Server version: 10.1.25-MariaDB
-- PHP Version: 7.1.7

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `cashier`
--

-- --------------------------------------------------------

--
-- Table structure for table `banks`
--

CREATE TABLE `banks` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `short_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `address` text COLLATE utf8mb4_unicode_ci,
  `remarks` text COLLATE utf8mb4_unicode_ci,
  `order` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '1',
  `created_by` int(10) UNSIGNED NOT NULL,
  `updated_by` int(10) UNSIGNED DEFAULT NULL,
  `deleted_by` int(10) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `banks`
--

INSERT INTO `banks` (`id`, `name`, `short_name`, `address`, `remarks`, `order`, `status`, `created_by`, `updated_by`, `deleted_by`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'South East Bank Limited', 'SEBL', 'House 22 Road 56', 'Remarks', NULL, 1, 1, NULL, NULL, '2017-09-25 19:18:24', '2017-09-25 19:18:24', NULL),
(2, 'Eastern Bank Limited', 'EBL', 'House 22 Road 56', 'Remarks', NULL, 1, 1, NULL, NULL, '2017-09-25 19:18:47', '2017-09-25 19:18:47', NULL),
(3, 'Commercial Bank Of Ceylon Limited', 'CBCL', NULL, NULL, NULL, 1, 0, NULL, NULL, NULL, NULL, NULL),
(4, 'HSBC', 'HSBC', NULL, NULL, NULL, 1, 0, NULL, NULL, NULL, NULL, NULL),
(5, 'Prime Bank Limited', 'PBL', NULL, NULL, NULL, 1, 0, NULL, NULL, NULL, NULL, NULL),
(6, 'Standard Chartered Bank', 'SCB', NULL, NULL, NULL, 1, 0, NULL, NULL, NULL, NULL, NULL),
(7, 'Trust Bank Limited', 'TBL', NULL, '1', NULL, 1, 1, 0, NULL, NULL, NULL, NULL),
(8, 'Eastern Bank Limited', 'EBL', NULL, NULL, NULL, 1, 0, NULL, NULL, NULL, '2017-09-25 23:18:26', NULL),
(9, 'Dutch Bangla Bank Limited', 'DBBL', NULL, NULL, NULL, 1, 0, NULL, NULL, NULL, NULL, NULL),
(10, 'Mercantile Bank Limited', 'MBL', NULL, NULL, NULL, 1, 0, NULL, NULL, NULL, NULL, NULL),
(11, 'Dhaka Bank Limited', 'DBL', NULL, NULL, NULL, 1, 0, NULL, NULL, NULL, NULL, NULL),
(12, 'Bank Asia Limited', 'BAL', NULL, NULL, NULL, 1, 0, NULL, NULL, NULL, NULL, NULL),
(13, 'AB Bank limited', 'ABBL', NULL, NULL, NULL, 1, 0, NULL, NULL, NULL, NULL, NULL),
(14, 'The City Bank Limited', 'TCBL', 'A', 'R', NULL, 1, 0, 1, NULL, NULL, '2017-09-27 17:15:49', NULL),
(15, 'Bank Al Falah Ltd.', 'BAFL', NULL, NULL, NULL, 1, 0, NULL, NULL, NULL, NULL, NULL),
(16, 'BRAC Bank Ltd.', 'BBL', NULL, NULL, NULL, 1, 0, NULL, NULL, NULL, NULL, NULL),
(17, 'CITI Bank N.A', 'CITI N.A', 'A', 'R', NULL, 1, 0, 1, NULL, NULL, '2017-09-27 17:16:16', NULL),
(18, 'Pubali Bank Limited', 'PUBL', 'House 22 Road 56', 'Remarks', NULL, 1, 1, NULL, NULL, '2017-10-31 05:32:00', '2017-10-31 05:32:00', NULL),
(19, 'Mutual Trust Bank Limited', 'MTB', 'House 22 Road 56', 'Remarks', NULL, 1, 1, NULL, NULL, '2017-10-31 05:32:30', '2017-10-31 05:32:30', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `branches`
--

CREATE TABLE `branches` (
  `id` int(10) UNSIGNED NOT NULL,
  `code` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `address1` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `address2` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `address3` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `address4` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `location` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `bic_id` int(11) DEFAULT NULL,
  `dbic_id` int(11) DEFAULT NULL,
  `aic_id` int(11) DEFAULT NULL,
  `order` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '1',
  `created_by` int(10) UNSIGNED NOT NULL,
  `updated_by` int(10) UNSIGNED DEFAULT NULL,
  `deleted_by` int(10) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `branches`
--

INSERT INTO `branches` (`id`, `code`, `name`, `address1`, `address2`, `address3`, `address4`, `email`, `location`, `bic_id`, `dbic_id`, `aic_id`, `order`, `status`, `created_by`, `updated_by`, `deleted_by`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, '110', 'Motijheel', '52, Motijheel C/A', NULL, NULL, 'Dhaka-1000', 'motijheel@tdcl.transcombd.com', NULL, NULL, NULL, NULL, '', 1, 0, 1, NULL, '2018-07-29 23:26:33', '2018-08-04 01:25:30', NULL),
(2, '115', 'Mirpur', 'Mirpur 1', NULL, NULL, NULL, 'mirpur@tdcl.transcombd.com', NULL, NULL, NULL, NULL, '', 1, 0, NULL, NULL, '2018-07-29 23:09:25', '2018-07-31 22:01:23', NULL),
(3, '120', 'Mohakhali', 'Sadar Road, Mohakhali', '', '', 'Dhaka', 'mohakhali@tdcl.transcombd.com', '', NULL, NULL, NULL, '', 1, 0, NULL, NULL, '2018-07-29 23:09:25', NULL, NULL),
(4, '130', 'Narayanganj', 'House No-01; Road No-03', 'Block-C; Middle Shastapur; Fatullah', '', 'Narayanganj-1400', 'narayangang@tdcl.transcombd.com', '', NULL, NULL, NULL, '', 1, 0, NULL, NULL, '2018-07-29 23:09:25', NULL, NULL),
(5, '140', 'Mymensingh', '6, Atul Chakraborty Road', '', '', 'Mymensingh', 'mymensingh@tdcl.transcombd.com', '', NULL, NULL, NULL, '', 1, 0, NULL, NULL, '2018-07-29 23:09:25', NULL, NULL),
(6, '150', 'Tangail', 'Holding No-103; Plot-446; Ward-15', 'Shibnath Para', '', 'Tangail', 'tangail@tdcl.transcombd.com', '', NULL, NULL, NULL, '', 1, 0, NULL, NULL, '2018-07-29 23:09:25', NULL, NULL),
(7, '160', 'Gazipur', 'Holding No-E 32', 'East Joydebpur (Baruda)', '', 'Gazipur-1700', 'gazipur@tdcl.transcombd.com', '', NULL, NULL, NULL, '', 1, 0, NULL, NULL, '2018-07-29 23:09:25', NULL, NULL),
(8, '170', 'Savar', 'House No-09; Tarapur', '', '', 'Savar', 'savar@tdcl.transcombd.com', '', NULL, NULL, NULL, '', 1, 0, NULL, NULL, '2018-07-29 23:09:25', NULL, NULL),
(9, '180', 'Bhairab', '1627/2, New Town; Bhairab', 'PS-Bhairab; Dist-Kishoregonj', '', 'Bhairab', 'bhairab@tdcl.transcombd.com', '', NULL, NULL, NULL, '', 1, 0, NULL, NULL, '2018-07-29 23:09:25', NULL, NULL),
(10, '190', 'Kishoregonj', 'Mollik Bhoban', 'Holding No-657/01; Nouga; Pakondia Road', '', 'Kishoregonj', 'kishoreganj@tdcl.transcombd.com', '', NULL, NULL, NULL, '', 1, 0, NULL, NULL, '2018-07-29 23:09:25', NULL, NULL),
(11, '200', 'Keraniganj', 'House No-53/1; Road No-04', 'Block -B; Ward No-03', 'Chunkutia West Para (Hijoltala), South Keraniganj', 'Dhaka-1310', 'keraniganj@tdcl.transcombd.com', '', NULL, NULL, NULL, '', 1, 0, NULL, NULL, '2018-07-29 23:09:25', NULL, NULL),
(12, '210', 'Chittagong South', 'Taher Chamber', '10, Agrabad C/A', '', 'Chittagong', 'chittagong@tdcl.transcombd.com', '', NULL, NULL, NULL, '', 1, 0, NULL, NULL, '2018-07-29 23:09:25', NULL, NULL),
(13, '220', 'Comilla', '123 New Nazrul Avenue', 'Ranir Bazar Road', '', 'Comilla', 'comilla@tdcl.transcombd.com', '', NULL, NULL, NULL, '', 1, 0, NULL, NULL, '2018-07-29 23:09:25', NULL, NULL),
(14, '230', 'Noakhali', 'Kazi Kolony, West Rajarampur', 'Maijdee Court', '', 'Noakhali', 'noakhali@tdcl.transcombd.com', '', NULL, NULL, NULL, '', 1, 0, NULL, NULL, '2018-07-29 23:09:25', NULL, NULL),
(15, '240', 'Cox\'s Bazar', 'Alir Zahil, Jilongza, Main Road', '', '', 'Cox\'s Bazar', 'coxsbazar@tdcl.transcombd.com', '', NULL, NULL, NULL, '', 1, 0, NULL, NULL, '2018-07-29 23:09:25', NULL, NULL),
(16, '250', 'Chandpur', 'Jalkador Monjil', 'Holding No-594; Ward-12; Mission Road', '', 'Chandpur', 'chandpur@tdcl.transcombd.com', '', NULL, NULL, NULL, '', 1, 0, NULL, NULL, '2018-07-29 23:09:25', NULL, NULL),
(17, '260', 'Chittagong North', 'Three Star Jilani Tower (1st Floor)', '1138, Hathazari Road, Roufabad', 'West Sholoshohor', 'Chittagong', 'ctgnorth@tdcl.transcombd.com', '', NULL, NULL, NULL, '', 1, 0, NULL, NULL, '2018-07-29 23:09:25', NULL, NULL),
(18, '270', 'Feni', 'Sunny Bhaban', 'Holding No-291; Ward No-05', 'BISCIC Road, Bathania', 'Feni', 'feni@tdcl.transcombd.com', '', NULL, NULL, NULL, '', 1, 0, NULL, NULL, '2018-07-29 23:09:25', NULL, NULL),
(19, '310', 'Rajshahi', 'Holding No-156/1, South Dorikhorbona', 'Kadirgonj Kathmil', '', 'Rajshahi', 'rajshahi@tdcl.transcombd.com', '', NULL, NULL, NULL, '', 1, 0, NULL, NULL, '2018-07-29 23:09:25', NULL, NULL),
(20, '320', 'Bogra', 'Fuldighi', '', '', 'Bogra', 'bogra@tdcl.transcombd.com', '', NULL, NULL, NULL, '', 1, 0, NULL, NULL, '2018-07-29 23:09:25', NULL, NULL),
(21, '330', 'Rangpur', 'Road-01, 120/3 Gomostapara', '', '', 'Rangpur', 'rangpur@tdcl.transcombd.com', '', NULL, NULL, NULL, '', 1, 0, NULL, NULL, '2018-07-29 23:09:25', NULL, NULL),
(22, '340', 'Dinajpur', 'Block No-10; Plot No-68; Road No-02', 'New Town', '', 'Dinajpur', 'dinajpur@tdcl.transcombd.com', '', NULL, NULL, NULL, '', 1, 0, NULL, NULL, '2018-07-29 23:09:25', NULL, NULL),
(23, '350', 'Pabna', 'Nurzahan Palace; Holding No-615', 'R M Academy Road; Radha Nagar', '', 'Pabna', 'pabna@tdcl.tnunscombd.com', '', NULL, NULL, NULL, '', 1, 0, NULL, NULL, '2018-07-29 23:09:25', NULL, NULL),
(24, '410', 'Khulna', 'House-111, Road-02', 'Sonadanga R/A', '', 'Khulna', 'khulna@tdcl.transcombd.com', '', NULL, NULL, NULL, '', 1, 0, NULL, NULL, '2018-07-29 23:09:25', NULL, NULL),
(25, '420', 'Faridpur', 'Jubilitank Road', 'South Kalibari, Jheeltuli', '', 'Faridpur', 'faridpur@tdcl.transcombd.com', '', NULL, NULL, NULL, '', 1, 0, NULL, NULL, '2018-07-29 23:09:25', NULL, NULL),
(26, '430', 'Kushtia', '41, Arjundas Agarwala Sarak (Near Rab Camp)', 'Court Para', '', 'Kushtia', 'kushtia@tdcl.transcombd.com', '', NULL, NULL, NULL, '', 1, 0, NULL, NULL, '2018-07-29 23:09:25', NULL, NULL),
(27, '440', 'Jessore', '57/A, Hazarat Borhan Shah Road', 'Karbala', '', 'Jessore', 'jessore@tdcl.transcombd.com', '', NULL, NULL, NULL, '', 1, 0, NULL, NULL, '2018-07-29 23:09:25', NULL, NULL),
(28, '510', 'Barisal', '367, Hazrat Shah Sarak', 'Alekanda', '', 'Barisal', 'barisal@tdcl.transcombd.com', '', NULL, NULL, NULL, '', 1, 0, NULL, NULL, '2018-07-29 23:09:25', NULL, NULL),
(29, '520', 'Patuakhali', 'Holding No-1109; Ward No-07', 'DC Banglo Road', '', 'Patuakhali', 'patuakhali@tdcl.transcombd.com', '', NULL, NULL, NULL, '', 1, 0, NULL, NULL, '2018-07-29 23:09:25', NULL, NULL),
(30, '610', 'Sylhet', 'Patharia Palace, Manikpir Road', 'Naya Sarak', '', 'Sylhet', 'sylhet@tdcl.transcombd.com', '', NULL, NULL, NULL, '', 1, 0, NULL, NULL, '2018-07-29 23:09:25', NULL, NULL),
(31, '620', 'Moulvi Bazar', 'Rahman Manjil', 'West Boro Kapon; Sylhet Road', '', 'Moulvi Bazar', 'moulvibazar@tdcl.transcombd.com', '', NULL, NULL, NULL, '', 1, 0, NULL, NULL, '2018-07-29 23:09:25', NULL, NULL),
(32, '000', 'Others', '', '', '', '', '', '', NULL, NULL, NULL, '', 1, 0, NULL, NULL, '2018-07-29 23:09:25', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `branch_bank`
--

CREATE TABLE `branch_bank` (
  `id` int(10) UNSIGNED NOT NULL,
  `branch_id` int(11) DEFAULT NULL,
  `bank_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `branch_bank`
--

INSERT INTO `branch_bank` (`id`, `branch_id`, `bank_id`) VALUES
(1, 1, 2),
(2, 1, 3),
(3, 2, 1),
(4, 2, 2),
(7, 2, 5),
(8, 2, 6),
(10, 2, 8),
(13, 2, 19);

-- --------------------------------------------------------

--
-- Table structure for table `brands`
--

CREATE TABLE `brands` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `code` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `remarks` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `order` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '1',
  `created_by` int(10) UNSIGNED NOT NULL,
  `updated_by` int(10) UNSIGNED DEFAULT NULL,
  `deleted_by` int(10) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `brands`
--

INSERT INTO `brands` (`id`, `name`, `code`, `remarks`, `order`, `status`, `created_by`, `updated_by`, `deleted_by`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'Pharma', '001', NULL, NULL, 1, 1, NULL, NULL, '2018-08-04 00:11:01', '2018-08-04 00:11:01', NULL),
(2, 'CBD', '002', NULL, NULL, 1, 1, NULL, NULL, '2018-08-04 00:11:29', '2018-08-04 00:11:29', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `employees`
--

CREATE TABLE `employees` (
  `id` int(10) UNSIGNED NOT NULL,
  `employee_id` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `employee_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `employee_designation` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `employee_avatar` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `grade` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `mobile_no` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email_address` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `branch_id` int(11) DEFAULT NULL,
  `branch_code` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `branch_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status_id` int(11) NOT NULL DEFAULT '1',
  `status_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'Entry',
  `order` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '1',
  `created_by` int(10) UNSIGNED NOT NULL,
  `updated_by` int(10) UNSIGNED DEFAULT NULL,
  `deleted_by` int(10) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `employees`
--

INSERT INTO `employees` (`id`, `employee_id`, `employee_name`, `employee_designation`, `employee_avatar`, `grade`, `mobile_no`, `email_address`, `branch_id`, `branch_code`, `branch_name`, `status_id`, `status_name`, `order`, `status`, `created_by`, `updated_by`, `deleted_by`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, '12002', 'Alamgir Hossain', 'BIC', NULL, 'A', '01709633912', 'aftab.tohan@hotmail.com', 1, '110', 'Motijheel', 1, 'Entry', NULL, 1, 1, NULL, NULL, '2018-08-02 05:20:40', '2018-08-02 05:20:40', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `history`
--

CREATE TABLE `history` (
  `id` int(10) UNSIGNED NOT NULL,
  `type_id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `entity_id` int(10) UNSIGNED DEFAULT NULL,
  `icon` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `class` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `text` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `assets` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `history`
--

INSERT INTO `history` (`id`, `type_id`, `user_id`, `entity_id`, `icon`, `class`, `text`, `assets`, `created_at`, `updated_at`) VALUES
(1, 1, 1, 1, 'save', 'bg-aqua', 'trans(\"history.backend.users.updated\") <strong>{user}</strong>', '{\"user_link\":[\"admin.access.user.show\",\"Admin Istrator\",1]}', '2018-07-31 22:18:06', '2018-07-31 22:18:06'),
(2, 1, 1, 1, 'save', 'bg-aqua', 'trans(\"history.backend.users.updated\") <strong>{user}</strong>', '{\"user_link\":[\"admin.access.user.show\",\"Admin Istrator\",1]}', '2018-07-31 22:18:43', '2018-07-31 22:18:43'),
(3, 1, 1, 1, 'save', 'bg-aqua', 'trans(\"history.backend.users.updated\") <strong>{user}</strong>', '{\"user_link\":[\"admin.access.user.show\",\"Admin Istrator\",1]}', '2018-08-01 00:06:38', '2018-08-01 00:06:38'),
(4, 1, 1, 1, 'save', 'bg-aqua', 'trans(\"history.backend.users.updated\") <strong>{user}</strong>', '{\"user_link\":[\"admin.access.user.show\",\"Admin Istrator\",1]}', '2018-08-01 00:58:09', '2018-08-01 00:58:09'),
(5, 1, 1, 5, 'plus', 'bg-green', 'trans(\"history.backend.users.created\") <strong>{user}</strong>', '{\"user_link\":[\"admin.access.user.show\",\"Md. Aftab Uddin\",5]}', '2018-08-01 01:09:53', '2018-08-01 01:09:53'),
(6, 1, 1, 6, 'plus', 'bg-green', 'trans(\"history.backend.users.created\") <strong>{user}</strong>', '{\"user_link\":[\"admin.access.user.show\",\"Md. Aftab Uddin\",6]}', '2018-08-01 02:26:16', '2018-08-01 02:26:16'),
(7, 1, 1, 4, 'plus', 'bg-green', 'trans(\"history.backend.users.created\") <strong>{user}</strong>', '{\"user_link\":[\"admin.access.user.show\",\"Md. Aftab Uddin\",4]}', '2018-08-01 02:27:48', '2018-08-01 02:27:48'),
(8, 1, 1, 6, 'plus', 'bg-green', 'trans(\"history.backend.users.created\") <strong>{user}</strong>', '{\"user_link\":[\"admin.access.user.show\",\"Estiak Md Almas Suzan\",6]}', '2018-08-01 02:46:08', '2018-08-01 02:46:08'),
(9, 1, 1, 2, 'save', 'bg-aqua', 'trans(\"history.backend.users.updated\") <strong>{user}</strong>', '{\"user_link\":[\"admin.access.user.show\",\"Backend User\",2]}', '2018-08-01 03:05:19', '2018-08-01 03:05:19'),
(10, 1, 1, 4, 'save', 'bg-aqua', 'trans(\"history.backend.users.updated\") <strong>{user}</strong>', '{\"user_link\":[\"admin.access.user.show\",\"Md. Aftab Uddin\",4]}', '2018-08-02 05:01:51', '2018-08-02 05:01:51'),
(11, 1, 4, 2, 'save', 'bg-aqua', 'trans(\"history.backend.users.updated\") <strong>{user}</strong>', '{\"user_link\":[\"admin.access.user.show\",\"Backend User\",2]}', '2018-08-06 22:11:57', '2018-08-06 22:11:57'),
(12, 1, 4, 4, 'save', 'bg-aqua', 'trans(\"history.backend.users.updated\") <strong>{user}</strong>', '{\"user_link\":[\"admin.access.user.show\",\"Md. Aftab Uddin\",4]}', '2018-08-07 04:45:06', '2018-08-07 04:45:06'),
(13, 1, 4, 4, 'save', 'bg-aqua', 'trans(\"history.backend.users.updated\") <strong>{user}</strong>', '{\"user_link\":[\"admin.access.user.show\",\"Md. Aftab Uddin\",4]}', '2018-08-07 04:45:14', '2018-08-07 04:45:14'),
(14, 1, 1, 6, 'save', 'bg-aqua', 'trans(\"history.backend.users.updated\") <strong>{user}</strong>', '{\"user_link\":[\"admin.access.user.show\",\"Estiak Md Almas Suzan\",6]}', '2018-08-07 04:50:24', '2018-08-07 04:50:24'),
(15, 1, 1, 1, 'save', 'bg-aqua', 'trans(\"history.backend.users.updated\") <strong>{user}</strong>', '{\"user_link\":[\"admin.access.user.show\",\"Admin Istrator\",1]}', '2018-08-07 04:52:02', '2018-08-07 04:52:02'),
(16, 1, 4, 4, 'save', 'bg-aqua', 'trans(\"history.backend.users.updated\") <strong>{user}</strong>', '{\"user_link\":[\"admin.access.user.show\",\"Md. Aftab Uddin\",4]}', '2018-08-07 23:52:09', '2018-08-07 23:52:09');

-- --------------------------------------------------------

--
-- Table structure for table `history_types`
--

CREATE TABLE `history_types` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `history_types`
--

INSERT INTO `history_types` (`id`, `name`, `created_at`, `updated_at`) VALUES
(1, 'User', '2018-07-31 02:38:38', '2018-07-31 02:38:38'),
(2, 'Role', '2018-07-31 02:38:38', '2018-07-31 02:38:38');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2015_12_28_171741_create_social_logins_table', 1),
(4, '2015_12_29_015055_setup_access_tables', 1),
(5, '2016_07_03_062439_create_history_tables', 1),
(6, '2017_04_04_131153_create_sessions_table', 1),
(7, '2017_10_12_090521_alter_users_table', 1),
(8, '2018_07_29_123213_create_branches_table', 1),
(9, '2018_08_01_065626_create_branch_user_table', 2),
(10, '2018_08_01_105751_create_bank_table', 3),
(11, '2018_08_02_065046_create_employee_table', 4),
(12, '2018_08_04_050639_create_brands_table', 5),
(13, '2018_08_04_061626_create_products_table', 6),
(14, '2018_08_04_071311_create_branch_bank_table', 7),
(17, '2018_08_05_094823_create_transactions_date_wise_summary_table', 10),
(18, '2018_08_07_070834_create_transactions_date_wise_details_table', 10),
(19, '2018_08_07_033356_create_branch_product_table', 11),
(20, '2018_08_08_100343_create_transactions_bank_charges_summary_table', 12),
(21, '2018_08_08_100530_create_transactions_bank_charges_details_table', 12),
(24, '2018_08_08_111614_create_transactions_petty_cash_summary_table', 13),
(25, '2018_08_08_111640_create_transactions_petty_cash_details_table', 14),
(26, '2018_08_09_052632_create_transactions_deposits_summary_table', 15),
(27, '2018_08_09_052912_create_transactions_deposits_details_table', 15);

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `permissions`
--

CREATE TABLE `permissions` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `permissions`
--

INSERT INTO `permissions` (`id`, `name`, `display_name`, `created_at`, `updated_at`) VALUES
(1, 'view-backend', 'View Backend', '2018-07-31 02:38:37', '2018-07-31 02:38:37');

-- --------------------------------------------------------

--
-- Table structure for table `permission_role`
--

CREATE TABLE `permission_role` (
  `id` int(10) UNSIGNED NOT NULL,
  `permission_id` int(10) UNSIGNED NOT NULL,
  `role_id` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `permission_role`
--

INSERT INTO `permission_role` (`id`, `permission_id`, `role_id`) VALUES
(1, 1, 2);

-- --------------------------------------------------------

--
-- Table structure for table `products`
--

CREATE TABLE `products` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `code` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `short_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `brand_id` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `brand_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `remarks` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `order` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '1',
  `created_by` int(10) UNSIGNED NOT NULL,
  `updated_by` int(10) UNSIGNED DEFAULT NULL,
  `deleted_by` int(10) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `products`
--

INSERT INTO `products` (`id`, `name`, `code`, `short_name`, `brand_id`, `brand_name`, `remarks`, `order`, `status`, `created_by`, `updated_by`, `deleted_by`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'SK+F (Pharma)', '1001', 'SK+F (Pharma)', '1', 'Pharma', NULL, '1', 1, 1, 1, NULL, '2018-08-04 00:50:54', '2018-08-05 02:43:52', NULL),
(2, 'SK+F (AHD)', '1002', 'SK+F (AHD)', '1', 'Pharma', NULL, '2', 1, 1, NULL, NULL, '2018-08-05 00:50:53', '2018-08-05 00:50:53', NULL),
(3, 'SK+F (Allergan)', '1003', 'SK+F (Allergan)', '1', 'Pharma', NULL, '3', 1, 1, NULL, NULL, '2018-08-06 00:50:53', '2018-08-06 00:50:53', NULL),
(4, 'Servier', '1004', 'Servier', '1', 'Pharma', NULL, '4', 1, 1, NULL, NULL, '2018-08-07 00:50:53', '2018-08-07 00:50:53', NULL),
(5, 'Novo Nordisk', '1005', 'Novo Nordisk', '1', 'Pharma', NULL, '5', 1, 1, NULL, NULL, '2018-08-08 00:50:53', '2018-08-08 00:50:53', NULL),
(6, 'Microlife', '1006', 'Microlife', '1', 'Pharma', NULL, '6', 1, 1, NULL, NULL, '2018-08-09 00:50:53', '2018-08-09 00:50:53', NULL),
(7, 'Sinocare', '1007', 'Sinocare', '1', 'Pharma', NULL, '7', 1, 1, NULL, NULL, '2018-08-10 00:50:53', '2018-08-10 00:50:53', NULL),
(8, 'Diagnostic', '1008', 'Diagnostic', '1', 'Pharma', NULL, '8', 1, 1, NULL, NULL, '2018-08-11 00:50:53', '2018-08-11 00:50:53', NULL),
(9, 'Omron', '1009', 'Omron', '1', 'Pharma', NULL, '9', 1, 1, NULL, NULL, '2018-08-12 00:50:53', '2018-08-12 00:50:53', NULL),
(10, 'Fritolays', '1010', 'Fritolays', '2', 'CBD', NULL, '10', 1, 1, NULL, NULL, '2018-08-12 00:50:53', '2018-08-12 00:50:53', NULL),
(11, 'Garnier', '1011', 'Garnier', '2', 'CBD', NULL, '11', 1, 1, NULL, NULL, '2018-08-12 00:50:53', '2018-08-12 00:50:53', NULL),
(12, 'Heinz', '1012', 'Heinz', '2', 'CBD', NULL, '12', 1, 1, NULL, NULL, '2018-08-12 00:50:53', '2018-08-12 00:50:53', NULL),
(13, 'Energizer', '1013', 'Energizer', '2', 'CBD', NULL, '13', 1, 1, NULL, NULL, '2018-08-12 00:50:53', '2018-08-12 00:50:53', NULL),
(14, 'Mars', '1014', 'Mars', '2', 'CBD', NULL, '14', 1, 1, NULL, NULL, '2018-08-12 00:50:53', '2018-08-12 00:50:53', NULL),
(15, 'CAL X   (SK+F CBD)', '1015', 'CAL X   (SK+F CBD)', '2', 'CBD', NULL, '15', 1, 1, NULL, NULL, '2018-08-12 00:50:53', '2018-08-12 00:50:53', NULL),
(16, 'Conagra', '1016', 'Conagra', '2', 'CBD', NULL, '16', 1, 1, NULL, NULL, '2018-08-12 00:50:53', '2018-08-12 00:50:53', NULL),
(17, 'L\'OREAL', '1017', 'L\'OREAL', '2', 'CBD', NULL, '17', 1, 1, NULL, NULL, '2018-08-12 00:50:53', '2018-08-12 00:50:53', NULL),
(18, 'VIDYUT/ SUPER MAX', '1018', 'VIDYUT/ SUPER MAX', '2', 'CBD', NULL, '18', 1, 1, NULL, NULL, '2018-08-12 00:50:53', '2018-08-12 00:50:53', NULL),
(19, 'Park Avenue', '1019', 'Park Avenue', '2', 'CBD', NULL, '19', 1, 1, NULL, NULL, '2018-08-12 00:50:53', '2018-08-12 00:50:53', NULL),
(20, 'Abbott', '1020', 'Abbott', '2', 'CBD', NULL, '20', 1, 1, NULL, NULL, '2018-08-12 00:50:53', '2018-08-12 00:50:53', NULL),
(21, 'Bajaj', '1021', 'Bajaj', '2', 'CBD', NULL, '21', 1, 1, NULL, NULL, '2018-08-12 00:50:53', '2018-08-12 00:50:53', NULL),
(22, 'Ferrerro', '1022', 'Ferrerro', '2', 'CBD', NULL, '22', 1, 1, NULL, NULL, '2018-08-12 00:50:53', '2018-08-12 00:50:53', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `product_branch`
--

CREATE TABLE `product_branch` (
  `id` int(10) UNSIGNED NOT NULL,
  `product_id` int(11) DEFAULT NULL,
  `branch_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `product_branch`
--

INSERT INTO `product_branch` (`id`, `product_id`, `branch_id`) VALUES
(1, 1, 1),
(2, 1, 2),
(3, 1, 3),
(4, 2, 2),
(5, 6, 3),
(6, 6, 4),
(7, 6, 5),
(8, 15, 1),
(9, 15, 2),
(10, 17, 2),
(11, 17, 4);

-- --------------------------------------------------------

--
-- Table structure for table `roles`
--

CREATE TABLE `roles` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `all` tinyint(1) NOT NULL DEFAULT '0',
  `sort` smallint(5) UNSIGNED NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `roles`
--

INSERT INTO `roles` (`id`, `name`, `all`, `sort`, `created_at`, `updated_at`) VALUES
(1, 'Administrator', 1, 1, '2018-07-31 02:38:36', '2018-07-31 02:38:36'),
(2, 'Executive', 0, 2, '2018-07-31 02:38:36', '2018-07-31 02:38:36'),
(3, 'User', 0, 3, '2018-07-31 02:38:36', '2018-07-31 02:38:36');

-- --------------------------------------------------------

--
-- Table structure for table `role_user`
--

CREATE TABLE `role_user` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `role_id` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `role_user`
--

INSERT INTO `role_user` (`id`, `user_id`, `role_id`) VALUES
(3, 3, 3),
(18, 2, 2),
(21, 6, 1),
(22, 1, 1),
(23, 1, 1),
(24, 4, 1);

-- --------------------------------------------------------

--
-- Table structure for table `sessions`
--

CREATE TABLE `sessions` (
  `id` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` int(10) UNSIGNED DEFAULT NULL,
  `ip_address` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `user_agent` text COLLATE utf8mb4_unicode_ci,
  `payload` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `last_activity` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `sessions`
--

INSERT INTO `sessions` (`id`, `user_id`, `ip_address`, `user_agent`, `payload`, `last_activity`) VALUES
('haAijN7GitkrjyfNMZ1zConL9wjicBWqKH2qSqOI', 4, '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:61.0) Gecko/20100101 Firefox/61.0', 'ZXlKcGRpSTZJak0yZEhCNE5YWlZSalJIY1dSYWRYVlhXbHBsU0djOVBTSXNJblpoYkhWbElqb2lhVnd2VVVGVFYxVmpUR2R3UkVSeGJIbEpPRlZhVkRWSFRtZGhaVlZTTjNvNFVua3hXRmRITXl0alNWWjFlVGRzY1VwUlMzWnlTMWRpVml0YWRrMDJhVEZ3TlVoUWMyUXJTRXBKZWxkSk4xb3dSbU5IWTNoaVNXbGFhMnREV1RaWU4xTkRSMDQwVDBJNWJYY3pjemhUY0Voa1VIbExRekpxUW5WNlRXNUlSVFJIYURSVmEzQjZRbVkxZEc0eU0wOUZOR1pKUTFSaWJVeHdLelJwY1ZSUVdXdExWMHBKVVdaMmR6Rk5NVkZ6WjJONlJGcE5lVE5rZVZwSVFrSXlZVkJPZDNwNVMwOXZZMlZFT1dodVZETjZibkYyU0dZMVJsd3ZWR2xtSzB4YVRTdHFlSGszUTBWTlhDOWxjMWczVldGNVNXdG1Xbk0xUWxWeFoyMXBRbEZoT0V0Q1oxUlJWWEJrTWpOblhDODBRbGRrV25CTWQxZEhha3QzY21NclpHWTBlRlZ1YTFkUWNGVkZWbWhSYVVwS1dYTlphVTV5Vm0wNFpWWnRaa3BFUkhka05uZFRSRUpYUm5VMFJIbHlaVUo0YUcxQk1XVTVNbUZaVlhOeGJGcE1UR1pIZUZ3dmVXTjBlVkJvZDJ4a2JWQjBkMjFUWkZ3dmVERkNPRFJ3YzFWbU5IVnhNMWd6YW5WTGRXYzFiMDlRTjJ0NVdsWnpTME5GUzNkd0syRm1aa0V5UVd3NVpXUlVRVU5rYTFwa1NESmpWVWc0VFN0dVQzWkZSMDQ1WmxoVVlVbDBkVzR3ZHlJc0ltMWhZeUk2SWpSbFlqVXpZVEprTWpRME5qaGlOVEk0TW1KaFkyVTJNR0U0WlRNMk1qZ3daRFJpTVRoa09EY3hPRGN3T1dRNE56Qm1PRE16WWpVM1pUbGpOVGxrWlRnaWZRPT0=', 1534073669);

-- --------------------------------------------------------

--
-- Table structure for table `social_logins`
--

CREATE TABLE `social_logins` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `provider` varchar(32) COLLATE utf8mb4_unicode_ci NOT NULL,
  `provider_id` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `avatar` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `transactions_bank_charge_details`
--

CREATE TABLE `transactions_bank_charge_details` (
  `id` int(10) UNSIGNED NOT NULL,
  `trans_id` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'Date Wise Receipt head id',
  `trans_sl` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'Date Wise Receipt head Serial Number',
  `branch_id` int(11) DEFAULT NULL,
  `branch_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `trans_date` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `bank_id` int(11) DEFAULT NULL,
  `bank_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `amount` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `optional_field1` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `optional_field2` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `optional_field3` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `year` int(11) NOT NULL,
  `month` varchar(15) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_by` int(10) UNSIGNED NOT NULL,
  `updated_by` int(10) UNSIGNED DEFAULT NULL,
  `deleted_by` int(10) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `transactions_bank_charge_details`
--

INSERT INTO `transactions_bank_charge_details` (`id`, `trans_id`, `trans_sl`, `branch_id`, `branch_name`, `trans_date`, `bank_id`, `bank_name`, `amount`, `optional_field1`, `optional_field2`, `optional_field3`, `status`, `year`, `month`, `created_by`, `updated_by`, `deleted_by`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, '1', 'MIR-20180812-1', 2, 'Mirpur', '2018-08-12', 1, 'South East Bank Limited', '123', NULL, NULL, NULL, '1', 2018, '7', 4, 4, NULL, '2018-08-12 02:48:16', '2018-08-12 02:58:21', NULL),
(2, '1', 'MIR-20180812-1', 2, 'Mirpur', '2018-08-12', 2, 'Eastern Bank Limited', '123', NULL, NULL, NULL, '1', 2018, '7', 4, 4, NULL, '2018-08-12 02:48:16', '2018-08-12 02:58:21', NULL),
(3, '1', 'MIR-20180812-1', 2, 'Mirpur', '2018-08-12', 5, 'Prime Bank Limited', '123', NULL, NULL, NULL, '1', 2018, '7', 4, 4, NULL, '2018-08-12 02:48:16', '2018-08-12 02:58:21', NULL),
(4, '1', 'MIR-20180812-1', 2, 'Mirpur', '2018-08-12', 6, 'Standard Chartered Bank', '12', NULL, NULL, NULL, '1', 2018, '7', 4, 4, NULL, '2018-08-12 02:48:16', '2018-08-12 02:58:21', NULL),
(5, '1', 'MIR-20180812-1', 2, 'Mirpur', '2018-08-12', 8, 'Eastern Bank Limited', '12', NULL, NULL, NULL, '1', 2018, '7', 4, 4, NULL, '2018-08-12 02:48:16', '2018-08-12 02:58:21', NULL),
(6, '1', 'MIR-20180812-1', 2, 'Mirpur', '2018-08-12', 19, 'Mutual Trust Bank Limited', '12', NULL, NULL, NULL, '1', 2018, '7', 4, 4, NULL, '2018-08-12 02:48:16', '2018-08-12 02:58:21', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `transactions_bank_charge_summary`
--

CREATE TABLE `transactions_bank_charge_summary` (
  `id` int(10) UNSIGNED NOT NULL,
  `sl` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'transaction no',
  `branch_id` int(11) DEFAULT NULL,
  `branch_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `trans_date` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `year` int(11) NOT NULL,
  `month` varchar(15) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_by` int(10) UNSIGNED NOT NULL,
  `updated_by` int(10) UNSIGNED DEFAULT NULL,
  `deleted_by` int(10) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `transactions_bank_charge_summary`
--

INSERT INTO `transactions_bank_charge_summary` (`id`, `sl`, `branch_id`, `branch_name`, `trans_date`, `status`, `year`, `month`, `created_by`, `updated_by`, `deleted_by`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'MIR-20180812-1', 2, 'Mirpur', '2018-08-12 08:48:16', '1', 2018, '7', 4, NULL, NULL, '2018-08-12 02:48:16', '2018-08-12 02:48:16', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `transactions_date_wise_details`
--

CREATE TABLE `transactions_date_wise_details` (
  `id` int(10) UNSIGNED NOT NULL,
  `trans_id` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'Date Wise Receipt head id',
  `trans_sl` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'Date Wise Receipt head Serial Number',
  `branch_id` int(11) DEFAULT NULL,
  `branch_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `trans_date` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `brand_id` int(11) DEFAULT NULL,
  `brand_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `product_id` int(11) DEFAULT NULL,
  `product_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `collection` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `money_receipt` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `adv_receipt` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `short_receipt` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'Short/Express Receipt',
  `adv_adj` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'Advance Adjustment',
  `adj_plus` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'Adjustment(+)',
  `adj_minus` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'Adjustment(-)',
  `status` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `year` int(11) NOT NULL,
  `month` varchar(15) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_by` int(10) UNSIGNED NOT NULL,
  `updated_by` int(10) UNSIGNED DEFAULT NULL,
  `deleted_by` int(10) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `transactions_date_wise_details`
--

INSERT INTO `transactions_date_wise_details` (`id`, `trans_id`, `trans_sl`, `branch_id`, `branch_name`, `trans_date`, `brand_id`, `brand_name`, `product_id`, `product_name`, `collection`, `money_receipt`, `adv_receipt`, `short_receipt`, `adv_adj`, `adj_plus`, `adj_minus`, `status`, `year`, `month`, `created_by`, `updated_by`, `deleted_by`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, '1', 'MIR-20180812-1', 2, 'Mirpur', '2018-08-12', 1, 'Pharma', 1, 'SK+F (Pharma)', '2', '2', '2', '2', '2', '2', '2', '1', 2018, '7', 4, 4, NULL, '2018-08-12 00:02:02', '2018-08-12 00:03:49', NULL),
(2, '1', 'MIR-20180812-1', 2, 'Mirpur', '2018-08-12', 1, 'Pharma', 2, 'SK+F (AHD)', '2', '2', '2', '2', '2', '2', '2', '1', 2018, '7', 4, 4, NULL, '2018-08-12 00:02:02', '2018-08-12 00:03:49', NULL),
(3, '1', 'MIR-20180812-1', 2, 'Mirpur', '2018-08-12', 2, 'CBD', 15, 'CAL X   (SK+F CBD)', '2', '2', '2', '2', '2', '2', '2', '1', 2018, '7', 4, 4, NULL, '2018-08-12 00:02:02', '2018-08-12 00:03:49', NULL),
(4, '1', 'MIR-20180812-1', 2, 'Mirpur', '2018-08-12', 2, 'CBD', 17, 'L\'OREAL', '2', '2', '2', '2', '2', '2', '2', '1', 2018, '7', 4, 4, NULL, '2018-08-12 00:02:02', '2018-08-12 00:03:50', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `transactions_date_wise_summary`
--

CREATE TABLE `transactions_date_wise_summary` (
  `id` int(10) UNSIGNED NOT NULL,
  `sl` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'transaction no',
  `branch_id` int(11) DEFAULT NULL,
  `branch_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `trans_date` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `year` int(11) NOT NULL,
  `month` varchar(15) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_by` int(10) UNSIGNED NOT NULL,
  `updated_by` int(10) UNSIGNED DEFAULT NULL,
  `deleted_by` int(10) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `transactions_date_wise_summary`
--

INSERT INTO `transactions_date_wise_summary` (`id`, `sl`, `branch_id`, `branch_name`, `trans_date`, `status`, `year`, `month`, `created_by`, `updated_by`, `deleted_by`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'MIR-20180812-1', 2, 'Mirpur', '2018-08-12 06:02:02', '1', 2018, '7', 4, NULL, NULL, '2018-08-12 00:02:02', '2018-08-12 00:02:02', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `transactions_deposit_details`
--

CREATE TABLE `transactions_deposit_details` (
  `id` int(10) UNSIGNED NOT NULL,
  `trans_id` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'Date Wise Receipt head id',
  `trans_sl` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'Date Wise Receipt head Serial Number',
  `branch_id` int(11) DEFAULT NULL,
  `branch_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `trans_date` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `brand_id` int(11) DEFAULT NULL,
  `brand_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `product_id` int(11) DEFAULT NULL,
  `product_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `bank_id` int(11) DEFAULT NULL,
  `bank_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `amount` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'Bank Deposit Amount',
  `status` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `year` int(11) NOT NULL,
  `month` varchar(15) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_by` int(10) UNSIGNED NOT NULL,
  `updated_by` int(10) UNSIGNED DEFAULT NULL,
  `deleted_by` int(10) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `transactions_deposit_details`
--

INSERT INTO `transactions_deposit_details` (`id`, `trans_id`, `trans_sl`, `branch_id`, `branch_name`, `trans_date`, `brand_id`, `brand_name`, `product_id`, `product_name`, `bank_id`, `bank_name`, `amount`, `status`, `year`, `month`, `created_by`, `updated_by`, `deleted_by`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, '1', 'MIR-20180812-1', 2, 'Mirpur', '2018-08-12', 1, 'Pharma', 1, 'SK+F (Pharma)', 1, 'South East Bank Limited', '0', '1', 2018, '7', 4, 4, NULL, '2018-08-12 05:33:18', '2018-08-12 05:33:49', NULL),
(2, '1', 'MIR-20180812-1', 2, 'Mirpur', '2018-08-12', 1, 'Pharma', 1, 'SK+F (Pharma)', 2, 'Eastern Bank Limited', '0', '1', 2018, '7', 4, 4, NULL, '2018-08-12 05:33:18', '2018-08-12 05:33:49', NULL),
(3, '1', 'MIR-20180812-1', 2, 'Mirpur', '2018-08-12', 1, 'Pharma', 1, 'SK+F (Pharma)', 5, 'Prime Bank Limited', '1', '1', 2018, '7', 4, 4, NULL, '2018-08-12 05:33:18', '2018-08-12 05:33:49', NULL),
(4, '1', 'MIR-20180812-1', 2, 'Mirpur', '2018-08-12', 1, 'Pharma', 1, 'SK+F (Pharma)', 6, 'Standard Chartered Bank', '1', '1', 2018, '7', 4, 4, NULL, '2018-08-12 05:33:18', '2018-08-12 05:33:49', NULL),
(5, '1', 'MIR-20180812-1', 2, 'Mirpur', '2018-08-12', 1, 'Pharma', 1, 'SK+F (Pharma)', 8, 'Eastern Bank Limited', '1', '1', 2018, '7', 4, 4, NULL, '2018-08-12 05:33:18', '2018-08-12 05:33:49', NULL),
(6, '1', 'MIR-20180812-1', 2, 'Mirpur', '2018-08-12', 1, 'Pharma', 1, 'SK+F (Pharma)', 19, 'Mutual Trust Bank Limited', '1', '1', 2018, '7', 4, 4, NULL, '2018-08-12 05:33:18', '2018-08-12 05:33:49', NULL),
(7, '1', 'MIR-20180812-1', 2, 'Mirpur', '2018-08-12', 1, 'Pharma', 2, 'SK+F (AHD)', 1, 'South East Bank Limited', '1', '1', 2018, '7', 4, 4, NULL, '2018-08-12 05:33:18', '2018-08-12 05:33:49', NULL),
(8, '1', 'MIR-20180812-1', 2, 'Mirpur', '2018-08-12', 1, 'Pharma', 2, 'SK+F (AHD)', 2, 'Eastern Bank Limited', '5', '1', 2018, '7', 4, 4, NULL, '2018-08-12 05:33:18', '2018-08-12 05:33:49', NULL),
(9, '1', 'MIR-20180812-1', 2, 'Mirpur', '2018-08-12', 1, 'Pharma', 2, 'SK+F (AHD)', 5, 'Prime Bank Limited', '1', '1', 2018, '7', 4, 4, NULL, '2018-08-12 05:33:18', '2018-08-12 05:33:49', NULL),
(10, '1', 'MIR-20180812-1', 2, 'Mirpur', '2018-08-12', 1, 'Pharma', 2, 'SK+F (AHD)', 6, 'Standard Chartered Bank', '1', '1', 2018, '7', 4, 4, NULL, '2018-08-12 05:33:18', '2018-08-12 05:33:49', NULL),
(11, '1', 'MIR-20180812-1', 2, 'Mirpur', '2018-08-12', 1, 'Pharma', 2, 'SK+F (AHD)', 8, 'Eastern Bank Limited', '1', '1', 2018, '7', 4, 4, NULL, '2018-08-12 05:33:18', '2018-08-12 05:33:49', NULL),
(12, '1', 'MIR-20180812-1', 2, 'Mirpur', '2018-08-12', 1, 'Pharma', 2, 'SK+F (AHD)', 19, 'Mutual Trust Bank Limited', '1', '1', 2018, '7', 4, 4, NULL, '2018-08-12 05:33:18', '2018-08-12 05:33:49', NULL),
(13, '1', 'MIR-20180812-1', 2, 'Mirpur', '2018-08-12', 2, 'CBD', 15, 'CAL X   (SK+F CBD)', 1, 'South East Bank Limited', '5', '1', 2018, '7', 4, 4, NULL, '2018-08-12 05:33:18', '2018-08-12 05:33:50', NULL),
(14, '1', 'MIR-20180812-1', 2, 'Mirpur', '2018-08-12', 2, 'CBD', 15, 'CAL X   (SK+F CBD)', 2, 'Eastern Bank Limited', '5', '1', 2018, '7', 4, 4, NULL, '2018-08-12 05:33:18', '2018-08-12 05:33:50', NULL),
(15, '1', 'MIR-20180812-1', 2, 'Mirpur', '2018-08-12', 2, 'CBD', 15, 'CAL X   (SK+F CBD)', 5, 'Prime Bank Limited', '5', '1', 2018, '7', 4, 4, NULL, '2018-08-12 05:33:18', '2018-08-12 05:33:50', NULL),
(16, '1', 'MIR-20180812-1', 2, 'Mirpur', '2018-08-12', 2, 'CBD', 15, 'CAL X   (SK+F CBD)', 6, 'Standard Chartered Bank', '5', '1', 2018, '7', 4, 4, NULL, '2018-08-12 05:33:18', '2018-08-12 05:33:50', NULL),
(17, '1', 'MIR-20180812-1', 2, 'Mirpur', '2018-08-12', 2, 'CBD', 15, 'CAL X   (SK+F CBD)', 8, 'Eastern Bank Limited', '5', '1', 2018, '7', 4, 4, NULL, '2018-08-12 05:33:18', '2018-08-12 05:33:50', NULL),
(18, '1', 'MIR-20180812-1', 2, 'Mirpur', '2018-08-12', 2, 'CBD', 15, 'CAL X   (SK+F CBD)', 19, 'Mutual Trust Bank Limited', '5', '1', 2018, '7', 4, 4, NULL, '2018-08-12 05:33:18', '2018-08-12 05:33:50', NULL),
(19, '1', 'MIR-20180812-1', 2, 'Mirpur', '2018-08-12', 2, 'CBD', 17, 'L\'OREAL', 1, 'South East Bank Limited', '1', '1', 2018, '7', 4, 4, NULL, '2018-08-12 05:33:18', '2018-08-12 05:33:50', NULL),
(20, '1', 'MIR-20180812-1', 2, 'Mirpur', '2018-08-12', 2, 'CBD', 17, 'L\'OREAL', 2, 'Eastern Bank Limited', '1', '1', 2018, '7', 4, 4, NULL, '2018-08-12 05:33:18', '2018-08-12 05:33:50', NULL),
(21, '1', 'MIR-20180812-1', 2, 'Mirpur', '2018-08-12', 2, 'CBD', 17, 'L\'OREAL', 5, 'Prime Bank Limited', '1', '1', 2018, '7', 4, 4, NULL, '2018-08-12 05:33:18', '2018-08-12 05:33:50', NULL),
(22, '1', 'MIR-20180812-1', 2, 'Mirpur', '2018-08-12', 2, 'CBD', 17, 'L\'OREAL', 6, 'Standard Chartered Bank', '1', '1', 2018, '7', 4, 4, NULL, '2018-08-12 05:33:18', '2018-08-12 05:33:50', NULL),
(23, '1', 'MIR-20180812-1', 2, 'Mirpur', '2018-08-12', 2, 'CBD', 17, 'L\'OREAL', 8, 'Eastern Bank Limited', '1', '1', 2018, '7', 4, 4, NULL, '2018-08-12 05:33:18', '2018-08-12 05:33:50', NULL),
(24, '1', 'MIR-20180812-1', 2, 'Mirpur', '2018-08-12', 2, 'CBD', 17, 'L\'OREAL', 19, 'Mutual Trust Bank Limited', '1', '1', 2018, '7', 4, 4, NULL, '2018-08-12 05:33:18', '2018-08-12 05:33:50', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `transactions_deposit_summary`
--

CREATE TABLE `transactions_deposit_summary` (
  `id` int(10) UNSIGNED NOT NULL,
  `sl` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'transaction no',
  `branch_id` int(11) DEFAULT NULL,
  `branch_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `trans_date` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `year` int(11) NOT NULL,
  `month` varchar(15) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_by` int(10) UNSIGNED NOT NULL,
  `updated_by` int(10) UNSIGNED DEFAULT NULL,
  `deleted_by` int(10) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `transactions_deposit_summary`
--

INSERT INTO `transactions_deposit_summary` (`id`, `sl`, `branch_id`, `branch_name`, `trans_date`, `status`, `year`, `month`, `created_by`, `updated_by`, `deleted_by`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'MIR-20180812-1', 2, 'Mirpur', '2018-08-12 11:33:18', '1', 2018, '7', 4, NULL, NULL, '2018-08-12 05:33:18', '2018-08-12 05:33:18', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `transactions_petty_cash_details`
--

CREATE TABLE `transactions_petty_cash_details` (
  `id` int(10) UNSIGNED NOT NULL,
  `trans_id` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'Date Wise Receipt head id',
  `trans_sl` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'Date Wise Receipt head Serial Number',
  `branch_id` int(11) DEFAULT NULL,
  `branch_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `trans_date` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `total_receipt_hq` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'Total Receipt From HQ',
  `transfer_imprest_cash` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'Transfer to Imprest Cash',
  `m_receipt` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'Miss. Receipt',
  `total_expense_cq` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'Total Expenses Through CQ',
  `total_exp` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'Total Expenses',
  `optional_field1` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `optional_field2` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `optional_field3` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `year` int(11) NOT NULL,
  `month` varchar(15) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_by` int(10) UNSIGNED NOT NULL,
  `updated_by` int(10) UNSIGNED DEFAULT NULL,
  `deleted_by` int(10) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `transactions_petty_cash_details`
--

INSERT INTO `transactions_petty_cash_details` (`id`, `trans_id`, `trans_sl`, `branch_id`, `branch_name`, `trans_date`, `total_receipt_hq`, `transfer_imprest_cash`, `m_receipt`, `total_expense_cq`, `total_exp`, `optional_field1`, `optional_field2`, `optional_field3`, `status`, `year`, `month`, `created_by`, `updated_by`, `deleted_by`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, '1', 'MIR-20180812-1', 2, 'Mirpur', '2018-08-12', '12312', '1231', '123', '12', '1', NULL, NULL, NULL, '1', 2018, '7', 4, 4, NULL, '2018-08-12 03:39:47', '2018-08-12 04:27:23', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `transactions_petty_cash_summary`
--

CREATE TABLE `transactions_petty_cash_summary` (
  `id` int(10) UNSIGNED NOT NULL,
  `sl` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'transaction no',
  `branch_id` int(11) DEFAULT NULL,
  `branch_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `trans_date` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `year` int(11) NOT NULL,
  `month` varchar(15) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_by` int(10) UNSIGNED NOT NULL,
  `updated_by` int(10) UNSIGNED DEFAULT NULL,
  `deleted_by` int(10) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `transactions_petty_cash_summary`
--

INSERT INTO `transactions_petty_cash_summary` (`id`, `sl`, `branch_id`, `branch_name`, `trans_date`, `status`, `year`, `month`, `created_by`, `updated_by`, `deleted_by`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'MIR-20180812-1', 2, 'Mirpur', '2018-08-12 09:39:47', '1', 2018, '7', 4, NULL, NULL, '2018-08-12 03:39:47', '2018-08-12 03:39:47', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `first_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `last_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `username` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` tinyint(3) UNSIGNED NOT NULL DEFAULT '1',
  `confirmation_code` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `api_access` tinyint(3) UNSIGNED NOT NULL DEFAULT '1',
  `confirmed` tinyint(1) NOT NULL DEFAULT '0',
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `first_name`, `last_name`, `username`, `email`, `password`, `status`, `confirmation_code`, `api_access`, `confirmed`, `remember_token`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'Admin', 'Istrator', 'admin', 'admin@admin.com', '$2y$10$2aVNLi2OLEbdVRtwga3Fzu3nfjrDDJOnrYAjyJIJE5DMK2x96XVsq', 1, '8abf758f471f3fb0273e641997766c9f', 1, 1, '57FuXsQWUOBzCjaJkQUcqjp0JTh175Vld7dbIkiYl1KZwqBmc3oO5l0VwYib', '2018-07-31 02:38:35', '2018-07-31 22:18:06', NULL),
(2, 'Backend', 'User', '88888888', 'executive@executive.com', '$2y$10$JRMxV7KJD9n9MExPHOoNoOO04mP5Yyo1NbldmdeR5RjH1gfBFblTW', 1, '0aa324601da89de51a7a2eed4414ca8a', 1, 1, 'QXJ9oUsivtpDhjEKTCSuMqnocEjeiNU1nBKCzaZmKrsa7uxIQAemIyrUAFvy', '2018-07-31 02:38:35', '2018-07-31 02:38:35', NULL),
(3, 'Default', 'User', '77777777', 'user@user.com', '$2y$10$e...cJVX2v6cKg3kSb5WeeISsuFm05XiacG/M/Uzm366hMNcSHeeG', 1, '4317ea3c38c5871158475bb24aac6d75', 1, 1, NULL, '2018-07-31 02:38:35', '2018-07-31 02:38:35', NULL),
(4, 'Md. Aftab', 'Uddin', 'aftab', 'aftab.uddin@transcombd.com', '$2y$10$UQZsOr8xbjS2f4Js4SRzn.ij4adThg3BqXUIKFdZOWAGOvz5cUb22', 1, '3bd3679f212e1c507b9e003101a49891', 1, 1, 'GllcwT2uAN1bTbV32qGZhm3vFnqLSPkZwo7qVfRTovQ9cvVVt133eaPhL1Gv', '2018-08-01 02:27:48', '2018-08-01 02:27:48', NULL),
(6, 'Estiak Md Almas', 'Suzan', 'almas', 'almas.suzan@transcombd.com', '$2y$10$Yunr1SCeJT6Iy1Vi7rPBiew8/UfN..3loDbAGihqOxJht8XchsTDe', 1, '5e5364e4dcbe26aa181edc217564aec1', 1, 1, NULL, '2018-08-01 02:46:07', '2018-08-01 02:46:07', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `user_branch`
--

CREATE TABLE `user_branch` (
  `id` int(10) UNSIGNED NOT NULL,
  `branch_id` int(11) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `user_branch`
--

INSERT INTO `user_branch` (`id`, `branch_id`, `user_id`) VALUES
(13, 2, 2),
(16, 1, 6),
(17, 2, 6),
(18, 3, 6),
(19, 4, 6),
(20, 5, 6),
(21, 6, 6),
(22, 7, 6),
(23, 8, 6),
(24, 9, 6),
(25, 10, 6),
(26, 1, 1),
(27, 2, 1),
(28, 2, 4);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `banks`
--
ALTER TABLE `banks`
  ADD PRIMARY KEY (`id`),
  ADD KEY `banks_created_by_index` (`created_by`);

--
-- Indexes for table `branches`
--
ALTER TABLE `branches`
  ADD PRIMARY KEY (`id`),
  ADD KEY `branches_created_by_index` (`created_by`);

--
-- Indexes for table `branch_bank`
--
ALTER TABLE `branch_bank`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `brands`
--
ALTER TABLE `brands`
  ADD PRIMARY KEY (`id`),
  ADD KEY `brands_created_by_index` (`created_by`);

--
-- Indexes for table `employees`
--
ALTER TABLE `employees`
  ADD PRIMARY KEY (`id`),
  ADD KEY `employees_created_by_index` (`created_by`);

--
-- Indexes for table `history`
--
ALTER TABLE `history`
  ADD PRIMARY KEY (`id`),
  ADD KEY `history_type_id_foreign` (`type_id`),
  ADD KEY `history_user_id_foreign` (`user_id`);

--
-- Indexes for table `history_types`
--
ALTER TABLE `history_types`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `permissions`
--
ALTER TABLE `permissions`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `permissions_name_unique` (`name`);

--
-- Indexes for table `permission_role`
--
ALTER TABLE `permission_role`
  ADD PRIMARY KEY (`id`),
  ADD KEY `permission_role_permission_id_foreign` (`permission_id`),
  ADD KEY `permission_role_role_id_foreign` (`role_id`);

--
-- Indexes for table `products`
--
ALTER TABLE `products`
  ADD PRIMARY KEY (`id`),
  ADD KEY `products_created_by_index` (`created_by`);

--
-- Indexes for table `product_branch`
--
ALTER TABLE `product_branch`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `roles_name_unique` (`name`);

--
-- Indexes for table `role_user`
--
ALTER TABLE `role_user`
  ADD PRIMARY KEY (`id`),
  ADD KEY `role_user_user_id_foreign` (`user_id`),
  ADD KEY `role_user_role_id_foreign` (`role_id`);

--
-- Indexes for table `sessions`
--
ALTER TABLE `sessions`
  ADD UNIQUE KEY `sessions_id_unique` (`id`);

--
-- Indexes for table `social_logins`
--
ALTER TABLE `social_logins`
  ADD PRIMARY KEY (`id`),
  ADD KEY `social_logins_user_id_foreign` (`user_id`);

--
-- Indexes for table `transactions_bank_charge_details`
--
ALTER TABLE `transactions_bank_charge_details`
  ADD PRIMARY KEY (`id`),
  ADD KEY `transactions_bank_charge_details_created_by_index` (`created_by`);

--
-- Indexes for table `transactions_bank_charge_summary`
--
ALTER TABLE `transactions_bank_charge_summary`
  ADD PRIMARY KEY (`id`),
  ADD KEY `transactions_bank_charges_summary_created_by_index` (`created_by`);

--
-- Indexes for table `transactions_date_wise_details`
--
ALTER TABLE `transactions_date_wise_details`
  ADD PRIMARY KEY (`id`),
  ADD KEY `transactions_date_wise_details_created_by_index` (`created_by`);

--
-- Indexes for table `transactions_date_wise_summary`
--
ALTER TABLE `transactions_date_wise_summary`
  ADD PRIMARY KEY (`id`),
  ADD KEY `transactions_date_wise_summary_created_by_index` (`created_by`);

--
-- Indexes for table `transactions_deposit_details`
--
ALTER TABLE `transactions_deposit_details`
  ADD PRIMARY KEY (`id`),
  ADD KEY `transactions_deposit_details_created_by_index` (`created_by`);

--
-- Indexes for table `transactions_deposit_summary`
--
ALTER TABLE `transactions_deposit_summary`
  ADD PRIMARY KEY (`id`),
  ADD KEY `transactions_deposit_summary_created_by_index` (`created_by`);

--
-- Indexes for table `transactions_petty_cash_details`
--
ALTER TABLE `transactions_petty_cash_details`
  ADD PRIMARY KEY (`id`),
  ADD KEY `transactions_petty_cash_details_created_by_index` (`created_by`);

--
-- Indexes for table `transactions_petty_cash_summary`
--
ALTER TABLE `transactions_petty_cash_summary`
  ADD PRIMARY KEY (`id`),
  ADD KEY `transactions_petty_cash_summary_created_by_index` (`created_by`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`),
  ADD UNIQUE KEY `users_id_no_unique` (`username`);

--
-- Indexes for table `user_branch`
--
ALTER TABLE `user_branch`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `banks`
--
ALTER TABLE `banks`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;
--
-- AUTO_INCREMENT for table `branches`
--
ALTER TABLE `branches`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=33;
--
-- AUTO_INCREMENT for table `branch_bank`
--
ALTER TABLE `branch_bank`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;
--
-- AUTO_INCREMENT for table `brands`
--
ALTER TABLE `brands`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `employees`
--
ALTER TABLE `employees`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `history`
--
ALTER TABLE `history`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;
--
-- AUTO_INCREMENT for table `history_types`
--
ALTER TABLE `history_types`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=28;
--
-- AUTO_INCREMENT for table `permissions`
--
ALTER TABLE `permissions`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `permission_role`
--
ALTER TABLE `permission_role`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `products`
--
ALTER TABLE `products`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;
--
-- AUTO_INCREMENT for table `product_branch`
--
ALTER TABLE `product_branch`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;
--
-- AUTO_INCREMENT for table `roles`
--
ALTER TABLE `roles`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `role_user`
--
ALTER TABLE `role_user`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=25;
--
-- AUTO_INCREMENT for table `social_logins`
--
ALTER TABLE `social_logins`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `transactions_bank_charge_details`
--
ALTER TABLE `transactions_bank_charge_details`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `transactions_bank_charge_summary`
--
ALTER TABLE `transactions_bank_charge_summary`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `transactions_date_wise_details`
--
ALTER TABLE `transactions_date_wise_details`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `transactions_date_wise_summary`
--
ALTER TABLE `transactions_date_wise_summary`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `transactions_deposit_details`
--
ALTER TABLE `transactions_deposit_details`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=25;
--
-- AUTO_INCREMENT for table `transactions_deposit_summary`
--
ALTER TABLE `transactions_deposit_summary`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `transactions_petty_cash_details`
--
ALTER TABLE `transactions_petty_cash_details`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `transactions_petty_cash_summary`
--
ALTER TABLE `transactions_petty_cash_summary`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `user_branch`
--
ALTER TABLE `user_branch`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=29;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `history`
--
ALTER TABLE `history`
  ADD CONSTRAINT `history_type_id_foreign` FOREIGN KEY (`type_id`) REFERENCES `history_types` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `history_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `permission_role`
--
ALTER TABLE `permission_role`
  ADD CONSTRAINT `permission_role_permission_id_foreign` FOREIGN KEY (`permission_id`) REFERENCES `permissions` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `permission_role_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `role_user`
--
ALTER TABLE `role_user`
  ADD CONSTRAINT `role_user_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `role_user_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `social_logins`
--
ALTER TABLE `social_logins`
  ADD CONSTRAINT `social_logins_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
