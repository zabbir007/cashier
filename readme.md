## CASHier (Current: Laravel 6.0)

[![Latest Stable Version](https://poser.pugx.org/rappasoft/laravel-boilerplate/v/stable)](https://packagist.org/packages/rappasoft/laravel-boilerplate)
[![Latest Unstable Version](https://poser.pugx.org/rappasoft/laravel-boilerplate/v/unstable)](https://packagist.org/packages/rappasoft/laravel-boilerplate) 
<br/>
[![StyleCI](https://styleci.io/repos/30171828/shield?style=plastic)](https://styleci.io/repos/30171828/shield?style=plastic)
[![CircleCI](https://circleci.com/gh/rappasoft/laravel-boilerplate/tree/master.svg?style=svg)](https://circleci.com/gh/rappasoft/laravel-boilerplate/tree/master)
<br/>

### Security Vulnerabilities

If you discover a security vulnerability within this boilerplate, please send an e-mail to Almas MD. Estiak at almas.estiak@transcombd.com, or create a pull request if possible. All security vulnerabilities will be promptly addressed.

### License

MIT: [http://mit-license.org](http://mit-license.org)
