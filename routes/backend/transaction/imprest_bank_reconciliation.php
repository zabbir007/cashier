<?php

Route::group([
    'prefix' => 'transaction',
    'as' => 'transaction.',
    'namespace' => 'Transaction',
], function () {
    Route::group([
//        'middleware' => 'access.routeNeedsPermission:menu-transaction-outstanding-reconciliation-index;menu-transaction-outstanding-reconciliation-create;menu-transaction-outstanding-reconciliation-store;menu-transaction-outstanding-reconciliation-show;menu-transaction-outstanding-reconciliation-edit;menu-transaction-outstanding-reconciliation-update;menu-transaction-outstanding-reconciliation-delete;menu-transaction-outstanding-reconciliation-destroy;menu-transaction-outstanding-reconciliation-restore;menu-transaction-outstanding-reconciliation-mark;menu-transaction-outstanding-reconciliation-view-deactivated;menu-transaction-outstanding-reconciliation-view-deleted;',
        'namespace' => 'ImprestBankReconciliation'
    ], function () {
        /*
          * For DataTables
          */
        Route::post('imprest_bank_reconciliations/get', 'ImprestBankReconciliationTableController')->name('imprest_bank_reconciliations.get');


        Route::get('imprest_bank_reconciliations/{outstandingreconciliation}/approve', 'ImprestBankReconciliationController@approve')->name('imprest_bank_reconciliation.approve');

        Route::patch('imprest_bank_reconciliations/{outstandingreconciliation}/approve', 'ImprestBankReconciliationController@approve')->name('imprest_bank_reconciliation.approve');

        Route::match(['get','post'],'imprest_bank_reconciliations/generate', 'ImprestBankReconciliationController@generate')->name('imprest_bank_reconciliations.generate');

        Route::get('imprest_bank_reconciliations/get_account_number', 'ImprestBankReconciliationController@getAccountNumber')->name('imprest_bank_reconciliations.get_account_number');
        Route::get('imprest_bank_reconciliations/check_entry', 'ImprestBankReconciliationController@checkPreviousEntry')->name('imprest_bank_reconciliations.check_entry');

        /*
         * Route CRUD
         */
        Route::resource('imprest_bank_reconciliations', 'ImprestBankReconciliationController');
    });

});
