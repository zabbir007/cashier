<?php

Route::group([
    'prefix' => 'transaction',
    'as' => 'transaction.',
    'namespace' => 'Transaction',
], function () {
    Route::group([
        'middleware' => 'access.routeNeedsPermission:menu-transaction-bank-charge-index;menu-transaction-bank-charge-create;menu-transaction-bank-charge-store;menu-transaction-bank-charge-show;menu-transaction-bank-charge-edit;menu-transaction-bank-charge-update;menu-transaction-bank-charge-delete;menu-transaction-bank-charge-destroy;menu-transaction-bank-charge-restore;menu-transaction-bank-charge-mark;menu-transaction-bank-charge-view-deactivated;menu-transaction-bank-charge-view-deleted;',
        'namespace' => 'BankCharge'
    ], function () {

        /*
         * For DataTables
         */
        Route::post('bankcharges/get', 'BankChargeTableController')->name('bankcharges.get');

        Route::get('bankcharges/deactivated', 'BankChargeController@getDeactivated')->name('bankcharges.deactivated');
        Route::get('bankcharges/deleted', 'BankChargeController@getDeleted')->name('bankcharges.deleted');

        Route::get('bankcharges/{bankcharge}/approve', 'BankChargeController@approve')->name('bankcharges.approve');
        Route::patch('bankcharges/{bankcharge}/approve', 'BankChargeController@approve')->name('bankcharges.approve');
        /*
         * Route CRUD
         */
        Route::resource('bankcharges', 'BankChargeController');

        /*
         * Specific Route
         */
        Route::group(['prefix' => 'bankcharges/{bankcharge}'], function () {

            // Status
            Route::get('mark/{status}', 'BankChargeController@mark')->name('bankcharges.mark')->where(['status' => '[1,2]']);
        });

        /*
         * Deleted Route
         */
        Route::group(['prefix' => 'bankcharges/{deletedBankCharge}'], function () {
            Route::get('delete', 'BankChargeController@delete')->name('bankcharges.delete-permanently');
            Route::get('restore', 'BankChargeController@restore')->name('bankcharges.restore');
        });

    });
});
