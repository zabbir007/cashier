<?php

Route::group([
    'prefix'     => 'transaction',
    'as'         => 'transaction.',
    'namespace'  => 'Transaction',
], function () {
    Route::group([
        'middleware' => 'access.routeNeedsPermission:menu-transaction-outstanding-reconciliation-index;menu-transaction-outstanding-reconciliation-create;menu-transaction-outstanding-reconciliation-store;menu-transaction-outstanding-reconciliation-show;menu-transaction-outstanding-reconciliation-edit;menu-transaction-outstanding-reconciliation-update;menu-transaction-outstanding-reconciliation-delete;menu-transaction-outstanding-reconciliation-destroy;menu-transaction-outstanding-reconciliation-restore;menu-transaction-outstanding-reconciliation-mark;menu-transaction-outstanding-reconciliation-view-deactivated;menu-transaction-outstanding-reconciliation-view-deleted;',
        'namespace' => 'OutstandingReconciliation'
    ], function () {
        /*
          * For DataTables
          */
        Route::post('outstandingreconciliations/get', 'OutstandingReconciliationTableController')->name('outstandingreconciliations.get');

            Route::get('outstandingreconciliations/deactivated', 'OutstandingReconciliationStatusController@getDeactivated')->name('outstandingreconciliations.deactivated');

            Route::get('outstandingreconciliations/deleted', 'OutstandingReconciliationStatusController@getDeleted')->name('outstandingreconciliations.deleted');

            Route::get('outstandingreconciliations/{outstandingreconciliation}/approve', 'OutstandingReconciliationController@approve')->name('outstandingreconciliations.approve');

            Route::patch('outstandingreconciliations/{outstandingreconciliation}/approve', 'OutstandingReconciliationController@approve')->name('outstandingreconciliations.approve');
            /*
             * Route CRUD
             */
            Route::resource('outstandingreconciliations', 'OutstandingReconciliationController');

            /*
             * Specific Route
             */
            Route::group(['prefix' => 'outstandingreconciliations/{outstandingreconciliation}'], function () {

                // Status
                Route::get('mark/{status}', 'OutstandingReconciliationStatusController@mark')->name('outstandingreconciliations.mark')->where(['status' => '[1,2]']);
            });

            /*
             * Deleted Route
             */
            Route::group(['prefix' => 'outstandingreconciliations/{deletedOutstandingReconciliation}'], function () {
                Route::get('delete', 'OutstandingReconciliationStatusController@delete')->name('outstandingreconciliations.delete-permanently');

                Route::get('restore', 'OutstandingReconciliationStatusController@restore')->name('outstandingreconciliations.restore');
            });
        });

});
