<?php

Route::group([
    'prefix' => 'transaction',
    'as' => 'transaction.',
    'namespace' => 'Transaction',
], function () {
    Route::group([
        'middleware' => 'access.routeNeedsPermission:menu-transaction-deposits-index;menu-transaction-deposits-create;menu-transaction-deposits-store;menu-transaction-deposits-show;menu-transaction-deposits-edit;menu-transaction-deposits-update;menu-transaction-deposits-delete;menu-transaction-deposits-destroy;menu-transaction-deposits-restore;menu-transaction-deposits-mark;menu-transaction-deposits-view-deactivated;menu-transaction-deposits-view-deleted;',
        'namespace' => 'Deposit'
    ], function () {
        /*
 * For DataTables
 */
        Route::post('deposits/get', 'DepositTableController')->name('deposits.get');

//        Route::get('deposits/fix', 'DepositController@fix');
        Route::get('deposits/deactivated', 'DepositStatusController@getDeactivated')->name('deposits.deactivated');
        Route::get('deposits/deleted', 'DepositStatusController@getDeleted')->name('deposits.deleted');
        Route::get('deposits/{deposit}/approve', 'DepositController@approve')->name('deposits.approve');
        Route::patch('deposits/{deposit}/approve', 'DepositController@approve')->name('deposits.approve');
        /*
         * Route CRUD
         */
        Route::resource('deposits', 'DepositController');

        /*
         * Specific Route
         */
        Route::group(['prefix' => 'deposits/{deposit}'], function () {

            // Status
            Route::get('mark/{status}', 'DepositStatusController@mark')->name('deposits.mark')->where(['status' => '[1,2]']);
        });

        /*
         * Deleted Route
         */
        Route::group(['prefix' => 'deposits/{deletedDeposit}'], function () {
            Route::get('delete', 'DepositStatusController@delete')->name('deposits.delete-permanently');
            Route::get('restore', 'DepositStatusController@restore')->name('deposits.restore');
        });

    });
});
