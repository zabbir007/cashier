<?php

Route::group([
    'prefix' => 'transaction',
    'as' => 'transaction.',
    'namespace' => 'Transaction',
], function () {
    Route::group([
//        'middleware' => 'access.routeNeedsPermission:menu-transaction-outstanding-reconciliation-index;menu-transaction-outstanding-reconciliation-create;menu-transaction-outstanding-reconciliation-store;menu-transaction-outstanding-reconciliation-show;menu-transaction-outstanding-reconciliation-edit;menu-transaction-outstanding-reconciliation-update;menu-transaction-outstanding-reconciliation-delete;menu-transaction-outstanding-reconciliation-destroy;menu-transaction-outstanding-reconciliation-restore;menu-transaction-outstanding-reconciliation-mark;menu-transaction-outstanding-reconciliation-view-deactivated;menu-transaction-outstanding-reconciliation-view-deleted;',
        'namespace' => 'ImprestCorporateReconciliation'
    ], function () {
        /*
          * For DataTables
          */
        Route::post('imprest_corporate_reconciliations/get', 'ImprestCorporateReconciliationTableController')->name('imprest_corporate_reconciliations.get');


        Route::get('imprest_corporate_reconciliations/{outstandingreconciliation}/approve', 'ImprestCorporateReconciliationController@approve')->name('imprest_corporate_reconciliation.approve');

        Route::patch('imprest_corporate_reconciliations/{outstandingreconciliation}/approve', 'ImprestCorporateReconciliationController@approve')->name('imprest_corporate_reconciliation.approve');

        Route::match(['get','post'],'imprest_corporate_reconciliations/generate', 'ImprestCorporateReconciliationController@generate')->name('imprest_corporate_reconciliations.generate');

        Route::get('imprest_corporate_reconciliations/get_account_number', 'ImprestCorporateReconciliationController@getAccountNumber')->name('imprest_corporate_reconciliations.get_account_number');
        Route::get('imprest_corporate_reconciliations/check_entry', 'ImprestCorporateReconciliationController@checkPreviousEntry')->name('imprest_corporate_reconciliations.check_entry');
        /*
         * Route CRUD
         */
        Route::resource('imprest_corporate_reconciliations', 'ImprestCorporateReconciliationController');
    });

});
