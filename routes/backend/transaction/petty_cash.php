<?php

Route::group([
    'prefix'     => 'transaction',
    'as'         => 'transaction.',
    'namespace'  => 'Transaction',
], function () {
    Route::group([
        'middleware' => 'access.routeNeedsPermission:menu-transaction-petty-cash-index;menu-transaction-petty-cash-create;menu-transaction-petty-cash-store;menu-transaction-petty-cash-show;menu-transaction-petty-cash-edit;menu-transaction-petty-cash-update;menu-transaction-petty-cash-delete;menu-transaction-petty-cash-destroy;menu-transaction-petty-cash-restore;menu-transaction-petty-cash-mark;menu-transaction-petty-cash-view-deactivated;menu-transaction-petty-cash-view-deleted;',
        'namespace' => 'PettyCash'
    ], function () {
        /*
                  * For DataTables
                  */
        Route::post('pettycashes/get', 'PettyCashTableController')->name('pettycashes.get');


            Route::get('pettycashes/deactivated', 'PettyCashStatusController@getDeactivated')->name('pettycashes.deactivated');
            Route::get('pettycashes/deleted', 'PettyCashStatusController@getDeleted')->name('pettycashes.deleted');
            Route::get('pettycashes/{pettycash}/approve', 'PettyCashController@approve')->name('pettycashes.approve');
            Route::patch('pettycashes/{pettycash}/approve', 'PettyCashController@approve')->name('pettycashes.approve');
            /*
             * Route CRUD
             */
            Route::resource('pettycashes', 'PettyCashController');

            /*
             * Specific Route
             */
            Route::group(['prefix' => 'pettycashes/{pettycash}'], function () {

                // Status
                Route::get('mark/{status}', 'PettyCashStatusController@mark')->name('pettycashes.mark')->where(['status' => '[1,2]']);
            });

            /*
             * Deleted Route
             */
            Route::group(['prefix' => 'pettycashes/{deletedPettyCash}'], function () {
                Route::get('delete', 'PettyCashStatusController@delete')->name('pettycashes.delete-permanently');
                Route::get('restore', 'PettyCashStatusController@restore')->name('pettycashes.restore');
            });

    });
});
