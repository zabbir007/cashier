<?php

Route::group([
    'prefix'     => 'transaction',
    'as'         => 'transaction.',
    'namespace'  => 'Transaction',
], function () {
    Route::group([
        'middleware' => 'access.routeNeedsPermission:menu-transaction-date-wise-receipt-index;menu-transaction-date-wise-receipt-create;menu-transaction-date-wise-receipt-store;menu-transaction-date-wise-receipt-show;menu-transaction-date-wise-receipt-edit;menu-transaction-date-wise-receipt-update;menu-transaction-date-wise-receipt-delete;menu-transaction-date-wise-receipt-destroy;menu-transaction-date-wise-receipt-restore;menu-transaction-date-wise-receipt-mark;menu-transaction-date-wise-receipt-view-deactivated;menu-transaction-date-wise-receipt-view-deleted;',
        'namespace' => 'DateWise'
    ], function () {

        /*
            * For DataTables
            */
        Route::post('datewises/get', 'DateWiseTableController')->name('datewises.get');


        Route::get('datewises/deactivated', 'DateWiseStatusController@getDeactivated')->name('datewises.deactivated');
            Route::get('datewises/deleted', 'DateWiseStatusController@getDeleted')->name('datewises.deleted');
            Route::get('datewises/{datewise}/approve', 'DateWiseController@approve')->name('datewises.approve');
            Route::patch('datewises/{datewise}/approve', 'DateWiseController@approve')->name('datewises.approve');
//        Route::get('datewises/fix', 'DateWiseController@fix');

            /*
             * Route CRUD
             */
            Route::resource('datewises', 'DateWiseController');

            /*
             * Specific Route
             */
            Route::group(['prefix' => 'datewises/{datewise}'], function () {

                // Status
                Route::get('mark/{status}', 'DateWiseStatusController@mark')->name('datewises.mark')->where(['status' => '[1,2]']);
            });

            /*
             * Deleted Route
             */
            Route::group(['prefix' => 'datewises/{deletedDateWise}'], function () {
                Route::get('delete', 'DateWiseStatusController@delete')->name('datewises.delete-permanently');
                Route::get('restore', 'DateWiseStatusController@restore')->name('datewises.restore');
            });

    });
});
