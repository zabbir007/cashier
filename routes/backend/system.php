<?php

/**
 * All route names are prefixed with 'admin.system'.
 */

use App\Http\Controllers\Backend\System\LoggedUser\LoggedUserController;
use App\Http\Controllers\Backend\System\SystemCommandController;

Route::group([
    'prefix' => 'system',
    'as' => 'system.',
    'namespace' => 'System',
//    'middleware' => 'access.routeNeedsRole:1,4',
], function () {
    /*
    * System Management
    */
    Route::group(['namespace' => 'Activity', 'as' => 'activity_log.', 'prefix' => 'activity_log'], function () {
        Route::get('/', 'ActivityController@index')->name('index');
        Route::get('{activity_log}', 'ActivityController@show')->name('show');
    });

    Route::group(['namespace' => 'TransactionSetting'], function () {
        /*
       * For DataTables
       */
        Route::post('transaction_settings/get', 'TransactionSettingTableController')->name('transaction_settings.get');

        Route::resource('transaction_settings', 'TransactionSettingController');
    });

    Route::group(['namespace' => 'LoggedUser'], function () {
        /*
    * For DataTables
    */
        Route::post('logged_users/get', 'LoggedUserTableController')->name('logged_users.get');

        Route::get('logged_users', [LoggedUserController::class, 'index'])->name('logged_users.index');
    });

    Route::get('clear-all', [SystemCommandController::class, 'clearAll'])->name('command.clear-all');
    Route::get('clear-cache', [SystemCommandController::class, 'clearCache'])->name('command.clear-cache');
    Route::get('clear-routes', [SystemCommandController::class, 'clearRoutes'])->name('command.clear-routes');
    Route::get('clear-views', [SystemCommandController::class, 'clearViews'])->name('command.clear-views');
    Route::get('clear-configs', [SystemCommandController::class, 'clearConfigs'])->name('command.clear-configs');
    Route::get('clear-debugbar', [SystemCommandController::class, 'clearDebugbar'])->name('command.clear-debugbar');

    Route::get('cache-all', [SystemCommandController::class, 'cacheAll'])->name('command.cache-all');
    Route::get('cache-configs', [SystemCommandController::class, 'cacheConfigs'])->name('command.cache-configs');
    Route::get('cache-route', [SystemCommandController::class, 'cacheRoutes'])->name('command.cache-routes');
});
