<?php

/*
 * All route names are prefixed with 'admin.'.
 */

use App\Http\Controllers\Backend\DashboardController;

Route::redirect('/', '/admin/dashboard', 301);
Route::get('dashboard', [DashboardController::class, 'index'])->name('dashboard');

Route::get('dashboard/total_summary', [DashboardController::class, 'loadTotalSummary'])->name('dashboard.total_summary');
Route::get('dashboard/segment_summary', [DashboardController::class, 'loadSegmentSummary'])->name('dashboard.segment_summary');
Route::get('dashboard/month_wise_summary', [DashboardController::class, 'loadMonthWiseSummary'])->name('dashboard.month_wise_summary');
