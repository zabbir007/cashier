<?php

Route::group([
    'prefix'     => 'setup',
    'as'         => 'setup.',
    'namespace'  => 'Setup',
], function () {
    Route::group([
        'middleware' => 'access.routeNeedsRole:1;2;4',
    ], function () {
        Route::group(['namespace' => 'Product'], function () {

            Route::get('products/deactivated', 'ProductController@getDeactivated')->name('products.deactivated');
            Route::get('products/deleted', 'ProductController@getDeleted')->name('products.deleted');

            /*
             * Route CRUD
             */
            Route::resource('products', 'ProductController');

            /*
             * Specific Route
             */
            Route::group(['prefix' => 'products/{product}'], function () {

                // Status
                Route::get('mark/{status}', 'ProductController@mark')->name('products.mark')->where(['status' => '[1,2]']);
            });

            /*
             * Deleted Route
             */
            Route::group(['prefix' => 'products/{deletedProduct}'], function () {
                Route::get('delete', 'ProductController@delete')->name('products.delete-permanently');
                Route::get('restore', 'ProductController@restore')->name('products.restore');
            });
        });
    });
});
