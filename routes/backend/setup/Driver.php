<?php

Route::group([
    'prefix' => 'setup',
    'as' => 'setup.',
    'namespace' => 'Setup',
], function () {
    Route::group([
        'middleware' => 'access.routeNeedsRole:1;2;4',
    ], function () {
        Route::group(['namespace' => 'Driver'], function () {

            Route::get('drivers/deactivated', 'DriverController@getDeactivated')->name('drivers.deactivated');
            Route::get('drivers/deleted', 'DriverController@getDeleted')->name('drivers.deleted');

            /*
             * Route CRUD
             */
            Route::resource('drivers', 'DriverController');

            /*
             * Specific Route
             */
            Route::group(['prefix' => 'drivers/{driver}'], function () {

                // Status
                Route::get('mark/{status}', 'DriverController@mark')->name('drivers.mark')->where(['status' => '[1,2]']);
            });

            /*
             * Deleted Route
             */
            Route::group(['prefix' => 'drivers/{deletedBank}'], function () {
                Route::get('delete', 'DriverController@delete')->name('drivers.delete-permanently');
                Route::get('restore', 'DriverController@restore')->name('drivers.restore');
            });
        });

    });
});
