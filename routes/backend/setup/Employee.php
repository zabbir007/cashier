<?php

Route::group([
    'prefix'     => 'setup',
    'as'         => 'setup.',
    'namespace'  => 'Setup',
], function () {
    Route::group([
        'middleware' => 'access.routeNeedsRole:1;2;4',
    ], function () {
        Route::group(['namespace' => 'Employee'], function () {

            Route::get('employees/deactivated', 'EmployeeController@getDeactivated')->name('employees.deactivated');
            Route::get('employees/deleted', 'EmployeeController@getDeleted')->name('employees.deleted');

            /*
             * Route CRUD
             */
            Route::resource('employees', 'EmployeeController');

            /*
             * Specific Route
             */
            Route::group(['prefix' => 'employees/{employee}'], function () {

                // Status
                Route::get('mark/{status}', 'EmployeeController@mark')->name('employees.mark')->where(['status' => '[1,2]']);
            });

            /*
             * Deleted Route
             */
            Route::group(['prefix' => 'employees/{deletedEmployee}'], function () {
                Route::get('delete', 'EmployeeController@delete')->name('employees.delete-permanently');
                Route::get('restore', 'EmployeeController@restore')->name('employees.restore');
            });
        });
    });
});
