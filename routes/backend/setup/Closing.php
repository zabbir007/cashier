<?php

Route::group([
    'prefix'     => 'setup',
    'as'         => 'setup.',
    'namespace'  => 'Setup',
], function () {
    Route::group([
        'middleware' => 'access.routeNeedsRole:1;2;4',
    ], function () {
        Route::group(['namespace' => 'Closing'], function () {

            Route::get('closings/deactivated', 'ClosingController@getDeactivated')->name('closings.deactivated');
            Route::get('closings/deleted', 'ClosingController@getDeleted')->name('closings.deleted');
            Route::get('closings/{opening}/approve', 'ClosingController@approve')->name('closings.approve');
            Route::patch('closings/{opening}/approve', 'ClosingController@approve')->name('closings.approve');
            /*
             * Route CRUD
             */
            Route::resource('closings', 'ClosingController');

            /*
             * Specific Route
             */
            Route::group(['prefix' => 'closings/{closing}'], function () {

                // Status
                Route::get('mark/{status}', 'ClosingController@mark')->name('closings.mark')->where(['status' => '[1,2]']);
            });

            /*
             * Deleted Route
             */
            Route::group(['prefix' => 'closings/{deletedOpening}'], function () {
                Route::get('delete', 'ClosingController@delete')->name('closings.delete-permanently');
                Route::get('restore', 'ClosingController@restore')->name('closings.restore');
            });
        });
    });
});
