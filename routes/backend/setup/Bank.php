<?php

Route::group([
    'prefix' => 'setup',
    'as' => 'setup.',
    'namespace' => 'Setup',
], function () {
    Route::group([
        'middleware' => 'access.routeNeedsRole:1;2;4',
    ], function () {
        Route::group(['namespace' => 'Bank'], function () {

            Route::get('banks/deactivated', 'BankController@getDeactivated')->name('banks.deactivated');
            Route::get('banks/deleted', 'BankController@getDeleted')->name('banks.deleted');

            /*
             * Route CRUD
             */
            Route::resource('banks', 'BankController');

            /*
             * Specific Route
             */
            Route::group(['prefix' => 'banks/{bank}'], function () {

                // Status
                Route::get('mark/{status}', 'BankController@mark')->name('banks.mark')->where(['status' => '[1,2]']);
            });

            /*
             * Deleted Route
             */
            Route::group(['prefix' => 'banks/{deletedBank}'], function () {
                Route::get('delete', 'BankController@delete')->name('banks.delete-permanently');
                Route::get('restore', 'BankController@restore')->name('banks.restore');
            });
        });

        Route::group([
            'namespace' => 'BankAccount',
        ], function () {
            Route::get('bank_accounts/deactivated', 'BankAccountController@getDeactivated')->name('bank_accounts.deactivated');
            Route::get('bank_accounts/deleted', 'BankAccountController@getDeleted')->name('bank_accounts.deleted');

            /*
            * Route CRUD
            **/
            Route::resource('bank_accounts', 'BankAccountController');

            /*
             * Specific Route
             */
            Route::group(['prefix' => 'bank_accounts/{bank_account}'], function () {

                // Status
                Route::get('/{status}', 'BankAccountController@mark')->name('bank_accounts.mark')->where(['status' => '[1,2]']);

                Route::get('delete', 'BankAccountController@delete')->name('bank_accounts.delete-permanently');
                Route::get('restore', 'BankAccountController@restore')->name('bank_accounts.restore');
            });

        });

    });
});
