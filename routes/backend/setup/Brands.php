<?php

Route::group([
    'prefix'     => 'setup',
    'as'         => 'setup.',
    'namespace'  => 'Setup',
], function () {
    Route::group([
        'middleware' => 'access.routeNeedsRole:1;2;4',
    ], function () {
        Route::group(['namespace' => 'Brand'], function () {

            Route::get('brands/deactivated', 'BrandController@getDeactivated')->name('brands.deactivated');
            Route::get('brands/deleted', 'BrandController@getDeleted')->name('brands.deleted');

            /*
             * Route CRUD
             */
            Route::resource('brands', 'BrandController');

            /*
             * Specific Route
             */
            Route::group(['prefix' => 'brands/{brand}'], function () {

                // Status
                Route::get('mark/{status}', 'BrandController@mark')->name('brands.mark')->where(['status' => '[1,2]']);
            });

            /*
             * Deleted Route
             */
            Route::group(['prefix' => 'brands/{deletedBrand}'], function () {
                Route::get('delete', 'BrandController@delete')->name('brands.delete-permanently');
                Route::get('restore', 'BrandController@restore')->name('brands.restore');
            });
        });
    });
});
