<?php

Route::group([
    'prefix'     => 'setup',
    'as'         => 'setup.',
    'namespace'  => 'Setup',
], function () {
    Route::group([
        'middleware' => 'access.routeNeedsRole:1;2;4',
    ], function () {
        Route::group(['namespace' => 'Opening'], function () {

            Route::get('openings/deactivated', 'OpeningController@getDeactivated')->name('openings.deactivated');
            Route::get('openings/deleted', 'OpeningController@getDeleted')->name('openings.deleted');
            Route::get('openings/{opening}/approve', 'OpeningController@approve')->name('openings.approve');
            Route::patch('openings/{opening}/approve', 'OpeningController@approve')->name('openings.approve');
            /*
             * Route CRUD
             */
            Route::resource('openings', 'OpeningController');

            /*
             * Specific Route
             */
            Route::group(['prefix' => 'openings/{opening}'], function () {

                // Status
                Route::get('mark/{status}', 'OpeningController@mark')->name('openings.mark')->where(['status' => '[1,2]']);
            });

            /*
             * Deleted Route
             */
            Route::group(['prefix' => 'openings/{deletedOpening}'], function () {
                Route::get('delete', 'OpeningController@delete')->name('openings.delete-permanently');
                Route::get('restore', 'OpeningController@restore')->name('openings.restore');
            });
        });
    });
});
