<?php

Route::group([
    'prefix'     => 'setup',
    'as'         => 'setup.',
    'namespace'  => 'Setup',
], function () {
    Route::group([
        'middleware' => 'access.routeNeedsRole:1;2;4',
    ], function () {
        Route::group(['namespace' => 'Branch'], function () {

            Route::get('branches/deactivated', 'BranchController@getDeactivated')->name('branches.deactivated');
            Route::get('branches/deleted', 'BranchController@getDeleted')->name('branches.deleted');

            /*
             * Route CRUD
             */
            Route::resource('branches', 'BranchController');

            /*
             * Specific Route
             */
            Route::group(['prefix' => 'branches/{branch}'], function () {

                // Status
                Route::get('mark/{status}', 'BranchController@mark')->name('branches.mark')->where(['status' => '[1,2]']);
            });

            /*
             * Deleted Route
             */
            Route::group(['prefix' => 'branches/{deletedBranch}'], function () {
                Route::get('delete', 'BranchController@delete')->name('branches.delete-permanently');
                Route::get('restore', 'BranchController@restore')->name('branches.restore');
            });
        });
    });
});
