<?php

Route::group([
    'prefix'     => 'setup',
    'as'         => 'setup.',
    'namespace'  => 'Setup',
], function () {
    Route::group([
        'middleware' => 'access.routeNeedsRole:1;2;4',
    ], function () {
        Route::group(['namespace' => 'Corporate'], function () {

            Route::get('corporates/deactivated', 'CorporateController@getDeactivated')->name('corporates.deactivated');
            Route::get('corporates/deleted', 'CorporateController@getDeleted')->name('corporates.deleted');
            Route::get('corporates/{Corporate}/approve', 'CorporateController@approve')->name('corporates.approve');
            Route::patch('corporates/{Corporate}/approve', 'CorporateController@approve')->name('corporates.approve');
            Route::get('corporates/getAccount', 'CorporateController@getAccount')->name('corporates.getAccount');
            /*
             * Route CRUD
             */
            Route::resource('corporates', 'CorporateController');

            /*
             * Specific Route
             */
            Route::group(['prefix' => 'corporates/{Corporate}'], function () {

                // Status
                Route::get('mark/{status}', 'CorporateController@mark')->name('corporates.mark')->where(['status' => '[1,2]']);
            });

            /*
             * Deleted Route
             */
            Route::group(['prefix' => 'corporates/{deletedOpening}'], function () {
                Route::get('delete', 'CorporateController@delete')->name('corporates.delete-permanently');
                Route::get('restore', 'CorporateController@restore')->name('corporates.restore');
            });
        });
    });
});
