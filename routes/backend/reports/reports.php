<?php

use App\Http\Controllers\Backend\Report\BackdateTransactionReport\BackdateTransactionReportController;
use App\Http\Controllers\Backend\Report\OutStanding\OutstandingReportController;
use App\Http\Controllers\Backend\Report\TransactionRestrictionReport\TransactionRestrictionReportController;

Route::group([
    'prefix' => 'reports',
    'as' => 'reports.',
    'namespace' => 'Report',
], function () {

    Route::group([
        'middleware' => 'access.routeNeedsPermission:menu-reports;',
    ], function () {

        Route::get('/', 'ReportController@index')->name('index');
        Route::post('show', 'ReportController@show')->name('show');

        Route::group(['namespace' => 'MrDeposit'], function () {
            Route::get('mr_deposit/{encodedReq}', 'MrDepositReportController@show')->name('mr_deposit.show');

        });

        Route::group(['namespace' => 'CoDeposit'], function () {
            Route::get('co_deposit/{encodedReq}', 'CoDepositReportController@show')->name('co_deposit.show');

        });

        Route::group(['namespace' => 'ImprestMoney'], function () {
            Route::get('imprest_money_datewise/{encodedReq}',
                'ImprestMoneyDatewiseReportController@show')->name('imprest_money_datewise.show');

        });
        Route::group(['namespace' => 'ImprestMoney'], function () {
            Route::get('imprest_money_branchwise/{encodedReq}',
                'ImprestMoneyBranchwiseReportController@show')->name('imprest_money_branchwise.show');

        });

        Route::group(['namespace' => 'Outstanding'], function () {
            Route::get('branch_wise_outstanding/{encodedReq}',
                [OutstandingReportController::class, 'branchWise'])->name('branch_wise_outstanding.show');
            Route::get('brand_wise_outstanding/{encodedReq}',
                [OutstandingReportController::class, 'brandWise'])->name('brand_wise_outstanding.show');
            Route::get('branch_brand_wise_outstanding/{encodedReq}',
                [OutstandingReportController::class, 'branchBrandWise'])->name('branch_brand_wise_outstanding.show');

        });

        Route::group(['namespace' => 'BrandWiseCollection'], function () {
            Route::get('brand_wise_collection/{encodedReq}', 'BrandWiseCollectionReportController@show')->name('brand_wise_collection.show');

        });

        Route::group(['namespace' => 'ConsolidateBankReconciliationStatement'], function () {
            Route::get('consolidate_bank_reconciliation_statement/{encodedReq}', 'ConsolidateBankReconciliationStatementController@show')->name('consolidate_bank_reconciliation_statement.show');

        });


        Route::group(['namespace' => 'TransactionRestrictionReport'], function () {
            Route::get('transaction_restriction_report/{encodedReq}', [TransactionRestrictionReportController::class,'show'])->name('transaction_restriction_report.show');

        });

        Route::group(['namespace' => 'BackdateTransactionReport'], function () {
            Route::get('backdated_transaction_report/{encodedReq}', [BackdateTransactionReportController::class,'show'])->name('backdated_transaction_report.show');

        });

    });


});
